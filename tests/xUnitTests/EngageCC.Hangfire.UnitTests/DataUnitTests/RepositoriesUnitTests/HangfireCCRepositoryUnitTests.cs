﻿using AutoMapper;
using Engage3.Common.Business.Interfaces;
using EngageCC.DataContext.DataModel;
using Hangfire.Data.Repositories;
using Hangfire.Models.Signing;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Hangfire.UnitTests.DataUnitTests.RepositoriesUnitTests
{
    public class HangfireCCRepositoryUnitTests
    {


        [Fact]
        public async Task InsertEngagementLetterTable_WhenValidDataProvided_ShouldInsertDataToDbContext()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestHangfireCCDatabase_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var mapperConfig = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<EngagementLetterResponse, EngagementLetter>();
                });
                var mapper = new Mapper(mapperConfig);
                
                var repository = new HangfireCCRepository(context, options, mapper, null);

                var engagementLetters = new List<EngagementLetterResponse>
                {
                     new EngagementLetterResponse
                        {
                            EngagementLetterId = 1,
                            BatchId = 2,
                            EngagementLetterName = "Sample Letter",
                            OfficeId = 3,
                            OfficeName = "Sample Office",
                            YearId = 4,
                            TaxYear = 2023,
                            TemplateId = 5,
                            TemplateName = "Template 1",
                            TemplateVersion = 1,
                            ClientId = 6,
                            ClientName = "Client A",
                            PartnerId = 7,
                            PartnerName = "Partner X",
                            AdminId = 8,
                            AdminName = "Admin Y",
                            DocumentStatusId = 9,
                            DepartmentId = 10,
                            EngageTypeId = 11,
                            CreatedOn = DateTime.Now,
                            CreatedBy = "User1",
                            ModifiedOn = null,
                            IsDeleted = false,
                            DeletedOn = null,
                            DeletedBy = null,
                            ModifiedBy = null,
                            BulkLettersId = 12,
                            IsProcess = true,
                            IsEnqueue = false,
                            Is7216Available = true,
                            ClientSignatureCount = 3,
                            IsNewClient = false,
                            SpouseFirstName = "Spouse A",
                            SpouseEmailId = "spouse@example.com",
                            SignatoryEmailId = "signatory@example.com",
                            SignatoryFirstName = "Signatory A",
                            IsEsigning = true,
                            PdfUrl = "Valid Pdf content",
                        },
                };

                // Act
                var result = await repository.InsertEngagementLetterTable(engagementLetters);

                // Assert
                Assert.True(result);
                var insertedData = context.EngagementLetter.ToList();
                Assert.Equal(engagementLetters.Count, insertedData.Count);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetBatchRequest_WhenDataExists_ShouldReturnBatchRequest()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestHangfireCCDatabase_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {

                context.BatchRequest.Add(new BatchRequest
                {
                    BatchRequestId = 1,
                    BatchId = 2,
                    BatchJson = "Sample JSON",
                    IsProcess = false,
                    Status = "Pending",
                    TemplateId = 3,
                    TemplateName = "Sample Template",
                    CreatedOn = DateTime.Now,
                    CreatedBy = "User1",
                });
                context.SaveChanges();

                var mapperConfig = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<EngagementLetterResponse, EngagementLetter>();
                });
                var mapper = new Mapper(mapperConfig);

                var repository = new HangfireCCRepository(context, options, mapper, null);

                // Act
                var result = await repository.GetBatchRequest("Pending", false);

                // Assert
                Assert.NotNull(result);
                Assert.Equal("Sample JSON", result.BatchJson);
                Assert.True(result.IsProcess);
                context.Dispose();
            }
        }

        [Fact]
        public async Task UpdateBatchRequestIsProcess_WhenBatchExists_ShouldUpdateIsProcess()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestHangfireCCDatabase_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                context.BatchRequest.Add(new BatchRequest
                {
                    BatchRequestId = 1,
                    BatchId = 2,
                    BatchJson = "Sample JSON",
                    IsProcess = false,
                    Status = "Pending",
                    TemplateId = 3,
                    TemplateName = "Sample Template",
                    CreatedOn = DateTime.Now,
                    CreatedBy = "User1",
                });
                context.SaveChanges();
                var getCurrentDatetimeMock = new Mock<IGetCurrentDatetime>();
                getCurrentDatetimeMock.Setup(mock => mock.GetCurrentEstDatetime())
          .ReturnsAsync(DateTime.Now);
                var repository = new HangfireCCRepository(context, options, null,getCurrentDatetimeMock.Object);

                // Act
                await repository.UpdateBatchRequestIsProcess(2);

                // Assert
                var updatedData = context.BatchRequest.FirstOrDefault(n => n.BatchId == 2);
                Assert.NotNull(updatedData);
                Assert.False(updatedData.IsProcess);
                context.Dispose();
            }
        }


        [Fact]
        public async Task GetBulkLettersList_WhenBulkletterIdsExist_ShouldReturnMatchingRecords()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestHangfireCCDatabase_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var bulkLetters = new List<BulkLetters>
            {
                new BulkLetters
                {
                        BulkLettersId = 1,
                        ClientId = 2,
                        ClientName = "Client A",
                        Office = "Office X",
                        SignatoryEmailId = "signatory@example.com",
                        SignatoryFirstName = "John",
                        PartnerName = "Partner Z",
                        BatchId = 3,
                        TemplateName = "Template 1",
                        FieldJson = "JSON Data",
                        DocumentStatusName = "Approved",

                },
                new BulkLetters
                {
                    BulkLettersId = 2,
                    ClientId = 3,
                    ClientName = "Client B",
                    Office = "Office Y",
                    SignatoryEmailId = "signatory2@example.com",
                    SignatoryFirstName = "Alice",
                    PartnerName = "Partner W",
                    BatchId = 4,
                    TemplateName = "Template 2",
                    FieldJson = "More JSON Data",
                    DocumentStatusName = "Pending",

                },
            };
                context.BulkLetters.AddRange(bulkLetters);
                context.SaveChanges();

                var repository = new HangfireCCRepository(context, options, null, null);

                var bulkletterIdsToRetrieve = new List<int?> { 1, 2 };

                // Act
                var result = await repository.GetBulkLettersList(bulkletterIdsToRetrieve);

                // Assert
                Assert.NotNull(result);
                Assert.Equal(2, result.Count);
                Assert.Equal("Client A", result[0].ClientName);
                Assert.Equal("Client B", result[1].ClientName);
                context.Dispose();
            }
        }


        [Fact]
        public async Task UpdateEngagementLetterStatus_WhenBatchExists_ShouldUpdateStatus()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestHangfireCCDatabase_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var batchId = 1;
                var bulkLetters = new List<BulkLetters>
            {
                new BulkLetters
                {
                    BulkLettersId = 1,
                        ClientId = 2,
                        ClientName = "Client A",
                        Office = "Office X",
                        SignatoryEmailId = "signatory@example.com",
                        SignatoryFirstName = "John",
                        PartnerName = "Partner Z",
                        BatchId = 1,
                        TemplateName = "Template 1",
                        FieldJson = "JSON Data",
                        DocumentStatusName = "Pending",
                },
                new BulkLetters
                {
                    BulkLettersId = 2,
                    ClientId = 3,
                    ClientName = "Client B",
                    Office = "Office Y",
                    SignatoryEmailId = "signatory2@example.com",
                    SignatoryFirstName = "Alice",
                    PartnerName = "Partner W",
                    BatchId = 1,
                    TemplateName = "Template 2",
                    FieldJson = "More JSON Data",
                    DocumentStatusName = "Pending",
                },
            };

                var batchRequest = new BatchRequest
                {
                    BatchRequestId = 1,
                    BatchId = batchId,
                    Status = "Completed - Batch",
                    ModifiedOn = null,
                };

                context.BulkLetters.AddRange(bulkLetters);
                context.BatchRequest.Add(batchRequest);
                context.SaveChanges();
                var getCurrentDatetimeMock = new Mock<IGetCurrentDatetime>();
                getCurrentDatetimeMock.Setup(mock => mock.GetCurrentEstDatetime())
          .ReturnsAsync(DateTime.Now);
                var repository = new HangfireCCRepository(context, options, null, getCurrentDatetimeMock.Object);

                // Act
                var result = await repository.UpdateEngagementLetterStatus(batchId);

                // Assert
                Assert.True(result);

                var updatedBulkLetters = context.BulkLetters
                    .Where(n => n.BatchId == batchId)
                    .ToList();
                var updatedBatchRequest = context.BatchRequest.FirstOrDefault(n => n.BatchId == batchId);
                Assert.NotNull(updatedBatchRequest);
                Assert.Equal("Completed - Batch", updatedBatchRequest.Status);
                context.Dispose();
            }
        }


        [Fact]
        public async Task GetEngagementLettersList_WhenRecordsExist_ShouldReturnMatchingRecordsAndUpdateEnqueueStatus()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestHangfireCCDatabase_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var engagementLetters = new List<EngagementLetter>
            {
                new EngagementLetter
                {
                    EngagementLetterId = 1,
                    IsProcess = false,
                    IsEnqueue = false,
                    IsDeleted = false,
                },
                new EngagementLetter
                {
                    EngagementLetterId = 2,
                    IsProcess = false,
                    IsEnqueue = false,
                    IsDeleted = false,
                },
                new EngagementLetter
                {
                    EngagementLetterId = 3,
                    IsProcess = true,
                    IsEnqueue = true,
                    IsDeleted = false,
                },
            };

                context.EngagementLetter.AddRange(engagementLetters);
                context.SaveChanges();

                var repository = new HangfireCCRepository(context, options, null, null);

                // Act
                var result = await repository.GetEngagementLettersList();

                // Assert
                Assert.NotNull(result);
                Assert.Equal(2, result.Count);
                foreach (var engagementLetter in result)
                {
                    Assert.True(engagementLetter.IsEnqueue);
                }

                context.Dispose();
            }
        }

        [Fact]
        public async Task GetMasterTemplate_WhenTemplateExists_ShouldReturnTemplate()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestHangfireCCDatabase_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var templateId = 1;
                var masterTemplate = new MasterTemplate
                {
                    TemplateId = templateId,
                    TemplateVersionId = 1,
                    Template = "Sample Template Content",
                };

                context.MasterTemplate.Add(masterTemplate);
                context.SaveChanges();

                var repository = new HangfireCCRepository(context, options, null, null);

                // Act
                var result = await repository.GetMasterTemplate(templateId);

                // Assert
                Assert.NotNull(result);
                Assert.Equal("Sample Template Content", result);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetClientSignatureCountfromTemplate_WhenTemplateExists_ShouldReturnClientSignatureCount()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestHangfireCCDatabase_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                // Initialize the database with a sample Template record
                var templateId = 1;
                var clientSignatureCount = 3;
                var template = new Template
                {
                    TemplateId = templateId,
                    ClientSignatureCount = clientSignatureCount,
                };

                context.Template.Add(template);
                context.SaveChanges();

                var repository = new HangfireCCRepository(context, options, null, null);

                // Act
                var result = await repository.GetClientSignatureCountfromTemplate(templateId);

                // Assert
                Assert.Equal(clientSignatureCount, result);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetClientSignatureCountfromTemplate_WhenTemplateDoesNotExist_ShouldReturnZero()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestHangfireCCDatabase_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var repository = new HangfireCCRepository(context, options, null, null);

                // Act
                var result = await repository.GetClientSignatureCountfromTemplate(templateId: -1);

                // Assert
                Assert.Equal(0, result);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetTemplateAttachments_WhenAttachmentsExist_ShouldReturnAttachmentsURL()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestHangfireCCDatabase_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var templateId = 1;
                var batchId = 2;
                var versionNumber = 1;
                var attachmentsURL = "Sample Attachments URL";

                var bulkLetterAttachments = new BulkLetterAttachments
                {
                    TemplateId = templateId,
                    BatchId = batchId,
                    VersionNumber = versionNumber,
                    AttachmentsURL = attachmentsURL,
                };

                context.BulkLetterAttachments.Add(bulkLetterAttachments);
                context.SaveChanges();

                var repository = new HangfireCCRepository(context, options, null, null);

                // Act
                var result = await repository.GetTemplateAttachments(templateId, batchId);

                // Assert
                Assert.Equal(attachmentsURL, result);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetTemplateAttachments_WhenAttachmentsDoNotExist_ShouldReturnTemplateAttachmentURL()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestHangfireCCDatabase_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var templateId = 1;
                var attachmentURL = "Sample Template Attachment URL";

                var template = new Template
                {
                    TemplateId = templateId,
                    AttachmentURL = attachmentURL,
                };

                context.Template.Add(template);
                context.SaveChanges();

                var repository = new HangfireCCRepository(context, options, null, null);

                // Act
                var result = await repository.GetTemplateAttachments(templateId: -1, batchId: -1);

                // Assert
                Assert.Empty(result);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetLetterFieldValues_WhenBulkLetterExists_ShouldReturnLatestLetterFieldValues()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestHangfireCCDatabase_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {

                var bulkLetterId = 1;

                var letterFieldValues1 = new LetterFieldValues
                {
                    BulkLettersId = bulkLetterId,
                    LetterVersion = 1,
                };

                var letterFieldValues2 = new LetterFieldValues
                {
                    BulkLettersId = bulkLetterId,
                    LetterVersion = 2,
                };

                context.LetterFieldValues.AddRange(new List<LetterFieldValues> { letterFieldValues1, letterFieldValues2 });
                context.SaveChanges();

                var repository = new HangfireCCRepository(context, options, null, null);

                // Act
                var result = await repository.GetLetterFieldValues(bulkLetterId);

                // Assert
                Assert.NotEmpty(result);
                Assert.Equal(1, result.Count);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetLetterFieldValues_WhenBulkLetterDoesNotExist_ShouldReturnEmptyList()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestHangfireCCDatabase_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var repository = new HangfireCCRepository(context, options, null, null);

                // Act
                var result = await repository.GetLetterFieldValues(bulkLetterId: -1);

                // Assert
                Assert.Empty(result);
                context.Dispose();
            }
        }

        [Fact]
        public async Task SaveSigningInfos_WhenValidSigningInfosProvided_ShouldSaveToDbContext()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestHangfireCCDatabase_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var repository = new HangfireCCRepository(context, options, null, null);

                var signingInfos = new List<SigningInfo>
            {
                new SigningInfo
                {
                     EngagementLetterId = 1,
                    SigningPartnerName = "John Doe",
                    DelegatedName = "Delegated Person",
                    SPEmail = "sp@example.com",
                    ContactPerson1Name = "Contact Person 1",
                    ContactPerson1Email = "cp1@example.com",
                    ContactPerson2Name = "Contact Person 2",
                    ContactPerson2Email = "cp2@example.com",
                    Title = "Signing Title",
                    Message = "Signing Message",
                    IsDeleted = false,
                    CreatedBy = "User1",
                    CreatedOn = DateTime.Now,
                    ModifiedOn = null,
                    ManualSigning = false,
                    FileId = "File123",
                    DelegatedBy = "Delegated By",
                    AgreementId = "Agreement123",
                    ModifiedBy = "ModifiedBy",
                    DeletedOn = null,
                    DeletedBy = "DeletedBy",
                    PdfUrl = "https://example.com/sample.pdf",
                    ExpiryDate7216 = "2023-12-31",
                    AdminName = "Admin User",
                    AdminEmail = "admin@example.com",
                    PartnerEmail = "partner@example.com",
                    PartnerName = "Partner Name"
                }
            };

                // Act
                var result = await repository.SaveSigningInfos(signingInfos);

                // Assert
                Assert.True(result); // Expecting success
                var savedSigningInfos = context.SigningInfo.ToList();
                Assert.Equal(signingInfos.Count, savedSigningInfos.Count);
                context.Dispose();
            }
        }

        [Fact]
        public async Task SaveSigningInfos_WhenEmptyListProvided_ShouldReturnFalse()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestHangfireCCDatabase_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var repository = new HangfireCCRepository(context, options, null, null);

                var emptySigningInfos = new List<SigningInfo>();

                // Act
                var result = await repository.SaveSigningInfos(emptySigningInfos);

                // Assert
                Assert.False(result);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetSignatoryLastName_WhenValidBulkLettersIdAndClientId_ShouldReturnSignatoryLastName()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestDataContext_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var bulklettersId = 1;
                var clientId = 2;
                var fieldName = "SignatoryLastName";
                var fieldValue = "John Doe";

                var letterFieldValues = new List<LetterFieldValues>
        {
            new LetterFieldValues
            {
                BulkLettersId = bulklettersId,
                ClientId = clientId,
                FieldName = fieldName,
                FieldValue = fieldValue,
                LetterVersion = 1
            },
            new LetterFieldValues
            {
                BulkLettersId = bulklettersId,
                ClientId = clientId,
                FieldName = fieldName,
                FieldValue = "John Doe",
                LetterVersion = 2
            }
        };

                context.LetterFieldValues.AddRange(letterFieldValues);
                context.SaveChanges();

                var repository = new HangfireCCRepository(context, options, null, null);

                // Act
                var result = await repository.GetSignatoryLastName(bulklettersId, clientId);

                // Assert
                Assert.Equal(fieldValue, result);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetSignatoryLastName_WhenInvalidBulkLettersIdOrClientId_ShouldReturnEmptyString()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestDataContext_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var bulklettersId = -1;
                var clientId = 2;
                var fieldName = "SignatoryLastName";
                var fieldValue = "John Doe";

                var letterFieldValues = new List<LetterFieldValues>
        {
            new LetterFieldValues
            {
                BulkLettersId = bulklettersId,
                ClientId = clientId,
                FieldName = fieldName,
                FieldValue = fieldValue,
                LetterVersion = 1
            }
        };

                context.LetterFieldValues.AddRange(letterFieldValues);
                context.SaveChanges();

                var repository = new HangfireCCRepository(context, options, null, null);

                // Act
                var result = await repository.GetSignatoryLastName(bulklettersId, clientId);

                // Assert
                Assert.Equal(string.Empty, result);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetChildEntityName_WhenValidBulkLettersIdAndClientId_ShouldReturnChildEntityName()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestDataContext_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var bulklettersId = 1;
                var clientId = 2;
                var fieldName = "ChildEntities";
                var fieldValue = "ChildEntity1,ChildEntity2,ChildEntity3";

                var letterFieldValues = new List<LetterFieldValues>
        {
            new LetterFieldValues
            {
                BulkLettersId = bulklettersId,
                ClientId = clientId,
                FieldName = fieldName,
                FieldValue = fieldValue,
                LetterVersion = 1
            },
            new LetterFieldValues
            {
                BulkLettersId = bulklettersId,
                ClientId = clientId,
                FieldName = fieldName,
                FieldValue = "ChildEntity4,ChildEntity5",
                LetterVersion = 2
            }
        };

                context.LetterFieldValues.AddRange(letterFieldValues);
                context.SaveChanges();

                var repository = new HangfireCCRepository(context, options, null, null);

                // Act
                var result = await repository.GetChildEntityName(bulklettersId, clientId);

                // Assert
                Assert.NotNull(result);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetChildEntityName_WhenInvalidBulkLettersIdOrClientId_ShouldReturnEmptyString()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestDataContext_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var bulklettersId = -1;
                var clientId = 2;
                var fieldName = "ChildEntities";
                var fieldValue = "ChildEntity1,ChildEntity2,ChildEntity3";

                var letterFieldValues = new List<LetterFieldValues>
        {
            new LetterFieldValues
            {
                BulkLettersId = bulklettersId,
                ClientId = clientId,
                FieldName = fieldName,
                FieldValue = fieldValue,
                LetterVersion = 1
            }
        };

                context.LetterFieldValues.AddRange(letterFieldValues);
                context.SaveChanges();

                var repository = new HangfireCCRepository(context, options, null, null);

                // Act
                var result = await repository.GetChildEntityName(bulklettersId, clientId);

                // Assert
                Assert.Equal(string.Empty, result);
                context.Dispose();
            }
        }

        [Fact]
        public async Task UpdateEngagementLetter_WhenValidDataProvided_ShouldUpdateEngagementLetters()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestDataContext_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var bulkLettersId = 1;

                var engagementLetters = new List<EngagementLetter>
        {
            new EngagementLetter
            {
                BulkLettersId = bulkLettersId,
                IsEsigning = true
            },
            new EngagementLetter
            {
                BulkLettersId = 2,
                IsEsigning = false
            }
        };

                context.EngagementLetter.AddRange(engagementLetters);
                context.SaveChanges();

                var repository = new HangfireCCRepository(context, options, null, null);

                // Act
                var result = await repository.UpdateEngagementLetter(engagementLetters);

                // Assert
                Assert.False(result);
                context.Dispose();
            }
        }

        [Fact]
        public async Task UpdateEngagementLetter_WhenNoDataProvided_ShouldReturnFalse()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestDataContext_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var repository = new HangfireCCRepository(context, options, null, null);

                // Act
                var result = await repository.UpdateEngagementLetter(new List<EngagementLetter>());

                // Assert
                Assert.False(result);
                context.Dispose();
            }
        }

        [Fact]
        public async Task UpdateHistoryLog_WhenValidDataProvided_ShouldUpdateHistoryLog()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestDataContext_" + Guid.NewGuid().ToString())
                .Options;
            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<HistoryLogModel, HistoryLog>();
            });
            var mapper = new Mapper(mapperConfig);

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var historyLogData = new List<HistoryLogModel>
        {
            new HistoryLogModel
            {
                HistoryLogId = 1,
                EngagementLetterName = "Log 1",
                DeclineTimestamp = DateTime.UtcNow
            },
            new HistoryLogModel
            {
                HistoryLogId = 2,
                EngagementLetterName = "Log 2",
                DeclineTimestamp = DateTime.UtcNow
            }
        };

                var repository = new HangfireCCRepository(context, options, mapper, null);

                // Act
                var result = await repository.UpdateHistoryLog(historyLogData);

                // Assert
                Assert.True(result);

                var updatedHistoryLog = context.HistoryLog.ToList();

                Assert.Equal(historyLogData.Count, updatedHistoryLog.Count);

                foreach (var item in historyLogData)
                {
                    var updatedItem = updatedHistoryLog.FirstOrDefault(e => e.HistoryLogId == item.HistoryLogId);
                    Assert.NotNull(updatedItem);
                    Assert.Equal(item.EngagementLetterName, updatedItem.EngagementLetterName);
                    Assert.Equal(item.DeclineTimestamp, updatedItem.DeclineTimestamp);
                }

                context.Dispose();
            }
        }

        [Fact]
        public async Task UpdateHistoryLog_WhenNoDataProvided_ShouldReturnFalse()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestDataContext_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var repository = new HangfireCCRepository(context, options, null, null);

                // Act
                var result = await repository.UpdateHistoryLog(new List<HistoryLogModel>());

                // Assert
                Assert.False(result);
                context.Dispose();
            }
        }


        [Fact]
        public async Task GetAgreementIdsList_WhenValidDataProvided_ShouldReturnSigningInfoModelsUsingMapper()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestDataContext_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var signingInfos = new List<SigningInfo>
        {
            new SigningInfo
            {
                EngagementLetterId = 1,
                AgreementId = "Agreement1",
                ManualSigning = false
            },
            new SigningInfo
            {
                EngagementLetterId = 2,
                AgreementId = "Agreement2",
                ManualSigning = true
            },
            new SigningInfo
            {
                EngagementLetterId = 3,
                AgreementId = "Agreement3",
                ManualSigning = false
            },
        };
                context.SigningInfo.AddRange(signingInfos);
                context.SaveChanges();

                var mapper = new Mock<IMapper>();
                mapper.Setup(m => m.Map<List<SigningInfoModel>>(It.IsAny<List<SigningInfo>>()))
                    .Returns((List<SigningInfo> input) => input.Select(si => new SigningInfoModel { EngagementLetterId = si.EngagementLetterId, AgreementId = si.AgreementId }).ToList());

                var repository = new HangfireCCRepository(context, options, mapper.Object, null);

                // Act
                var signingInfoModels = await repository.GetAgreementIdsList(signingInfos);

                // Assert
                Assert.NotNull(signingInfoModels);
                Assert.NotEmpty(signingInfoModels);
                Assert.Equal(2, signingInfoModels.Count);
                Assert.Contains(signingInfoModels, si => si.EngagementLetterId == 1 && si.AgreementId == "Agreement1");
                Assert.Contains(signingInfoModels, si => si.EngagementLetterId == 3 && si.AgreementId == "Agreement3");
                context.Dispose();
            }
        }


        [Fact]
        public async Task GetEngagementLetterName_WhenLetterIdIsValid_ShouldReturnEngagementLetterName()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestDataContext_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var engagementLetter = new EngagementLetter
                {
                    EngagementLetterId = 1,
                    EngagementLetterName = "Sample Engagement Letter",
                };
                context.EngagementLetter.Add(engagementLetter);
                context.SaveChanges();

                var repository = new HangfireCCRepository(context, options, null, null);

                // Act
                var result = await repository.GetEngagementLetterName(1);

                // Assert
                Assert.Equal("Sample Engagement Letter", result);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetEngagementLetterName_WhenLetterIdIsInvalid_ShouldReturnEmptyString()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestDataContext_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var repository = new HangfireCCRepository(context, options, null, null);

                // Act
                var result = await repository.GetEngagementLetterName(2); 

                // Assert
                Assert.Equal(string.Empty, result);
                context.Dispose();
            }
        }

        [Fact]
        public async Task EngagementLetterErrorLog_WhenEngagementLetterIsValid_ShouldAddErrorLogEntry()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestDataContext_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var engagementLetter = new EngagementLetter
                {
                    EngagementLetterId = 1,
                    BatchId = 2,
                    ClientId = 3,
                    CreatedOn = DateTime.Now,
                };
                context.EngagementLetter.Add(engagementLetter);
                context.SaveChanges();

                var mapperMock = new Mock<IMapper>();
                var getCurrentDatetimeMock = new Mock<IGetCurrentDatetime>();
                getCurrentDatetimeMock.Setup(mock => mock.GetCurrentEstDatetime())
          .ReturnsAsync(DateTime.Now);
                var repository = new HangfireCCRepository(context, options, mapperMock.Object, getCurrentDatetimeMock.Object);

                mapperMock.Setup(mapper => mapper.Map<BulkLogger>(It.IsAny<BulkLetterErrorLogVM>()))
                    .Returns((BulkLetterErrorLogVM vm) => new BulkLogger
                    {
                        EngagementLetterId = vm.EngagementLetterId,
                        BatchId = vm.BatchId,
                        ClientId = vm.ClientId,
                        RequestLog = vm.RequestLog,
                        CreatedOn = vm.CreatedOn,
                        ModifiedOn = DateTime.Now,
                        ErrorMessage = vm.ErrorMessage
                    });

                // Act
                await repository.EngagementLetterErrorLog(engagementLetter, "Sample Request Log", "Sample Error Message");

                // Assert
                var errorLogEntry = context.BulkLoggers.FirstOrDefault();
                Assert.NotNull(errorLogEntry);
                Assert.Equal(1, errorLogEntry.EngagementLetterId);
                Assert.Equal(2, errorLogEntry.BatchId);
                Assert.Equal(3, errorLogEntry.ClientId);
                Assert.Equal("Sample Request Log", errorLogEntry.RequestLog);
                Assert.Equal("Sample Error Message", errorLogEntry.ErrorMessage);
                mapperMock.Verify(mapper => mapper.Map<BulkLogger>(It.IsAny<BulkLetterErrorLogVM>()), Times.Once);
                context.Dispose();
            }
        }


        [Fact]
        public async Task EngagementLetterErrorLog_WhenEngagementLetterIsNull_ShouldNotAddErrorLogEntry()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestDataContext_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var repository = new HangfireCCRepository(context, options, null, null);

                // Act
                await repository.EngagementLetterErrorLog(null, "Sample Request Log", "Sample Error Message");

                // Assert
                var errorLogEntry = context.BulkLoggers.FirstOrDefault();
                Assert.Null(errorLogEntry);
                context.Dispose();
            }
        }

        [Fact]
        public async Task EsigningEngagementLetterErrorLog_WhenAgreementIsValid_ShouldAddErrorLogEntry()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestDataContext_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var mapperMock = new Mock<IMapper>();
                var getCurrentDatetimeMock = new Mock<IGetCurrentDatetime>();
                getCurrentDatetimeMock.Setup(mock => mock.GetCurrentEstDatetime())
          .ReturnsAsync(DateTime.Now);
                var repository = new HangfireCCRepository(context, options, mapperMock.Object, getCurrentDatetimeMock.Object);
                mapperMock.Setup(mapper => mapper.Map<BulkLogger>(It.IsAny<BulkLetterErrorLogVM>()))
                    .Returns((BulkLetterErrorLogVM vm) => new BulkLogger
                    {
                        EngagementLetterId = vm.EngagementLetterId,
                        RequestLog = vm.RequestLog,
                        CreatedOn = vm.CreatedOn,
                        ModifiedOn = vm.ModifiedOn,
                        ErrorMessage = vm.ErrorMessage
                    });
                var agreement = new SigningInfoModel
                {
                    EngagementLetterId = 1
                };

                // Act
                await repository.EsigningEngagementLetterErrorLog(agreement, "Sample Request Log", "Sample Error Message");

                // Assert
                var errorLogEntry = context.BulkLoggers.FirstOrDefault();
                Assert.NotNull(errorLogEntry);
                Assert.Equal(1, errorLogEntry.EngagementLetterId);
                Assert.Equal("Sample Request Log", errorLogEntry.RequestLog);
                Assert.Equal("Sample Error Message", errorLogEntry.ErrorMessage);
                mapperMock.Verify(mapper => mapper.Map<BulkLogger>(It.IsAny<BulkLetterErrorLogVM>()), Times.Once);
                context.Dispose();
            }
        }

        [Fact]
        public async Task SaveEngagementLetterErrorLog_WhenValidData_ShouldAddErrorLogEntry()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<EngageCC.DataContext.DataContext>()
                .UseInMemoryDatabase(databaseName: "TestDataContext_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new EngageCC.DataContext.DataContext(options))
            {
                var mapperMock = new Mock<IMapper>();
                var getCurrentDatetimeMock = new Mock<IGetCurrentDatetime>();
                getCurrentDatetimeMock.Setup(mock => mock.GetCurrentEstDatetime())
.ReturnsAsync(DateTime.Now);
                var repository = new HangfireCCRepository(context, options, mapperMock.Object, getCurrentDatetimeMock.Object);
                mapperMock.Setup(mapper => mapper.Map<BulkLogger>(It.IsAny<BulkLetterErrorLogVM>()))
                    .Returns((BulkLetterErrorLogVM vm) => new BulkLogger
                    {
                        BatchId = vm.BatchId,
                        RequestLog = vm.RequestLog,
                        CreatedOn = vm.CreatedOn,
                        ModifiedOn = vm.ModifiedOn,
                        ErrorMessage = vm.ErrorMessage
                    });

                // Act
                await repository.SaveEngagementLetterErrorLog(1, "Sample Request Log", "Sample Error Message");

                // Assert
                var errorLogEntry = context.BulkLoggers.FirstOrDefault();
                Assert.NotNull(errorLogEntry);
                Assert.Equal(1, errorLogEntry.BatchId);
                Assert.Equal("Sample Request Log", errorLogEntry.RequestLog);
                Assert.Equal("Sample Error Message", errorLogEntry.ErrorMessage);
                mapperMock.Verify(mapper => mapper.Map<BulkLogger>(It.IsAny<BulkLetterErrorLogVM>()), Times.Once);
                context.Dispose();
            }
        }



    }
}
