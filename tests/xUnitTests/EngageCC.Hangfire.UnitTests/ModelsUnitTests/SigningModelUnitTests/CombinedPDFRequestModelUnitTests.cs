﻿using Hangfire.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Hangfire.UnitTests.ModelsUnitTests.SigningModelUnitTests
{
    public class CombinedPDFRequestModelUnitTests
    {
        [Fact]
        public void CombinedPDFRequest_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new CombinedPDFRequest
            {
                assets = new List<Assete>
                {
                    new Assete { assetID = "Asset1" },
                    new Assete { assetID = "Asset2" }
                }
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void Metadatas_ValidModel_PassesValidation()
        {
            // Arrange
            string type = "document";
            int size = 1024;

            // Act
            var model = new Metadatas
            {
                type = type,
                size = size,
            };
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void CombinedPDFResponseModel_ValidModel_PassesValidation()
        {
            // Arrange
            string status = "Success";

            var assetes = new Assetes
            {
                assetID = "1",
                downloadUri = "Test",
               metadata = new Metadatas
               { 
                   type = "12", size = 12
               }
            };

            // Act
            var model = new CombinedPDFResponseModel
            {
                status = status,
                asset = assetes,
            };
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
