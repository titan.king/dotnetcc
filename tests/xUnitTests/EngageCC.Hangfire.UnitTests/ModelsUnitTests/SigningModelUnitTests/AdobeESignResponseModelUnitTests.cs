﻿using Hangfire.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileInfo = Hangfire.Models.Signing.FileInfo;

namespace EngageCC.Hangfire.UnitTests.ModelsUnitTests.SigningModelUnitTests
{
    public class AdobeESignResponseModelUnitTests
    {
        [Fact]
        public void AdobeESignResponse_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new AdobeESignResponse
            {
                id = "ValidId"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }


        [Fact]
        public void SignedDocumentResponse_ValidModel_PassesValidation()
        {
            // Arrange
            string url = "https://example.com/documents/signed_document.pdf";

            // Act
            var model = new SignedDocumentResponse
            {
                url = url,
            };
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void ParticipantSetsInfo_ValidModel_PassesValidation()
        {
            // Arrange
            var memberInfos = new List<MemberInfo>
        {
            new MemberInfo { email = "JohnDoe@gmail.com" },
            new MemberInfo { email = "JaneSmith@yahoo.co.in" },
        };
            int order = 1;
            string role = "Signatory";

            // Act
            var model = new ParticipantSetsInfo
            {
                memberInfos = memberInfos,
                order = order,
                role = role,
            };
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void FileInfo_ValidModel_PassesValidation()
        {
            // Arrange
            string mimeType = "application/pdf";
            string name = "example.pdf";
            string url = "https://example.com/files/example.pdf";

            var urlFileInfo = new UrlFileInfo
            {
                mimeType = mimeType,
                name = name,
                url = url,
            };

            // Act
            var model = new FileInfo
            {
                urlFileInfo = urlFileInfo,
            };

            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void AdobeESignExpireyModel_ValidModel_PassesValidation()
        {
            // Arrange
            List<FileInfo> fileInfos = new List<FileInfo>
        {
            new FileInfo { urlFileInfo = new UrlFileInfo() }
        };
            string name = "MyDocument";
            List<ParticipantSetsInfo> participantSetsInfo = new List<ParticipantSetsInfo>
        {
            new ParticipantSetsInfo { order = 1, role = "Signers" }
        };
            string signatureType = "ESign";
            ExternalId externalId = new ExternalId { id = "12345" };
            string state = "Draft";
            string message = "Document ready for signature";

            // Act
            var model = new AdobeESignExpireyModel
            {
                fileInfos = fileInfos,
                name = name,
                participantSetsInfo = participantSetsInfo,
                signatureType = signatureType,
                externalId = externalId,
                state = state,
                message = message
            };

            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

    }
}
