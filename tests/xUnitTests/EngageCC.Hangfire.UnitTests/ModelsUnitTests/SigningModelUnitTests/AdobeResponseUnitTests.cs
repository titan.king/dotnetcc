﻿using Hangfire.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Hangfire.UnitTests.ModelsUnitTests.SigningModelUnitTests
{
    public class AdobeResponseUnitTests
    {

        [Fact]
        public void AdobeResponse_ValidModel_PassesValidation()
        {
            // Arrange
            string uploadUri = "https://example.com/upload";
            string assetID = "12345";
            string attachmentPDFContent = "Sample PDF content";
            var model = new AdobeResponse
            {
                uploadUri = uploadUri,
                assetID = assetID,
                AttachemntPDFcontent = attachmentPDFContent,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
