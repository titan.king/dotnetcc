﻿using Hangfire.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Hangfire.UnitTests.ModelsUnitTests.SigningModelUnitTests
{
    public class HtmlFileResponseModelUnitTests
    {
        [Fact]
        public void HtmlFileResponseModel_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new HtmlFileResponseModel
            {
                HtmlUrl = "https://example.com/sample.html",
                StatusCode = HttpStatusCode.OK,
                Message = "HTML file retrieved successfully"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
