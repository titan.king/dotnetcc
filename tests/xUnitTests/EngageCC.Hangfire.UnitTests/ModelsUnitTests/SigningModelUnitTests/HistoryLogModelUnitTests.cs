﻿using Hangfire.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Hangfire.UnitTests.ModelsUnitTests.SigningModelUnitTests
{
    public class HistoryLogModelUnitTests
    {
        [Fact]
        public void HistoryLogModel_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new HistoryLogModel
            {
                HistoryLogId = 1,
                EngagementLetterId = 123,
                EngagementLetterName = "Letter1",
                BatchId = 456,
                Status = "Processed",
                EditedBy = "User1",
                Version = 2,
                LastModified = new DateTime(2023, 10, 15),
                Downloaded = new DateTime(2023, 10, 16),
                Delegated = true,
                ClientEmailId = "client@example.com",
                ReasonforDecline = "Not approved",
                DeclineTimestamp = new DateTime(2023, 10, 17),
                PDFUrl = "http://example.com/document.pdf",
                IncrementedVersion = 3
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
