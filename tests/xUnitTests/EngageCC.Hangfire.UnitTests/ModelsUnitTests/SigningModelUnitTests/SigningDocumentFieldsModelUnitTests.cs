﻿using Hangfire.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Hangfire.Models.Signing.SigningDocumentFields;

namespace EngageCC.Hangfire.UnitTests.ModelsUnitTests.SigningModelUnitTests
{
    public class SigningDocumentFieldsModelUnitTests
    {
        [Fact]
        public void SigningDocumentFields_ValidModel_PassesValidation()
        {

            var model = new SigningDocumentFields
            {
                EngagementLetterId = 1,
                SigningPartnerName = "John Smith",
                SigningPartnerSignatureImage = "signature-image-url",
                DelegatedName = "Alice Johnson",
                SPEmail = "john.smith@example.com",
                DelegatedEmail = "alice.johnson@example.com",
                ContactPerson1Name = "Bob Davis",
                ContactPerson1Email = "bob.davis@example.com",
                ContactPerson2Name = "Eve Wilson",
                ContactPerson2Email = "eve.wilson@example.com",
                Title = "Document Title",
                Message = "Please sign this document.",
                FileId = "file-123",
                DocumentId = "document-456",
                Status = "Pending",
                embedded_claim_url = "embedded-claim-url",
                IsDeleted = false,
                IsRefreshButtonClicked = "false",
                ManualSigning = false,
                CreatedById = 1,
                ModifiedById = null,
                CreatedOn = DateTime.Now,
                ModifiedOn = null,
                TransactionStatus = "Success",
                AttachmentFile = "attachment-file-url",
                TaxYear = 2023,
                Templatename = "Template Name",
                UploadPDFcontent1 = "PDF Content 1",
                UploadPDFdocument1 = new byte[] { 0x1, 0x2, 0x3 },
                access_token = "access-token",
                GetAbouts = new List<SigningDocumentFields.About>
                {
                    new SigningDocumentFields.About { IFrameUrl = "about-iframe-url-1" },
                    new SigningDocumentFields.About { IFrameUrl = "about-iframe-url-2" }
                },
                IFrameUrl = "main-iframe-url",
                document_hash = "document-hash",
                code = "code",
                state = "state",
                Iframestatus = "iframe-status",
                EditedBy = "Editor Name",
                UploadedDocument = "uploaded-document-url",
                PDFbyteArray = "PDF-byte-array",
                PreviewQuestions = "Some preview questions",
                PreviewPDFContent = "Preview PDF Content",
                LetterHtmlContent = "HTML Content",
                EngagementLetterName = "Engagement Letter 2023",
                StatusCode = System.Net.HttpStatusCode.OK
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);


        }


        [Fact]
        public void FlatteningModel_ValidModel_PassesValidation()
        {
            // Arrange
            string pdfContent = "Sample PDF content...";

            // Act
            var model = new FlatteningModel
            {
                PDFContent = pdfContent,
            };
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }


        [Fact]
        public void EngagementLetterDetails_ValidModel_PassesValidation()
        {
            // Arrange
            string code = "ABC123";
            string state = "Active";
            int engagementLetterId = 42;

            // Act
            var model = new EngagementLetterDetails
            {
                code = code,
                state = state,
                EngagementLetterId = engagementLetterId
            };
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void NewRootObject_ValidModel_PassesValidation()
        {
            // Arrange
            string documentHash = "ABC123";
            string requesterEmail = "requester@example.com";
            string customRequesterName = "Custom Requester";
            string customRequesterEmail = "custom@example.com";
            int isDraft = 0;
            int isTemplate = 1;
            int isCompleted = 0;
            int isArchived = 1;
            int isDeleted = 0;
            int isTrashed = 0;
            int isCancelled = 0;
            int isExpired = 0;
            int embedded = 1;
            int inPerson = 0;
            int embeddedSigningEnabled = 1;
            int flexibleSigning = 1;
            string permission = "Read";
            string templateId = "Template123";
            string title = "Document Title";
            string subject = "Document Subject";
            string message = "Document Message";
            int useSignerOrder = 1;
            int reminders = 3;
            int requireAllSigners = 1;
            string redirect = "https://redirect-url.com";
            string redirectDecline = "https://decline-url.com";
            string client = "Client Name";
            int created = (int)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
            string expires = "2023-12-31T23:59:59Z";

            var signers = new List<Signer>();
            var recipients = new List<Recipient>();
            var fields = new List<List<object>>();
            var log = new List<Log>();
            var meta = new Meta();
            string embeddedClaimUrl = "https://claim-url.com";
            string embeddedSigningUrl = "https://signing-url.com";

            // Act
            var model = new NewRoot
            {
                document_hash = documentHash,
                requester_email = requesterEmail,
                custom_requester_name = customRequesterName,
                custom_requester_email = customRequesterEmail,
                is_draft = isDraft,
                is_template = isTemplate,
                is_completed = isCompleted,
                is_archived = isArchived,
                is_deleted = isDeleted,
                is_trashed = isTrashed,
                is_cancelled = isCancelled,
                is_expired = isExpired,
                embedded = embedded,
                in_person = inPerson,
                embedded_signing_enabled = embeddedSigningEnabled,
                flexible_signing = flexibleSigning,
                permission = permission,
                template_id = templateId,
                title = title,
                subject = subject,
                message = message,
                use_signer_order = useSignerOrder,
                reminders = reminders,
                require_all_signers = requireAllSigners,
                redirect = redirect,
                redirect_decline = redirectDecline,
                client = client,
                created = created,
                expires = expires,
                signers = signers,
                recipients = recipients,
                fields = fields,
                log = log,
                meta = meta,
                embedded_claim_url = embeddedClaimUrl,
                embedded_signing_url = embeddedSigningUrl,
            };

            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void Signer_ValidModel_PassesValidation()
        {
            // Arrange
            int id = 1;
            string name = "John Doe";
            string email = "john.doe@example.com";
            string role = "Client";
            int order = 1;
            string pin = "1234";
            string message = "Please sign the document.";
            int signed = 0;
            object signedTimestamp = null;
            int required = 1;
            int deliverEmail = 1;
            string language = "en";
            int declined = 0;
            int removed = 0;
            int bounced = 0;
            int sent = 0;
            int viewed = 0;
            string status = "Pending";
            string embeddedSigningUrl = "https://example.com/signing-url";

            // Act
            var model = new Signer
            {
                id = id,
                name = name,
                email = email,
                role = role,
                order = order,
                pin = pin,
                message = message,
                signed = signed,
                signed_timestamp = signedTimestamp,
                required = required,
                deliver_email = deliverEmail,
                language = language,
                declined = declined,
                removed = removed,
                bounced = bounced,
                sent = sent,
                viewed = viewed,
                status = status,
                embedded_signing_url = embeddedSigningUrl
            };

            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void Recipient_ValidModel_PassesValidation()
        {
            // Arrange
            string name = "John Doe";
            string email = "john@example.com";
            string role = "Signer";
            string message = "Please sign the document.";
            int required = 1;
            string language = "English";

            // Act
            var model = new Recipient
            {
                name = name,
                email = email,
                role = role,
                message = message,
                required = required,
                language = language,
            };
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void LogEntry_ValidModel_PassesValidation()
        {
            // Arrange
            string logEvent = "DocumentSigned";
            object signer = new { Name = "John Doe", Email = "john@example.com" };
            int timestamp = 1635324800;

            // Act
            var model = new Log
            {
                @event = logEvent,
                signer = signer,
                timestamp = timestamp,
            };
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void CustomList_ValidModel_PassesValidation()
        {
            // Arrange
            string name = "Document Name";
            string file_id = "12345";
            int pages = 5;

            // Act
            var model = new CustomList
            {
                name = name,
                file_id = file_id,
                pages = pages,
            };
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void Meta_ValidModel_PassesValidation()
        {
            // Arrange
            string someKey = "key1";
            string anotherKey = "key2";

            // Act
            var model = new Meta
            {
                some_key = someKey,
                another_key = anotherKey,
            };
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
