﻿using Hangfire.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Hangfire.UnitTests.ModelsUnitTests.SigningModelUnitTests
{
    public class AdobePDFStatusResponseUnitTests
    {

        [Fact]
        public void AdobePDFStatusResponse_ValidModel_PassesValidation()
        {
            // Arrange
            string status = "Success";
            Asset asset = new Asset();

            // Act
            var model = new AdobePDFStatusResponse
            {
                status = status,
                asset = asset,
            };
            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
