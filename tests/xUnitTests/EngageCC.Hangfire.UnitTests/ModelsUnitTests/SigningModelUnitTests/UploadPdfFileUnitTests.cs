﻿using Hangfire.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Hangfire.UnitTests.ModelsUnitTests.SigningModelUnitTests
{
    public class UploadPdfFileUnitTests
    {

        [Fact]
        public void UploadPdfFile_ValidModel_PassesValidation()
        {
            // Arrange
            string pdfUrl = "https://example.com/sample.pdf";
            string fileName = "sample.pdf";

            // Act
            var model = new UploadPdfFile
            {
                PdfUrl = pdfUrl,
                FileName = fileName,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
