﻿using Hangfire.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Hangfire.UnitTests.ModelsUnitTests.SigningModelUnitTests
{
    public class AssetUnitTests
    {
        [Fact]
        public void Asset_ValidModel_PassesValidation()
        {
            // Arrange
            Metadata metadata = new Metadata();
            string downloadUri = "https://example.com/download";
            string assetID = "12345";

            // Act
            var model = new Asset
            {
                metadata = metadata,
                downloadUri = downloadUri,
                assetID = assetID,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
