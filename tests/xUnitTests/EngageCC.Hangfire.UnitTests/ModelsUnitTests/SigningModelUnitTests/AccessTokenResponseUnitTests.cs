﻿using Hangfire.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Hangfire.UnitTests.ModelsUnitTests.SigningModelUnitTests
{
    public class AccessTokenResponseUnitTests
    {
        [Fact]
        public void AccessTokenResponse_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new AccessTokenResponse
            {
                token_type = "Bearer",
                access_token = "exampleAccessToken",
                expires_in = 3600
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
