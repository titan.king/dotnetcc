﻿using Hangfire.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Hangfire.UnitTests.ModelsUnitTests.SigningModelUnitTests
{
    public class SigningInfoModelUnitTests
    {
        [Fact]
        public void SigningInfoModel_ValidModel_PassesValidation()
        {
            // Arrange
            DateTime now = DateTime.Now;

            // Act
            var model = new SigningInfoModel
            {
                SigningInfoId = 1,
                EngagementLetterId = 123,
                EngagementLetterName = "Engagement Letter 2023",
                SigningPartnerName = "John Smith",
                DelegatedName = "Alice Johnson",
                SPEmail = "john.smith@example.com",
                ContactPerson1Name = "Bob Davis",
                ContactPerson1Email = "bob.davis@example.com",
                Title = "Document Title",
                Message = "Please sign this document.",
                CreatedOn = now,
                ModifiedOn = now.AddHours(1),
                FileId = "file-123",
                DelegatedBy = "Charlie Brown",
                AgreementId = "agreement-456",
                DeletedOn = null, 
                PdfUrl = "pdf-url",
                DocumentStatusId = 2,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
