﻿using Hangfire.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Hangfire.UnitTests.ModelsUnitTests.SigningModelUnitTests
{
    public class BulkLetterErrorLogVMUnitTests
    {
        [Fact]
        public void BulkLetterErrorLogVM_ValidModel_PassesValidation()
        {
            // Arrange
            int engagementLetterId = 1;
            int batchId = 123;
            int? clientId = 456;
            string requestLog = "Sample request log";
            string errorMessage = "Sample error message";
            DateTime createdOn = DateTime.Now;
            DateTime? modifiedOn = null;

            // Act
            var model = new BulkLetterErrorLogVM
            {
                EngagementLetterId = engagementLetterId,
                BatchId = batchId,
                ClientId = clientId,
                RequestLog = requestLog,
                ErrorMessage = errorMessage,
                CreatedOn = createdOn,
                ModifiedOn = modifiedOn,
            };

            var validationContext = new ValidationContext(model);
        var validationResults = new List<ValidationResult>();
        var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

        // Assert
        Assert.True(isValid);
        }
}
}
