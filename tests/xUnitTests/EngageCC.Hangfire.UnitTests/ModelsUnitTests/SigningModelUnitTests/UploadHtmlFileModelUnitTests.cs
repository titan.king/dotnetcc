﻿using Hangfire.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Hangfire.UnitTests.ModelsUnitTests.SigningModelUnitTests
{
    public class UploadHtmlFileModelUnitTests
    {

        [Fact]
        public void UploadHtmlFile_ValidModel_PassesValidation()
        {
            // Arrange
            string htmlData = "<html><body><h1>Hello, World!</h1></body></html>";
            string fileName = "test.html";

            // Act
            var model = new UploadHtmlFile
            {
                HtmlData = htmlData,
                FileName = fileName,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
