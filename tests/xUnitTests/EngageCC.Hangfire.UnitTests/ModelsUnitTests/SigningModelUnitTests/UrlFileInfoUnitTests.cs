﻿using Hangfire.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Hangfire.UnitTests.ModelsUnitTests.SigningModelUnitTests
{
    public class UrlFileInfoUnitTests
    {
        [Fact]
        public void CanCreateUrlFileInfo()
        {
            // Arrange
            string mimeType = "application/pdf";
            string name = "example.pdf";
            string url = "https://example.com/files/example.pdf";

            // Act
            var model = new UrlFileInfo
            {
                mimeType = mimeType,
                name = name,
                url = url,
            };
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
