﻿using Hangfire.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Hangfire.UnitTests.ModelsUnitTests.SigningModelUnitTests
{
    public class PdfConversionUnitTests
    {
        [Fact]
        public void PdfConversion_ValidModel_PassesValidation()
        {
            // Arrange
            var pageLayout = new PageLayout
            {
                pageWidth = "8.5in",
                pageHeight = "11in"
            };

            var model = new PdfConversion
            {
                inputUrl = "https://example.com/document.pdf",
                json = "{\"key\":\"value\"}",
                includeHeaderFooter = true,
                pageLayout = pageLayout
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
