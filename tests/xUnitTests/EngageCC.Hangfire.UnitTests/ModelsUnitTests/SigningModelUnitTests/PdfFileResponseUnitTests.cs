﻿using Hangfire.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Hangfire.UnitTests.ModelsUnitTests.SigningModelUnitTests
{
    public class PdfFileResponseUnitTests
    {

        [Fact]
        public void PdfFileResponse_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new PdfFileResponse
            {
                PdflUrl = "https://example.com/sample.pdf",
                StatusCode = HttpStatusCode.OK,
                Message = "PDF file retrieved successfully"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
