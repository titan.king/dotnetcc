﻿using Hangfire.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Hangfire.UnitTests.ModelsUnitTests.SigningModelUnitTests
{
    public class ResponseModelUnitTests
    {

        [Fact]
        public void ResponseModel_ConstructorWithStatusCodeAndErrorMessage_InitializesStatusCodeAndErrorMessage()
        {
            // Arrange
            var expectedStatusCode = HttpStatusCode.NotFound;
            var expectedErrorMessage = "Sample error message";

            // Act
            var response = new ResponseModel(expectedStatusCode, expectedErrorMessage);

            // Assert
            Assert.Equal(expectedStatusCode, response.StatusCode);
            Assert.False(response.Status);
            Assert.Null(response.Data);
            Assert.Equal(expectedErrorMessage, response.ErrorMessage);
            Assert.Equal(string.Empty, response.PdfUrl);
        }

        [Fact]
        public void ResponseModel_PdfUrl_DefaultValueIsStringEmpty()
        {
            // Arrange

            // Act
            var response = new ResponseModel();

            // Assert
            Assert.Equal(string.Empty, response.PdfUrl);
        }
    }
}
