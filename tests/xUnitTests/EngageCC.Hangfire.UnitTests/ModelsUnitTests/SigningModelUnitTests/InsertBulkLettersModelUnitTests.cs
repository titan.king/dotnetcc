﻿using Hangfire.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Hangfire.UnitTests.ModelsUnitTests.SigningModelUnitTests
{
    public class InsertBulkLettersModelUnitTests
    {
        [Fact]
        public void InsertBulkLettersModel_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new InsertBulkLettersModel
            {
                DocumentStatusId = 1,
                EngageTypeId = 2,
                DepartmentId = 3,
                TaxYear = "2023",
                OfficeId = 4,
                TemplateVersion = 5,
                EngagementLetterName = "Test Letter",
                TemplateId = 6
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
