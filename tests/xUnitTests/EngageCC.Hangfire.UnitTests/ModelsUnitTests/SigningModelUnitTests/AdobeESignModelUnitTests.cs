﻿using Hangfire.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileInfo = Hangfire.Models.Signing.FileInfo;

namespace EngageCC.Hangfire.UnitTests.ModelsUnitTests.SigningModelUnitTests
{
    public class AdobeESignModelUnitTests
    {

        [Fact]
        public void AdobeESignModel_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new AdobeESignModel
            {
                fileInfos = new List<FileInfo>(),
                name = "Valid Name",
                participantSetsInfo = new List<ParticipantSetsInfo>(),
                signatureType = "Valid Signature Type",
                externalId = new ExternalId { id = "Valid Id" },
                state = "Valid State",
                message = "Valid Message",
                expirationTime = "2023-12-31T23:59:59Z"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
