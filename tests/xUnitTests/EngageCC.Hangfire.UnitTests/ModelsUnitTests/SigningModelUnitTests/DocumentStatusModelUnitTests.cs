﻿using Hangfire.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Hangfire.UnitTests.ModelsUnitTests.SigningModelUnitTests
{
    public class DocumentStatusModelUnitTests
    {

        [Fact]
        public void DocumentStatusModel_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new DocumentStatusModel
            {
                DocumentStatusId = 1,
                DocumentStatusName = "Pending",
                EngagementLetterId = 123
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
