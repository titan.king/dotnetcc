﻿using BulkSigning.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.ContextUnitTests
{
    public class BSDBContextUnitTests
    {
        private readonly string _databaseName = "Test_BSDB_" + Guid.NewGuid().ToString();

        [Fact]
        public void BSDBContext_WhenInitialized_ShouldCreateBatchRequestDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new BSDBContext(options))
            {
                // Assert
                Assert.NotNull(context.BatchRequest);
            }
        }

        [Fact]
        public void BSDBContext_WhenInitialized_ShouldCreateEngagementLetterDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new BSDBContext(options))
            {
                // Assert
                Assert.NotNull(context.EngagementLetter);
            }
        }

        [Fact]
        public void BSDBContext_WhenInitialized_ShouldCreateBulkLettersDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new BSDBContext(options))
            {
                // Assert
                Assert.NotNull(context.BulkLetters);
            }
        }


        [Fact]
        public void BSDBContext_WhenInitialized_ShouldCreateMasterTemplateDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new BSDBContext(options))
            {
                // Assert
                Assert.NotNull(context.MasterTemplate);
            }
        }

        [Fact]
        public void BSDBContext_WhenInitialized_ShouldCreateTemplateAttachmentsDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new BSDBContext(options))
            {
                // Assert
                Assert.NotNull(context.TemplateAttachments);
            }
        }

        [Fact]
        public void BSDBContext_WhenInitialized_ShouldCreateLetterFieldValuesDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new BSDBContext(options))
            {
                // Assert
                Assert.NotNull(context.LetterFieldValues);
            }
        }

        [Fact]
        public void BSDBContext_WhenInitialized_ShouldCreateSigningInfoDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new BSDBContext(options))
            {
                // Assert
                Assert.NotNull(context.SigningInfo);
            }
        }

        [Fact]
        public void BSDBContext_WhenInitialized_ShouldCreateStatusDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new BSDBContext(options))
            {
                // Assert
                Assert.NotNull(context.Status);
            }
        }

        [Fact]
        public void BSDBContext_WhenInitialized_ShouldCreateTemplateDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new BSDBContext(options))
            {
                // Assert
                Assert.NotNull(context.Template);
            }
        }


        [Fact]
        public void BSDBContext_WhenInitialized_ShouldCreateBulkLetterAttachmentsDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new BSDBContext(options))
            {
                // Assert
                Assert.NotNull(context.BulkLetterAttachments);
            }
        }

    }
}
