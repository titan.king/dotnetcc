﻿using AutoMapper;
using BulkSigning.Data.Context;
using BulkSigning.Data.DataModel;
using BulkSigning.Data.Repositories;
using BulkSigning.Models.BulkSigning;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.RepositoriesUnitTests
{
    public class BulkSigningRepositoryUnitTests
    {
       
        [Fact]
        public async Task GetEngagementLettersList_ShouldReturnMatchingEngagementLetterAndMarkAsEnqueued()
        {
            // Arrange
            var engagementLetterId = 1;
            var dbContextOptions = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_BSDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new BSDBContext(dbContextOptions))
            {
                var repository = new BulkSigningRepository(context, dbContextOptions, null, null);
                context.EngagementLetter.Add(new EngagementLetter
                {
                    EngagementLetterId = engagementLetterId,
                    IsProcess = false,
                    IsEnqueue = false,
                    IsDeleted = false
                });
                context.SaveChanges();
                var engagementLetterViewModel = new EngagementLetterViewModel { EngagementLetterId = engagementLetterId };

                // Act
                var result = await repository.GetEngagementLettersList(engagementLetterViewModel);

                // Assert
                Assert.NotNull(result);
                Assert.False(result.IsEnqueue);
                Assert.Equal(engagementLetterId, result.EngagementLetterId);
            }
        }

        [Fact]
        public async Task GetEngagementLettersList_WhenNoMatchingEngagementLetterFound_ShouldReturnNull()
        {
            // Arrange
            var engagementLetterId = 1;
            var dbContextOptions = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_BSDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new BSDBContext(dbContextOptions))
            {
                var repository = new BulkSigningRepository(context, dbContextOptions, null, null);
                var engagementLetterViewModel = new EngagementLetterViewModel { EngagementLetterId = engagementLetterId };

                // Act
                var result = await repository.GetEngagementLettersList(engagementLetterViewModel);

                // Assert
                Assert.Null(result);
            }
        }

        [Fact]
        public async Task GetMasterTemplate_WhenValidTemplateIdProvided_ShouldReturnMasterTemplateText()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_BSDB")
                .Options;

            using (var context = new BSDBContext(dbContextOptions))
            {
                context.MasterTemplate.AddRange(new List<MasterTemplate>
                {
                    new MasterTemplate
                    {
                        TemplateId = 1,
                        TemplateVersionId = 1,
                        Template = "This is the first version of the template."
                    },
                    new MasterTemplate
                    {
                        TemplateId = 1,
                        TemplateVersionId = 2,
                        Template = "This is the second version of the template."
                    },
                    new MasterTemplate
                    {
                        TemplateId = 2,
                        TemplateVersionId = 1,
                        Template = "Another template."
                    },
                });
                context.SaveChanges();

                var repository = new BulkSigningRepository(context, dbContextOptions, null, null);

                // Act
                var result = await repository.GetMasterTemplate(1);

                // Assert
                Assert.NotNull(result);
                Assert.Equal("This is the second version of the template.", result);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetMasterTemplate_WhenNoMasterTemplateExists_ShouldReturnEmptyString()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_BSDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new BSDBContext(dbContextOptions))
            {
                var repository = new BulkSigningRepository(context, dbContextOptions, null, null);

                var templateId = 1;

                // Act
                var result = await repository.GetMasterTemplate(templateId);

                // Assert
                Assert.NotNull(result);
                Assert.Equal(string.Empty, result);
            }
        }

        [Fact]
        public async Task GetTemplateAttachments_WhenValidTemplateIdAndBatchIdProvided_ShouldReturnAttachmentsURL()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_BSDB")
                .Options;

            using (var context = new BSDBContext(dbContextOptions))
            {
                context.BulkLetterAttachments.AddRange(new List<BulkLetterAttachments>
                {
                    new BulkLetterAttachments
                    {
                        TemplateId = 1,
                        BatchId = 1,
                        VersionNumber = 1,
                        AttachmentsURL = "AttachmentURL-1"
                    },
                    new BulkLetterAttachments
                    {
                        TemplateId = 1,
                        BatchId = 1,
                        VersionNumber = 2,
                        AttachmentsURL = "AttachmentURL-2"
                    },
                    new BulkLetterAttachments
                    {
                        TemplateId = 2,
                        BatchId = 2,
                        VersionNumber = 1,
                        AttachmentsURL = "AttachmentURL-3"
                    },
                });

                context.Template.AddRange(new List<Template>
                {
                    new Template
                    {
                        TemplateId = 1,
                        AttachmentURL = "TemplateAttachmentURL-1"
                    },
                    new Template
                    {
                        TemplateId = 2,
                        AttachmentURL = "TemplateAttachmentURL-2"
                    },
                });

                context.SaveChanges();

                var repository = new BulkSigningRepository(context, dbContextOptions, null, null);

                // Act
                var result = await repository.GetTemplateAttachments(1, 1);

                // Assert
                Assert.NotNull(result);
                Assert.Equal("AttachmentURL-2", result);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetTemplateAttachments_WhenNoAttachmentsExist_ShouldReturnTemplateAttachmentURL()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_BSDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new BSDBContext(dbContextOptions))
            {
                var repository = new BulkSigningRepository(context, dbContextOptions, null, null);

                var templateId = 1;
                var batchId = 2;

                // Act
                var result = await repository.GetTemplateAttachments(templateId, batchId);

                // Assert
                Assert.NotNull(result);
                Assert.Equal(string.Empty, result);
            }
        }

        [Fact]
        public async Task GetLetterFieldValues_WhenValidBulkLetterIdProvided_ShouldReturnLatestRecords()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_BSDB")
                .Options;

            using (var context = new BSDBContext(dbContextOptions))
            {
                context.LetterFieldValues.AddRange(new List<LetterFieldValues>
                {
                    new LetterFieldValues
                    {
                        BulkLettersId = 1,
                        LetterVersion = 1,
                    },
                    new LetterFieldValues
                    {
                        BulkLettersId = 1,
                        LetterVersion = 2,
                    },
                    new LetterFieldValues
                    {
                        BulkLettersId = 2,
                        LetterVersion = 1,
                    },
                });

                context.SaveChanges();

                var repository = new BulkSigningRepository(context, dbContextOptions, null, null);

                // Act
                var result = await repository.GetLetterFieldValues(1);

                // Assert
                Assert.NotNull(result);
                Assert.Equal(1, result.Count);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetLetterFieldValues_WhenInvalidBulkLetterIdProvided_ShouldReturnEmptyList()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_BSDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new BSDBContext(dbContextOptions))
            {
                var repository = new BulkSigningRepository(context, dbContextOptions, null, null);

                var bulkLetterId = 1;

                // Act
                var result = await repository.GetLetterFieldValues(bulkLetterId);

                // Assert
                Assert.NotNull(result);
                Assert.Empty(result);
            }
        }

        [Fact]
        public async Task SaveSigningInfos_WhenValidSigningInfosProvided_ShouldSaveDataAndReturnTrue()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_BSDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new BSDBContext(dbContextOptions))
            {
                var repository = new BulkSigningRepository(context, dbContextOptions, null, null);

                var signingInfos = new List<SigningInfo>
                {
                    new SigningInfo
                    {
                         EngagementLetterId = 1,
                        SigningPartnerName = "John Doe",
                        DelegatedName = "Jane Smith",
                        SPEmail = "john.doe@example.com",
                        ContactPerson1Name = "Contact 1",
                        ContactPerson1Email = "contact1@example.com",
                        ContactPerson2Name = "Contact 2",
                        ContactPerson2Email = "contact2@example.com",
                        Title = "Signing Title",
                        Message = "Hello, please sign this document.",
                        IsDeleted = false,
                        CreatedBy = "User1",
                        CreatedOn = DateTime.Now,
                        ModifiedOn = null,
                        ManualSigning = false,
                        FileId = "File123",
                        DelegatedBy = "Manager1",
                        AgreementId = "Agreement123",
                        ModifiedBy = null,
                        DeletedOn = null,
                        DeletedBy = null,
                        PdfUrl = "https://example.com/document.pdf",
                        EngagementLetter = new EngagementLetter
                        {
                            EngagementLetterId =1
                        },
                        SigningInfoId = 1,
                    },
                };

                bool result = false;
                foreach (var sign in signingInfos)
                {
                    // Act
                    result = await repository.SaveSigningInfos(sign);
                }


                // Assert
                Assert.True(result);
                Assert.Equal(signingInfos.Count, context.SigningInfo.Count());
                var savedInfos = context.SigningInfo.ToList();
                foreach (var signingInfo in signingInfos)
                {
                    Assert.Contains(savedInfos, s =>
                        s.EngagementLetterId == signingInfo.EngagementLetterId &&
                        s.SigningPartnerName == signingInfo.SigningPartnerName
                    );
                }
                context.Dispose();
            }
        }

        [Fact]
        public async Task SaveSigningInfos_WhenNullSigningInfoProvided_ShouldNotSaveDataAndReturnFalse()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_BSDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new BSDBContext(dbContextOptions))
            {
                var repository = new BulkSigningRepository(context, dbContextOptions, null, null);

                // Act
                var result = await repository.SaveSigningInfos(null);

                // Assert
                Assert.False(result);
                Assert.Equal(0, context.SigningInfo.Count());
            }
        }

        [Fact]
        public async Task UpdateEngagementLetter_WithValidEngagementLetter_ReturnsTrue()
        {
            // Arrange
            var engagementLetter = new EngagementLetter
            {
                EngagementLetterId = 1,
                IsEsigning = true,
                IsProcess = true,
                DocumentStatusId = 2

            };

            var dbContextOptions = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            using (var dbContext = new BSDBContext(dbContextOptions))
            {
                dbContext.EngagementLetter.Add(engagementLetter);
                dbContext.SaveChanges();
            }

            using (var dbContext = new BSDBContext(dbContextOptions))
            {
                var repository = new BulkSigningRepository(dbContext, dbContextOptions, null, null);

                // Act
                var result = await repository.UpdateEngagementLetter(engagementLetter, true);

                // Assert
                Assert.True(result);
                Assert.True(engagementLetter.IsProcess);
                Assert.False(engagementLetter.IsEnqueue);
                Assert.Equal(2, engagementLetter.DocumentStatusId);
            }
        }

        [Fact]
        public async Task UpdateEngagementLetter_WithInvalidEngagementLetter_ReturnsFalse()
        {
            // Arrange
            var engagementLetter = new EngagementLetter
            {
                EngagementLetterId = 999,
                IsEsigning = false
            };

            var dbContextOptions = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            using (var dbContext = new BSDBContext(dbContextOptions))
            {
                var repository = new BulkSigningRepository(dbContext, dbContextOptions, null, null);

                // Act
                var result = await repository.UpdateEngagementLetter(engagementLetter, true);

                // Assert
                Assert.False(result);
            }
        }

        [Fact]
        public async Task GetAgreementId_WhenMatchingRecordExists_ShouldReturnSigningInfoModel()
        {
            // Arrange
            var engagementLetterId = 1;
            var dbContextOptions = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_BSDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new BSDBContext(dbContextOptions))
            {
                var mapperConfig = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<SigningInfo, SigningInfoModel>();
                });
                var mapper = new Mapper(mapperConfig);

                var repository = new BulkSigningRepository(context, dbContextOptions, mapper, null);

                context.EngagementLetter.Add(new EngagementLetter
                {
                    EngagementLetterId = engagementLetterId,
                    EngagementLetterName = "Sample Engagement Letter"
                });

                context.SigningInfo.Add(new SigningInfo
                {
                    EngagementLetterId = engagementLetterId,
                    AgreementId = "Agreement123",
                    ManualSigning = false
                });

                context.SaveChanges();

                var signingInfos = new SigningInfo { EngagementLetterId = engagementLetterId };

                // Act
                var result = await repository.GetAgreementId(signingInfos);

                // Assert
                Assert.NotNull(result);
                Assert.Equal(engagementLetterId, result.EngagementLetterId);
                Assert.Equal("Agreement123", result.AgreementId);
                Assert.Equal("Sample Engagement Letter", result.EngagementLetterName);
            }
        }

        [Fact]
        public async Task GetAgreementId_WhenNoMatchingRecordFound_ShouldReturnEmptySigningInfoModel()
        {
            // Arrange
            var engagementLetterId = 1;
            var dbContextOptions = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_BSDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new BSDBContext(dbContextOptions))
            {
                var mapperConfig = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<SigningInfo, SigningInfoModel>();
                });
                var mapper = new Mapper(mapperConfig);

                var repository = new BulkSigningRepository(context, dbContextOptions, mapper, null);

                var signingInfos = new SigningInfo { EngagementLetterId = engagementLetterId };

                // Act
                var result = await repository.GetAgreementId(signingInfos);

                // Assert
                Assert.NotNull(result);
                Assert.NotEqual(engagementLetterId, result.EngagementLetterId);
                Assert.Equal(string.Empty, result.AgreementId);
                Assert.Equal(string.Empty, result.EngagementLetterName);
            }
        }

        [Fact]
        public async Task GetHistoryLogVersion_WhenMatchingRecordExists_ShouldReturnHistoryLogModel()
        {
            // Arrange
            var engagementLetterId = 1;
            var dbContextOptions = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_BSDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new BSDBContext(dbContextOptions))
            {
                var repository = new BulkSigningRepository(context, dbContextOptions, null, null);

                context.HistoryLog.Add(new HistoryLog
                {
                    EngagementLetterId = engagementLetterId,
                    Version = 1,
                });

                context.SaveChanges();

                var engagementLetters = new EngagementLetter { EngagementLetterId = engagementLetterId, DocumentStatusId = 1 };

                // Act
                var result = await repository.GetHistoryLogVersion(engagementLetters);

                // Assert
                Assert.NotNull(result);
                Assert.Equal(engagementLetterId, result.EngagementLetterId);
                Assert.Equal(1, result.Version);
            }
        }

        [Fact]
        public async Task GetHistoryLogVersion_WhenNoMatchingRecordFound_ShouldReturnEmptyHistoryLogModel()
        {
            // Arrange
            var engagementLetterId = 1;
            var dbContextOptions = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_BSDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new BSDBContext(dbContextOptions))
            {
                var repository = new BulkSigningRepository(context, dbContextOptions, null, null);
                var engagementLetters = new EngagementLetter { EngagementLetterId = engagementLetterId };

                // Act
                var result = await repository.GetHistoryLogVersion(engagementLetters);

                // Assert
                Assert.NotNull(result);
                Assert.NotEqual(engagementLetterId, result.EngagementLetterId);
                Assert.Null(result.Version);
            }
        }

        [Fact]
        public async Task UpdateHistoryLog_WhenHistoryLogModelIsProvided_ShouldReturnTrue()
        {
            // Arrange
            var engagementLetterId = 1;
            var historyLogModel = new HistoryLogModel
            {
                EngagementLetterId = engagementLetterId,
                Version = 2,
            };

            var dbContextOptions = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_BSDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new BSDBContext(dbContextOptions))
            {

                var mapperConfig = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<HistoryLogModel, HistoryLog>();
                });
                var mapper = new Mapper(mapperConfig);

                var repository = new BulkSigningRepository(context, dbContextOptions, mapper, null);

                // Act
                var result = await repository.UpdateHistoryLog(historyLogModel);

                // Assert
                Assert.True(result);
                var savedRecord = await context.HistoryLog.FirstOrDefaultAsync();
                Assert.NotNull(savedRecord);
            }
        }

        [Fact]
        public async Task UpdateHistoryLog_WhenHistoryLogModelIsNull_ShouldReturnFalse()
        {
            // Arrange
            HistoryLogModel historyLogModel = null;

            var dbContextOptions = new DbContextOptionsBuilder<BSDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_BSDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new BSDBContext(dbContextOptions))
            {
                var repository = new BulkSigningRepository(context, dbContextOptions, null, null);

                // Act
                var result = await repository.UpdateHistoryLog(historyLogModel);

                // Assert
                Assert.False(result);
                var savedRecord = await context.HistoryLog.FirstOrDefaultAsync();
                Assert.Null(savedRecord);
            }
        }
    }
}
