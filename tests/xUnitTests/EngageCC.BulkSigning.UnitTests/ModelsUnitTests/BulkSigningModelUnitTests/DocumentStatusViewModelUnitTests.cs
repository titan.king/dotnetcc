﻿using BulkSigning.Data.DataModel;
using BulkSigning.Models.BulkSigning;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.ModelsUnitTests.BulkSigningModelUnitTests
{
    public class DocumentStatusViewModelUnitTests
    {
        [Fact]
        public void DocumentStatus_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new DocumentStatusModel
            {
                DocumentStatusId = 1,
                DocumentStatusName = "Draft",
                EngagementLetterId = 123,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
