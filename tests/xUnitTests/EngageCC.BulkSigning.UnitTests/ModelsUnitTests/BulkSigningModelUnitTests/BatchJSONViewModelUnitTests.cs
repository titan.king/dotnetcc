﻿using BulkSigning.Models.BulkSigning;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.ModelsUnitTests.BulkSigningModelUnitTests
{
    public class BatchJSONViewModelUnitTests
    {

        [Fact]
        public void BatchJSONModel_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new BatchJSONModel
            {
                ClientId = 1,
                BulkLettersId = 2,
                BatchId = 3,
                DocumentStatusId = 4,
                EngageTypeId = 5,
                DepartmentId = 6,
                TaxYear = "2023",
                OfficeId = 7,
                TemplateVersion = 8
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
