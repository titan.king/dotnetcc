﻿using BulkSigning.Models.BulkSigning;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.ModelsUnitTests.BulkSigningModelUnitTests
{
    public class PdfConversionViewModelUnitTests
    {
        [Fact]
        public void PdfConversion_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new PdfConversion
            {
                inputUrl = "https://example.com/document.pdf",
                json = "{ 'key': 'value' }",
                includeHeaderFooter = true,
                pageLayout = new PageLayout
                {
                    pageWidth = "8.5in",
                    pageHeight = "11in"
                }
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void PdfConversionRequest_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new PdfConversionRequest
            {
                pdfConversion = new PdfConversion
                {
                    inputUrl = "https://example.com/document.pdf",
                    json = "{ 'key': 'value' }",
                    includeHeaderFooter = true,
                    pageLayout = new PageLayout
                    {
                        pageWidth = "8.5in",
                        pageHeight = "11in"
                    }
                }
            };


            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void AdobePDFStatusResponse_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new AdobePDFStatusResponse
            {
                status = "Valid",
                asset = new Asset
                {
                    assetID = "1",
                    downloadUri = "sample",
                    metadata = new Metadata
                    {
                        size = 100,
                        type = "text/html",
                    }
                }
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void Metadata_ValidModel_PassesValidation()
        {
            //Arrange
            var model = new Metadata
            {
                size = 100,
                type = "Valid Type"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void Asset_ValidModel_PassesValidation()
        {
            //Arrange
            var model = new Asset
            {
                metadata = new Metadata
                {
                    type = "Document",
                    size = 1024
                },
                assetID = "1",
                downloadUri = "https://example.com/download"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void AccessTokenResponse_ValidModel_PassesValidation()
        {
            //Arrange
            var model = new AccessTokenResponse
            {
                access_token = "sample_token",
                expires_in = 3600,
                token_type = "Bearer",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
