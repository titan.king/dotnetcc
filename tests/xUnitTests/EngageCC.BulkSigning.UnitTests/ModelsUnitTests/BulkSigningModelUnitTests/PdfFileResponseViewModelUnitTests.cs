﻿using BulkSigning.Models.BulkSigning;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.ModelsUnitTests.BulkSigningModelUnitTests
{
    public class PdfFileResponseViewModelUnitTests
    {
        [Fact]
        public void PdfFileResponse_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new PdfFileResponse
            {
                PdflUrl = "https://example.com/pdf.pdf",
                StatusCode = System.Net.HttpStatusCode.OK,
                Message = "Valid PDF File Response",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
