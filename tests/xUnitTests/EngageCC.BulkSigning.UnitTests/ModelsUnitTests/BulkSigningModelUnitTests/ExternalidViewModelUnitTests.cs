﻿using BulkSigning.Models.BulkSigning;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.ModelsUnitTests.BulkSigningModelUnitTests
{
    public class ExternalidViewModelUnitTests
    {
        [Fact]
        public void Externalid_ValidModel_PassesValidation()
        {
            // Arrange
            var externalId = new Externalid
            {
                id = "Valid Id"
            };

            // Act
            var validationContext = new ValidationContext(externalId);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(externalId, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void Externalid_InvalidModel_FailsValidation()
        {
            // Arrange
            var externalId = new Externalid
            {
                id = null
            };

            // Act
            var validationContext = new ValidationContext(externalId);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(externalId, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
