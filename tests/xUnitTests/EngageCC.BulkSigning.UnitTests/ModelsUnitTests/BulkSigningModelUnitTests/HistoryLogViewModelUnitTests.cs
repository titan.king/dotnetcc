﻿using BulkSigning.Models.BulkSigning;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.ModelsUnitTests.BulkSigningModelUnitTests
{
    public class HistoryLogViewModelUnitTests
    {

        [Fact]
        public void HistoryLog_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new HistoryLogModel
            {
                HistoryLogId = 1,
                EngagementLetterId = 123,
                EngagementLetterName = "EngagementLetter123",
                BatchId = 456,
                Status = "Completed",
                EditedBy = "Editor456",
                Version = 2,
                LastModified = DateTime.Now,
                Downloaded = DateTime.Now.AddDays(1),
                Delegated = true,
                ClientEmailId = "client@example.com",
                ReasonforDecline = "Not ready to sign",
                DeclineTimestamp = DateTime.Now.AddDays(2),
                PDFUrl = "https://example.com/document.pdf",
                IncrementedVersion = 3,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

    }
}
