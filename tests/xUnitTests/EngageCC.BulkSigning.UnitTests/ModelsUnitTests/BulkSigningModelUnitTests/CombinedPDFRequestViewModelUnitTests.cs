﻿using BulkSigning.Models.BulkSigning;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.ModelsUnitTests.BulkSigningModelUnitTests
{
    public class CombinedPDFRequestViewModelUnitTests
    {
        [Fact]
        public void CombinedPDFRequest_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new CombinedPDFRequest
            {
                assets = new List<Assete>
                {
                    new Assete { assetID = "Asset1" },
                    new Assete { assetID = "Asset2" }
                }
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
