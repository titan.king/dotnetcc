﻿using BulkSigning.Models.BulkSigning;
using Microsoft.AspNetCore.Http.HttpResults;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.ModelsUnitTests.BulkSigningModelUnitTests
{
    public class SigningInfoViewModelUnitTests
    {

        [Fact]
        public void SigningInfo_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new SigningInfoModel
            {
                SigningInfoId = 1,
                EngagementLetterId = 2,
                EngagementLetterName = "Engagement Letter 1",
                SigningPartnerName = "John Doe",
                DelegatedName = "Delegated Person",
                SPEmail = "sp@example.com",
                ContactPerson1Name = "Contact 1",
                ContactPerson1Email = "contact1@example.com",
                ContactPerson2Name = "Contact 2",
                ContactPerson2Email = "contact2@example.com",
                Title = "Document Title",
                Message = "Hello, please sign this document.",
                IsDeleted = false,
                CreatedBy = "Creator",
                CreatedOn = DateTime.Now,
                ModifiedOn = null,
                ManualSigning = true,
                FileId = "File123",
                DelegatedBy = "Delegator",
                AgreementId = "Agreement123",
                ModifiedBy = "Modifier",
                DeletedOn = DateTime.Now,
                DeletedBy = "Deleter",
                PdfUrl = "https://example.com/document.pdf",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

    }
}
