﻿using BulkSigning.Models.BulkSigning;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.ModelsUnitTests.BulkSigningModelUnitTests
{
    public class EngagementLetterListViewModelUnitTests
    {
        [Fact]
        public void EngagementLetterList_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new EngagementLetterList
            {
                EngagementLetterId = 1,
                BatchId = 2,
                EngagementLetterName = "Valid Engagement Letter",
                OfficeId = 3,
                OfficeName = "Valid Office",
                YearId = 4,
                TaxYear = "2023",
                TemplateId = 5,
                TemplateName = "Valid Template",
                TemplateVersion = 6,
                ClientId = 7,
                ClientName = "Valid Client",
                PartnerId = 8,
                PartnerName = "Valid Partner",
                AdminId = 9,
                AdminName = "Valid Admin",
                DocumentStatusId = 10,
                DocumentDescription = "Valid Description",
                DepartmentId = 11,
                DepartmentName = "Valid Department",
                EngageTypeId = 12,
                EngageTypeName = "Valid Engage Type",
                CreatedOn = DateTime.Now,
                CreatedBy = "ValidUser",
                ModifiedOn = null,
                IsDeleted = false,
                DeletedOn = null,
                DeletedBy = "DeletedUser",
                ModifiedBy = "ModifiedUser",
                BulkLettersId = 13,
                IsProcess = true,
                IsEnqueue = false,
                Is7216Available = true,
                ClientSignatureCount = 14,
                IsNewClient = true,
                SpouseFirstName = "Valid Spouse",
                SpouseEmailId = "spouse@example.com",
                SignatoryEmailId = "signatory@example.com",
                SignatoryFirstName = "Valid Signatory"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
