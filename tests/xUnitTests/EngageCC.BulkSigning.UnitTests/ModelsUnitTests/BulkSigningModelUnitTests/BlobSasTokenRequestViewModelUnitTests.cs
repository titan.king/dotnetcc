﻿using BulkSigning.Models.BulkSigning;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.ModelsUnitTests.BulkSigningModelUnitTests
{
    public class BlobSasTokenRequestViewModelUnitTests
    {
        [Fact]
        public void BlobSasTokenRequest_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new BlobSasTokenRequest
            {
                AccountKey = "ValidAccountKey",
                AccountName = "ValidAccountName",
                ConnectionString = "ValidConnectionString",
                ContainerName = "ValidContainerName",
                FileName = "ValidFileName"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
