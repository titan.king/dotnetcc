﻿using BulkSigning.Models.BulkSigning;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.ModelsUnitTests.BulkSigningModelUnitTests
{
    public class ParticipantsetsinfoViewModelUnitTests
    {
        [Fact]
        public void Participantsetsinfo_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new Participantsetsinfo
            {
                id = "ValidId",
                memberInfos = new Memberinfo[] { new Memberinfo {  email = "example@example.com",
                name = "John Doe",
                id = "12345",
                securityOption = new Securityoption
                {
                    authenticationMethod = "Test"
                } } },
                role = "ValidRole",
                order = 1
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
