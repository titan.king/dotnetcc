﻿using BulkSigning.Models.BulkSigning;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using static BulkSigning.Models.BulkSigning.SigningDocumentFields;
using File = BulkSigning.Models.BulkSigning.SigningDocumentFields.File;

namespace EngageCC.BulkSigning.UnitTests.ModelsUnitTests.BulkSigningModelUnitTests
{
    public class SigningDocumentFieldsViewModelUnitTests
    {
        [Fact]
        public void SigningDocumentFields_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new SigningDocumentFields
            {
                EngagementLetterId = 1,
                SigningPartnerName = "John Doe",
                SigningPartnerSignatureImage = "SignatureImageBase64",
                DelegatedName = "Delegated Person",
                SPEmail = "sp@example.com",
                DelegatedEmail = "delegated@example.com",
                ContactPerson1Name = "Contact 1",
                ContactPerson1Email = "contact1@example.com",
                ContactPerson2Name = "Contact 2",
                ContactPerson2Email = "contact2@example.com",
                Title = "Document Title",
                Message = "Hello, please sign this document.",
                FileId = "File123",
                DocumentId = "Doc456",
                Status = "Pending",
                embedded_claim_url = "https://example.com/claim",
                IsDeleted = false,
                IsRefreshButtonClicked = "No",
                ManualSigning = true,
                CreatedById = 123,
                ModifiedById = null,
                CreatedOn = DateTime.Now,
                ModifiedOn = null,
                TransactionStatus = "Success",
                AttachmentFile = "AttachmentFileName",
                TaxYear = 2023,
                Templatename = "Template123",
                UploadPDFcontent1 = "PDF Content 1",
                UploadPDFdocument1 = Encoding.UTF8.GetBytes("PDF Document Content"),
                access_token = "AccessToken123",
                GetAbouts = new List<SigningDocumentFields.About>
                {
                   new SigningDocumentFields.About { IFrameUrl = "https://example.com/about1" },
                   new SigningDocumentFields.About { IFrameUrl = "https://example.com/about2" }
                },

                IFrameUrl = "https://example.com/iframe",
                document_hash = "Hash123",
                code = "Code123",
                state = "State123",
                Iframestatus = "Active",
                EditedBy = "Editor123",
                UploadedDocument = "UploadedDocumentFileName",
                PDFbyteArray = "PDFByteArrayBase64",
                PreviewQuestions = "Question1, Question2",
                PreviewPDFContent = "Preview PDF Content",
                LetterHtmlContent = "<html>Engagement Letter</html>",
                EngagementLetterName = "EngagementLetter123",
                StatusCode = HttpStatusCode.OK,
                ErrorMessage = "No errors",
                ErrorPopupMessage = "Error message here",
            };


            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }


        [Fact]
        public void About_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new About
            {
                IFrameUrl = "https://example.com/about",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }


        [Fact]
        public void Redirect_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new Redirect
            {
                success = "SuccessValue",
                access_token = "AccessTokenValue",
                token_type = "TokenTypeValue",
                expires_in = "ExpiresInValue",
                state = "StateValue",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }


        [Fact]
        public void File_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new File
            {
                name = "FileName",
                file_id = "FileIdValue",
                pages = 42,
                status = "StatusValue",
                document_hash = "DocumentHashValue",
                id = 123,
                embedded_claim_url = "https://example.com/claim",
                embedded_signing_url = "https://example.com/signing",
            };


            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void InsideFile_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new InsideFile
            {
                name = "InsideFileName",
                file_id = "InsideFileIdValue",
                pages = 42,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }


        [Fact]
        public void sign_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new sign
            {
                XID = "XIDValue",
                YID = "YIDValue",
                Height = "HeightValue",
                Width = "WidthValue",
                Pageid = 42,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }



        [Fact]
        public void Signer_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new Signer
            {
                id = 1,
                name = "John Doe",
                email = "johndoe@example.com",
                role = "Signatory",
                order = 2,
                pin = "1234",
                message = "Please sign the document.",
                signed = 1,
                signed_timestamp = new DateTime(2023, 10, 31, 12, 0, 0),
                required = 1,
                deliver_email = 1,
                language = "English",
                declined = 0,
                removed = 0,
                bounced = 0,
                sent = 1,
                viewed = 1,
                status = "Completed",
                embedded_signing_url = "https://example.com/signing-url",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }



        [Fact]
        public void Recipient_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new Recipient
            {
                name = "Jane Doe",
                email = "janedoe@example.com",
                role = "Recipient",
                message = "Please review the document.",
                required = 1,
                language = "English",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }



        [Fact]
        public void Log_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new Log
            {
                @event = "EventName",
                signer = new { Name = "John Doe", Email = "johndoe@example.com" },
                timestamp = 1635721200,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }



        [Fact]
        public void Meta_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new Meta
            {
                some_key = "SomeValue",
                another_key = "AnotherValue",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }


        [Fact]
        public void CustomList_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new CustomList
            {
                name = "ListName",
                file_id = "FileIdValue",
                pages = 42,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }


        [Fact]
        public void NewRootList_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new NewRootList
            {
                rootList = new List<NewRoot>
                {
                  new NewRoot { document_hash = "Hash1" },
                  new NewRoot { document_hash = "Hash2" },
                }
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }


        [Fact]
        public void NewRoot_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new NewRoot
            {
                document_hash = "Hash123",
                requester_email = "requester@example.com",
                custom_requester_name = "Custom Requester",
                custom_requester_email = "custom.requester@example.com",
                is_draft = 1,
                is_template = 0,
                is_completed = 1,
                is_archived = 0,
                is_deleted = 0,
                is_trashed = 0,
                is_cancelled = 0,
                is_expired = 0,
                embedded = 1,
                in_person = 0,
                embedded_signing_enabled = 1,
                flexible_signing = 1,
                permission = "PermissionValue",
                template_id = "TemplateId123",
                title = "Document Title",
                subject = "Document Subject",
                message = "Document Message",
                use_signer_order = 1,
                reminders = 3,
                require_all_signers = 1,
                redirect = "RedirectURL",
                redirect_decline = "DeclineURL",
                client = "ClientName",
                created = 1234567890,
                expires = "2023-12-31",
                files = new List<File>
        {
            new File { name = "File1" },
            new File { name = "File2" },
        },
                signers = new List<Signer>
        {
            new Signer { id = 1, name = "Signer1" },
            new Signer { id = 2, name = "Signer2" },
        },
                recipients = new List<Recipient>
        {
            new Recipient { name = "Recipient1" },
            new Recipient { name = "Recipient2" },
        },
                fields = new List<List<object>>
        {
            new List<object> { "Field1" },
            new List<object> { "Field2" },
        },
                log = new List<Log>
        {
            new Log { @event = "Event1" },
            new Log { @event = "Event2" },
        },
                meta = new Meta { some_key = "Key1", another_key = "Key2" },
                embedded_claim_url = "https://example.com/claim",
                embedded_signing_url = "https://example.com/signing",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }


        [Fact]
        public void EngagementLetterDetails_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new EngagementLetterDetails
            {
                code = "CodeValue",
                state = "StateValue",
                EngagementLetterId = 123,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }


        [Fact]
        public void FlatteningModel_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new FlatteningModel
            {
                PDFContent = "PDF Content Value",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void AdobeResponse_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new AdobeResponse
            {
                uploadUri = "https://example.com/upload",
                assetID = "Asset123",
                AttachemntPDFcontent = "Attachment PDF Content",
            };


            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
