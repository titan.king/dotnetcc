﻿using BulkSigning.Models.BulkSigning;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileInfo = BulkSigning.Models.BulkSigning.FileInfo;

namespace EngageCC.BulkSigning.UnitTests.ModelsUnitTests.BulkSigningModelUnitTests
{
    public class AdobeESignExpireyViewModelUnitTests
    {
        [Fact]
        public void AdobeESignExpireyModel_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new AdobeESignExpireyModel
            {
                fileInfos = new List<FileInfo>(),
                name = "Valid Name",
                participantSetsInfo = new List<ParticipantSetsInfo>(),
                signatureType = "Valid Signature Type",
                externalId = new ExternalId { id = "Valid Id" },
                state = "Valid State",
                message = "Valid Message",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
