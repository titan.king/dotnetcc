﻿using BulkSigning.Models.BulkSigning;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.ModelsUnitTests.BulkSigningModelUnitTests
{
    public class EngagementLetterViewModelUnitTests
    {
        [Fact]
        public void EngagementLetterViewModel_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new EngagementLetterViewModel
            {
                EngagementLetterId = 1,
                BatchId = 2,
                EngagementLetterName = "LetterName",
                OfficeId = 3,
                OfficeName = "OfficeName",
                YearId = 4,
                TaxYear = "2023",
                TemplateId = 5,
                TemplateName = "TemplateName",
                TemplateVersion = 6,
                ClientId = 7,
                ClientName = "ClientName",
                PartnerId = 8,
                PartnerName = "PartnerName",
                AdminId = 9,
                AdminName = "AdminName",
                DocumentStatusId = 10,
                EngageTypeId = 11,
                DepartmentId = 12,
                CreatedOn = DateTime.Now,
                CreatedBy = "CreatedBy",
                ModifiedOn = null,
                IsDeleted = false,
                DeletedOn = null,
                DeletedBy = null,
                ModifiedBy = "ModifiedBy",
                BulkLettersId = 13,
                IsProcess = false,
                IsEnqueue = false,
                Is7216Available = false,
                ClientSignatureCount = 14,
                IsNewClient = false,
                SpouseFirstName = "SpouseFirstName",
                SpouseEmailId = "spouse@example.com",
                SignatoryEmailId = "signatory@example.com",
                SignatoryFirstName = "SignatoryFirstName",
                IsEsigning = true,
                SpouseLastName = "SpouseLastName",
                SignatoryLastName = "SignatoryLastName",
                SignatoryTitle = "SignatoryTitle",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

    }
}
