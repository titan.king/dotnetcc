﻿using BulkSigning.Models.BulkSigning;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.ModelsUnitTests.BulkSigningModelUnitTests
{
    public class HtmlFileResponseViewModelUnitTests
    {

        [Fact]
        public void HtmlFileResponse_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new HtmlFileResponseModel
            {
                HtmlUrl = "https://example.com/document.html",
                StatusCode = HttpStatusCode.OK,
                Message = "HTML file generated successfully",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

    }
}
