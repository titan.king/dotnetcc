﻿using BulkSigning.Models.BulkSigning;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.ModelsUnitTests.BulkSigningModelUnitTests
{
    public class CombinedPDFResponseModelUnitTests
    {
        [Fact]
        public void CombinedPDFResponseModel_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new CombinedPDFResponseModel
            {
                status = "ValidStatus",
                asset = new Assetes
                {
                    metadata = new Metadatas
                    {
                        type = "ValidType",
                        size = 1024
                    },
                    downloadUri = "https://example.com/document.pdf",
                    assetID = "AssetID"
                }
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
