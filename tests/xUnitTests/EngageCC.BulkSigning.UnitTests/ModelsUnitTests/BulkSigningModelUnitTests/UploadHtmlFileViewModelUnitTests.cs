﻿using BulkSigning.Models.BulkSigning;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.ModelsUnitTests.BulkSigningModelUnitTests
{
    public class UploadHtmlFileViewModelUnitTests
    {

        [Fact]
        public void UploadHtmlFile_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new UploadHtmlFile
            {
                HtmlData = "<html><body><p>Test HTML</p></body></html>",
                FileName = "test.html"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
