﻿using BulkSigning.Models.BulkSigning;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileInfo = BulkSigning.Models.BulkSigning.FileInfo;

namespace EngageCC.BulkSigning.UnitTests.ModelsUnitTests.BulkSigningModelUnitTests
{
    public class AdobeEsignViewModelUnitTests
    {

        [Fact]
        public void AdobeESignModel_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new AdobeESignModel
            {
                fileInfos = new List<FileInfo>(),
                name = "Valid Name",
                participantSetsInfo = new List<ParticipantSetsInfo>(),
                signatureType = "Valid Signature Type",
                externalId = new ExternalId { id = "Valid Id" },
                state = "Valid State",
                message = "Valid Message",
                expirationTime = "2023-12-31T23:59:59Z" 
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void FileInfo_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new FileInfo
            {
                urlFileInfo = new UrlFileInfo
                {
                    mimeType = "application/pdf",
                    name = "document.pdf",
                    url = "https://example.com/document.pdf"
                }
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void MemberInfo_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new MemberInfo
            {
                email = "test@example.com"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void ParticipantSetsInfo_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new ParticipantSetsInfo
            {
                memberInfos = new List<MemberInfo>
                {
                    new MemberInfo { email = "test1@example.com" },
                    new MemberInfo { email = "test2@example.com" }
                }, 
                order =1,
                role = "Participant"
        };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void UrlFileInfo_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new UrlFileInfo
            {
               mimeType = "application/pdf",
               name = "document.pdf",
               url = "https://example.com/document.pdf",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
