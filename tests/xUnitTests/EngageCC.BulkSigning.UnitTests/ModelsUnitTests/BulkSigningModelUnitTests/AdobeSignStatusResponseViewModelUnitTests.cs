﻿using BulkSigning.Models.BulkSigning;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.ModelsUnitTests.BulkSigningModelUnitTests
{
    public class AdobeSignStatusResponseViewModelUnitTests
    {
        [Fact]
        public void AdobeSignStatusResponseVM_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new AdobeSignStatusResponseVM
            {
                id = "ValidId",
                name = "ValidName",
                groupId = "ValidGroupId",
                type = "ValidType",
                participantSetsInfo = new Participantsetsinfo[] { new Participantsetsinfo() },
                senderEmail = "sender@example.com",
                createdDate = DateTime.Now,
                lastEventDate = DateTime.Now,
                signatureType = "ValidSignatureType",
                externalId = new Externalid { id = "ExternalId" },
                locale = "en-US",
                status = "ValidStatus",
                documentVisibilityEnabled = true,
                hasFormFieldData = true,
                hasSignerIdentityReport = true,
                agreementSettingsInfo = new Agreementsettingsinfo(),
                sendType = "ValidSendType",
                senderSigns = "ValidSenderSigns",
                documentRetentionApplied = true
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
