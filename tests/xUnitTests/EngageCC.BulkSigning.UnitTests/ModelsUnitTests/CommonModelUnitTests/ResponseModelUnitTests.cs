﻿using BulkSigning.Models.CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.ModelsUnitTests.CommonModelUnitTests
{
    public class ResponseModelUnitTests
    {
    
        [Fact]
        public void ResponseModel_ConstructorWithStatusCodeAndErrorMessage_InitializesStatusCodeAndErrorMessage()
        {
            // Arrange
            var expectedStatusCode = HttpStatusCode.NotFound;
            var expectedErrorMessage = "Sample error message";

            // Act
            var response = new ResponseModel(expectedStatusCode, expectedErrorMessage);

            // Assert
            Assert.Equal(expectedStatusCode, response.StatusCode);
            Assert.False(response.Status);
            Assert.Null(response.Data);
            Assert.Equal(expectedErrorMessage, response.ErrorMessage);
            Assert.Equal(string.Empty, response.PdfUrl);
        }

        [Fact]
        public void ResponseModel_PdfUrl_DefaultValueIsStringEmpty()
        {
            // Arrange

            // Act
            var response = new ResponseModel();

            // Assert
            Assert.Equal(string.Empty, response.PdfUrl);
        }

        [Fact]
        public void Constructor_WithStatusCode_PropertiesAreInitializedCorrectly()
        {
            // Arrange
            var expectedStatusCode = HttpStatusCode.BadRequest;

            // Act
            var responseModel = new ResponseModel(expectedStatusCode);

            // Assert
            Assert.Equal(expectedStatusCode, responseModel.StatusCode);
            Assert.False(responseModel.Status);
            Assert.Null(responseModel.Data); 
            Assert.Null(responseModel.ErrorMessage); 
            Assert.Equal(string.Empty, responseModel.PdfUrl);
        }

        [Fact]
        public void PdfConversion_ValidModel_PassesValidation()
        {
            // Arrange

            var model = new ResponseModel
            {
                StatusCode = HttpStatusCode.OK,
                Status = true,
                Data = new { Property1 = "Value1", Property2 = 42 },
                ErrorMessage = "No errors",
                PdfUrl = "https://example.com/document.pdf",
            };


            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }




    }
}
