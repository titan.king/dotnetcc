﻿using BulkSigning.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.ModelsUnitTests.CommonModelUnitTests
{
    public class PartnersViewModelUnitTests
    {
        [Fact]
        public void PartnersViewModel_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new PartnersViewModel
            {
                PartnerId = 1,
                PartnerName = "Valid Partner Name"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

    }
}
