﻿using AutoFixture;
using BulkSigning.Data.DataModel;
using Dashboard.Models.Login;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.DataModelUnitTests
{
    public class BatchRequestModelUnitTests
    {
        [Fact]
        public void ValidBatchRequestModel_ValidatesSuccessfully()
        {
            // Arrange
            var fixture = new Fixture();
            var model = fixture.Create<BatchRequest>();

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }


    }
}
