﻿using AutoFixture;
using BulkSigning.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.DataModelUnitTests
{
    public class BulkLetterAttachmentsModelUnitTests
    {

        [Fact]
        public void ValidBulkLetterAttachmentsModel_ValidatesSuccessfully()
        {
            // Arrange
            var fixture = new Fixture();
            var model = fixture.Create<BulkLetterAttachments>();

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
