﻿using BulkSigning.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.DataModelUnitTests
{
    public class TemplateModelUnitTests
    {
        [Fact]
        public void Template_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new Template
            {
                TemplateId = 1,
                TemplateName = "Valid Template Name",
                TemplateDescription = "Valid Template Description",
                DepartmentId = 2,
                Departments = new Department
                {
                    DepartmentId =2
                },
                EngageTypeId = 3,
                EngageType = new EngageType
                {
                    EngageTypeId =3
                },
                ChangeNotes = "Valid Change Notes",
                IsDeleted = false,
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                CreatedBy = "Valid User",
                ModifiedBy = "Valid User",
                DeletedOn = DateTime.Now,
                DeletedBy = "Valid User",
                AttachmentCount =1,
                StatusId = 4,
                Status = new Status
                {
                    StatusId =4
                },
                AttachmentURL = "Valid Attachment URL",
                Is7216Available = true,
                ClientSignatureCount = 1
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
