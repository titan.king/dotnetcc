﻿using BulkSigning.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.DataModelUnitTests
{
    public class LetterFieldValuesModelUnitTests
    {
        [Fact]
        public void LetterFieldValues_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new LetterFieldValues
            {
                LetterFieldValuesId = 1,
                ClientId = 2,
                PartnerId = 3,
                BulkLettersId = 4,
                BulkLetters = new BulkLetters
                {
                    BulkLettersId=4
                },
                FieldId = 5,
                FieldValue = "Valid Field Value",
                FieldName = "Valid Field Name",
                LetterVersion = 6,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void LetterFieldValues_InvalidFieldValue_FailsValidation()
        {
            // Arrange
            var model = new LetterFieldValues
            {
                LetterFieldValuesId = 1,
                ClientId = 2,
                PartnerId = 3,
                BulkLettersId = 4,
                FieldId = 5,
                FieldValue = new string('a', 256),
                FieldName = "Valid Field Name",
                LetterVersion = 6,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("FieldValue"));
        }

       
    }
}
