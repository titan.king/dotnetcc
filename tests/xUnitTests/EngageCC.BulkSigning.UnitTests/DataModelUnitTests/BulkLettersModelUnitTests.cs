﻿using AutoFixture;
using BulkSigning.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.DataModelUnitTests
{
    public class BulkLettersModelUnitTests
    {
        [Fact]
        public void BulkLetters_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new BulkLetters
            {
                BulkLettersId = 1,
                ClientId = 2,
                ClientName = "John Doe",
                OfficeId = 3,
                Office = "Office A",
                SignatoryEmailId = "john@example.com",
                SignatoryFirstName = "John",
                PartnerName = "Partner X",
                BatchId = 4,
                TemplateName = "Template ABC",
                FieldJson = "{'Field1': 'Value1', 'Field2': 'Value2'}",
                DocumentStatusName = "Draft",
                IsDraft = false,
                IsDeleted = false,
                IsUpdated = false,
                CreatedOn = DateTime.Now,
                ModifiedOn = null,
                CreatedBy = "User123",
                ModifiedBy = null,
                DeletedOn = null,
                DeletedBy = null,
                IsBulkLetter = true,
                IsProcess = false,
                IsEnqueue = false,
                PartnerId = 5,
                IsEsigning = null,
                Is7216Available = false,
                ClientSignatureCount = 0,
                IsNewClient = false,
                SpouseFirstName = "Jane",
                SpouseEmailId = "jane@example.com",
                SpouseLastName = "Doe",
                SignatoryLastName = "Doe",
                SignatoryTitle = "Mr.",
                letterFieldValues = new List<LetterFieldValues>
                {
                new LetterFieldValues
                {
                     LetterFieldValuesId = 1,
                },
               
            }
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void BulkLetters_InvalidClientName_FailsValidation()
        {
            // Arrange
            var model = new BulkLetters
            {
                BulkLettersId = 1,
                ClientId = 2,
                ClientName = new string('a', 256),
                CreatedOn = DateTime.Now,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("ClientName"));
        }

   
    }
}
