﻿using BulkSigning.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.DataModelUnitTests
{
    public class SigningInfoModelUnitTests
    {

        [Fact]
        public void SigningInfo_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new SigningInfo
            {
                SigningInfoId = 1,
                EngagementLetterId = 2,
                EngagementLetter = new EngagementLetter
                {
                    EngagementLetterId =2
                },
                SigningPartnerName = "Valid Partner Name",
                DelegatedName = "Valid Delegated Name",
                SPEmail = "valid@email.com",
                ContactPerson1Name = "Valid Contact Person 1 Name",
                ContactPerson1Email = "valid@email.com",
                Title = "Valid Title",
                Message = "Valid Message",
                CreatedBy = "Valid User",
                CreatedOn = DateTime.Now,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }


        [Fact]
        public void SigningInfo_InvalidTitle_FailsValidation()
        {
            // Arrange
            var model = new SigningInfo
            {
                SigningInfoId = 1,
                EngagementLetterId = 2,
                SigningPartnerName = "Valid Partner Name",
                DelegatedName = "Valid Delegated Name",
                SPEmail = "valid@email.com",
                ContactPerson1Name = "Valid Contact Person 1 Name",
                ContactPerson1Email = "valid@email.com",
                Title = new string('a', 401),
                Message = "Valid Message",
                CreatedBy = "Valid User",
                CreatedOn = DateTime.Now,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("Title"));
        }

       
    }
}
