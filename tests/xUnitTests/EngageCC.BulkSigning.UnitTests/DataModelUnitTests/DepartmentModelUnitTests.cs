﻿using BulkSigning.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.DataModelUnitTests
{
    public class DepartmentModelUnitTests
    {
        [Fact]
        public void Department_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new Department
            {
                DepartmentId = 1,
                DepartmentName = "Valid Department",
                IsDeleted = false,
                DepartmentStatusId = 2,
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                CreatedBy = "Valid User",
                ModifiedBy ="Valid User",
                DeletedOn = DateTime.Now,
                DeletedBy = "Valid User"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void Department_InvalidDepartmentName_FailsValidation()
        {
            // Arrange
            var model = new Department
            {
                DepartmentId = 1,
                DepartmentName = new string('a', 256), // Invalid, exceeds MaxLength
                IsDeleted = false,
                DepartmentStatusId = 2,
                CreatedOn = DateTime.Now,
                // Set other properties with valid values
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("DepartmentName"));
        }

    }
}
