﻿using AutoFixture;
using BulkSigning.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.DataModelUnitTests
{
    public class EngagementLetterModelUnitTests
    {
        [Fact]
        public void EngagementLetter_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new EngagementLetter
            {
                EngagementLetterId = 1,
                BatchId = 2,
                EngagementLetterName = "Valid Engagement Letter",
                OfficeId = 3,
                OfficeName = "Valid Office",
                YearId = 4,
                TaxYear = "2023",
                TemplateId = 5,
                TemplateName = "Valid Template",
                TemplateVersion = 6,
                ClientId = 7,
                ClientName = "Valid Client",
                PartnerId = 8,
                PartnerName = "Valid Partner",
                AdminId = 9,
                AdminName = "Valid Admin",
                DocumentStatusId = 10,
                DocumentStatus = new DocumentStatus
                {
                    DocumentStatusId =10
                },
                EngageTypeId = 11,
                EngageType = new EngageType
                {
                    EngageTypeId =11
                },
                DepartmentId = 12,
                Departments = new Department
                {
                    DepartmentId =12
                },
                CreatedOn = DateTime.Now,
                CreatedBy = "Valid User",
                IsDeleted = false,
                ModifiedOn = DateTime.Now,
                DeletedOn = DateTime.Now,
                DeletedBy = "Deleted User",
                ModifiedBy = "Modified User",
                BulkLettersId = 13,
                IsProcess = true,
                IsEnqueue = false,
                Is7216Available = true,
                ClientSignatureCount = 2,
                IsNewClient = true,
                SpouseFirstName = "Valid Spouse",
                SpouseEmailId = "spouse@example.com",
                SignatoryEmailId = "signatory@example.com",
                SignatoryFirstName = "Valid Signatory",
                IsEsigning = true,
                SignatoryLastName ="Valid Signatory",
                SignatoryTitle = "Valid Title",
                SpouseLastName = "Valid SpouseName"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

    }
}
