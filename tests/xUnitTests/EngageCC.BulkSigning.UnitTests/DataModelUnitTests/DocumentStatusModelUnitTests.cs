﻿using BulkSigning.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.DataModelUnitTests
{
    public class DocumentStatusModelUnitTests
    {
        [Fact]
        public void DocumentStatus_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new DocumentStatus
            {
                DocumentStatusId = 1,
                Description = "Valid Description",
                Remarks = "Valid Remarks",
                IsDeleted = false,
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                CreatedBy = "Valid User",
                ModifiedBy = "Valid User",
                DeletedOn = DateTime.Now,
                DeletedBy = "Valid User"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }


    }
}
