﻿using BulkSigning.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.DataModelUnitTests
{
    public class EngageTypeModelUnitTests
    {
        [Fact]
        public void EngageType_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new EngageType
            {
                EngageTypeId = 1,
                EngageTypeName = "Valid Engage Type",
                IsDeleted = false,
                EngageTypeStatusId = 2,
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                DepartmentId = 3,
                CreatedBy = "Valid User",
                ModifiedBy = "Valid User",
                DeletedOn = DateTime.Now,
                DeletedBy = "Valid User"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void EngageType_InvalidEngageTypeName_FailsValidation()
        {
            // Arrange
            var model = new EngageType
            {
                EngageTypeId = 1,
                EngageTypeName = new string('a', 256), 
                IsDeleted = false,
                EngageTypeStatusId = 2,
                CreatedOn = DateTime.Now,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("EngageTypeName"));
        }

    }
}
