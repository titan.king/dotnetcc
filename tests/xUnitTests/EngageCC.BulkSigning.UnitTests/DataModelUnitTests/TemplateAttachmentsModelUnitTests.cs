﻿using BulkSigning.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.BulkSigning.UnitTests.DataModelUnitTests
{
    public class TemplateAttachmentsModelUnitTests
    {
        [Fact]
        public void TemplateAttachments_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new TemplateAttachments
            {
                TemplateAttachmentsId = 1,
                TemplateId = 2,
                Template = new Template
                {
                    TemplateId =2
                },
                AttachmentJson = "Valid Attachment JSON",
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now, 
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
