﻿using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.Interfaces;
using Dashboard.Models.Login;
using Dashboard.Models.ResponseModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.LoginServiceTests
{
    public class GetRolesandScreenListUnitTests
    {
        [Fact]
        public async Task GetRolesandScreenList_ShouldReturn_LoginViewModel()
        {
            // Arrange
            var mockLoginRepository = new Mock<ILoginRepository>();
            var mockMapper = new Mock<IMapper>();
            var loginService = new LoginService(mockLoginRepository.Object, mockMapper.Object);

            var inputLoginListModel = new LoginListModel
            {
                GroupIds = "1,2,3"
            };

            var expectedRoles = new List<string> { "Role1", "Role2" };
            var expectedScreenNames = new List<string> { "Screen1", "Screen2" };
            var expectedViewModel = new LoginViewModel
            {
                Roles = expectedRoles,
                ScreenNames = expectedScreenNames
            };

            mockLoginRepository.Setup(repo => repo.GetRolesandScreenList(inputLoginListModel))
                .ReturnsAsync(new LoginListModel
                {
                    Roles = expectedRoles,
                    ScreenNames = expectedScreenNames
                });

            mockMapper.Setup(mapper => mapper.Map<LoginViewModel>(It.IsAny<LoginListModel>()))
                .Returns(expectedViewModel);

            // Act
            var result = await loginService.GetRolesandScreenList(inputLoginListModel);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(expectedRoles, result.Roles);
            Assert.Equal(expectedScreenNames, result.ScreenNames);
            Assert.IsAssignableFrom<LoginViewModel>(result);
        }
    }
}
