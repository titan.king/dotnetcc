﻿using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.Interfaces;
using Dashboard.Models.Login;
using Dashboard.Models.ResponseModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.LoginServiceTests
{
    public class GetRolesUnitTests
    {

        [Fact]
        public async Task GetRoles_ShouldReturnLoginViewModel()
        {
            // Arrange
            var mockLoginRepository = new Mock<ILoginRepository>();
            var mockMapper = new Mock<IMapper>();
            var loginService = new LoginService(mockLoginRepository.Object, mockMapper.Object);
            var inputPermissions = new LoginViewModel
            {
                GroupIds = "1,2,3"
            };
            var expectedRoles = new List<string> { "Role1", "Role2" };
            var expectedViewModel = new LoginViewModel
            {
                GroupIds = inputPermissions.GroupIds,
                Roles = expectedRoles
            };

            mockLoginRepository.Setup(repo => repo.GetRoles(inputPermissions))
                .ReturnsAsync(expectedViewModel);

            // Act
            var result = await loginService.GetRoles(inputPermissions);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(expectedRoles, result.Roles);
            Assert.IsAssignableFrom<LoginViewModel>(result);
        }
    }
}
