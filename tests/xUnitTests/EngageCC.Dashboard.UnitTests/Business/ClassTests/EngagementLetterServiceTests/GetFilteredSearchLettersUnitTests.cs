﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Models.Blocks;
using Dashboard.Models.EngagementLetter;
using Dashboard.Models.ResponseModel;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.EngagementLetterServiceTests
{
    public class GetFilteredSearchLettersUnitTests
    {
        private readonly Fixture _fixture;
        private readonly Mock<IEngagementLetterRepository> _engagementLetterRepositoryMock;
        private readonly Mock<IMapper> _mapper;
        private readonly Mock<IConfiguration> _configurationMock;

        public GetFilteredSearchLettersUnitTests()
        {
            _fixture = new Fixture();
            _engagementLetterRepositoryMock = new Mock<IEngagementLetterRepository>();
            _mapper = new Mock<IMapper>();
            _configurationMock = new Mock<IConfiguration>();
        }

        [Fact]
        public async Task GetFilteredSearchLetters_ReturnsValidViewModel()
        {
            // Arrange
            var engagementLetterService = new EngagementLetterService(
                _engagementLetterRepositoryMock.Object,
                _mapper.Object,
                _configurationMock.Object
            );

            var expectedFilteredList = _fixture.CreateMany<List<EngagementLetterSearchList>>(3).ToList();
            var expectedMappedList = _fixture.CreateMany<EngagementLetterListViewModel>(3).ToList();

            _mapper.Setup(mapper =>
                mapper.Map<List<EngagementLetterListViewModel>>(It.IsAny<IEnumerable<EngagementLetterSearchList>>()))
                .Returns(expectedMappedList);

            _engagementLetterRepositoryMock
                .Setup(repo => repo.GetFilteredSearchLetters(It.IsAny<EngagementLetterSearchList>()))
                .ReturnsAsync(expectedFilteredList.SelectMany(list => list));

            // Act
            var result = await engagementLetterService.GetFilteredSearchLetters(new EngagementLetterSearchList());

            // Assert
            Assert.NotNull(result);
            Assert.Equal(expectedMappedList, result); 
        }

    }
}
