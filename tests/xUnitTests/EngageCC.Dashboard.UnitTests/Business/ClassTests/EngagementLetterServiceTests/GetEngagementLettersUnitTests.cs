﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.EngagementLetterServiceTests
{
    public class GetEngagementLettersUnitTests
    {

        private readonly Fixture _fixture;
        private readonly Mock<IEngagementLetterRepository> _engagementLetterRepositoryMock;
        private readonly Mock<IMapper> _mapper;
        private readonly Mock<IConfiguration> _configurationMock;

        public GetEngagementLettersUnitTests()
        {
            _fixture = new Fixture();
            _engagementLetterRepositoryMock = new Mock<IEngagementLetterRepository>();
            _mapper = new Mock<IMapper>();
            _configurationMock = new Mock<IConfiguration>();
        }

        [Fact]
        public async Task GetEngagementLetters_ReturnsEngagementLetterList()
        {
            var engagementLetterService = new EngagementLetterService(
                _engagementLetterRepositoryMock.Object,
                _mapper.Object,
                _configurationMock.Object
            );

            var expectedEngagementLetters = _fixture.Create<List<EngagementLetter>>();
            var expectedEngagementLetterList = expectedEngagementLetters
                .Select(letter => new EngagementLetterList 
                {
                    EngagementLetterId = letter.EngagementLetterId 
                })
                .ToList();

            _engagementLetterRepositoryMock.Setup(repo => repo.GetEngagementLetters())
                .ReturnsAsync(expectedEngagementLetterList);

            // Act
            var result = await engagementLetterService.GetEngagementLetters();

            // Assert
            Assert.NotNull(result);
            Assert.Equal(expectedEngagementLetterList.Count, result.Count());
        }
    }
}
