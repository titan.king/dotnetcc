﻿using Dashboard.Business.Services;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.EngagementLetterServiceTests
{
    public class RefreshedSignedLetterStatusUnitTests
    {
        [Fact]
        public async Task RefreshedSignedLetterStatus_FailsWhenDocumentStatusIdIsNot2()
        {
            // Arrange
            var configurationMock = new Mock<IConfiguration>();
            var engagementLetterRepositoryMock = new Mock<IEngagementLetterRepository>();
            var engagementLetterService = new EngagementLetterService(
                engagementLetterRepositoryMock.Object,
                null, 
                configurationMock.Object 
            );

            engagementLetterRepositoryMock.Setup(repo => repo.GetEngagementLetterStatus(It.IsAny<int>()))
                .ReturnsAsync(new EngagementLetter { DocumentStatusId = 1 }); 

            // Act
            var result = await engagementLetterService.RefreshedSignedLetterStatus(1, "mocked-agreement-id");

            // Assert
            Assert.NotNull(result);
            Assert.False(result.Status); 
            Assert.Null(result.Data);
        }
    }
}
