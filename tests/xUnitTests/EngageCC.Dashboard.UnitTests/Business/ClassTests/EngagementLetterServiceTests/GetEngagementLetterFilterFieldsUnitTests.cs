﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Models.EngagementLetter;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.EngagementLetterServiceTests
{
    public class GetEngagementLetterFilterFieldsUnitTests
    {
        private readonly Fixture _fixture;
        private readonly Mock<IEngagementLetterRepository> _engagementLetterRepositoryMock;
        private readonly IMapper _mapper;
        private Mock<IConfiguration> _cofigurationMock;

        public GetEngagementLetterFilterFieldsUnitTests()
        {
            _fixture = new Fixture();
            _engagementLetterRepositoryMock = new Mock<IEngagementLetterRepository>();
            _mapper = new Mapper(new MapperConfiguration(cfg => cfg.CreateMap<EngagementLetter, DeleteEngagementLetterViewModel>()));
            _cofigurationMock = new Mock<IConfiguration>();
        }

        [Fact]
        public async Task GetEngagementLetterFilterFields_ReturnsValidViewModel()
        {
            // Arrange
            var engagementLetterService = new EngagementLetterService(
                _engagementLetterRepositoryMock.Object,
                _mapper,
               _cofigurationMock.Object
            );

            var fixture = new Fixture();
            fixture.Behaviors.Add(new OmitOnRecursionBehavior());
            var expectedEngagementLetterIds = fixture.CreateMany<EngagementLetterIdsViewModel>(5).ToList();

            var expectedEngagementLetterNames = _fixture.Create<List<EngagementLetterNamesViewModel>>();
            var expectedClientNames = _fixture.Create<List<ClientNamesViewModel>>();
            var expectedYears = _fixture.Create<List<TaxYearsViewModel>>();
            var expectedPartnerNames = _fixture.Create<List<SigningPartnersViewModel>>();
            var expectedOfficeNames = _fixture.Create<List<OfficeDetailsViewModel>>();
            var expectedAdminNames = _fixture.Create<List<AdminDetailsViewModel>>();
            var expectedTemplateNames = _fixture.Create<List<TemplateNamesViewModel>>();
            var expectedTypes = _fixture.Create<List<EngageTypesViewModel>>();
            var expectedLetterStatus = _fixture.Create<List<LetterStatusViewModel>>();
            var expectedDepartments = _fixture.Create<List<DepartmentsViewModel>>();

            _engagementLetterRepositoryMock.Setup(repo => repo.GetEngagementLettersSearchFilter())
                .ReturnsAsync(_fixture.Create<List<EngagementLetter>>());
            _engagementLetterRepositoryMock.Setup(repo => repo.GetDepartmentSearchFilter())
                .ReturnsAsync(_fixture.Create<List<Department>>());
            _engagementLetterRepositoryMock.Setup(repo => repo.GetEngageTypeSearchFilter())
                .ReturnsAsync(_fixture.Create<List<EngageType>>());
            _engagementLetterRepositoryMock.Setup(repo => repo.GetDocumentStatusSearchFilter())
                .ReturnsAsync(_fixture.Create<List<DocumentStatus>>());

            // Act
            var result = await engagementLetterService.GetEngagementLetterFilterFields();

            // Assert
            Assert.NotNull(result);
  
        }
    }
}
