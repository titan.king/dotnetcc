﻿using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Business.Services;
using Dashboard.Data.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.ResponseModel;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.EngagementLetterServiceTests
{
    public class DeleteEngagementLetterUnitTests
    {

        [Fact]
        public async Task DeleteEngagementLetter_FailsWhenEngLetterIdIsNull()
        {
            // Arrange
            var engagementLetterRepositoryMock = new Mock<IEngagementLetterRepository>();
            var configurationMock = new Mock<IConfiguration>();
            configurationMock.Setup(config => config["AppSettingsAdobe:adobe_integrationkey"]).Returns("mocked-key");
            IEngagementLetterService engagementLetterService = new EngagementLetterService(
                engagementLetterRepositoryMock.Object,
                null, 
                configurationMock.Object
            );

            // Act
            var result = await engagementLetterService.DeleteEngagementLetter(null); // Pass null as engLetterId

            // Assert
            Assert.True(result); 
            engagementLetterRepositoryMock.Verify(repo => repo.GetSigningInfoDetail(It.IsAny<int>()), Times.Never);
        }


    }
}
