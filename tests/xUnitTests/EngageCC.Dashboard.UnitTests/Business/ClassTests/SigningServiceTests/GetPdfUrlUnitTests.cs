﻿using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.Interfaces;
using Engage3.Common.Business;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.SigningServiceTests
{
    public class GetPdfUrlUnitTests
    {
        private readonly EmailHelper _emailHelper;

        [Fact]
        public async Task GetPdfUrl_ShouldReturn_PdfUrl()
        {
            // Arrange
            var mockSigningRepository = new Mock<ISigningRepository>();
            var mockMapper = new Mock<IMapper>();
            var mockResourceManager = new Mock<ResourceManager>();
            var mockConfiguration = new Mock<IConfiguration>();

            var signingService = new SigningService(
                mockSigningRepository.Object,
                mockResourceManager.Object,
                mockMapper.Object,
                mockConfiguration.Object,
                null,
                null
                );

            var letterId = 123;
            var expectedPdfUrl = "https://example.com/pdf";

            mockSigningRepository.Setup(repo => repo.GetPdfUrl(letterId))
                .ReturnsAsync(expectedPdfUrl);

            // Act
            var result = await signingService.GetPdfUrl(letterId);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(expectedPdfUrl, result);
        }
    }
}
