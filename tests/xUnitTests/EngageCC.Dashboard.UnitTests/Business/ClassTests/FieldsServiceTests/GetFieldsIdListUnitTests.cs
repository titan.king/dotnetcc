﻿using Dashboard.Business.Services;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.FieldsServiceTests
{
    public class GetFieldsIdListUnitTests
    {
        [Fact]
        public async Task GetFieldsIdList_ReturnsListOfFieldsId()
        {
            // Arrange
            var fieldsRepositoryMock = new Mock<IFieldsRepository>();
            var fieldsService = new FieldsService(fieldsRepositoryMock.Object, null);

            var expectedFieldIds = new List<int> { 1, 2, 3 }; 

            fieldsRepositoryMock.Setup(repo => repo.GetFieldsIdList())
                .ReturnsAsync(expectedFieldIds.Select(id => new FieldsIdListResModel { FieldId = id }));

            // Act
            var result = await fieldsService.GetFieldsIdList();

            // Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<IEnumerable<FieldsIdListResModel>>(result);
            Assert.Equal(expectedFieldIds, result.Select(field => field.FieldId));
        }
    }
}
