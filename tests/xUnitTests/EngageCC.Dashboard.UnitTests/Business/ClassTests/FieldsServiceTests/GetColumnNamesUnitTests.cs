﻿using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.FieldsServiceTests
{
    public class GetColumnNamesUnitTests
    {
        [Fact]
        public async Task GetColumnNames_ReturnsValidColumnNames()
        {
            // Arrange
            var mapperMock = new Mock<IMapper>();
            var fieldsRepositoryMock = new Mock<IFieldsRepository>();
            var fieldsService = new FieldsService(fieldsRepositoryMock.Object, mapperMock.Object);

            var columnNamesData = new List<ColumnNames>
            {
                new ColumnNames { ColumnNamesId = 1, ColumnName = "Column1" },
                new ColumnNames { ColumnNamesId = 2, ColumnName = "Column2" },
                // Add more sample column names as needed
            };

            fieldsRepositoryMock.Setup(repo => repo.GetColumnNames())
                .ReturnsAsync(columnNamesData.Select(column => new ColumnNamesResModel { ColumnId = column.ColumnNamesId, ColumnNames = column.ColumnName }).ToList());

            // Act
            var result = await fieldsService.GetColumnNames();

            // Assert
            Assert.NotNull(result);
            Assert.IsType<List<ColumnNamesResModel>>(result);

        }
    }
}
