﻿using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.Interfaces;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.FieldsServiceTests
{
    public class DeleteFieldsUnitTests
    {
        [Fact]
        public async Task DeleteFields_DeletesFieldsAndReturnsDeleteId()
        {
            // Arrange
            var mapperMock = new Mock<IMapper>();
            var fieldsRepositoryMock = new Mock<IFieldsRepository>();
            var fieldsService = new FieldsService(fieldsRepositoryMock.Object, mapperMock.Object);

            var deleteFieldsViewModel = new DeleteFieldsViewModel
            {
                FieldId = new int[] { 1, 2, 3 }, 
                IsDeleted = true
            };

            fieldsRepositoryMock.Setup(repo => repo.GetFieldsById(It.IsAny<int>()))
                .ReturnsAsync(new GetFieldsByIdList
                {
                    FieldsListById = new List<FieldListModel> { new FieldListModel { FieldId = 1 } },
                    FieldsConnectedBlocks = new List<BlocksList>()
                });

            mapperMock.Setup(mapper => mapper.Map<DeleteFieldResModel>(deleteFieldsViewModel))
                .Returns(new DeleteFieldResModel());

            // Act
            var result = await fieldsService.DeleteFields(deleteFieldsViewModel);

            // Assert
            Assert.NotNull(result);
            Assert.IsType<DeleteFieldsViewModel>(result);
            Assert.True(result.IsDeleted);
        }
    }
}
