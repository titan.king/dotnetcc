﻿using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.FieldsServiceTests
{
    public class UpdateFieldUnitTests
    {
        [Fact]
        public async Task UpdateField_ValidInput_ShouldReturnExpectedTuple()
        {
            // Arrange
            var mapperMock = new Mock<IMapper>();
            var fieldsRepositoryMock = new Mock<IFieldsRepository>();
            var fieldsService = new FieldsService(fieldsRepositoryMock.Object, mapperMock.Object);

            var fieldListModel = new FieldListModel
            {
                FieldId = 0,
                FieldName = "TestField",
            };

            var fieldToAdd = new Field
            {
                FieldId = 1,
                FieldName = "TestField",
            };

            fieldsRepositoryMock.Setup(repo => repo.IsValidFieldName(fieldListModel))
                .ReturnsAsync(true);
            mapperMock.Setup(mapper => mapper.Map<FieldListModel>(fieldToAdd))
                .Returns(fieldListModel);
            fieldsRepositoryMock.Setup(repo => repo.UpdateField(It.IsAny<Field>()))
                .ReturnsAsync(fieldToAdd);

            // Act
            var result = await fieldsService.UpdateField(fieldListModel);

            // Assert
            Assert.NotNull(result);
            Assert.IsType<Tuple<FieldListModel, string>>(result);
        }
    }
}
