﻿using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.FieldsServiceTests
{
    public class IsValidUnitTests
    {
        [Fact]
        public async Task IsValid_ExistingFieldName_ReturnsFalse()
        {
            // Arrange
            var mapperMock = new Mock<IMapper>();
            var fieldsRepositoryMock = new Mock<IFieldsRepository>();
            var fieldsService = new FieldsService(fieldsRepositoryMock.Object, mapperMock.Object);

            var existingFieldName = "ExistingField";
            var fieldListModel = new FieldListModel { FieldId = 1, FieldName = existingFieldName };

            fieldsRepositoryMock.Setup(repo => repo.IsValidFieldName(fieldListModel))
                .ReturnsAsync(false);

            // Act
            var result = await fieldsService.IsValid(fieldListModel);

            // Assert
            Assert.NotNull(result);
            Assert.IsType<Tuple<bool, string>>(result);
            Assert.False(result.Item1);  
            Assert.Equal("Field Name already exists.", result.Item2);
        }

        [Fact]
        public async Task IsValid_NonExistingFieldName_ReturnsTrue()
        {
            // Arrange
            var mapperMock = new Mock<IMapper>();
            var fieldsRepositoryMock = new Mock<IFieldsRepository>();
            var fieldsService = new FieldsService(fieldsRepositoryMock.Object, mapperMock.Object);

            var nonExistingFieldName = "NewField";
            var fieldListModel = new FieldListModel { FieldId = 1, FieldName = nonExistingFieldName };

            fieldsRepositoryMock.Setup(repo => repo.IsValidFieldName(fieldListModel))
                .ReturnsAsync(true);

            // Act
            var result = await fieldsService.IsValid(fieldListModel);

            // Assert
            Assert.NotNull(result);
            Assert.IsType<Tuple<bool, string>>(result);
            Assert.True(result.Item1);  
            Assert.Equal(string.Empty, result.Item2);
        }
    }
}
