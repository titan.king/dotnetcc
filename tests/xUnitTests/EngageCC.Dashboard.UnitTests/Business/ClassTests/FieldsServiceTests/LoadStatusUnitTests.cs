﻿using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.FieldsServiceTests
{
    public class LoadStatusUnitTests
    {
        [Fact]
        public async Task LoadStatus_ReturnsValidFieldsStatusResponse()
        {
            // Arrange
            var mapperMock = new Mock<IMapper>();
            var fieldsRepositoryMock = new Mock<IFieldsRepository>();
            var fieldsService = new FieldsService(fieldsRepositoryMock.Object, mapperMock.Object);

            var statusData = new List<Status>
            {
                new Status { StatusId = 1, StatusName = "Active" },
                new Status { StatusId = 2, StatusName = "Inactive" }
            };

            fieldsRepositoryMock.Setup(repo => repo.LoadStatus())
                .ReturnsAsync(new FieldsStatusResModel
                {
                    FieldsStatus = statusData.Select(status => new FieldsStatus { StatusId = status.StatusId, StatusName = status.StatusName }).ToList(),
                    FieldsDataType = new List<FieldsDataType>()
                });

            // Act
            var result = await fieldsService.LoadStatus();

            // Assert
            Assert.NotNull(result);
            Assert.IsType<FieldsStatusResModel>(result);
            Assert.NotNull(result.FieldsStatus);
            Assert.Equal(statusData.Count, result.FieldsStatus.Count);
            for (var i = 0; i < statusData.Count; i++)
            {
                Assert.Equal(statusData[i].StatusId, result.FieldsStatus[i].StatusId);
                Assert.Equal(statusData[i].StatusName, result.FieldsStatus[i].StatusName);
            }

        }
    }
}
