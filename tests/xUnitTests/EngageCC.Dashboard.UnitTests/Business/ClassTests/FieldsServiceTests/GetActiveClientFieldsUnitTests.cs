﻿using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.FieldsServiceTests
{
    public class GetActiveClientFieldsUnitTests
    {
        [Fact]
        public async Task GetActiveClientFields_ReturnsValidResponse()
        {
            // Arrange
            var mapperMock = new Mock<IMapper>();
            var fieldsRepositoryMock = new Mock<IFieldsRepository>();
            var fieldsService = new FieldsService(fieldsRepositoryMock.Object, mapperMock.Object);

            var clientId = 123;  
            var partnerId = 456;  

            var clientFieldsData = new ClientFields
            {
                ClientId = clientId,
            };

            var partnerOfficeData = new PartnerOfficeResModel
            {
                PartnerId = partnerId,
            };

            mapperMock.Setup(mapper => mapper.Map<ClientFieldsViewModel>(It.IsAny<ClientFields>()))
                .Returns(new ClientFieldsViewModel { ClientId = clientId  });

            mapperMock.Setup(mapper => mapper.Map<PartnerOfficeResModel>(It.IsAny<PartnerOfficeResModel>()))
                .Returns(partnerOfficeData );

            fieldsRepositoryMock.Setup(repo => repo.GetActiveClientFields(clientId, partnerId))
                .ReturnsAsync(new FieldsResponse
                {
                    ClientFields = mapperMock.Object.Map<ClientFieldsViewModel>(clientFieldsData),
                    PartnerOffice = mapperMock.Object.Map<PartnerOfficeResModel>(partnerOfficeData)
                });

            // Act
            var result = await fieldsService.GetActiveClientFields(clientId, partnerId);

            // Assert
            Assert.NotNull(result);
            Assert.IsType<FieldsResponse>(result);
            Assert.Equal(clientId, result.ClientFields.ClientId);

        }
    }
}
