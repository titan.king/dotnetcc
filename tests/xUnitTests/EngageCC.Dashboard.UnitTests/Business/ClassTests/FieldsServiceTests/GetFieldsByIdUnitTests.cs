﻿using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.FieldsServiceTests
{
    public class GetFieldsByIdUnitTests
    {

        [Fact]
        public async Task GetFieldsById_ReturnsFieldsListById()
        {
            // Arrange
            var mapperMock = new Mock<IMapper>();
            var fieldsRepositoryMock = new Mock<IFieldsRepository>();
            var fieldsListById = new List<FieldListModel>
            {
                new FieldListModel { FieldId = 1, FieldName = "Field1" },
                new FieldListModel { FieldId = 2, FieldName = "Field2" }
            };

            var fieldsConnectedBlocks = new List<BlocksList>
            {
                new BlocksList { BlockName = "Block1", Content = "Block1 Content" },
                new BlocksList { BlockName = "Block2", Content = "Block2 Content" }
            };

            var getFieldsByIdList = new GetFieldsByIdList
            {
                FieldsListById = fieldsListById,
                FieldsConnectedBlocks = fieldsConnectedBlocks
            };

            var fieldId = 1; 

            fieldsRepositoryMock.Setup(repo => repo.GetFieldsById(fieldId))
                .ReturnsAsync(getFieldsByIdList);

            var fieldsService = new FieldsService(fieldsRepositoryMock.Object, mapperMock.Object);

            // Act
            var result = await fieldsService.GetFieldsById(fieldId);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(fieldsListById.Count, result.FieldsListById.Count);
            Assert.Equal(fieldsConnectedBlocks.Count, result.FieldsConnectedBlocks.Count);
          
        }
    }
}
