﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.FieldsServiceTests
{
    public class GetFieldsUnitTests
    {

        [Fact]
        public async Task GetFields_ReturnsListOfFieldListModel()
        {
            // Arrange
            var fixture = new Fixture();
            var mapperMock = new Mock<IMapper>();
            var fieldsRepositoryMock = new Mock<IFieldsRepository>();
            var fieldModels = fixture.CreateMany<FieldListModel>().ToList();
            mapperMock.Setup(mapper => mapper.Map<IEnumerable<FieldListModel>>(It.IsAny<IEnumerable<FieldListModel>>()))
                .Returns((IEnumerable<FieldListModel> source) => source);

            fieldsRepositoryMock.Setup(repo => repo.GetFields())
                .ReturnsAsync(fieldModels);

            var fieldsService = new FieldsService(fieldsRepositoryMock.Object, mapperMock.Object);

            // Act
            var result = await fieldsService.GetFields();

            // Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<IEnumerable<FieldListModel>>(result);
            Assert.Equal(fieldModels.Count, result.Count());
        }
    }
}
