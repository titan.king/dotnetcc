﻿using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.FieldsServiceTests
{
    public class GetActiveFieldsUnitTests
    {
        [Fact]
        public async Task GetActiveFields_ReturnsActiveFieldList()
        {
            // Arrange
            var mapperMock = new Mock<IMapper>();
            var fieldsRepositoryMock = new Mock<IFieldsRepository>();
            var activeFieldModels = new List<FieldListModel>
            {
                new FieldListModel { FieldId = 1, FieldName = "Field1", StatusName = "Active" },
                new FieldListModel { FieldId = 2, FieldName = "Field2", StatusName = "Active" },
            };

            mapperMock.Setup(mapper => mapper.Map<IEnumerable<FieldListModel>>(It.IsAny<IEnumerable<FieldListModel>>()))
                .Returns(activeFieldModels);

            fieldsRepositoryMock.Setup(repo => repo.GetActiveFields())
                .ReturnsAsync(activeFieldModels);

            var fieldsService = new FieldsService(fieldsRepositoryMock.Object, mapperMock.Object);

            // Act
            var result = await fieldsService.GetActiveFields();

            // Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<IEnumerable<FieldListModel>>(result);
            Assert.Equal(activeFieldModels.Count, result.Count());
            Assert.All(result, field => Assert.Equal("Active", field.StatusName));
        }
    }
}
