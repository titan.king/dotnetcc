﻿using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.TemplateServiceUnitTests
{
    public class GetFileFromBlobUnitTests
    {
        [Fact]
        public async Task GetFileFromBlob_ValidRequest_ShouldReturnResponseModelWithPdfUrl()
        {
            // Arrange
            var templateRepositoryMock = new Mock<ITemplateRespository>();
            var mapperMock = new Mock<IMapper>();
            var Mockconfiguration = new Mock<IConfiguration>();
            var service = new TemplateService(templateRepositoryMock.Object, mapperMock.Object,null, Mockconfiguration.Object, null, null);


            var random = new Random();
            var accountName = "account" + random.Next(1000, 9999);
            var accountKey = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            var connectionString = $"DefaultEndpointsProtocol=https;AccountName={accountName};AccountKey={accountKey};EndpointSuffix=core.windows.net";
            var containerName = "container" + random.Next(1000, 9999);
            var fileName = "your_file_name";

            var blobSasTokenRequest = new BlobSasTokenRequest
            {
                AccountName = accountName,
                AccountKey = accountKey,
                ConnectionString = connectionString,
                ContainerName = containerName,
                FileName = fileName
            };

            var configuration = new ConfigurationBuilder().AddInMemoryCollection(new Dictionary<string, string>{
            { "BloBStorageConfig:AzureAccountName", accountName },
            { "ConnectionStrings:AzureBlobStorage", connectionString },
            { "BloBStorageConfig:AzureAccountKey", accountKey },
            { "BloBStorageConfig:AzureContainer_Html", containerName }
            }).Build();

            Mockconfiguration.Setup(a => a.GetSection(It.IsAny<string>())).Returns(configuration.GetSection);
            Mockconfiguration.Setup(a => a[It.IsAny<string>()]).Returns((string key) => configuration[key]);

            // Act
            var result = await service.GetFileFromBlob(blobSasTokenRequest);

            // Assert
            var responseModel = result;
            Assert.NotNull(responseModel);
            Assert.Equal(HttpStatusCode.OK, responseModel.StatusCode);
            Assert.NotNull(responseModel.Data);
        }
    }
}
