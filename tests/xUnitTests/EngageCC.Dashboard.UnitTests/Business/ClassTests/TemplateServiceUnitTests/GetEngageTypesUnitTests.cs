﻿using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.TemplateServiceUnitTests
{
    public class GetEngageTypesUnitTests
    {
        [Fact]
        public async Task GetEngageTypes_ValidDepartmentId_ShouldReturn_ListOfEngageTypesList()
        {
            // Arrange
            int departmentId = 123;
            var engageTypesList = new List<EngageTypesList>();
            var engageTypesListMapped = new List<EngageTypesList>();

            var templateRepositoryMock = new Mock<ITemplateRespository>();
            var mapperMock = new Mock<IMapper>();
            var service = new TemplateService(templateRepositoryMock.Object, mapperMock.Object,null,null,null,null);

            templateRepositoryMock.Setup(r => r.GetEngageTypes(departmentId)).ReturnsAsync(engageTypesList);

            mapperMock.Setup(m => m.Map<IEnumerable<EngageTypesList>>(engageTypesList)).Returns(engageTypesListMapped);

            // Act
            var result = await service.GetEngageTypes(departmentId);

            // Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<IEnumerable<EngageTypesList>>(result);
            Assert.Equal(engageTypesList, result);

            templateRepositoryMock.Verify(r => r.GetEngageTypes(departmentId), Times.Once);
            mapperMock.Verify(m => m.Map<IEnumerable<EngageTypesList>>(engageTypesList), Times.Once);
        }
    }
}
