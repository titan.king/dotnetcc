﻿using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.TemplateServiceUnitTests
{
    public class GetStatusUnitTests
    {
        [Fact]
        public async Task GetStatus_ShouldReturn_ListOfStatusListModel()
        {
            // Arrange
            var statusList = new List<StatusListModel>();
            var statusListModelList = new List<StatusListModel>();

            var templateRepositoryMock = new Mock<ITemplateRespository>();
            var mapperMock = new Mock<IMapper>();
            var service = new TemplateService(templateRepositoryMock.Object, mapperMock.Object,null,null,null,null);

            templateRepositoryMock.Setup(r => r.GetStatus()).ReturnsAsync(statusList);

            mapperMock.Setup(m => m.Map<IEnumerable<StatusListModel>>(statusList)).Returns(statusListModelList);

            // Act
            var result = await service.GetStatus();

            // Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<IEnumerable<StatusListModel>>(result);
            Assert.Equal(statusListModelList, result);

            templateRepositoryMock.Verify(r => r.GetStatus(), Times.Once);
            mapperMock.Verify(m => m.Map<IEnumerable<StatusListModel>>(statusListModelList), Times.Once);
        }
    }
}
