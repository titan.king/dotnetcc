﻿using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Dashboard.Models.Template;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.TemplateServiceUnitTests
{
    public class DeleteTemplateUnitTests
    {
        [Fact]
        public async Task DeleteTemplate_ValidInput_ShouldReturnTrue()
        {
           
            var deleteModel = new TemplateViewModel
            {   
            };

            var deleteReq = new TemplateListModel();
            var deleteResp = true;

            var templateRepositoryMock = new Mock<ITemplateRespository>();
            var mapperMock = new Mock<IMapper>();
            var service = new TemplateService(templateRepositoryMock.Object, mapperMock.Object,null,null,null,null);


            mapperMock.Setup(m => m.Map<TemplateListModel>(deleteModel)).Returns(deleteReq);

            templateRepositoryMock.Setup(r => r.DeleteTemplate(deleteReq)).ReturnsAsync(deleteResp);

            // Act
            var result = await service.DeleteTemplate(deleteModel);

            // Assert
            Assert.True(result); 
                                 
        }
    }
}
