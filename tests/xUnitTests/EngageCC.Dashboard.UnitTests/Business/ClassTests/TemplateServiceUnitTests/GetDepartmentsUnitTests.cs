﻿using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.TemplateServiceUnitTests
{
    public class GetDepartmentsUnitTests
    {
        [Fact]
        public async Task GetDepartments_ShouldReturn_ListOfDepartmentsList()
        {
            // Arrange
            var departmentList = new List<DepartmentsList>();
            var departmentsList = new List<DepartmentsList>();

            var templateRepositoryMock = new Mock<ITemplateRespository>();
            var mapperMock = new Mock<IMapper>();
            var service = new TemplateService(templateRepositoryMock.Object, mapperMock.Object,null,null,null,null);

            templateRepositoryMock.Setup(r => r.GetDepartments()).ReturnsAsync(departmentList);

            mapperMock.Setup(m => m.Map<IEnumerable<DepartmentsList>>(departmentList)).Returns(departmentsList);

            // Act
            var result = await service.GetDepartments();

            // Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<IEnumerable<DepartmentsList>>(result);
            Assert.Equal(departmentsList, result);

            templateRepositoryMock.Verify(r => r.GetDepartments(), Times.Once);
            mapperMock.Verify(m => m.Map<IEnumerable<DepartmentsList>>(departmentList), Times.Once);
        }
    }
}
