﻿using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.TemplateServiceUnitTests
{
    public class GetTemplateByIdUnitTests
    {
        [Fact]
        public async Task GetTemplateById_ValidId_ShouldReturn_TemplateListModel()
        {
            // Arrange
            int templateId = 123; 
            var templateRepositoryMock = new Mock<ITemplateRespository>();
            var mapperMock = new Mock<IMapper>();
            var service = new TemplateService(templateRepositoryMock.Object, mapperMock.Object,null,null,null,null);

            var templateModel = new TemplateListModel();
            var templateListModel = new TemplateListModel();

            templateRepositoryMock.Setup(r => r.GetTemplateById(templateId)).ReturnsAsync(templateModel);

            mapperMock.Setup(m => m.Map<TemplateListModel>(templateModel)).Returns(templateListModel);

            // Act  
            var result = await service.GetTemplateById(templateId);

            // Assert 
            Assert.NotNull(result);
            Assert.IsType<TemplateListModel>(result);
            Assert.Equal(templateListModel, result);

            templateRepositoryMock.Verify(r => r.GetTemplateById(templateId), Times.Once);
            mapperMock.Verify(m => m.Map<TemplateListModel>(templateModel), Times.Once);
        }
    }
}
