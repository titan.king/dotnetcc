﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.Context;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.TemplateServiceUnitTests
{
    public class GetTemplatesUnitTests
    {
        [Fact]
        public async Task GetTemplates_ShouldReturnListOfTemplateListModel()
        {
            // Arrange
            var templateRepositoryMock = new Mock<ITemplateRespository>();
            var mapperMock = new Mock<IMapper>();
            var dbcontext = new Mock<DashboardDBContext>();
            var configuration = new Mock<IConfiguration>();
            var httpcontextaccessor = new Mock<IHttpContextAccessor>();
            var resourceManager = new Mock<ResourceManager>();
             var fixture = new Fixture();

            var service = new TemplateService(
             templateRepositoryMock.Object,
             mapperMock.Object,
             null,null,null,null
         );

            var templateList = new List<TemplateListModel>();
            var templateListModelList = new List<TemplateListModel>();


            templateRepositoryMock.Setup(r => r.GetTemplates()).ReturnsAsync(templateList);

            mapperMock.Setup(m => m.Map<IEnumerable<TemplateListModel>>(templateList))
                .Returns(templateListModelList);

            // Act
            var result = await service.GetTemplates();

            // Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<IEnumerable<TemplateListModel>>(result);
            Assert.Equal(templateListModelList, result);
            templateRepositoryMock.Verify(r => r.GetTemplates(), Times.Once);
            mapperMock.Verify(m => m.Map<IEnumerable<TemplateListModel>>(templateList), Times.Once);
        }
    }
}
