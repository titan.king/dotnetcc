﻿using Dashboard.Business.Services;
using Dashboard.Data.Interfaces;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.TemplateServiceUnitTests
{
    public class GetTemplatesIdListUnitTests
    {
        [Fact]
        public async Task GetTemplatesIdList_ShouldReturnListOfTemplateIds()
        {
            // Arrange
            var expectedTemplateIds = new List<int> { 1, 2, 3 };

            var templateRepositoryMock = new Mock<ITemplateRespository>();
            var templateService = new TemplateService(templateRepositoryMock.Object, null,null,null,null,null);

            templateRepositoryMock.Setup(repo => repo.GetTemplatesIdList()).ReturnsAsync(expectedTemplateIds);

            // Act
            var result = await templateService.GetTemplatesIdList();

            // Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<IEnumerable<int>>(result);
            Assert.Equal(expectedTemplateIds, result);
        }
    }
}
