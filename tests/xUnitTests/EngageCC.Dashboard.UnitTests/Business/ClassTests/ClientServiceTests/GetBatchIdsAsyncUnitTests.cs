﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.Interfaces;
using Dashboard.Models.Clients;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.ClientServiceTests
{
    public class GetBatchIdsAsyncUnitTests
    {
        private readonly Mock<IClientRepository> _clientRepositoryMock;
        private readonly Mock<IMapper> _mapperMock;
        private readonly ClientService _clientService;
        private readonly Fixture _fixture;

        public GetBatchIdsAsyncUnitTests()
        {
            _clientRepositoryMock = new Mock<IClientRepository>();
            _mapperMock = new Mock<IMapper>();
            _clientService = new ClientService(_clientRepositoryMock.Object, _mapperMock.Object,null,null,null,null);
            _fixture = new Fixture();
        }

        [Fact]
        public async Task GetBatchIdsAsync_ShouldReturnBatchIds()
        {
            // Arrange
            var expectedBatchIds = _fixture.Create<List<BatchIdsResponse>>();

            _clientRepositoryMock.Setup(repo => repo.GetBatchIdsAsync())
                .ReturnsAsync(expectedBatchIds);

            // Act
            var result = await _clientService.GetBatchIdsAsync();

            // Assert
            Assert.NotNull(result);
            Assert.Equal(expectedBatchIds, result);
        }
    }
}
