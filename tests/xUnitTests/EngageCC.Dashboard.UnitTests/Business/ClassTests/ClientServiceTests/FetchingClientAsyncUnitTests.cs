﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.Clients;
using Dashboard.Models.Fields;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.ClientServiceTests
{
    public class FetchingClientAsyncUnitTests
    {
        private readonly Mock<IClientRepository> _clientRepositoryMock;
        private readonly Mock<IMapper> _mapperMock;
        private readonly ClientService _clientService;

        public FetchingClientAsyncUnitTests()
        {
            _clientRepositoryMock = new Mock<IClientRepository>();
            _mapperMock = new Mock<IMapper>();
            _clientService = new ClientService(_clientRepositoryMock.Object, _mapperMock.Object,null,null,null,null);
        }

        [Fact]
        public async Task FetchingClientAsync_WhenRepositoryThrowsException_ShouldPropagateException()
        {
            // Arrange
            var partnerRequest = new PartnerRequest();

            // Mock the repository to throw an exception
            _clientRepositoryMock.Setup(repo => repo.FetchingClientAsync(partnerRequest))
                .ThrowsAsync(new Exception("Test Exception"));

            // Act and Assert
            await Assert.ThrowsAsync<Exception>(() => _clientService.FetchingClientAsync(partnerRequest));
        }


        //[Fact]
        //public async Task FetchingClientAsync_ValidRequest_ReturnsLetterFieldValuesList()
        //{
        //    // Arrange
        //    var partnerRequest = new PartnerRequest();
        //    var letterFieldValuesBase = new LetterFieldValuesBase();
        //    var bulkLettersResponse = new List<BulkLettersResponse>();
        //    var expectedLetterFieldValuesList = new LetterFieldValuesList();

        //    _clientRepositoryMock.Setup(repo => repo.FetchingClientAsync(partnerRequest))
        //        .ReturnsAsync(letterFieldValuesBase);

        //    _clientRepositoryMock.Setup(repo => repo.CheckExistingBatchId(It.IsAny<int>()))
        //        .ReturnsAsync(new BulkLetters());

        //    _clientRepositoryMock
        //    .Setup(repo => repo.SaveFetchingLetterAsync(It.IsAny<List<BulkLetters>>(), It.IsAny<int>()))
        //    .ReturnsAsync((List<BulkLetters> bulkLetters, int batchId) => bulkLetters);

        //    // Act
        //    var result = await _clientService.FetchingClientAsync(partnerRequest);

        //    // Assert
        //    Assert.NotNull(result);
        //}


        //[Fact]
        //public async Task FetchingClientAsync_ShouldReturnExpectedResult()
        //{
        //    // Arrange
        //    var partnerRequest = new PartnerRequest
        //    {
        //        PartnersName = new List<PartnersViewModel>
        //{
        //    new PartnersViewModel { PartnerId = "1", PartnerName = "Partner1" },
        //    new PartnersViewModel { PartnerId = "2", PartnerName = "Partner2" }
        //},
        //        TemplateFieldNames = new List<TemplateFields>
        //{
        //    new TemplateFields { FieldName = "Field1" },
        //    new TemplateFields { FieldName = "Field2" }
        //},
        //        Admin = "AdminUser",
        //        TemplateName = "Template123",
        //        BatchId = 123,
        //        DocumentStatusId = 456,
        //        FieldJson = "SampleFieldJson",
        //        IsNewBulk = true
        //    };

        //    var expectedResponse = new LetterFieldValuesList
        //    {
        //        ClientIdErrorList = "ErrorList",
        //        GetCurrentDate = "2023-10-31",
        //        BulkLettersResponse = new List<BulkLettersResponse>
        //{
        //    new BulkLettersResponse
        //    {
        //        BulkLettersId = 1,
        //        ClientId = 101,
        //        PartnerId = 201,
        //        ClientName = "ClientName1",
        //        OfficeId = 301,
        //        Office = "Office1",
        //        SignatoryEmailId = "Email1",
        //        SignatoryFirstName = "FirstName1",
        //        SignatoryLastName = "LastName1",
        //        SignatoryTitle = "Title1",
        //        SpouseEmail = "SpouseEmail1",
        //        SpouseLastName = "SpouseLastName1",
        //        PartnerName = "Partner1",
        //        BatchId = 123,
        //        TemplateName = "Template123",
        //        FieldJson = "SampleFieldJson",
        //        DocumentStatusName = "DocumentStatus1",
        //        IsDraft = true,
        //        IsDeleted = false,
        //        IsUpdated = false,
        //        CreatedOn = DateTime.Now,
        //        ModifiedOn = null,
        //        CreatedBy = "AdminUser",
        //        ModifiedBy = null,
        //        DeletedOn = null,
        //        DeletedBy = null,
        //        Is7216Available = false,
        //        ClientSignatureCount = 0,
        //        IsNewClient = false,
        //        IsEsigning = null,
        //        TaxYear = "2023",
        //        IsBatchActive = false,
        //        IsBatchDelete = false,
        //        OfficePhoneNumber = "PhoneNumber1",
        //        OfficeZipCode = "ZipCode1",
        //        OfficeState = "State1",
        //        OfficeCity = "City1",
        //        OfficeAddress = "Address1",
        //        Date = "2023-10-31",
        //        Address = "Address1",
        //        City = "City1",
        //        State = "State1",
        //        Zip = "Zip1",
        //        Jurisdiction = "Jurisdiction1",
        //    }
        //},

        //    };


        //    _clientRepositoryMock.Setup(repo => repo.FetchingClientAsync(partnerRequest))
        //        .ReturnsAsync(new LetterFieldValuesBase
        //        {
        //            BulkLettersResponse = expectedResponse.BulkLettersResponse
        //        });

        //    _clientRepositoryMock.Setup(repo => repo.CheckExistingBatchId(It.IsAny<int>()))
        //        .ReturnsAsync(new BulkLetters());

        //    _clientRepositoryMock.Setup(repo => repo.SaveFetchingLetterAsync(It.IsAny<List<BulkLetters>>(), It.IsAny<int>()))
        //        .ReturnsAsync(new List<BulkLetters>());

        //    _mapperMock.Setup(mapper => mapper.Map<IEnumerable<BulkLetters>>(It.IsAny<IEnumerable<BulkLettersResponse>>()))
        //        .Returns(new List<BulkLetters>());

        //    // Act
        //    var result = await _clientService.FetchingClientAsync(partnerRequest);

        //    // Assert
        //    Assert.NotNull(result);
        //    Assert.Equal(expectedResponse.BulkLettersResponse, result.BulkLettersResponse);
        //    Assert.Equal(expectedResponse.ClientIdErrorList, result.ClientIdErrorList);
        //    Assert.Equal(expectedResponse.GetCurrentDate, result.GetCurrentDate);

        //}



        //[Fact]
        //public async Task FetchingClientAsync_WhenPartnerRequestIsNull_ShouldThrowArgumentNullException()
        //{
        //    // Arrange
        //    PartnerRequest partnerRequest = null;

        //    // Act and Assert
        //    await Assert.ThrowsAsync<ArgumentNullException>(() => _clientService.FetchingClientAsync(partnerRequest));
        //}

        //[Fact]
        //public async Task FetchingClientAsync_WhenDuplicatePartnersExist_ShouldSetDuplicatePartnersInResponse()
        //{
        //    // Arrange
        //    var partnerRequest = new PartnerRequest();
        //    var duplicateData = new List<DuplicatePartners> { new DuplicatePartners() };
        //    _clientRepositoryMock.Setup(repo => repo.FetchingClientAsync(partnerRequest))
        //        .ReturnsAsync(new LetterFieldValuesBase { DuplicatePartners = duplicateData });

        //    // Act
        //    var result = await _clientService.FetchingClientAsync(partnerRequest);

        //    // Assert
        //    Assert.NotNull(result);
        //    Assert.NotNull(result.DuplicatePartners);
        //    Assert.Equal(duplicateData, result.DuplicatePartners);
        //}

        //[Fact]
        //public async Task FetchingClientAsync_WhenSomeBulkLettersFound_ShouldReturnResponseWithBulkLetters()
        //{
        //    // Arrange
        //    var partnerRequest = new PartnerRequest();
        //    var bulkLettersResponse = new List<BulkLettersResponse> { new BulkLettersResponse() };
        //    _clientRepositoryMock.Setup(repo => repo.FetchingClientAsync(partnerRequest))
        //        .ReturnsAsync(new LetterFieldValuesBase { BulkLettersResponse = bulkLettersResponse });

        //    // Act
        //    var result = await _clientService.FetchingClientAsync(partnerRequest);

        //    // Assert
        //    Assert.NotNull(result);
        //    Assert.NotNull(result.BulkLettersResponse);
        //    Assert.Equal(bulkLettersResponse, result.BulkLettersResponse);
        //}

    }
}
