﻿using AutoFixture;
using Dashboard.Business.Services;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.ClientServiceTests
{
    public class GetPartnersNameAsyncUnitTests
    {
        [Fact]
        public async Task GetPartnersNameAsync_ShouldReturnPartners()
        {
            // Arrange
            var fixture = new Fixture();
            var partners = fixture.Create<List<t_engletterdata>>();

            var clientRepositoryMock = new Mock<IClientRepository>();
            clientRepositoryMock.Setup(repo => repo.GetPartnersNameAsync())
                               .ReturnsAsync(partners);

            var clientService = new ClientService(clientRepositoryMock.Object, null, null, null, null, null);

            // Act
            var result = await clientService.GetPartnersNameAsync();

            // Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<IEnumerable<t_engletterdata>>(result);
            Assert.Equal(partners, result);
        }
    }
}
