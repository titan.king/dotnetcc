﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.Interfaces;
using Dashboard.Models.BulkLetterGeneration;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.ClientServiceTests
{
    public class GetBatchLettersByIdAsyncUnitTests
    {
        private readonly Mock<IClientRepository> _clientRepositoryMock;
        private readonly Mock<IMapper> _mapperMock;
        private readonly ClientService _clientService;
        private readonly Fixture _fixture;

        public GetBatchLettersByIdAsyncUnitTests()
        {
            _clientRepositoryMock = new Mock<IClientRepository>();
            _mapperMock = new Mock<IMapper>();
            _clientService = new ClientService(_clientRepositoryMock.Object, _mapperMock.Object, null, null, null,null);
            _fixture = new Fixture();
        }

        [Fact]
        public async Task GetBatchLettersByIdAsync_ShouldReturnBatchLetter()
        {
            // Arrange
            var batchId = _fixture.Create<int>();
            var expectedBatchLetter = _fixture.Create<BulkDraftLetterResponse>();

            _clientRepositoryMock.Setup(repo => repo.GetBatchLettersByIdAsync(batchId))
                .ReturnsAsync(expectedBatchLetter);

            // Act
            var result = await _clientService.GetBatchLettersByIdAsync(batchId);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(expectedBatchLetter, result);
        }
    }
}
