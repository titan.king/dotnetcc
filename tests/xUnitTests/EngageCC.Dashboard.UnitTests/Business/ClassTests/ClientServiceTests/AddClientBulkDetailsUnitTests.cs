﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.Interfaces;
using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Fields;
using Engage3.Common.Business;
using Engage3.Common.Business.Interfaces;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.ClientServiceTests
{
    public class AddClientBulkDetailsUnitTests
    {

        private readonly Mock<IClientRepository> _clientRepositoryMock;
        private readonly Mock<IMapper> _mapperMock;
        private readonly ClientService _clientService;
        private readonly Fixture _fixture;
        private readonly Mock<IGetCurrentDatetime> _getCurrentDateTime;

        public AddClientBulkDetailsUnitTests()
        {
            _clientRepositoryMock = new Mock<IClientRepository>();
            _mapperMock = new Mock<IMapper>();
            _getCurrentDateTime = new Mock<IGetCurrentDatetime>();
            _clientService = new ClientService(_clientRepositoryMock.Object, _mapperMock.Object, null, null, null, _getCurrentDateTime.Object);
            _fixture = new Fixture();
        }

        [Fact]
        public async Task AddClientBulkDetails_ValidBulkLetters_ShouldReturnSuccessResponse()
        {
            // Arrange
            var bulkLettersResponse = _fixture.Create<BulkLettersResponse>();
            var clientId = _fixture.Create<int>();
            bulkLettersResponse.ClientId = clientId;
            
            _getCurrentDateTime.Setup(mock => mock.GetCurrentEstDatetime())
.ReturnsAsync(DateTime.Now);


            _clientRepositoryMock.Setup(repo => repo.AddFieldIdtoList(It.IsAny<List<LetterFieldValuesResponse>>()))
                .ReturnsAsync(new List<LetterFieldValuesResponse>
                {
        new LetterFieldValuesResponse { FieldName = "SignatoryTitle", FieldValue = "Mr." },
        new LetterFieldValuesResponse { FieldName = "SignatoryLastName", FieldValue = "Doe" },
                    // Add more fields as needed
                });

            _clientRepositoryMock.Setup(repo => repo.GetAddClientData(It.IsAny<string>()))
                .ReturnsAsync((2, true));

            _clientRepositoryMock.Setup(repo => repo.IsNotExistingClientId(It.IsAny<int>(), It.IsAny<int>()))
                .ReturnsAsync(true);

            _clientRepositoryMock.Setup(repo => repo.IsNotExistingClientName(It.IsAny<string>(), It.IsAny<int>()))
                .ReturnsAsync(true);

            _clientRepositoryMock.Setup(repo => repo.AddClientBulkDetails(It.IsAny<BulkLettersResponse>()))
                .ReturnsAsync(new BulkLettersResponse());

            // Act
            var result = await _clientService.AddClientBulkDetails(bulkLettersResponse);

            // Assert
            Assert.NotNull(result);

        }
    }
}
