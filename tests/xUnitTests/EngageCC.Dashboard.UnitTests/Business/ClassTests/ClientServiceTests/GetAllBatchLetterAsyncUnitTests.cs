﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.Interfaces;
using Dashboard.Models.BulkLetterGeneration;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.ClientServiceTests
{
    public class GetAllBatchLetterAsyncUnitTests
    {
        private readonly Mock<IClientRepository> _clientRepositoryMock;
        private readonly Mock<IMapper> _mapperMock;
        private readonly ClientService _clientService;
        private readonly Fixture _fixture;

        public GetAllBatchLetterAsyncUnitTests()
        {
            _clientRepositoryMock = new Mock<IClientRepository>();
            _mapperMock = new Mock<IMapper>();
            _clientService = new ClientService(_clientRepositoryMock.Object, _mapperMock.Object,null,null,null,null);
            _fixture = new Fixture();
        }

        [Fact]
        public async Task GetAllBatchLetterAsync_ShouldReturnBatchLetters()
        {
            // Arrange
            var expectedBatchLetters = _fixture.Create<List<BulkLetterBatchResponse>>();

            _clientRepositoryMock.Setup(repo => repo.GetAllBatchLetterAsync())
                .ReturnsAsync(expectedBatchLetters);

            // Act
            var result = await _clientService.GetAllBatchLetterAsync();

            // Assert
            Assert.NotNull(result);
            Assert.Equal(expectedBatchLetters, result);
        }
    }
}
