﻿using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.BlockServiceTests
{
    public class GetBlocksByIdUnitTests
    {

        [Fact]
        public async Task GetBlocksById_ValidId_ShouldReturnBlocksList()
        {
            // Arrange
            int blockId = 123131;
            var expectedBlocksList = new BlocksCommonModel()
            {
                BlocksById = new List<BlocksList>() { new BlocksList() { BlockId = blockId, BlockName = "EERERER" } },
                ConnectedTemplate = new List<TemplateListResponseModel>()
            };

            var blockRepositoryMock = new Mock<IBlocksRepository>();
            blockRepositoryMock.Setup(br => br.GetBlocksById(blockId)).ReturnsAsync(expectedBlocksList);
            var mapperMock = new Mock<IMapper>();
            var blockService = new BlocksService(blockRepositoryMock.Object, mapperMock.Object);

            // Act
            var result = await blockService.GetBlocksById(blockId);

            // Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<List<BlocksList>>(result);
        }
    }
}
