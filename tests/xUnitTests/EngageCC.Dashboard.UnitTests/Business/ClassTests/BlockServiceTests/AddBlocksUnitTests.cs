﻿using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Data.Repositories;
using Dashboard.Models.ResponseModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.BlockServiceTests
{
    public class AddBlocksUnitTests
    {
        [Fact]
        public async Task AddBlocks_ValidInput_ShouldReturnExpectedTuple()
        {
            // Arrange
            var mapperMock = new Mock<IMapper>();
            var blockRepositoryMock = new Mock<IBlocksRepository>();
            var blockService = new BlocksService(blockRepositoryMock.Object, mapperMock.Object);
            var testBlocks = new BlocksResponseModel() { BlockId = 121212, BlockName = "BALOON", IsDeleted = false, StatusId = 1, CreatedOn = DateTime.Now };
            var isValidResult = Tuple.Create(true, "Validation successful");
            var blockToAdd = new Block();
            var addedBlock = new Block();
            blockRepositoryMock.Setup(m => m.IsValidBlockName(testBlocks)).ReturnsAsync(isValidResult.Item1);
            mapperMock.Setup(m => m.Map<Block>(testBlocks)).Returns(blockToAdd);
            mapperMock.Setup(m => m.Map<BlocksResponseModel>(addedBlock)).Returns(new BlocksResponseModel());
            blockRepositoryMock.Setup(br => br.AddBlocks(blockToAdd)).ReturnsAsync(addedBlock);

            // Act
            var result = await blockService.AddBlocks(testBlocks);

            // Assert
            Assert.NotNull(result);
            Assert.IsType<Tuple<BlocksResponseModel, string>>(result);
        }

        [Fact]
        public async Task AddBlocks_InvalidValidInput_ShouldReturnEmptyModelWithTuple()
        {
            // Arrange
            var mapperMock = new Mock<IMapper>();
            var blockRepositoryMock = new Mock<IBlocksRepository>();
            var blockService = new BlocksService(blockRepositoryMock.Object, mapperMock.Object);
            var testBlocks = new BlocksResponseModel();
            var isValidResult = Tuple.Create(false, "Validation unsuccessful");
            var blockToAdd = new Block();
            var addedBlock = new Block();
            blockRepositoryMock.Setup(m => m.IsValidBlockName(testBlocks)).ReturnsAsync(isValidResult.Item1);
            mapperMock.Setup(m => m.Map<Block>(testBlocks)).Returns(blockToAdd);
            mapperMock.Setup(m => m.Map<BlocksResponseModel>(addedBlock)).Returns(new BlocksResponseModel());
            blockRepositoryMock.Setup(br => br.AddBlocks(blockToAdd)).ReturnsAsync(addedBlock);

            // Act
            var result = await blockService.AddBlocks(testBlocks);

            // Assert
            Assert.NotNull(result);
            Assert.IsType<Tuple<BlocksResponseModel, string>>(result);
        }
    }
}
