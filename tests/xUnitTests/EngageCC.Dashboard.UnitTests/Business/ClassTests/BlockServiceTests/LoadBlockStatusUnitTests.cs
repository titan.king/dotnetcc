﻿using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.BlockServiceTests
{
    public class LoadBlockStatusUnitTests
    {
        [Fact]
        public async Task Load_ShouldReturnBlockStatusListResModel()
        {
            // Arrange
            var expectedBlockStatusListResModel = new BlockStatusListResModel();

            var mapperMock = new Mock<IMapper>();
            var blockRepositoryMock = new Mock<IBlocksRepository>();
            var blockService = new BlocksService(blockRepositoryMock.Object, mapperMock.Object);
            blockRepositoryMock.Setup(br => br.Load()).ReturnsAsync(expectedBlockStatusListResModel);

            // Act
            var result = await blockService.Load();

            // Assert
            Assert.NotNull(result);
            Assert.IsType<BlockStatusListResModel>(result);
        }
    }
}
