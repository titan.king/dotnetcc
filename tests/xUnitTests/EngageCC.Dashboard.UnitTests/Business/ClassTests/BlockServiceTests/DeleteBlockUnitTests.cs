﻿using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.BlockServiceTests
{
    public class DeleteBlockUnitTests
    {
        [Fact]
        public async Task DeleteBlock_ValidInput_ShouldReturnExpectedResult()
        {
            // Arrange
            var mapperMock = new Mock<IMapper>();
            var blockRepositoryMock = new Mock<IBlocksRepository>();
            var blockService = new BlocksService(blockRepositoryMock.Object, mapperMock.Object);
            int blockId = 24234234;
            var testDeleteBlockResModel = new DeleteBlockResModel() { 
            BlockId = new int[]{ blockId }
            }; 
            var blocksList = new BlocksCommonModel()
            {
                BlocksById = new List<BlocksList>() { new BlocksList() { BlockId = blockId, BlockName = "FDFFF" } },
                ConnectedTemplate = new List<TemplateListResponseModel>()
            };
            
            blockRepositoryMock.Setup(br => br.GetBlocksById(blockId)).ReturnsAsync(blocksList); 
            mapperMock.Setup(m => m.Map<DeleteBlockResModel>(testDeleteBlockResModel)).Returns(testDeleteBlockResModel); 
            blockRepositoryMock.Setup(br => br.DeleteBlock(testDeleteBlockResModel)).ReturnsAsync(true);

            // Act
            var result = await blockService.DeleteBlock(testDeleteBlockResModel);

            // Assert 
            Assert.NotNull(result); 
        }
    }
}
