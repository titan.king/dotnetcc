﻿using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.BlockServiceTests
{
    public class GetBlocksUnitTests
    {
        [Fact]
        public async Task GetBlocks_ShouldReturnListOfBlocks()
        {
            // Arrange
            var expectedBlocks = new List<BlocksList>();

            var mapperMock = new Mock<IMapper>();
            var blockRepositoryMock = new Mock<IBlocksRepository>();
            var blockService = new BlocksService(blockRepositoryMock.Object, mapperMock.Object);
            blockRepositoryMock.Setup(br => br.GetBlocks()).ReturnsAsync(expectedBlocks);

            // Act
            var result = await blockService.GetBlocks();

            // Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<IEnumerable<BlocksList>>(result);
            Assert.Equal(expectedBlocks, result);
        }
    }
}
