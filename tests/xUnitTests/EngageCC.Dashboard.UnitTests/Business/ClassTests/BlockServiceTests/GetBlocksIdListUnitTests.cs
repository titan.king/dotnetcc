﻿using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.BlockServiceTests
{
    public class GetBlocksIdListUnitTests
    {
        [Fact]
        public async Task GetBlocksIdList_ShouldReturnListOfBlockIdListResponseModel()
        {
            // Arrange
            var expectedBlockIdList = new List<BlockIdListResponseModel>();

            var mapperMock = new Mock<IMapper>();
            var blockRepositoryMock = new Mock<IBlocksRepository>();
            var blockService = new BlocksService(blockRepositoryMock.Object, mapperMock.Object);
            blockRepositoryMock.Setup(br => br.GetBlocksIdList()).ReturnsAsync(expectedBlockIdList);
             

            // Act
            var result = await blockService.GetBlocksIdList();

            // Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<IEnumerable<BlockIdListResponseModel>>(result); 
        }
    }
}
