﻿using AutoMapper;
using Dashboard.Business.Services;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.ClassTests.BlockServiceTests
{
    public class GetActiveBlocksListUnitTests
    {
        [Fact]
        public async Task GetActiveBlocksList_ShouldReturnListOfActiveBlocks()
        {
            // Arrange
            var expectedBlocksList = new List<ActiveBlocksList>();

            var blockRepositoryMock = new Mock<IBlocksRepository>();
            blockRepositoryMock.Setup(br => br.GetActiveBlocksList()).ReturnsAsync(expectedBlocksList);
            var mapperMock = new Mock<IMapper>();
            var blockService = new BlocksService(blockRepositoryMock.Object, mapperMock.Object);

            // Act
            var result = await blockService.GetActiveBlocksList();

            // Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<IEnumerable<ActiveBlocksList>>(result);
            Assert.Equal(expectedBlocksList, result);
        }
    }
}
