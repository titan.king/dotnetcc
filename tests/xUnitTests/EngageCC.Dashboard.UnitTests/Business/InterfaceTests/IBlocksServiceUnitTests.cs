﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Business.Services;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.Business.InterfaceTests
{
    public class IBlocksServiceUnitTests
    {
        private readonly Fixture _fixture;
        private readonly Mock<IBlocksRepository> _mockBlockRepository;
        private readonly Mock<IMapper> _mockMapper;
        private readonly BlocksService _blocksService;

        public IBlocksServiceUnitTests()
        {
            _fixture = new Fixture();
            _mockBlockRepository = new Mock<IBlocksRepository>();
            _mockMapper = new Mock<IMapper>();
            _blocksService = new BlocksService(_mockBlockRepository.Object, _mockMapper.Object);
        }

        [Fact]
        public async Task AddBlocks_ShouldReturnTuple()
        {
            // Arrange
            var mockBlockRepository = new Mock<IBlocksRepository>();
            var mockmapper = new Mock<IMapper>();
            var blocksService = new BlocksService(mockBlockRepository.Object, mockmapper.Object);
            var testBlockResponsemodel = new Mock<BlocksResponseModel>();
            var testBlock = new Mock<Block>();
            var blocker = mockmapper.Setup(m => m.Map<Block>(It.IsAny<BlocksResponseModel>())).Returns(testBlock.Object);
            mockmapper.Setup(m => m.Map<BlocksResponseModel>(It.IsAny<Block>())).Returns(testBlockResponsemodel.Object);

            mockBlockRepository.Setup(r => r.IsValidBlockName(testBlockResponsemodel.Object)).ReturnsAsync(true);
            mockBlockRepository.Setup(r => r.AddBlocks(testBlock.Object)).ReturnsAsync(testBlock.Object);

            // Act
            var result = await blocksService.AddBlocks(testBlockResponsemodel.Object);

            // Assert
            Assert.NotNull(result);
            Assert.IsType<Tuple<BlocksResponseModel, string>>(result);
        }


        [Fact]
        public async Task AddBlocks_InvalidBlock_ShouldReturnErrorMessage()
        {
            // Arrange
            var testBlocksResponseModel = _fixture.Create<BlocksResponseModel>();
            var expectedErrorMessage = "Block Name already exists.";

            _mockMapper.Setup(m => m.Map<Block>(It.IsAny<BlocksResponseModel>())).Returns((Block)null);  
            _mockBlockRepository.Setup(r => r.IsValidBlockName(testBlocksResponseModel)).ReturnsAsync(false); 

            // Act
            var result = await _blocksService.AddBlocks(testBlocksResponseModel);

            // Assert
            Assert.NotNull(result);
            Assert.IsType<Tuple<BlocksResponseModel, string>>(result);
            Assert.True(string.IsNullOrEmpty(result.Item1.BlockName));
            Assert.True(string.IsNullOrEmpty(result.Item1.Content));
            Assert.True(string.IsNullOrEmpty(result.Item1.ConnectedTemplates));
            Assert.Equal(expectedErrorMessage, result.Item2);
        }

        [Fact]
        public async Task UpdateBlocks_ShouldReturnTuple()
        {
            // Arrange
            var testBlocksResponseModel = _fixture.Create<BlocksResponseModel>();
            var block = _fixture.Create<Block>();

            _mockMapper.Setup(m => m.Map<Block>(It.IsAny<BlocksResponseModel>())).Returns(block);
            _mockMapper.Setup(m => m.Map<BlocksResponseModel>(It.IsAny<Block>())).Returns(testBlocksResponseModel);

            _mockBlockRepository.Setup(r => r.IsValidBlockName(testBlocksResponseModel)).ReturnsAsync(true);
            _mockBlockRepository.Setup(r => r.UpdateBlocks(It.IsAny<Block>())).ReturnsAsync(block);

            // Act
            var result = await _blocksService.UpdateBlocks(testBlocksResponseModel);

            // Assert
            Assert.NotNull(result);
            Assert.IsType<Tuple<BlocksResponseModel, string>>(result);
            Assert.Equal(testBlocksResponseModel, result.Item1);
            Assert.Equal(string.Empty, result.Item2);
        }

        [Fact]
        public async Task UpdateBlocks_InvalidBlock_ShouldReturnErrorMessage()
        {
            // Arrange
            var testBlocksResponseModel = _fixture.Create<BlocksResponseModel>();
            var expectedErrorMessage = "Block Name already exists.";

            _mockMapper.Setup(m => m.Map<Block>(It.IsAny<BlocksResponseModel>())).Returns((Block)null); 
            _mockBlockRepository.Setup(r => r.IsValidBlockName(testBlocksResponseModel)).ReturnsAsync(false);  
            _mockBlockRepository.Setup(r => r.UpdateBlocks(It.IsAny<Block>())).ReturnsAsync((Block)null);  

            // Act
            var result = await _blocksService.UpdateBlocks(testBlocksResponseModel);

            // Assert
            Assert.NotNull(result);
            Assert.IsType<Tuple<BlocksResponseModel, string>>(result);
            Assert.True(string.IsNullOrEmpty(result.Item1.BlockName));
            Assert.True(string.IsNullOrEmpty(result.Item1.Content));
            Assert.True(string.IsNullOrEmpty(result.Item1.ConnectedTemplates));
            Assert.Equal(expectedErrorMessage, result.Item2);
        }

        [Fact]
        public async Task GetBlocksIdList_ShouldReturnListOfBlockIds()
        {
            // Arrange
            var blockIdList = _fixture.CreateMany<int>().ToList();
            var mappedBlockIdList = _fixture.CreateMany<BlockIdListResponseModel>().ToList();

            _mockBlockRepository.Setup(r => r.GetBlocksIdList()).ReturnsAsync(mappedBlockIdList);
            _mockMapper.Setup(m => m.Map<IEnumerable<BlockIdListResponseModel>>(It.IsAny<IEnumerable<int>>()))
                .Returns(mappedBlockIdList);

            // Act
            var result = await _blocksService.GetBlocksIdList();

            // Assert
            Assert.NotNull(result);
            Assert.IsType<List<BlockIdListResponseModel>>(result);
            Assert.Equal(mappedBlockIdList.Count, result.Count());
        }

        [Fact]
        public async Task GetBlocksIdList_EmptyList_ShouldReturnEmptyList()
        {
            // Arrange
            _mockBlockRepository.Setup(r => r.GetBlocksIdList()).ReturnsAsync(new List<BlockIdListResponseModel>());

            // Act
            var result = await _blocksService.GetBlocksIdList();

            // Assert
            Assert.NotNull(result);
            Assert.IsType<List<BlockIdListResponseModel>>(result);
            Assert.Empty(result);
        }

        [Fact]
        public async Task GetBlocks_ShouldReturnBlocksList()
        {
            // Arrange
            var blocksListFromRepository = _fixture.CreateMany<BlocksList>().ToList();
            var expectedBlocksList = _fixture.CreateMany<BlocksList>().ToList();

            _mockBlockRepository.Setup(r => r.GetBlocks()).Returns(Task.FromResult<IEnumerable<BlocksList>>(blocksListFromRepository));  

            _mockMapper.Setup(m => m.Map<List<BlocksList>>(It.IsAny<List<BlocksList>>())).Returns(expectedBlocksList);

            // Act
            var result = await _blocksService.GetBlocks();

            // Assert
            Assert.NotNull(result);
            Assert.IsType<List<BlocksList>>(result);
        }

        [Fact]
        public async Task GetBlocks_Fails_ShouldReturnEmptyList()
        {
            // Arrange
            _mockBlockRepository.Setup(r => r.GetBlocks()).ReturnsAsync((IEnumerable<BlocksList>)null);  

            // Act
            var result = await _blocksService.GetBlocks();

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public async Task GetActiveBlocksList_ShouldReturnBlocksList()
        {
            // Arrange
            var blocksListFromRepository = _fixture.CreateMany<ActiveBlocksList>().ToList();
            var expectedActiveBlocksList = _fixture.CreateMany<ActiveBlocksList>().ToList();

            _mockBlockRepository.Setup(r => r.GetActiveBlocksList()).Returns(Task.FromResult<IEnumerable<ActiveBlocksList>>(blocksListFromRepository));  // Correcting the setup

            _mockMapper.Setup(m => m.Map<List<ActiveBlocksList>>(It.IsAny<List<ActiveBlocksList>>())).Returns(expectedActiveBlocksList);

            // Act
            var result = await _blocksService.GetActiveBlocksList();

            // Assert
            Assert.NotNull(result);
            Assert.IsType<List<ActiveBlocksList>>(result);

        }

        [Fact]
        public async Task GetActiveBlocksList_Fails_ShouldReturnEmptyList()
        {
            // Arrange
            _mockBlockRepository.Setup(r => r.GetActiveBlocksList()).ReturnsAsync((IEnumerable<ActiveBlocksList>)null);  

            // Act
            var result = await _blocksService.GetActiveBlocksList();

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public async Task Load_ShouldReturnBlockStatusListResModel()
        {
            // Arrange
            var expectedBlockStatusListResModel = _fixture.Create<BlockStatusListResModel>();

            _mockBlockRepository.Setup(r => r.Load()).ReturnsAsync(expectedBlockStatusListResModel);
            _mockMapper.Setup(m => m.Map<BlockStatusListResModel>(It.IsAny<BlockStatusListResModel>())).Returns(expectedBlockStatusListResModel);

            // Act
            var result = await _blocksService.Load();

            // Assert
            Assert.NotNull(result);
            Assert.IsType<BlockStatusListResModel>(result);
        }

        [Fact]
        public async Task Load_Fails_ShouldReturnNull()
        {
            // Arrange
            _mockBlockRepository.Setup(r => r.Load()).ReturnsAsync((BlockStatusListResModel)null); 

            // Act
            var result = await _blocksService.Load();

            // Assert
            Assert.Null(result);  
        }


    }
}
