﻿using Dashboard.Data.Context;
using Microsoft.EntityFrameworkCore;
using Xunit;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests
{
    public class DashboardDBContextUnitTests
    {
        private readonly string _databaseName = "Test_DashboardDB";

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateAzureGroupDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.AzureGroup);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateEngageTypeDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.EngageType);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateBatchRequestDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.BatchRequest);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateBlockDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.Block);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateBulkLetterAttachmentsDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.BulkLetterAttachments);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateBulkLettersDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.BulkLetters);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateClientFieldsDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.ClientFields);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateColumnNamesDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.ColumnNames);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateDepartmentDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.Department);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateDocumentStatusDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.DocumentStatus);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateEmailSettingsDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.EmailSettings);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateEmailTemplateDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.EmailTemplate);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateEngagementLetterDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.EngagementLetter);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateFieldDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.Field);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateFieldDataTypesDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.FieldDataTypes);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateHistoryLogDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.HistoryLog);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateJurisdictionDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.Jurisdiction);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateLetterFieldValuesDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.LetterFieldValues);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateLettersStatusReportsDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.LettersStatusReports);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateLoginInfoDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.LoginInfo);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateMasterTemplateDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.MasterTemplate);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateOfficeInfoDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.OfficeInfo);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateParentChildInfoDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.ParentChildInfo);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreatePartnerClientMappingDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.PartnerClientMapping);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreatePartnerOfficeInfoDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.PartnerOfficeInfo);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreatePartnersDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.Partner);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateRolePermissionMappingDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.RolePermissionMapping);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateRolesDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.Roles);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateScreensDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.Screens);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateSigningInfoDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.SigningInfo);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateStatusDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.Status);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateTemplateDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.Template);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateTemplateAttachmentsDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.TemplateAttachments);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateTemplateVersionDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.TemplateVersion);
            }
        }

        [Fact]
        public void DashboardDBContext_WhenInitialized_ShouldCreateUserDetailsDbSet()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: _databaseName)
                .Options;

            // Act
            using (var context = new DashboardDBContext(options))
            {
                // Assert
                Assert.NotNull(context.UserDetails);
            }
        }
    }
}
