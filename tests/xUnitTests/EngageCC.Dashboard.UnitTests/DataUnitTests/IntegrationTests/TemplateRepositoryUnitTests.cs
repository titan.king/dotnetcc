﻿using AutoMapper;
using Dashboard.Data.Context;
using Dashboard.Data.DataModel;
using Dashboard.Data.Repositories;
using Dashboard.Models.ResponseModel;
using Engage3.Common.Business.Interfaces;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.IntegrationTests
{
    public class TemplateRepositoryUnitTests
    {

        [Fact]
        public async Task SaveTemplate_WithValidTemplate_ShouldSaveTemplateAndRelatedData()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var mapperConfiguration = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<TemplateListModel, Template>();
                });

                IMapper mapper = mapperConfiguration.CreateMapper();
                var getCurrentDatetimeMock = new Mock<IGetCurrentDatetime>();
                getCurrentDatetimeMock.Setup(mock => mock.GetCurrentEstDatetime())
          .ReturnsAsync(DateTime.Now);
                var repository = new TemplateRepository(context, mapper, dbContextOptions, getCurrentDatetimeMock.Object);

                getCurrentDatetimeMock.Setup(mock => mock.GetCurrentEstDatetime())
            .ReturnsAsync(DateTime.Now);

                var template = new TemplateListModel
                {
                };

                // Act
                var result = await repository.SaveTemplate(template);

                // Assert
                Assert.NotNull(result);
                Assert.NotEqual(0, result.TemplateId);
                Assert.NotEqual(0, result.TemplateVersionId);
                Assert.Equal(1, context.Template.Count());
                Assert.Equal(1, context.TemplateVersion.Count());
                Assert.Equal(1, context.MasterTemplate.Count());
                Assert.Equal(1, context.TemplateAttachments.Count());
            }
        }

        [Fact]
        public async Task SaveTemplate_WithInvalidTemplate_ShouldNotSaveData()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var mapperConfiguration = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<TemplateListModel, Template>();
                });

                IMapper mapper = mapperConfiguration.CreateMapper();
                var repository = new TemplateRepository(context, mapper, dbContextOptions, null);

                var template = new TemplateListModel
                {
                    TemplateId = 1
                };

                // Act
                var result = await repository.SaveTemplate(template);

                // Assert
                Assert.NotNull(result);
                Assert.Equal(1, result.TemplateId);
            }
        }

        [Fact]
        public async Task SaveTemplateVersion_WithValidTemplate_ShouldSaveTemplateVersion()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new TemplateRepository(context, null, dbContextOptions, null);

                var template = new TemplateListModel
                {
                    TemplateId = 1,
                    TemplateLogic = "Sample Template Logic",
                };

                // Act
                var result = await repository.SaveTemplateVersion(template);

                // Assert
                Assert.NotNull(result);
                Assert.NotEqual(0, result.TemplateVersionId);
                Assert.Equal(1, context.TemplateVersion.Count());
            }
        }


        [Fact]
        public async Task SaveMasterTable_WithValidTemplate_ShouldSaveMasterTemplate()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new TemplateRepository(context, null, dbContextOptions, null);

                var template = new TemplateListModel
                {
                    TemplateId = 1,
                    TemplateHtml = "<html><body><h1>Sample Template</h1></body></html>",
                    TemplateName = "Sample Template",
                    TemplateVersionId = 1,
                };

                // Act
                var result = await repository.SaveMasterTable(template);

                // Assert
                Assert.NotNull(result);
                Assert.Equal(1, context.MasterTemplate.Count());
                Assert.Equal(1, result.TemplateId);
            }
        }


        [Fact]
        public async Task SaveAttachments_WithValidTemplate_ShouldSaveAttachments()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var getCurrentDatetimeMock = new Mock<IGetCurrentDatetime>();
                getCurrentDatetimeMock.Setup(mock => mock.GetCurrentEstDatetime())
          .ReturnsAsync(DateTime.Now);
                var repository = new TemplateRepository(context, null, dbContextOptions, getCurrentDatetimeMock.Object);

                var template = new TemplateListModel
                {
                    TemplateId = 1,
                    AttachmentJson = "{ \"attachment\": \"data\" }",
                };

                // Act
                var result = await repository.SaveAttachments(template);

                // Assert
                Assert.NotNull(result);
                Assert.Equal(1, result.TemplateId);
                Assert.Equal(1, context.TemplateAttachments.Count());
            }
        }

        [Fact]
        public async Task UpdateTemplate_WhenValidData_ShouldUpdateAndReturnUpdatedTemplate()
        {
            // Arrange
            var templateId = 1;
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                context.Template.Add(new Template
                {
                    TemplateId = templateId,
                });

                var existingData = new TemplateVersion
                {
                    TemplateId = 1,
                    TemplateLogic = "Sample Template Logic",
                    VersionNumber = 1
                };

                context.TemplateVersion.Add(existingData);
                context.SaveChanges();
            }

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var mapper = new Mapper(new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<TemplateListModel, Template>();
                }));

                var getCurrentDatetimeMock = new Mock<IGetCurrentDatetime>();
                getCurrentDatetimeMock.Setup(mock => mock.GetCurrentEstDatetime())
          .ReturnsAsync(DateTime.Now);
                var repository = new TemplateRepository(context, mapper, dbContextOptions, getCurrentDatetimeMock.Object);

                var updatedTemplate = new TemplateListModel
                {
                    TemplateId = templateId,
                    TemplateLogic = "Updated Template Logic"
                };

                // Act
                var result = await repository.UpdateTemplate(updatedTemplate);

                // Assert
                Assert.NotNull(result);
                Assert.Equal(templateId, result.TemplateId);
                Assert.Equal(updatedTemplate.TemplateLogic, result.TemplateLogic);

                var updatedEntity = context.Template.Find(templateId);
                Assert.NotNull(updatedEntity);
                context.Dispose();
            }
        }

        [Fact]
        public async Task UpdateTemplate_WhenInvalidData_ShouldNotUpdateAndReturnNull()
        {
            // Arrange
            var templateId = 0; // An invalid or non-existent TemplateId
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var mapper = new Mapper(new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<TemplateListModel, Template>();
                }));

                var repository = new TemplateRepository(context, mapper, dbContextOptions, null);

                var updatedTemplate = new TemplateListModel
                {
                    TemplateId = templateId,
                    TemplateLogic = "Updated Template Logic"
                };

                // Act
                var result = await repository.UpdateTemplate(updatedTemplate);

                // Assert
                Assert.Equal(0, result.TemplateId);
                var updatedEntity = context.Template.Find(templateId);
                Assert.Null(updatedEntity);
                context.Dispose();
            }
        }

        [Fact]
        public async Task UpdateTemplateVersion_WithValidTemplate_ShouldUpdateTemplateVersion()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var existingData = new TemplateVersion
                {
                    TemplateId = 1,
                    TemplateLogic = "Sample Template Logic",
                    VersionNumber = 1
                };

                context.TemplateVersion.Add(existingData);
                context.SaveChanges();

                var repository = new TemplateRepository(context, null, dbContextOptions, null);

                var template = new TemplateListModel
                {
                    TemplateId = 1,
                    TemplateLogic = "Updated Template Logic"
                };

                // Act
                var result = await repository.UpdateTemplateVersion(template);

                // Assert
                Assert.NotNull(result);
                Assert.NotEqual(0, result.TemplateVersionId);
                Assert.Equal(template.TemplateLogic, result.TemplateLogic);
            }
        }


        [Fact]
        public async Task UpdateTemplateVersion_WithInvalidTemplate_ShouldNotUpdate()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new TemplateRepository(context, null, dbContextOptions, null);

                var template = new TemplateListModel
                {
                    TemplateId = 0,
                    TemplateLogic = "Updated Template Logic"
                };

                // Act
                var result = await repository.UpdateTemplateVersion(template);

                // Assert
                Assert.NotNull(result);
                Assert.Equal(0, result.TemplateVersionId);
                var updatedTemplateVersion = await context.TemplateVersion.FirstOrDefaultAsync(tv => tv.TemplateId == template.TemplateId);
                Assert.Null(updatedTemplateVersion);
            }
        }

        [Fact]
        public async Task UpdateMasterTable_WithValidTemplate_ShouldUpdateMasterTable()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new TemplateRepository(context, null, dbContextOptions, null);

                var template = new TemplateListModel
                {
                    TemplateId = 1,
                    TemplateVersionId = 1,
                    TemplateHtml = "<html><body>Updated HTML</body></html>",
                    TemplateName = "Updated Template",
                };

                // Act
                var result = await repository.UpdateMasterTable(template);

                // Assert
                Assert.NotNull(result);

                var updatedMasterTemplate = context.MasterTemplate.Find(result.MasterTemplateId);
                Assert.NotNull(updatedMasterTemplate);
                Assert.Equal(template.TemplateId, updatedMasterTemplate.TemplateId);
                Assert.Equal(template.TemplateHtml, updatedMasterTemplate.Template);
                Assert.Equal(template.TemplateName, updatedMasterTemplate.TemplateName);
                Assert.Equal(template.TemplateVersionId, updatedMasterTemplate.TemplateVersionId);
            }
        }


        [Fact]
        public async Task UpdateMasterTable_WithInValidTemplate_ShouldNotUpdateMasterTable()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new TemplateRepository(context, null, dbContextOptions, null);

                var template = new TemplateListModel
                {
                    TemplateId = 0,
                    TemplateVersionId = 0,
                    TemplateHtml = null,
                    TemplateName = null
                };

                // Act
                var result = await repository.UpdateMasterTable(template);

                // Assert
                Assert.Equal(0, result.TemplateId);
                var updatedMasterTemplate = context.MasterTemplate.Find(template.TemplateId);
                Assert.Null(updatedMasterTemplate);
            }
        }


        [Fact]
        public async Task UpdateAttachments_WithValidTemplate_ShouldUpdateAttachments()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var getCurrentDatetimeMock = new Mock<IGetCurrentDatetime>();
                getCurrentDatetimeMock.Setup(mock => mock.GetCurrentEstDatetime())
          .ReturnsAsync(DateTime.Now);
                var repository = new TemplateRepository(context, null, dbContextOptions, getCurrentDatetimeMock.Object);

                var initialTemplateAttachment = new TemplateAttachments
                {
                    TemplateId = 1,
                    AttachmentJson = "Initial Attachment JSON",
                    CreatedOn = DateTime.Now.AddMinutes(-30),
                    ModifiedOn = DateTime.Now.AddMinutes(-30),
                };

                context.TemplateAttachments.Add(initialTemplateAttachment);
                context.SaveChanges();

                var template = new TemplateListModel
                {
                    TemplateId = 1,
                    AttachmentJson = "Updated Attachment JSON",
                };

                // Act
                var result = await repository.UpdateAttachments(template);

                // Assert
                Assert.NotNull(result);

                var updatedTemplateAttachment = context.TemplateAttachments.Find(result.TemplateAttachmentsId);
                Assert.NotNull(updatedTemplateAttachment);
                Assert.Equal(template.TemplateId, updatedTemplateAttachment.TemplateId);
                Assert.Equal(template.AttachmentJson, updatedTemplateAttachment.AttachmentJson);

            }
        }


        [Fact]
        public async Task UpdateAttachments_WithInValidTemplate_ShouldNotUpdateAttachments()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var getCurrentDatetimeMock = new Mock<IGetCurrentDatetime>();
                getCurrentDatetimeMock.Setup(mock => mock.GetCurrentEstDatetime())
          .ReturnsAsync(DateTime.Now);
                var repository = new TemplateRepository(context, null, dbContextOptions, getCurrentDatetimeMock.Object);

                var initialTemplateAttachment = new TemplateAttachments
                {
                    TemplateId = 0,
                    AttachmentJson = null,
                };

                context.TemplateAttachments.Add(initialTemplateAttachment);
                context.SaveChanges();

                var template = new TemplateListModel
                {
                    TemplateId = 0,
                    AttachmentJson = null,
                };

                // Act
                var result = await repository.UpdateAttachments(template);

                // Assert
                Assert.Equal(0,result.TemplateId);
                var updatedTemplateAttachment = context.TemplateAttachments.Find(template.TemplateId);
                Assert.Null(updatedTemplateAttachment);
            }
        }

        [Fact]
        public async Task GetDepartments_WithValidDepartments_ShouldReturnListOfDepartments()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new TemplateRepository(context, null, dbContextOptions, null);

                context.Department.AddRange(new List<Department>
            {
                new Department { DepartmentId = 1, DepartmentName = "Department1", IsDeleted = false },
                new Department { DepartmentId = 2, DepartmentName = "Department2", IsDeleted = false },
                new Department { DepartmentId = 3, DepartmentName = "Department3", IsDeleted = true },
            });
                context.SaveChanges();

                // Act
                var result = await repository.GetDepartments();

                // Assert
                Assert.NotNull(result);
                Assert.Equal(2, result.Count());
                Assert.Contains(result, d => d.DepartmentId == 1 && d.Department == "Department1");
                Assert.Contains(result, d => d.DepartmentId == 2 && d.Department == "Department2");
            }
        }



        [Fact]
        public async Task GetDepartments_WithEmptyDepartments_ShouldReturnEmptyList()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new TemplateRepository(context, null, dbContextOptions, null);

                // Act
                var result = await repository.GetDepartments();

                // Assert
                Assert.NotNull(result);
                Assert.Empty(result);
            }
        }


        [Fact]
        public async Task GetTemplates_WithValidTemplates_ShouldReturnListOfTemplates()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new TemplateRepository(context, null, dbContextOptions, null);

                context.Template.AddRange(new List<Template>
               {
                new Template
                {
                    TemplateId = 1,
                    TemplateName = "Template1",
                    IsDeleted = false,
                },
                new Template
                {
                    TemplateId = 2,
                    TemplateName = "Template2",
                    IsDeleted = false,
                },
                new Template
                {
                    TemplateId = 3,
                    TemplateName = "Template3",
                    IsDeleted = true,
                },
               });

                context.Department.Add(new Department { DepartmentId = 1, DepartmentName = "Department1" });
                context.EngageType.Add(new EngageType { EngageTypeId = 1, EngageTypeName = "EngageType1" });
                context.Status.Add(new Status { StatusId = 1, StatusName = "Status1" });
                context.TemplateVersion.AddRange(new List<TemplateVersion>
            {
                new TemplateVersion { TemplateVersionId = 1, TemplateId = 1, VersionNumber = 1, TemplateLogic = "Logic1" },
                new TemplateVersion { TemplateVersionId = 2, TemplateId = 2, VersionNumber = 1, TemplateLogic = "Logic2" },
            });

                context.SaveChanges();

                // Act
                var result = await repository.GetTemplates();

                // Assert
                Assert.NotNull(result);
                Assert.Equal(2, result.Count());
                Assert.Contains(result, t => t.TemplateId == 1 && t.TemplateName == "Template1");
                Assert.Contains(result, t => t.TemplateId == 2 && t.TemplateName == "Template2");
            }
        }


        [Fact]
        public async Task GetTemplates_WithAllTemplatesDeleted_ShouldReturnEmptyList()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new TemplateRepository(context, null, dbContextOptions, null);

                context.Template.AddRange(new List<Template>
               {
                new Template
                {
                    TemplateId = 1,
                    TemplateName = "Template1",
                    IsDeleted = true,

                },
                new Template
                {
                    TemplateId = 2,
                    TemplateName = "Template2",
                    IsDeleted = true

                },
                new Template
                {
                    TemplateId = 3,
                    TemplateName = "Template3",
                    IsDeleted = true,

                },
            });

                context.SaveChanges();

                // Act
                var result = await repository.GetTemplates();

                // Assert
                Assert.NotNull(result);
                Assert.Empty(result);
            }
        }

        [Fact]
        public async Task GetTemplateById_WhenValidTemplateId_ShouldReturnTemplate()
        {
            // Arrange
            int templateId = 1;
            string expectedAttachmentJson = "Sample Attachment JSON";

            var templates = new Template
            {
                TemplateId = templateId,
                TemplateName = "Sample Template",
                DepartmentId = 1,
                EngageTypeId = 1,
                StatusId = 1,
                IsDeleted = false
            };

            var departments = new Department
            {
                DepartmentId = 1,
                DepartmentName = "DepartmentName"
            };

            var engageType = new EngageType
            {
                EngageTypeId = 1,
                EngageTypeName = "EngageTypeName"
            };

            var status = new Status
            {
                StatusId = 1,
                StatusName = "StatusName"
            };

            var templateAttachments = new TemplateAttachments
            {
                TemplateAttachmentsId = 1,
                TemplateId = templateId,
                AttachmentJson = expectedAttachmentJson
            };


            var templateVersions = new TemplateVersion
            {
                TemplateId = templateId,
                VersionNumber = 1,
                TemplateLogic = "Template Logic here"
            };

            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                context.Template.Add(templates);
                context.Department.Add(departments);
                context.EngageType.Add(engageType);
                context.Status.Add(status);
                context.TemplateAttachments.Add(templateAttachments);
                context.TemplateVersion.Add(templateVersions);
                context.SaveChanges();
            }

            var mockMapper = new Mock<IMapper>();
            mockMapper.Setup(m => m.Map<TemplateListModel>(It.IsAny<Template>())).Returns((Template source) =>
            {
                return new TemplateListModel
                {
                    TemplateId = source.TemplateId,
                    AttachmentJson = expectedAttachmentJson
                };
            });

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new TemplateRepository(context, mockMapper.Object, dbContextOptions, null);

                // Act
                var result = await repository.GetTemplateById(templateId);

                // Assert
                Assert.NotNull(result);
                Assert.Equal(templateId, result.TemplateId);
                Assert.Equal(expectedAttachmentJson, result.AttachmentJson);
            }
        }

        [Fact]
        public async Task GetTemplateById_WhenInvalidTemplateId_ShouldReturnNull()
        {
            // Arrange
            int templateId = 999;
            string expectedAttachmentJson = "Sample Attachment JSON";


            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            var mockMapper = new Mock<IMapper>();
            mockMapper.Setup(m => m.Map<TemplateListModel>(It.IsAny<Template>())).Returns((Template source) =>
            {
                return new TemplateListModel
                {
                    TemplateId = source.TemplateId,
                    AttachmentJson = expectedAttachmentJson
                };
            });

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new TemplateRepository(context, mockMapper.Object, dbContextOptions, null);

                // Act
                var result = await repository.GetTemplateById(templateId);

                // Assert
                Assert.Null(result);
            }
        }

        [Fact]
        public async Task GetEngageTypes_WithValidDepartmentId_ShouldReturnListOfEngageTypes()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new TemplateRepository(context, null, dbContextOptions, null);

                context.Department.Add(new Department { DepartmentId = 1, DepartmentName = "Department1" });
                context.Department.Add(new Department { DepartmentId = 2, DepartmentName = "Department2" });
                context.EngageType.AddRange(new List<EngageType>
                {
                new EngageType { EngageTypeId = 1, EngageTypeName = "Type1", DepartmentId = 1 },
                new EngageType { EngageTypeId = 2, EngageTypeName = "Type2", DepartmentId = 2 },
                new EngageType { EngageTypeId = 3, EngageTypeName = "Type3", DepartmentId = 1 },
                });

                context.SaveChanges();

                // Act
                var departmentId = 1;
                var result = await repository.GetEngageTypes(departmentId);

                // Assert
                Assert.NotNull(result);
                Assert.Equal(2, result.Count());
                Assert.Contains(result, e => e.EngageTypeId == 1 && e.EngageTypeName == "Type1");
                Assert.Contains(result, e => e.EngageTypeId == 3 && e.EngageTypeName == "Type3");
            }
        }

        [Fact]
        public async Task GetEngageTypes_WithInvalidDepartmentId_ShouldReturnEmptyList()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new TemplateRepository(context, null, dbContextOptions, null);

                context.Department.Add(new Department { DepartmentId = 1, DepartmentName = "Department1" });
                context.EngageType.AddRange(new List<EngageType>
                {
                new EngageType { EngageTypeId = 1, EngageTypeName = "Type1", DepartmentId = 1 },
                new EngageType { EngageTypeId = 2, EngageTypeName = "Type2", DepartmentId = 1 },
                });

                context.SaveChanges();

                // Act
                var departmentId = 2;
                var result = await repository.GetEngageTypes(departmentId);

                // Assert
                Assert.NotNull(result);
                Assert.Empty(result);
            }
        }


        [Fact]
        public async Task GetStatus_ShouldReturnListOfStatus()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new TemplateRepository(context, null, dbContextOptions, null);

                context.Status.AddRange(new List<Status>
                {
                new Status { StatusId = 1, StatusName = "Status1" },
                new Status { StatusId = 2, StatusName = "Status2" },
                new Status { StatusId = 3, StatusName = "Status3" },
                });

                context.SaveChanges();

                // Act
                var result = await repository.GetStatus();

                // Assert
                Assert.NotNull(result);
                Assert.Equal(3, result.Count());
                Assert.Contains(result, s => s.StatusId == 1 && s.StatusName == "Status1");
                Assert.Contains(result, s => s.StatusId == 2 && s.StatusName == "Status2");
                Assert.Contains(result, s => s.StatusId == 3 && s.StatusName == "Status3");
            }
        }


        [Fact]
        public async Task GetStatus_WithNoStatusRecords_ShouldReturnEmptyList()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString()  )
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new TemplateRepository(context, null, dbContextOptions, null);

                // Act
                var result = await repository.GetStatus();

                // Assert
                Assert.NotNull(result);
                Assert.Empty(result);
            }
        }

        [Fact]
        public async Task DeleteTemplate_WhenValidDeleteIds_ShouldDeleteTemplates()
        {
            // Arrange
            var deleteModel = new TemplateListModel
            {
                DeleteId = new int[] { 1, 2, 3 },
                DeletedBy = "John Doe"
            };

            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(options))
            {
                context.Template.Add(new Template
                {
                    TemplateId = 1,
                    TemplateName = "Template1",
                });
                context.Template.Add(new Template
                {
                    TemplateId = 2,
                    TemplateName = "Template2",
                });
                context.Template.Add(new Template
                {
                    TemplateId = 3,
                    TemplateName = "Template3",
                });
                context.BulkLetters.Add(new BulkLetters
                {
                    TemplateName = null
                });

                context.SaveChanges();
                var getCurrentDatetimeMock = new Mock<IGetCurrentDatetime>();
                getCurrentDatetimeMock.Setup(mock => mock.GetCurrentEstDatetime())
          .ReturnsAsync(DateTime.Now);
                var repository = new TemplateRepository(context, null, options, getCurrentDatetimeMock.Object);

                // Act
                var result = await repository.DeleteTemplate(deleteModel);

                // Assert
                Assert.True(result);
                Assert.True(context.Template.All(t => t.IsDeleted));
                Assert.Equal(3, context.Template.Count());
            }
        }

        [Fact]
        public async Task DeleteTemplate_WhenInValidDeleteIds_ShouldNotDeleteTemplates()
        {
            // Arrange
            var deleteModel = new TemplateListModel
            {
                DeleteId = new int[] { 1 },
                DeletedBy = "John Doe"
            };

            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(options))
            {
                context.Template.Add(new Template
                {
                    TemplateId = 1,
                    TemplateName = "Template1",
                });

                context.BulkLetters.Add(new BulkLetters
                {
                    TemplateName = "Template1"

                });

                context.SaveChanges();

                var repository = new TemplateRepository(context, null, options, null);

                // Act
                var result = await repository.DeleteTemplate(deleteModel);

                // Assert
                Assert.False(result);
                Assert.Equal(1, context.Template.Count());
                Assert.False(context.Template.First().IsDeleted);
            }
        }

        [Fact]
        public async Task GetTemplatesIdList_ShouldReturnListOfTemplateIds()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new TemplateRepository(context, null, dbContextOptions, null);

                context.Status.Add(new Status { StatusId = 1, StatusName = "Status1" });
                context.Status.Add(new Status { StatusId = 2, StatusName = "Status2" });
                context.Template.AddRange(new List<Template>
                {
                new Template { TemplateId = 1, StatusId = 1 },
                new Template { TemplateId = 2, StatusId = 1 },
                new Template { TemplateId = 3, StatusId = 2 },
                });

                context.SaveChanges();

                // Act
                var result = await repository.GetTemplatesIdList();

                // Assert
                Assert.NotNull(result);
                Assert.Equal(3, result.Count());
                Assert.Contains(1, result);
                Assert.Contains(2, result);
                Assert.Contains(3, result);
            }
        }

        [Fact]
        public async Task GetTemplatesIdList_WithNoTemplates_ShouldReturnEmptyList()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new TemplateRepository(context, null, dbContextOptions, null);

                // Act
                var result = await repository.GetTemplatesIdList();

                // Assert
                Assert.NotNull(result);
                Assert.Empty(result);
            }
        }
    }
}
