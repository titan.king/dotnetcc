﻿using AutoFixture;
using AutoMapper;
using Dashboard.Data.Context;
using Dashboard.Data.DataModel;
using Dashboard.Data.Repositories;
using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.Fields;
using Microsoft.EntityFrameworkCore;
using Moq;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.IntegrationTests
{
    public class ClientRepositoryUnitTests
    {
        [Fact]
        public async Task AddClientBulkDetails_WhenValidBulkLettersProvided_ShouldAddBulkLettersToDbContext()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;
            var fixture = new Fixture();
            var dbContext = new DashboardDBContext(dbContextOptions);
            var configuration = new Mock<Microsoft.Extensions.Configuration.IConfiguration>();
            configuration.Setup(c => c["TakeValue"]).Returns("10");
            var mapper = new Mock<IMapper>();
            mapper.Setup(m => m.Map<BulkLetters>(It.IsAny<BulkLettersResponse>()))
                .Returns<BulkLettersResponse>(response => new BulkLetters { });

            var repository = new ClientRepository(dbContext, configuration.Object, mapper.Object, dbContextOptions, null);
            var bulkLetters = fixture.Create<BulkLettersResponse>();

            // Act
            var result = await repository.AddClientBulkDetails(bulkLetters);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(0, dbContext.BulkLetters.Count());
            dbContext.Dispose();
        }

        [Fact]
        public async Task IsNotExistingClientId_WhenClientIdDoesNotExist_ShouldReturnTrue()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;
            var mapper = new Mock<IMapper>();
            var configuration = new Mock<Microsoft.Extensions.Configuration.IConfiguration>();
            configuration.Setup(c => c["TakeValue"]).Returns("10");
            var dbContext = new DashboardDBContext(dbContextOptions);
            var repository = new ClientRepository(dbContext, configuration.Object, mapper.Object, dbContextOptions, null);

            int clientIdToCheck = 123;

            // Act
            var result = await repository.IsNotExistingClientId(clientIdToCheck, 1);

            // Assert
            Assert.True(result);
            dbContext.Dispose();
        }

        [Fact]
        public async Task IsNotExistingClientId_WhenClientIdExists_ShouldReturnFalse()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;
            var configuration = new Mock<Microsoft.Extensions.Configuration.IConfiguration>();
            configuration.Setup(c => c["TakeValue"]).Returns("10");
            var mapper = new Mock<IMapper>();
            var dbContext = new DashboardDBContext(dbContextOptions);
            var repository = new ClientRepository(dbContext, configuration.Object, mapper.Object, dbContextOptions, null);

            int clientIdToCheck = 123;

            dbContext.BulkLetters.Add(new BulkLetters { ClientId = clientIdToCheck });
            dbContext.SaveChanges();

            // Act
            var result = await repository.IsNotExistingClientId(clientIdToCheck, 0);

            // Assert
            Assert.False(result);
            dbContext.Dispose();
        }

        [Fact]
        public async Task AddFieldIdtoList_WhenFieldsExistInDatabase_ShouldSetFieldIds()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;
            var configuration = new Mock<Microsoft.Extensions.Configuration.IConfiguration>();
            configuration.Setup(c => c["TakeValue"]).Returns("10");
            var mapper = new Mock<IMapper>();
            var dbContext = new DashboardDBContext(dbContextOptions);
            var repository = new ClientRepository(dbContext, configuration.Object, mapper.Object, dbContextOptions, null);


            dbContext.Field.Add(new Field { DisplayName = "Field1", IsDeleted = false, FieldId = 1 });
            dbContext.Field.Add(new Field { DisplayName = "Field2", IsDeleted = false, FieldId = 2 });
            dbContext.SaveChanges();

            var letterFieldValues = new List<LetterFieldValuesResponse>
                {
            new LetterFieldValuesResponse { FieldName = "Field1", PartnerId = 1 },
            new LetterFieldValuesResponse { FieldName = "Field2" },
            new LetterFieldValuesResponse { FieldName = "Field3", PartnerId = 2 },
                };

            // Act
            var result = await repository.AddFieldIdtoList(letterFieldValues);

            // Assert
            Assert.Equal(1, result[0].FieldId);
            Assert.Equal(2, result[1].FieldId);
            Assert.NotNull(result[2].FieldId);
            dbContext.Dispose();
        }

        //[Fact]
        //public async Task GetPartnersNameAsync_ShouldReturnListOfPartners()
        //{
        //    // Arrange
        //    var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
        //        .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
        //        .Options;
        //    var configuration = new Mock<Microsoft.Extensions.Configuration.IConfiguration>();
        //    configuration.Setup(c => c["TakeValue"]).Returns("10");
        //    var mapper = new Mock<IMapper>();
        //    var dbContext = new DashboardDBContext(dbContextOptions);
        //    var repository = new ClientRepository(dbContext, configuration.Object, mapper.Object, dbContextOptions, null);
        //    var partners = new List<t_engletterdata>
        //     {
        //        new t_engletterdata { Id = 1,
        //                        row_num = "1",
        //                        ClientId = "CL001",
        //                        OfficeId = "OF001",
        //                        PartnerId = "PT001",
        //                        PartnerName = "Sample Partner",
        //                        OfficeName = "Sample Office",
        //                        OfficeAddress = "123 Main St",
        //                        OfficeCity = "Sample City",
        //                        OfficeState = "CA",
        //                        OfficePhoneNumber = "555-123-4567",
        //                        OfficeZipCode = "12345",
        //                        Date = "2023-10-26",
        //                        ClientName = "John Doe",
        //                        Address = "456 Elm St",
        //                        City = "Client City",
        //                        State = "NY",
        //                        Zip = "54321",
        //                        SignatoryEmail = "john.doe@email.com",
        //                        SpouseEmail = "jane.doe@email.com",
        //                        SignatoryFirstName = "John",
        //                        SignatoryLastName = "Doe",
        //                        SignatoryTitle = "CEO",
        //                        SpouseFirstName = "Jane",
        //                        SpouseLastName = "Doe",
        //                        Jurisdiction = "Sample Jurisdiction",
        //                        FiscalYear = "2023",
        //                        ELTemplate = "Sample Template",
        //                        Expiration7216Form = "2023-12-31" },
        //             };
        //    dbContext.ClientData.AddRange(partners);
        //    dbContext.SaveChanges();

        //    // Act
        //    var result = await repository.GetPartnersNameAsync();

        //    // Assert
        //    Assert.NotNull(result);
        //    Assert.IsAssignableFrom<IEnumerable<ClientData>>(result);
        //    var partnerList = result.ToList();
        //    Assert.Equal(1, partnerList.Count);
        //    dbContext.Dispose();
        //}

        [Fact]
        public async Task GetBatchIdsAsync_ShouldReturnBatchIds()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;
            var configuration = new Mock<Microsoft.Extensions.Configuration.IConfiguration>();
            configuration.Setup(c => c["TakeValue"]).Returns("10");
            var mapper = new Mock<IMapper>();
            var dbContext = new DashboardDBContext(dbContextOptions);

            dbContext.BulkLetters.AddRange(new List<BulkLetters>
        {
            new BulkLetters { BatchId = 1, IsDeleted = false },
            new BulkLetters { BatchId = 2, IsDeleted = false },
            new BulkLetters { BatchId = 2, IsDeleted = false },
            new BulkLetters { BatchId = 3, IsDeleted = true }
        });

            dbContext.SaveChanges();

            var repository = new ClientRepository(dbContext, configuration.Object, mapper.Object, dbContextOptions, null);

            // Act
            var result = await repository.GetBatchIdsAsync();

            // Assert
            Assert.NotNull(result);
            var batchIdsList = result.ToList();
            Assert.Equal(2, batchIdsList.Count);
            Assert.Equal(2, batchIdsList[0].BatchId);
            Assert.Equal(1, batchIdsList[1].BatchId);

            // Clean up
            dbContext.Dispose();
        }

    }
}
