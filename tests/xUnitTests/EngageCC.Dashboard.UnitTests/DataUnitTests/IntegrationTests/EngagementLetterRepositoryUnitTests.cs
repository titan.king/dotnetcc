﻿using AutoFixture;
using Dashboard.Data.Context;
using Dashboard.Data.DataModel;
using Dashboard.Data.Repositories;
using Dashboard.Models.ResponseModel;
using Engage3.Common.Business.Interfaces;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.IntegrationTests
{
    public class EngagementLetterRepositoryUnitTests
    {
        [Fact]
        public async Task GetEngagementLettersSearchFilter_WhenDataExists_ShouldReturnEngagementLetters()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;
            var fixture = new Fixture();
            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var engagementLetter1 = fixture.Create<EngagementLetter>();
                var engagementLetter2 = fixture.Create<EngagementLetter>();
                context.EngagementLetter.AddRange(engagementLetter1, engagementLetter2);
                context.SaveChanges();
            }

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new EngagementLetterRepository(dbContextOptions, context, null, null);

                // Act
                var engagementLetters = await repository.GetEngagementLettersSearchFilter();

                // Assert
                Assert.NotNull(engagementLetters);
                Assert.NotEmpty(engagementLetters);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetEngagementLettersSearchFilter_WhenNoDataExists_ShouldReturnEmptyList()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new EngagementLetterRepository(dbContextOptions, context, null, null);

                // Act
                var engagementLetters = await repository.GetEngagementLettersSearchFilter();

                // Assert
                Assert.NotNull(engagementLetters);
                Assert.Empty(engagementLetters);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetSigningInfoDetail_WhenDataExists_ShouldReturnAgreementId()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var engagementLetter = new EngagementLetter { EngagementLetterId = 1 };
                var signingInfo = new SigningInfo
                {
                    EngagementLetterId = engagementLetter.EngagementLetterId,
                    AgreementId = "TestAgreementId"
                };
                context.EngagementLetter.Add(engagementLetter);
                context.SigningInfo.Add(signingInfo);
                context.SaveChanges();
            }

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new EngagementLetterRepository(dbContextOptions, context, null, null);

                // Act
                int engLetterId = 1;
                var agreementId = await repository.GetSigningInfoDetail(engLetterId);

                // Assert
                Assert.NotNull(agreementId);
                Assert.Equal("TestAgreementId", agreementId);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetSigningInfoDetail_WhenNoDataExists_ShouldReturnNull()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;
            var fixture = new Fixture();
            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new EngagementLetterRepository(dbContextOptions, context, null, null);

                // Act
                int engLetterId = 0;
                var agreementId = await repository.GetSigningInfoDetail(engLetterId);

                // Assert
                Assert.Null(agreementId);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetEngagementLetterStatus_WhenEngLetterExists_ShouldReturnEngagementLetter()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var engagementLetter = new EngagementLetter
                {
                    EngagementLetterId = 1,
                };
                context.EngagementLetter.Add(engagementLetter);
                context.SaveChanges();
            }

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new EngagementLetterRepository(dbContextOptions, context, null, null);

                // Act
                var engLetterId = 1; 
                var engagementLetter = await repository.GetEngagementLetterStatus(engLetterId);

                // Assert
                Assert.NotNull(engagementLetter);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetEngagementLetterStatus_WhenEngLetterDoesNotExist_ShouldReturnNull()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new EngagementLetterRepository(dbContextOptions, context, null, null);

                // Act
                var engLetterId = 1; 
                var engagementLetter = await repository.GetEngagementLetterStatus(engLetterId);

                // Assert
                Assert.Null(engagementLetter);
                context.Dispose();
            }
        }

        [Fact]
        public async Task UpdateSignedStatusEngagementLetter_WhenValidData_ShouldUpdateAndReturnUpdatedEngagementLetter()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var engagementLetter = new EngagementLetter
                {
                    EngagementLetterId = 1,
                    DocumentStatusId = 1, 
                };
                context.EngagementLetter.Add(engagementLetter);
                context.SaveChanges();
            }

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var getCurrentDatetimeMock = new Mock<IGetCurrentDatetime>();
                var repository = new EngagementLetterRepository(dbContextOptions, context, null, getCurrentDatetimeMock.Object);

                var updatedEngagementLetter = new EngagementLetter
                {
                    EngagementLetterId = 1,
                    DocumentStatusId = 2, 
                };

                // Act
                var result = await repository.UpdateSignedStatusEngagementLetter(updatedEngagementLetter);

                // Assert
                Assert.NotNull(result);
                Assert.Equal(2, result.DocumentStatusId);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetDepartmentSearchFilter_WhenDataExists_ShouldReturnDepartments()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;
            var fixture = new Fixture();
            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var department1 = fixture.Create<Department>(); 
                var department2 = fixture.Create<Department>();
                context.Department.AddRange(department1, department2);
                context.SaveChanges();
            }

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new EngagementLetterRepository(dbContextOptions, context, null, null);

                // Act
                var departments = await repository.GetDepartmentSearchFilter();

                // Assert
                Assert.NotNull(departments);
                Assert.NotEmpty(departments);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetDepartmentSearchFilter_WhenNoDataExists_ShouldReturnEmptyList()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new EngagementLetterRepository(dbContextOptions, context, null, null);

                // Act
                var departments = await repository.GetDepartmentSearchFilter();

                // Assert
                Assert.NotNull(departments);
                Assert.Empty(departments);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetEngageTypeSearchFilter_WhenDataExists_ShouldReturnEngageTypes()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;
            var fixture = new Fixture();
            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var engageType1 = fixture.Create<EngageType>();  
                var engageType2 = fixture.Create<EngageType>();
                context.EngageType.AddRange(engageType1, engageType2);
                context.SaveChanges();
            }

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new EngagementLetterRepository(dbContextOptions, context, null, null);

                // Act
                var engageTypes = await repository.GetEngageTypeSearchFilter();

                // Assert
                Assert.NotNull(engageTypes);
                Assert.NotEmpty(engageTypes);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetEngageTypeSearchFilter_WhenNoDataExists_ShouldReturnEmptyList()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new EngagementLetterRepository(dbContextOptions, context, null, null);

                // Act
                var engageTypes = await repository.GetEngageTypeSearchFilter();

                // Assert
                Assert.NotNull(engageTypes);
                Assert.Empty(engageTypes);
                context.Dispose();
            }
        }


        [Fact]
        public async Task GetDocumentStatusSearchFilter_WhenDataExists_ShouldReturnDocumentStatus()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;
            var fixture = new Fixture();
            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var documentStatus1 = fixture.Create<DocumentStatus>(); 
                var documentStatus2 = fixture.Create<DocumentStatus>();
                context.DocumentStatus.AddRange(documentStatus1, documentStatus2);
                context.SaveChanges();
            }

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new EngagementLetterRepository(dbContextOptions, context, null, null);

                // Act
                var documentStatusList = await repository.GetDocumentStatusSearchFilter();

                // Assert
                Assert.NotNull(documentStatusList);
                Assert.NotEmpty(documentStatusList);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetDocumentStatusSearchFilter_WhenNoDataExists_ShouldReturnEmptyList()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new EngagementLetterRepository(dbContextOptions, context, null, null);

                // Act
                var documentStatusList = await repository.GetDocumentStatusSearchFilter();

                // Assert
                Assert.NotNull(documentStatusList);
                Assert.Empty(documentStatusList);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetEngagementLetters_WhenDataExists_ShouldReturnEngagementLetterList()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;
            var fixture = new Fixture();
            using (var context = new DashboardDBContext(dbContextOptions))
            {

                var engagementLetter = fixture.Create<EngagementLetter>(); 
                context.EngagementLetter.Add(engagementLetter);
                context.SaveChanges();
            }

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new EngagementLetterRepository(dbContextOptions, context, null, null);

                // Act
                var engagementLetterList = await repository.GetEngagementLetters();

                // Assert
                Assert.NotNull(engagementLetterList);
                Assert.NotEmpty(engagementLetterList);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetEngagementLetters_WhenNoDataExists_ShouldReturnEmptyList()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new EngagementLetterRepository(dbContextOptions, context, null, null);

                // Act
                var engagementLetterList = await repository.GetEngagementLetters();

                // Assert
                Assert.NotNull(engagementLetterList);
                Assert.Empty(engagementLetterList);
                context.Dispose();
            }
        }

        //[Fact]
        //public async Task GetFilteredSearchLetters_WithFilters_ShouldReturnFilteredResults()
        //{
        //    // Arrange
        //    var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
        //        .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
        //        .Options;
        //    var fixture = new Fixture();
        //    var engagementLetter1 = fixture.Create<EngagementLetter>();
        //    var engagementLetter2 = fixture.Create<EngagementLetter>();
        //    using (var context = new DashboardDBContext(dbContextOptions))
        //    {
        //        context.EngagementLetter.AddRange(engagementLetter1, engagementLetter2);
        //        context.SaveChanges();
        //    }

        //    using (var context = new DashboardDBContext(dbContextOptions))
        //    {
        //        var repository = new EngagementLetterRepository(dbContextOptions, context);
        //        var filter = fixture.Create<EngagementLetterSearchList>();
              

        //        // Act
        //        var filteredLetters = await repository.GetFilteredSearchLetters(filter);

        //        // Assert
        //        Assert.NotNull(filteredLetters);
        //        Assert.NotEmpty(filteredLetters);
        //        context.Dispose();
        //    }
        //}

        [Fact]
        public async Task DeleteEngagementLetter_WhenValidEngagementLetterId_ShouldDeleteEngagementLetter()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var engagementLetter = new EngagementLetter
                {
                    EngagementLetterId = 1,
                    IsDeleted = false,
                    DeletedBy = null,
                    DeletedOn = null,
                    ModifiedOn = DateTime.Now,
                };

                context.EngagementLetter.Add(engagementLetter);
                context.SaveChanges();

                var getCurrentDatetimeMock = new Mock<IGetCurrentDatetime>();
                getCurrentDatetimeMock.Setup(mock => mock.GetCurrentEstDatetime())
          .ReturnsAsync(DateTime.Now);

                var repository = new EngagementLetterRepository(dbContextOptions, context, null, getCurrentDatetimeMock.Object);
                var deleteEngagementLetterResModel = new DeleteEngagementLetterResModel
                {
                    EngagementLetterId = 1
                };

                // Act
                var result = await repository.DeleteEngagementLetter(deleteEngagementLetterResModel);

                // Assert
                Assert.True(result);
                var deletedEngagementLetter = context.EngagementLetter.FirstOrDefault(e => e.EngagementLetterId == 1);
                Assert.NotNull(deletedEngagementLetter);
                Assert.True(deletedEngagementLetter.IsDeleted);
                context.Dispose();
            }
        }

    }
}
