﻿using AutoFixture;
using Dashboard.Data.Context;
using Dashboard.Data.DataModel;
using Dashboard.Data.Repositories;
using Dashboard.Models.ResponseModel;
using Engage3.Common.Business.Interfaces;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.IntegrationTests
{
    public class BlocksRepositoryUnitTests
    {
        [Fact]
        public async Task AddBlocks_WhenValidBlockProvided_ShouldAddBlockToDbContext()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB")
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var getCurrentDatetimeMock = new Mock<IGetCurrentDatetime>();
                getCurrentDatetimeMock.Setup(mock => mock.GetCurrentEstDatetime())
          .ReturnsAsync(DateTime.Now);
                var repository = new BlocksRepository(dbContextOptions, context, getCurrentDatetimeMock.Object);

                var block = new Block
                {
                    BlockId = 0,
                    CreatedOn = DateTime.Now,
                    ModifiedOn = DateTime.Now
                };

                // Act
                var result = await repository.AddBlocks(block);

                // Assert
                Assert.NotNull(result);
                Assert.Equal(1, context.Block.Count());
                Assert.Contains(block, context.Block.ToList());
            }
        }

        [Fact]
        public async Task AddBlocks_WhenNonZeroBlockIdProvided_ShouldNotAddBlockToDbContext()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;
            var dbContext = new DashboardDBContext(dbContextOptions);
            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new BlocksRepository(dbContextOptions, context, null);

                var block = new Block
                {
                    BlockId = 1,
                    CreatedOn = DateTime.Now,
                    ModifiedOn = DateTime.Now
                };

                // Act
                var result = await repository.AddBlocks(block);

                // Assert
                Assert.NotNull(result);
                Assert.Equal(0, context.Block.Count());
                dbContext.Dispose();
            }
        }

        [Fact]
        public async Task UpdateBlocks_WithValidBlockId_ShouldUpdateBlock()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "TestDatabase_" + Guid.NewGuid().ToString())
                .Options;

            using var dbContext = new DashboardDBContext(options);
            var existingBlock = new Block
            {
                BlockId = 1,
            };
            dbContext.Block.Add(existingBlock);
            await dbContext.SaveChangesAsync();
            var getCurrentDatetimeMock = new Mock<IGetCurrentDatetime>();
            getCurrentDatetimeMock.Setup(mock => mock.GetCurrentEstDatetime())
          .ReturnsAsync(DateTime.Now);
            var repository = new BlocksRepository(options, dbContext, getCurrentDatetimeMock.Object);
            var knownModifiedDate = DateTime.Parse("2023-01-01 12:00:00");
            existingBlock.ModifiedOn = knownModifiedDate;

            // Act
            var result = await repository.UpdateBlocks(existingBlock);

            // Assert
            Assert.Equal(existingBlock, result);
            Assert.Null(result.DeletedOn);
            Assert.NotEqual(knownModifiedDate, result.ModifiedOn);
            dbContext.Dispose();
        }

        [Fact]
        public async Task UpdateBlocks_WithNonPositiveBlockId_ShouldNotUpdateBlock()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "TestDatabase_" + Guid.NewGuid().ToString())
                .Options;

            using var dbContext = new DashboardDBContext(options);
            var invalidBlock = new Block
            {
                BlockId = 0,
            };
            var repository = new BlocksRepository(options, dbContext, null);

            // Act
            var result = await repository.UpdateBlocks(invalidBlock);

            // Assert
            Assert.Equal(invalidBlock, result);
            Assert.Null(result.DeletedOn);
            Assert.Null(result.ModifiedOn);
            dbContext.Dispose();
        }

        [Fact]
        public async Task GetConnectedTemplates_ReturnsTemplates()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "TestDatabase_" + Guid.NewGuid().ToString())
                .Options;

            using var dbContext = new DashboardDBContext(dbContextOptions);

            var fixture = new Fixture();
            var templates = fixture.Build<Template>()
                .With(t => t.TemplateId, 0)
                .With(t => t.IsDeleted, false)
                .With(t => t.ModifiedOn, DateTime.Now)
                .CreateMany<Template>(5);

            dbContext.Template.AddRange(templates);
            await dbContext.SaveChangesAsync();
            var repository = new BlocksRepository(dbContextOptions, dbContext, null);

            // Act
            var result = await repository.GetConnectedTemplates();

            // Assert
            Assert.NotNull(result);
            dbContext.Dispose();
        }


        [Fact]
        public async Task GetConnectedTemplates_WhenNoTemplates_ShouldReturnEmptyList()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(options))
            {
                var repository = new BlocksRepository(options, context, null);

                // Act
                var connectedTemplates = await repository.GetConnectedTemplates();

                // Assert
                Assert.NotNull(connectedTemplates);
                Assert.Empty(connectedTemplates);
                context.Dispose();
            }
        }

        // commanded due to connected block issue
        //[Fact]
        //public async Task UpdateConnectedTemplates_ShouldUpdateTemplates()
        //{
        //    // Arrange
        //    var options = new DbContextOptionsBuilder<DashboardDBContext>()
        //        .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
        //        .Options;

        //    using (var context = new DashboardDBContext(options))
        //    {
        //        var repository = new BlocksRepository(options, context);
        //        var departmentId = 1; 
        //        var engageTypeId = 2; 

        //        var template1 = new Template
        //        {
        //            TemplateId = 1,
        //            DepartmentId = departmentId,
        //            EngageTypeId = engageTypeId,
        //            IsDeleted = false
        //        };

        //        var template2 = new Template
        //        {
        //            TemplateId = 2,
        //            DepartmentId = departmentId,
        //            EngageTypeId = engageTypeId,
        //            IsDeleted = false
        //        };

        //        context.Template.AddRange(template1, template2);
        //        context.SaveChanges();

        //        var connectedTemplatesResModel = new ConnectedTemplatesResModel
        //        {
        //            DepartmentId = departmentId,
        //            EngageTypeId = engageTypeId,
        //            Content = "Updated Template Logic"
        //        };

        //        // Act
        //        await repository.UpdateConnectedTemplates(connectedTemplatesResModel);

        //        // Assert
        //        var updatedTemplates = context.TemplateVersion
        //            .Where(tv => tv.TemplateId == template1.TemplateId || tv.TemplateId == template2.TemplateId)
        //            .OrderByDescending(tv => tv.VersionNumber)
        //            .ToList();
        //        Assert.Equal(2, updatedTemplates.Count);
        //        Assert.Equal(1, updatedTemplates[0].VersionNumber);
        //        Assert.Equal("Updated Template Logic", updatedTemplates[0].TemplateLogic);
        //        Assert.Equal(1, updatedTemplates[1].VersionNumber);
        //        Assert.Equal("Updated Template Logic", updatedTemplates[1].TemplateLogic);
        //        context.Dispose();
        //    }
        //}

        [Fact]
        public async Task GetBlocksById_ShouldReturnBlocks()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(options))
            {
                var repository = new BlocksRepository(options, context, null);
                var blockId = 1; 
                var status = new Status { StatusId = 1, StatusName = "Active" };
                var block = new Block
                {
                    BlockId = blockId,
                    BlockName = "Block 1",
                    Description = "Description",
                    Content = "Content",
                    ChangeNotes = "Change Notes",
                    StatusId = status.StatusId,
                    IsDeleted = false
                };
                context.Status.Add(status);
                context.Block.Add(block);
                context.SaveChanges();

                // Act
                var result = await repository.GetBlocksById(blockId);

                // Assert
                Assert.NotNull(result);
                Assert.NotNull(result.BlocksById);
                Assert.Single(result.BlocksById);
                var retrievedBlock = result.BlocksById[0];
                Assert.Equal(blockId, retrievedBlock.BlockId);
                Assert.Equal("Block 1", retrievedBlock.BlockName);
                Assert.Equal("Description", retrievedBlock.Description);
                Assert.Equal("Content", retrievedBlock.Content);
                Assert.Equal("Change Notes", retrievedBlock.ChangeNotes);
                Assert.Equal(status.StatusId, retrievedBlock.StatusId);
                Assert.Equal("Active", retrievedBlock.StatusName);
                Assert.Equal("", retrievedBlock.ConnectedTemplates);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetBlocksById_WithNonExistentBlock_ShouldReturnEmpty()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(options))
            {
                var repository = new BlocksRepository(options, context, null);

                // Act
                var result = await repository.GetBlocksById(999); 

                // Assert
                Assert.NotNull(result);
                Assert.NotNull(result.BlocksById);
                Assert.Empty(result.BlocksById);
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetBlocksIdList_ShouldReturnBlockIds()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(options))
            {
                var repository = new BlocksRepository(options, context, null);

                var status = new Status { StatusId = 1, StatusName = "Active" };
                var blocks = new List<Block>
            {
                new Block { BlockId = 1, StatusId = status.StatusId },
                new Block { BlockId = 2, StatusId = status.StatusId },
                new Block { BlockId = 3, StatusId = status.StatusId }
            };
                context.Status.Add(status);
                context.Block.AddRange(blocks);
                context.SaveChanges();

                // Act
                var result = await repository.GetBlocksIdList();

                // Assert
                Assert.NotNull(result);
                var blockIds = result.ToList();

                Assert.Equal(3, blockIds.Count);
                Assert.Collection(blockIds,
                    item => Assert.Equal(3, item.BlockId),
                    item => Assert.Equal(2, item.BlockId),
                    item => Assert.Equal(1, item.BlockId));
                context.Dispose();
            }
        }

        [Fact]
        public async Task GetBlocks_ShouldReturnBlocks()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(options))
            {
                var repository = new BlocksRepository(options, context, null);
                var status = new Status { StatusId = 1, StatusName = "Active" };
                var blocks = new List<Block>
            {
                new Block
                {
                    BlockId = 1,
                    BlockName = "Block 1",
                    Description = "Description 1",
                    StatusId = status.StatusId,
                    ChangeNotes = "Change Notes 1",
                    Content = "Content 1",
                    ModifiedOn = DateTime.UtcNow.AddDays(-1),
                    IsDeleted = false
                },
                new Block
                {
                    BlockId = 2,
                    BlockName = "Block 2",
                    Description = "Description 2",
                    StatusId = status.StatusId,
                    ChangeNotes = "Change Notes 2",
                    Content = "Content 2",
                    ModifiedOn = DateTime.UtcNow,
                    IsDeleted = false
                }
            };
                context.Status.Add(status);
                context.Block.AddRange(blocks);
                context.SaveChanges();

                // Act
                var result = await repository.GetBlocks();

                // Assert
                Assert.NotNull(result);
                var blockList = result.ToList();
                Assert.Equal(2, blockList.Count);

                // Check the first block
                Assert.Equal(2, blockList[0].BlockId);
                Assert.Equal("Block 2", blockList[0].BlockName);
                Assert.Equal("Description 2", blockList[0].Description);
                Assert.Equal(status.StatusId, blockList[0].StatusId);
                Assert.Equal("Active", blockList[0].StatusName);
                Assert.Equal("Change Notes 2", blockList[0].ChangeNotes);
                Assert.Equal("Content 2", blockList[0].Content);

                // Check the second block
                Assert.Equal(1, blockList[1].BlockId);
                Assert.Equal("Block 1", blockList[1].BlockName);
                Assert.Equal("Description 1", blockList[1].Description);
                Assert.Equal(status.StatusId, blockList[1].StatusId);
                Assert.Equal("Active", blockList[1].StatusName);
                Assert.Equal("Change Notes 1", blockList[1].ChangeNotes);
                Assert.Equal("Content 1", blockList[1].Content);
                context.Dispose();
            }
        }

        [Fact]
        public async Task DeleteBlock_WithValidBlockId_ShouldMarkBlockAsDeleted()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(options))
            {
                var getCurrentDatetimeMock = new Mock<IGetCurrentDatetime>();
                getCurrentDatetimeMock.Setup(mock => mock.GetCurrentEstDatetime())
          .ReturnsAsync(DateTime.Now);
                var repository = new BlocksRepository(options, context, getCurrentDatetimeMock.Object);

                var blockId = 1;
                var block = new Block
                {
                    BlockId = blockId,
                    IsDeleted = false
                };
                context.Block.Add(block);
                context.SaveChanges();

                var deleteBlockResModel = new DeleteBlockResModel
                {
                    BlockId = new int[] { blockId },
                    DeletedBy = "User123"
                };

                // Act
                var result = await repository.DeleteBlock(deleteBlockResModel);

                // Assert
                Assert.True(result);
                var deletedBlock = await context.Block.FirstOrDefaultAsync(b => b.BlockId == blockId);
                Assert.NotNull(deletedBlock);
                Assert.True(deletedBlock.IsDeleted);
                Assert.Equal("User123", deletedBlock.DeletedBy);
                Assert.NotNull(deletedBlock.DeletedOn);
                Assert.NotNull(deletedBlock.ModifiedOn);
                context.Dispose();
            }
        }

        [Fact]
        public async Task DeleteBlock_WithInvalidBlockId_ShouldReturnFalse()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(options))
            {
                var repository = new BlocksRepository(options, context, null);
                var deleteBlockResModel = new DeleteBlockResModel
                {
                    BlockId = new int[] { 999 },
                    DeletedBy = "User123"
                };

                // Act
                var result = await repository.DeleteBlock(deleteBlockResModel);

                // Assert
                Assert.False(result);
                context.Dispose();
            }
        }

        [Fact]
        public async Task Load_ShouldReturnBlockStatusList()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(options))
            {
                var repository = new BlocksRepository(options, context, null);
                var blockStatus1 = new Status { StatusId = 1, StatusName = "Active" };
                var blockStatus2 = new Status { StatusId = 2, StatusName = "Inactive" };
                var blockStatus3 = new Status { StatusId = 3, StatusName = "Pending" }; 
                context.Status.AddRange(blockStatus1, blockStatus2, blockStatus3);
                context.SaveChanges();

                // Act
                var result = await repository.Load();

                // Assert
                Assert.NotNull(result);
                var blockStatusList = result.BlockStatus.ToList();
                Assert.Equal(2, blockStatusList.Count);
                Assert.Equal(1, blockStatusList[0].StatusId);
                Assert.Equal("Active", blockStatusList[0].StatusName);
                Assert.Equal(2, blockStatusList[1].StatusId);
                Assert.Equal("Inactive", blockStatusList[1].StatusName);
                context.Dispose();
            }
        }

        [Fact]
        public async Task IsValidBlockName_WithNewBlockName_ShouldReturnTrue()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(options))
            {
                var repository = new BlocksRepository(options, context, null);
                var block = new Block
                {
                    BlockId = 0,
                    BlockName = "NewBlockName",
                    IsDeleted = false
                };
                context.Block.Add(block);
                context.SaveChanges();

                var newBlock = new BlocksResponseModel
                {
                    BlockId = 0,
                    BlockName = "UniqueBlockName"
                };

                // Act
                var result = await repository.IsValidBlockName(newBlock);

                // Assert
                Assert.True(result);
                context.Dispose();
            }
        }

        [Fact]
        public async Task IsValidBlockName_WithDuplicateBlockName_ShouldReturnFalse()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(options))
            {
                var repository = new BlocksRepository(options, context, null);
                var block = new Block
                {
                    BlockId = 0,
                    BlockName = "DuplicateBlockName",
                    IsDeleted = false
                };
                context.Block.Add(block);
                context.SaveChanges();
                var duplicateBlock = new BlocksResponseModel
                {
                    BlockId = 0,
                    BlockName = "DuplicateBlockName"
                };

                // Act
                var result = await repository.IsValidBlockName(duplicateBlock);

                // Assert
                Assert.False(result);
                context.Dispose();
            }
        }

        [Fact]
        public async Task IsValidBlockName_WithDuplicateBlockNameForExistingBlock_ShouldReturnTrue()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(options))
            {
                var repository = new BlocksRepository(options, context, null);
                var block = new Block
                {
                    BlockId = 1, 
                    BlockName = "ExistingBlockName",
                    IsDeleted = false
                };
                context.Block.Add(block);
                context.SaveChanges();

                var duplicateBlock = new BlocksResponseModel
                {
                    BlockId = 1, 
                    BlockName = "ExistingBlockName"
                };

                // Act
                var result = await repository.IsValidBlockName(duplicateBlock);

                // Assert
                Assert.True(result);
                context.Dispose();
            }
        }
    }
}
