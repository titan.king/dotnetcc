﻿using AutoFixture;
using AutoMapper;
using Dashboard.Data.Context;
using Dashboard.Data.DataModel;
using Dashboard.Data.Repositories;
using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.Clients;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;
using Engage3.Common.Business.Interfaces;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.IntegrationTests
{
    public class BulkLettersRepositoryUnitTests
    {
        private Mock<ResourceManager> _mockResourceManager = new Mock<ResourceManager>();
        [Fact]
        public async Task AddBatchRequestDetails_WhenValidBatchRequestProvided_ShouldAddBatchRequestToDbContext()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;
            var dbContext = new DashboardDBContext(dbContextOptions);
            var fixture = new Fixture();
            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var mapperMock = new Mock<IMapper>();
                var getCurrentDatetimeMock = new Mock<IGetCurrentDatetime>();
                getCurrentDatetimeMock.Setup(mock => mock.GetCurrentEstDatetime())
          .ReturnsAsync(DateTime.Now);
                mapperMock.Setup(mapper => mapper.Map<BatchRequest>(It.IsAny<BatchRequestResponse>()))
                    .Returns(new BatchRequest { });
                mapperMock.Setup(mapper => mapper.Map<BatchRequestResponse>(It.IsAny<BatchRequest>()))
                    .Returns(new BatchRequestResponse { });
                var repository = new BulkLettersRepository(dbContextOptions, context, _mockResourceManager.Object, mapperMock.Object, getCurrentDatetimeMock.Object);
                var batchRequest = fixture.Create<BatchRequestResponse>();

                // Act
                var result = await repository.AddBatchRequestDetails(batchRequest);

                // Assert
                Assert.NotNull(result);
                Assert.Equal(1, context.BatchRequest.Count());
                dbContext.Dispose();
            }
        }

        [Fact]
        public async Task UpdateBulkLetterStatus_WhenValidBatchIdProvided_ShouldUpdateDocumentStatusInDbContext()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;
            var dbContext = new DashboardDBContext(dbContextOptions);
            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var bulkLetters = new List<BulkLetters>
                {
                    new BulkLetters { BatchId = 1 },
                    new BulkLetters { BatchId = 2 },
                    new BulkLetters { BatchId = 2 },
                };
                context.BulkLetters.AddRange(bulkLetters);
                context.SaveChanges();

                var mockResourceManager = new Mock<ResourceManager>();
                mockResourceManager.Setup(r => r.GetString("UpdateBulkLetterstatusforAddBatchRequest"))
                    .Returns("In Progress"); 

                var repository = new BulkLettersRepository(dbContextOptions, context, mockResourceManager.Object, new Mock<IMapper>().Object, null);

                int batchIdToTest = 2;
                string modifiedByToTest = "Sample Name";

                // Act
                await repository.UpdateBulkLetterStatus(batchIdToTest, modifiedByToTest);

                // Assert
                var updatedBulkLetters = context.BulkLetters.Where(b => b.BatchId == batchIdToTest).ToList();
                Assert.All(updatedBulkLetters, b => Assert.Equal("In Progress", b.DocumentStatusName));
                dbContext.Dispose();
            }
        }

        [Fact]
        public async Task IsBatchrequestIdExists_WhenBatchIdExists_ShouldReturnTrue()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;
            var dbContext = new DashboardDBContext(dbContextOptions);
            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var batchRequests = new[]
                {
                new BatchRequest { BatchId = 1 },
                new BatchRequest { BatchId = 2 },
            };
                context.BatchRequest.AddRange(batchRequests);
                context.SaveChanges();

                var repository = new BulkLettersRepository(dbContextOptions, context, _mockResourceManager.Object, new Mock<IMapper>().Object, null);

                int batchIdToTest = 2;

                // Act
                bool result = await repository.IsBatchrequestIdExists(batchIdToTest);

                // Assert
                Assert.True(result);
                dbContext.Dispose();
            }
        }

        [Fact]
        public async Task IsBatchrequestIdExists_WhenBatchIdDoesNotExists_ShouldReturnFalse()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;
            var dbContext = new DashboardDBContext(dbContextOptions);
            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new BulkLettersRepository(dbContextOptions, context, _mockResourceManager.Object, new Mock<IMapper>().Object, null);

                int batchIdToTest = 3;

                // Act
                bool result = await repository.IsBatchrequestIdExists(batchIdToTest);

                // Assert
                Assert.False(result);
                dbContext.Dispose();
            }
        }

        [Fact]
        public async Task InsertAttachmentsBulkLetter_WhenEditAttachmentsModelIsNull_ShouldReturnFalse()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;
            var dbContext = new DashboardDBContext(dbContextOptions);
            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new BulkLettersRepository(dbContextOptions, context, _mockResourceManager.Object, new Mock<IMapper>().Object, null);

                // Act
                bool result = await repository.InsertAttachmentsBulkLetter(null);

                // Assert
                Assert.False(result);
                dbContext.Dispose();
            }
        }

        [Fact]
        public async Task GetBulkLetterAttachmentById_WhenAttachmentExists_ShouldReturnValidModel()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;
            var dbContext = new DashboardDBContext(dbContextOptions);
            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var batchId = 1;
                var templateId = 1;

                var bulkLetterAttachment = new BulkLetterAttachments
                {
                    BatchId = batchId,
                    TemplateId = templateId,
                    AttachmentsJSON = "AttachmentData",
                    AttachmentsURL = "AttachmentURL",
                    VersionNumber = 1
                };

                context.BulkLetterAttachments.Add(bulkLetterAttachment);
                context.SaveChanges();

                var repository = new BulkLettersRepository(dbContextOptions, context, _mockResourceManager.Object, new Mock<IMapper>().Object, null);

                // Act
                var result = await repository.GetBulkLetterAttachmentById(batchId, templateId);

                // Assert
                Assert.NotNull(result);
                Assert.Equal(bulkLetterAttachment.AttachmentsJSON, result.AttachmentsJSON);
                Assert.Equal(bulkLetterAttachment.AttachmentsURL, result.AttachmentsURL);
                Assert.Equal(bulkLetterAttachment.VersionNumber, result.VersionNumber);
                dbContext.Dispose();
            }
        }

        [Fact]
        public async Task DeleteByBatchId_WhenItemsExist_ShouldMarkItemsAsDeleted()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;
            var dbContext = new DashboardDBContext(dbContextOptions);
            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var batchId = 1;
                var deletedBy = "TestUser";

                var bulkLetters = new List<BulkLetters>
            {
                new BulkLetters { BatchId = batchId },
                new BulkLetters { BatchId = batchId },
                new BulkLetters { BatchId = batchId },
            };

                context.BulkLetters.AddRange(bulkLetters);
                context.SaveChanges();
                var getCurrentDatetimeMock = new Mock<IGetCurrentDatetime>();
                getCurrentDatetimeMock.Setup(mock => mock.GetCurrentEstDatetime())
          .ReturnsAsync(DateTime.Now);
                var repository = new BulkLettersRepository(dbContextOptions, context, _mockResourceManager.Object, new Mock<IMapper>().Object, getCurrentDatetimeMock.Object);
                var deleteByBatchResModel = new DeleteByBatchResModel
                {
                    BatchId = batchId,
                    DeletedBy = deletedBy
                };

                // Act
                bool result = await repository.DeleteByBatchId(deleteByBatchResModel);

                // Assert
                Assert.True(result);
                var deletedItems = context.BulkLetters.Where(b => b.BatchId == batchId && b.IsDeleted).ToList();
                foreach (var deletedItem in deletedItems)
                {
                    Assert.Equal(deletedBy, deletedItem.DeletedBy);
                    Assert.True(deletedItem.DeletedOn != default);
                    Assert.True(deletedItem.ModifiedOn != default);
                }
                dbContext.Dispose();
            }
        }


        [Fact]
        public async Task InsertFieldsList_WhenValidFieldsListProvided_ShouldInsertFieldValues()
        {
            // Arrange
            var fixture = new Fixture();
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;
            var dbContext = new DashboardDBContext(dbContextOptions);
            using (var context = new DashboardDBContext(dbContextOptions))
            {

                var fieldsList = fixture.Create<BulkLetterFieldsResponseModel>();

                var repository = new BulkLettersRepository(dbContextOptions, context, _mockResourceManager.Object, new Mock<IMapper>().Object, null);

                // Act
                var result = await repository.InsertFieldsList(fieldsList);

                // Assert
                Assert.NotNull(result);
                dbContext.Dispose();
            }
        }

        [Fact]
        public async Task GetPartnerOfficeListAsync_WhenBatchExists_ShouldReturnPartnerOfficeList()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;
            var dbContext = new DashboardDBContext(dbContextOptions);
            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var batchId = 1;

                var bulkLetters = new List<BulkLetters>
            {
                new BulkLetters { BatchId = batchId, PartnerName = "PartnerA", Office = "Office1" },
                new BulkLetters { BatchId = batchId, PartnerName = "PartnerA", Office = "Office2" },
                new BulkLetters { BatchId = batchId, PartnerName = "PartnerB", Office = "Office1" },
            };

                context.BulkLetters.AddRange(bulkLetters);
                context.SaveChanges();

                var repository = new BulkLettersRepository(dbContextOptions, context, _mockResourceManager.Object, new Mock<IMapper>().Object, null);

                // Act
                var result = await repository.GetPartnerOfficeListAsync(batchId);

                // Assert
                Assert.NotNull(result);
                Assert.Contains(result, p =>
                    p.PartnerName == "PartnerA" && p.Office == "Office1");
                Assert.Contains(result, p =>
                    p.PartnerName == "PartnerA" && p.Office == "Office2");
                Assert.Contains(result, p =>
                    p.PartnerName == "PartnerB" && p.Office == "Office1");
                dbContext.Dispose();
            }
        }
    }
}
