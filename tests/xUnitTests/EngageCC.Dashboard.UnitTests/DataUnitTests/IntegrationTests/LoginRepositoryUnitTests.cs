﻿using Dashboard.Data.Context;
using Dashboard.Data.DataModel;
using Dashboard.Data.Repositories;
using Dashboard.Models.Login;
using Dashboard.Models.ResponseModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.IntegrationTests
{
    public class LoginRepositoryUnitTests
    {
        [Fact]
        public async Task GetRoles_WithValidData_ShouldReturnRoles()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
           .UseInMemoryDatabase(databaseName: "Test_DashboardDB")
           .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new LoginRepository(dbContextOptions, context);

                var permissions = new LoginViewModel
                {
                    GroupIds = "1,2"
                };

                // Act
                var result = await repository.GetRoles(permissions);

                // Assert
                Assert.NotNull(result);
                Assert.NotNull(result.Roles);
            }

        }

        [Fact]
        public async Task GetRoles_WithInvalidPermissions_ShouldReturnNoRoles()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB")
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {

                var repository = new LoginRepository(dbContextOptions, context);

                var permissions = new LoginViewModel
                {
                    GroupIds = null
                };

                // Act
                var result = await repository.GetRoles(permissions);

                // Assert
                Assert.NotNull(result);
                Assert.Null(result.Roles);
            }
        }


        [Fact]
        public async Task GetRolesandScreenList_WithValidPermissions_ShouldReturnRolesAndScreenNames()
        {
            // Arrange

            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB")
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                context.AzureGroup.Add(new AzureGroup { GroupId = "Group1" });
                context.Roles.Add(new Roles {  RoleId = 1, RoleName = "Role1" });
                context.Screens.Add(new Screens { ScreenId = 1, ScreenName = "Screen1" });
                context.RolePermissionMapping.Add(new RolePermissionMapping { RoleId = 1, ScreenId = 1 });

                context.SaveChanges();

                var repository = new LoginRepository(dbContextOptions, context);

                var loginListModel = new LoginListModel
                {
                    GroupIds = "Group1"
                };

                // Act
                var result = await repository.GetRolesandScreenList(loginListModel);

                // Assert
                Assert.NotNull(result);
                Assert.Equal(1, context.Roles.Count());
                Assert.Equal(1, context.Screens.Count());
            }
        }

        [Fact]
        public async Task GetRolesandScreenList_WithInvalidPermissions_ShouldReturnEmptyResult()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB")
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {

                var repository = new LoginRepository(dbContextOptions, context);

                var loginListModel = new LoginListModel
                {
                    GroupIds = "NonExistentGroup"
                };

                // Act
                var result = await repository.GetRolesandScreenList(loginListModel);

                // Assert
                Assert.NotNull(result);
                Assert.Empty(result.Roles);     
                Assert.Empty(result.ScreenNames);
            }
        }

    }
}
