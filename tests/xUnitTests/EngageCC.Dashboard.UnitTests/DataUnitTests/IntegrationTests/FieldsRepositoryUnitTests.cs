﻿using AutoFixture;
using AutoMapper;
using Dashboard.Data.Context;
using Dashboard.Data.DataModel;
using Dashboard.Data.Repositories;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;
using Engage3.Common.Business.Interfaces;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.IntegrationTests
{
    public class FieldsRepositoryUnitTests
    {
        private Mock<ResourceManager> _resourceManager;
        private Mock<IMapper> _mapper;

        public FieldsRepositoryUnitTests()
        {
            _resourceManager = new Mock<ResourceManager>();
            _mapper = new Mock<IMapper>();
        }

        [Fact]
        public async void GetFields_ShouldReturnListOfFieldListModel()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                // Populate the in-memory database with test data
                var testData = new List<Field>
                {
                    new Field { FieldId = 1, FieldName = "Field1", StatusId = 1, FieldDataTypeId = 1, DirectInput = true, MDDLookup = false },
                    new Field { FieldId = 2, FieldName = "Field2", StatusId = 2, FieldDataTypeId = 2, DirectInput = false, MDDLookup = true },
                };
                context.Field.AddRange(testData);
                context.Status.AddRange(new[] { new Status { StatusId = 1, StatusName = "Active" }, new Status { StatusId = 2, StatusName = "Inactive" } });
                context.FieldDataTypes.AddRange(new[] { new FieldDataTypes { FieldDataTypeId = 1, FieldDataType = "DataType1" }, new FieldDataTypes { FieldDataTypeId = 2, FieldDataType = "DataType2" } });
                context.SaveChanges();

                var mockMapper = new Mock<IMapper>();
                var repository = new FieldsRepository(dbContextOptions, context, new Mock<ResourceManager>().Object, mockMapper.Object, null);

                // Act
                var result = await repository.GetFields();

                // Assert
                Assert.NotNull(result);
                var fieldList = result.ToList();
                Assert.Equal(2, fieldList.Count);
                Assert.Equal("Field1", fieldList[0].FieldName);
                Assert.Equal("Field2", fieldList[1].FieldName);
                Assert.Equal("Active", fieldList[0].StatusName);
                Assert.Equal("Inactive", fieldList[1].StatusName);
                Assert.Equal("DataType1", fieldList[0].FieldDataType);
                Assert.Equal("DataType2", fieldList[1].FieldDataType);
                Assert.True(fieldList[0].DirectInput);
                Assert.True(fieldList[1].MDDLookup);
                context.Dispose();
            }
        }

        [Fact]
        public async void GetFieldsById_ShouldReturnFieldsAndConnectedBlocks()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var testData = new List<Field>
                {
                    new Field
                    {
                        FieldId = 1,
                        FieldName = "Field1",
                        StatusId = 1,
                        FieldDataTypeId = 1,
                        DirectInput = true,
                        MDDLookup = false,
                        ColumnId = 1,
                    },
                };

                var blockData = new List<Block>
                {
                    new Block { Content = "BlockContent1", BlockName = "Block1" },
                    new Block { Content = "BlockContent2", BlockName = "Block2" },
                };

                var columnData = new List<ColumnNames>
                {
                    new ColumnNames { ColumnNamesId = 1, ColumnName = "ColumnName1" },
                };

                context.Field.AddRange(testData);
                context.Status.AddRange(new[] { new Status { StatusId = 1, StatusName = "Active" } });
                context.FieldDataTypes.AddRange(new[] { new FieldDataTypes { FieldDataTypeId = 1, FieldDataType = "DataType1" } });
                context.ColumnNames.AddRange(columnData);
                context.Block.AddRange(blockData);
                context.SaveChanges();

                var mockMapper = new Mock<IMapper>();
                mockMapper
                    .Setup(mapper => mapper.Map<FieldListModel>(It.IsAny<Field>()))
                    .Returns<Field>((field) =>
                        new FieldListModel
                        {
                            FieldName = field.FieldName,
                            FieldId = field.FieldId,
                            StatusName = context.Status.FirstOrDefault(s => s.StatusId == field.StatusId)?.StatusName,
                            FieldDataType = context.FieldDataTypes.FirstOrDefault(d => d.FieldDataTypeId == field.FieldDataTypeId)?.FieldDataType,
                            DirectInput = field.DirectInput,
                            MDDLookup = field.MDDLookup,
                            MDDColumnNames = context.ColumnNames.FirstOrDefault(c => c.ColumnNamesId == field.ColumnId)?.ColumnName,
                        });

                var repository = new FieldsRepository(dbContextOptions, context, new Mock<ResourceManager>().Object, mockMapper.Object, null);

                int fieldIdToTest = 1;

                // Act
                var result = await repository.GetFieldsById(fieldIdToTest);

                // Assert
                Assert.NotNull(result);
                Assert.NotEmpty(result.FieldsConnectedBlocks);
                Assert.Equal(2, result.FieldsConnectedBlocks.Count);
                Assert.Equal("BlockContent1", result.FieldsConnectedBlocks[0].Content);
                Assert.Equal("Block1", result.FieldsConnectedBlocks[0].BlockName);
                context.Dispose();
            }
        }

        [Fact]
        public async void GetActiveFields_ShouldReturnListOfActiveFieldListModel()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var testData = new List<Field>
                {
                    new Field
                    {
                        FieldId = 1,
                        FieldName = "Field1",
                        StatusId = 1,
                        FieldDataTypeId = 1,
                        DirectInput = true,
                        MDDLookup = false,
                        ColumnId = 1,
                        IsDeleted = false,
                    },
                    new Field
                    {
                        FieldId = 2,
                        FieldName = "Field2",
                        StatusId = 2,
                        FieldDataTypeId = 2,
                        DirectInput = false,
                        MDDLookup = true,
                        ColumnId = 2,
                        IsDeleted = false,
                    },
                };

                context.Field.AddRange(testData);
                context.Status.AddRange(new[] { new Status { StatusId = 1, StatusName = "Active" }, new Status { StatusId = 2, StatusName = "Inactive" } });
                context.FieldDataTypes.AddRange(new[] { new FieldDataTypes { FieldDataTypeId = 1, FieldDataType = "DataType1" }, new FieldDataTypes { FieldDataTypeId = 2, FieldDataType = "DataType2" } });
                context.ColumnNames.AddRange(new[] { new ColumnNames { ColumnNamesId = 1, ColumnName = "ColumnName1" }, new ColumnNames { ColumnNamesId = 2, ColumnName = "ColumnName2" } });
                context.SaveChanges();

                var mockMapper = new Mock<IMapper>();
                mockMapper
                    .Setup(mapper => mapper.Map<FieldListModel>(It.IsAny<Field>()))
                    .Returns<Field>((field) =>
                        new FieldListModel
                        {
                            FieldName = field.FieldName,
                            FieldId = field.FieldId,
                            StatusName = context.Status.FirstOrDefault(s => s.StatusId == field.StatusId)?.StatusName,
                            FieldDataType = context.FieldDataTypes.FirstOrDefault(d => d.FieldDataTypeId == field.FieldDataTypeId)?.FieldDataType,
                            DirectInput = field.DirectInput,
                            MDDLookup = field.MDDLookup,
                            MDDColumnNames = context.ColumnNames.FirstOrDefault(c => c.ColumnNamesId == field.ColumnId)?.ColumnName,
                        });

                var repository = new FieldsRepository(dbContextOptions, context, new Mock<ResourceManager>().Object, mockMapper.Object, null);

                // Act
                var result = await repository.GetActiveFields();

                // Assert
                Assert.NotNull(result);
                var activeFieldList = result.ToList();
                Assert.Equal(1, activeFieldList.Count);
                Assert.Equal("Field1", activeFieldList[0].FieldName);
                Assert.Equal("Active", activeFieldList[0].StatusName);
                context.Dispose();
            }
        }

        [Fact]
        public async void GetFieldsIdList_ShouldReturnListOfFieldsIdListResModel()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;
            var fixture = new Fixture();
            using (var context = new DashboardDBContext(options))
            {
                var testData = fixture.Create<Field>();

                context.Field.AddRange(testData);
                context.SaveChanges();
            }

            using (var context = new DashboardDBContext(options))
            {
                var repository = new FieldsRepository(options, context, new Mock<ResourceManager>().Object, new Mock<IMapper>().Object, null);

                // Act
                var result = await repository.GetFieldsIdList();

                // Assert
                Assert.NotNull(result);
                var fieldsIdList = result.ToList();
                context.Dispose();
            }
        }


        [Fact]
        public async void LoadStatus_ShouldReturnFieldsStatusResModel()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(options))
            {
                context.Status.AddRange(new[]
                {
                    new Status { StatusId = 1, StatusName = "Active" },
                    new Status { StatusId = 2, StatusName = "Inactive" },
                    new Status { StatusId = 3, StatusName = "Deprecated" },
                });

                context.FieldDataTypes.AddRange(new[]
                {
                    new FieldDataTypes { FieldDataTypeId = 1, FieldDataType = "DataType1" },
                    new FieldDataTypes { FieldDataTypeId = 2, FieldDataType = "DataType2" },
                    new FieldDataTypes { FieldDataTypeId = 3, FieldDataType = "DataType3" },
                });

                context.SaveChanges();
            }

            using (var context = new DashboardDBContext(options))
            {
                var repository = new FieldsRepository(options, context, new Mock<ResourceManager>().Object, new Mock<IMapper>().Object, null);

                // Act
                var result = await repository.LoadStatus();

                // Assert
                Assert.NotNull(result);
                Assert.NotNull(result.FieldsStatus);
                Assert.Equal(2, result.FieldsStatus.Count); 
                Assert.Contains(result.FieldsStatus, s => s.StatusId == 1);
                Assert.Contains(result.FieldsStatus, s => s.StatusId == 2);

                Assert.NotNull(result.FieldsDataType);
                Assert.Equal(3, result.FieldsDataType.Count); 
                Assert.Contains(result.FieldsDataType, dt => dt.FieldDataTypeId == 1);
                Assert.Contains(result.FieldsDataType, dt => dt.FieldDataTypeId == 2);
                context.Dispose();
            }
        }

        [Fact]
        public async void DeleteFields_ShouldMarkFieldAsDeleted()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(options))
            {
                var fieldToBeDeleted = new Field
                {
                    FieldId = 1,
                    IsDeleted = false,
                    ModifiedOn = DateTime.Now,
                    DeletedBy = null,
                    DeletedOn = null,
                };

                context.Field.Add(fieldToBeDeleted);
                context.SaveChanges();
            }

            using (var context = new DashboardDBContext(options))
            {
                var getCurrentDatetimeMock = new Mock<IGetCurrentDatetime>();
                getCurrentDatetimeMock.Setup(mock => mock.GetCurrentEstDatetime())
          .ReturnsAsync(DateTime.Now);
                var repository = new FieldsRepository(options, context, new Mock<ResourceManager>().Object, new Mock<IMapper>().Object, getCurrentDatetimeMock.Object);

                var deleteFieldRequest = new DeleteFieldResModel
                {
                    FieldId = new int[] { 1 },
                    DeletedBy = "TestUser",
                };

                // Act
                var result = await repository.DeleteFields(deleteFieldRequest);

                // Assert
                Assert.NotNull(result);
                Assert.Equal(1, result.FieldId[0]); 
                var deletedField = await context.Field.FirstOrDefaultAsync(f => f.FieldId == 1);
                Assert.NotNull(deletedField);
                Assert.Equal("TestUser", deletedField.DeletedBy);
                Assert.NotNull(deletedField.DeletedOn); 
                Assert.NotNull(deletedField.ModifiedOn);
                context.Dispose();
            }
        }

        [Fact]
        public async void AddField_ShouldAddNewFieldAndReturnUpdatedFieldObject()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(options))
            {
                var getCurrentDatetimeMock = new Mock<IGetCurrentDatetime>();
                getCurrentDatetimeMock.Setup(mock => mock.GetCurrentEstDatetime())
          .ReturnsAsync(DateTime.Now);
                var repository = new FieldsRepository(options, context, new Mock<ResourceManager>().Object, new Mock<IMapper>().Object, getCurrentDatetimeMock.Object);

                var newField = new Field
                {
                    FieldId = 0,

                };

                // Act
                var result = await repository.AddField(newField);

                // Assert
                Assert.NotNull(result);
                Assert.NotEqual(0, result.FieldId); 
                Assert.NotNull(result.CreatedOn); 
                Assert.NotNull(result.ModifiedOn); 
                var addedField = await context.Field.FirstOrDefaultAsync(f => f.FieldId == result.FieldId);
                Assert.NotNull(addedField);
                context.Dispose();
            }
        }

        [Fact]
        public async void UpdateField_ShouldUpdateExistingFieldAndReturnUpdatedFieldObject()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(options))
            {
                var existingField = new Field
                {
                    FieldId = 1,
                };

                context.Field.Add(existingField);
                context.SaveChanges();
            }

            using (var context = new DashboardDBContext(options))
            {
                var getCurrentDatetimeMock = new Mock<IGetCurrentDatetime>();
                getCurrentDatetimeMock.Setup(mock => mock.GetCurrentEstDatetime())
          .ReturnsAsync(DateTime.Now);
                var repository = new FieldsRepository(options, context, new Mock<ResourceManager>().Object, new Mock<IMapper>().Object, getCurrentDatetimeMock.Object);

                var updatedField = new Field
                {
                    FieldId = 1, 
                };

                // Act
                var result = await repository.UpdateField(updatedField);

                // Assert
                Assert.NotNull(result);
                Assert.Equal(1, result.FieldId);
                Assert.NotNull(result.ModifiedOn);
                var updatedDatabaseField = await context.Field.FirstOrDefaultAsync(f => f.FieldId == result.FieldId);
                Assert.NotNull(updatedDatabaseField);
                context.Dispose();
            }
        }

        [Fact]
        public async void IsValidFieldName_ShouldReturnTrueIfFieldNameIsValid()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(options))
            {
                var existingField1 = new Field
                {
                    FieldId = 1,
                    FieldName = "Field1",
                    IsDeleted = false,
                };
                var existingField2 = new Field
                {
                    FieldId = 2,
                    FieldName = "Field2",
                    IsDeleted = false,
                };

                context.Field.AddRange(existingField1, existingField2);
                context.SaveChanges();
            }

            using (var context = new DashboardDBContext(options))
            {
                var repository = new FieldsRepository(options, context, new Mock<ResourceManager>().Object, new Mock<IMapper>().Object, null);

                var newField = new FieldListModel
                {
                    FieldId = 0,
                    FieldName = "NewField",
                };

                // Act
                var result = await repository.IsValidFieldName(newField);

                // Assert
                Assert.True(result);
                context.Dispose();
            }
        }

        [Fact]
        public async void IsValidFieldName_ShouldReturnFalseIfFieldNameIsInvalid()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(options))
            {
                var existingField = new Field
                {
                    FieldId = 1,
                    FieldName = "ExistingField",
                    IsDeleted = false,
                };

                context.Field.Add(existingField);
                context.SaveChanges();
            }

            using (var context = new DashboardDBContext(options))
            {
                var repository = new FieldsRepository(options, context, new Mock<ResourceManager>().Object, new Mock<IMapper>().Object, null);

                var existingFieldWithSameName = new FieldListModel
                {
                    FieldId = 0, 
                    FieldName = "ExistingField",
                };

                // Act
                var result = await repository.IsValidFieldName(existingFieldWithSameName);

                // Assert
                Assert.False(result);
                context.Dispose();
            }
        }

        [Fact]
        public async void GetColumnNames_ShouldReturnListOfColumnNames()
        {
            // Arrange
            var options = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(options))
            {
                var columnNames = new List<ColumnNames>
                {
                    new ColumnNames { ColumnNamesId = 1, ColumnName = "Column1" },
                    new ColumnNames { ColumnNamesId = 2, ColumnName = "Column2" },
                    new ColumnNames { ColumnNamesId = 3, ColumnName = "Column3" },
                };

                context.ColumnNames.AddRange(columnNames);
                context.SaveChanges();
            }

            using (var context = new DashboardDBContext(options))
            {
                var repository = new FieldsRepository(options, context, new Mock<ResourceManager>().Object, new Mock<IMapper>().Object, null);

                // Act
                var result = await repository.GetColumnNames();

                // Assert
                Assert.NotNull(result);
                var columnNamesList = result.ToList();
                Assert.Equal(3, columnNamesList.Count); 
                Assert.Contains(columnNamesList, cn => cn.ColumnId == 1);
                Assert.Contains(columnNamesList, cn => cn.ColumnId == 2);
                Assert.Contains(columnNamesList, cn => cn.ColumnId == 3);
                context.Dispose();
            }
        }

    }
}
