﻿using AutoMapper;
using Dashboard.Data.Context;
using Dashboard.Data.DataModel;
using Dashboard.Data.Repositories;
using Dashboard.Models.Signing;
using Engage3.Common.Business.Interfaces;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.IntegrationTests
{
    public class SigningRepositoryUnitTests
    {
        [Fact]
        public async Task GetEngagementLetterStatus_WithValidEngLetterId_ShouldReturnEngagementLetter()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {

                var engagementLetter = new EngagementLetter
                {
                    EngagementLetterId = 1,
                    IsDeleted = false
                };

                context.EngagementLetter.Add(engagementLetter);
                context.SaveChanges();

                var repository = new SigningRepository(dbContextOptions, context, null, null, null);

                // Act
                var result = await repository.GetEngagementLetterStatus(1);

                // Assert
                Assert.NotNull(result);
                Assert.Equal(1, result.EngagementLetterId);
                Assert.False(result.IsDeleted);
            }
        }

        [Fact]
        public async Task GetEngagementLetterStatus_WithDeletedEngLetterId_ShouldReturnNull()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB")
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var engagementLetter = new EngagementLetter
                {
                    EngagementLetterId = 1,
                    IsDeleted = true
                };

                context.EngagementLetter.Add(engagementLetter);
                context.SaveChanges();

                var repository = new SigningRepository(dbContextOptions, context, null, null, null);

                // Act
                var result = await repository.GetEngagementLetterStatus(1);

                // Assert
                Assert.Null(result);
            }
        }

        [Fact]
        public async Task GetAllEngagementLetter_WithMatchingEngagementLetters_ShouldReturnList()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                context.EngagementLetter.Add(new EngagementLetter
                {
                    EngagementLetterId = 1,
                    DocumentStatusId = 2,
                    IsDeleted = false
                });

                context.EngagementLetter.Add(new EngagementLetter
                {
                    EngagementLetterId = 2,
                    DocumentStatusId = 2,
                    IsDeleted = false
                });

                context.SaveChanges();

                var repository = new SigningRepository(dbContextOptions, context, null, null, null);

                // Act
                var result = await repository.GetAllEngagementLetter();

                // Assert
                Assert.NotNull(result);
                Assert.Equal(new List<int> { 1, 2 }, result);
            }
        }

        [Fact]
        public async Task GetAllEngagementLetter_WithNoMatchingEngagementLetters_ShouldReturnEmptyList()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB")
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {

                var repository = new SigningRepository(dbContextOptions, context, null, null, null);

                // Act
                var result = await repository.GetAllEngagementLetter();

                // Assert
                Assert.NotNull(result);
                Assert.Empty(result);
            }
        }


        [Fact]
        public async Task GetPdfUrl_WithExistingEngagementLetterId_ShouldReturnPdfUrl()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB")
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                context.SigningInfo.Add(new SigningInfo
                {
                    EngagementLetterId = 1,
                    PdfUrl = "https://example.com/pdf1",
                    IsDeleted = false
                });

                context.SigningInfo.Add(new SigningInfo
                {
                    EngagementLetterId = 2,
                    PdfUrl = "https://example.com/pdf2",
                    IsDeleted = false
                });

                context.SaveChanges();

                var repository = new SigningRepository(dbContextOptions, context, null, null, null);

                // Act
                var result = await repository.GetPdfUrl(1);

                // Assert
                Assert.NotNull(result);
                Assert.Equal("https://example.com/pdf1", result);
            }
        }

        [Fact]
        public async Task GetPdfUrl_WithDeletedSigningInfo_ShouldReturnNull()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB")
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                context.SigningInfo.Add(new SigningInfo
                {
                    EngagementLetterId = 1,
                    PdfUrl = "https://example.com/pdf1",
                    IsDeleted = true
                });

                context.SaveChanges();

                var repository = new SigningRepository(dbContextOptions, context, null, null, null);

                // Act
                var result = await repository.GetPdfUrl(1);

                // Assert
                Assert.Null(result);
            }
        }

        [Fact]
        public async Task GetPdfUrl_WithDeletedStatusSigningInfo_ShouldReturnNull()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                context.SigningInfo.Add(new SigningInfo
                {
                    EngagementLetterId = 1,
                    PdfUrl = "https://example.com/pdf1",
                    IsDeleted = true
                });

                context.SaveChanges();

                var repository = new SigningRepository(dbContextOptions, context, null, null, null);

                // Act
                var result = await repository.GetPdfUrl(1);

                // Assert
                Assert.Null(result);
            }
        }

        [Fact]
        public async Task GetAgreementId_WithExistingSigningInfo_ShouldReturnList()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                context.SigningInfo.Add(new SigningInfo
                {
                    SigningInfoId = 1,
                    IsDeleted = false
                });

                context.SigningInfo.Add(new SigningInfo
                {
                    SigningInfoId = 2,
                    IsDeleted = false
                });

                context.SaveChanges();

                var repository = new SigningRepository(dbContextOptions, context, null, null, null);

                // Act
                var result = await repository.GetAgreementId();

                // Assert
                Assert.NotNull(result);
                Assert.Equal(2, result.Count());
            }
        }

        [Fact]
        public async Task GetAgreementId_WithDeletedSigningInfo_ShouldReturnEmptyList()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                context.SigningInfo.Add(new SigningInfo
                {
                    SigningInfoId = 1,
                    IsDeleted = true
                });
                context.SaveChanges();

                var repository = new SigningRepository(dbContextOptions, context, null, null, null);

                // Act
                var result = await repository.GetAgreementId();

                // Assert
                Assert.NotNull(result);
                Assert.Empty(result);
            }
        }

        [Fact]
        public async Task UpdateSignedStatusEngagementLetter_WithValidEngagementLetter_ShouldUpdateAndReturnEngagementLetter()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var engagementLetter = new EngagementLetter
                {
                    EngagementLetterId = 1,
                    DocumentStatusId = 1, 
                    ModifiedOn = DateTime.Now.AddDays(-1)
                };

                context.EngagementLetter.Add(engagementLetter);
                context.SaveChanges();

                var getCurrentDatetimeMock = new Mock<IGetCurrentDatetime>();
                getCurrentDatetimeMock.Setup(mock => mock.GetCurrentEstDatetime())
          .ReturnsAsync(DateTime.Now);
                var repository = new SigningRepository(dbContextOptions, context, null, null, getCurrentDatetimeMock.Object);

                engagementLetter.DocumentStatusId = 2;
                engagementLetter.ModifiedOn = DateTime.Now;

                // Act
                var result = await repository.UpdateSignedStatusEngagementLetter(engagementLetter);

                // Assert
                Assert.NotNull(result);
                Assert.Equal(2, result.DocumentStatusId);
            }
        }

        [Fact]
        public async Task UpdateSignedStatusEngagementLetter_WithInvalidEngagementLetter_ShouldNotUpdateAndReturnOriginalEngagementLetter()
       {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {

                var engagementLetter = new EngagementLetter
                {
                    EngagementLetterId = 1,
                    DocumentStatusId = 1,
                    ModifiedOn = DateTime.Now.AddDays(-1) 
                };

                context.EngagementLetter.Add(engagementLetter);
                context.SaveChanges();

                var repository = new SigningRepository(dbContextOptions, context, null, null, null);
  
                engagementLetter.DocumentStatusId = 0;
                engagementLetter.ModifiedOn = DateTime.Now;

                // Act
                var result = await repository.UpdateSignedStatusEngagementLetter(engagementLetter);

                // Assert
                Assert.NotNull(result);
                Assert.NotEqual(1, result.DocumentStatusId); // Check that DocumentStatusId was not updated
            }
        }

        [Fact]
        public async Task GetHistoryLogVersion_WithValidEngagementId_ShouldReturnVersion()
        {
            // Arrange
            var engagementId = 1;
            var expectedVersion = 3;

            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var historyLogs = new List<HistoryLog>
                {
                     new HistoryLog { EngagementLetterId = engagementId, Version = 1, Status = "Draft" },
                     new HistoryLog { EngagementLetterId = engagementId, Version = 2, Status = "Signed" },
                     new HistoryLog { EngagementLetterId = engagementId, Version = 3, Status = "Finalized" },
                };

                context.HistoryLog.AddRange(historyLogs);
                context.SaveChanges();

                var repository = new SigningRepository(dbContextOptions, context, null, null, null);

                // Act
                var result = await repository.GetHistoryLogVersion(engagementId);

                // Assert
                Assert.Equal(expectedVersion, result);
            }
        }

        [Fact]
        public async Task GetHistoryLogVersion_WithNonexistentEngagementId_ShouldReturnNull()
        {
            // Arrange
            var engagementId = 123;

            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {

                var repository = new SigningRepository(dbContextOptions, context, null, null, null);

                // Act
                var result = await repository.GetHistoryLogVersion(engagementId);

                // Assert
                Assert.Null(result);
            }
        }

        [Fact]
        public async Task InsertHistoryLog_WithValidHistoryLog_ShouldAddToDatabase()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new SigningRepository(dbContextOptions, context, null, null, null);

                var historyLog = new HistoryLog
                {
                    HistoryLogId= 1,
                    EngagementLetterId = 1,
                    Version = 1,
                };

                // Act
                await repository.InsertHistoryLog(historyLog);

                // Assert
                Assert.Equal(1, context.HistoryLog.Count());
                var addedHistoryLog = await context.HistoryLog.FirstOrDefaultAsync();
                Assert.NotNull(addedHistoryLog);
                Assert.Equal(1, addedHistoryLog.EngagementLetterId);
                Assert.Equal(1, addedHistoryLog.Version);
            }
        }

        [Fact]
        public async Task InsertHistoryLog_WithNullHistoryLog_ShouldNotAddToDatabase()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var repository = new SigningRepository(dbContextOptions, context, null, null, null);

                // Act
                await repository.InsertHistoryLog(null); // Passing null as the HistoryLog

                // Assert
                Assert.Equal(0, context.HistoryLog.Count()); // Ensure no records were added
            }
        }


        [Fact]
        public async Task GetHistoryLogStatus_WithValidEngagementId_ShouldReturnStatus()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var engagementId = 1;
                var historyLogs = new List<HistoryLog>
            {
                new HistoryLog { EngagementLetterId = engagementId, Version = 1, Status = "Draft" },
                new HistoryLog { EngagementLetterId = engagementId, Version = 2, Status = "Signed" },
                new HistoryLog { EngagementLetterId = engagementId, Version = 3, Status = "Finalized" },
            };

                context.HistoryLog.AddRange(historyLogs);
                context.SaveChanges();

                var repository = new SigningRepository(dbContextOptions, context, null, null, null);

                // Act
                var result = await repository.GetHistoryLogStatus(engagementId);

                // Assert
                Assert.NotNull(result);
                Assert.Equal("Finalized", result);
            }
        }

        [Fact]
        public async Task GetHistoryLogStatus_WithNonexistentEngagementId_ShouldReturnNull()
        {
            // Arrange
            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {

                var repository = new SigningRepository(dbContextOptions, context, null, null, null);

                // Act
                var result = await repository.GetHistoryLogStatus(1);

                // Assert
                Assert.Null(result);
            }
        }

        [Fact]
        public async Task InsertLetterStatusReports_WithValidInput_ShouldInsertAndReturnInput()
        {
            // Arrange
            var letterStatusRequest = new LetterStatusRequest
            {
            };

            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var mapper = new Mapper(new MapperConfiguration(cfg => {
                    cfg.CreateMap<LetterStatusRequest, LettersStatusReports>();
                }));

                var repository = new SigningRepository(dbContextOptions, context,null,mapper, null);

                // Act
                var result = await repository.InsertLetterStatusReports(letterStatusRequest);

                // Assert
                Assert.NotNull(result);
                var insertedEntity = await context.LettersStatusReports.FirstOrDefaultAsync();
                Assert.NotNull(insertedEntity);
            }
        }

        [Fact]
        public async Task InsertLetterStatusReports_WithInvalidInput_ShouldNotInsertAndReturnInput()
        {
            // Arrange
            var invalidLetterStatusRequest = new LetterStatusRequest
            {
                LettersStatusReportId=1
            };

            var dbContextOptions = new DbContextOptionsBuilder<DashboardDBContext>()
                .UseInMemoryDatabase(databaseName: "Test_DashboardDB_" + Guid.NewGuid().ToString())
                .Options;

            using (var context = new DashboardDBContext(dbContextOptions))
            {
                var mapper = new Mapper(new MapperConfiguration(cfg => {
                    cfg.CreateMap<LetterStatusRequest, LettersStatusReports>();
                }));

                var repository = new SigningRepository(dbContextOptions, context, null, mapper, null);

                // Act
                var result = await repository.InsertLetterStatusReports(invalidLetterStatusRequest);

                // Assert
                Assert.NotNull(result);
                var insertedEntity = await context.LettersStatusReports.FirstOrDefaultAsync();
                Assert.Null(insertedEntity);
            }
        }
    }
}
