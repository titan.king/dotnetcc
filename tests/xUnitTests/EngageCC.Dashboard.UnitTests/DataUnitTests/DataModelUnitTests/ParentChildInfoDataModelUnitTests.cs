﻿using Dashboard.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.DataModelUnitTests
{
    public class ParentChildInfoDataModelUnitTests
    {
        [Fact]
        public void ParentChildInfo_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new ParentChildInfo
            {
                ParentClientId = 1,
                Child1 = "Sample Child 1",
                Child2 = "Sample Child 2",
                Child3 = "Sample Child 3",
                Child4 = "Sample Child 4",
                Child5 = "Sample Child 5",
                Child6 = "Sample Child 6",
                Child7 = "Sample Child 7",
                Child8 = "Sample Child 8",
                Child9 = "Sample Child 9",
                Child10 = "Sample Child 10",
                CreatedOn = DateTime.Now, 
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
