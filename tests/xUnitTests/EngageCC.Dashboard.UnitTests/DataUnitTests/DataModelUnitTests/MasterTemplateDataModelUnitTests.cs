﻿using Dashboard.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.DataModelUnitTests
{
    public class MasterTemplateDataModelUnitTests
    {
        [Fact]
        public void MasterTemplate_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new MasterTemplate
            {
                TemplateId = 1,
                TemplateName = "Sample Template Name",
                TemplateVersionId = 2,
                Template = "Sample Template Content",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
