﻿using Dashboard.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.DataModelUnitTests
{
    public class HistoryLogDataModelUnitTests
    {
        [Fact]
        public void HistoryLog_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new HistoryLog
            {
                EngagementLetterId = 1, 
                EngagementLetterName = "Sample Engagement Letter Name",
                BatchId = 2, 
                Status = "Sample Status",
                EditedBy = "Sample Edited By",
                Version = 3, 
                LastModified = DateTime.Now, 
                Downloaded = null, 
                Delegated = true, 
                ClientEmailId = "sample@example.com",
                ReasonforDecline = "Sample Reason for Decline",
                DeclineTimestamp = DateTime.Now, 
                PDFUrl = "Sample PDF URL",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
