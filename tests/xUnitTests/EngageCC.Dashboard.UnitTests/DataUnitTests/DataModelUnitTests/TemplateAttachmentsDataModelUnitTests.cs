﻿using Dashboard.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.DataModelUnitTests
{
    public class TemplateAttachmentsDataModelUnitTests
    {

        [Fact]
        public void TemplateAttachments_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new TemplateAttachments
            {
                TemplateAttachmentsId = 1,
                TemplateId = 2,
                AttachmentJson = "{'attachment1': 'data1', 'attachment2': 'data2'}", 
                CreatedOn = DateTime.Now,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
