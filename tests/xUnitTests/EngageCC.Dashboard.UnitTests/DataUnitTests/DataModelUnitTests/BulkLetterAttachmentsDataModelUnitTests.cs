﻿using Dashboard.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.DataModelUnitTests
{
    public class BulkLetterAttachmentsDataModelUnitTests
    {
        [Fact]
        public void BulkLetterAttachments_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new BulkLetterAttachments
            {
                BatchId = 1, 
                TemplateId = 2,
                AttachmentsJSON = "Valid JSON", 
                AttachmentsURL = "https://example.com/valid-url", 
                VersionNumber = 3, 
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
