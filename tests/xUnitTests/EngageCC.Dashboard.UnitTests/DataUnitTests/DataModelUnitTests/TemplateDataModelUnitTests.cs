﻿using Dashboard.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.DataModelUnitTests
{
    public class TemplateDataModelUnitTests
    {
        [Fact]
        public void Template_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new Template
            {
                TemplateId = 1,
                TemplateName = "Sample Template",
                TemplateDescription = "Sample Template Description",
                DepartmentId = 1,
                EngageTypeId = 2,
                ChangeNotes = "Sample Change Notes",
                CreatedOn = DateTime.Now,
                CreatedBy = "JohnDoe",
                StatusId = 3,
                AttachmentURL = "http://example.com/template.pdf", 
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
