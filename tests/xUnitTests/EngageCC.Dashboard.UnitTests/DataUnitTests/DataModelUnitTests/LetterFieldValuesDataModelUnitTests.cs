﻿using Dashboard.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.DataModelUnitTests
{
    public class LetterFieldValuesDataModelUnitTests
    {
        [Fact]
        public void LetterFieldValues_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new LetterFieldValues
            {
                ClientId = 1, 
                PartnerId = 2,
                BulkLettersId = 3, 
                FieldId = 4, 
                FieldValue = "Sample Field Value",
                FieldName = "Sample Field Name",
                LetterVersion = 5,
                DisplayName = "Sample Display Name",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
