﻿using Dashboard.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.DataModelUnitTests
{
    public class BlockDataModelUnitTests
    {
        [Fact]
        public void Block_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new Block
            {
                BlockId = 1,
                BlockName = "Sample Block Name",
                Description = "Sample Description",
                Content = "Sample Content",
                ChangeNotes = "Sample Change Notes",
                IsDeleted = false, 
                StatusId = 2, 
                Status = new Status 
                {
                    StatusId = 3,
                    StatusName = "Test"
                },
                CreatedOn = DateTime.Now, 
                ModifiedOn = null, 
                CreatedBy = "Sample CreatedBy",
                ModifiedBy = null,
                DeletedOn = null, 
                DeletedBy = null,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
