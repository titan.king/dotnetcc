﻿using Dashboard.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.DataModelUnitTests
{
    public class PartnersDataModelUnitTests
    {
        [Fact]
        public void Partners_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new Partners
            {
                PartnerId = 1,
                PartnerName = "Sample Partner Name",
                CreatedOn = DateTime.Now, 
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
