﻿using Dashboard.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.DataModelUnitTests
{
    public class BatchRequestDataModelUnitTests
    {
        [Fact]
        public void BatchRequest_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new BatchRequest
            {
                BatchRequestId = 1,
                BatchId = 2,
                BatchJson = "Valid Batch JSON",
                IsProcess = true,
                Status = "Valid Status",
                TemplateId = 3,
                TemplateName = "Valid Template Name",
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                CreatedBy = "Valid Created By",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
