﻿using Dashboard.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.DataModelUnitTests
{
    public class SigningInfoDataModelUnitTests
    {
        [Fact]
        public void SigningInfo_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new SigningInfo
            {
                SigningInfoId = 1,
                EngagementLetterId = 1,
                SigningPartnerName = "Sample Partner Name",
                DelegatedName = "Sample Delegated Name",
                SPEmail = "sample@example.com",
                ContactPerson1Name = "Contact Person 1",
                ContactPerson1Email = "contact1@example.com",
                ContactPerson2Name = "Contact Person 2",
                ContactPerson2Email = "contact2@example.com",
                Title = "Sample Title",
                Message = "Sample Message",
                CreatedBy = "Sample Creator",
                CreatedOn = DateTime.Now,
                ModifiedOn = null, 
                ManualSigning = true, 
                FileId = "SampleFile123",
                DelegatedBy = "Delegated User",
                AgreementId = "Agreement123",
                ModifiedBy = "Modified User",
                DeletedOn = null, 
                DeletedBy = "Deleted User",
                PdfUrl = "https://example.com/sample.pdf",
                ExpiryDate7216 = "2023-12-31", 
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
