﻿using Dashboard.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.DataModelUnitTests
{
    public class AzureGroupDataModelUnitTests
    {
        [Fact]
        public void AzureGroup_ValidModel_PassesValidation()
        {
            // Arrange
            var azureGroup = new AzureGroup
            {
                AzureGroupId = 1,
                GroupId = "Valid Group Id",
                GroupName = "Valid Group Name",
            };

            // Act
            var validationContext = new ValidationContext(azureGroup);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(azureGroup, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
