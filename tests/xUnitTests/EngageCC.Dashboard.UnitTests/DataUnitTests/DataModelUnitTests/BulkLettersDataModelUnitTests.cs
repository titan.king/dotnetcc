﻿using Dashboard.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.DataModelUnitTests
{
    public class BulkLettersDataModelUnitTests
    {
        [Fact]
        public void BulkLetters_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new BulkLetters
            {
                ClientId = 1, 
                ClientName = "Sample Client Name",
                Office = "Sample Office",
                SignatoryEmailId = "sample@example.com",
                SignatoryFirstName = "Sample Signatory First Name",
                PartnerName = "Sample Partner Name",
                BatchId = 2, 
                TemplateName = "Sample Template Name",
                FieldJson = "Sample Field JSON",
                DocumentStatusName = "Sample Document Status",
                CreatedOn = DateTime.Now, 
                ModifiedOn = null, 
                CreatedBy = "Sample CreatedBy",
                ModifiedBy = null,
                DeletedOn = null, 
                DeletedBy = null, 
                IsBulkLetter = true,
                IsProcess = false,
                IsEnqueue = false,
                PartnerId = 3,
                IsEsigning = true, 
                Is7216Available = false,
                ClientSignatureCount = 4, 
                IsNewClient = false,
                SpouseFirstName = "Sample Spouse First Name",
                SpouseEmailId = "spouse@example.com",
                TaxYear = "2023", 
                OfficeId = 5, 
                letterFieldValues = new System.Collections.Generic.List<LetterFieldValues>(),
                IsBatchActive = true,
                IsBatchDelete = false,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
