﻿using Dashboard.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.DataModelUnitTests
{
    public class FieldDataModelUnitTests
    {
        [Fact]
        public void Field_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new Field
            {
                FieldName = "SampleFieldName",
                DisplayName = "Sample Display Name",
                FieldDataTypeId = 1,
                HintText = "Sample Hint Text",
                ChangeNotes = "Sample Change Notes",
                IsDeleted = false, 
                CreatedOn = DateTime.Now, 
                ModifiedOn = null, 
                DirectInput = false, 
                MDDLookup = false,
                ColumnId = 2,
                MDDColumnNames = "Sample MDD Column Names",
                MDDColumnData = "Sample MDD Column Data",
                CreatedBy = "Sample CreatedBy",
                ModifiedBy = null, 
                DeletedOn = null, 
                DeletedBy = null, 
                StatusId = 3, 
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
