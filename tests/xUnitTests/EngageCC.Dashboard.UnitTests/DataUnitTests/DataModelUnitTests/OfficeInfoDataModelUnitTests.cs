﻿using Dashboard.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.DataModelUnitTests
{
    public class OfficeInfoDataModelUnitTests
    {
        [Fact]
        public void OfficeInfo_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new OfficeInfo
            {
                OfficeId = 1,
                Name = "Sample Office Name",
                Office = "Sample Office",
                Address = "Sample Address",
                City = "Sample City",
                State = "Sample State",
                ZipCode = "Sample ZipCode",
                StateSignatureNameAddress = "Sample State Signature Name/Address",
                StateSignatureCity = "Sample State Signature City",
                StateSignatureState = "Sample State Signature State",
                StateSignatureZipCode = "Sample State Signature ZipCode",
                PhoneNumber = "Sample Phone Number",
                CreatedOn = DateTime.Now, 
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
