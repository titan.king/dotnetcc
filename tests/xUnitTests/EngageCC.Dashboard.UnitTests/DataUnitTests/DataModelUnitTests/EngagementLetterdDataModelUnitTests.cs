﻿using Dashboard.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.DataModelUnitTests
{
    public class EngagementLetterdDataModelUnitTests
    {

        [Fact]
        public void EngagementLetter_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new EngagementLetter
            {
                BatchId = 1, 
                EngagementLetterName = "Sample Engagement Letter Name",
                OfficeId = 2, 
                OfficeName = "Sample Office Name",
                YearId = 3, 
                TaxYear = "2023",
                TemplateId = 4, 
                TemplateName = "Sample Template Name",
                TemplateVersion = 5, 
                ClientId = 6, 
                ClientName = "Sample Client Name",
                PartnerId = 7, 
                PartnerName = "Sample Partner Name",
                AdminId = 8, 
                AdminName = "Sample Admin Name",
                DocumentStatusId = 9, 
                DepartmentId = 10, 
                EngageTypeId = 11,
                CreatedOn = DateTime.Now, 
                CreatedBy = "Sample CreatedBy",
                ModifiedOn = null,
                IsDeleted = false, 
                DeletedOn = null, 
                DeletedBy = null, 
                ModifiedBy = null, 
                BulkLettersId = 12,
                IsProcess = false, 
                IsEnqueue = false, 
                Is7216Available = false,
                ClientSignatureCount = 13, 
                IsNewClient = false, 
                SpouseFirstName = "Sample Spouse First Name",
                SpouseEmailId = "sample@example.com",
                SignatoryEmailId = "signatory@example.com",
                SignatoryFirstName = "Sample Signatory First Name",
                IsEsigning = true, 
                OtherEntityDetails = "Sample Other Entity Details",
                PrimarySignerLastName = "Sample Primary Signer Last Name",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
