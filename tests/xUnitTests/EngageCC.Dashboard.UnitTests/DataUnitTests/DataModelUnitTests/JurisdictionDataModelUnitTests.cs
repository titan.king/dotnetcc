﻿using Dashboard.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.DataModelUnitTests
{
    public class JurisdictionDataModelUnitTests
    {
        [Fact]
        public void Jurisdiction_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new Jurisdiction
            {
                JurisdictionId = 1, 
                CitrinDatabase = "Sample Citrin Database",
                ReceivedFromCitrin = "Sample Received From Citrin",
                JurisdictionData = "Sample Jurisdiction Data",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
