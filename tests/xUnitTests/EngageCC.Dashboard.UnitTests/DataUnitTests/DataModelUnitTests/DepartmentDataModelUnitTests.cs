﻿using Dashboard.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.DataModelUnitTests
{
    public class DepartmentDataModelUnitTests
    {
        [Fact]
        public void Department_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new Department
            {
                DepartmentName = "Sample Department Name",
                IsDeleted = false,
                DepartmentStatusId = 1, 
                CreatedOn = DateTime.Now, 
                ModifiedOn = null, 
                CreatedBy = "Sample CreatedBy",
                ModifiedBy = null, 
                DeletedOn = null, 
                DeletedBy = null, 
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
