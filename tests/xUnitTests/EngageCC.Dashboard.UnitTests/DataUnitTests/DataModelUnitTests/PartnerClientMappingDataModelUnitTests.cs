﻿using Dashboard.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.DataModelUnitTests
{
    public class PartnerClientMappingDataModelUnitTests
    {
        [Fact]
        public void PartnerClientMapping_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new PartnerClientMapping
            {
                PartnerId = 1,
                PartnerName = "Sample Partner Name",
                ParentClientId = 2,
                ParentClientName = "Sample Parent Client Name",
                ChildClient = "Sample Child Client",
                AddressDetail = "Sample Address Detail",
                City = "Sample City",
                State = "Sample State",
                ZipCode = "Sample ZipCode",
                ContactName = "Sample Contact Name",
                ContactEmail = "sample@example.com",
                CreatedOn = DateTime.Now, 
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
