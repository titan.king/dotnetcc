﻿using Dashboard.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.DataModelUnitTests
{
    public class LettersStatusReportsDataModelUnitTests
    {
        [Fact]
        public void LettersStatusReports_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new LettersStatusReports
            {
                EngagementLetterId = 1,
                EngagementLetterName = "Sample Engagement Letter Name",
                ClientId = 2, 
                ClientName = "Sample Client Name",
                PartnerId = 3,
                PartnerName = "Sample Partner Name",
                OfficeId = 4, 
                OfficeName = "Sample Office Name",
                YearId = 5, 
                TaxYear = "Sample Tax Year",
                Date = "Sample Date",
                Department = "Sample Department",
                Type = "Sample Type",
                SignersCount = 6, 
                OtherEntityDetails = "Sample Other Entity Details",
                IsEsigning = true, 
                IsManualSigning = false, 
                Is7216Available = false, 
                IsNewClient = true, 
                ExpiryDate7216 = "Sample Expiry Date",
                EngagementLetterStatus = "Sample Engagement Letter Status",
                EngagementLetterPDF = "Sample PDF URL",
                IsLetterUploaded = true, 
                IsLetterStatusUpdated = false, 
                Is7216StatusUpdated = false, 
                Is7216ExpiryStatusUpdated = true, 
                Bot_Status = "Sample Bot Status",
                BotExtracted7216SignCount = 7, 
                PrimarySignerFirstName = "Sample First Name",
                PrimarySignerLastName = "Sample Last Name",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

    }
}
