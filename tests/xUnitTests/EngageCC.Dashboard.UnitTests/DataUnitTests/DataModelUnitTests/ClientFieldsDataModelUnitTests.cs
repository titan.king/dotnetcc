﻿using Dashboard.Data.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.DataUnitTests.DataModelUnitTests
{
    public class ClientFieldsDataModelUnitTests
    {
        [Fact]
        public void ClientFields_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new ClientFields
            {
                ClientFirstName = "Sample First Name",
                Spouse = "Sample Spouse Name",
                ClientLastName = "Sample Last Name",
                Signing = "Sample Signing Name",
                Officer = "Sample Officer Name",
                Lastname_Spouse = "Sample Last Name Spouse",
                TrustEsatate_Name = "Sample Trust Estate Name",
                TrusteeFiduciary_Name = "Sample Trustee Fiduciary Name",
                TrusteeFiduciary_Last_Name = "Sample Trustee Fiduciary Last Name",
                TrusteeFiduciary_Title = "Sample Trustee Fiduciary Title",
                Year = "2023", 
                TrustName = "Sample Trust Name",
                Trust = "Sample Trust",
                HusbandFullName = "Sample Husband's Full Name",
                WifeFullName = "Sample Wife's Full Name",
                EstateName = "Sample Estate Name",
                Trust_Name = "Sample Trust Name",
                Street_Address = "Sample Street Address",
                City_Address = "Sample City Address",
                City = "Sample City",
                State = "Sample State",
                Business_Name = "Sample Business Name",
                ClientId = 1, 
                NameofClients = "Sample Name of Clients",
                AddressofClients = "Sample Address of Clients",
                NameofClient = "Sample Name of Client",
                ClientName = "Sample Client Name",
                Address1 = "Sample Address Line 1",
                Address2 = "Sample Address Line 2",
                OfficeLocation = 2, 
                CreatedOn = DateTime.Now,
                ModifiedOn = null, 
                IsDeleted = false, 
                DeletedOn = null, 
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
