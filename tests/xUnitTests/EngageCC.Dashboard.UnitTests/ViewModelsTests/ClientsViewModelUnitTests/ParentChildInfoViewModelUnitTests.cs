﻿using Dashboard.Models.Clients;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ClientsViewModelUnitTests
{
    public class ParentChildInfoViewModelUnitTests
    {
        [Fact]
        public void ParentChildInfoViewModel_ValidProperties_ValidatesSuccessfully()
        {
            // Arrange
            var model = new ParentChildInfoViewModel
            {
                ParentChildInfoId = 1,
                ParentClientId = 2,
                Child1 = "Child 1",
                Child2 = "Child 2",
                Child3 = "Child 3",
                Child4 = "Child 4",
                Child5 = "Child 5",
                Child6 = "Child 6",
                Child7 = "Child 7",
                Child8 = "Child 8",
                Child9 = "Child 9",
                Child10 = "Child 10",
                IsDeleted = false,
                ChildConcatenated = "demo",
                ModifiedOn = DateTime.Now,
                CreatedOn = DateTime.Now,
                DeletedOn = DateTime.Now,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
