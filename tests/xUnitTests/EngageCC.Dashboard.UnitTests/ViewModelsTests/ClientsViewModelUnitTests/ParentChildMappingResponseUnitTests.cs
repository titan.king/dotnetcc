﻿using Dashboard.Models.Clients;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ClientsViewModelUnitTests
{
    public class ParentChildMappingResponseUnitTests
    {
        [Fact]
        public void ParentChildMappingResponse_ValidChildClientName_ValidatesSuccessfully()
        {
            // Arrange
            var model = new ParentChildMappingResponse
            {
                ChildClientName = "Child Name",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
