﻿using Dashboard.Models.Clients;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ClientsViewModelUnitTests
{
    public class ParentChildObjectUnitTests
    {
        [Fact]
        public void ParentChildObject_ValidProperties_ValidatesSuccessfully()
        {
            // Arrange
            var model = new ParentChildObject
            {
                parent_client_id = "123",
                child1 = "Child 1",
                child2 = "Child 2",
                child3 = "Child 3",
                child4 = "Child 4",
                child5 = "Child 5",
                child6 = "Child 6",
                child7 = "Child 7",
                child8 = "Child 8",
                child9 = "Child 9",
                child10 = "Child 10",

            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
