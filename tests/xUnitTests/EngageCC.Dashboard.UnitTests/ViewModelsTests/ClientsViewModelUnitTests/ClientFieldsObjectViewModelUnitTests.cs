﻿using Dashboard.Models.Clients;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ClientsViewModelUnitTests
{
    public class ClientFieldsObjectViewModelUnitTests
    {
        [Fact]
        public void ClientFieldsObject_ValidatesSuccessfully()
        {
            // Arrange
            var model = new ClientFieldsObject
            {
                ClientFirstName = "John",
                Spouse = "Jane",
                ClientLastName = "Doe",
                Signing = "John's Signature",
                Officer = "Officer Name",
                Lastname_Spouse = "Spouse's Last Name",
                TrustEsatate_Name = "Estate Name",
                TrusteeFiduciary_Name = "Trustee Name",
                TrusteeFiduciary_Last_Name = "Trustee Last Name",
                TrusteeFiduciary_Title = "Trustee Title",
                Year = 2023,
                TrustName = "Trust Name",
                Trust = "Trust Info",
                HusbandFullName = "John Doe",
                WifeFullName = "Jane Doe",
                EstateName = "Estate Name",
                Trust_Name = "Trust Name",
                Street_Address = "123 Main St",
                City_Address = "City Address",
                City = "City",
                State = "State",
                Business_Name = "Business Name",
                ClientId = "12345",
                NameofClients = "Client Names",
                AddressofClients = "Client Addresses",
                NameofClient = "Client Name",
                ClientName = "Client Name",
                Address1 = "Address Line 1",
                Address2 = "Address Line 2",
                OfficeLocation = "Office Location",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
