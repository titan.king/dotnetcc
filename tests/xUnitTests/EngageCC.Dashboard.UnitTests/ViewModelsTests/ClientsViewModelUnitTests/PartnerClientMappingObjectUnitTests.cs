﻿using Dashboard.Models.Clients;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ClientsViewModelUnitTests
{
    public class PartnerClientMappingObjectUnitTests
    {
        [Fact]
        public void PartnerClientMappingObject_ValidProperties_ValidatesSuccessfully()
        {
            // Arrange
            var model = new PartnerClientMappingObject
            {
                PartnerID = "123",
                PartnerName = "Partner Name",
                ParentClientID = "456",
                ParentClientName = "Parent Name",
                ChildClient = "Child Client",
                AddressDetail = "123 Main St",
                City = "City",
                State = "State",
                ZipCode = "12345",
                ContactName = "John Doe",
                ContactEmail = "john.doe@example.com",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
