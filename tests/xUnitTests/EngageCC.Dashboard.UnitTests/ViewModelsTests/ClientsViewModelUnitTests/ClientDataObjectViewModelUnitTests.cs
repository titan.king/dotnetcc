﻿using Dashboard.Models.Clients;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ClientsViewModelUnitTests
{
    public class ClientDataObjectViewModelUnitTests
    {
        [Fact]
        public void ClientDataObject_ValidationTest()
        {
            // Arrange
            var model = new ClientDataObject
            {
                row_num = "1",
                ClientId = "123",
                OfficeId = "456",
                PartnerId = "789",
                PartnerName = "Partner 1",
                Office = "Office A",
                OfficeAddress = "123 Main St",
                OfficeCity = "City",
                OfficeState = "State",
                PhoneNumber = "123-456-7890",
                OfficeZipCode = "12345",
                Date = "2023-10-27",
                ClientName = "Client 1",
                Address = "456 Elm St",
                City = "Another City",
                State = "Another State",
                Zip = "54321",
                SignatoryEmail = "signatory@example.com",
                SpouseEmail = "spouse@example.com",
                SignatoryFirstName = "John",
                SignatoryLastName = "Doe",
                SignatoryTitle = "CEO",
                SpouseFirstName = "Jane",
                SpouseLastName = "Doe",
                Jurisdiction = "Some Jurisdiction",
                FiscalYear = "2023",
                ELTemplate = "Template A",
                Expiration7216Form = "2023-12-31"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
