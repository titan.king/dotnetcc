﻿using Dashboard.Models.Clients;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ClientsViewModelUnitTests
{
    public class ParentChildMappingObjectUnitTest
    {
        [Fact]
        public void ParentChildMappingObject_ValidProperties_ValidatesSuccessfully()
        {
            // Arrange
            var model = new ParentChildMappingObject
            {
                ParentClientId = "123",
                ClientName = "Parent Name",
                child_client_name = "Child Name",
                Office = "Office 123",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
