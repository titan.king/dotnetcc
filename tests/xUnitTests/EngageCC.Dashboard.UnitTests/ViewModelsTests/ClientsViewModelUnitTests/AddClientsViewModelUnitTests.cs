﻿using Dashboard.Models.Clients;
using Dashboard.Models.Fields;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ClientsViewModelUnitTests
{
    public class AddClientsViewModelUnitTests
    {
        [Fact]
        public void AddClientsViewModel_Validation()
        {
            // Arrange
            var model = new AddClientsViewModel
            {
                BulkLettersId = 1,
                ClientId = 2,
                PartnerId = 3,
                ClientName = "John Doe",
                OfficeId = 4,
                Office = "OfficeName",
                SignatoryEmailId = "john@example.com",
                SignatoryFirstName = "John",
                PartnerName = "PartnerName",
                BatchId = 5,
                TemplateName = "TemplateName",
                FieldJson = "FieldJson",
                DocumentStatusName = "StatusName",
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                CreatedBy = "Creator",
                ModifiedBy = "Modifier",
                DeletedOn = DateTime.Now,
                DeletedBy = "Deleter",
                ClientSignatureCount = 6,
                IsNewClient = true,
                SpouseFirstName = "SpouseFirstName",
                SpouseEmailId = "spouse@example.com",
                IsEsigning = true,
                TaxYear = "2023",
                Is7216Available = true,
                IsDraft = false, 
                IsDeleted = false, 
                IsUpdated = false,
                letterFieldValues = new List<LetterFieldValuesResponse>()
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
