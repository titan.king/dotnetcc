﻿using Dashboard.Models.Clients;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ClientsViewModelUnitTests
{
    public class PartnerOfficeViewModelUnitTests
    {
        [Fact]
        public void PartnerOfficeViewModel_ValidProperties_ValidatesSuccessfully()
        {
            // Arrange
            var model = new PartnerOfficeViewModel
            {
                PartnerId = 123,
                PartnerName = "Partner Name",
                FieldId = 456,
                OfficeName = "Office Name",
                Jurisdiction = "Jurisdiction",
                OfficeAddress = "123 Main St",
                OfficeCity = "City",
                OfficeState = "State",
                OfficeZipCode = "12345",
                OfficePhoneNumber = "123-456-7890",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
