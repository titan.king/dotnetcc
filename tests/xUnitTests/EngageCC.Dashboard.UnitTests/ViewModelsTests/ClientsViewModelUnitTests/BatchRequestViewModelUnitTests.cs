﻿using Dashboard.Models.Clients;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ClientsViewModelUnitTests
{
    public class BatchRequestViewModelUnitTests
    {
        [Fact]
        public void BatchRequestViewModel_ValidatesSuccessfully()
        {
            // Arrange
            var model = new BatchRequestViewModel
            {
                BatchRequestId = 1,
                BatchId = 2,
                BatchJson = "Sample JSON",
                IsProcess = true,
                Status = "Processing",
                TemplateId = 3,
                TemplateName = "Template 1",
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                CreatedBy = "JohnDoe",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
