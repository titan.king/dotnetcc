﻿using Dashboard.Models.Clients;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ClientsViewModelUnitTests
{
    public class ClientsResponseViewModelUnitTests
    {
        [Fact]
        public void ClientsResponse_ValidatesSuccessfully()
        {
            // Arrange
            var expectedStatusCode = HttpStatusCode.OK;
            var expectedStatus = true;
            var expectedData = new Data(); 
            var expectedErrorMessage = "No errors";

            var model = new ClientsResponse
            {
                StatusCode = expectedStatusCode,
                Status = expectedStatus,
                Data = expectedData,
                ErrorMessage = expectedErrorMessage
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
