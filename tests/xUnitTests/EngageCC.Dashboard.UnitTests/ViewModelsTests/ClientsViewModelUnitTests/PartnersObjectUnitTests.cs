﻿using Dashboard.Models.Clients;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ClientsViewModelUnitTests
{
    public class PartnersObjectUnitTests
    {
        [Fact]
        public void PartnersObject_ValidProperties_ValidatesSuccessfully()
        {
            // Arrange
            var model = new PartnersObject
            {
                PartnerID = "123",
                PartnerName = "Partner Name",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
