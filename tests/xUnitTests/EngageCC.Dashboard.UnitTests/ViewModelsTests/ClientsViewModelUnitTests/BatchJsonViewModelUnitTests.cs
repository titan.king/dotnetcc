﻿using Dashboard.Models.Clients;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ClientsViewModelUnitTests
{
    public class BatchJsonViewModelUnitTests
    {
        [Fact]
        public void BatchJsonViewModel_ValidatesSuccessfully()
        {
            // Arrange
            var model = new BatchJsonViewModel
            {
                ClientId = 1,
                BulkLettersId = 2,
                BatchId = 3,
                DocumentStatusId = 4,
                EngageTypeId = 5,
                DepartmentId = 6,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

    }
}
