﻿using Dashboard.Models.Clients;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ClientsViewModelUnitTests
{
    public class PartnerRequestUnitTests
    {
        [Fact]
        public void PartnerRequest_ValidProperties_ValidatesSuccessfully()
        {
            // Arrange
            var model = new PartnerRequest
            {
                PartnersName = new List<PartnersViewModel>
            {
                new PartnersViewModel { PartnerName = "Partner 1" },
                new PartnersViewModel { PartnerName = "Partner 2" }
            },
                TemplateFieldNames = new List<TemplateFields>
            {
                new TemplateFields { FieldName = "Field 1" },
                new TemplateFields { FieldName = "Field 2" }
            },
                Admin = "Admin Name",
                TemplateName = "Template Name",
                BatchId = 123,
                DocumentStatusId = 456,
                FieldJson = "Field JSON",
                IsNewBulk = true,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
