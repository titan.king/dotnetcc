﻿using Dashboard.Models.Clients;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ClientsViewModelUnitTests
{
    public class OfficeObjectViewModelUnitTests
    {
        [Fact]
        public void OfficeObject_ValidatesSuccessfully()
        {
            // Arrange
            var model = new OfficeObject
            {
                id = "123",
                name = "John Doe",
                office = "ABC Inc.",
                address = "123 Main St",
                city = "City",
                state = "State",
                zip_code = "12345",
                state_signature_name_address = "John Doe - 123 Main St",
                state_signature_city = "City",
                state_signature_state = "State",
                state_signature_zip_code = "12345",
                phone_number = "123-456-7890",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
