﻿using Dashboard.Models.EngagementLetter;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.EngagementLetterViewModelUnitTests
{
    public class SigningPartnersViewModelUnitTests
    {
        [Fact]
        public void SigningPartnersViewModel_AllAttributes_AssignedValuesValidate()
        {
            // Arrange
            var model = new SigningPartnersViewModel
            {
                PartnerId = 1,
                PartnerName = "Valid Partner Name"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
