﻿using Dashboard.Models.EngagementLetter;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.EngagementLetterViewModelUnitTests
{
    public class EngagementLetterSearchFilterViewModelUnitTests
    {
        [Fact]
        public void EngagementLetterSearchFilterViewModel_AssignedValues_ValidationSucceeds()
        {
            // Arrange
            var model = new EngagementLetterSearchFilterViewModel
            {
                EngagementLetterIds = new List<EngagementLetterIdsViewModel>
            {
                new EngagementLetterIdsViewModel
                {
                    EngagementLetterId = 1,
                    PartnerName = "Partner 1"
                },
                new EngagementLetterIdsViewModel
                {
                    EngagementLetterId = 2,
                    PartnerName = "Partner 2"
                }
            },
                EngagementLetterNames = new List<EngagementLetterNamesViewModel>
            {
                new EngagementLetterNamesViewModel
                {
                    EngagementLetterName = "Letter 1"
                }
            },
                ClientNames = new List<ClientNamesViewModel>
            {
                new ClientNamesViewModel
                {
                    ClientId = 123,
                    ClientName = "Client A"
                }
            },
                Years = new List<TaxYearsViewModel>
            {
                new TaxYearsViewModel
                {
                    TaxYear = "2023"
                }
            },
                Departments = new List<DepartmentsViewModel>
            {
                new DepartmentsViewModel
                {
                    DepartmentId = 456,
                    Department = "Department X"
                }
            },
                Types = new List<EngageTypesViewModel>
            {
                new EngageTypesViewModel
                {
                    EngageTypeId = 789,
                    EngageTypeName = "Type Y"
                }
            },
                PartnerNames = new List<SigningPartnersViewModel>
            {
                new SigningPartnersViewModel
                {
                    PartnerId = 987,
                    PartnerName = "Partner Z"
                }
            },
                AdminNames = new List<AdminDetailsViewModel>
            {
                new AdminDetailsViewModel
                {
                    AdminId = 111,
                    AdminName = "Admin P"
                }
            },
                OfficeNames = new List<OfficeDetailsViewModel>
            {
                new OfficeDetailsViewModel
                {
                    OfficeId = 222,
                    OfficeName = "Office Q"
                }
            },
                LetterStatus = new List<LetterStatusViewModel>
            {
                new LetterStatusViewModel
                {
                    DocumentStatusId = 333,
                    Status = "Active"
                }
            },
                TemplateNames = new List<TemplateNamesViewModel>
            {
                new TemplateNamesViewModel
                {
                    TemplateId = 444,
                    TemplateName = "Template T"
                }
            }
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid); 
        }
    }
}
