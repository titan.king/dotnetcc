﻿using Dashboard.Models.EngagementLetter;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.EngagementLetterViewModelUnitTests
{
    public class ClientNamesViewModelUnitTests
    {
        [Fact]
        public void ClientName_RequiredAttribute_ValidatesSuccessfully()
        {
            // Arrange
            var model = new ClientNamesViewModel
            {
                ClientId = 1,
                ClientName = "Valid Client Name"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid); 
        }

        [Fact]
        public void ClientName_RequiredAttribute_FailsValidation()
        {
            // Arrange
            var model = new ClientNamesViewModel
            {
                ClientId = 1,
                ClientName = null
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid); 

            var clientNameValidationResult = validationResults.SingleOrDefault(result => result.MemberNames.Contains("ClientName"));
            Assert.NotNull(clientNameValidationResult); 
            Assert.Equal("Admin Name is required", clientNameValidationResult.ErrorMessage); 
        }
    }
}
