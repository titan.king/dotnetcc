﻿using Dashboard.Models.EngagementLetter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.EngagementLetterViewModelUnitTests
{
    public class DepartmentsViewModelUnitTests
    {
        [Fact]
        public void DepartmentsViewModel_InitializedWithValues_ValuesAreSet()
        {
            // Arrange
            var departmentId = 123;
            var departmentName = "Sample Department";

            // Act
            var model = new DepartmentsViewModel
            {
                DepartmentId = departmentId,
                Department = departmentName,
            };

            // Assert
            Assert.Equal(departmentId, model.DepartmentId); 
            Assert.Equal(departmentName, model.Department); 
        }
    }
}
