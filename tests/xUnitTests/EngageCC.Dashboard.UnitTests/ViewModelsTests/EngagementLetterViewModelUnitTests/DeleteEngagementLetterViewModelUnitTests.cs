﻿using Dashboard.Models.EngagementLetter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.EngagementLetterViewModelUnitTests
{
    public class DeleteEngagementLetterViewModelUnitTests
    {
   

        [Fact]
        public void DeleteEngagementLetterViewModel_InitializedWithValues_ValuesAreSet()
        {
            // Arrange
            var engagementLetterId = 123;
            var deletedBy = "John Doe";
            var isDeleted = true;
            var deletedOn = DateTime.Now;

            // Act
            var model = new DeleteEngagementLetterViewModel
            {
                EngagementLetterId = engagementLetterId,
                DeletedBy = deletedBy,
                IsDeleted = isDeleted,
                DeletedOn = deletedOn,
            };

            // Assert
            Assert.Equal(engagementLetterId, model.EngagementLetterId); 
            Assert.Equal(deletedBy, model.DeletedBy); 
            Assert.Equal(isDeleted, model.IsDeleted);
            Assert.Equal(deletedOn, model.DeletedOn); 
        }
    }
}
