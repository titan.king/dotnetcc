﻿using Dashboard.Models.EngagementLetter;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.EngagementLetterViewModelUnitTests
{
    public class GetEngagementLetterListViewModelUnitTests
    {
        [Fact]
        public void GetEngagementLetterListViewModel_AllAttributes_AssignedValuesValidate()
        {
            // Arrange
            var model = new GetEngagementLetterListViewModel
            {
                EngagementLetterId = 1,
                BatchId = 123,
                EngagementLetterName = "Letter 1",
                OfficeId = 456,
                OfficeName = "Office A",
                YearId = 2023,
                TaxYear = "2023",
                TemplateId = 789,
                TemplateName = "Template 123",
                TemplateVersion = 2,
                ClientId = 456,
                ClientName = "Client X",
                PartnerId = 789,
                PartnerName = "Partner Y",
                AdminId = 101,
                AdminName = "Admin Z",
                DocumentStatusId = 3,
                DocumentDescription = "Description",
                DepartmentId = 12,
                DepartmentName = "HR",
                EngageTypeId = 5,
                EngageTypeName = "Type A",
                CreatedOn = DateTime.Now,
                CreatedBy = "John Doe",
                IsDeleted = false,
                DeletedOn = null,
                DeletedBy = null,
                ModifiedBy = null,
                IsProcess = true,
                IsEnqueue = false,
                Is7216Available = true,
                ClientSignatureCount = 2,
                IsNewClient = true,
                SpouseFirstName = "Spouse Name",
                SpouseEmailId = "spouse@example.com",
                SignatoryEmailId = "signatory@example.com",
                SignatoryFirstName = "Signatory Name",
                IsEsigning = true,
                OtherEntityDetails = "Entity Details",
                PrimarySignerLastName = "Signer Last Name"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
