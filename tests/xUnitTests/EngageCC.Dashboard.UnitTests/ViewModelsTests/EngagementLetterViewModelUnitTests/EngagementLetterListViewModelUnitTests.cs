﻿using Dashboard.Models.EngagementLetter;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.EngagementLetterViewModelUnitTests
{
    public class EngagementLetterListViewModelUnitTests
    {

        [Fact]
        public void EngagementLetterListViewModel_WithAssignedValues_ValidatesSuccessfully()
        {
            // Arrange
            var model = new EngagementLetterListViewModel
            {
                EngagementLetterId = 1,
                BatchId = 2,
                EngagementLetterName = "Sample Engagement Letter",
                ClientName = "Client ABC",
                ClientId = 123,
                TaxYear = 2023,
                DepartmentId = 456,
                EngageTypeId = 789,
                DepartmentName = "HR",
                EngageGroupName = "Group A",
                EngageTypeName = "Type X",
                EngageBasisName = "Basis 1",
                OfficeName = "Office 123",
                TemplateName = "Template XYZ",
                PartnerName = "John Doe",
                DocumentStatusId = 4,
                DocumentDescription = "Description of the document",
                CreatedBy = "Jane Smith",
                AdminName = "Admin Z",
                LastModifieds = DateTime.Now,
                LastModified = "Last modified info"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid); 
        }
    }
}
