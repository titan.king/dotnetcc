﻿using Dashboard.Models.EngagementLetter;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.EngagementLetterViewModelUnitTests
{
    public class LetterStatusViewModelUnitTests
    {
        [Fact]
        public void LetterStatusViewModel_AllAttributes_AssignedValuesValidate()
        {
            // Arrange
            var model = new LetterStatusViewModel
            {
                DocumentStatusId = 1,
                Status = "Active"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid); 
        }

        [Fact]
        public void LetterStatusViewModel_Status_RequiredAttribute_FailsValidation()
        {
            // Arrange
            var model = new LetterStatusViewModel
            {
                DocumentStatusId = 1,
                Status = null
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid); 
            var statusValidationResult = validationResults.SingleOrDefault(result => result.MemberNames.Contains("Status"));
            Assert.NotNull(statusValidationResult); 
            Assert.Equal("Status is required", statusValidationResult.ErrorMessage);
        }
    }
}
