﻿using Dashboard.Models.EngagementLetter;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.EngagementLetterViewModelUnitTests
{
    public class EngagementLetterIdsViewModelUnitTests
    {

        [Fact]
        public void PartnerName_ValidatesSuccessfully()
        {
            // Arrange
            var model = new EngagementLetterIdsViewModel
            {
                EngagementLetterId = 1,
                PartnerName = "Valid Partner Name",
                CreatedBy = "John Doe",
                Status = "Active",
                OfficeName = "Office A",
                AdminName = "Admin X",
                TemplateName = "Template 123",
                clientId = 123,
                DepartmentId = 456,
                TaxYear = "2023",
                EngageTypeId = 789
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid); 
        }

        [Fact]
        public void PartnerName_RequiredAttribute_FailsValidation()
        {
            // Arrange
            var model = new EngagementLetterIdsViewModel
            {
                EngagementLetterId = 1,
                PartnerName = null 
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid); 
            var partnerNameValidationResult = validationResults.SingleOrDefault(result => result.MemberNames.Contains("PartnerName"));
            Assert.NotNull(partnerNameValidationResult); 
            Assert.Equal("SigningPartnerName is required", partnerNameValidationResult.ErrorMessage); 
        }
    }
}
