﻿using Dashboard.Models.EngagementLetter;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.EngagementLetterViewModelUnitTests
{
    public class TemplateNamesViewModelUnitTests
    {
        [Fact]
        public void TemplateNamesViewModel_AssignedValues_ValidatesSuccessfully()
        {
            // Arrange
            var model = new TemplateNamesViewModel
            {
                TemplateId = 1,
                TemplateName = "Sample Template",
                TemplateVersion = 2
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
