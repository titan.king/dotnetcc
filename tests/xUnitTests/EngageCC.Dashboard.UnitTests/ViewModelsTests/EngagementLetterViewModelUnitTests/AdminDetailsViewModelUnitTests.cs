﻿using Dashboard.Models.EngagementLetter;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.EngagementLetterViewModelUnitTests
{
    public class AdminDetailsViewModelUnitTests
    {
        [Fact]
        public void AdminName_RequiredAttribute_Validates()
        {
            // Arrange
            var model = new AdminDetailsViewModel();

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid); 

            var adminNameValidationResult = validationResults.SingleOrDefault(result => result.MemberNames.Contains("AdminName"));
            Assert.NotNull(adminNameValidationResult);
            Assert.Equal("Admin Name is required", adminNameValidationResult.ErrorMessage);
        }

        [Fact]
        public void AdminName_Validates()
        {
            // Arrange
            var model = new AdminDetailsViewModel
            {
                AdminName = "Valid Admin Name"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid); 
        }
    }
}
