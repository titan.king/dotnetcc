﻿using Dashboard.Models.EngagementLetter;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.EngagementLetterViewModelUnitTests
{
    public class EngagementLetterNamesViewModelUnitTests
    {
        [Fact]
        public void EngagementLetterNamesViewModel_AllAttributes_AssignedValuesValidate()
        {
            // Arrange
            var model = new EngagementLetterNamesViewModel
            {
                EngagementLetterId = 1,
                EngagementLetterName = "Valid Engagement Letter",
                PartnerName = "John Doe",
                CreatedBy = "Admin X"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid); 
        }
    }
}
