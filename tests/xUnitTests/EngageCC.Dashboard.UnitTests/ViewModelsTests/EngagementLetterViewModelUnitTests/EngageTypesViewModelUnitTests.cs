﻿using Dashboard.Models.EngagementLetter;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.EngagementLetterViewModelUnitTests
{
    public class EngageTypesViewModelUnitTests
    {
        [Fact]
        public void EngageTypesViewModel_AllAttributes_AssignedValuesValidate()
        {
            // Arrange
            var model = new EngageTypesViewModel
            {
                EngageTypeId = 1,
                EngageTypeName = "Sample Engage Type"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
            Assert.Equal(1, model.EngageTypeId);
            Assert.Equal("Sample Engage Type", model.EngageTypeName);
        }
    }
}
