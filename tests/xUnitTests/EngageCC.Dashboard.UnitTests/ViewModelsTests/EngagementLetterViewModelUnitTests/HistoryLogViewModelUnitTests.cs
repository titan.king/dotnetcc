﻿using Dashboard.Models.EngagementLetter;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.EngagementLetterViewModelUnitTests
{
    public class HistoryLogViewModelUnitTests
    {
        [Fact]
        public void HistoryLogViewModel_AllAttributes_AssignedValuesValidate()
        {
            // Arrange
            var model = new HistoryLogViewModel
            {
                HistoryLogId = 1,
                EngagementLetterId = 123,
                EngagementLetterName = "Sample Engagement Letter",
                BatchId = 456,
                Status = "Approved",
                EditedBy = "John Doe",
                Version = 2,
                LastModified = DateTime.Now,
                Downloaded = DateTime.Now,
                Delegated = true,
                ClientEmailId = "client@example.com",
                ReasonforDecline = "Not ready",
                DeclineTimestamp = "1699016971",
                PDFUrl = "https://example.com/document.pdf"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
