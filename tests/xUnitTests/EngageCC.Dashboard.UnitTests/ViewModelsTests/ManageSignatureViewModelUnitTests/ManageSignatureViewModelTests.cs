﻿using Dashboard.Models.ManageSignature;
using System.ComponentModel.DataAnnotations;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ManageSignatureViewModelUnitTests
{
    public class ManageSignatureViewModelTests
    {
        [Fact]
        public void ValidManageSignatureViewModel_ValidatesSuccessfully()
        {
            // Arrange
            var model = new ManageSignatureViewModel
            {
                Id = 1,
                SigningPartner = "Valid Signing Partner",
                Status = "Valid Status",
                ChangeNotes = "Valid Change Notes",
                SignatureImage = "Valid Signature Image",
                ModifiedOn = DateTime.Now,
                IsDeleted = false,
                ImageName = "Valid Image Name",
                CreatedOn = DateTime.Now,
                CreatedBy = "Valid Created By",
                ModifiedBy = "Valid Modified By",
                TransactionStatus = "Pending",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void ManageSignatureViewModel_InvalidSigningPartner_FailsValidation()
        {
            // Arrange
            var model = new ManageSignatureViewModel
            {
                SigningPartner = string.Empty,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("SigningPartner"));
        }
    }

}
