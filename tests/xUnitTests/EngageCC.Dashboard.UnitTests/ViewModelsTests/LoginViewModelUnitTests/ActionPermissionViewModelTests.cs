﻿using Dashboard.Models.Login;
using System.ComponentModel.DataAnnotations;


namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.LoginViewModelUnitTests
{
    public class ActionPermissionViewModelTests
    {
        [Fact]
        public void ValidActionPermissionViewModel_ValidatesSuccessfully()
        {
            // Arrange
            var model = new ActionPermissionViewModel
            {
                ScreenId = 1,
                ScreenName = "Valid Screen Name",
                Permissions = "Read,Write,Delete",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void ActionPermissionViewModel_InvalidScreenName_FailsValidation()
        {
            // Arrange
            var model = new ActionPermissionViewModel
            {
                ScreenName = string.Empty,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("ScreenName"));
        }
    }
}
