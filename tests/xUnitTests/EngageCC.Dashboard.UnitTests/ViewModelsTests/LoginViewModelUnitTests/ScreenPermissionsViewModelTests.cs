﻿using Dashboard.Models.Login;
using System.ComponentModel.DataAnnotations;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.LoginViewModelUnitTests
{
    public class ScreenPermissionsViewModelTests
    {
        [Fact]
        public void ValidScreenPermissionsViewModel_ValidatesSuccessfully()
        {
            // Arrange
            var model = new ScreenPermissionsViewModel
            {
                ScreenId = 1,
                ScreenName = "ValidScreenName",
                ActionId = 1,
                ActionName = "ValidActionName",
                isHavingPermission = true
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void ScreenPermissionsViewModel_InvalidScreenName_FailsValidation()
        {
            // Arrange
            var model = new ScreenPermissionsViewModel
            {
                ScreenName = string.Empty,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("ScreenName"));
        }
    }
}
