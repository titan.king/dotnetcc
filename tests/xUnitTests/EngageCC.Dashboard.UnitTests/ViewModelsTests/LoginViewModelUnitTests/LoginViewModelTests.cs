﻿using Dashboard.Models.Login;
using System.ComponentModel.DataAnnotations;


namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.LoginViewModelUnitTests
{
    public class LoginViewModelTests
    {
        [Fact]
        public void ValidLoginViewModel_ValidatesSuccessfully()
        {
            // Arrange
            var model = new LoginViewModel
            {
                ClientId = "client123",
                GroupIds = "group1,group2",
                UserId = 1,
                ScreenNames = new List<string> { "Screen1", "Screen2" },
                Roles = new List<string> { "Role1", "Role2" }
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void LoginViewModel_InvalidClientId_FailsValidation()
        {
            // Arrange
            var model = new LoginViewModel
            {
                ClientId = string.Empty,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("ClientId"));
        }
    }
}
