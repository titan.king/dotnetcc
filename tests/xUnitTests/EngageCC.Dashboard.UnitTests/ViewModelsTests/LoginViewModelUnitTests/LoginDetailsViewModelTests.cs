﻿using Dashboard.Models.Login;
using System.ComponentModel.DataAnnotations;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.LoginViewModelUnitTests
{
    public class LoginDetailsViewModelTests
    {
        [Fact]
        public void ValidLoginDetailsViewModel_ValidatesSuccessfully()
        {
            // Arrange
            var model = new LoginDetailsViewModel
            {
                Id = 1,
                UserId = "user123",
                LoggedInTime = DateTime.Now,
                LoggedInUserName = "John Doe",
                LoggedInInstance = "Instance1",
                LoggedOff = true,
                LoggedOffTime = DateTime.Now.AddMinutes(30),
                LoggedIn = 1,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void LoginDetailsViewModel_InvalidUserId_FailsValidation()
        {
            // Arrange
            var model = new LoginDetailsViewModel
            {
                UserId = string.Empty, // Invalid
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("UserId"));
        }
    }
}
