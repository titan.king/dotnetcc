﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class AssetUnitTests
    {
        [Fact]
        public void Asset_Properties_AssignedValuesValidate()
        {
            // Arrange
            var metadata = new Metadata
            {
                type = "Sample Title",
                size = 12
            };

            var model = new Asset
            {
                metadata = metadata,
                downloadUri = "https://example.com/file.pdf",
                assetID = "12345"
            };


            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
