﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class BlobSasTokenRequestUnitTest
    {
        [Fact]
        public void BlobSasTokenRequest_AssignedValue_Validates()
        {
            // Arrange
            var model = new BlobSasTokenRequest
            {
                AccountKey = "sampleAccountKey",
                AccountName = "sampleAccountName",
                ConnectionString = "sampleConnectionString",
                ContainerName = "sampleContainerName",
                FileName = "sampleFileName",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);

        }
    }
}
