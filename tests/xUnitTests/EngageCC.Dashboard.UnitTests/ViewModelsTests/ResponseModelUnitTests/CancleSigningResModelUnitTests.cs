﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class CancleSigningResModelUnitTests
    {
        [Fact]
        public void CancleSigningResModel_AssignedValue_Validates()
        {
            // Arrange
            var model = new CancleSigningResModel
            {
                EngagementLetterId = 1,
                ModifiedBy = "User1",
                PdfUrl = "https://example.com/engagementletter.pdf"
            };


            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
