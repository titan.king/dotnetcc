﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class PagedJsResponseModelUnitTests
    {
        [Fact]
        public void PagedJsResponseModel_PropertiesAreSetCorrectly()
        {
            // Arrange
            var model = new PagedJsResponseModel
            {
                success = "true",
                Data = "SampleData",
                payload = "SamplePayload",
                StatusCode = System.Net.HttpStatusCode.OK
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
