﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class ConnectedTemplatesResModelUnitTests
    {
        [Fact]
        public void ConnectedTemplatesResModel_AssignedValue_Validates()
        {
            // Arrange
            var model = new ConnectedTemplatesResModel
            {
                TemplateId = 1,
                TemplateName = "Template1",
                TemplateDescription = "Description",
                DepartmentName = "Dept1",
                EngageTypeName = "Type1",
                StatusName = "Active",
                ChangeNotes = "Changes",
                Content = "Template Content",
                TemplateLogic = "Logic",
                CreatedBy = "User1",
                DepartmentId = 123,
                EngageTypeId = 456,
                ModifiedOn = DateTime.Now
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
