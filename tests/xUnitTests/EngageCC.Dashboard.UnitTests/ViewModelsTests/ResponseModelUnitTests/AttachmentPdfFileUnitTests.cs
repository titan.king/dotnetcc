﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class AttachmentPdfFileUnitTests
    {
        [Fact]
        public void AttachmentPdfFile_UploadAttachmentJSON_PopulatesList()
        {
            // Arrange
            var attachment1 = new AttachmentBase64String
            {
                Attachment = 1,
                Filename = "example1.pdf",
                FileId = "file123",
                Base64String = "VGhpcyBpcyBhIGJhc2U2NDEgc3RyaW5nMQ==",
                attachPdfScr = "pdf_scr_1"
            };

            var attachment2 = new AttachmentBase64String
            {
                Attachment = 2,
                Filename = "example2.pdf",
                FileId = "file456",
                Base64String = "VGhpcyBpcyBhIGJhc2U2NDIgc3RyaW5nMg==",
                attachPdfScr = "pdf_scr_2"
            };

            var model = new AttachmentPdfFile
            {
                UploadAttachmentJSON = new List<AttachmentBase64String> { attachment1, attachment2 }
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
