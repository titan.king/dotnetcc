﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class ActiveBlocksListUnitTests
    {
        [Fact]
        public void ActiveBlocksList_AllAttributes_AssignedValuesValidate()
        {
            // Arrange
            var model = new ActiveBlocksList
            {
                BlockId = 1,
                BlockName = "Sample Block",
                Description = "Block Description",
                StatusName = "Active",
                ChangeNotes = "Change Notes for Block",
                Content = "Block Content"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
