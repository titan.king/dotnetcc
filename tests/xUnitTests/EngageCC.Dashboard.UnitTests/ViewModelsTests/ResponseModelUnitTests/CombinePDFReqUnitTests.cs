﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class CombinePDFReqUnitTests
    {
        [Fact]
        public void CombinePDFReq_PropertiesAreSet()
        {
            // Arrange
            var model = new CombinePDFReq
            {
                TemplateName = "Template1",
                LetterHTMLAssetID1 = "HTMLAsset1",
                Attachment1 = "Attachment1.pdf",
                Attachment2 = "Attachment2.pdf",
                Attachment3 = "Attachment3.pdf",
                Attachment4 = "Attachment4.pdf",
                Attachment5 = "Attachment5.pdf",
                LetterId = 123
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
