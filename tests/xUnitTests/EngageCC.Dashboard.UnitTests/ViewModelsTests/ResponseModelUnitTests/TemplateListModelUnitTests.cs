﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class TemplateListModelUnitTests
    {
        [Fact]
        public void TemplateListModel_AssignedValue_Validates()
        {
            // Arrange
            var model = new TemplateListModel
            {
                TemplateId = 1,
                TemplateName = "Test Template",
                TemplateDescription = "Description",
                DepartmentId = 2,
                DepartmentName = "Sample Department",
                EngageTypeId = 3,
                EngageTypeName = "Engage Type",
                StatusId = 4,
                StatusName = "Active",
                ChangeNotes = "Change notes",
                IsDeleted = false,
                CreatedOn = new DateTime(2023, 10, 27),
                ModifiedOn = null,
                CreatedBy = "Creator",
                ModifiedBy = "Modifier",
                DeletedBy = "Deleter",
                TemplateVersionId = 5,
                TemplateLogic = "Template Logic",
                VersionNumber = 1,
                MasterTemplateId = 6,
                AttachmentCount = 2,
                AttachmentJson = "Attachments JSON",
                Is7216Available = false,
                ClientSignatureCount = 3,
                DeleteId = new int[] { 7, 8, 9 },
                TemplateHtml = "HTML Content",
                AttachmentURL = "Attachment URL"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
