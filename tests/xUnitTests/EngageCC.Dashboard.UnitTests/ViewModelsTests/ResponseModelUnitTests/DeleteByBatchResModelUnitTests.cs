﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class DeleteByBatchResModelUnitTests
    {
        [Fact]
        public void DeleteByBatchResModel_AssignedValue_Validates()
        {
            // Arrange
            var model = new DeleteByBatchResModel
            {
                BatchId = 456,
                DeletedBy = "User2",
                IsDeleted = true,
                DeletedOn = DateTime.Now
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
