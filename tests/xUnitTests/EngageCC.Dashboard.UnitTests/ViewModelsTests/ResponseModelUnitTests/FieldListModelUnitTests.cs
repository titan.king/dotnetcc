﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class FieldListModelUnitTests
    {
        [Fact]
        public void FieldListModel_AssignedValue_Validates()
        {
            // Arrange
            var model = new FieldListModel
            {
                FieldId = 1,
                FieldName = "Field Name",
                DisplayName = "Display Name",
                FieldDataTypeId = 2,
                FieldDataType = "Data Type",
                HintText = "Hint Text",
                ChangeNotes = "Change Notes",
                IsDeleted = false,
                StatusId = 3,
                StatusName = "Status",
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                TransactionStatus = "Transaction Status",
                ConnectedBlocks = "Connected Blocks",
                DirectInput = true,
                MDDLookup = true,
                ColumnId = 4,
                MDDColumnNames = "MDD Column Names",
                MDDColumnData = new object(),
                ModifiedBy = "Modified By",
                CreatedBy = "Created By",
                InputType = "Input Type"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
