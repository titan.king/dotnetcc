﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class FieldsStatusResModelUnitTests
    {

        [Fact]
        public void FieldsStatusResModel_AssignedValue_Validates()
        {
            // Arrange
            var fieldsStatus = new List<FieldsStatus>
        {
            new FieldsStatus { StatusId = 1, StatusName = "Active" },
            new FieldsStatus { StatusId = 2, StatusName = "Inactive" }
        };

            var fieldsDataType = new List<FieldsDataType>
        {
            new FieldsDataType { FieldDataTypeId = 1, FieldType = "Text" },
            new FieldsDataType { FieldDataTypeId = 2, FieldType = "Number" }
        };

            var model = new FieldsStatusResModel
            {
                FieldsStatus = fieldsStatus,
                FieldsDataType = fieldsDataType
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
