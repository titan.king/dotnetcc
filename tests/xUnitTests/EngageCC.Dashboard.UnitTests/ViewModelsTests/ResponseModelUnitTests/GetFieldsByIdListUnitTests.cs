﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class GetFieldsByIdListUnitTests
    {
        [Fact]
        public void GetFieldsByIdList_AssignedValue_Validates()
        {
            // Arrange
            var fieldsListById = new List<FieldListModel>
        {
            new FieldListModel { FieldId = 1, FieldName = "Field1" },
            new FieldListModel { FieldId = 2, FieldName = "Field2" }
        };

            var fieldsConnectedBlocks = new List<BlocksList>
        {
            new BlocksList { BlockId = 1, BlockName = "BlockA" },
            new BlocksList { BlockId = 2, BlockName = "BlockB" }
        };

            var model = new GetFieldsByIdList
            {
                FieldsListById = fieldsListById,
                FieldsConnectedBlocks = fieldsConnectedBlocks
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
