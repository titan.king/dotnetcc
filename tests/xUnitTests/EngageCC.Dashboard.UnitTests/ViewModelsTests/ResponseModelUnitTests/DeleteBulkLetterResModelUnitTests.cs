﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class DeleteBulkLetterResModelUnitTests
    {
        [Fact]
        public void DeleteBulkLetterResModel_AssignedValue_Validates()
        {
            // Arrange
            var model = new DeleteBulkLetterResModel
            {
                BulkLetterId = 123,
                DeletedBy = "User1",
                IsDeleted = true,
                DeletedOn = DateTime.Now
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
