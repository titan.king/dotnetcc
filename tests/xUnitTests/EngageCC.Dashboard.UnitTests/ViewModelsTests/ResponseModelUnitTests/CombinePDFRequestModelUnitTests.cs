﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class CombinePDFRequestModelUnitTests
    {
        [Fact]
        public void CombinePDFRequestModel_AssignedValue_Validates()
        {
            // Arrange
            var model = new CombinePDFRequestModel
            {
                pdfURLfromHTML = "https://example.com/pdf1"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
