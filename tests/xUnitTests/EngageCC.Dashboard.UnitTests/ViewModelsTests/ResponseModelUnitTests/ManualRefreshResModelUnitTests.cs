﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class ManualRefreshResModelUnitTests
    {
        [Fact]
        public void ManualRefreshResModel_AssignedValue_Validates()
        {
            // Arrange
            var model = new ManualRefreshResModel
            {
                EngagementLetterId = 123,
                IsRefreshButtonClicked = "Yes",
                PdfUrl = "https://example.com/pdf",
                ModifiedBy = "John Doe"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
