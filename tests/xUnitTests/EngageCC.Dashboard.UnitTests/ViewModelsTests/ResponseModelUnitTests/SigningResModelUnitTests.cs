﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class SigningResModelUnitTests
    {
        [Fact]
        public void SigningResModel_AssignedValue_Validates()
        {
            // Arrange
            var model = new SigningResModel
            {
                EngagementLetterId = 1,
                SigningPartnerName = "John Doe",
                SigningPartnerSignatureImage = "signature.png",
                DelegatedName = "Delegated User",
                SPEmail = "john@example.com",
                DelegatedEmail = "delegated@example.com",
                ContactPerson1Name = "Contact Person 1",
                ContactPerson1Email = "contact1@example.com",
                ContactPerson2Name = "Contact Person 2",
                ContactPerson2Email = "contact2@example.com",
                Title = "Title",
                Message = "This is a test message",
                FileId = "file123",
                DocumentId = "doc456",
                Status = "Pending",
                IsDeleted = false,
                ManualSigning = true ,
                CreatedBy = "Creator",
                ModifiedBy = "Modifier",
                DeletedBy = "Deleter",
                CreatedOn = DateTime.UtcNow,
                ModifiedOn = DateTime.UtcNow,
                DeletedOn = DateTime.UtcNow,
                document_hash = "hash123",
                EngagementLetterName = "Engagement Letter",
                AdobeSignLink = "adobesignlink",
                AdobeIntegrationKey = "integrationkey123",
                AdobeDeclineLink = "adobedeclinelink",
                AgreementStatus = "Agreed",
                SigningRejectState = "Rejected",
                StatusCode = HttpStatusCode.OK,
                ErrorMessage = "No errors"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
