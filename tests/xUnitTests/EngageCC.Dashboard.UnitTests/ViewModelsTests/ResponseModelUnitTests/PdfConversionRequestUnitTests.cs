﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class PdfConversionRequestUnitTests
    {

        [Fact]
        public void PdfConversionRequest_AssignedValue_Validates()
        {
            // Arrange
            var pdfConversion = new PdfConversion
            {
                inputUrl = "https://example.com/document.pdf",
                json = "{ \"property\": \"value\" }",
                includeHeaderFooter = true,
                pageLayout = new PageLayout { pageWidth = "800", pageHeight = "600" }
            };

            var model = new PdfConversionRequest
            {
                EngagementLetterId = 123,
                pdfConversion = pdfConversion
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);

        }
    }
}
