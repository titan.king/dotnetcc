﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class BlockStatusListResModelUnitTests
    {
        [Fact]
        public void BlockStatusListResModel_WithBlockStatusList_AssignedValuesValidate()
        {
            // Arrange
            var blockStatusList = new List<BlockStatus>
        {
            new BlockStatus { StatusId = 1, StatusName = "Active" },
            new BlockStatus { StatusId = 2, StatusName = "Inactive" }
        };

            var model = new BlockStatusListResModel
            {
                BlockStatus = blockStatusList
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
