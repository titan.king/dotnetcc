﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class CombinedPDFResponseModelUnitTests
    {
        [Fact]
        public void CombinedPDFResponseModel_AssignedValue_Validates()
        {
            // Arrange
            var asset = new Assetes { assetID = "1" };

            var model = new CombinedPDFResponseModel
            {
                status = "Success",
                asset = asset
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
