﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class EngagementLetterSearchListUnitTests
    {
        [Fact]
        public void EngagementLetterSearchList_AssignedValue_Validates()
        {
            // Arrange
            var model = new EngagementLetterSearchList
            {
                EngagementLetterId = 123,
                BatchId = 456,
                EngagementLetterName = "Letter Name",
                ClientName = "Client Name",
                ClientId = 789,
                TaxYear = "2023",
                DepartmentId = 101,
                EngageTypeId = 202,
                DepartmentName = "Department Name",
                EngageGroupName = "Group Name",
                EngageTypeName = "Type Name",
                EngageBasisName = "Basis Name",
                OfficeName = "Office Name",
                TemplateName = "Template Name",
                PartnerName = "Partner Name",
                DocumentStatusId = 303,
                DocumentDescription = "Document Description",
                CreatedBy = "Creator",
                AdminName = "Admin Name",
                LastModifieds = DateTime.Now,
                LastModified = "Last Modified",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
