﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class BlocksResponseModelUnitTests
    {
        [Fact]
        public void BlocksResponseModel_AssignedValue_Validates()
        {
            // Arrange
            var model = new BlocksResponseModel
            {
                BlockId = 1,
                BlockName = "Block Name",
                Description = "Block Description",
                Content = "Block Content",
                ChangeNotes = "Change Notes",
                IsDeleted = false,
                StatusId = 1,
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now.AddHours(1),
                CreatedBy = "User1",
                DeletedBy = "User2",
                DeletedOn = DateTime.Now.AddHours(2),
                ModifiedBy = "User3",
                ConnectedTemplates = "Template1, Template2",
                TransactionStatus = "Pending"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
