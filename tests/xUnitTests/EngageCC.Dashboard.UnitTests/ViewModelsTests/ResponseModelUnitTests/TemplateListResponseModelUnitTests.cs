﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class TemplateListResponseModelUnitTests
    {
        [Fact]
        public void TemplateListResponseModel_AssignedValue_Validates()
        {
            // Arrange
            var model = new TemplateListResponseModel
            {
                TemplateId = 1,
                TemplateName = "Test Template",
                TemplateDescription = "Description",
                DepartmentId = 2,
                DepartmentName = "Sample Department",
                EngageTypeId = 3,
                EngageTypeName = "Engage Type",
                StatusId = 4,
                StatusName = "Active",
                ChangeNotes = "Change notes",
                IsDeleted = false,
                CreatedOn = new DateTime(2023, 10, 27),
                ModifiedOn = null,
                CreatedBy = "Creator",
                TemplateLogic = "Template Logic",
                ConnectedTemplates = "Connected Templates"
            };


            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
