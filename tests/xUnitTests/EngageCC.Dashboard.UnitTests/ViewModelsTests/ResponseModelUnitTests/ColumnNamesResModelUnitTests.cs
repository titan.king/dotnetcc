﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class ColumnNamesResModelUnitTests
    {

        [Fact]
        public void ColumnNamesResModel_AssignedValue_Validates()
        {
            // Arrange
            var model = new ColumnNamesResModel
            {
                ColumnId = 1,
                ColumnNames = "ColumnName1"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
