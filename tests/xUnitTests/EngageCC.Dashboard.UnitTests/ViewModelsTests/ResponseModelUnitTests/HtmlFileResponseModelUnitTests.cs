﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class HtmlFileResponseModelUnitTests
    {
        [Fact]
        public void HtmlFileResponseModel_AssignedValue_Validates()
        {
            // Arrange
            var model = new HtmlFileResponseModel
            {
                HtmlUrl = "https://example.com/document.html",
                StatusCode = HttpStatusCode.OK,
                Message = "File generated successfully"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
