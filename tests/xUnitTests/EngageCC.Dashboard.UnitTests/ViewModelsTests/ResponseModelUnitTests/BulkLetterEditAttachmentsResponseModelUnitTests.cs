﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class BulkLetterEditAttachmentsResponseModelUnitTests
    {
        [Fact]
        public void BulkLetterEditAttachmentsResponseModel_PropertiesAreSet()
        {
            // Arrange
            var model = new BulkLetterEditAttachmentsResponseModel
            {
                BulkLetterAttachmentId = 1,
                BatchId = 101,
                TemplateId = 201,
                AttachmentsJSON = "Attachment Data",
                AttachmentsURL = "https://example.com/attachments",
                VersionNumber = 2
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
            Assert.Equal(1, model.BulkLetterAttachmentId); 
            Assert.Equal(101, model.BatchId); 
            Assert.Equal(201, model.TemplateId);
            Assert.Equal("Attachment Data", model.AttachmentsJSON); 
            Assert.Equal("https://example.com/attachments", model.AttachmentsURL); 
            Assert.Equal(2, model.VersionNumber); 
        }
    }
}
