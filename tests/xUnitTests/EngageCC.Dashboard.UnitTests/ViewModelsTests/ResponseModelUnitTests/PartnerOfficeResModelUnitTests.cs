﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class PartnerOfficeResModelUnitTests
    {
        [Fact]
        public void PartnerOfficeResModel_SetProperties()
        {
            // Arrange
            var model = new PartnerOfficeResModel
            {
                PartnerId = 1,
                PartnerName = "John Doe",
                FieldId = 2,
                OfficeName = "Office 123",
                OfficeAddress = "123 Main St",
                OfficeCity = "City",
                OfficeState = "State",
                OfficeZipCode = "12345",
                OfficePhoneNumber = "555-123-4567",
                Jurisdiction = "Local Jurisdiction"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

    }
}
