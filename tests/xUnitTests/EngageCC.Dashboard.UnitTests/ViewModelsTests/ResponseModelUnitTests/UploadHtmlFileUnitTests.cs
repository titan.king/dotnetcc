﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class UploadHtmlFileUnitTests
    {
        [Fact]
        public void UploadHtmlFile_AssignedValue_Validates()
        {
            // Arrange
            var model = new UploadHtmlFile
            {
                Base64Html = "SGVsbG8gV29ybGQh", 
                FileName = "test.html"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
