﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class LoginListModelUnitTests
    {

        [Fact]
        public void LoginListModel_AssignedValue_Validates()
        {
            // Arrange
            var model = new LoginListModel
            {
                ClientId = "12345",
                GroupIds = "1,2,3",
                UserId = 789,
                ScreenNames = new List<string> { "Screen1", "Screen2" },
                Roles = new List<string> { "Role1", "Role2" }
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
