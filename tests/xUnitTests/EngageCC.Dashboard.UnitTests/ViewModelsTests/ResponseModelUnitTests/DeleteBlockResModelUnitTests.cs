﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class DeleteBlockResModelUnitTests
    {
        [Fact]
        public void DeleteBlockResModel_AssignedValue_Validates()
        {
            // Arrange
            var model = new DeleteBlockResModel
            {
                BlockId = new int[] { 1, 2, 3 },
                DeletedBy = "User1",
                IsDeleted = true,
                ModifiedOn = DateTime.Now
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
