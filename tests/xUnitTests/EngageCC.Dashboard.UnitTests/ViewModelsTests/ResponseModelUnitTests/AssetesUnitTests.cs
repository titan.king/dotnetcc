﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class AssetesUnitTests
    {
        [Fact]
        public void Assetes_PropertiesWithAssignedValues_AreValidated()
        {
            // Arrange
            var metadata = new Metadatas
            {
                type = "Image",
                size = 1024
            };

            var model = new Assetes
            {
                metadata = metadata,
                downloadUri = "https://example.com/asset.png",
                assetID = "12345",
            };


            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
