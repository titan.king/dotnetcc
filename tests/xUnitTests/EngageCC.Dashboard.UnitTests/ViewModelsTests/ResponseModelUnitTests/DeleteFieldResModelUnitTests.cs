﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class DeleteFieldResModelUnitTests
    {
        [Fact]
        public void DeleteFieldResModel_AssignedValue_Validates()
        {
            // Arrange
            var model = new DeleteFieldResModel
            {
                FieldId = new[] { 1, 2, 3 },
                DeletedBy = "User4",
                IsDeleted = true,
                ModifiedOn = DateTime.Now
            };


            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
