﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class EngagementLetterListUnitTests
    {
        [Fact]
        public void EngagementLetterList_AssignedValue_Validates()
        {
            // Arrange
            var model = new EngagementLetterList
            {
                EngagementLetterId = 123,
                BatchId = 456,
                BulkLettersId = 789,
                EngagementLetterName = "Letter Name",
                OfficeId = 101,
                OfficeName = "Office Name",
                YearId = 202,
                TaxYear = "2023",
                TemplateId = 303,
                TemplateName = "Template Name",
                TemplateVersion = 2,
                ClientId = 404,
                ClientName = "Client Name",
                PartnerId = 505,
                PartnerName = "Partner Name",
                AdminId = 606,
                AdminName = "Admin Name",
                DocumentStatusId = 707,
                DocumentDescription = "Document Description",
                DepartmentId = 808,
                DepartmentName = "Department Name",
                EngageTypeId = 909,
                EngageTypeName = "Engage Type",
                CreatedOn = DateTime.Now,
                CreatedBy = "Creator",
                ModifiedOn = null,
                IsDeleted = false,
                DeletedOn = null,
                DeletedBy = "Deleter",
                ModifiedBy = null,
                IsProcess = false,
                IsEnqueue = false,
                Is7216Available = false,
                ClientSignatureCount = 3,
                IsNewClient = false,
                SpouseFirstName = "Spouse First Name",
                SpouseEmailId = "spouse@example.com",
                SignatoryEmailId = "signatory@example.com",
                SignatoryFirstName = "Signatory First Name",
                IsEsigning = true,
                OtherEntityDetails = "Other Entity Details",
                PrimarySignerLastName = "Primary Signer Last Name",
                AdminEmail = "admin@example.com",
                PartnerEmail = "partner@example.com",
                SpouseLastName = "Spouse Last Name",
                SignatoryLastName = "Signatory Last Name",
                SignatoryTitle = "Signatory Title",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
