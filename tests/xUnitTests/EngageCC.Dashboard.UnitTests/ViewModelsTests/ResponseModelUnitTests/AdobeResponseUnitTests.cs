﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class AdobeResponseUnitTests
    {
        [Fact]
        public void AdobeResponse_Properties_ValuesAreAssignedAndValidated()
        {
            // Arrange
            var model = new AdobeResponse
            {
                uploadUri = "https://example.com/upload",
                assetID = "12345",
                AttachemntPDFcontent = "PDF Content"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
