﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class CombinedPDFRequestModelUnitTests
    {
        [Fact]
        public void CombinedPDFRequestModel_AssignedValue_Validates()
        {
            // Arrange
            var assets = new List<Assete>
        {
            new Assete { assetID = "1" },
            new Assete { assetID = "2" }
        };

            var model = new CombinedPDFRequestModel
            {
                assets = assets
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
