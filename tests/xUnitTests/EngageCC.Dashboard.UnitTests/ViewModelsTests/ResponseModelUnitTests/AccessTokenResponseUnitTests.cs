﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class AccessTokenResponseUnitTests
    {
        [Fact]
        public void AccessTokenResponse_AllAttributes_AssignedValuesValidate()
        {
            // Arrange
            var model = new AccessTokenResponse
            {
                token_type = "Bearer",
                access_token = "sample-access-token",
                expires_in = 3600
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
