﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class BlocksCommonModelUnitTests
    {

        [Fact]
        public void BlocksCommonModel_AssignedValue_Validates()
        {
            // Arrange
            var block1 = new BlocksList
            {
                BlockId = 1,
                BlockName = "Block 1",
                Description = "Description 1",
                Content = "Block content 1",
                ChangeNotes = "Change notes 1",
                IsDeleted = false,
                StatusId = 1,
                StatusName = "Active",
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now.AddHours(1),
                CreatedBy = "User1",
                DeletedBy = "User2",
                DeletedOn = DateTime.Now.AddHours(2),
                ModifiedBy = "User3",
                ConnectedTemplates = "Template1, Template2",
                DepartmentId = 101,
                DepartmentName = "Department A",
                EngageTypeId = 201,
                EngageTypeName = "Engagement Type X"
            };

            var block2 = new BlocksList
            {
                BlockId = 2,
                BlockName = "Block 2",
                Description = "Description 2",
                Content = "Block content 2",
                ChangeNotes = "Change notes 2",
                IsDeleted = true,
                StatusId = 2,
                StatusName = "Inactive",
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now.AddHours(1),
                CreatedBy = "User4",
                DeletedBy = "User5",
                DeletedOn = DateTime.Now.AddHours(2),
                ModifiedBy = "User6",
                ConnectedTemplates = "Template2, Template3",
                DepartmentId = 102,
                DepartmentName = "Department B",
                EngageTypeId = 202,
                EngageTypeName = "Engagement Type Y"
            };

            var template1 = new TemplateListResponseModel
            {
                TemplateId = 1,
                TemplateName = "Template 1",
                TemplateDescription = "Template Description 1",
                DepartmentId = 101,
                DepartmentName = "Department A",
                EngageTypeId = 201,
                StatusId = 1,
                StatusName = "Active",
                ChangeNotes = "Change notes for Template 1",
                IsDeleted = false,
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now.AddHours(1),
                CreatedBy = "User1",
                TemplateLogic = "Template Logic 1",
                ConnectedTemplates = "Template2, Template3"
            };

            var template2 = new TemplateListResponseModel
            {
                TemplateId = 2,
                TemplateName = "Template 2",
                TemplateDescription = "Template Description 2",
                DepartmentId = 102,
                DepartmentName = "Department B",
                EngageTypeId = 202,
                StatusId = 2,
                StatusName = "Inactive",
                ChangeNotes = "Change notes for Template 2",
                IsDeleted = true,
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now.AddHours(1),
                CreatedBy = "User4",
                TemplateLogic = "Template Logic 2",
                ConnectedTemplates = "Template1, Template3"
            };

            var model = new BlocksCommonModel
            {
                BlocksById = new List<BlocksList> { block1, block2 },
                ConnectedTemplate = new List<TemplateListResponseModel> { template1, template2 }
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
