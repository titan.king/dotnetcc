﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class AttachmentBase64StringUnitTest
    {
        [Fact]
        public void AttachmentBase64String_AssignedValues_Validates()
        {
            // Arrange
            var model = new AttachmentBase64String
            {
                Attachment = 1,
                Filename = "example.pdf",
                FileId = "file123",
                Base64String = "VGhpcyBpcyBhIGJhc2U2NCBzdHJpbmc=",
                attachPdfScr = "pdf_scr"
            };
            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
