﻿using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class BulkLetterFieldsResponseModelUnitTests
    {
        [Fact]
        public void BulkLetterFieldsResponseModel_PropertiesAreSet()
        {
            // Arrange
            var fieldList = new Fieldlist[]
            {
            new Fieldlist { FieldId = 1, FieldName = "Field1" },
            new Fieldlist { FieldId = 2, FieldName = "Field2" }
            };

            var model = new BulkLetterFieldsResponseModel
            {
                FieldList = fieldList,
                IsEsigning = true,
                Is7216Available = "Available",
                ClientSignatureCount = 2,
                ClientId = 101,
                PartnerId = 201,
                BulkLettersId = 301,
                LetterVersion = 1
            };


            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);

        }
    }
}
