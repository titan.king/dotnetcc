﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class HistoryLogResponseModelUnitTests
    {
        [Fact]
        public void HistoryLogResponseModel_PropertiesAreSet()
        {
            // Arrange
            var model = new HistoryLogResponseModel
            {
                HistoryLogId = 1,
                EngagementLetterId = 123,
                EngagementLetterName = "LetterName",
                BatchId = 456,
                Status = "Completed",
                EditedBy = "Editor",
                Version = 2,
                LastModified = new System.DateTime(2023, 10, 1),
                Downloaded = new System.DateTime(2023, 10, 2),
                Delegated = true,
                ClientEmailId = "client@example.com",
                ReasonforDecline = "Decline Reason",
                DeclineTimestamp = "1699016971",
                PDFUrl = "https://example.com/document.pdf"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
