﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class AdobePDFStatusResponseUnitTests
    {
        [Fact]
        public void AdobePDFStatusResponse_Properties_AssignedValuesValidate()
        {
            // Arrange
            var model = new AdobePDFStatusResponse
            {
                status = "Success",
                asset = new Asset
                {
                    assetID = "546456",
                    downloadUri = "document",
                    metadata = new Metadata { size = 1, type = "demo" },
                }
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
