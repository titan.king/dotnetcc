﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class CheckSigningStatusResModelUnitTests
    {
        [Fact]
        public void CheckSigningStatusResModel_AssignedValue_Validates()
        {
            // Arrange
            var model = new CheckSigningStatusResModel
            {
                EngagementLetterId = 1,
                EngagementLetterName = "Letter1",
                Version = 2,
                PdfURL = "https://example.com/engagementletter.pdf",
                DelegatedName = "Delegated User",
                SigningPartnerName = "Partner User",
                AgreementId = "Agreement123",
                ModifiedBy = "User1"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
