﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class EditBlocksResModelUnitTests
    {
        [Fact]
        public void EditBlocksResModel_AssignedValue_Validates()
        {
            // Arrange
            var model = new EditBlocksResModel
            {
                EngagementLetterId = 123,
                BlockText = "Block Text",
                BlockName = "Block Name",
                Content = "Block Content",
                ModifiedOn = DateTime.Now,
                EditedOnlyForThisLetter = true,
                EditedBlockID = 456,
                EditedHtmlContent = "Edited HTML Content",
                EditedColourHtmlContent = "Edited Color HTML Content",
                ModifiedColourjsonContent = "Modified Color JSON Content",
                TransactionStatus = "Success",
                DepartmentId = 789,
                EngageTypeId = 101,
                DocumentId = 202,
                TemplateJSON = "Template JSON",
                RemovedTagsOnTemplateJSON = "Removed Tags in Template JSON",
                ColorTagsRemovedforTempJSON = true,
                StatusCode = HttpStatusCode.OK,
                ErrorMessage = "No errors",
                ErrorPopupMessage = "No popup message",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
