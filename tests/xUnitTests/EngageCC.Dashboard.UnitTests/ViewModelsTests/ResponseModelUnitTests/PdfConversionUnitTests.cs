﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class PdfConversionUnitTests
    {
        [Fact]
        public void PdfConversion_PropertiesAreSetCorrectly()
        {
            // Arrange
            var model = new PdfConversion
            {
                inputUrl = "https://example.com/document.pdf",
                json = "{ \"property\": \"value\" }",
                includeHeaderFooter = true,
                pageLayout = new PageLayout { pageWidth = "800", pageHeight = "600" }
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
