﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.ResponseModelUnitTests
{
    public class CombinedPdfUrlModelUnitTests
    {
        [Fact]
        public void CombinedPdfUrlModel_AssignedValue_Validates()
        {
            // Arrange
            var attachmentList = new List<AttachmentBase64String>
        {
            new AttachmentBase64String { Attachment = 1, Filename = "File1.pdf" },
            new AttachmentBase64String { Attachment = 2, Filename = "File2.pdf" }
        };

            var model = new CombinedPdfUrlModel
            {
                LetterHtmlContent = "<html><body>Letter Content</body></html>",
                TemplateName = "Template1",
                UploadAttachmentJSON = attachmentList
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
