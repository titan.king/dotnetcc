﻿using Dashboard.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.SigningViewModelUnitTests
{
    public class SigningViewModelUnitTests
    {

        [Fact]
        public void SigningViewModel_AssignedValue_Validates()
        {
            // Arrange
            var model = new SigningViewModel
            {
                EngagementLetterId = 1,
                SigningPartnerName = "John Doe",
                SigningPartnerSignatureImage = "signature_image.png",
                DelegatedName = "Jane Smith",
                SPEmail = "john@example.com",
                DelegatedEmail = "jane@example.com",
                ContactPerson1Name = "Alice",
                ContactPerson1Email = "alice@example.com",
                ContactPerson2Name = "Bob",
                ContactPerson2Email = "bob@example.com",
                Title = "Title",
                Message = "Test message",
                FileId = "file123",
                DocumentId = "doc456",
                Status = "Pending",
                IsDeleted = false,
                ManualSigning = true,
                CreatedBy = "AdminUser",
                ModifiedBy = "John Doe",
                DeletedBy = "",
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                DeletedOn = DateTime.MinValue,
                document_hash = "hash123",
                EngagementLetterName = "Engagement Letter 1",
                AdobeSignLink = "adobe_sign_link",
                AdobeIntegrationKey = "integration_key",
                AdobeDeclineLink = "adobe_decline_link",
                AgreementStatus = "In Progress",
                SigningRejectState = "Not Rejected",
                StatusCode = HttpStatusCode.OK,
                ErrorMessage = "No errors",
                LetterHtmlContent = "<html><body>Content</body></html>",
                AttachmentFileId1 = "attachment1",
                IsForm7216 = true,
                SignerCount = 2,
                DelegatedBy = "ManagerUser"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
