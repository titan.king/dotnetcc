﻿using Dashboard.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.SigningViewModelUnitTests
{
    public class MemberinfoUnitTests
    {
        [Fact]
        public void Memberinfo_AssignedValue_Validates()
        {
            // Arrange
            var securityOption = new Securityoption
            {
                authenticationMethod = "Option A"
            };

            var model = new Memberinfo
            {
                email = "test@example.com",
                name = "John Doe",
                id = "123",
                securityOption = securityOption
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);

        }
    }
}
