﻿using Dashboard.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.SigningViewModelUnitTests
{
    public class ParticipantsetsinfoUnitTests
    {
        [Fact]
        public void Participantsetsinfo_AssignedValue_Validates()
        {
            // Arrange
            var member1 = new Memberinfo { name = "John", id = "30" };
            var member2 = new Memberinfo { name = "Alice", id = "25" };
            var model = new Participantsetsinfo
            {
                id = "1",
                memberInfos = new Memberinfo[] { member1, member2 },
                role = "Manager",
                order = 2
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);

        }
    }
}
