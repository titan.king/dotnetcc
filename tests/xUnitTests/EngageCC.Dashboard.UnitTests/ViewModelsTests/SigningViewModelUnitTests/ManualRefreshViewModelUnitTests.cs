﻿using Dashboard.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.SigningViewModelUnitTests
{
    public class ManualRefreshViewModelUnitTests
    {

        [Fact]
        public void ManualRefreshViewModel_AssignedValue_Validates()
        {
            // Arrange
            var model = new ManualRefreshViewModel
            {
                EngagementLetterId = 123,
                IsRefreshButtonClicked = "Yes"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
