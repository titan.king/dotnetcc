﻿using Dashboard.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.SigningViewModelUnitTests
{
    public class AdobeAgreementEventVMUnitTests
    {
        [Fact]
        public void AdobeAgreementEventVM_AssignedValue_Validates()
        {
            // Arrange
            var model = new AdobeAgreementEventVM
            {
                events = new List<Events>
            {
                new Events
                {
                        actingUserEmail = "actingUser@example.com",
                        actingUserIpAddress = "192.168.0.1",
                        actingUserName = "Acting User",
                        date = "2023-10-27",
                        description = "This is a sample event.",
                        device = "Sample Device",
                        deviceLocation = new DeviceLocation
                        {
                            latitude = "40.7128",
                            longitude = "-74.0060",
                        },
                        devicePhoneNumber = "123-456-7890",
                        digitalSignatureInfo = new DigitalSignatureInfo
                        {
                            certificateIssuer = "Cert123",
                            cloudProviderName = "Issuer123",
                        },
                        electronicSealInfo = new ElectronicSealInfo
                        {
                            id = "Seal456",
                            company = "Provider456",
                        },
                        initiatingUserEmail = "initiatingUser@example.com",
                        initiatingUserName = "Initiating User",
                        participantEmail = "participant@example.com",
                        participantId = "Participant123",
                        participantRole = "Reviewer",
                        synchronizationId = "Sync123",
                        type = "Signing",
                        vaultEventId = "Event456",
                        vaultProviderName = "VaultProvider",
                        versionId = "Version789",
                        comment = "This is a comment for the event.",
                        id = "Event123",
                        reminderParticipants = new List<ReminderParticipants>
                        {
                            new ReminderParticipants
                            {
                                email = "reminder1@example.com",
                                name = "Reminder 1",
                            },
                            new ReminderParticipants
                            {
                                email = "reminder2@example.com",
                                name = "Reminder 2",
                            },
                        },
                        replacedParticipantEmail = "replacedParticipant@example.com",
                        replacedParticipantName = "Replaced Participant",
                        signerNewName = "New Signer",
                        signerOldName = "Old Signer",
                        signingReason = "Authorization",
                },
            }
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
