﻿using Dashboard.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.SigningViewModelUnitTests
{
    public class EventsUnitTests
    {
        [Fact]
        public void Events_AssignedValue_Validates()
        {
            // Arrange
            var model = new Events
            {
                actingUserEmail = "actinguser@example.com",
                actingUserIpAddress = "192.168.0.1",
                actingUserName = "Acting User",
                date = "2023-10-27",
                description = "Event Description",
                device = "Mobile Device",
                deviceLocation = new DeviceLocation
                {
                    latitude = "123.456",
                    longitude = "789.012"
                },
                devicePhoneNumber = "123-456-7890",
                digitalSignatureInfo = new DigitalSignatureInfo
                {
                    email = "Digital",
                    certificateIssuer = "20"
                },
                electronicSealInfo = new ElectronicSealInfo
                {
                    company = "Electronic",
                    id = "20"
                },
                initiatingUserEmail = "initiator@example.com",
                initiatingUserName = "Initiating User",
                participantEmail = "participant@example.com",
                participantId = "Participant123",
                participantRole = "Role",
                synchronizationId = "SyncID123",
                type = "EventType",
                vaultEventId = "VaultEventID123",
                vaultProviderName = "ProviderName",
                versionId = "Version123",
                comment = "Event Comment",
                id = "EventID123",
                reminderParticipants = new List<ReminderParticipants>
            {
                new ReminderParticipants
                {
                    email = "reminder@example.com",
                    participantId = "20231029"
                }
            },
                replacedParticipantEmail = "replaced@example.com",
                replacedParticipantName = "Replaced Participant",
                signerNewName = "New Signer",
                signerOldName = "Old Signer",
                signingReason = "Reason for Signing"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
