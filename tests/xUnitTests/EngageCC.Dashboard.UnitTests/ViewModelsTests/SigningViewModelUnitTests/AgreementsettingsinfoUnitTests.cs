﻿using Dashboard.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.SigningViewModelUnitTests
{
    public class AgreementsettingsinfoUnitTests
    {
        [Fact]
        public void AgreementSettingsInfo_AssignedValue_Validates()
        {
            // Arrange
            var model = new Agreementsettingsinfo
            {
                canEditFiles = true,
                canEditElectronicSeals = false,
                canEditAgreementSettings = true,
                showAgreementReminderSentEvents = true,
                hipaaEnabled = false,
                showDocumentsViewedEvents = true,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
