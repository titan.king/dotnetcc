﻿using Dashboard.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.SigningViewModelUnitTests
{
    public class HTMLFileConversionViewModelUnitTests
    {

        [Fact]
        public void HTMLFileConversionViewModel_AssignedValue_Validates()
        {
            // Arrange
            var model = new HTMLFileConversionViewModel
            {
                Base64HtmlData = "Base64 HTML Data",
                EngagementLetterId = 123,
                EngagementLetterName = "Sample Letter",
                Host = "example.com",
                PathChange = "path/to/file"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
