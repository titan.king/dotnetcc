﻿using Dashboard.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.SigningViewModelUnitTests
{
    public class AdobeSignStatusResponseVMUnitTests
    {
        [Fact]
        public void AdobeSignStatusResponseVM_AssignedValue_Validates()
        {
            // Arrange
            var participantSetInfo = new Participantsetsinfo
            {
                id = "test",
                memberInfos = new Memberinfo[0],
                role = "sample",
                order = 1,
            };

            var externalId = new Externalid
            {
                id = "sample"
            };

            var agreementSettingsInfo = new Agreementsettingsinfo
            {
                canEditFiles = true,
                canEditElectronicSeals = true,
                canEditAgreementSettings = true,
                showAgreementReminderSentEvents = true,
                hipaaEnabled = true,
                showDocumentsViewedEvents = true
            };

            var model = new AdobeSignStatusResponseVM
            {
                id = "123",
                name = "Sample Agreement",
                groupId = "456",
                type = "Agreement",
                participantSetsInfo = new[] { participantSetInfo },
                senderEmail = "sender@example.com",
                createdDate = DateTime.Now,
                lastEventDate = DateTime.Now,
                signatureType = "Electronic",
                externalId = externalId,
                locale = "en-US",
                status = "Completed",
                documentVisibilityEnabled = true,
                hasFormFieldData = true,
                hasSignerIdentityReport = false,
                agreementSettingsInfo = agreementSettingsInfo,
                sendType = "Email",
                senderSigns = "Individual",
                documentRetentionApplied = true
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
