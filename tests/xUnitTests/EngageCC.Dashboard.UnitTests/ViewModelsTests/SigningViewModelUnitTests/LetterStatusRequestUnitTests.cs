﻿using Dashboard.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.SigningViewModelUnitTests
{
    public class LetterStatusRequestUnitTests
    {

        [Fact]
        public void LetterStatusRequest_AssignedValue_Validates()
        {
            // Arrange
            var model = new LetterStatusRequest
            {
                LettersStatusReportId = 1,
                EngagementLetterId = 123,
                EngagementLetterName = "Sample Letter",
                ClientId = 456,
                ClientName = "Client A",
                PartnerId = 789,
                PartnerName = "John Doe",
                OfficeId = 101,
                OfficeName = "Office XYZ",
                YearId = 2023,
                TaxYear = "2023",
                Date = "2023-10-31",
                Department = "Finance",
                Type = "Type A",
                SignersCount = 2,
                OtherEntityDetails = "Entity XYZ",
                IsEsigning = true,
                IsManualSigning = false,
                Is7216Available = true,
                IsNewClient = true,
                ExpiryDate7216 = "2023-12-31",
                EngagementLetterStatus = "Active",
                EngagementLetterPDF = "https://example.com/letter.pdf",
                IsLetterUploaded = true,
                IsLetterStatusUpdated = true,
                Is7216StatusUpdated = false,
                Is7216ExpiryStatusUpdated = true,
                Bot_Status = "Completed",
                BotExtracted7216SignCount = 3,
                PrimarySignerFirstName = "Alice",
                PrimarySignerLastName = "Smith"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
