﻿using Dashboard.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.SigningViewModelUnitTests
{
    public class ReminderParticipantsUnitTests
    {
        [Fact]
        public void ReminderParticipants_AssignedValue_Validates()
        {
            // Arrange
            var model = new ReminderParticipants
            {
                email = "user@example.com",
                participantId = "123",
                name = "John Doe"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
