﻿using Dashboard.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.SigningViewModelUnitTests
{
    public class ElectronicSealInfoUnitTests
    {

        [Fact]
        public void ElectronicSealInfo_AssignedValue_Validates()
        {
            // Arrange
            var model = new ElectronicSealInfo
            {
                company = "ACME Inc.",
                id = "12345",
                name = "John Doe"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
