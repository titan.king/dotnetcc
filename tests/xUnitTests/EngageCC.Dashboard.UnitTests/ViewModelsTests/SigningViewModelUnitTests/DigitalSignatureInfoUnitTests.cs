﻿using Dashboard.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.SigningViewModelUnitTests
{
    public class DigitalSignatureInfoUnitTests
    {
        [Fact]
        public void DigitalSignatureInfo_AssignedValue_Validates()
        {
            // Arrange
            var model = new DigitalSignatureInfo
            {
                company = "Test Company",
                email = "test@example.com",
                name = "John Doe",
                certificateIssuer = "Test Issuer",
                cloudProviderIp = "192.168.1.1",
                cloudProviderName = "Test Cloud Provider",
                cloudProviderUrl = "https://example.com/cloud"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
