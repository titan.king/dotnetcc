﻿using Dashboard.Models.Signing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.SigningViewModelUnitTests
{
    public class AdobeCancleSignRequestUnitTests
    {

        [Fact]
        public void AdobeCancleSignRequest_PropertiesAreSetCorrectly()
        {
            // Arrange
            var model = new AdobeCancleSignRequest
            {
                state = "canceled",
                agreementID = "12345"
            };


            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
