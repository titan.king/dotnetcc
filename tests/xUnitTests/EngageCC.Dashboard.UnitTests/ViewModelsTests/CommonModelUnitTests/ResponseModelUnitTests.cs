﻿using Dashboard.Models.CommonModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.CommonModelUnitTests
{
    public class ResponseModelUnitTests
    {

        [Fact]
        public void ResponseModel_DefaultConstructor_InitializesCorrectly()
        {
            // Arrange
            var model = new ResponseModel();

            // Act and Assert
            Assert.False(model.Status); 
            Assert.Null(model.Data); 
            Assert.Null(model.ErrorMessage); 
            Assert.Equal(string.Empty, model.PdfUrl); 
        }

        [Fact]
        public void ResponseModel_ConstructorWithStatusCode_InitializesCorrectly()
        {
            // Arrange
            var statusCode = HttpStatusCode.InternalServerError;

            // Act
            var model = new ResponseModel(statusCode);

            // Assert
            Assert.Equal(statusCode, model.StatusCode);
            Assert.False(model.Status); 
            Assert.Null(model.Data); 
            Assert.Null(model.ErrorMessage); 
            Assert.Equal(string.Empty, model.PdfUrl); 
        }

        [Fact]
        public void ResponseModel_ConstructorWithStatusCodeAndErrorMessage_InitializesCorrectly()
        {
            // Arrange
            var statusCode = HttpStatusCode.NotFound;
            var errorMessage = "Not found";

            // Act
            var model = new ResponseModel(statusCode, errorMessage);

            // Assert
            Assert.Equal(statusCode, model.StatusCode);
            Assert.False(model.Status); 
            Assert.Null(model.Data); 
            Assert.Equal(errorMessage, model.ErrorMessage);
            Assert.Equal(string.Empty, model.PdfUrl); 
        }
    }
}
