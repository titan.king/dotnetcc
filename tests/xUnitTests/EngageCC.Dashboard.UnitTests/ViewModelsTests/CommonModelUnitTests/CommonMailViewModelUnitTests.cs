﻿using Dashboard.Models.CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.CommonModelUnitTests
{
    public class CommonMailViewModelUnitTests
    {
        [Fact]
        public void CommonMailViewModel_ValidProperties_ValidatesSuccessfully()
        {
            // Arrange
            var model = new CommonMailViewModel
            {
                DocumentStatusId = 1,
                FromMail = "from@example.com",
                FromName = "Sender Name",
                Subject = "Test Subject",
                ToIds = "to@example.com",
                LetterName = "Letter Name",
                StatusContent = "Status Content",
                LinkContent = "Link Content",
                EngagementLetterId = 123,
                pdfContent = "PDF Content",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

    }
}
