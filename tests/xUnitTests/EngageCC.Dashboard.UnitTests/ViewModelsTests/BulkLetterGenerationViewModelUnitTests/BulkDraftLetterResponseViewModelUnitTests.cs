﻿using Dashboard.Models.BulkLetterGeneration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.BulkLetterGenerationViewModelUnitTests
{
    public class BulkDraftLetterResponseViewModelUnitTests
    {
        [Fact]
        public void BulkDraftLetterResponse_ValidatesSuccessfully()
        {
            // Arrange
            var draftLetter1 = new DraftLetters
            {
                BulkLettersId = 1,
                ClientId = 1001,
                ClientName = "John Doe",
                Office = "Office A",
                SignatoryEmailId = "john.doe@example.com",
                SignatoryFirstName = "John",
                PartnerName = "Partner 1",
                BatchId = 2,
                TemplateName = "Template 1",
                FieldJson = "{ \"field1\": \"value1\" }",
                DocumentStatusName = "Draft",
                CreatedOn = DateTime.Now,
                CreatedBy = "Admin",
                TemplateId = 101,
                OfficeId = 201,
                TaxYear = "2023"
            };

            var partner1 = new PartnersNames
            {
                PartnerId = "1",
                PartnerName = "Partner 1"
            };

            var model = new BulkDraftLetterResponse
            {
                DraftLetters = new List<DraftLetters> { draftLetter1 },
                PartnersNames = new List<PartnersNames> { partner1 },
                BatchId = 2,
                ClientIdErrorList = "Error List",
                GetCurrentDate = "2023-10-27"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
