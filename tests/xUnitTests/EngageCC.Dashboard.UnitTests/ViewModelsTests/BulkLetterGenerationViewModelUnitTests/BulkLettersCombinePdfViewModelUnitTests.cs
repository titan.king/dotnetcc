﻿using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.BulkLetterGenerationViewModelUnitTests
{
    public class BulkLettersCombinePdfViewModelUnitTests
    {
        [Fact]
        public void BulkLettersCombinePdfViewModel_ValidatesSuccessfully()
        {
            // Arrange
            var model = new BulkLettersCombinePdfViewModel
            {
                LetterHtmlContent = "<html><body>Sample letter content</body></html>",
                BatchId = 1,
                TemplateId = 2,
                TemplateName = "Sample Template",
                UploadAttachmentJSON = new List<AttachmentBase64String>
            {
                new AttachmentBase64String {  },
                new AttachmentBase64String {  }
            }
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
