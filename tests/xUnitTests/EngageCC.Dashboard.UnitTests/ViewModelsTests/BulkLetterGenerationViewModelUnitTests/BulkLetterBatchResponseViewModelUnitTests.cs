﻿using Dashboard.Models.BulkLetterGeneration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.BulkLetterGenerationViewModelUnitTests
{
    public class BulkLetterBatchResponseViewModelUnitTests
    {
        [Fact]
        public void BulkLetterBatchResponse_ValidatesSuccessfully()
        {
            // Arrange
            var model = new BulkLetterBatchResponse
            {
                BatchId = 1,
                Admin = "AdminName",
                PartnerName = "PartnerName",
                Office = "OfficeName",
                TemplateName = "TemplateName",
                NoOfLetters = 10,
                LastUpdatedOn = "2023-10-27",
                DocumentStatus = "Status",
                PartnerCount = 5,
                UpdatedOn = DateTime.Now
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
