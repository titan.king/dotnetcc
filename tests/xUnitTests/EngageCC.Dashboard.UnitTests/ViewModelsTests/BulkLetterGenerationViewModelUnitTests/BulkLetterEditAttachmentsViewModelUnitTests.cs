﻿using Dashboard.Models.BulkLetterGeneration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.BulkLetterGenerationViewModelUnitTests
{
    public class BulkLetterEditAttachmentsViewModelUnitTests
    {
        [Fact]
        public void BulkLetterEditAttachmentsViewModel_ValidatesSuccessfully()
        {
            // Arrange
            var model = new BulkLetterEditAttachmentsViewModel
            {
                BulkLetterAttachmentId = 1,
                BatchId = 2,
                TemplateId = 3,
                AttachmentsJSON = "JSON Data",
                AttachmentsURL = "https://example.com/attachments",
                VersionNumber = 4,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
