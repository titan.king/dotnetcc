﻿using Dashboard.Models.BulkLetterGeneration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.BulkLetterGenerationViewModelUnitTests
{
    public class BatchRequestResponseViewModelUnitTests
    {
        [Fact]
        public void BatchRequestResponse_ValidatesSuccessfully()
        {
            // Arrange
            var model = new BatchRequestResponse
            {
                BatchRequestId = 1,
                BatchId = 2,
                BatchJson = "{ \"data\": \"example data\" }",
                IsProcess = true,
                Status = "Processing",
                TemplateId = 3,
                TemplateName = "Sample Template",
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now.AddMinutes(30),
                CreatedBy = "JohnDoe",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
