﻿using Dashboard.Models.BulkLetterGeneration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.BulkLetterGenerationViewModelUnitTests
{
    public class ActiveUserRequestViewModelUnitTests
    {
        [Fact]
        public void ActiveUserRequest_ValidatesSuccessfully()
        {
            // Arrange
            var model = new ActiveUserRequest
            {
                BatchId = 1,
                UserName = "ValidUserName",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
