﻿using Dashboard.Models.BulkLetterGeneration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.BulkLetterGenerationViewModelUnitTests
{
    public class FieldlistViewModelUnitTests
    {
        [Fact]
        public void Fieldlist_ValidFields_ValidatesSuccessfully()
        {
            // Arrange
            var model = new Fieldlist
            {
                FieldName = "Field Name",
                FieldValue = "Field Value",
                FieldId = 1,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
