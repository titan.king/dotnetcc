﻿using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.Fields;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.BulkLetterGenerationViewModelUnitTests
{
    public class BulkLettersResponseViewModelUnitTests
    {
        [Fact]
        public void BulkLettersResponse_ValidatesSuccessfully()
        {
            // Arrange
            var model = new BulkLettersResponse
            {
                BulkLettersId = 1,
                ClientId = 2,
                PartnerId = 3,
                ClientName = "Client Name",
                OfficeId = 4,
                Office = "Office Name",
                SignatoryEmailId = "signatory@example.com",
                SignatoryFirstName = "Signatory First Name",
                SignatoryLastName = "Signatory Last Name",
                SignatoryTitle = "Signatory Title",
                SpouseEmail = "spouse@example.com",
                SpouseLastName = "Spouse Last Name",
                PartnerName = "Partner Name",
                BatchId = 5,
                TemplateName = "Template Name",
                FieldJson = "Field JSON",
                DocumentStatusName = "Document Status",
                CreatedOn = DateTime.Now,
                ModifiedOn = null,
                CreatedBy = "Creator",
                ModifiedBy = null,
                DeletedOn = null,
                DeletedBy = null,
                Is7216Available = true,
                ClientSignatureCount = 6,
                IsNewClient = true,
                SpouseFirstName = "Spouse First Name",
                SpouseEmailId = "spouse@example.com",
                IsEsigning = true,
                TaxYear = "2023",
                letterFieldValues = new List<LetterFieldValuesResponse>(),
                IsBatchActive = false,
                IsBatchDelete = false,
                OfficePhoneNumber = "123-456-7890",
                OfficeZipCode = "12345",
                OfficeState = "CA",
                OfficeCity = "Los Angeles",
                OfficeAddress = "123 Main St",
                OfficeName = "Office Name",
                Date = "2023-10-27",
                Address = "456 Elm St",
                City = "New York",
                State = "NY",
                Zip = "10001",
                Jurisdiction = "Jurisdiction Name"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
