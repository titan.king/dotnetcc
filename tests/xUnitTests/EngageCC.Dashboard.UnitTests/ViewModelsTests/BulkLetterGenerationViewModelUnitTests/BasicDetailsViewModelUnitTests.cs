﻿using Dashboard.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.BulkLetterGenerationViewModelUnitTests
{
    public class BasicDetailsViewModelUnitTests
    {
        [Fact]
        public void BasicDetailsViewModel_AllPropertiesSet_ValidatesSuccessfully()
        {
            // Arrange
            var model = new BasicDetailsViewModel
            {
                BatchId = 1,
                PartnerName = "Partner Name",
                AdminName = "Admin Name",
                TemplateName = "Template Name",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
