﻿using Dashboard.Models.BulkLetterGeneration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.BulkLetterGenerationViewModelUnitTests
{
    public class DeleteByBatchViewModelUnitTests
    {
        [Fact]
        public void DeleteByBatchViewModel_ValidatesSuccessfully()
        {
            // Arrange
            var model = new DeleteByBatchViewModel
            {
                BatchId = 123,
                DeletedBy = "John Doe",
                IsDeleted = true,
                DeletedOn = DateTime.Now,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
