﻿using Dashboard.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.BulkLetterGenerationViewModelUnitTests
{
    public class BatchDetailsViewModelUnitTests
    {
        [Fact]
        public void BatchDetailsViewModel_ValidatesSuccessfully()
        {
            // Arrange
            var model = new BatchDetailsViewModel
            {
                BatchId = 1,
                PartnerName = "Partner ABC",
                OfficeName = "Office XYZ",
                TemplateName = "Template 123",
                LetterCount = 10,
                LastUpdateOn = DateTime.Now,
                BatchStatus = "Active",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
