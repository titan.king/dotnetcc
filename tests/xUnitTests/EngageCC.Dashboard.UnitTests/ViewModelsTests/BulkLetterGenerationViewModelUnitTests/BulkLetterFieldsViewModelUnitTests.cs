﻿using Dashboard.Models.BulkLetterGeneration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.BulkLetterGenerationViewModelUnitTests
{
    public class BulkLetterFieldsViewModelUnitTests
    {
        [Fact]
        public void BulkLetterFieldsViewModel_ValidatesSuccessfully()
        {
            // Arrange
            var model = new BulkLetterFieldsViewModel
            {
                FieldList = new Fieldlist[] { new Fieldlist { FieldName = "Field1", FieldValue = "Value1" } },
                Esigning = "EsigningValue",
                Is7216Available = "Is7216AvailableValue",
                ClientSignatureCount = 1,
                ClientId = 2,
                PartnerId = 3,
                BulkLettersId = 4,
                LetterVersion = 5,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
