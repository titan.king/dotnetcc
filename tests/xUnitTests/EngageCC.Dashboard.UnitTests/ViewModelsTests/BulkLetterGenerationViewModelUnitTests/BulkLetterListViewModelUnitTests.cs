﻿using Dashboard.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.BulkLetterGenerationViewModelUnitTests
{
    public class BulkLetterListViewModelUnitTests
    {
        [Fact]
        public void BulkLetterListViewModel_ValidatesSuccessfully()
        {
            // Arrange
            var model = new BulkLetterListViewModel
            {
                RowId = 1,
                ClientId = 123,
                ClientName = "Client Name",
                OfficeName = "Office Name",
                SignatoryEmailId = "signatory@example.com",
                SignatoryFirstName = "John",
                PartnerName = "Partner",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
