﻿using Dashboard.Models.Blocks;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.BlocksViewModelUnitTests
{
    public class TemplateJSONViewModelUnitTests
    {
        [Fact]
        public void TemplateJSON_ValidatesSuccessfully()
        {
            // Arrange
            var template = new TemplateJSON.Rootobject
            {
                pages = new TemplateJSON.Page[]
                {
                new TemplateJSON.Page
                {
                    name = "Page 1",
                    elements = new TemplateJSON.Element[]
                    {
                        new TemplateJSON.Element
                        {
                            type = "Text",
                            name = "Element1",
                            visibleIf = "Condition1",
                            startWithNewLine = true,
                            title = "Element Title",
                            titleLocation = "top",
                            readOnly = false,
                            html = "<p>Some HTML content</p>",
                            hideNumber = true,
                            choices = new string[] { "Choice1", "Choice2" },
                            visible = true
                        },
                    }
                },
                }
            };

            // Act
            var validationContext = new ValidationContext(template);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(template, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
