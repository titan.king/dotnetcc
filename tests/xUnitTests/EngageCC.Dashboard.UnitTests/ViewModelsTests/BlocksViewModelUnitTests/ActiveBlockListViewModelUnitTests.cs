﻿using Dashboard.Models.Blocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.BlocksViewModelUnitTests
{
    public class ActiveBlockListViewModelUnitTests
    {
        [Fact]
        public void ActiveBlockListViewModel_PropertiesInitializedCorrectly()
        {
            // Arrange
            var viewModel = new ActiveBlockListViewModel();

            // Act and Assert
            Assert.Equal(0, viewModel.BlockId);
            Assert.Equal(string.Empty, viewModel.BlockName);
            Assert.Equal(string.Empty, viewModel.Description);
            Assert.Equal(string.Empty, viewModel.Status);
            Assert.Equal(string.Empty, viewModel.ChangeNotes);
            Assert.Equal(string.Empty, viewModel.Content);
        }

        [Fact]
        public void BlockName_CanBeSetAndGet()
        {
            // Arrange
            var viewModel = new ActiveBlockListViewModel();
            string expectedBlockName = "TestBlock";

            // Act
            viewModel.BlockName = expectedBlockName;

            // Assert
            Assert.Equal(expectedBlockName, viewModel.BlockName);
        }

        [Fact]
        public void Description_CanBeSetAndGet()
        {
            // Arrange
            var viewModel = new ActiveBlockListViewModel();
            string expectedDescription = "Description for testing";

            // Act
            viewModel.Description = expectedDescription;

            // Assert
            Assert.Equal(expectedDescription, viewModel.Description);
        }

        [Fact]
        public void Status_CanBeSetAndGet()
        {
            // Arrange
            var viewModel = new ActiveBlockListViewModel();
            string expectedStatus = "Status for testing";

            // Act
            viewModel.Status = expectedStatus;

            // Assert
            Assert.Equal(expectedStatus, viewModel.Status);
        }

        [Fact]
        public void ChangeNotes_CanBeSetAndGet()
        {
            // Arrange
            var viewModel = new ActiveBlockListViewModel();
            string expectedChangeNotes = "ChangeNotes for testing";

            // Act
            viewModel.ChangeNotes = expectedChangeNotes;

            // Assert
            Assert.Equal(expectedChangeNotes, viewModel.ChangeNotes);
        }

        [Fact]
        public void Content_CanBeSetAndGet()
        {
            // Arrange
            var viewModel = new ActiveBlockListViewModel();
            string expectedContent = "Content for testing";

            // Act
            viewModel.Content = expectedContent;

            // Assert
            Assert.Equal(expectedContent, viewModel.Content);
        }
    }
}
