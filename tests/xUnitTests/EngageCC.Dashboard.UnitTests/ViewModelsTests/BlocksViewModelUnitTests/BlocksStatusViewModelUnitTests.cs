﻿using Dashboard.Models.Blocks;
using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.BlocksViewModelUnitTests
{
    public class BlocksStatusViewModelUnitTests
    {
        [Fact]
        public void BlocksStatusViewModel_ValidatesSuccessfully()
        {
            // Arrange
            var model = new BlocksStatusViewModel
            {
                BlockStatus = new List<BlockStatus>
            {
                new BlockStatus
                {
                    StatusId = 1,
                    StatusName = "Status 1",
                },
                new BlockStatus
                {
                    StatusId = 2,
                    StatusName = "Status 2",
                }
            }
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
