﻿using Dashboard.Models.Blocks;
using System.ComponentModel.DataAnnotations;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.BlocksViewModelUnitTests
{
    public class BlocksViewListModelTests
    {
        [Fact]
        public void BlocksViewListModel_ValidValues_PassesValidation()
        {
            // Arrange
            var model = new BlocksListViewModel
            {
                BlockName = "Valid Name 1",
                Description = "Valid Description",
                Content = "Valid Content",
                QCRequired = true,
                EditFreeBlock = false,
                ChangeNotes = "Valid Change Notes",
                IsDeleted = false,
                StatusId = 1,
                CreatedOn = DateTime.Now,
                ModifiedOn = null
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void BlocksViewListModel_InvalidName_FailsValidation()
        {
            // Arrange
            var model = new BlocksListViewModel
            {
                BlockName = string.Empty

            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("BlockName"));
        }
    }
}
