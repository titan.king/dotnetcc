﻿using Dashboard.Models.Blocks;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.BlocksViewModelUnitTests
{
    public class BlocksListViewModelUnitTests
    {
        [Fact]
        public void BlocksListViewModel_AllProperties_ValidatesSuccessfully()
        {
            // Arrange
            var model = new BlocksListViewModel
            {
                BlockId = 1,
                BlockName = "Sample Block",
                Description = "Sample Description",
                Content = "Sample Content",
                ChangeNotes = "Sample Change Notes",
                IsDeleted = false,
                StatusId = 1,
                StatusName = "Active",
                CreatedOn = DateTime.Now,
                ModifiedOn = null,
                QCRequired = true,
                EditFreeBlock = false,
                CreatedBy = "John Doe",
                DeletedBy = string.Empty,
                DeletedOn = null,
                ModifiedBy = string.Empty,
                ConnectedTemplates = "Template1, Template2",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
