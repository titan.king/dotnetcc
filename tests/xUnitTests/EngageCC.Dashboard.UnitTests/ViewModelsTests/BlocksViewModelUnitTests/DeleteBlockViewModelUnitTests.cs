﻿using Dashboard.Models.Blocks;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.BlocksViewModelUnitTests
{
    public class DeleteBlockViewModelUnitTests
    {
        [Fact]
        public void DeleteBlockViewModel_Validation()
        {
            // Arrange
            var model = new DeleteBlockViewModel
            {
                BlockId = new int[] { 1, 2, 3 }, 
                DeletedBy = "John Doe",          
                IsDeleted = true,               
                ModifiedOn = DateTime.Now      
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
