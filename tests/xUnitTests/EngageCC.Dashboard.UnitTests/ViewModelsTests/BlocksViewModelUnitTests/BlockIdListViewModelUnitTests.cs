﻿using Dashboard.Models.Blocks;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.BlocksViewModelUnitTests
{
    public class BlockIdListViewModelUnitTests
    {
        [Fact]
        public void BlockIdListViewModel_ValidBlockId_ValidatesSuccessfully()
        {
            // Arrange
            var model = new BlockIdListViewModel
            {
                BlockId = 1,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

    
    }
}
