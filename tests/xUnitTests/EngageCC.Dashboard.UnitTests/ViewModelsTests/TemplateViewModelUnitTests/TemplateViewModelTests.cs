﻿using Dashboard.Models.Template;
using System.ComponentModel.DataAnnotations;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.TemplateViewModelUnitTests
{
    public class TemplateViewModelTests
    {
        [Fact]
        public void TemplateViewModel_ValidTemplateName_PassesValidation()
        {
            // Arrange
            var model = new TemplateViewModel
            {
                TemplateName = "ValidTemplateName",
                TemplateDescription = "ValidTemplateDescription",
                DepartmentId = 1,
                EngageTypeId = 3,
                ChangeNotes = "ValidChangeNotes",
                TemplateLogic = "ValidTemplateLogic",
                IsDeleted = false,
                CreatedBy = "ValidCreatedBy",
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void TemplateViewModel_InvalidTemplateName_FailsValidation()
        {
            // Arrange
            var model = new TemplateViewModel
            {
                TemplateName = string.Empty,
                TemplateDescription = "ValidTemplateDescription",
                DepartmentId = 1,
                EngageTypeId = 3,
                ChangeNotes = "ValidChangeNotes",
                TemplateLogic = "ValidTemplateLogic",
                IsDeleted = false,
                CreatedBy = "ValidCreatedBy",
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("TemplateName"));
        }
    }
}
