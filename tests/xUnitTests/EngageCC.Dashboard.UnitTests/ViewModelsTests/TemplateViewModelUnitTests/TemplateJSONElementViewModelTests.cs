﻿using System.ComponentModel.DataAnnotations;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.TemplateViewModelUnitTests
{
    public class TemplateJSONElementViewModelTests
    {
        [Fact]
        public void TemplateJSONElementViewModel_ValidValues_PassesValidation()
        {
            // Arrange
            var model = new global::Dashboard.Models.Template.TemplateJSONElementViewModel
            {
                PageType = "ValidType",
                PageName = "ValidName",
                PageVisibleIf = "ValidVisibleIf",
                PageStartWithNewLine = true,
                PageTitle = "ValidTitle",
                PageTitleLocation = "ValidTitleLocation",
                PageReadOnly = false,
                PageHtml = "<p>ValidHTML</p>",
                HideNumber = true,
                Choices = new string[] { "Choice1", "Choice2" },
                PageVisible = true
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }


        [Fact]
        public void TemplateJSONElementViewModel_InvalidName_FailsValidation()
        {
            // Arrange
            var model = new global::Dashboard.Models.Template.TemplateJSONElementViewModel
            {
                PageName = string.Empty,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("PageName"));
        }
    }
}
