﻿using Dashboard.Models.Template;
using System.ComponentModel.DataAnnotations;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.TemplateViewModelUnitTests
{
    public class TemplatePageViewModelTests
    {
        [Fact]
        public void TemplatePageViewModel_ValidValues_PassesValidation()
        {
            // Arrange
            var model = new TemplatePageViewModel
            {
                PageName = "ValidPageName",
                Elements = new TemplateJSONElementViewModel[]
                {
                    new TemplateJSONElementViewModel
                    {
                        PageType = "ValidType",
                        PageName = "ValidElementName",
                        PageVisibleIf = "ValidVisibleIf",
                        PageStartWithNewLine = true,
                        PageTitle = "ValidTitle",
                        PageTitleLocation = "ValidTitleLocation",
                        PageReadOnly = false,
                        PageHtml = "<p>ValidHTML</p>",
                        HideNumber = true,
                        Choices = new string[] { "Choice1", "Choice2" },
                        PageVisible = true
                    }
                }
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void TemplatePageViewModel_InvalidPageName_FailsValidation()
        {
            // Arrange
            var model = new TemplatePageViewModel
            {
                PageName = string.Empty,
                Elements = new TemplateJSONElementViewModel[]
                {
                    new TemplateJSONElementViewModel
                    {
                        PageType = "ValidType",
                        PageName = "ValidElementName",
                    }
                }
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("PageName"));
        }
    }
}

