﻿using Dashboard.Models.Template;
using System.ComponentModel.DataAnnotations;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.TemplateViewModelUnitTests
{
    public class TemplateApprovalViewModelTests
    {
        [Fact]
        public void ValidTemplateApprovalViewModel_ValidatesSuccessfully()
        {
            // Arrange
            var model = new TemplateApprovalViewModel
            {
                Id = 1,
                StatusId = 1,
                ApprovalComments = "Valid comments",
                ModifiedById = 1,
                ModifiedOn = DateTime.Now,
                TransactionStatus = "Approved"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void TemplateApprovalViewModel_InvalidApprovalComments_FailsValidation()
        {
            // Arrange
            var model = new TemplateApprovalViewModel
            {
                ApprovalComments = string.Empty, // Invalid
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("ApprovalComments"));
        }
    }
}
