﻿using Dashboard.Models.Fields;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.FieldsViewModelUnitTests
{
    public class DeleteFieldsViewModelUnitTests
    {
        [Fact]
        public void DeleteFieldsViewModel_AssignedValues_Validate()
        {
            // Arrange
            var fieldIds = new int[] { 1, 2, 3 };
            var deletedBy = "John Doe";
            var isDeleted = true;
            var modifiedOn = DateTime.Now;

            var model = new DeleteFieldsViewModel
            {
                FieldId = fieldIds,
                DeletedBy = deletedBy,
                IsDeleted = isDeleted,
                ModifiedOn = modifiedOn
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }


    }
}
