﻿using Dashboard.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.FieldsViewModelUnitTests
{
    public class SearchFilterViewModelUnitTests
    {
        [Fact]
        public void SearchFilterViewModel_WithAllAttributes_ValidatesSuccessfully()
        {
            // Arrange
            var model = new SearchFilterViewModel
            {
                EngagementLetterId = 1,
                EngagementLetterName = "Sample Letter",
                OfficeName = "Office A",
                TaxYear = 2023, // Valid TaxYear
                TemplateName = "Template 123",
                CilentName = "Client X",
                PartnerName = "Partner Y",
                Department = "Dept A",
                AdminName = "Admin Z",
                LetterStatus = "Active"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid); 
        }

        [Fact]
        public void TaxYear_RequiredAttribute_FailsValidation()
        {
            // Arrange
            var model = new SearchFilterViewModel
            {
                EngagementLetterId = 1,
                EngagementLetterName = "Sample Letter",
                OfficeName = "Office A",
                TaxYear = null, 
                TemplateName = "Template 123",
                CilentName = "Client X",
                PartnerName = "Partner Y",
                Department = "Dept A",
                AdminName = "Admin Z",
                LetterStatus = "Active"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid); 
            var taxYearValidationResult = validationResults.SingleOrDefault(result => result.MemberNames.Contains("TaxYear"));
            Assert.NotNull(taxYearValidationResult);
            Assert.Equal("TaxYear is required.", taxYearValidationResult.ErrorMessage); 
        }
    }
}
