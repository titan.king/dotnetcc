﻿using Dashboard.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.FieldsViewModelUnitTests
{
    public class SearchFilterViewModelTests
    {
        [Fact]
        public void SearchFilterViewModel_ValidValues_PassesValidation()
        {
            // Arrange
            var model = new SearchFilterViewModel
            {
                EngagementLetterId = 1,
                EngagementLetterName = "Valid Letter Name",
                OfficeName = "Valid Office Name",
                TaxYear = 2023,
                TemplateName = "Valid Template Name",
                CilentName = "Valid Client Name",
                PartnerName = "Valid Partner Name",
                Department = "Valid Department",
                AdminName = "Valid Admin Name",
                LetterStatus = "Valid Letter Status",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void SearchFilterViewModel_InvalidTaxYear_FailsValidation()
        {
            // Arrange
            var model = new SearchFilterViewModel
            {
                EngagementLetterId = 1,
                EngagementLetterName = "Valid Letter Name",
                OfficeName = "Valid Office Name",
                TaxYear = null, // Invalid 
                TemplateName = "Valid Template Name",
                CilentName = "Valid Client Name",
                PartnerName = "Valid Partner Name",
                Department = "Valid Department",
                AdminName = "Valid Admin Name",
                LetterStatus = "Valid Letter Status",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("TaxYear"));
        }
    }
}
