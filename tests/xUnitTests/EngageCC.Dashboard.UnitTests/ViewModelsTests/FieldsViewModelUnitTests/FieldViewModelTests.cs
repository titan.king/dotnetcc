﻿using Dashboard.Models.Fields;
using System.ComponentModel.DataAnnotations;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.FieldsViewModelUnitTests
{
    public class FieldViewModelTests
    {
        [Fact]
        public void FieldViewModel_ValidValues_PassesValidation()
        {
            // Arrange
            var model = new FieldViewModel
            {
                FieldId = 1,
                FieldName = "ValidName",
                DisplayName = "Valid Display Name",
                FieldDataTypeId = 2,
                HintText = "Valid Hint Text",
                ChangeNotes = "Valid Change Notes",
                IsDeleted = false,
                StatusId = 3,
                CreatedOn = DateTime.Now,
                TransactionStatus = "Valid Transaction Status",
                connectedblocks = "Block1,Block2",
                DirectInput = true,
                MDDLookup = false,
                ColumnId = 5,
                MDDColumnNames = "Column1,Column2",
                MDDColumnData = null
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void FieldViewModel_InvalidDisplayName_FailsValidation()
        {
            // Arrange
            var model = new FieldViewModel
            {
                FieldId = 1,
                FieldName = string.Empty,
                DisplayName = string.Empty, //Invalid 
                FieldDataTypeId = 2,
                HintText = "Valid Hint Text",
                ChangeNotes = "Valid Change Notes",
                IsDeleted = false,
                StatusId = 3,
                CreatedOn = DateTime.Now,
                TransactionStatus = "Valid Transaction Status",
                connectedblocks = "Block1,Block2",
                DirectInput = true,
                MDDLookup = false,
                ColumnId = 5,
                MDDColumnNames = "Column1,Column2",
                MDDColumnData = null
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("FieldName"));
        }

    }
}
