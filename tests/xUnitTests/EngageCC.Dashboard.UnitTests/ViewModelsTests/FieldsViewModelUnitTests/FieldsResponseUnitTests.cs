﻿using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.FieldsViewModelUnitTests
{
    public class FieldsResponseUnitTests
    {
        [Fact]
        public void FieldsResponse_PropertiesWithAssignedValues_ValidatesSuccessfully()
        {
            // Arrange
            var fieldList = new List<FieldListModel>
        {
            new FieldListModel { FieldId = 1, FieldName = "Field1" },
            new FieldListModel { FieldId = 2, FieldName = "Field2" }
        };

            var clientFields = new ClientFieldsViewModel
            {
                ClientId = 123,
                ClientFirstName = "ClientField"
            };

            var partnerOffice = new PartnerOfficeResModel
            {
                PartnerId = 456,
   
            };

            var model = new FieldsResponse
            {
                FieldList = fieldList,
                ClientFields = clientFields,
                PartnerOffice = partnerOffice
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
