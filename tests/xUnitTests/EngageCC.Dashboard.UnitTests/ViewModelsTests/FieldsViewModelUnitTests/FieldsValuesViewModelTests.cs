﻿using Dashboard.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.FieldsViewModelUnitTests
{
    public class FieldsValuesViewModelTests
    {
        [Fact]
        public void FieldsValuesViewModel_ValidValues_PassesValidation()
        {
            // Arrange
            var model = new FieldsValuesViewModel
            {
                BatchId = 1,
                LetterId = 2,
                LetterName = "Valid Letter",
                Office = "Valid Office",
                TaxYear = 2023,
                TemplateName = "Valid Template",
                ClientName = "Valid Client",
                PartnerName = "Valid Partner",
                AdminName = "Valid Admin",
                LetterStatus = "Valid Status",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void FieldsValuesViewModel_InvalidTaxYear_FailsValidation()
        {
            // Arrange
            var model = new FieldsValuesViewModel
            {
                BatchId = 1,
                LetterId = 2,
                LetterName = "Valid Letter",
                Office = "Valid Office",
                TaxYear = null,
                TemplateName = "Valid Template",
                ClientName = "Valid Client",
                PartnerName = "Valid Partner",
                AdminName = "Valid Admin",
                LetterStatus = "Valid Status",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("TaxYear"));
        }

    }
}
