﻿using Dashboard.Models.Fields;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.FieldsViewModelUnitTests
{
    public class FieldsGetByIdViewModelUnitTests
    {
        [Fact]
        public void FieldsGetByIdViewModel_AssignedValues_Validates()
        {
            // Arrange
            var model = new FieldsGetByIdViewModel
            {
                FieldId = 1,
                FieldName = "Field1",
                DisplayName = "Field One",
                FieldDataTypeId = 2,
                FieldDataType = "String",
                HintText = "Enter field data",
                ChangeNotes = "Updated field",
                IsDeleted = false,
                StatusId = 1,
                StatusName = "Active",
                CreatedOn = DateTime.Now,
                ModifiedOn = null,
                ModifiedBy = null,
                TransactionStatus = "Pending",
                connectedblocks = "Block1",
                DirectInput = true,
                MDDLookup = false,
                ColumnId = 3,
                MDDColumnNames = "ColumnA",
                MDDColumnData = null,
                CreatedBy = "User123",
                InputType = "Text"
            };
            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

    }
}
