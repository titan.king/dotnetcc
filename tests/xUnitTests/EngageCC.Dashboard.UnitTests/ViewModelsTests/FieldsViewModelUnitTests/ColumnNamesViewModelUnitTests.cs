﻿using Dashboard.Models.Fields;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.FieldsViewModelUnitTests
{
    public class ColumnNamesViewModelUnitTests
    {
        [Fact]
        public void ColumnNamesViewModel_AssignedValues_Validate()
        {
            // Arrange
            var model = new ColumnNamesViewModel
            {
                ColumnId = 1,
                ColumnNames = "Column A"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
