﻿using Dashboard.Models.Fields;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.FieldsViewModelUnitTests
{
    public class EditFieldsValuesUnitTests
    {
        [Fact]
        public void EditFieldsValues_AllAttributes_AssignedValuesValidate()
        {
            // Arrange
            var model = new EditFieldsValues
            {
                LetterFieldValuesId = 1,
                ClientId = 123,
                PartnerId = 456,
                BulkLettersId = 789,
                FieldId = 101,
                FieldValue = "Field Value",
                FieldName = "Field Name",
                LetterVersion = 2,
                DisplayName = "Display Name"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
