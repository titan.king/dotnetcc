﻿using Dashboard.Models.Fields;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.FieldsViewModelUnitTests
{
    public class EditFieldsRequestUnitTests
    {
        [Fact]
        public void EditFieldsRequest_AllAttributes_AssignedValuesValidate()
        {
            // Arrange
            var editField1 = new EditFields
            {
                FieldName = "Field1",
                FieldValue = "Value1"
            };

            var editField2 = new EditFields
            {
                FieldName = "Field2",
                FieldValue = "Value2"
            };

            var editFieldsList = new List<EditFields> { editField1, editField2 };

            var model = new EditFieldsRequest
            {
                EditFields = editFieldsList,
                ESigning = true,
                Is7216Available = false,
                ClientSignatureCount = 3
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);

        }
    }
}
