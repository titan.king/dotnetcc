﻿using Dashboard.Models.Fields;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.FieldsViewModelUnitTests
{
    public class EditFieldValuesResponseUnitTests
    {
        [Fact]
        public void EditFieldValuesResponse_AssignedValues_Validates()
        {
            // Arrange
            var editFieldsValues = new List<EditFieldsValues>
        {
            new EditFieldsValues { FieldName = "Field1", FieldValue = "Value1" },
            new EditFieldsValues { FieldName = "Field2", FieldValue = "Value2" }
        };

            var model = new EditFieldValuesResponse
            {
                EditFieldsValues = editFieldsValues,
                Is7216Available = true,
                ClientSignatureCount = 5,
                BulkLettersId = 123,
                IsEsigning = true
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
