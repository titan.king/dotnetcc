﻿using Dashboard.Models.Fields;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.FieldsViewModelUnitTests
{
    public class FieldViewModelUnitTests
    {
        [Fact]
        public void FieldName_RequiredAttribute_Validates()
        {
            // Arrange
            var model = new FieldViewModel
            {
                FieldId = 1,
                FieldName = "Valid Field Name",
                DisplayName = "Display Name",
                FieldDataTypeId = 2,
                FieldDataType = "Text",
                HintText = "Hint Text",
                ChangeNotes = "Change Notes",
                IsDeleted = false,
                StatusId = 3,
                StatusName = "Active",
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                TransactionStatus = "Pending",
                connectedblocks = "Block 123",
                DirectInput = true,
                MDDLookup = false,
                ColumnId = 4,
                ColumnName = "Column A",
                MDDColumnNames = "MDD Column",
                MDDColumnData = "MDD Data",
                ModifiedBy = "John Doe",
                CreatedBy = "Alice",
                InputType = "Text"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid); 
        }

        [Fact]
        public void FieldName_RequiredAttribute_FailsValidation()
        {
            // Arrange
            var model = new FieldViewModel
            {
                FieldId = 1,
                FieldName = null 
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            var fieldNameValidationResult = validationResults.SingleOrDefault(result => result.MemberNames.Contains("FieldName"));
            Assert.NotNull(fieldNameValidationResult); 
            Assert.Equal("FieldName is required", fieldNameValidationResult.ErrorMessage);
        }
    }
}
