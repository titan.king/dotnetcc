﻿using Dashboard.Models.Fields;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.FieldsViewModelUnitTests
{
    public class ClientFieldsViewModelUnitTests
    {
        [Fact]
        public void ClientFieldsViewModel_AllAttributes_AssignedValuesValidate()
        {
            // Arrange
            var model = new ClientFieldsViewModel
            {
                ClientFieldsId = 1,
                ClientFirstName = "John",
                Spouse = "Jane",
                ClientLastName = "Doe",
                Signing = "John S.",
                Officer = "Officer A",
                Lastname_Spouse = "Doe Spouse",
                TrustEsatate_Name = "Estate ABC",
                TrusteeFiduciary_Name = "Trustee X",
                TrusteeFiduciary_Last_Name = "Trustee Last",
                TrusteeFiduciary_Title = "Trustee Title",
                Year = "2023",
                TrustName = "Trust ABC",
                Trust = "Trust XYZ",
                HusbandFullName = "John Doe",
                WifeFullName = "Jane Doe",
                EstateName = "Estate XYZ",
                Trust_Name = "Trust Name",
                Street_Address = "123 Main St",
                City_Address = "456 Elm St",
                City = "City A",
                State = "State X",
                Business_Name = "Business ABC",
                ClientId = 123,
                NameofClients = "Client Names",
                AddressofClients = "Client Addresses",
                NameofClient = "Client Name",
                ClientName = "John's Client",
                Address1 = "789 Oak St",
                Address2 = "Suite 101",
                OfficeLocation = 456,
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                IsDeleted = true,
                DeletedOn = DateTime.Now
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid); 
        }
    }
}
