﻿using Dashboard.Models;
using System.ComponentModel.DataAnnotations;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.FieldsViewModelUnitTests
{
    public class EditFieldsViewModelTests
    {
        [Fact]
        public void EditFieldsViewModel_ValidValues_PassesValidation()
        {
            // Arrange
            var model = new EditFieldsViewModel
            {
                ClientId = 1,
                ClientName = "Valid Client",
                PartnerName = "Valid Partner",
                ContactPerson1 = "Person 1",
                ContactPerson1EmailId = "person1@example.com",
                ContactPerson1Title = "Title 1",
                ContactPerson2EmailId = "person2@example.com",
                ContactPerson2Title = "Title 2",
                Address1 = "Address Line 1",
                City = "City",
                State = "State",
                ZipCode = "12345",
                CreatedOnDate = DateTime.Now,
                TaxYear = 2023,
                OtherEntities = "Other Entities",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void EditFieldsViewModel_InvalidClientId_FailsValidation()
        {
            // Arrange
            var model = new EditFieldsViewModel
            {
                ClientId = null,
                ClientName = "Valid Client",
                PartnerName = "Valid Partner",
                ContactPerson1 = "Person 1",
                ContactPerson1EmailId = "person1@example.com",
                ContactPerson1Title = "Title 1",
                ContactPerson2EmailId = "person2@example.com",
                ContactPerson2Title = "Title 2",
                Address1 = "Address Line 1",
                City = "City",
                State = "State",
                ZipCode = "12345",
                CreatedOnDate = DateTime.Now,
                TaxYear = DateTime.Now.Year,
                OtherEntities = "Other Entities",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("ClientId"));
        }
    }
}
