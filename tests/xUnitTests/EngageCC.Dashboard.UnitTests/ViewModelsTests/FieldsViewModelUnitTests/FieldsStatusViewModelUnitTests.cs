﻿using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ViewModelsTests.FieldsViewModelUnitTests
{
    public class FieldsStatusViewModelUnitTests
    {
        [Fact]
        public void FieldsStatusViewModel_PopulatedProperties_AssignedValuesAreValid()
        {
            // Arrange
            var fieldsStatusList = new List<FieldsStatus>
        {
            new FieldsStatus { StatusId = 1, StatusName = "Active" },
            new FieldsStatus { StatusId = 2, StatusName = "Inactive" }
        };

            var fieldsDataTypeList = new List<FieldsDataType>
        {
            new FieldsDataType { FieldDataTypeId = 1, FieldType = "String" },
            new FieldsDataType { FieldDataTypeId = 2, FieldType = "Integer" }
        };

            var model = new FieldsStatusViewModel
            {
                FieldsStatus = fieldsStatusList,
                FieldsDataType = fieldsDataTypeList
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
