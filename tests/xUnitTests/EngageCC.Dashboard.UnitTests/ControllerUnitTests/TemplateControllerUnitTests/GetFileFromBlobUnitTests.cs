﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using EngageCC.DashboardAPI.Helpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage;
using Moq;
using System;
using System.Collections.Generic;
using System.IO.Abstractions;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.TemplateControllerUnitTests
{
    public class GetFileFromBlobUnitTests
    {
        private Mock<ITemplateService> _templateServiceMock;
        private Mock<IMapper> _mapperMock;
        private Mock<ILogger<TemplateController>> _loggerMock;
        private TemplateController _controller;
        private readonly Fixture _fixture;
        private Mock<ResourceManager> _resourceManagerMock;
        private Mock<IHttpContextAccessor> _httpContextAccessorMock;
        private Mock<IConfiguration> _configurationMock;
        private Mock<IFileSystem> _fileSystemMock;
        private Mock<IWebHostEnvironment> _envMock;
        private BlobStorageConfiguration _blobStorageConfig;

        public GetFileFromBlobUnitTests()
        {
            _templateServiceMock = new Mock<ITemplateService>();
            _mapperMock = new Mock<IMapper>();
            _loggerMock = new Mock<ILogger<TemplateController>>();
            _fixture = new Fixture();
            _resourceManagerMock = new Mock<ResourceManager>();
            _configurationMock = new Mock<IConfiguration>();
            var mockConfSection = new Mock<IConfigurationSection>();
            mockConfSection.SetupGet(m => m[It.Is<string>(s => s == "AzureBlobStorage")]).Returns("");

            _configurationMock.Setup(a => a.GetSection(It.Is<string>(s => s == "ConnectionStrings"))).Returns(mockConfSection.Object);
            _httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            _fileSystemMock = new Mock<IFileSystem>();
            _envMock = new Mock<IWebHostEnvironment>();
            _blobStorageConfig = new BlobStorageConfiguration(_configurationMock.Object);


            _controller = new TemplateController(
                _templateServiceMock.Object,
                _loggerMock.Object,
                _configurationMock.Object,
                _resourceManagerMock.Object,
                _mapperMock.Object,
                _httpContextAccessorMock.Object,
                _fileSystemMock.Object,
                _envMock.Object,
                _blobStorageConfig
            );
        }

        [Fact]
        public async Task GetFileFromBlob_Returns_Ok()
        {

            // Arrange
            var random = new Random();
            var accountName = "account" + random.Next(1000, 9999);
            var accountKey = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            var connectionString = $"DefaultEndpointsProtocol=https;AccountName={accountName};AccountKey={accountKey};EndpointSuffix=core.windows.net";
            var containerName = "container" + random.Next(1000, 9999);
            var fileName = "your_file_name";

            var blobSasTokenRequest = new BlobSasTokenRequest
            {
                AccountName = accountName,
                AccountKey = accountKey,
                ConnectionString = connectionString,
                ContainerName = containerName,
                FileName = fileName
            };

            var configuration = new ConfigurationBuilder().AddInMemoryCollection(new Dictionary<string, string>{
            { "BloBStorageConfig:AzureAccountName", accountName },
            { "ConnectionStrings:AzureBlobStorage", connectionString },
            { "BloBStorageConfig:AzureAccountKey", accountKey },
            { "BloBStorageConfig:AzureContainer_Html", containerName }
            }).Build();

            _configurationMock.Setup(a => a.GetSection(It.IsAny<string>())).Returns(configuration.GetSection);
            _configurationMock.Setup(a => a[It.IsAny<string>()]).Returns((string key) => configuration[key]);

            // Act
            var result = await _controller.GetFileFromBlob(blobSasTokenRequest);

            // Assert
            var responseModel = result;
            Assert.NotNull(responseModel);
            Assert.Equal(HttpStatusCode.OK, responseModel.StatusCode);
            Assert.NotNull(responseModel.Data);
        }

        [Fact]
        public async Task GetFileFromBlob_ThrowsException_ReturnsInternalServerError()
        {
            // Arrange
            var blobSasTokenRequest = new BlobSasTokenRequest
            {
                ConnectionString = "InvalidConnectionString"                                                         
            };

            // Act
            var result = await _controller.GetFileFromBlob(blobSasTokenRequest);

            // Assert
            var responseModel = result;
            Assert.NotNull(responseModel);
            Assert.Null(responseModel.Data);
            Assert.Equal(HttpStatusCode.InternalServerError, responseModel.StatusCode);
        }


    }
}

