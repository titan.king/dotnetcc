﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using EngageCC.DashboardAPI.Helpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.IO.Abstractions;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.TemplateControllerUnitTests
{
    public class GetCombinedPDFURLUnitTests
    {
        private Mock<ITemplateService> _templateServiceMock;
        private Mock<IMapper> _mapperMock;
        private Mock<ILogger<TemplateController>> _loggerMock;
        private TemplateController _controller;
        private readonly Fixture _fixture;
        private Mock<ResourceManager> _resourceManagerMock;
        private Mock<IHttpContextAccessor> _httpContextAccessorMock;
        private Mock<IConfiguration> _configurationMock;
        private Mock<IFileSystem> _fileSystemMock;
        private Mock<IWebHostEnvironment> _envMock;
        private BlobStorageConfiguration _blobStorageConfig;

        public GetCombinedPDFURLUnitTests()
        {
            _templateServiceMock = new Mock<ITemplateService>();
            _mapperMock = new Mock<IMapper>();
            _loggerMock = new Mock<ILogger<TemplateController>>();
            _fixture = new Fixture();
            _resourceManagerMock = new Mock<ResourceManager>();
            _configurationMock = new Mock<IConfiguration>();
            var mockConfSection = new Mock<IConfigurationSection>();
            mockConfSection.SetupGet(m => m[It.Is<string>(s => s == "AzureBlobStorage")]).Returns("");

            _configurationMock.Setup(a => a.GetSection(It.Is<string>(s => s == "ConnectionStrings"))).Returns(mockConfSection.Object);
            _httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            _fileSystemMock = new Mock<IFileSystem>();
            _envMock = new Mock<IWebHostEnvironment>();
            _blobStorageConfig = new BlobStorageConfiguration(_configurationMock.Object);


            _controller = new TemplateController(
                _templateServiceMock.Object,
                _loggerMock.Object,
                _configurationMock.Object,
                _resourceManagerMock.Object,
                _mapperMock.Object,
                _httpContextAccessorMock.Object,
                _fileSystemMock.Object,
                _envMock.Object,
                _blobStorageConfig
            );
        }


        [Fact]
        public async Task GetCombinedPDFURL_ValidInput_Returns_Success()
        {
            // Arrange

            var combinedPdf = _fixture.Create<CombinedPdfUrlModel>();
            var combinePDFResponse = "CombinePDFResponse";

            _templateServiceMock.Setup(service => service.UploadHTMLLetter(It.IsAny<LetterHTMLRequestModel>()))
                .ReturnsAsync(new ResponseModel
                {
                    Data = new HtmlFileResponseModel
                    {
                        StatusCode = HttpStatusCode.OK
                    }
                });

            _templateServiceMock.Setup(service => service.UploadAttachment(It.IsAny<AttachmentPdfFile>()))
                .ReturnsAsync(new List<string> { "Attachment1", "Attachment2" });

            _templateServiceMock.Setup(service => service.CombinePDFReq(It.IsAny<CombinePDFReq>()))
                .ReturnsAsync(new ResponseModel
                {
                    Data = combinePDFResponse,
                    StatusCode = HttpStatusCode.OK
                });

            // Act
            var result = await _controller.GetCombinedPDFURL(combinedPdf);

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            var data = responseModel.Data;
            Assert.NotNull(data);
            var statusCode = data?.GetType().GetProperty("StatusCode")?.GetValue(data);
            Assert.Equal(HttpStatusCode.OK, statusCode);
        }

        [Fact]
        public async Task GetCombinedPDFURL_InvalidInput_ThrowsException()
        {
            // Arrange
            var combinedPdf = _fixture.Create<CombinedPdfUrlModel>();

            _templateServiceMock.Setup(service => service.UploadHTMLLetter(It.IsAny<LetterHTMLRequestModel>()))
                .ThrowsAsync(new Exception("Simulated exception"));

            _resourceManagerMock.Setup(rm => rm.GetString("ErrorMessage")).Returns("Test error message");

            // Act
            var result = _controller.GetCombinedPDFURL(combinedPdf);

            // Assert
            var data = result.Result;
            Assert.NotNull(data);
            Assert.False(data.Status);
            Assert.Equal("Simulated exception", data?.ErrorMessage);
            Assert.Equal(HttpStatusCode.InternalServerError, data?.StatusCode);
        }

    }
}
