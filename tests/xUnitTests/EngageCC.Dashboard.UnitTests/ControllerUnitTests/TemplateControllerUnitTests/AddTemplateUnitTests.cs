﻿using AutoFixture;
using AutoMapper;
using Azure.Storage.Blobs;
using Dashboard.Business.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Template;
using EngageCC.DashboardAPI.Controllers;
using EngageCC.DashboardAPI.Helpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.IO.Abstractions;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using EngageCC.DashboardAPI.Helpers;
using System.Runtime.CompilerServices;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.TemplateControllerUnitTests
{
    public class AddTemplateUnitTests
    {
        private Mock<ITemplateService> _templateServiceMock;
        private Mock<IMapper> _mapperMock;
        private Mock<ILogger<TemplateController>> _loggerMock;
        private TemplateController _controller;
        private readonly Fixture _fixture;
        private Mock<ResourceManager> _resourceManagerMock;
        private Mock<IHttpContextAccessor> _httpContextAccessorMock;
        private Mock<IConfiguration> _configurationMock;
        private Mock<IFileSystem> _fileSystemMock;
        private Mock<IWebHostEnvironment> _envMock;
        private BlobStorageConfiguration _blobStorageConfig;
       
        public AddTemplateUnitTests()
        {
            _templateServiceMock = new Mock<ITemplateService>();
            _mapperMock = new Mock<IMapper>();
            _loggerMock = new Mock<ILogger<TemplateController>>();
            _fixture = new Fixture();
            _resourceManagerMock = new Mock<ResourceManager>();
            _configurationMock = new Mock<IConfiguration>();
            var mockConfSection = new Mock<IConfigurationSection>();
            mockConfSection.SetupGet(m => m[It.Is<string>(s => s == "AzureBlobStorage")]).Returns("");
            _configurationMock.Setup(a => a.GetSection(It.Is<string>(s => s == "ConnectionStrings"))).Returns(mockConfSection.Object);
            _httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            _fileSystemMock = new Mock<IFileSystem>();
            _envMock = new Mock<IWebHostEnvironment>();
            _blobStorageConfig = new BlobStorageConfiguration(_configurationMock.Object);

            _controller = new TemplateController(
                _templateServiceMock.Object,
                _loggerMock.Object,
                _configurationMock.Object,
                _resourceManagerMock.Object,
                _mapperMock.Object,
                _httpContextAccessorMock.Object,
                _fileSystemMock.Object,
                _envMock.Object,
                _blobStorageConfig 
            );
        }

        [Fact]
        public async Task AddTemplate_ValidInput_Returns_Ok()
        {
            // Arrange
            var templateViewModel = _fixture.Build<TemplateViewModel>()
                                            .With(t => t.TemplateLogic, "{\"pages\":[{\"elements\":[{\"PageVisibleIf\":\"some condition\"}]}]}")
                                            .Create();

            var responseModel = new ResponseModel();
            _templateServiceMock.Setup(service => service.IsValid(It.IsAny<TemplateViewModel>()))
                .ReturnsAsync(Tuple.Create(true, string.Empty));
            _templateServiceMock.Setup(service => service.SaveTemplate(It.IsAny<TemplateViewModel>()))
                .ReturnsAsync(templateViewModel);
            _mapperMock.Setup(mapper => mapper.Map<TemplateViewModel>(It.IsAny<TemplateViewModel>()))
                .Returns(templateViewModel);

            // Act
            var result = await _controller.AddTemplate(templateViewModel);

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.True(response.Status);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }


        [Fact]
        public async Task AddTemplate_Exception_Returns_Internal_ServerError()
        {
            // Arrange
            var templateViewModel = _fixture.Build<TemplateViewModel>().Create();
            _templateServiceMock.Setup(service => service.IsValid(It.IsAny<TemplateViewModel>()))
                              .ThrowsAsync(new Exception("Simulated exception"));
            _resourceManagerMock.Setup(rm => rm.GetString("ErrorMessage")).Returns("Test error message");

            // Act
            var result = await _controller.AddTemplate(templateViewModel);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
        }

        [Fact]
        public async Task AddTemplate_Invalid_ModelState_Returns_InternalServerError()
        {
            // Arrange
            var invalidTemplateViewModel = new TemplateViewModel();
            var errorMessage = "Template Logic requested is in Incorrect Format";
            _templateServiceMock.Setup(service => service.IsValid(It.IsAny<TemplateViewModel>()))
                              .ReturnsAsync(Tuple.Create(false, errorMessage));

            // Act
            var result = await _controller.AddTemplate(invalidTemplateViewModel);

            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, result.StatusCode);
            Assert.False(result.Status);
            Assert.Contains(errorMessage, result.ErrorMessage);
        }

    }
}
