﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Template;
using EngageCC.DashboardAPI.Controllers;
using EngageCC.DashboardAPI.Helpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.IO.Abstractions;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.TemplateControllerUnitTests
{
    public class DeleteTemplateUnitTests
    {
        private Mock<ITemplateService> _templateServiceMock;
        private Mock<IMapper> _mapperMock;
        private Mock<ILogger<TemplateController>> _loggerMock;
        private TemplateController _controller;
        private readonly Fixture _fixture;
        private Mock<ResourceManager> _resourceManagerMock;
        private Mock<IHttpContextAccessor> _httpContextAccessorMock;
        private Mock<IConfiguration> _configurationMock;
        private Mock<IFileSystem> _fileSystemMock;
        private Mock<IWebHostEnvironment> _envMock;
        private BlobStorageConfiguration _blobStorageConfig;

        public DeleteTemplateUnitTests()
        {
            _templateServiceMock = new Mock<ITemplateService>();
            _mapperMock = new Mock<IMapper>();
            _loggerMock = new Mock<ILogger<TemplateController>>();
            _fixture = new Fixture();
            _resourceManagerMock = new Mock<ResourceManager>();
            _configurationMock = new Mock<IConfiguration>();
            var mockConfSection = new Mock<IConfigurationSection>();
            mockConfSection.SetupGet(m => m[It.Is<string>(s => s == "AzureBlobStorage")]).Returns("");
            _configurationMock.Setup(a => a.GetSection(It.Is<string>(s => s == "ConnectionStrings"))).Returns(mockConfSection.Object);
            _httpContextAccessorMock = new Mock<IHttpContextAccessor>();
            _fileSystemMock = new Mock<IFileSystem>();
            _envMock = new Mock<IWebHostEnvironment>();
            _blobStorageConfig = new BlobStorageConfiguration(_configurationMock.Object);

            _controller = new TemplateController(
                _templateServiceMock.Object,
                _loggerMock.Object,
                _configurationMock.Object,
                _resourceManagerMock.Object,
                _mapperMock.Object,
                _httpContextAccessorMock.Object,
                _fileSystemMock.Object,
                _envMock.Object,
                _blobStorageConfig
            );
        }

        [Fact]
        public async Task DeleteTemplate_Success_Returns_OK()
        {
            // Arrange
            var deleteModel = _fixture.Create<TemplateViewModel>();
            _templateServiceMock.Setup(service => service.DeleteTemplate(deleteModel)).ReturnsAsync(true);

            // Act
            var result = await _controller.DeleteTemplate(deleteModel);

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.True(response.Status);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.True((bool)response.Data);
        }

        [Fact]
        public async Task DeleteTemplate_Failure_Returns_NotAcceptable()
        {
            // Arrange
            var deleteModel = _fixture.Create<TemplateViewModel>();
            _templateServiceMock.Setup(service => service.DeleteTemplate(deleteModel)).ReturnsAsync(false);

            // Act
            var result = await _controller.DeleteTemplate(deleteModel);

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.False(response.Status);
            Assert.Equal(HttpStatusCode.NotAcceptable, response.StatusCode);
            Assert.False((bool)response.Data);
        }

        [Fact]
        public async Task DeleteTemplate_Exception_Returns_InternalServerError()
        {
            // Arrange
            var deleteModel = _fixture.Create<TemplateViewModel>();
            _templateServiceMock.Setup(service => service.DeleteTemplate(deleteModel)).ThrowsAsync(new Exception("Simulated error"));
            _resourceManagerMock.Setup(rm => rm.GetString("ErrorMessage")).Returns("Simulated error");

            // Act
            var result = await _controller.DeleteTemplate(deleteModel);

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.False(response.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, response.StatusCode);
        }
    }
}
