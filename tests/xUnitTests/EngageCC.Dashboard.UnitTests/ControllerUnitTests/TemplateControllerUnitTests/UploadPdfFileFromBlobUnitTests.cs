﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using EngageCC.DashboardAPI.Helpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Moq;
using Newtonsoft.Json.Linq;
using RestSharp.Validation;
using System;
using System.Collections.Generic;
using System.IO.Abstractions;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.TemplateControllerUnitTests
{
    //CloudBlobContainerWrapper created to mock blob.DeleteIfExistsAsync()
    public class CloudBlobContainerWrapper
    {
        private readonly string _accountName;
        private readonly string _accountKey;
        private readonly string _containerName;
        private readonly CloudBlobClient _blobClient;

        public CloudBlobContainerWrapper(string accountName, string accountKey, string containerName)
        {
            _accountName = accountName;
            _accountKey = accountKey;
            _containerName = containerName;
            _blobClient = new CloudStorageAccount(new StorageCredentials(accountName, accountKey), true).CreateCloudBlobClient();
        }

        public CloudBlockBlob GetBlockBlobReference(string blobName)
        {
            var container = _blobClient.GetContainerReference(_containerName);
            container.CreateIfNotExistsAsync().Wait();  // Create the container if it doesn't exist

            return container.GetBlockBlobReference(blobName);
        }
    }

    public class UploadPdfFileFromBlobUnitTests
    {
        private readonly Mock<ITemplateService> _templateServiceMock;
        private readonly Mock<IMapper> _mapperMock;
        private readonly Mock<ILogger<TemplateController>> _loggerMock;
        private readonly Mock<IConfiguration> _configurationMock;
        private readonly TemplateController _controller;

        public UploadPdfFileFromBlobUnitTests()
        {
            // Setup mock configuration with random Azure Blob Storage account information
            var random = new Random();
            var randomAccountName = "account" + random.Next(1000, 9999);
            var randomAccountKey = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            var randomConnectionString = $"DefaultEndpointsProtocol=https;AccountName={randomAccountName};AccountKey={randomAccountKey};EndpointSuffix=core.windows.net";
            var randomContainerName = "container" + random.Next(1000, 9999);

            var configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(new Dictionary<string, string>
                {
                    { "BloBStorageConfig:AzureAccountName", randomAccountName },
                    { "ConnectionStrings:AzureBlobStorage", randomConnectionString },
                    { "BloBStorageConfig:AzureAccountKey", randomAccountKey },
                    { "BloBStorageConfig:AzureContainer_Html", randomContainerName }
                })
                .Build();

            _configurationMock = new Mock<IConfiguration>();
            _configurationMock.Setup(a => a.GetSection(It.IsAny<string>())).Returns(configuration.GetSection);
            _configurationMock.Setup(a => a[It.IsAny<string>()]).Returns((string key) => configuration[key]);

            // Create mocks and controller instance
            _templateServiceMock = new Mock<ITemplateService>();
            _mapperMock = new Mock<IMapper>();
            _loggerMock = new Mock<ILogger<TemplateController>>();

            _controller = new TemplateController(
                _templateServiceMock.Object,
                _loggerMock.Object,
                _configurationMock.Object,
                new Mock<ResourceManager>().Object,
                _mapperMock.Object,
                new Mock<IHttpContextAccessor>().Object,
                new Mock<IFileSystem>().Object,
                new Mock<IWebHostEnvironment>().Object,
                new BlobStorageConfiguration(_configurationMock.Object)
            );
        }

        //[Fact]
        //public async Task UploadPdfFileFromBlob_ValidInput_ReturnsSuccess()
        //{
        //    // Arrange
        //    var accountName = _configurationMock.Object["BloBStorageConfig:AzureAccountName"];
        //    var accountKey = _configurationMock.Object["BloBStorageConfig:AzureAccountKey"];
        //    var containerName = _configurationMock.Object["BloBStorageConfig:AzureContainer_Html"];

        //    var uploadPdfFile = new UploadPdfFile
        //    {
        //        PdfUrl = "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf",
        //        FileName = "dummy.pdf"
        //    };

        //    var blobName = "dummy.pdf";
        //    var mockBlobUri = new Uri($"http://{accountName}.blob.core.windows.net/{containerName}/{blobName}");
        //    var mockBlob = new Mock<CloudBlockBlob>(mockBlobUri);
        //    mockBlob.Setup(blob => blob.DeleteIfExistsAsync())
        //            .ReturnsAsync(true);
        //    var cloudBlobContainerWrapper = new CloudBlobContainerWrapper(accountName, accountKey, containerName);

        //    // Act
        //    var result = await _controller.UploadPdfFileFromBlob(uploadPdfFile);

        //    // Assert
        //    Assert.NotNull(result);
        //    Assert.Equal(HttpStatusCode.OK, result.StatusCode);
        //}

        [Fact]
        public async Task UploadPdfFileFromBlob_InvalidInput_ReturnsError()
        {
            // Arrange
            var uploadPdfFile = new UploadPdfFile
            {
                PdfUrl = null, 
                FileName = "dummy.pdf"
            };

            // Act
            var result = await _controller.UploadPdfFileFromBlob(uploadPdfFile);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(HttpStatusCode.NoContent, result.StatusCode);
            Assert.Equal("Pdf url is null or engagement letter name is null", result.ErrorMessage);
        }



    }
}
