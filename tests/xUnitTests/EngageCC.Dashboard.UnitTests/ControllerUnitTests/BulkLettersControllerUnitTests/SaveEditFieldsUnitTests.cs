﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Fields;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using System.Net;
using System.Resources;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.BulkLettersControllerUnitTests
{
    public class SaveEditFieldsUnitTests
    {
        private Mock<IBulkLettersService> _bulkLettersServiceMock;
        private Mock<IMapper> _mapperMock;
        private Mock<ILogger<BulkLettersController>> _loggerMock;
        private Mock<ResourceManager> _resourceManagerMock;
        private BulkLettersController _controller;
        private readonly Fixture _fixture;
        private Mock<IClientService> _clientServiceMock;
        public SaveEditFieldsUnitTests()
        {
            _bulkLettersServiceMock = new Mock<IBulkLettersService>();
            _mapperMock = new Mock<IMapper>();
            _loggerMock = new Mock<ILogger<BulkLettersController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _fixture = new Fixture();
            _clientServiceMock = new Mock<IClientService>();
            _controller = new BulkLettersController(
                _bulkLettersServiceMock.Object,
                _loggerMock.Object,
                _resourceManagerMock.Object,
                _mapperMock.Object,
                  _clientServiceMock.Object
            );
        }

        //Test case for Save Edit Fields
        [Fact]
        public async Task SaveEditFields_ValidInput_ReturnsOk()
        {
            // Arrange
            var editFieldValuesResponse = _fixture.Create<EditFieldValuesResponse>();
            _bulkLettersServiceMock.Setup(service => service.SaveEditFields(It.IsAny<EditFieldValuesResponse>()))
                                   .ReturnsAsync(true);

            // Act
            var result = await _controller.SaveEditFields(editFieldValuesResponse);

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.True(response.Status);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.True((bool?)response.Data);
        }

        [Fact]
        public async Task SaveEditFields_ExceptionThrown_ReturnsInternalServerError()
        {
            // Arrange
            var editFieldValuesResponse = _fixture.Create<EditFieldValuesResponse>();
            var exceptionMessage = "Test exception message";
            _bulkLettersServiceMock.Setup(service => service.SaveEditFields(It.IsAny<EditFieldValuesResponse>()))
                                   .Throws(new Exception(exceptionMessage));
            _resourceManagerMock.Setup(rm => rm.GetString("ErrorMessage")).Returns("Test exception message.");
            // Act
            var result = await _controller.SaveEditFields(editFieldValuesResponse);

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.False(response.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, response.StatusCode);
            Assert.Contains(exceptionMessage, response.ErrorMessage);
            Assert.Null(response.Data);
        }
    }
}
