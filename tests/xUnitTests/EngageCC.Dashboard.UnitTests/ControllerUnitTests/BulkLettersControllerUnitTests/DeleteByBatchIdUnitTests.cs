﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.CommonModel;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using System.Net;
using System.Resources;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.BulkLettersControllerUnitTests
{
    public class DeleteByBatchIdUnitTests
    {
        private Mock<IBulkLettersService> _bulkLettersServiceMock;
        private Mock<IMapper> _mapperMock;
        private Mock<ILogger<BulkLettersController>> _loggerMock;
        private Mock<ResourceManager> _resourceManagerMock;
        private BulkLettersController _controller;
        private readonly Fixture _fixture;
        private Mock<IClientService> _clientServiceMock;
        public DeleteByBatchIdUnitTests()
        {
            _bulkLettersServiceMock = new Mock<IBulkLettersService>();
            _mapperMock = new Mock<IMapper>();
            _loggerMock = new Mock<ILogger<BulkLettersController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _fixture = new Fixture();
            _clientServiceMock = new Mock<IClientService>();

            _controller = new BulkLettersController(
                _bulkLettersServiceMock.Object,
                _loggerMock.Object,
                _resourceManagerMock.Object,
                _mapperMock.Object,
                  _clientServiceMock.Object
            );
        }

        //Test case for Delete By BatchId
        [Fact]
        public async Task DeleteByBatchId_ValidInput_ReturnsOk()
        {
            // Arrange
            var deleteByBatchViewModel = _fixture.Create<DeleteByBatchViewModel>();
            var deleteByBatchResModel = _mapperMock.Setup(mapper => mapper.Map<DeleteByBatchResModel>(It.IsAny<DeleteByBatchViewModel>()))
                                                  .Returns(_fixture.Create<DeleteByBatchResModel>());
            _bulkLettersServiceMock.Setup(service => service.DeleteByBatchId(It.IsAny<DeleteByBatchResModel>()))
                                   .ReturnsAsync(true);

            // Act
            var result = await _controller.DeleteByBatchId(deleteByBatchViewModel);

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.True(response.Status);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.True((bool?)response.Data);
        }

        [Fact]
        public async Task DeleteByBatchId_ExceptionThrown_ReturnsInternalServerError()
        {
            // Arrange
            var deleteByBatchViewModel = _fixture.Create<DeleteByBatchViewModel>();
            var exceptionMessage = "Test exception message";
            _mapperMock.Setup(mapper => mapper.Map<DeleteByBatchResModel>(It.IsAny<DeleteByBatchViewModel>()))
                       .Throws(new Exception(exceptionMessage));

            // Act
            var result = await _controller.DeleteByBatchId(deleteByBatchViewModel);

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.False(response.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, response.StatusCode);
        }
    }
}
