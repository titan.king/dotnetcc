﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.CommonModel;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.BulkLettersControllerUnitTests
{
    public class DeleteByBulkLetterIdUnitTests
    {

        private Mock<IBulkLettersService> _bulkLettersServiceMock;
        private Mock<IMapper> _mapperMock;
        private Mock<ILogger<BulkLettersController>> _loggerMock;
        private Mock<ResourceManager> _resourceManagerMock;
        private BulkLettersController _controller;
        private readonly Fixture _fixture;
        private Mock<IClientService> _clientServiceMock;
        public DeleteByBulkLetterIdUnitTests()
        {
            _bulkLettersServiceMock = new Mock<IBulkLettersService>();
            _mapperMock = new Mock<IMapper>();
            _loggerMock = new Mock<ILogger<BulkLettersController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _fixture = new Fixture();
            _clientServiceMock = new Mock<IClientService>();

            _controller = new BulkLettersController(
                _bulkLettersServiceMock.Object,
                _loggerMock.Object,
                _resourceManagerMock.Object,
                _mapperMock.Object,
                  _clientServiceMock.Object
            );
        }

        //Test case for Delete By BulkLetterId
        [Fact]
        public async Task DeleteByBulkLetterId_ValidInput_ReturnsOk()
        {
            // Arrange
            var deleteBulkLetterViewModel = _fixture.Create<DeleteBulkLetterViewModel>();
            var deleteBulkLetterResModel = _mapperMock.Setup(mapper => mapper.Map<DeleteBulkLetterResModel>(It.IsAny<DeleteBulkLetterViewModel>()))
                                                      .Returns(_fixture.Create<DeleteBulkLetterResModel>());
            _bulkLettersServiceMock.Setup(service => service.DeleteByBulkLetterId(It.IsAny<DeleteBulkLetterResModel>()))
                                   .ReturnsAsync(true);

            // Act
            var result = await _controller.DeleteByBulkLetterId(deleteBulkLetterViewModel);

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.True(response.Status);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.True((bool?)response.Data);
        }

        [Fact]
        public async Task DeleteByBulkLetterId_ExceptionThrown_ReturnsInternalServerError()
        {
            // Arrange
            var deleteBulkLetterViewModel = _fixture.Create<DeleteBulkLetterViewModel>();
            var exceptionMessage = "Test exception message";
            _mapperMock.Setup(mapper => mapper.Map<DeleteBulkLetterResModel>(It.IsAny<DeleteBulkLetterViewModel>()))
                       .Throws(new Exception(exceptionMessage));

            // Act
            var result = await _controller.DeleteByBulkLetterId(deleteBulkLetterViewModel);

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.False(response.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, response.StatusCode);
        }
    }
}
