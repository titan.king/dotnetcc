﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using System.Net;
using System.Resources;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.BulkLettersControllerUnitTests
{
    public class InsertFieldsListUnitTests
    {
        private Mock<IClientService> _clientServiceMock;
        private Mock<IBulkLettersService> _bulkLettersServiceMock;
        private Mock<IMapper> _mapperMock;
        private Mock<ILogger<BulkLettersController>> _loggerMock;
        private Mock<ResourceManager> _resourceManagerMock;
        private BulkLettersController _controller;
        private readonly Fixture _fixture;

        public InsertFieldsListUnitTests()
        {
            _bulkLettersServiceMock = new Mock<IBulkLettersService>();
            _mapperMock = new Mock<IMapper>();
            _loggerMock = new Mock<ILogger<BulkLettersController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _fixture = new Fixture();
            _clientServiceMock = new Mock<IClientService>();

            _controller = new BulkLettersController(
                _bulkLettersServiceMock.Object,
                _loggerMock.Object,
                _resourceManagerMock.Object,
                _mapperMock.Object,
                _clientServiceMock.Object
            );
        }

        [Fact]
        public async Task InsertFieldsList_Returns_Successful_ResponseModel()
        {
            // Arrange
            var fieldsListViewModel = _fixture.Create<BulkLetterFieldsViewModel>();
            var bulkLetterFieldsResponseModel = new BulkLetterFieldsResponseModel();
            _bulkLettersServiceMock.Setup(service => service.InsertFieldsList(It.IsAny<BulkLetterFieldsResponseModel>())).ReturnsAsync(bulkLetterFieldsResponseModel);

            _mapperMock.Setup(mapper => mapper.Map<BulkLetterFieldsResponseModel>(It.IsAny<BulkLetterFieldsViewModel>())).Returns(bulkLetterFieldsResponseModel);

            // Act
            var result = await _controller.InsertFieldsList(fieldsListViewModel);

            // Assert
            Assert.NotNull(result);
            Assert.True(result.Status);
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
        }

        [Fact]
        public async Task InsertFieldsList_Returns_No_Content_On_Exception()
        {
            // Arrange

            _bulkLettersServiceMock.Setup(service => service.InsertFieldsList(It.IsAny<BulkLetterFieldsResponseModel>())).ThrowsAsync(new Exception("Simulated exception"));
            // Act
            var result = await _controller.InsertFieldsList(It.IsAny<BulkLetterFieldsViewModel>());

            // Assert
            Assert.NotNull(result);
            Assert.False(result.Status);
            Assert.Equal(HttpStatusCode.NoContent, result.StatusCode);
        }
    }
}
