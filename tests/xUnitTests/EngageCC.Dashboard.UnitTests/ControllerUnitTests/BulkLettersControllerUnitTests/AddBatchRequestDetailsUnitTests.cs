﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.Clients;
using Dashboard.Models.CommonModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using System.Net;
using System.Resources;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.BulkLettersControllerUnitTests
{
    public class AddBatchRequestDetailsUnitTests
    {
        private Mock<IBulkLettersService> _bulkLettersServiceMock;
        private Mock<IMapper> _mapperMock;
        private Mock<ILogger<BulkLettersController>> _loggerMock;
        private Mock<ResourceManager> _resourceManagerMock;
        private BulkLettersController _controller;
        private readonly Fixture _fixture;
        private Mock<IClientService> _clientServiceMock;
        public AddBatchRequestDetailsUnitTests()
        {
            _bulkLettersServiceMock = new Mock<IBulkLettersService>();
            _mapperMock = new Mock<IMapper>();
            _loggerMock = new Mock<ILogger<BulkLettersController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _fixture = new Fixture();
            _clientServiceMock = new Mock<IClientService>();

            _controller = new BulkLettersController(
                _bulkLettersServiceMock.Object,
                _loggerMock.Object,
                _resourceManagerMock.Object,
                _mapperMock.Object,
                  _clientServiceMock.Object
            );
        }

        //Test case for Add Batch Request Details
        [Fact]
        public async Task AddBatchRequestDetails_ValidInput_ReturnsOk()
        {
            // Arrange
            var batchRequestViewModel = _fixture.Create<BatchRequestViewModel>();
            var expectedResponse = new ResponseModel
            {
                Status = true,
                StatusCode = HttpStatusCode.OK
            };
            batchRequestViewModel.BatchJson = "";
            _mapperMock.Setup(mapper => mapper.Map<BatchRequestResponse>(It.IsAny<BatchRequestViewModel>()))
                       .Returns(_fixture.Create<BatchRequestResponse>());

            _bulkLettersServiceMock.Setup(service => service.AddBatchRequestDetails(It.IsAny<BatchRequestResponse>()))
                      .ReturnsAsync(new ResponseModel
                      {
                          Status = true,
                          StatusCode = HttpStatusCode.OK
                      });

            // Act
            var result = await _controller.AddBatchRequestDetails(batchRequestViewModel);

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.Equal(expectedResponse.Status, responseModel.Status);
        }


        [Fact]
        public async Task AddBatchRequestDetails_ExceptionThrown_ReturnsInternalServerError()
        {
            // Arrange
            var batchRequestViewModel = _fixture.Create<BatchRequestViewModel>();
            var exceptionMessage = "Test exception message";
            _mapperMock.Setup(mapper => mapper.Map<BatchRequestResponse>(It.IsAny<BatchRequestViewModel>()))
                       .Throws(new Exception(exceptionMessage));
            _resourceManagerMock.Setup(rm => rm.GetString("ErrorMessage")).Returns("Test exception message.");
            // Act
            var result = await _controller.AddBatchRequestDetails(batchRequestViewModel);

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.False(responseModel.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, responseModel.StatusCode);
        }
    }
}
