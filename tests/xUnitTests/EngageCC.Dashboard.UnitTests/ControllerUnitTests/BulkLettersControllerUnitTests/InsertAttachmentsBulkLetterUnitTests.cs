﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.CommonModel;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using System.Net;
using System.Resources;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.BulkLettersControllerUnitTests
{
    public class InsertAttachmentsBulkLetterUnitTests
    {
        private Mock<IBulkLettersService> _bulkLettersServiceMock;
        private Mock<IMapper> _mapperMock;
        private Mock<ILogger<BulkLettersController>> _loggerMock;
        private Mock<ResourceManager> _resourceManagerMock;
        private BulkLettersController _controller;
        private readonly Fixture _fixture;
        private Mock<IClientService> _clientServiceMock;
        public InsertAttachmentsBulkLetterUnitTests()
        {
            _bulkLettersServiceMock = new Mock<IBulkLettersService>();
            _mapperMock = new Mock<IMapper>();
            _loggerMock = new Mock<ILogger<BulkLettersController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _fixture = new Fixture();
            _clientServiceMock = new Mock<IClientService>();
            _controller = new BulkLettersController(
                _bulkLettersServiceMock.Object,
                _loggerMock.Object,
                _resourceManagerMock.Object,
                _mapperMock.Object,
                  _clientServiceMock.Object
            );
        }

        //Test case for Insert Attachments BulkLetter
        [Fact]
        public async Task InsertAttachmentsBulkLetter_ValidInput_ReturnsOk()
        {
            // Arrange
            var editAttachmentsViewModel = _fixture.Create<BulkLetterEditAttachmentsViewModel>();
            var editAttachmentsResponseModel = _mapperMock.Setup(mapper => mapper.Map<BulkLetterEditAttachmentsResponseModel>(It.IsAny<BulkLetterEditAttachmentsViewModel>()))
                                                          .Returns(_fixture.Create<BulkLetterEditAttachmentsResponseModel>());
            _bulkLettersServiceMock.Setup(service => service.InsertAttachmentsBulkLetter(It.IsAny<BulkLetterEditAttachmentsResponseModel>()))
                                   .ReturnsAsync(true);

            // Act
            var result = await _controller.InsertAttachmentsBulkLetter(editAttachmentsViewModel);

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.True(responseModel.Status);
            Assert.Equal(HttpStatusCode.OK, responseModel.StatusCode);
            Assert.True((bool?)responseModel.Data);
        }

        [Fact]
        public async Task InsertAttachmentsBulkLetter_ExceptionThrown_ReturnsInternalServerError()
        {
            // Arrange
            var editAttachmentsViewModel = _fixture.Create<BulkLetterEditAttachmentsViewModel>();
            var exceptionMessage = "Test exception message";
            _mapperMock.Setup(mapper => mapper.Map<BulkLetterEditAttachmentsResponseModel>(It.IsAny<BulkLetterEditAttachmentsViewModel>()))
                       .Throws(new Exception(exceptionMessage));
            _resourceManagerMock.Setup(rm => rm.GetString("ErrorMessage")).Returns("Test exception message.");
            // Act
            var result = await _controller.InsertAttachmentsBulkLetter(editAttachmentsViewModel);

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.False(responseModel.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, responseModel.StatusCode);
            Assert.Contains(exceptionMessage, responseModel.ErrorMessage);
            Assert.Null(responseModel.Data);
        }


    }
}
