﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.CommonModel;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using System.Net;
using System.Resources;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.BulkLettersControllerUnitTests
{
    public class GetCombinedPdfUrlUnitTests
    {
        private Mock<IBulkLettersService> _bulkLettersServiceMock;
        private Mock<IMapper> _mapperMock;
        private Mock<ILogger<BulkLettersController>> _loggerMock;
        private Mock<ResourceManager> _resourceManagerMock;
        private BulkLettersController _controller;
        private readonly Fixture _fixture;
        private Mock<IClientService> _clientServiceMock;
        public GetCombinedPdfUrlUnitTests()
        {
            _bulkLettersServiceMock = new Mock<IBulkLettersService>();
            _mapperMock = new Mock<IMapper>();
            _loggerMock = new Mock<ILogger<BulkLettersController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _fixture = new Fixture();
            _clientServiceMock = new Mock<IClientService>();
            _controller = new BulkLettersController(
                _bulkLettersServiceMock.Object,
                _loggerMock.Object,
                _resourceManagerMock.Object,
                _mapperMock.Object,
                  _clientServiceMock.Object
            );
        }

        //Test Case for Get Combined PDFURL
        [Fact]
        public async Task GetCombinedPDFURL_ValidInput_ReturnsOk()
        {
            // Arrange
            var combinedPdfViewModel = _fixture.Create<BulkLettersCombinePdfViewModel>();
            _bulkLettersServiceMock.SetupSequence(service => service.UploadHTMLLetter(It.IsAny<LetterHTMLRequestModel>()))
                                   .ReturnsAsync(new ResponseModel { Data = "HTMLAssetID", Status = true, StatusCode = HttpStatusCode.OK })
                                   .ReturnsAsync(new ResponseModel { Data = "PdfUrl", Status = true, StatusCode = HttpStatusCode.OK });

            _bulkLettersServiceMock.Setup(service => service.GetAttachmentsList(It.IsAny<int>(), It.IsAny<int>()))
                                   .ReturnsAsync(new List<AttachmentBase64String>());

            _bulkLettersServiceMock.Setup(service => service.UploadPdfFileFromBlob(It.IsAny<UploadPdfFile>()))
                                   .ReturnsAsync(new ResponseModel { Data = "PdfUrl", Status = true, StatusCode = HttpStatusCode.OK });

            _bulkLettersServiceMock.Setup(service => service.CombinePDFReq(It.IsAny<CombinePDFReq>()))
                                   .ReturnsAsync(new ResponseModel { Data = "CombinedPdfUrl", Status = true, StatusCode = HttpStatusCode.OK });

            // Act
            var result = await _controller.GetCombinedPDFURL(combinedPdfViewModel);

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.True(response.Status);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal("PdfUrl", response.Data); // Corrected expected value to "PdfUrl"
        }

        [Fact]
        public async Task GetCombinedPDFURL_InvalidInput_ReturnsInternalServerError()
        {
            // Arrange
            var combinedPdfViewModel = _fixture.Create<BulkLettersCombinePdfViewModel>();
            _bulkLettersServiceMock.Setup(service => service.UploadHTMLLetter(It.IsAny<LetterHTMLRequestModel>()))
                                   .Throws(new Exception("Simulated error"));
            _resourceManagerMock.Setup(rm => rm.GetString("ErrorMessage")).Returns("Test exception message.");
            // Act
            var result = await _controller.GetCombinedPDFURL(combinedPdfViewModel);

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.False(response.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, response.StatusCode);
        }

        [Fact]
        public async Task GetCombinedPDFURL_WithAttachments_ReturnsOk()
        {
            // Arrange
            var combinedPdfViewModel = _fixture.Create<BulkLettersCombinePdfViewModel>();
            _bulkLettersServiceMock.SetupSequence(service => service.UploadHTMLLetter(It.IsAny<LetterHTMLRequestModel>()))
                                   .ReturnsAsync(new ResponseModel { Data = "HTMLAssetID", Status = true, StatusCode = HttpStatusCode.OK })
                                   .ReturnsAsync(new ResponseModel { Data = "PdfUrl", Status = true, StatusCode = HttpStatusCode.OK });

            var attachments = new List<AttachmentBase64String> { new AttachmentBase64String { Base64String = "Attachment1" } };
            _bulkLettersServiceMock.Setup(service => service.GetAttachmentsList(It.IsAny<int>(), It.IsAny<int>()))
                                   .ReturnsAsync(attachments);

            _bulkLettersServiceMock.Setup(service => service.UploadAttachment(It.IsAny<AttachmentPdfFile>()))
                                   .ReturnsAsync(new List<string> { "AttachmentUrl" });

            _bulkLettersServiceMock.Setup(service => service.CombinePDFReq(It.IsAny<CombinePDFReq>()))
                                   .ReturnsAsync(new ResponseModel { Data = "CombinedPdfUrl", Status = true, StatusCode = HttpStatusCode.OK });

            // Act
            var result = await _controller.GetCombinedPDFURL(combinedPdfViewModel);

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.True(response.Status);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.NotNull(response.Data);
        }

        [Fact]
        public async Task GetCombinedPDFURL_AttachmentUploadFailure_ReturnsError()
        {
            // Arrange
            var combinedPdfViewModel = _fixture.Create<BulkLettersCombinePdfViewModel>();
            _bulkLettersServiceMock.SetupSequence(service => service.UploadHTMLLetter(It.IsAny<LetterHTMLRequestModel>()))
                                   .ReturnsAsync(new ResponseModel { Data = "HTMLAssetID", Status = true, StatusCode = HttpStatusCode.OK })
                                   .ReturnsAsync(new ResponseModel { Data = "PdfUrl", Status = true, StatusCode = HttpStatusCode.OK });

            var attachments = new List<AttachmentBase64String> { new AttachmentBase64String { Base64String = "Attachment1" } };

            _bulkLettersServiceMock.Setup(service => service.UploadAttachment(It.IsAny<AttachmentPdfFile>()))
                                   .ReturnsAsync((List<string>)null);
            _resourceManagerMock.Setup(rm => rm.GetString("ErrorMessage")).Returns("Test exception message.");
            // Act
            var result = await _controller.GetCombinedPDFURL(combinedPdfViewModel);

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.False(response.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, response.StatusCode);
        }

        [Fact]
        public async Task GetCombinedPDFURL_HTMLUploadFailure_ReturnsError()
        {
            // Arrange
            var combinedPdfViewModel = _fixture.Create<BulkLettersCombinePdfViewModel>();
            _bulkLettersServiceMock.SetupSequence(service => service.UploadHTMLLetter(It.IsAny<LetterHTMLRequestModel>()))
                                   .ReturnsAsync(new ResponseModel { Data = "HTMLAssetID", Status = true, StatusCode = HttpStatusCode.OK })
                                   .ReturnsAsync(new ResponseModel { Data = "PdfUrl", Status = true, StatusCode = HttpStatusCode.OK });

            var attachments = new List<AttachmentBase64String> { new AttachmentBase64String { Base64String = "Attachment1" } };

            _bulkLettersServiceMock.Setup(service => service.UploadHTMLLetter(It.IsAny<LetterHTMLRequestModel>()))
                                   .ReturnsAsync(new ResponseModel { Status = false, StatusCode = HttpStatusCode.BadRequest, ErrorMessage = "HTML upload failed" });
            _resourceManagerMock.Setup(rm => rm.GetString("ErrorMessage")).Returns("Test exception message.");
            // Act
            var result = await _controller.GetCombinedPDFURL(combinedPdfViewModel);

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.False(response.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, response.StatusCode);
        }
    }
}
