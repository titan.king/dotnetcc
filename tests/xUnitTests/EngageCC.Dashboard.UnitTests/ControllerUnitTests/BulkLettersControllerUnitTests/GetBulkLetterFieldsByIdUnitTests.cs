﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Fields;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using System.Net;
using System.Resources;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.BulkLettersControllerUnitTests
{
    public class GetBulkLetterFieldsByIdUnitTests
    {
        private Mock<IBulkLettersService> _bulkLettersServiceMock;
        private Mock<IMapper> _mapperMock;
        private Mock<ILogger<BulkLettersController>> _loggerMock;
        private Mock<ResourceManager> _resourceManagerMock;
        private BulkLettersController _controller;
        private readonly Fixture _fixture;
        private Mock<IClientService> _clientServiceMock;
        public GetBulkLetterFieldsByIdUnitTests()
        {
            _bulkLettersServiceMock = new Mock<IBulkLettersService>();
            _mapperMock = new Mock<IMapper>();
            _loggerMock = new Mock<ILogger<BulkLettersController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _fixture = new Fixture();
            _clientServiceMock = new Mock<IClientService>();
            _controller = new BulkLettersController(
                _bulkLettersServiceMock.Object,
                _loggerMock.Object,
                _resourceManagerMock.Object,
                _mapperMock.Object,
                  _clientServiceMock.Object
            );
        }

        //Test case for Get BulkLetter Fields ById
        [Fact]
        public async Task GetBulkLetterFieldsById_ValidInput_ReturnsOk()
        {
            // Arrange
            var bulkLetterId = 1;
            var editFieldValuesResponse = _fixture.Create<EditFieldValuesResponse>();
            _bulkLettersServiceMock.Setup(service => service.GetBulkLetterFieldsById(bulkLetterId))
                                   .ReturnsAsync(editFieldValuesResponse);

            // Act
            var result = await _controller.GetBulkLetterFieldsById(bulkLetterId);

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.True(response.Status);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(editFieldValuesResponse, response.Data);
        }

        [Fact]
        public async Task GetBulkLetterFieldsById_ExceptionThrown_ReturnsInternalServerError()
        {
            // Arrange
            var bulkLetterId = 1;
            var exceptionMessage = "Test exception message";
            _bulkLettersServiceMock.Setup(service => service.GetBulkLetterFieldsById(bulkLetterId))
                                   .Throws(new Exception(exceptionMessage));

            // Act
            var result = await _controller.GetBulkLetterFieldsById(bulkLetterId);

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.False(response.Status);
        }
    }
}
