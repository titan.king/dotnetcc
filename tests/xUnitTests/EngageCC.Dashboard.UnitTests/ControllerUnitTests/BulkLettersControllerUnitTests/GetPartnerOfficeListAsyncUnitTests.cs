﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.CommonModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.BulkLettersControllerUnitTests
{
    public class GetPartnerOfficeListAsyncUnitTests
    {
        private Mock<IBulkLettersService> _bulkLettersServiceMock;
        private Mock<IMapper> _mapperMock;
        private Mock<ILogger<BulkLettersController>> _loggerMock;
        private Mock<ResourceManager> _resourceManagerMock;
        private BulkLettersController _controller;
        private readonly Fixture _fixture;
        private Mock<IClientService> _clientServiceMock;
        public GetPartnerOfficeListAsyncUnitTests()
        {
            _bulkLettersServiceMock = new Mock<IBulkLettersService>();
            _mapperMock = new Mock<IMapper>();
            _loggerMock = new Mock<ILogger<BulkLettersController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _fixture = new Fixture();
            _clientServiceMock = new Mock<IClientService>();
            _controller = new BulkLettersController(
                _bulkLettersServiceMock.Object,
                _loggerMock.Object,
                _resourceManagerMock.Object,
                _mapperMock.Object,
                  _clientServiceMock.Object
            );
        }

        //Test case for Partner Office By BatchId

        [Fact]
        public async Task GetPartnerOfficeListAsync_ValidInput_ReturnsOk()
        {
            // Arrange
            var batchId = 1;
            var partnerOfficeResponses = _fixture.Create<List<PartnerOfficeResponse>>();
            _bulkLettersServiceMock.Setup(service => service.GetPartnerOfficeListAsync(batchId))
                                   .ReturnsAsync(partnerOfficeResponses);

            // Act
            var result = await _controller.GetPartnerOfficeListAsync(batchId);

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.True(response.Status);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(partnerOfficeResponses, response.Data);
        }

        [Fact]
        public async Task GetPartnerOfficeListAsync_ExceptionThrown_ReturnsInternalServerError()
        {
            // Arrange
            var batchId = 1;
            var exceptionMessage = "Test exception message";
            _bulkLettersServiceMock.Setup(service => service.GetPartnerOfficeListAsync(batchId))
                                   .Throws(new Exception(exceptionMessage));

            // Act
            var result = await _controller.GetPartnerOfficeListAsync(batchId);

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.False(response.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, response.StatusCode);
            Assert.Contains(exceptionMessage, response.ErrorMessage);
            Assert.Null(response.Data);
        }
    }
}
