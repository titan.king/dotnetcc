﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.Blocks;
using Dashboard.Models.CommonModel;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.BlocksControllerUnitTests
{
    public class DeleteBlockUnitTests
    {
        private Mock<IBlocksService> _blocksServiceMock;
        private Mock<IMapper> _mapperMock;
        private Mock<ILogger<BlocksController>> _loggerMock;
        private Mock<ResourceManager> _resourceManagerMock;
        private BlocksController _controller;
        private readonly Fixture _fixture;

        public DeleteBlockUnitTests()
        {
            _blocksServiceMock = new Mock<IBlocksService>();
            _mapperMock = new Mock<IMapper>();
            _loggerMock = new Mock<ILogger<BlocksController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _fixture = new Fixture();

            _controller = new BlocksController(
                _blocksServiceMock.Object,
                _loggerMock.Object,
                _resourceManagerMock.Object,
                _mapperMock.Object
            );
        }

        [Fact]
        public async Task DeleteBlock_ValidInput_Returns_Ok()
        {
            // Arrange
            var blockIdToDelete = 1;  // Block ID to delete
            var deleteBlockViewModel = new DeleteBlockViewModel { BlockId = new int[] { blockIdToDelete } };
            var deleteBlockResModel = new DeleteBlockResModel { IsDeleted = true };

            _blocksServiceMock.Setup(service => service.DeleteBlock(It.IsAny<DeleteBlockResModel>()))
                              .ReturnsAsync(deleteBlockResModel);

            // Act
            var result = await _controller.DeleteBlock(deleteBlockViewModel);

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);

            Assert.True(responseModel.Status);
            Assert.Equal(HttpStatusCode.OK, responseModel.StatusCode);
        }

        [Fact]
        public async Task DeleteBlock_Block_Not_Deleted_Returns_NotAcceptable()
        {
            // Arrange
            var blockIdToDelete = 1;  // Block ID to delete
            var deleteBlockViewModel = new DeleteBlockViewModel { BlockId = new int[] { blockIdToDelete } };
            var deleteBlockResModel = new DeleteBlockResModel { BlockId = new int[] { blockIdToDelete } };
            var deleteBlockResModel1 = new DeleteBlockResModel { IsDeleted = false };

            _blocksServiceMock.Setup(service => service.DeleteBlock(It.IsAny<DeleteBlockResModel>()))
                              .ReturnsAsync(deleteBlockResModel1);

            // Act
            var result = await _controller.DeleteBlock(deleteBlockViewModel);

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);

            Assert.False(responseModel.Status);
            Assert.Equal(HttpStatusCode.NotAcceptable, responseModel.StatusCode);
        }
    }
}
