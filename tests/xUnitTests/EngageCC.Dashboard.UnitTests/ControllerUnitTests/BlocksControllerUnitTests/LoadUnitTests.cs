﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.Blocks;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.BlocksControllerUnitTests
{
    public class LoadUnitTests
    {
        private Mock<IBlocksService> _blocksServiceMock;
        private Mock<IMapper> _mapperMock;
        private Mock<ILogger<BlocksController>> _loggerMock;
        private Mock<ResourceManager> _resourceManagerMock;
        private BlocksController _controller;
        private readonly Fixture _fixture;

        public LoadUnitTests()
        {
            _blocksServiceMock = new Mock<IBlocksService>();
            _mapperMock = new Mock<IMapper>();
            _loggerMock = new Mock<ILogger<BlocksController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _fixture = new Fixture();

            _controller = new BlocksController(
                _blocksServiceMock.Object,
                _loggerMock.Object,
                _resourceManagerMock.Object,
                _mapperMock.Object
            );
        }

        [Fact]
        public async Task Load_Returns_Successful_ResponseModel()
        {
            // Arrange
            var blocksStatusViewModel = _fixture.Create<BlocksStatusViewModel>();
            var blockStatusListResModel = new BlockStatusListResModel();
            _blocksServiceMock.Setup(service => service.Load()).ReturnsAsync(blockStatusListResModel);

            _mapperMock.Setup(mapper => mapper.Map<BlocksStatusViewModel>(It.IsAny<BlockStatusListResModel>())).Returns(blocksStatusViewModel);

            // Act
            var result = await _controller.Load();

            // Assert
            Assert.NotNull(result);
            Assert.True(result.Status);
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            Assert.Equal(blockStatusListResModel, result.Data);
        }

        [Fact]
        public async Task Load_Returns_InternalServerError_On_Exception()
        {
            // Arrange
            _blocksServiceMock.Setup(service => service.Load()).ThrowsAsync(new Exception("Simulated exception"));

            // Act
            var result = await _controller.Load();

            // Assert
            Assert.NotNull(result);
            Assert.False(result.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, result.StatusCode);
        }
    }
}
