﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.BlocksControllerUnitTests
{
    public class GetBlocksIdListUnitTests
    {
        private Mock<IBlocksService> _blocksServiceMock;
        private Mock<IMapper> _mapperMock;
        private Mock<ILogger<BlocksController>> _loggerMock;
        private Mock<ResourceManager> _resourceManagerMock;
        private BlocksController _controller;
        private readonly Fixture _fixture;

        public GetBlocksIdListUnitTests()
        {
            _blocksServiceMock = new Mock<IBlocksService>();
            _mapperMock = new Mock<IMapper>();
            _loggerMock = new Mock<ILogger<BlocksController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _fixture = new Fixture();

            _controller = new BlocksController(
                _blocksServiceMock.Object,
                _loggerMock.Object,
                _resourceManagerMock.Object,
                _mapperMock.Object
            );
        }


        [Fact]
        public async Task GetBlocksIdList_Returns_Ok_WithData()
        {
            // Arrange
            var expectedBlocksIdList = _fixture.CreateMany<BlockIdListResponseModel>(3).ToList();

            _blocksServiceMock.Setup(service => service.GetBlocksIdList())
                .ReturnsAsync(expectedBlocksIdList);

            // Act
            var result = await _controller.GetBlocksIdList();

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);

            Assert.True(responseModel.Status);
            Assert.Equal(HttpStatusCode.OK, responseModel.StatusCode);
            Assert.Equal(expectedBlocksIdList, responseModel.Data);
        }

        [Fact]
        public async Task GetBlocksIdList_Exception_Returns_Internal_ServerError()
        {
            // Arrange
            _blocksServiceMock.Setup(service => service.GetBlocksIdList())
                .ThrowsAsync(new Exception("Simulated exception"));

            // Act
            var result = await _controller.GetBlocksIdList();

            // Assert
            Assert.NotNull(result);
            Assert.Equal(HttpStatusCode.InternalServerError, result.StatusCode);
        }
    }
}
