﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.Blocks;
using Dashboard.Models.CommonModel;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.BlocksControllerUnitTests
{
    public class UpdateBlocksUnitTests
    {
        private Mock<IBlocksService> _blocksServiceMock;
        private Mock<IMapper> _mapperMock;
        private Mock<ILogger<BlocksController>> _loggerMock;
        private Mock<ResourceManager> _resourceManagerMock;
        private BlocksController _controller;
        private readonly Fixture _fixture;

        public UpdateBlocksUnitTests()
        {
            _blocksServiceMock = new Mock<IBlocksService>();
            _mapperMock = new Mock<IMapper>();
            _loggerMock = new Mock<ILogger<BlocksController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _fixture = new Fixture();

            _controller = new BlocksController(
                _blocksServiceMock.Object,
                _loggerMock.Object,
                _resourceManagerMock.Object,
                _mapperMock.Object
            );
        }

        //Test cases for Update Blocks
        [Fact]
        public async Task UpdateBlocks_ValidInput_Returns_Ok()
        {
            // Arrange
            var blocksViewModel = _fixture.Build<BlocksViewModel>().With(vm => vm.Content).Create();
            var blocksResponseModel = new BlocksResponseModel();

            _blocksServiceMock.Setup(service => service.UpdateBlocks(It.IsAny<BlocksResponseModel>()))
              .ReturnsAsync(Tuple.Create(blocksResponseModel, string.Empty));
            _blocksServiceMock.Setup(service => service.UpdateBlocks(It.IsAny<BlocksResponseModel>()))
                .ReturnsAsync(Tuple.Create(blocksResponseModel, string.Empty));
            _mapperMock.Setup(mapper => mapper.Map<BlocksResponseModel>(It.IsAny<BlocksViewModel>()))
                .Returns(blocksResponseModel);

            // Act
            var result = await _controller.UpdateBlocks(blocksViewModel);

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);

            Assert.True(responseModel.Status);
            Assert.Equal(HttpStatusCode.OK, responseModel.StatusCode);
        }


        [Fact]
        public async Task UpdateBlocks_Exception_Returns_Internal_ServerError()
        {
            // Arrange
            var blocksViewModel = _fixture.Build<BlocksViewModel>().With(vm => vm.Content).Create();

            _blocksServiceMock.Setup(service => service.UpdateBlocks(It.IsAny<BlocksResponseModel>()))
                              .ThrowsAsync(new Exception("Simulated exception"));
            _mapperMock.Setup(mapper => mapper.Map<BlocksResponseModel>(It.IsAny<BlocksViewModel>()))
                       .Throws(new Exception("Simulated exception"));
            _resourceManagerMock.Setup(rm => rm.GetString("ErrorMessage")).Returns("Test Error Message.");
            // Act
            var result = await _controller.UpdateBlocks(blocksViewModel);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(HttpStatusCode.InternalServerError, result.StatusCode);
        }

        [Fact]
        public async Task UpdateBlocks_Invalid_ModelState_Returns_Internal_ServerError()
        {
            // Arrange
            var invalidBlocksViewModel = new BlocksViewModel();
            var errorMessage = "The Content field is required.";

            // Mock the service to throw a ValidationException
            _blocksServiceMock.Setup(service => service.UpdateBlocks(It.IsAny<BlocksResponseModel>()))
                .ThrowsAsync(new ValidationException(errorMessage));
            _resourceManagerMock.Setup(rm => rm.GetString("ErrorMessage")).Returns("The Content field is required.");

            // Act
            var result = await _controller.UpdateBlocks(invalidBlocksViewModel);

            // Assert
            Assert.Equal(HttpStatusCode.InternalServerError, result.StatusCode);
            Assert.False(result.Status);
            Assert.Contains(errorMessage, "The Content field is required.");
        }
    }
}
