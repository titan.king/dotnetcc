﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.SigningControllerUnitTests
{
    public class AutoRefreshStatusUnitTests
    {
        private Mock<ISigningService> _signingServiceMock;
        private Mock<ILogger<SigningController>> _logger;
        private Mock<IConfiguration> _configurationMock;
        private Mock<ResourceManager> _resourceManagerMock;
        private Mock<IMapper> _mapper;
        private SigningController _controller;
        private readonly Fixture _fixture;

        public AutoRefreshStatusUnitTests()
        {
            _signingServiceMock = new Mock<ISigningService>();
            _logger = new Mock<ILogger<SigningController>>();
            _configurationMock = new Mock<IConfiguration>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _mapper = new Mock<IMapper>();
            _fixture = new Fixture();

            _controller = new SigningController(
                _signingServiceMock.Object,
                _configurationMock.Object,
                 _resourceManagerMock.Object,
                 _mapper.Object,
                _logger.Object
            );
        }

        [Fact]
        public async Task AutoRefreshStatus_ReturnsOkResult()
        {
            // Arrange
            _signingServiceMock.Setup(service => service.AutoRefreshStatus())
                .ReturnsAsync(new ResponseModel
                {
                    Data = "refreshedStatus",
                    Status = true,
                    StatusCode = HttpStatusCode.OK
                });

            // Act
            var actionResult = await _controller.AutoRefreshStatus();

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(actionResult);
            Assert.NotNull(responseModel.Data);
            Assert.True(responseModel.Status);
            Assert.Equal(HttpStatusCode.OK, responseModel.StatusCode);
        }

        [Fact]
        public async Task AutoRefreshStatus_ReturnsInternalServerErrorResultOnError()
        {
            // Arrange
            _signingServiceMock.Setup(service => service.AutoRefreshStatus())
                .ThrowsAsync(new Exception("Simulated exception"));

            // Act
            var actionResult = await _controller.AutoRefreshStatus();

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(actionResult);
            Assert.False(responseModel.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, responseModel.StatusCode);
            Assert.Equal("Simulated exception", responseModel.ErrorMessage); ;
        }

    }
}
