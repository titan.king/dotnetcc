﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Login;
using Dashboard.Models.Signing;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.SigningControllerUnitTests
{
    public class DeclineSigningUnitTests
    {
        private Mock<ISigningService> _signingServiceMock;
        private Mock<ILogger<SigningController>> _logger;
        private Mock<IConfiguration> _configurationMock;
        private Mock<ResourceManager> _resourceManagerMock;
        private Mock<IMapper> _mapper;
        private SigningController _controller;
        private readonly Fixture _fixture;

        public DeclineSigningUnitTests()
        {
            _signingServiceMock = new Mock<ISigningService>();
            _logger = new Mock<ILogger<SigningController>>();
            _configurationMock = new Mock<IConfiguration>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _mapper = new Mock<IMapper>();
            _fixture = new Fixture();

            _controller = new SigningController(
               _signingServiceMock.Object,
                _configurationMock.Object,
                 _resourceManagerMock.Object,
                 _mapper.Object,
                _logger.Object
            );
        }

        //[Fact]
        //public async void DeclineSigning_ValidInput_Returns_Ok()
        //{
        //    //Arrange
        //    var signingViewModel = new SigningViewModel
        //    {
        //        AgreementStatus = "CANCELLED",
        //        SigningRejectState = "REJECTED",
        //        AdobeIntegrationKey = "AdobeIntegrationKey",
        //        AdobeSignLink = "AdobeSignLink"
        //    };

        //    _signingServiceMock.Setup(x => x.DeclineSigning(It.IsAny<SigningViewModel>()))
        //  .ReturnsAsync((SigningViewModel inputViewModel) =>
        //  {
        //      inputViewModel.AgreementStatus = "DECLINED";
        //      return inputViewModel;
        //  });

        //    // Act
        //    var result = await _controller.DeclineSigning(signingViewModel);

        //    // Assert
        //    Assert.True(result.Status);
        //    Assert.Equal(HttpStatusCode.OK, result.StatusCode);
        //}

        //[Fact]
        //public async Task DeclineSigning_ThrowsException_ReturnsInternalServerError()
        //{
        //    // Arrange
        //    var signingViewModel = new SigningViewModel
        //    {
        //        AgreementStatus = "CANCELLED",
        //        SigningRejectState = "REJECTED",
        //        AdobeIntegrationKey = "AdobeIntegrationKey",
        //        AdobeSignLink = "AdobeSignLink"
        //    };

        //    _signingServiceMock.Setup(x => x.DeclineSigning(It.IsAny<SigningViewModel>()))
        //        .Throws(new Exception("Simulated exception"));

        //    // Act
        //    var result = await _controller.DeclineSigning(signingViewModel);

        //    // Assert
        //    var responseModel = Assert.IsType<ResponseModel>(result);

        //    Assert.False(responseModel.Status);
        //    Assert.Equal(HttpStatusCode.InternalServerError, responseModel.StatusCode);
        //    Assert.Equal("Simulated exception", responseModel.ErrorMessage);
        //}

    }
}
