﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Login;
using Dashboard.Models.ResponseModel;
using Dashboard.Models.Signing;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.SigningControllerUnitTests
{
    public class CancelSigningUnitTests
    {
        private Mock<ISigningService> _signingServiceMock;
        private Mock<ILogger<SigningController>> _logger;
        private Mock<IConfiguration> _configurationMock;
        private Mock<ResourceManager> _resourceManagerMock;
        private Mock<IMapper> _mapper;
        private SigningController _controller;
        private readonly Fixture _fixture;

        public CancelSigningUnitTests()
        {
            _signingServiceMock = new Mock<ISigningService>();
            _logger = new Mock<ILogger<SigningController>>();
            _configurationMock = new Mock<IConfiguration>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _mapper = new Mock<IMapper>();
            _fixture = new Fixture();

            _controller = new SigningController(
                _signingServiceMock.Object,
                _configurationMock.Object,
                 _resourceManagerMock.Object,
                 _mapper.Object,
                _logger.Object
            );
        }


        [Fact]
        public async Task CancelSigning_ValidModel_ReturnsOk()
        {
            // Arrange
            //var signingViewModel = new SigningViewModel(); // Initialize with valid data
            var expectedResponse = new ResponseModel
            {
                Status = true,
                StatusCode = HttpStatusCode.OK,
                //Data = new RefreshStatus()
            };

            var cancleSigningResModel = new CancleSigningResModel
            {
                EngagementLetterId = 1
            };

            var signingServiceMock = new Mock<ISigningService>();
            signingServiceMock.Setup(service => service.CancelSigning(It.IsAny<CancleSigningResModel>()))
                    .ReturnsAsync(expectedResponse);

            var configurationMock = new Mock<IConfiguration>();
            var resourceManagerMock = new Mock<ResourceManager>();
            var mapperMock = new Mock<IMapper>();
            var loggerMock = new Mock<ILogger<SigningController>>();

            var controller = new SigningController(signingServiceMock.Object, configurationMock.Object, resourceManagerMock.Object, mapperMock.Object, loggerMock.Object);

            // Act
            var result = await controller.CancelSigning(cancleSigningResModel);

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);

            Assert.NotNull(responseModel);
            Assert.NotNull(responseModel.Data);
            Assert.True(responseModel.Status);
            Assert.Equal(HttpStatusCode.OK, responseModel.StatusCode);
        }

        [Fact]
        public async Task CancelSigning_ThrowsException_ReturnsInternalServerError()
        {
            // Arrange
            var cancleSigningResModel = new CancleSigningResModel
            {
               EngagementLetterId = 1
            };

            _signingServiceMock.Setup(x => x.CancelSigning(It.IsAny<CancleSigningResModel>()))
                .Throws(new Exception("Simulated exception"));

            // Act
            var result = await _controller.CancelSigning(cancleSigningResModel);

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.False(responseModel.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, responseModel.StatusCode);
            Assert.Equal("Simulated exception", responseModel.ErrorMessage);
        }
    }
}

