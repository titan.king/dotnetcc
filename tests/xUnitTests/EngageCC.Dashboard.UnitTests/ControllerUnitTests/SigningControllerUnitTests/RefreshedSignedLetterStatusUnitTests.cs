﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.SigningControllerUnitTests
{
    public class RefreshedSignedLetterStatusUnitTests
    {
        private Mock<ISigningService> _signingServiceMock;
        private Mock<ILogger<SigningController>> _logger;
        private Mock<IConfiguration> _configurationMock;
        private Mock<ResourceManager> _resourceManagerMock;
        private Mock<IMapper> _mapper;
        private SigningController _controller;
        private readonly Fixture _fixture;

        public RefreshedSignedLetterStatusUnitTests()
        {
            _signingServiceMock = new Mock<ISigningService>();
            _logger = new Mock<ILogger<SigningController>>();
            _configurationMock = new Mock<IConfiguration>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _mapper = new Mock<IMapper>();
            _fixture = new Fixture();

            _controller = new SigningController(
                _signingServiceMock.Object,
                _configurationMock.Object,
                 _resourceManagerMock.Object,
                 _mapper.Object,
                _logger.Object
            );
        }

        [Fact]
        public async Task RefreshedSignedLetterStatus_ValidInput_ReturnsResponseModel()
        {
            // Arrange
            var expectedRefreshStatus = new ManualRefreshResModel
            {
                PdfUrl = "https://example.com/refreshed-letter.pdf" // Adjust this to match the expected behavior
            };

            _signingServiceMock.Setup(x => x.RefreshedSignedLetterStatus(It.IsAny<ManualRefreshResModel>()))
                .ReturnsAsync(expectedRefreshStatus);

            // Act
            var result = await _controller.RefreshedSignedLetterStatus(expectedRefreshStatus);

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.NotNull(responseModel.Data);
            Assert.True(responseModel.Status);
            Assert.Equal(HttpStatusCode.OK, responseModel.StatusCode);
        }

        [Fact]
        public async Task RefreshedSignedLetterStatus_ThrowsException_ReturnsInternalServerError()
        {
            // Arrange
            var expectedRefreshStatus = new ManualRefreshResModel
            {
                PdfUrl = "https://example.com/refreshed-letter.pdf" // Adjust this to match the expected behavior
            };

            _signingServiceMock.Setup(x => x.RefreshedSignedLetterStatus(It.IsAny<ManualRefreshResModel>()))
                .Throws(new Exception("Simulated exception"));

            var result = await _controller.RefreshedSignedLetterStatus(expectedRefreshStatus);

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.False(responseModel.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, responseModel.StatusCode);
            Assert.Equal("Simulated exception", responseModel.ErrorMessage);
        }
    }
}
