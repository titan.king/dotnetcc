﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.EngagementLetter;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.EngagementLetterControllerUnitTests
{
    public class DeleteEngagementLetterUnitTests
    {
        private readonly IFixture _fixture;
        private readonly Mock<IEngagementLetterService> _engagementLetterServiceMock;
        private readonly Mock<ILogger<EngagementLetterController>> _loggerMock;
        private readonly Mock<ResourceManager> _resourceManagerMock;
        private readonly Mock<IMapper> _mapperMock;
        private EngagementLetterController _controller;
        private Mock<IConfiguration> _configurationMock;
        public DeleteEngagementLetterUnitTests()
        {
            _fixture = new Fixture();
            _engagementLetterServiceMock = new Mock<IEngagementLetterService>();
            _loggerMock = new Mock<ILogger<EngagementLetterController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _mapperMock = new Mock<IMapper>();
            _configurationMock = new Mock<IConfiguration>();

            _controller = new EngagementLetterController(
               _engagementLetterServiceMock.Object,
               _loggerMock.Object,
               _resourceManagerMock.Object,
               _mapperMock.Object,
               _configurationMock.Object
           );
        }

        [Fact]
        public async Task DeleteEngagementLetter_ValidData_Returns_Ok()
        {
            // Arrange
            var deleteEngagementLetterViewModel = _fixture.Create<DeleteEngagementLetterViewModel>();


            var deleteEngagementLetterResModel = _mapperMock.Object.Map<DeleteEngagementLetterResModel>(deleteEngagementLetterViewModel);

            _engagementLetterServiceMock.Setup(service => service.DeleteEngagementLetter(deleteEngagementLetterResModel))
                                      .ReturnsAsync(true);

            // Act
            var result = await _controller.DeleteEngagementLetter(deleteEngagementLetterViewModel);

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.True(response.Status);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task DeleteEngagementLetter_Null_Data_Returns_NotAcceptable()
        {
            // Arrange
            DeleteEngagementLetterViewModel deleteEngagementLetterViewModel = null;

            // Act
            var result = await _controller.DeleteEngagementLetter(deleteEngagementLetterViewModel);

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.False(response.Status);
            Assert.Equal(HttpStatusCode.NotAcceptable, response.StatusCode);
        }

        [Fact]
        public async Task DeleteEngagementLetter_Exception_Returns_Internal_ServerError()
        {
            // Arrange
            var deleteEngagementLetterViewModel = _fixture.Create<DeleteEngagementLetterViewModel>();

            var deleteEngagementLetterResModel = _mapperMock.Object.Map<DeleteEngagementLetterResModel>(deleteEngagementLetterViewModel);

            _engagementLetterServiceMock.Setup(service => service.DeleteEngagementLetter(deleteEngagementLetterResModel))
                                      .ThrowsAsync(new Exception("Simulated error"));

            // Act
            var result = await _controller.DeleteEngagementLetter(deleteEngagementLetterViewModel);

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.False(response.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, response.StatusCode);
        }
    }
}
