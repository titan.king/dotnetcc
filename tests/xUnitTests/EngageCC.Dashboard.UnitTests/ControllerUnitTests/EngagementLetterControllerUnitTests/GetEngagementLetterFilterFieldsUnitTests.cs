﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Business.Services;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.EngagementLetter;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.EngagementLetterControllerUnitTests
{
    public class GetEngagementLetterFilterFieldsUnitTests
    {
        private readonly IFixture _fixture;
        private readonly Mock<IEngagementLetterService> _engagementLetterServiceMock;
        private readonly Mock<ILogger<EngagementLetterController>> _loggerMock;
        private readonly Mock<ResourceManager> _resourceManagerMock;
        private readonly Mock<IMapper> _mapperMock;
        private EngagementLetterController _controller;
        private Mock<IConfiguration> _configurationMock;
        public GetEngagementLetterFilterFieldsUnitTests()
        {
            _fixture = new Fixture();
            _engagementLetterServiceMock = new Mock<IEngagementLetterService>();
            _loggerMock = new Mock<ILogger<EngagementLetterController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _mapperMock = new Mock<IMapper>();
            _configurationMock = new Mock<IConfiguration>();

            _controller = new EngagementLetterController(
               _engagementLetterServiceMock.Object,
               _loggerMock.Object,
               _resourceManagerMock.Object,
               _mapperMock.Object,
               _configurationMock.Object
           );
        }

        [Fact]
        public async Task GetEngagementLetterFilterFields_Valid_Data_Returns_FilterFields()
        {
            // Arrange
            var expectedFilterFields = _fixture.Create<EngagementLetterSearchFilterViewModel>();
            expectedFilterFields.Departments = _fixture.CreateMany<DepartmentsViewModel>().ToList();
            expectedFilterFields.EngagementLetterIds = _fixture.CreateMany<EngagementLetterIdsViewModel>().ToList();
            _engagementLetterServiceMock.Setup(service => service.GetEngagementLetterFilterFields()).ReturnsAsync(expectedFilterFields);

            // Act
            var result = await _controller.GetEngagementLetterFilterFields();

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.NotNull(responseModel.Data);
            Assert.Equal(expectedFilterFields, responseModel.Data);
        }

        [Fact]
        public async Task GetEngagementLetterFilterFields_Invalid_EngagementLetters_Returns_Empty_FilterFields()
        {
            // Arrange
            var emptyFilterFields = new EngagementLetterSearchFilterViewModel
            {
                EngagementLetterIds = new List<EngagementLetterIdsViewModel>(),
                // Other properties initialization...
            };

            _engagementLetterServiceMock
                .Setup(repo => repo.GetEngagementLetterFilterFields())
                .Returns(Task.FromResult(emptyFilterFields));
            // Act
            var result = await _controller.GetEngagementLetterFilterFields();

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.NotNull(responseModel.Data);
            Assert.Empty(((EngagementLetterSearchFilterViewModel)responseModel.Data).EngagementLetterIds);
        }

        [Fact]
        public async Task GetEngagementLetterFilterFields_ExceptionThrown_Returns_Internal_ServerError()
        {
            // Arrange
            _engagementLetterServiceMock.Setup(repo => repo.GetEngagementLetterFilterFields()).ThrowsAsync(new Exception("Simulated error"));

            // Act
            var result = await _controller.GetEngagementLetterFilterFields();

            // Assert
            Assert.NotNull(result);
            Assert.False(result.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, result.StatusCode);
        }



    }
}
