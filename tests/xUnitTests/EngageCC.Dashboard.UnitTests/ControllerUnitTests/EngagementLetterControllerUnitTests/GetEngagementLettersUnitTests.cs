﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.EngagementLetter;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.EngagementLetterControllerUnitTests
{
    public class GetEngagementLettersUnitTests
    {

        private readonly IFixture _fixture;
        private readonly Mock<IEngagementLetterService> _engagementLetterServiceMock;
        private readonly Mock<ILogger<EngagementLetterController>> _loggerMock;
        private readonly Mock<ResourceManager> _resourceManagerMock;
        private readonly Mock<IMapper> _mapperMock;
        private EngagementLetterController _controller;
        private Mock<IConfiguration> _configurationMock;
        public GetEngagementLettersUnitTests()
        {
            _fixture = new Fixture();
            _engagementLetterServiceMock = new Mock<IEngagementLetterService>();
            _loggerMock = new Mock<ILogger<EngagementLetterController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _mapperMock = new Mock<IMapper>();
            _configurationMock = new Mock<IConfiguration>();

            _controller = new EngagementLetterController(
               _engagementLetterServiceMock.Object,
               _loggerMock.Object,
               _resourceManagerMock.Object,
               _mapperMock.Object,
                _configurationMock.Object
           );
        }

        [Fact]
        public async Task GetEngagementLetters_Valid_Input_Returns_Ok()
        {
            // Arrange
            var engagementLetterList = _fixture.CreateMany<EngagementLetterList>(3);

            _engagementLetterServiceMock.Setup(service => service.GetEngagementLetters())
                                        .ReturnsAsync(engagementLetterList);

            var mappedFieldsList = engagementLetterList
                .Select(item => new GetEngagementLetterListViewModel { } )
                .ToList();
            _mapperMock.Setup(mapper => mapper.Map<List<GetEngagementLetterListViewModel>>(engagementLetterList))
                       .Returns(mappedFieldsList);

            // Act
            var result = await _controller.GetEngagementLetters();

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.True(response.Status);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task GetEngagementLetters_Exception_Returns_Internal_ServerError()
        {
            // Arrange
            _engagementLetterServiceMock.Setup(service => service.GetEngagementLetters())
                                      .ThrowsAsync(new Exception("Simulated error"));
            _resourceManagerMock.Setup(rm => rm.GetString("ErrorMessage")).Returns("Simulated error");
            // Act
            var result = await _controller.GetEngagementLetters();

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.False(response.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, response.StatusCode);
        }

    }
}
