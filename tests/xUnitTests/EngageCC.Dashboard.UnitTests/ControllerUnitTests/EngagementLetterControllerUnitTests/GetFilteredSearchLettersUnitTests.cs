﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Data.DataModel;
using Dashboard.Models.CommonModel;
using Dashboard.Models.EngagementLetter;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.EngagementLetterControllerUnitTests
{
    public class GetFilteredSearchLettersUnitTests
    {
        private readonly IFixture _fixture;
        private readonly Mock<IEngagementLetterService> _engagementLetterServiceMock;
        private readonly Mock<ILogger<EngagementLetterController>> _loggerMock;
        private readonly Mock<ResourceManager> _resourceManagerMock;
        private readonly Mock<IMapper> _mapperMock;
        private EngagementLetterController _controller;
        private Mock<IConfiguration> _configurationMock;
        public GetFilteredSearchLettersUnitTests()
        {
            _fixture = new Fixture();
            _engagementLetterServiceMock = new Mock<IEngagementLetterService>();
            _loggerMock = new Mock<ILogger<EngagementLetterController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _mapperMock = new Mock<IMapper>();
            _configurationMock = new Mock<IConfiguration>();

            _controller = new EngagementLetterController(
               _engagementLetterServiceMock.Object,
               _loggerMock.Object,
               _resourceManagerMock.Object,
               _mapperMock.Object,
               _configurationMock.Object
           );
        }

        [Fact]
        public async Task GetFilteredSearchLetters_Valid_Input_Returns_Ok()
        {
            // Arrange
            var engagementLetterListViewModel = new EngagementLetterListViewModel(); 
            var engagementLetterSearchList = new EngagementLetterSearchList();
            _mapperMock.Setup(mapper => mapper.Map<EngagementLetterSearchList>(It.IsAny<EngagementLetterListViewModel>()))
                       .Returns(engagementLetterSearchList);
            var expectedResult = new List<EngagementLetterListViewModel>();

            _engagementLetterServiceMock.Setup(service => service.GetFilteredSearchLetters(engagementLetterSearchList))
                                       .ReturnsAsync(expectedResult);

            // Act
            var result = await _controller.GetFilteredSearchLetters(engagementLetterListViewModel);

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.True(response.Status);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Same(expectedResult, response.Data);
        }

        [Fact]
        public async Task GetFilteredSearchLetters_ExceptionThrown_Returns_Internal_ServerError()
        {
            // Arrange
            var engagementLetterListViewModel = new EngagementLetterListViewModel(); 
            var engagementLetterSearchList = new EngagementLetterSearchList();
            _mapperMock.Setup(mapper => mapper.Map<EngagementLetterSearchList>(It.IsAny<EngagementLetterListViewModel>()))
                       .Returns(engagementLetterSearchList);
            var errorMessage = "Simulated error message.";
            _engagementLetterServiceMock.Setup(service => service.GetFilteredSearchLetters(engagementLetterSearchList))
                                       .ThrowsAsync(new Exception(errorMessage));
            _resourceManagerMock.Setup(rm => rm.GetString("ErrorMessage")).Returns("Simulated error message.");

            // Act
            var result = await _controller.GetFilteredSearchLetters(engagementLetterListViewModel);

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.False(response.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, response.StatusCode);
            Assert.Null(response.Data);
        }

  
    }
}
