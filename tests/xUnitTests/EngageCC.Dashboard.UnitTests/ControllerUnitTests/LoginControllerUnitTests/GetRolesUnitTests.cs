﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Login;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.LoginControllerUnitTests
{
    public class GetRolesUnitTests
    {
        private Mock<ILoginService> _loginServiceMock;
        private Mock<IConfiguration> _configurationMock;
        private Mock<ILogger<LoginController>> _loggerMock;
        private Mock<ResourceManager> _resourceManagerMock;
        private Mock<IMapper> _mapperMock;
        private LoginController _controller;
        private readonly Fixture _fixture;

        public GetRolesUnitTests()
        {
            _loginServiceMock = new Mock<ILoginService>();
            _loggerMock = new Mock<ILogger<LoginController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _configurationMock = new Mock<IConfiguration>();
            _mapperMock = new Mock<IMapper>();
            _fixture = new Fixture();

            var expectedTenantId = "mockedTenantId";

            var azureAdSection = new ConfigurationBuilder()
                .AddInMemoryCollection(new[]
                {
            new KeyValuePair<string, string>("TenantId", expectedTenantId)
                })
                .Build();

            _configurationMock.Setup(x => x.GetSection("AzureAd")).Returns(azureAdSection.GetSection("TenantId"));

            _controller = new LoginController(
                _loginServiceMock.Object,
                _loggerMock.Object,
                _resourceManagerMock.Object,
                _mapperMock.Object,
                _configurationMock.Object
            );
        }

        [Fact]
        public async Task GetRoles_ValidInput_Returns_Ok()
        {
            // Arrange
            var loginViewModel = _fixture.Build<LoginViewModel>().Create();
            var expectedResult = new LoginViewModel();

            _loginServiceMock.Setup(service => service.GetRoles(It.IsAny<LoginViewModel>()))
                .ReturnsAsync(expectedResult);

            // Act
            var result = await _controller.GetRoles(loginViewModel);

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.True(responseModel.Status);
            Assert.Equal(HttpStatusCode.OK, responseModel.StatusCode);
        }

        [Fact]
        public async Task GetRoles_InvalidInput_Returns_Error()
        {
            // Arrange
            LoginViewModel permissions = null;
            var expectedErrorMessage = "Invalid input provided";

            _loginServiceMock.Setup(service => service.GetRoles(null))
                .ThrowsAsync(new Exception(expectedErrorMessage));

            // Act
            var result = await _controller.GetRoles(permissions);

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.False(responseModel.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, responseModel.StatusCode);
            Assert.Equal(expectedErrorMessage, responseModel.ErrorMessage);
        }

        [Fact]
        public async Task GetRoles_Exception_Returns_Internal_Server_Error()
        {
            // Arrange
            var loginViewModel = _fixture.Build<LoginViewModel>().Create();
            var exceptionMessage = "Simulated exception message";

            _loginServiceMock.Setup(service => service.GetRoles(It.IsAny<LoginViewModel>()))
                .ThrowsAsync(new Exception(exceptionMessage));

            // Act
            var result = await _controller.GetRoles(loginViewModel);

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.False(responseModel.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, responseModel.StatusCode);
        }
    }
}
