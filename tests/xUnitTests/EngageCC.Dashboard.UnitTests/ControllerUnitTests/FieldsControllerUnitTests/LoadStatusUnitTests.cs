﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.FieldsControllerUnitTests
{
    public class LoadStatusUnitTests
    {

        private readonly IFixture _fixture;
        private Mock<IFieldsService> _fieldsServiceMock;
        private Mock<ILogger<FieldsController>> _loggerMock;
        private Mock<ResourceManager> _resourceManagerMock = new Mock<ResourceManager>();
        private Mock<IMapper> _mapperMock = new Mock<IMapper>();
        private FieldsController _controller;
        public List<FieldListModel> FieldsListById { get; set; }
        public LoadStatusUnitTests()
        {
            _fixture = new Fixture();
            _fieldsServiceMock = new Mock<IFieldsService>();
            _loggerMock = new Mock<ILogger<FieldsController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _mapperMock = new Mock<IMapper>();
            _controller = new FieldsController(_fieldsServiceMock.Object, _resourceManagerMock.Object, _loggerMock.Object, _mapperMock.Object);
        }

        [Fact]
        public async Task LoadStatus_Returns_Response_Model_With_Fields_Status()
        {
            // Arrange

            var expectedFieldsStatus = _fixture.Create<FieldsStatusResModel>(); 
            var expectedResponseModel = new ResponseModel
            {
                Data = expectedFieldsStatus,
                Status = true,
                StatusCode = HttpStatusCode.OK
            };

            _fieldsServiceMock.Setup(service => service.LoadStatus()).ReturnsAsync(expectedFieldsStatus);

            // Act
            var result = await _controller.LoadStatus();

            // Assert
            Assert.NotNull(result);
            Assert.Equal(expectedResponseModel.Status, result.Status);
            Assert.Equal(expectedResponseModel.StatusCode, result.StatusCode);

        }

        [Fact]
        public async Task LoadStatus_Returns_Internal_ServerError_On_Exception()
        {
            // Arrange
            _fieldsServiceMock.Setup(service => service.LoadStatus()).ThrowsAsync(new Exception("Simulated exception"));
            _resourceManagerMock.Setup(rm => rm.GetString("ErrorMessage")).Returns("Simulated exception");
            // Act
            var result = await _controller.LoadStatus();

            // Assert
            Assert.NotNull(result);
            Assert.False(result.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, result.StatusCode);
        }

        [Fact]
        public async Task LoadStatus_Returns_Null_Response_Model_On_Service_Returning_Null()
        {
            // Arrange
            _fieldsServiceMock.Setup(service => service.LoadStatus()).Returns(Task.FromResult<FieldsStatusResModel>(null));
            // Act
            var result = await _controller.LoadStatus();

            // Assert
            Assert.Null(result.Data);
            Assert.False(result.Status);
        }

    }
}
