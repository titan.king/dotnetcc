﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.FieldsControllerUnitTests
{
    public class AddFieldUnitTests
    {
        private readonly IFixture _fixture;
        private Mock<IFieldsService> _fieldsServiceMock;
        private Mock<ILogger<FieldsController>> _loggerMock;
        private Mock<ResourceManager> _resourceManagerMock = new Mock<ResourceManager>();
        private Mock<IMapper> _mapperMock = new Mock<IMapper>();
        private FieldsController _controller;

        public AddFieldUnitTests()
        {
            _fixture = new Fixture();
            _fieldsServiceMock = new Mock<IFieldsService>();
            _loggerMock = new Mock<ILogger<FieldsController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _mapperMock = new Mock<IMapper>();

            _controller = new FieldsController(_fieldsServiceMock.Object, _resourceManagerMock.Object, _loggerMock.Object, _mapperMock.Object);
        }


        [Fact]
        public async Task AddField_When_ValidField_Returns_OK_Response_WithData()
        {
            // Arrange
            var fieldViewModel =  _fixture.Create<FieldViewModel>();
            var fieldListModel = new FieldListModel();
            var expectedFieldListModel = new FieldListModel();
            expectedFieldListModel = fieldListModel;
            _mapperMock.Setup(m => m.Map<FieldListModel>(fieldViewModel)).Returns(fieldListModel);
            _fieldsServiceMock.Setup(s => s.AddField(fieldListModel))
                              .ReturnsAsync(Tuple.Create(expectedFieldListModel, string.Empty));

            // Act
            var result = await _controller.AddField(fieldViewModel);

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.True(responseModel.Status);
            Assert.Equal(HttpStatusCode.OK, responseModel.StatusCode);
        }

        [Fact]
        public async Task AddField_When_InvalidField_Returns_NoContent()
        {
            // Arrange
            var invalidFieldViewModel = new FieldViewModel();
            _controller.ModelState.AddModelError("someKey", "someErrorMessage");

            // Act
            var result = await _controller.AddField(invalidFieldViewModel);

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.False(responseModel.Status);
            Assert.Equal(HttpStatusCode.NoContent, responseModel.StatusCode);
        }

        [Fact]
        public async Task AddField_When_Error_From_Service_Returns_Ambiguous()
        {
            // Arrange
            var fieldViewModel = _fixture.Create<FieldViewModel>();
            var fieldListModel = _fixture.Create<FieldListModel>();

            _mapperMock.Setup(m => m.Map<FieldListModel>(fieldViewModel)).Returns(fieldListModel);
            _fieldsServiceMock.Setup(s => s.AddField(fieldListModel))
                              .ReturnsAsync(Tuple.Create((FieldListModel)null, "Some error message"));

            // Act
            var result = await _controller.AddField(fieldViewModel);

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.False(responseModel.Status);
            Assert.Equal(HttpStatusCode.Ambiguous, responseModel.StatusCode);
            Assert.Equal("Some error message", responseModel.ErrorMessage);
        }
    }
}
