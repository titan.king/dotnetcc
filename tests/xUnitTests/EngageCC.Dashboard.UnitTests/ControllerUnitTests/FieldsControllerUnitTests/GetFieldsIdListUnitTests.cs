﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.FieldsControllerUnitTests
{
    public class GetFieldsIdListUnitTests
    {

        private readonly IFixture _fixture;
        private Mock<IFieldsService> _fieldServiceMock;
        private Mock<ILogger<FieldsController>> _loggerMock;
        private Mock<ResourceManager> _resourceManagerMock = new Mock<ResourceManager>();
        private Mock<IMapper> _mapperMock = new Mock<IMapper>();
        private FieldsController _controller;

        public GetFieldsIdListUnitTests()
        {
            _fixture = new Fixture();
            _fieldServiceMock = new Mock<IFieldsService>();
            _loggerMock = new Mock<ILogger<FieldsController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _mapperMock = new Mock<IMapper>();

            _controller = new FieldsController(_fieldServiceMock.Object, _resourceManagerMock.Object, _loggerMock.Object, _mapperMock.Object);
        }

        [Fact]
        public async Task GetFieldsIdList_WhenCalled_ReturnsOkResponseWithData()
        {
            // Arrange
            var expectedFieldsIdList = _fixture.CreateMany<FieldsIdListResModel>(3).ToList(); 

            _fieldServiceMock.Setup(x => x.GetFieldsIdList()).ReturnsAsync(expectedFieldsIdList);

            var expectedFieldsListViewModel = _fixture.CreateMany<FieldsIdListViewModel>(3).ToList();
            _mapperMock.Setup(mapper => mapper.Map<List<FieldsIdListViewModel>>(expectedFieldsIdList))
                       .Returns(expectedFieldsListViewModel);

            // Act
            var result = await _controller.GetFieldsIdList();

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.True(responseModel.Status);
            Assert.Equal(HttpStatusCode.OK, responseModel.StatusCode);
            Assert.Equal(expectedFieldsListViewModel, responseModel.Data);
        }

        [Fact]
        public async Task GetFieldsIdList_When_ExceptionThrown_Returns_Internal_ServerError()
        {
            // Arrange
            _fieldServiceMock.Setup(x => x.GetFieldsIdList()).ThrowsAsync(new Exception("Test exception message"));
            _resourceManagerMock.Setup(rm => rm.GetString("ErrorMessage")).Returns("Test exception message");

            // Act
            var result = await _controller.GetFieldsIdList();

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.False(responseModel.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, responseModel.StatusCode);
        }

    }
}
