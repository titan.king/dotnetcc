﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.FieldsControllerUnitTests
{
    public class GetColumnNamesUnitTests
    {
        private readonly IFixture _fixture;
        private Mock<IFieldsService> _fieldsServiceMock;
        private Mock<ILogger<FieldsController>> _loggerMock;
        private Mock<ResourceManager> _resourceManagerMock = new Mock<ResourceManager>();
        private Mock<IMapper> _mapperMock = new Mock<IMapper>();
        private FieldsController _controller;
        public List<FieldListModel> FieldsListById { get; set; }
        public GetColumnNamesUnitTests()
        {
            _fixture = new Fixture();
            _fieldsServiceMock = new Mock<IFieldsService>();
            _loggerMock = new Mock<ILogger<FieldsController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _mapperMock = new Mock<IMapper>();
            _controller = new FieldsController(_fieldsServiceMock.Object, _resourceManagerMock.Object, _loggerMock.Object, _mapperMock.Object);
        }

        [Fact]
        public async Task GetColumnNames_Returns_Successful_ResponseModel()
        {
            // Arrange
            var columnNamesResModels = new List<ColumnNamesResModel>
            {
                new ColumnNamesResModel { ColumnNames = "Column1" },
                new ColumnNamesResModel { ColumnNames = "Column2" },
            };
            _fieldsServiceMock.Setup(service => service.GetColumnNames()).ReturnsAsync(columnNamesResModels);

            // Act
            var result = await _controller.GetColumnNames();

            // Assert
            Assert.True(result.Status);
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
        }

        [Fact]
        public async Task GetColumnNames_Returns_NotFound_ResponseModel()
        {
            // Arrange
            _fieldsServiceMock.Setup(service => service.GetColumnNames()).Returns(Task.FromResult<IEnumerable<ColumnNamesResModel>>(null));
            // Act
            var result = await _controller.GetColumnNames();

            // Assert
            Assert.NotNull(result);
            Assert.True(result.Status);
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
        }

        [Fact]
        public async Task GetColumnNames_Returns_InternalServerError_On_Exception()
        {
            // Arrange
            _fieldsServiceMock.Setup(service => service.GetColumnNames()).ThrowsAsync(new Exception("Simulated exception"));
            // Act
            var result = await _controller.GetColumnNames();

            // Assert
            Assert.NotNull(result);
            Assert.False(result.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, result.StatusCode);
        }
    }
}
