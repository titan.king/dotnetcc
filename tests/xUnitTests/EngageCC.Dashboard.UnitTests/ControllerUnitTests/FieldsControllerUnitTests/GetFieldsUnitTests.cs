﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.FieldsControllerUnitTests
{
    public class GetFieldsUnitTests
    {

        private readonly IFixture _fixture;
        private Mock<IFieldsService> _fieldServiceMock;
        private Mock<ILogger<FieldsController>> _loggerMock;
        private Mock<ResourceManager> _resourceManagerMock = new Mock<ResourceManager>();
        private Mock<IMapper> _mapperMock = new Mock<IMapper>();
        private FieldsController _controller;

        public GetFieldsUnitTests()
        {
            _fixture = new Fixture();
            _fieldServiceMock = new Mock<IFieldsService>();
            _loggerMock = new Mock<ILogger<FieldsController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _mapperMock = new Mock<IMapper>();

            _controller = new FieldsController(_fieldServiceMock.Object, _resourceManagerMock.Object, _loggerMock.Object, _mapperMock.Object);
        }

        [Fact]
        public async Task GetFields_When_Data_Exists_Returns_Ok_Response_WithData()
        {
            // Arrange
            var expectedFieldListRes = _fixture.CreateMany<FieldListModel>(3).ToList();

            _fieldServiceMock.Setup(x => x.GetFields()).ReturnsAsync(expectedFieldListRes);
            _mapperMock.Setup(mapper => mapper.Map<List<FieldViewModel>>(It.IsAny<List<FieldListModel>>()))
                       .Returns(new List<FieldViewModel>()); 

            // Act
            var result = await _controller.GetFields();

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.True(responseModel.Status);
            Assert.Equal(HttpStatusCode.OK, responseModel.StatusCode);
        }

        [Fact]
        public async Task GetFields_ExceptionThrown_Returns_InternalServerError()
        {
            // Arrange
            _fieldServiceMock.Setup(x => x.GetFields()).ThrowsAsync(new Exception("Test exception message"));
            _resourceManagerMock.Setup(rm => rm.GetString("ErrorMessage")).Returns("Test exception message");
            // Act
            var result = await _controller.GetFields();

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.False(responseModel.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, responseModel.StatusCode);
        }

        [Fact]
        public async Task GetFields_When_No_Data_Exists_Returns_EmptyList()
        {
            // Arrange
            var expectedFieldListRes = new List<FieldListModel>();

            _fieldServiceMock.Setup(x => x.GetFields()).ReturnsAsync(expectedFieldListRes);
            _mapperMock.Setup(mapper => mapper.Map<List<FieldViewModel>>(It.IsAny<List<FieldListModel>>()))
                       .Returns(new List<FieldViewModel>());

            // Act
            var result = await _controller.GetFields();

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            var responseData = Assert.IsType<List<FieldViewModel>>(responseModel.Data);
            Assert.Empty(responseData);
        }

    }
}
