﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.FieldsControllerUnitTests
{
    public class GetActiveClientFieldsUnitTests
    {

        private readonly IFixture _fixture;
        private Mock<IFieldsService> _fieldsServiceMock;
        private Mock<ILogger<FieldsController>> _loggerMock;
        private Mock<ResourceManager> _resourceManagerMock = new Mock<ResourceManager>();
        private Mock<IMapper> _mapperMock = new Mock<IMapper>();
        private FieldsController _controller;
        public List<FieldListModel> FieldsListById { get; set; }
        public GetActiveClientFieldsUnitTests()
        {
            _fixture = new Fixture();
            _fieldsServiceMock = new Mock<IFieldsService>();
            _loggerMock = new Mock<ILogger<FieldsController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _mapperMock = new Mock<IMapper>();
            _controller = new FieldsController(_fieldsServiceMock.Object, _resourceManagerMock.Object, _loggerMock.Object, _mapperMock.Object);
        }

        [Fact]
        public async Task GetActiveClientFields_Returns_Successful_ResponseModel()
        {
            // Arrange
            int clientId = 123;
            int partnerId = 456;

            var fieldsResponse = new FieldsResponse();

            _fieldsServiceMock.Setup(service => service.GetActiveClientFields(clientId, partnerId)).ReturnsAsync(fieldsResponse);

            // Act
            var result = await _controller.GetActiveClientFields(clientId, partnerId);

            // Assert
            Assert.NotNull(result);
            Assert.True(result.Status);
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
            Assert.NotNull(result.Data);
            //Assert.Equal(fieldsResponse, result.Data);
        }

        [Fact]
        public async Task GetActiveClientFields_Returns_NoContent_When_ClientId_Is_Zero()
        {
            // Arrange
            int clientId = 0; 
            // Act
            var result = await _controller.GetActiveClientFields(clientId, It.IsAny<int>());

            // Assert
            Assert.NotNull(result);
            Assert.False(result.Status);
            Assert.Equal(HttpStatusCode.NoContent, result.StatusCode);
            Assert.Equal("Client id is empty", result.ErrorMessage);
        }

        [Fact]
        public async Task GetActiveClientFields_Returns_Internal_ServerError_On_Exception()
        {
            // Arrange
            int clientId = 123;
            int partnerId = 456;

            _fieldsServiceMock.Setup(service => service.GetActiveClientFields(clientId, partnerId)).ThrowsAsync(new Exception("Simulated exception"));

            // Act
            var result = await _controller.GetActiveClientFields(clientId, partnerId);

            // Assert
            Assert.NotNull(result);
            Assert.False(result.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, result.StatusCode);
        }
    }
}
