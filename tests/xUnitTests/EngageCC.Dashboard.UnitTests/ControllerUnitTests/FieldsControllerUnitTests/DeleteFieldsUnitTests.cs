﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.FieldsControllerUnitTests
{
    public class DeleteFieldsUnitTests
    {
        private readonly IFixture _fixture;
        private Mock<IFieldsService> _fieldsServiceMock;
        private Mock<ILogger<FieldsController>> _loggerMock;
        private Mock<ResourceManager> _resourceManagerMock = new Mock<ResourceManager>();
        private Mock<IMapper> _mapperMock = new Mock<IMapper>();
        private FieldsController _controller;
        public List<FieldListModel> FieldsListById { get; set; }
        public DeleteFieldsUnitTests()
        {
            _fixture = new Fixture();
            _fieldsServiceMock = new Mock<IFieldsService>();
            _loggerMock = new Mock<ILogger<FieldsController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _mapperMock = new Mock<IMapper>();
            _controller = new FieldsController(_fieldsServiceMock.Object, _resourceManagerMock.Object, _loggerMock.Object, _mapperMock.Object);
        }

        [Fact]
        public async Task DeleteFields_Returns_Successful_ResponseModel()
        {
            // Arrange
            var deleteFieldsViewModel = new DeleteFieldsViewModel { FieldId = new[] { 1, 2, 3 } }; 
            var fieldsServiceMock = new Mock<IFieldsService>();
            _fieldsServiceMock.Setup(service => service.DeleteFields(deleteFieldsViewModel)).ReturnsAsync(new DeleteFieldsViewModel { IsDeleted = true });
            // Act
            var result = await _controller.DeleteFields(deleteFieldsViewModel);

            // Assert
            Assert.NotNull(result);
            Assert.True(result.Status);
            Assert.Equal(HttpStatusCode.OK, result.StatusCode);
        }

        [Fact]
        public async Task DeleteFields_Returns_NotAcceptable_ResponseModel()
        {
            // Arrange
            var deleteFieldsViewModel = new DeleteFieldsViewModel { FieldId = new[] { 4, 5, 6 } };

            var fieldsServiceMock = new Mock<IFieldsService>();
            _fieldsServiceMock.Setup(service => service.DeleteFields(deleteFieldsViewModel)).ReturnsAsync(new DeleteFieldsViewModel { IsDeleted = false });

            // Act
            var result = await _controller.DeleteFields(deleteFieldsViewModel);

            // Assert
            Assert.NotNull(result);
            Assert.False(result.Status);
            Assert.Equal(HttpStatusCode.NotAcceptable, result.StatusCode);
        }

        [Fact]
        public async Task DeleteFields_Returns_Internal_ServerError_On_Exception()
        {
            // Arrange
            var deleteFieldsViewModel = new DeleteFieldsViewModel { FieldId = new[] { 7, 8, 9 } }; 

            var fieldsServiceMock = new Mock<IFieldsService>();
            _fieldsServiceMock.Setup(service => service.DeleteFields(deleteFieldsViewModel)).ThrowsAsync(new Exception("Simulated exception"));
            
            // Act
            var result = await _controller.DeleteFields(deleteFieldsViewModel);

            // Assert
            Assert.NotNull(result);
            Assert.False(result.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, result.StatusCode);
        }
    }
}
