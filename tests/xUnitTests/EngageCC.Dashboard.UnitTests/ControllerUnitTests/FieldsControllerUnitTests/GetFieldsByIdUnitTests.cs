﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.FieldsControllerUnitTests
{
    public class GetFieldsByIdUnitTests
    {
        private readonly IFixture _fixture;
        private Mock<IFieldsService> _fieldsServiceMock;
        private Mock<ILogger<FieldsController>> _loggerMock;
        private Mock<ResourceManager> _resourceManagerMock = new Mock<ResourceManager>();
        private Mock<IMapper> _mapperMock = new Mock<IMapper>();
        private FieldsController _controller;
        public List<FieldListModel> FieldsListById { get; set; }
        public GetFieldsByIdUnitTests()
        {
            _fixture = new Fixture();
            _fieldsServiceMock = new Mock<IFieldsService>();
            _loggerMock = new Mock<ILogger<FieldsController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _mapperMock = new Mock<IMapper>();
            _controller = new FieldsController(_fieldsServiceMock.Object, _resourceManagerMock.Object, _loggerMock.Object, _mapperMock.Object);
        }


        [Fact]
        public async Task GetFieldsById_ValidInput_ReturnsOkResponse()
        {
            // Arrange
            var fieldId = _fixture.Create<int>();
            var viewModel = _fixture.Build<FieldsGetByIdViewModel>().With(f => f.FieldId, fieldId).Create();

            var fieldsServiceResponse = _fixture.Create<GetFieldsByIdList>();
            fieldsServiceResponse.FieldsListById = new List<FieldListModel>
        {
            new FieldListModel
            {
               FieldId = viewModel.FieldId,
                FieldName = viewModel.FieldName,
                DisplayName = viewModel.DisplayName,
                FieldDataTypeId = viewModel.FieldDataTypeId,
                FieldDataType = viewModel.FieldDataType,
                HintText = viewModel.HintText,
                ChangeNotes = viewModel.ChangeNotes,
                IsDeleted = viewModel.IsDeleted,
                StatusId = viewModel.StatusId,
                StatusName = viewModel.StatusName,
                CreatedOn = viewModel.CreatedOn,
                ModifiedOn = viewModel.ModifiedOn,
                TransactionStatus = viewModel.TransactionStatus,
                ConnectedBlocks = viewModel.TransactionStatus,
                DirectInput = viewModel.DirectInput,
                MDDLookup = viewModel.MDDLookup,
                ColumnId = viewModel.ColumnId,
                MDDColumnNames = viewModel.MDDColumnNames,
                MDDColumnData = viewModel.MDDColumnData,
                ModifiedBy = viewModel.ModifiedBy,
                CreatedBy = viewModel.CreatedBy,
                InputType = viewModel.InputType
            }
        };
            _fieldsServiceMock.Setup(x => x.GetFieldsById(fieldId)).ReturnsAsync(fieldsServiceResponse);

            // Act
            var response = await _controller.GetFieldsById(viewModel);

            // Assert
            var okResult = Assert.IsType<ResponseModel>(response);
            Assert.True(okResult.Status);
            Assert.Equal(HttpStatusCode.OK, okResult.StatusCode);
        }

        [Fact]
        public async Task GetFieldsById_InvalidInput_Returns_Internal_ServerError()
        {
            // Arrange
            var invalidFieldId = 0;
            var viewModel = _fixture.Build<FieldsGetByIdViewModel>().With(f => f.FieldId, invalidFieldId).Create();

            _fieldsServiceMock.Setup(x => x.GetFieldsById(invalidFieldId))
                .ThrowsAsync(new Exception("Test exception message"));
            _resourceManagerMock.Setup(rm => rm.GetString("ErrorMessage")).Returns("Test exception message");
            // Act
            var response = await _controller.GetFieldsById(viewModel);

            // Assert
            Assert.NotNull(response);
            Assert.IsType<ResponseModel>(response); 
            Assert.Equal(HttpStatusCode.InternalServerError, response.StatusCode);
        }

        [Fact]
        public async Task GetFieldsById_Null_Input_Returns_No_Content()
        {
            // Arrange 
            var viewModel = new FieldsGetByIdViewModel { FieldId = 0 };
            _resourceManagerMock.Setup(rm => rm.GetString("ErrorMessage")).Returns("The Content field is required.");
            // Act
            var response = await _controller.GetFieldsById(viewModel);

            // Assert
            Assert.NotNull(response);
            Assert.IsType<ResponseModel>(response);
            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
        }
    }
}
