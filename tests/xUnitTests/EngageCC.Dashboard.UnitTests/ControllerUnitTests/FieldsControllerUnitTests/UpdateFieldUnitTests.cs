﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.FieldsControllerUnitTests
{
    public class UpdateFieldUnitTests
    {
        private readonly IFixture _fixture;
        private Mock<IFieldsService> _fieldsServiceMock;
        private Mock<ILogger<FieldsController>> _loggerMock;
        private Mock<ResourceManager> _resourceManagerMock = new Mock<ResourceManager>();
        private Mock<IMapper> _mapperMock = new Mock<IMapper>();
        private FieldsController _controller;

        public UpdateFieldUnitTests()
        {
            _fixture = new Fixture();
            _fieldsServiceMock = new Mock<IFieldsService>();
            _loggerMock = new Mock<ILogger<FieldsController>>();
            _resourceManagerMock = new Mock<ResourceManager>();
            _mapperMock = new Mock<IMapper>();

            _controller = new FieldsController(_fieldsServiceMock.Object, _resourceManagerMock.Object, _loggerMock.Object, _mapperMock.Object);
        }

        [Fact]
        public async Task UpdateField_When_ValidField_Returns_OK_Response_WithData()
        {
            // Arrange
            var fieldViewModel = _fixture.Create<FieldViewModel>();
            var fieldListModel = _fixture.Create<FieldListModel>();
            var expectedFieldListModel = _fixture.Create<FieldListModel>();

            _mapperMock.Setup(m => m.Map<FieldListModel>(It.IsAny<FieldViewModel>()))
                       .Returns(fieldListModel);

            _fieldsServiceMock.Setup(s => s.UpdateField(It.IsAny<FieldListModel>()))
                   .ReturnsAsync(() => Tuple.Create(expectedFieldListModel, string.Empty));

            // Act
            var result = await _controller.UpdateField(fieldViewModel);

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.True(responseModel.Status);
            Assert.Equal(HttpStatusCode.OK, responseModel.StatusCode);
        }

        [Fact]
        public async Task UpdateField_When_InvalidModel_Returns_NoContent()
        {
            // Arrange
            var fieldViewModel = new FieldViewModel(); 

            // Act
            var result = await _controller.UpdateField(fieldViewModel);

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.False(responseModel.Status);
            Assert.Equal(HttpStatusCode.NotAcceptable, responseModel.StatusCode);
        }
    }
}
