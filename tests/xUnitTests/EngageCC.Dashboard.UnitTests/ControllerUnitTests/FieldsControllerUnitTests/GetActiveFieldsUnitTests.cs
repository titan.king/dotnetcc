﻿using AutoFixture;
using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.Dashboard.UnitTests.ControllerUnitTests.FieldsControllerUnitTests
{
    public class GetActiveFieldsUnitTests
    {
        private readonly FieldsController _controller;
        private readonly Mock<IFieldsService> _fieldsServiceMock = new Mock<IFieldsService>();
        private readonly Mock<IMapper> _mapperMock = new Mock<IMapper>();
        private readonly Mock<ILogger<FieldsController>> _loggerMock = new Mock<ILogger<FieldsController>>();
        private readonly IFixture _fixture;
        private Mock<ResourceManager> _resourceManagerMock = new Mock<ResourceManager>();

        public GetActiveFieldsUnitTests()
        {
            _fixture = new Fixture();
            _resourceManagerMock = new Mock<ResourceManager>();
            _controller = new FieldsController(_fieldsServiceMock.Object, _resourceManagerMock.Object, _loggerMock.Object, _mapperMock.Object);
        }

        [Fact]
        public async Task GetActiveFields_WhenDataExists_ReturnsOkResponseWithData()
        {
            // Arrange
            var expectedActiveFieldListRes = _fixture.CreateMany<FieldListModel>(3).ToList();


            _fieldsServiceMock.Setup(x => x.GetActiveFields()).ReturnsAsync(expectedActiveFieldListRes);
            _mapperMock.Setup(mapper => mapper.Map<List<FieldViewModel>>(It.IsAny<List<FieldListModel>>()))
                       .Returns(new List<FieldViewModel>()); 

            // Act
            var result = await _controller.GetActiveFields();

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.True(responseModel.Status);
            Assert.Equal(HttpStatusCode.OK, responseModel.StatusCode);
        }

        [Fact]
        public async Task GetActiveFields_When_No_DataExists_Returns_EmptyList()
        {
            // Arrange
            var expectedActiveFieldListRes = new List<FieldListModel>(); 

            _fieldsServiceMock.Setup(x => x.GetActiveFields()).ReturnsAsync(expectedActiveFieldListRes);
            _mapperMock.Setup(mapper => mapper.Map<List<FieldViewModel>>(It.IsAny<List<FieldListModel>>()))
                       .Returns(new List<FieldViewModel>()); 

            // Act
            var result = await _controller.GetActiveFields();

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.True(responseModel.Status);
            Assert.Equal(HttpStatusCode.OK, responseModel.StatusCode);

            var responseData = Assert.IsType<List<FieldViewModel>>(responseModel.Data);
            Assert.Empty(responseData);
        }

        [Fact]
        public async Task GetActiveFields_When_ExceptionThrown_Returns_Internal_ServerError()
        {
            // Arrange
            _fieldsServiceMock.Setup(x => x.GetActiveFields()).ThrowsAsync(new Exception("Test exception message"));
            _resourceManagerMock.Setup(rm => rm.GetString("ErrorMessage")).Returns("Test exception message");
            // Act
            var result = await _controller.GetActiveFields();

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.False(responseModel.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, responseModel.StatusCode);
        }
    }

}
