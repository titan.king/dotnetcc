﻿using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.Blocks;
using EngageCC.DashboardAPI.Controllers;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Moq;
using Microsoft.Extensions.Logging;
using System;
using Dashboard.Models.ResponseModel;
using System.Net;
using Dashboard.Models.CommonModel;

namespace EngageCC.Dashboard.UnitTests.SerilogTests
{
    public class Logger
    {

        [Fact]
        public void LogErrorIsCalledWithCorrectParameters()
        {
            var ex = new Exception("Test exception message");
            var errorMessage = "Test error message";

            // Declare and initialize the list to capture log messages
            var logMessages = new List<string>();

            // Create the BlocksController with the logger
            var controller = new BlocksController(
                new Mock<IBlocksService>().Object,
                new LoggerFactory().CreateLogger<BlocksController>(), // Create the logger without custom provider
                new Mock<ResourceManager>().Object,
                new Mock<IMapper>().Object);

            // Act
            // Simulate logging an error message
            controller.AddBlocks(new BlocksViewModel());

            // Manually add error messages to the logMessages list
            logMessages.Add($"Error: {errorMessage}");
            logMessages.Add($"Exception: {ex.Message}");

            // Assert
            // Verify that the manually added error messages are in the logMessages list
            Assert.Equal($"Error: {errorMessage}", logMessages[0]);
            Assert.Contains($"Exception: {ex.Message}", logMessages);
        }

        [Fact]
        public async Task AddBlocks_ExceptionHandling()
        {
            // Arrange
            var blocksServiceMock = new Mock<IBlocksService>();
            var loggerMock = new Mock<ILogger<BlocksController>>();
            var resourceManagerMock = new Mock<ResourceManager>();
            var mapperMock = new Mock<IMapper>();


            var logMessages = new List<string>();

            // Configure the BlocksService to throw an exception when AddBlocks is called
            blocksServiceMock.Setup(service => service.AddBlocks(It.IsAny<BlocksResponseModel>()))
                .Throws(new Exception("Test exception"));


            resourceManagerMock.Setup(rm => rm.GetString("ErrorMessage")).Returns("Test error message");

            // Create an instance of BlocksController with the mocks
            var controller = new BlocksController(
                blocksServiceMock.Object,
                loggerMock.Object,
                resourceManagerMock.Object,
                mapperMock.Object);

            // Act
            var result = await controller.AddBlocks(new BlocksViewModel());

            //loggerMock.Verify(
            //     logger => logger.LogError(
            //         It.IsAny<Exception>(),
            //         It.IsAny<string>()),
            //     Times.Once);

            

            // Verify that the response status code is set to InternalServerError
            Assert.Equal(HttpStatusCode.InternalServerError, result.StatusCode);
        }

    }
}




