﻿using ClientData.Business.Services;
using ClientData.Data.Interfaces;
using ClientData.Models.CommonModel;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.ClientsData.UnitTests.Business.ServiceUnitTests
{
    public class ClientDataServiceUnitTests
    {
        [Fact]
        public async Task FieldsData_ShouldReturnFieldsData()
        {
            // Arrange
            var clientDataRepositoryMock = new Mock<IClientDataRepository>();
            var expectedFieldsData = new FieldsData();
            var clientDataService = new ClientDataService(clientDataRepositoryMock.Object);

            clientDataRepositoryMock.Setup(repo => repo.FieldsData()).ReturnsAsync(expectedFieldsData);

            // Act
            var result = await clientDataService.FieldsData();

            // Assert
            Assert.Equal(expectedFieldsData, result);
            clientDataRepositoryMock.Verify(repo => repo.FieldsData(), Times.Once);
        }

        [Fact]
        public async Task GetParentandChildData_ShouldReturnParentandChildData()
        {
            // Arrange
            var clientDataRepositoryMock = new Mock<IClientDataRepository>();
            var expectedParentandChildData = new ParentandChildData();
            var clientDataService = new ClientDataService(clientDataRepositoryMock.Object);

            clientDataRepositoryMock.Setup(repo => repo.GetParentandChildData()).ReturnsAsync(expectedParentandChildData);

            // Act
            var result = await clientDataService.GetParentandChildData();

            // Assert
            Assert.Equal(expectedParentandChildData, result);
            clientDataRepositoryMock.Verify(repo => repo.GetParentandChildData(), Times.Once);
        }

        [Fact]
        public async Task GetParentChildData_ShouldReturnParentChildData()
        {
            // Arrange
            var clientDataRepositoryMock = new Mock<IClientDataRepository>();
            var expectedParentChildData = new ParentChildData(); 
            var clientDataService = new ClientDataService(clientDataRepositoryMock.Object);

            clientDataRepositoryMock.Setup(repo => repo.GetParentChildData()).ReturnsAsync(expectedParentChildData);

            // Act
            var result = await clientDataService.GetParentChildData();

            // Assert
            Assert.Equal(expectedParentChildData, result);
            clientDataRepositoryMock.Verify(repo => repo.GetParentChildData(), Times.Once);
        }

        [Fact]
        public async Task GetPartnersList_ShouldReturnPartnersList()
        {
            // Arrange
            var clientDataRepositoryMock = new Mock<IClientDataRepository>();
            var expectedPartnersData = new PartnersData(); 
            var clientDataService = new ClientDataService(clientDataRepositoryMock.Object);

            clientDataRepositoryMock.Setup(repo => repo.GetPartnersList()).ReturnsAsync(expectedPartnersData);

            // Act
            var result = await clientDataService.GetPartnersList();

            // Assert
            Assert.Equal(expectedPartnersData, result);
            clientDataRepositoryMock.Verify(repo => repo.GetPartnersList(), Times.Once);
        }

        [Fact]
        public async Task GetPartnerandClientMappingData_ShouldReturnPartnerandClientMappingData()
        {
            // Arrange
           
            var clientDataRepositoryMock = new Mock<IClientDataRepository>();    
            var expectedPartnerandClientMappingData = new PartnerandClientMappingData(); 
            var clientDataService = new ClientDataService(clientDataRepositoryMock.Object);

            clientDataRepositoryMock.Setup(repo => repo.GetPartnerandClientMappingData()).ReturnsAsync(expectedPartnerandClientMappingData);

            // Act
            var result = await clientDataService.GetPartnerandClientMappingData();

            // Assert
            Assert.Equal(expectedPartnerandClientMappingData, result);
            clientDataRepositoryMock.Verify(repo => repo.GetPartnerandClientMappingData(), Times.Once);
        }

        [Fact]
        public async Task GetPartnerandofficeData_ShouldReturnPartnerandofficeData()
        {
            // Arrange
            var clientDataRepositoryMock = new Mock<IClientDataRepository>();
            var expectedPartnerandofficeData = new PartnerandofficeMapping();
            var clientDataService = new ClientDataService(clientDataRepositoryMock.Object);

            clientDataRepositoryMock.Setup(repo => repo.GetPartnerandofficeData()).ReturnsAsync(expectedPartnerandofficeData);

            // Act
            var result = await clientDataService.GetPartnerandofficeData();

            // Assert
            Assert.Equal(expectedPartnerandofficeData, result);
            clientDataRepositoryMock.Verify(repo => repo.GetPartnerandofficeData(), Times.Once);
        }

        [Fact]
        public async Task GetOfficeDetails_ShouldReturnOfficeDetails()
        {
            // Arrange
            var clientDataRepositoryMock = new Mock<IClientDataRepository>();
            var expectedOfficeDetails = new OfficeDetails(); 
            var clientDataService = new ClientDataService(clientDataRepositoryMock.Object);
      
            clientDataRepositoryMock.Setup(repo => repo.GetOfficeDetails()).ReturnsAsync(expectedOfficeDetails);

            // Act
            var result = await clientDataService.GetOfficeDetails();

            // Assert
            Assert.Equal(expectedOfficeDetails, result);
            clientDataRepositoryMock.Verify(repo => repo.GetOfficeDetails(), Times.Once);
        }

        [Fact]
        public async Task GetClientDetails_ShouldReturnClientDetails()
        {
            // Arrange
            var clientDataRepositoryMock = new Mock<IClientDataRepository>();
            var expectedClientDetails = new LoadClientDetails();         
            var clientDataService = new ClientDataService(clientDataRepositoryMock.Object);

            clientDataRepositoryMock.Setup(repo => repo.GetClientDetails()).ReturnsAsync(expectedClientDetails);

            // Act
            var result = await clientDataService.GetClientDetails();

            // Assert
            Assert.Equal(expectedClientDetails, result);
            clientDataRepositoryMock.Verify(repo => repo.GetClientDetails(), Times.Once);
        }
    }
}
