﻿using ClientData.Business.Interface;
using ClientData.Models.CommonModel;
using EngageCC.ClientDataAPI.Controllers;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.ClientsData.UnitTests.ControllerUnitTests.ClientDataSyncUnitTests
{
    public class GetOfficeDetailsUnitTests
    {
        private Mock<IClientDataService> _clientDataServiceMock;
        private ClientDataSync _controller;
        public GetOfficeDetailsUnitTests()
        {
            _clientDataServiceMock = new Mock<IClientDataService>();

            _controller = new ClientDataSync(
                _clientDataServiceMock.Object
            );
        }

        [Fact]
        public async Task GetOfficeDetails_ValidData_ReturnsOk()
        {
            // Arrange
            var expectedData = new OfficeDetails(); 
            _clientDataServiceMock.Setup(service => service.GetOfficeDetails())
                .ReturnsAsync(expectedData);

            // Act
            var result = await _controller.GetOfficeDetails();

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.True(response.Status);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Same(expectedData, response.Data);
        }

        [Fact]
        public async Task GetOfficeDetails_Exception_ReturnsInternalServerError()
        {
            // Arrange
            var errorMessage = "Simulated error message";
            _clientDataServiceMock.Setup(service => service.GetOfficeDetails())
                .ThrowsAsync(new Exception(errorMessage));

            // Act
            var result = await _controller.GetOfficeDetails();

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.False(responseModel.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, responseModel.StatusCode);
            Assert.Contains(errorMessage, responseModel.ErrorMessage);
        }
    }
}
