﻿using AutoMapper;
using ClientData.Business.Interface;
using ClientData.Models.CommonModel;
using EngageCC.ClientDataAPI.Controllers;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.ClientsData.UnitTests.ControllerUnitTests.ClientDataSyncUnitTests
{
    public class FieldsDataUnitTests
    {
        private Mock<IClientDataService> _clientDataServiceMock;
        private ClientDataSync _controller;
        public FieldsDataUnitTests() 
        {
           _clientDataServiceMock = new Mock<IClientDataService>();

            _controller = new ClientDataSync(
                _clientDataServiceMock.Object
            );
        }

        [Fact]
        public async Task FieldsData_ValidData_ReturnsOk()
        {
            // Arrange
            var expectedData = new FieldsData();

            _clientDataServiceMock.Setup(service => service.FieldsData())
                .ReturnsAsync(expectedData);

            // Act
            var result = await _controller.FieldsData();

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.True(response.Status);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Same(expectedData, response.Data);
        }

        [Fact]
        public async Task FieldsData_Exception_ReturnsInternalServerErrorResponse()
        {
            // Arrange
            var errorMessage = "Simulated error message";
            _clientDataServiceMock.Setup(service => service.FieldsData())
                .ThrowsAsync(new Exception(errorMessage));

            // Act
            var result = await _controller.FieldsData();

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.False(responseModel.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, responseModel.StatusCode);
            Assert.Contains(errorMessage, responseModel.ErrorMessage);
        }
    }
}
