﻿using ClientData.Business.Interface;
using ClientData.Models.CommonModel;
using EngageCC.ClientDataAPI.Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.ClientsData.UnitTests.ControllerUnitTests.ClientDataSyncUnitTests
{
    public class GetPartnersListUnitTests
    {
        private Mock<IClientDataService> _clientDataServiceMock;
        private ClientDataSync _controller;
        public GetPartnersListUnitTests() 
        {
            _clientDataServiceMock = new Mock<IClientDataService>();

            _controller = new ClientDataSync(
                _clientDataServiceMock.Object
            );
        }

        [Fact]
        public async Task GetPartnersList_ValidData_ReturnsOk()
        {
            // Arrange
            var expectedData = new PartnersData(); 
            _clientDataServiceMock.Setup(service => service.GetPartnersList())
                .ReturnsAsync(expectedData);

            // Act
            var result = await _controller.GetPartnersList();

            // Assert
            var response = Assert.IsType<ResponseModel>(result);
            Assert.True(response.Status);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Same(expectedData, response.Data);
        }

        [Fact]
        public async Task GetPartnersList_Exception_ReturnsInternalServerError()
        {
            // Arrange
            var errorMessage = "Simulated error message";
            _clientDataServiceMock.Setup(service => service.GetPartnersList())
                .ThrowsAsync(new Exception(errorMessage));

            // Act
            var result = await _controller.GetPartnersList();

            // Assert
            var responseModel = Assert.IsType<ResponseModel>(result);
            Assert.False(responseModel.Status);
            Assert.Equal(HttpStatusCode.InternalServerError, responseModel.StatusCode);
            Assert.Contains(errorMessage, responseModel.ErrorMessage);
        }
    }
}
