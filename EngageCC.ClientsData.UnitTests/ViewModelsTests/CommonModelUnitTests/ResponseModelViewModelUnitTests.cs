﻿using ClientData.Models.CommonModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.ClientsData.UnitTests.ViewModelsTests.CommonModelUnitTests
{
    public class ResponseModelViewModelUnitTests
    {
        [Fact]
        public void ResponseModel_Properties_CanBeSetAndRetrieved()
        {
            // Arrange
            var statusCode = HttpStatusCode.OK;
            var status = true;
            var data = new object();
            var errorMessage = "No errors";

            // Act
            var responseModel = new ResponseModel
            {
                StatusCode = statusCode,
                Status = status,
                Data = data,
                ErrorMessage = errorMessage
            };

            // Assert
            Assert.Equal(statusCode, responseModel.StatusCode);
            Assert.Equal(status, responseModel.Status);
            Assert.Same(data, responseModel.Data);
            Assert.Equal(errorMessage, responseModel.ErrorMessage);
        }

        [Fact]
        public void ResponseModel_DefaultConstructor_SetsProperties()
        {
            // Arrange and Act
            var responseModel = new ResponseModel();

            // Assert
            Assert.False(responseModel.Status);
            Assert.Null(responseModel.Data);
            Assert.Null(responseModel.ErrorMessage);
        }

        [Fact]
        public void ResponseModel_ConstructorWithStatusCodeAndErrorMessage_SetsProperties()
        {
            // Arrange
            var statusCode = HttpStatusCode.NotFound;
            var errorMessage = "Not found";

            // Act
            var responseModel = new ResponseModel(statusCode, errorMessage);

            // Assert
            Assert.Equal(statusCode, responseModel.StatusCode);
            Assert.Equal(errorMessage, responseModel.ErrorMessage);
        }
    }
}
