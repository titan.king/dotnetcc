﻿using ClientData.Models.CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.ClientsData.UnitTests.ViewModelsTests.CommonModelUnitTests
{
    public class ClientDetailsViewModelUnitTests
    {
        [Fact]
        public void ClientDetails_ValidData_PassesValidation()
        {
            //Arrange
            var model = new ClientDetails
            {
                FieldsData = new object(),
                ParentandChildData = new object(),
                PartnerandClientMappingData = new object(),   
                PartnersList  = new object(),

            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void ClientDetails_InValidData_FailsValidation()
        {
            // Arrange and Act
            var clientDetails = new ClientDetails();

            // Assert
            Assert.Null(clientDetails.FieldsData);
            Assert.Null(clientDetails.ParentandChildData);
            Assert.Null(clientDetails.PartnersList);
            Assert.Null(clientDetails.PartnerandClientMappingData);
        }
    }   
}
