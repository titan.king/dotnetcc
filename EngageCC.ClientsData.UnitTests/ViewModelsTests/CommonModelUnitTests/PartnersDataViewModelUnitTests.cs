﻿using ClientData.Models.CommonModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.ClientsData.UnitTests.ViewModelsTests.CommonModelUnitTests
{
    public class PartnersDataViewModelUnitTests
    {
        [Fact]
        public void PartnersData_ValidData_PassesValidation()
        {
            //Arrange
            var model = new PartnersData
            {
                DataList = new object()
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void PartnersData_InValidData_FailsValidation()
        {
            // Arrange and Act
            var partnersData = new PartnersData();

            // Assert
            Assert.Null(partnersData.DataList);
        }
    }
}
