﻿using BulkSigning.Data.DataModel;
using Microsoft.EntityFrameworkCore;

namespace BulkSigning.Data.Context
{
    public class BSDBContext : DbContext
    {
        public BSDBContext(DbContextOptions<BSDBContext> options) : base(options)
        {
        }
        public DbSet<BatchRequest> BatchRequest { get; set; }
        public DbSet<EngagementLetter> EngagementLetter { get; set; }
        public DbSet<BulkLetters> BulkLetters { get; set; }
        public DbSet<MasterTemplate> MasterTemplate { get; set; }
        public DbSet<TemplateAttachments> TemplateAttachments { get; set; }
        public DbSet<LetterFieldValues> LetterFieldValues { get; set; }
        public DbSet<SigningInfo> SigningInfo { get; set; }
        public DbSet<Status> Status { get; set; }
        public DbSet<Template> Template { get; set; }
        public DbSet<BulkLetterAttachments> BulkLetterAttachments { get; set; }
        public DbSet<HistoryLog> HistoryLog { get; set; }
        public DbSet<DocumentStatus> DocumentStatus { get; set; }
        public DbSet<BulkLogger> BulkLoggers { get; set; }
    }
}
