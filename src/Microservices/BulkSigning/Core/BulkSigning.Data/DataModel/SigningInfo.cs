﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkSigning.Data.DataModel
{
    public class SigningInfo
    {
        [Key]
        public int SigningInfoId { get; set; }
        public int EngagementLetterId { get; set; }
        [ForeignKey("EngagementLetterId")]
        public virtual EngagementLetter EngagementLetter { get; set; }
        [MaxLength(255)]
        public string? SigningPartnerName { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? DelegatedName { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? SPEmail { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? ContactPerson1Name { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? ContactPerson1Email { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? ContactPerson2Name { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? ContactPerson2Email { get; set; } = string.Empty;
        [MaxLength(400)]
        public string? Title { get; set; } = string.Empty;
        [MaxLength(400)]
        public string? Message { get; set; } = string.Empty;
        public bool IsDeleted { get; set; } = false;
        [MaxLength(40)]
        public string? CreatedBy { get; set; } = string.Empty;
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool ManualSigning { get; set; } = false;
        [MaxLength(255)]
        public string? FileId { get; set; } = string.Empty;
        [MaxLength(40)]
        public string? DelegatedBy { get; set; } = string.Empty;
        public string? AgreementId { get; set; } = string.Empty;
        [MaxLength(40)]
        public string? ModifiedBy { get; set; } = string.Empty;
        public DateTime? DeletedOn { get; set; }
        [MaxLength(40)]
        public string? DeletedBy { get; set; } = string.Empty;
        public string? PdfUrl { get; set; }
    }
}
