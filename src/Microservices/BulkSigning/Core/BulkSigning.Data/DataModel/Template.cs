﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkSigning.Data.DataModel
{
    public class Template
    {
        [Key]
        public int TemplateId { get; set; }
        [MaxLength(255)]
        public string? TemplateName { get; set; } = string.Empty;
        public string? TemplateDescription { get; set; } = string.Empty;
        public int DepartmentId { get; set; }
        [ForeignKey("DepartmentId")]
        public virtual Department Departments { get; set; }
        public int EngageTypeId { get; set; }
        [ForeignKey("EngageTypeId")]
        public virtual EngageType EngageType { get; set; }
        [MaxLength(500)]
        public string? ChangeNotes { get; set; } = string.Empty;
        public bool IsDeleted { get; set; } = false;
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        [MaxLength(40)]
        public string CreatedBy { get; set; } = string.Empty;
        [MaxLength(40)]
        public string? ModifiedBy { get; set; } = string.Empty;
        public DateTime? DeletedOn { get; set; }
        [MaxLength(40)]
        public string? DeletedBy { get; set; } = string.Empty;
        public int? AttachmentCount { get; set; }
        public int StatusId { get; set; }
        [ForeignKey("StatusId")]
        public virtual Status Status { get; set; }
        [MaxLength(255)]
        public string? AttachmentURL { get; set; } = string.Empty;
        public bool Is7216Available { get; set; } = false;
        public int? ClientSignatureCount { get; set; }
    }
}
