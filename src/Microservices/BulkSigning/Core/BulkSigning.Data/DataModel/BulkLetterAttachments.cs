﻿using System.ComponentModel.DataAnnotations;

namespace BulkSigning.Data.DataModel
{
    public class BulkLetterAttachments
    {
        [Key]
        public int BulkLetterAttachmentId { get; set; }
        public int BatchId { get; set; }
        public int TemplateId { get; set; }
        public string? AttachmentsJSON { get; set; } = string.Empty;
        public string? AttachmentsURL { get; set; } = string.Empty;
        public int? VersionNumber { get; set; }
    }
}
