﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkSigning.Data.DataModel
{
    public class BulkLetters
    {
        [Key]
        public int BulkLettersId { get; set; }
        public int? ClientId { get; set; }
        [MaxLength(255)]
        public string? ClientName { get; set; }
        public int? OfficeId { get; set; }
        [MaxLength(255)]
        public string? Office { get; set; }
        [MaxLength(255)]
        public string? SignatoryEmailId { get; set; }
        [MaxLength(255)]
        public string? SignatoryFirstName { get; set; }
        [MaxLength(255)]
        public string? PartnerName { get; set; }
        public int BatchId { get; set; }
        [MaxLength(255)]
        public string? TemplateName { get; set; }
        public string? FieldJson { get; set; }
        [MaxLength(255)]
        public string? DocumentStatusName { get; set; }
        public bool IsDraft { get; set; } = false;
        public bool IsDeleted { get; set; } = false;
        public bool IsUpdated { get; set; } = false;
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        [MaxLength(40)]
        public string CreatedBy { get; set; } = string.Empty;
        [MaxLength(40)]
        public string? ModifiedBy { get; set; } = string.Empty;
        public DateTime? DeletedOn { get; set; }
        [MaxLength(40)]
        public string? DeletedBy { get; set; } = string.Empty;
        public bool IsBulkLetter { get; set; } = true;
        public bool IsProcess { get; set; } = false;
        public bool IsEnqueue { get; set; } = false;
        public int? PartnerId { get; set; }
        public bool? IsEsigning { get; set; }
        public bool Is7216Available { get; set; } = false;
        public int? ClientSignatureCount { get; set; }
        public bool IsNewClient { get; set; } = false;
        [MaxLength(255)]
        public string? SpouseFirstName { get; set; }
        [MaxLength(255)]
        public string? SpouseEmailId { get; set; }
        [MaxLength(255)]
        public string? SpouseLastName { get; set; }
        [MaxLength(255)]
        public string? SignatoryLastName { get; set; }
        [MaxLength(255)]
        public string? SignatoryTitle { get; set; }
        public ICollection<LetterFieldValues> letterFieldValues { get; set; }
        public bool? IsCanabisAvailable { get; set; }
        public string? ReturnTypeCode { get; set; }
    }
}
