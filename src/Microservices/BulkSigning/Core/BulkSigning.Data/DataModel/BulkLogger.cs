﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkSigning.Data.DataModel
{
    public class BulkLogger
    {
        [Key]
        public int BulkLoggerId { get; set; }
        public int? EngagementLetterId { get; set; }
        public int? BatchId { get; set; }
        public int? ClientId { get; set; }
        public string? RequestLog { get; set; }
        public string? ErrorMessage { get; set; } = string.Empty;
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
