﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkSigning.Data.DataModel
{
    public class LetterFieldValues
    {
        [Key]
        public int LetterFieldValuesId { get; set; }
        public int? ClientId { get; set; }
        public int? PartnerId { get; set; }
        public int? BulkLettersId { get; set; }
        [ForeignKey("BulkLettersId")]
        public virtual BulkLetters BulkLetters { get; set; }
        public int? FieldId { get; set; }
        [MaxLength(255)]
        public string? FieldValue { get; set; } = string.Empty;
        [MaxLength(50)]
        public string? FieldName { get; set; } = string.Empty;
        public int LetterVersion { get; set; }
    }
}
