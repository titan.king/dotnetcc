﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkSigning.Data.DataModel
{
    public class DocumentStatus
    {
        [Key]
        public int DocumentStatusId { get; set; }
        public string? Description { get; set; } = string.Empty;
        public string? Remarks { get; set; } = string.Empty;
        public bool IsDeleted { get; set; } = false;
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        [MaxLength(40)]
        public string? CreatedBy { get; set; } = string.Empty;
        [MaxLength(40)]
        public string? ModifiedBy { get; set; } = string.Empty;
        public DateTime? DeletedOn { get; set; }
        [MaxLength(40)]
        public string? DeletedBy { get; set; } = string.Empty;
    }
}
