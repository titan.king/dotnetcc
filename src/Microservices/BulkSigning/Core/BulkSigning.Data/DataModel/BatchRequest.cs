﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkSigning.Data.DataModel
{
    public class BatchRequest
    {
        [Key]
        public int BatchRequestId { get; set; }
        public int BatchId { get; set; }
        public string BatchJson { get; set; } = string.Empty;
        public bool IsProcess { get; set; }
        [MaxLength(50)]
        public string Status { get; set; } = string.Empty;
        public int TemplateId { get; set; }
        [MaxLength(255)]
        public string TemplateName { get; set; } = string.Empty;
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        [MaxLength(255)]
        public string? CreatedBy { get; set; } = string.Empty;
    }
}
