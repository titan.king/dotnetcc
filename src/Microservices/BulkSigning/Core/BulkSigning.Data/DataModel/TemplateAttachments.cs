﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkSigning.Data.DataModel
{
    public class TemplateAttachments
    {
        [Key]
        public int TemplateAttachmentsId { get; set; }
        public int TemplateId { get; set; }
        [ForeignKey("TemplateId")]
        public virtual Template Template { get; set; }
        public string? AttachmentJson { get; set; } = string.Empty;
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
