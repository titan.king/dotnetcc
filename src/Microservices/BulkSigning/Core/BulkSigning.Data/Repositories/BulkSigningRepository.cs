﻿using AutoMapper;
using BulkSigning.Data.Context;
using BulkSigning.Data.DataModel;
using BulkSigning.Data.Interfaces;
using BulkSigning.Models.BulkSigning;
using BulkSigning.Models.CommonModel;
using Engage3.Common.Business.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BulkSigning.Data.Repositories
{
    public class BulkSigningRepository : IBulkSigningRepository
    {
        private readonly BSDBContext _dbContext;
        private readonly DbContextOptions<BSDBContext> _dbContextOptions;
        private readonly IMapper _mapper;
        private readonly IGetCurrentDatetime _getCurrentDateTime;
        public BulkSigningRepository(BSDBContext dbContext, DbContextOptions<BSDBContext> dbContextOptions, IMapper mapper, IGetCurrentDatetime getCurrentDatetime)
        {
            _dbContext = dbContext;
            _dbContextOptions = dbContextOptions;
            _mapper = mapper;
            _getCurrentDateTime = getCurrentDatetime;
        }
        public async Task<EngagementLetter> GetEngagementLettersList(EngagementLetterViewModel engagementLetter)
        {
            try
            {
                using (var _dbContext = new BSDBContext(_dbContextOptions))
                {
                    EngagementLetter getEngageLetterList = await _dbContext.EngagementLetter
                                                            .FirstOrDefaultAsync(n => n.IsDeleted == false && n.EngagementLetterId == engagementLetter.EngagementLetterId);
                    return getEngageLetterList;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<string> GetMasterTemplate(int templateId)
        {
            try
            {
                using (var _dbContext = new BSDBContext(_dbContextOptions))
                {
                    var getMasterTemplate = await _dbContext.MasterTemplate.OrderByDescending(n => n.TemplateVersionId).FirstOrDefaultAsync(n => n.TemplateId == templateId);
                    return getMasterTemplate?.Template ?? string.Empty;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<string> GetTemplateAttachments(int templateId, int batchId)
        {
            try
            {
                using (var _dbContext = new BSDBContext(_dbContextOptions))
                {
                    var resData = await _dbContext.BulkLetterAttachments
                                                 .FirstOrDefaultAsync(n => n.TemplateId == templateId && n.BatchId == batchId && n.VersionNumber ==
                                                     _dbContext.BulkLetterAttachments
                                                         .Where(subQuery => subQuery.TemplateId == n.TemplateId && subQuery.BatchId == n.BatchId)
                                                         .Max(subQuery => subQuery.VersionNumber));

                    if (resData != null && !string.IsNullOrEmpty(resData.AttachmentsURL))
                    {
                        return resData?.AttachmentsURL ?? string.Empty;
                    }
                    var getTemplateAttachments = await _dbContext.Template.Where(n => n.TemplateId == templateId).FirstOrDefaultAsync();
                    return getTemplateAttachments?.AttachmentURL ?? string.Empty;
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        public async Task<List<LetterFieldValues>> GetLetterFieldValues(int bulkLetterId)
        {
            try
            {
                using (var _dbContext = new BSDBContext(_dbContextOptions))
                {

                    var latestRecords = await _dbContext.LetterFieldValues
                                              .Where(n => n.BulkLettersId == bulkLetterId && n.LetterVersion ==
                                                  _dbContext.LetterFieldValues
                                                      .Where(subQuery => subQuery.BulkLettersId == n.BulkLettersId)
                                                      .Max(subQuery => subQuery.LetterVersion))
                                              .ToListAsync();

                    return latestRecords;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> SaveSigningInfos(SigningInfo signingInfos)
        {
            try
            {
                if (signingInfos != null)
                {
                    using (var _dbContext = new BSDBContext(_dbContextOptions))
                    {
                        await _dbContext.SigningInfo.AddAsync(signingInfos);
                        await _dbContext.SaveChangesAsync();
                        return true;
                    }
                }
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> UpdateEngagementLetter(EngagementLetter engagementLetters, bool? isEsigning)
        {
            try
            {
                if (engagementLetters != null && isEsigning != null)
                {
                    using (var _dbContext = new BSDBContext(_dbContextOptions))
                    {
                        var existingData = await _dbContext.EngagementLetter
                                          .Where(a => engagementLetters.EngagementLetterId == a.EngagementLetterId)
                                          .FirstOrDefaultAsync();
                        if (existingData != null)
                        {
                            existingData.AdminName = engagementLetters.AdminName;
                            existingData.ModifiedBy = engagementLetters.ModifiedBy;
                            existingData.IsProcess = true;
                            existingData.IsEnqueue = false;
                            existingData.IsEsigning = isEsigning;
                            existingData.DocumentStatusId = isEsigning == true ? 2 : 3;

                            _dbContext.EngagementLetter.Update(existingData);
                            await _dbContext.SaveChangesAsync();
                            return true;
                        }
                    }
                }
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<SigningInfoModel> GetAgreementId(SigningInfo signingInfos)
        {
            try
            {
                using (var _dbContext = new BSDBContext(_dbContextOptions))
                {
                    SigningInfo signersAgreementList = new SigningInfo();
                    var agreementId = await _dbContext.SigningInfo
                                    .Where(a => a.EngagementLetterId == signingInfos.EngagementLetterId && a.ManualSigning == false)
                                    .Select(a => new SigningInfo
                                    {
                                        EngagementLetterId = a.EngagementLetterId,
                                        AgreementId = a.AgreementId
                                    })
                                    .FirstOrDefaultAsync();
                    if (agreementId != null)
                    {
                        signersAgreementList = agreementId;
                    }
                    var respModel = _mapper.Map<SigningInfoModel>(signersAgreementList);
                    var getEngagementLetterName = await _dbContext.EngagementLetter
                                                    .Where(n => n.EngagementLetterId == signingInfos.EngagementLetterId)
                                                    .Select(n => new SigningInfoModel
                                                    {
                                                        EngagementLetterName = n.EngagementLetterName
                                                    })
                                                    .FirstOrDefaultAsync();
                    respModel.EngagementLetterName = getEngagementLetterName?.EngagementLetterName ?? string.Empty;

                    return respModel;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<HistoryLogModel> GetHistoryLogVersion(EngagementLetter engagementLetters)
        {
            try
            {
                using (var _dbContext = new BSDBContext(_dbContextOptions))
                {
                    HistoryLogModel logModels = new HistoryLogModel();
                    int documentStatusIds = engagementLetters.DocumentStatusId;
                    var letterIds = await _dbContext.HistoryLog
                                    .Where(a => a.EngagementLetterId == engagementLetters.EngagementLetterId)
                                    .OrderByDescending(a => a.Version)
                                    .Select(a => new HistoryLogModel
                                    {
                                        EngagementLetterId = a.EngagementLetterId,
                                        Version = a.Version
                                    })
                                    .FirstOrDefaultAsync();
                    if (letterIds != null)
                    {
                        logModels = letterIds;
                    }

                    return logModels;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<bool> UpdateHistoryLog(HistoryLogModel historyLog)
        {
            try
            {
                if (historyLog != null)
                {
                    var mapRecordsList = _mapper.Map<HistoryLog>(historyLog);
                    using (var _dbContext = new BSDBContext(_dbContextOptions))
                    {
                        await _dbContext.HistoryLog.AddAsync(mapRecordsList);
                        await _dbContext.SaveChangesAsync();
                        return true;
                    }
                }
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task EngagementLetterErrorLog(EngagementLetter eng, string requestLog, string errorMessage)
        {
            if (eng != null)
            {
                var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
                var errorLogEntry = _mapper.Map<BulkLogger>(new BulkLetterErrorLogVM
                {
                    EngagementLetterId = eng.EngagementLetterId,
                    BatchId = eng.BatchId,
                    ClientId = eng.ClientId,
                    RequestLog = requestLog,
                    CreatedOn = eng.CreatedOn,
                    ModifiedOn = estTime,
                    ErrorMessage = errorMessage
                });

                using (var _dbContext = new BSDBContext(_dbContextOptions))
                {
                    _dbContext.BulkLoggers.Add(errorLogEntry);
                    await _dbContext.SaveChangesAsync();
                }
            }
        }

        public async Task EsigningEngagementLetterErrorLog(SigningInfo agreement, string requestLog, string errorMessage)
        {
            if (agreement != null)
            {
                var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
                var errorLogEntry = _mapper.Map<BulkLogger>(new BulkLetterErrorLogVM
                {
                    EngagementLetterId = agreement.EngagementLetterId,
                    RequestLog = requestLog,
                    CreatedOn = estTime,
                    ModifiedOn = estTime,
                    ErrorMessage = errorMessage
                });

                using (var _dbContext = new BSDBContext(_dbContextOptions))
                {
                    _dbContext.BulkLoggers.Add(errorLogEntry);
                    await _dbContext.SaveChangesAsync();
                }
            }
        }

        public async Task SaveEngagementLetterErrorLog(int batchId, string requestLog, string errorMessage)
        {
            var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
            var errorLogEntry = _mapper.Map<BulkLogger>(new BulkLetterErrorLogVM
            {
                BatchId = batchId,
                RequestLog = requestLog,
                CreatedOn = estTime,
                ModifiedOn = estTime,
                ErrorMessage = errorMessage
            });

            using (var _dbContext = new BSDBContext(_dbContextOptions))
            {
                _dbContext.BulkLoggers.Add(errorLogEntry);
                await _dbContext.SaveChangesAsync();
            }
        }
    }
}
