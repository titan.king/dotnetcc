﻿using BulkSigning.Data.DataModel;
using BulkSigning.Models.BulkSigning;
using BulkSigning.Models.CommonModel;

namespace BulkSigning.Data.Interfaces
{
    public interface IBulkSigningRepository
    {
        Task<EngagementLetter> GetEngagementLettersList(EngagementLetterViewModel engagementLetter);
        Task<string> GetMasterTemplate(int templateId);
        Task<string> GetTemplateAttachments(int templateId, int batchId);
        Task<List<LetterFieldValues>> GetLetterFieldValues(int bulkLetterId);
        Task<bool> SaveSigningInfos(SigningInfo signingInfos);
        Task<bool> UpdateEngagementLetter(EngagementLetter engagementLetters, bool? isEsigning);
        Task<SigningInfoModel> GetAgreementId(SigningInfo signingInfos);
        Task<HistoryLogModel> GetHistoryLogVersion(EngagementLetter engagementLetters);
        Task<bool> UpdateHistoryLog(HistoryLogModel historyLog);
        Task EngagementLetterErrorLog(EngagementLetter eng, string requestLog, string erorMessage);
        Task EsigningEngagementLetterErrorLog(SigningInfo agreement, string requestLog, string errorMessage);
        Task SaveEngagementLetterErrorLog(int batchId, string requestLog, string errorMessage);
    }
}
