﻿using BulkSigning.Models.BulkSigning;

namespace BulkSigning.Business.Interfaces
{
    public interface IBulkSigningService
    {
        Task<bool> ProceedForESigning(EngagementLetterViewModel engagementLetter);
    }
}
