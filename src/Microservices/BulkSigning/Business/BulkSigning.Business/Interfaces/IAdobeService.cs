﻿using BulkSigning.Models.BulkSigning;
using BulkSigning.Models.CommonModel;

namespace BulkSigning.Business.Interfaces
{
    public interface IAdobeService
    {
        Task<ResponseModel> GetFileFromBlob(int signerCount);
        Task<ResponseModel> GetSignedDocument(string agreementID);
        Task<ResponseModel> UploadHtmlFileFromBlob(UploadHtmlFile uploadHtmlFile);
        Task<ResponseModel> HtmltoPDFConvert(PdfConversionRequest pdfConversionRequest, string accessToken);
        Task<ResponseModel> GetAccessToken();
        Task<ResponseModel> UploadPdfFromUrl(AdobeResponse adobeResponse, string accessToken);
        Task<ResponseModel> CombinePDF(CombinedPDFRequest combinedPDFRequestModel, string accessToken);
        Task<ResponseModel> UploadPdfFileFromBlob(UploadPdfFile uploadPdfFile);
    }
}
