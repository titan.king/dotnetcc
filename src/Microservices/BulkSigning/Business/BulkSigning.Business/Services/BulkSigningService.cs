﻿using AutoMapper;
using BulkSigning.Business.Interfaces;
using BulkSigning.Data.DataModel;
using BulkSigning.Data.Interfaces;
using BulkSigning.Models.BulkSigning;
using BulkSigning.Models.CommonModel;
using Engage3.Common.Business;
using Engage3.Common.Business.Interfaces;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System.Data.Common;
using System.Globalization;
using System.Net;

namespace BulkSigning.Business.Services
{
    public class BulkSigningService : IBulkSigningService
    {
        private readonly IAdobeService _adobeService;
        private readonly IBulkSigningRepository _bulkSigningRepository;
        ResponseModel resobj = new ResponseModel();
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IGetCurrentDatetime _getCurrentDateTime;
        public BulkSigningService(IBulkSigningRepository bulkSigningRepository, IMapper mapper, IConfiguration configuration, IAdobeService adobeService, IGetCurrentDatetime getCurrentDateTime)
        {
            _mapper = mapper;
            _bulkSigningRepository = bulkSigningRepository;
            _configuration = configuration;
            _adobeService = adobeService;
            _getCurrentDateTime = getCurrentDateTime;
        }
        public async Task<bool> ProceedForESigning(EngagementLetterViewModel engagementLettermodel)
        {
            //var reqEngagementLetter = _mapper.Map<EngagementLetter>(engagementLettermodel);
            SigningInfo signingInfos = new SigningInfo();
            try
            {
                var accessToken = await _adobeService.GetAccessToken();
                if (accessToken != null && accessToken.StatusCode == HttpStatusCode.OK && accessToken.Data != null)
                {
                    var serial = JsonConvert.SerializeObject(accessToken.Data);
                    var authorize_token = JsonConvert.DeserializeObject<AccessTokenResponse>(serial);
                    string authorize = authorize_token?.access_token ?? string.Empty;
                    EngagementLetter engagementLetters = await _bulkSigningRepository.GetEngagementLettersList(engagementLettermodel);
                    try
                    {
                        signingInfos = await SendToAdobe(engagementLetters, authorize);
                    }
                    catch (Exception ex)
                    {
                        var errorMessage = ex.InnerException?.ToString() + ex.StackTrace?.ToString() + ex.Message?.ToString();
                        var requestLog = JsonConvert.SerializeObject(engagementLetters);
                        await _bulkSigningRepository.SaveEngagementLetterErrorLog(0, requestLog, errorMessage);
                    }
                    EngagementLetterResponse engagementLetterList = new EngagementLetterResponse();
                    try
                    {
                        if (signingInfos != null && signingInfos.EngagementLetterId > 0)
                        {
                            await _bulkSigningRepository.SaveSigningInfos(signingInfos);
                            var getAgreementsIdsList = signingInfos.AgreementId;
                            try
                            {
                                var agreementID = getAgreementsIdsList;
                                if (agreementID != null && !string.IsNullOrEmpty(agreementID))
                                {
                                    var getdocumentURL = await _adobeService.GetSignedDocument(agreementID);
                                    if (getdocumentURL.StatusCode == HttpStatusCode.OK)
                                    {
                                        var docFileURL = JsonConvert.SerializeObject(getdocumentURL);
                                        var completeDocFileURL = JsonConvert.DeserializeObject<ResponseModel>(docFileURL);

                                        HtmlFileResponseModel htmlFileResponse = new HtmlFileResponseModel();

                                        UploadPdfFile uploadPdf = new UploadPdfFile();
                                        uploadPdf.PdfUrl = completeDocFileURL?.Data?.ToString() ?? string.Empty;
                                        uploadPdf.FileName = signingInfos?.EngagementLetterId.ToString() + "-" + engagementLetters?.EngagementLetterName ?? string.Empty;
                                        var respPDF = await _adobeService.UploadPdfFileFromBlob(uploadPdf);
                                        //resobj.PdfUrl = respPDF.PdfUrl;
                                        signingInfos.PdfUrl = respPDF?.Data?.ToString();

                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                var errorMessage = ex.InnerException?.ToString() + ex.StackTrace?.ToString() + ex.Message?.ToString();
                                var requestLog = JsonConvert.SerializeObject(engagementLetters);
                                await _bulkSigningRepository.EsigningEngagementLetterErrorLog(signingInfos, requestLog, errorMessage);
                            }
                            engagementLetterList.PdfUrl = signingInfos.PdfUrl;
                        }
                        if (engagementLetters != null)
                        {
                            bool? isEsigning = engagementLettermodel.IsEsigning;
                            engagementLetters.ModifiedBy = engagementLettermodel.ModifiedBy;
                            engagementLetters.AdminName = engagementLettermodel.ModifiedBy;
                            await _bulkSigningRepository.UpdateEngagementLetter(engagementLetters, isEsigning);
                            
                            var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
                            //Insert into History Log
                            HistoryLogModel logModels = await _bulkSigningRepository.GetHistoryLogVersion(engagementLetters);
                            EngagementLetter engagements = new EngagementLetter();

                            logModels.EngagementLetterId = engagementLetters.EngagementLetterId;
                            logModels.EngagementLetterName = engagementLetters.EngagementLetterName;
                            logModels.BatchId = engagementLetters?.BatchId;
                            logModels.EditedBy = engagementLettermodel?.ModifiedBy;
                            logModels.LastModified = estTime;
                            logModels.PDFUrl = engagementLetterList.PdfUrl;
                            logModels.Status = engagementLettermodel?.IsEsigning == true ? "With Client For E-Signing" : "With Client For Manual Signing";
                            logModels.Version += 1;
                            if (logModels != null && logModels.EngagementLetterId > 0)
                            {
                                var historyResp = await _bulkSigningRepository.UpdateHistoryLog(logModels);
                                return true;
                            }

                            return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        var errorMessage = ex.InnerException?.ToString() + ex.StackTrace?.ToString() + ex.Message?.ToString();
                        var requestLog = JsonConvert.SerializeObject(engagementLetters);
                        await _bulkSigningRepository.SaveEngagementLetterErrorLog(0, requestLog, errorMessage);
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                var errorMessage = ex.InnerException?.ToString() + ex.StackTrace?.ToString() + ex.Message?.ToString();
                var requestLog = string.Empty;
                await _bulkSigningRepository.SaveEngagementLetterErrorLog(0, requestLog, errorMessage);
            }
            return false;
        }
        public async Task<SigningInfo> SendToAdobe(EngagementLetter engagementLetters, string accessToken)
        {
            ResponseModel responseModel = new ResponseModel();
            SigningInfo signingInfo = new SigningInfo();
            string letterPdf = string.Empty;
            string letterPdfAssetId = string.Empty;
            string formPdfAssetId = string.Empty;

            var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
            try
            {
                if (engagementLetters != null)
                {
                    try
                    {
                        var masterTemplate = await _bulkSigningRepository.GetMasterTemplate(engagementLetters.TemplateId ?? 0);

                        var fields = await _bulkSigningRepository.GetLetterFieldValues(engagementLetters.BulkLettersId);
                        if (!string.IsNullOrEmpty(masterTemplate) && fields.Any())
                        {
                            string updatedContent = masterTemplate;
                            foreach (var val in fields)
                            {
                                if (val.FieldName == "Date")
                                {
                                    string dateFl = Convert.ToDateTime(val.FieldValue).ToString("MMMM d, yyyy");
                                    updatedContent = updatedContent.Replace("##" + val.FieldName + "##", dateFl);
                                }
                                else
                                {
                                    if (val.FieldName == "ChildEntities")
                                    {
                                        if (!string.IsNullOrEmpty(val.FieldValue))
                                        {
                                            //string[] values = val.FieldValue.Split(',');
                                            List<string> childEntitiesArray = JsonConvert.DeserializeObject<List<string>>(val.FieldValue);
                                            updatedContent = ReplacePlaceholderWithValues(updatedContent, childEntitiesArray);
                                        }
                                        else
                                        {
                                            updatedContent = updatedContent.Replace("##" + val.FieldName + "##", string.Empty);
                                        }
                                    }
                                    else
                                    {
                                        updatedContent = updatedContent.Replace("##" + val.FieldName + "##", val.FieldValue);
                                    }
                                }
                            }

                            updatedContent = updatedContent.Replace("##ClientID##", engagementLetters.ClientId.ToString());
                            updatedContent = updatedContent.Replace("##Lettername##", engagementLetters.EngagementLetterName);

                            UploadHtmlFile uploadHtmlFile = new UploadHtmlFile();

                            uploadHtmlFile.HtmlData = updatedContent;
                            uploadHtmlFile.FileName = engagementLetters.EngagementLetterName ?? string.Empty;
                            var uploadRes = await _adobeService.UploadHtmlFileFromBlob(uploadHtmlFile);
                            if (uploadRes != null && uploadRes.StatusCode == HttpStatusCode.OK && uploadRes.Data != null)
                            {
                                PdfConversionRequest pdfConversionRequest = new PdfConversionRequest();
                                var pdfConversion = new PdfConversion();

                                pdfConversion.inputUrl = uploadRes?.Data?.ToString() ?? string.Empty;
                                pdfConversion.includeHeaderFooter = true;
                                pdfConversion.pageLayout = new PageLayout { pageWidth = "11", pageHeight = "8.5" };
                                pdfConversion.json = "{}";

                                pdfConversionRequest.pdfConversion = pdfConversion;

                                var pdfResponse = await _adobeService.HtmltoPDFConvert(pdfConversionRequest, accessToken);
                                if (pdfResponse.StatusCode == HttpStatusCode.OK && pdfResponse.Data != null)
                                {
                                    letterPdf = pdfResponse?.Data?.ToString() ?? string.Empty;
                                    letterPdfAssetId = pdfResponse?.PdfUrl?.ToString() ?? string.Empty;
                                }
                            }
                        }

                        string attachment = await _bulkSigningRepository.GetTemplateAttachments(engagementLetters.TemplateId ?? 0, engagementLetters.BatchId);

                        string TaxReturnConsentForm = string.Empty;
                        if (engagementLetters.Is7216Available == true)
                        {
                            var dataRes = await _adobeService.GetFileFromBlob(1);
                            if (dataRes != null && dataRes.StatusCode == HttpStatusCode.OK && dataRes.Data != null)
                            {
                                string consentFormPath = dataRes?.Data?.ToString() ?? string.Empty;
                                if (!string.IsNullOrEmpty(consentFormPath))
                                {
                                    using (HttpClient client = new HttpClient())
                                    {
                                        HttpResponseMessage responseUrl = await client.GetAsync(consentFormPath);
                                        if (responseUrl.IsSuccessStatusCode)
                                        {
                                            string htmlContent = await responseUrl.Content.ReadAsStringAsync();

                                            int form7216Taxyear = Convert.ToInt32(_configuration["Form7216_TaxYear"]);
                                            var year = Convert.ToInt16(engagementLetters.TaxYear) + form7216Taxyear;
                                            string modifiedContent = htmlContent.Replace("{{consentDate}}", "December 31, " + year);
                                            modifiedContent = modifiedContent.Replace("##ClientId##", engagementLetters.ClientId.ToString());
                                            modifiedContent = modifiedContent.Replace("##ClientName##", engagementLetters.ClientName);
                                            UploadHtmlFile uploadHtmlFile = new UploadHtmlFile();

                                            uploadHtmlFile.HtmlData = modifiedContent;
                                            uploadHtmlFile.FileName = engagementLetters.EngagementLetterName + "_Form7216";
                                            var uploadRes = await _adobeService.UploadHtmlFileFromBlob(uploadHtmlFile);
                                            if (uploadRes != null && uploadRes.StatusCode == HttpStatusCode.OK && uploadRes.Data != null)
                                            {
                                                PdfConversionRequest pdfConversionRequest = new PdfConversionRequest();
                                                var pdfConversion = new PdfConversion();

                                                pdfConversion.inputUrl = uploadRes?.Data?.ToString() ?? string.Empty;
                                                pdfConversion.includeHeaderFooter = true;
                                                pdfConversion.pageLayout = new PageLayout { pageWidth = "11", pageHeight = "8.5" };
                                                pdfConversion.json = "{}";

                                                pdfConversionRequest.pdfConversion = pdfConversion;

                                                var pdfResponse = await _adobeService.HtmltoPDFConvert(pdfConversionRequest, accessToken);
                                                if (pdfResponse.StatusCode == HttpStatusCode.OK && pdfResponse.Data != null)
                                                {
                                                    TaxReturnConsentForm = pdfResponse?.Data?.ToString() ?? string.Empty;
                                                    formPdfAssetId = pdfResponse?.PdfUrl?.ToString() ?? string.Empty;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        signingInfo.EngagementLetterId = engagementLetters.EngagementLetterId;
                        signingInfo.SigningPartnerName = engagementLetters.PartnerName;
                        signingInfo.ContactPerson1Email = engagementLetters.SignatoryEmailId;
                        signingInfo.ContactPerson1Name = engagementLetters.SignatoryFirstName;
                        signingInfo.ContactPerson2Email = engagementLetters.SpouseEmailId;
                        signingInfo.ContactPerson2Name = engagementLetters.SpouseFirstName;
                        signingInfo.CreatedBy = engagementLetters.CreatedBy;
                        signingInfo.CreatedOn = estTime;
                        if (engagementLetters.IsEsigning == true)
                        {
                            string connectionString = _configuration.GetConnectionString("EngageDbConnection") ?? string.Empty;
                            string subject_Append = "Dev";
                            if (!string.IsNullOrEmpty(connectionString))
                            {
                                var builder = new DbConnectionStringBuilder { ConnectionString = connectionString };
                                if (builder.ContainsKey("Database"))
                                {
                                    string databaseName = builder["Database"].ToString() ?? string.Empty;
                                    if (databaseName == "Engage3_MainSqlDB")
                                    {
                                        subject_Append = "Dev";
                                    }
                                    else if (databaseName == "Engage3_QA_MainSqlDB")
                                    {
                                        subject_Append = "Qa";
                                    }
                                    else if (databaseName == "Engage3_UAT_MainSqlDB")
                                    {
                                        subject_Append = "Uat";
                                    }
                                }
                            }
                            var integrationkey = _configuration["AppSettingsAdobe:adobe_integrationkey"];
                            var Oauthreq = new RestClient("https://api.na4.adobesign.com/api/rest/v6/agreements");
                            var Oauthreq1 = new RestRequest(Method.POST);
                            Oauthreq1.AddHeader("Authorization", "Bearer " + integrationkey);
                            Oauthreq1.AddHeader("Content-Type", "application/json");
                            var getProductionEmailStatus = _configuration["AppSettingsAdobe:ProductionEmailNeeded"] ?? string.Empty;
                            string signer2 = string.Empty;
                            string ContactPerson1Email = string.Empty;
                            if (getProductionEmailStatus == "true")
                            {
                                if (subject_Append == "Dev")
                                {
                                    ContactPerson1Email = _configuration["ESignToEmail:DevSigner1"] ?? string.Empty;
                                    signingInfo.ContactPerson1Email = ContactPerson1Email;
                                    if (engagementLetters.ClientSignatureCount == 2)
                                    {
                                        signer2 = _configuration["ESignToEmail:DevSigner2"] ?? string.Empty;
                                        signingInfo.ContactPerson2Email = signer2;
                                    }
                                }
                                else if (subject_Append == "Qa")
                                {
                                    ContactPerson1Email = _configuration["ESignToEmail:QaSigner1"] ?? string.Empty;
                                    signingInfo.ContactPerson1Email = ContactPerson1Email;
                                    if (engagementLetters.ClientSignatureCount == 2)
                                    {
                                        signer2 = _configuration["ESignToEmail:QaSigner2"] ?? string.Empty;
                                        signingInfo.ContactPerson2Email = signer2;
                                    }
                                }
                                else if (subject_Append == "Uat")
                                {
                                    ContactPerson1Email = _configuration["ESignToEmail:UatSigner1"] ?? string.Empty;
                                    signingInfo.ContactPerson1Email = ContactPerson1Email;
                                    if (engagementLetters.ClientSignatureCount == 2)
                                    {
                                        signer2 = _configuration["ESignToEmail:UatSigner2"] ?? string.Empty;
                                        signingInfo.ContactPerson2Email = signer2;
                                    }
                                }
                            }
                            else
                            {
                                ContactPerson1Email = signingInfo?.ContactPerson1Email ?? string.Empty;
                                if (engagementLetters.ClientSignatureCount == 2)
                                {
                                    signer2 = signingInfo?.ContactPerson2Email ?? string.Empty;
                                }
                            }
                            var ContactPerson2Email = signer2;
                            var PDFUrl = letterPdf;
                            var EngagementLetterName = engagementLetters.EngagementLetterName + ".pdf";

                            var Attachment0 = attachment;
                            var message = "";

                            var reqbody = "";
                            string emailSubject = subject_Append + "_" + engagementLetters.BatchId + "_" + engagementLetters.EngagementLetterId + "_Signature";

                            reqbody = @"{
                          ""fileInfos"": [
                        {
                          ""urlFileInfo"": {
                              ""mimeType"": ""application/pdf"",
                              ""name"": ""@@EngagementLetterName"",
                              ""url"": ""@@PDFUrl""
                              }
                        },
                        {
                          ""urlFileInfo"": {
                              ""mimeType"": ""application/pdf"",
                              ""name"": ""Attachment File0.pdf"",
                              ""url"": ""@@Attachment0""
                              }
                         },
                         {
                          ""urlFileInfo"": {
                              ""mimeType"": ""application/pdf"",
                              ""name"": ""7216 Consent Form.pdf"",
                              ""url"": ""@@TaxReturnConsentForm""
                              }
                         }
                         ],
                          ""name"": ""@@emailSubject"",
                          ""message"": ""@@Message"",
                          ""participantSetsInfo"": [{
                              ""memberInfos"": [
                                {
                                    ""email"": ""@@ContactPerson1Email""
                                }
                              ],
                              ""order"": 1,
                              ""role"": ""SIGNER""
                           },
                           {
                              ""memberInfos"": [
                                {
                                 ""email"": ""@@ContactPerson2Email""
                                }
                              ],
                              ""order"": 1,
                              ""role"": ""SIGNER""
                            }
                          ],
                           
                          ""signatureType"": ""ESIGN"",
                          ""externalId"": {
                            ""id"": ""NA2Account_{{$timestamp}}""
                          },
                          ""state"": ""IN_PROCESS""
                        }";

                            reqbody = reqbody.Replace("@@EngagementLetterName", EngagementLetterName)
                                                 .Replace("@@ContactPerson1Email", ContactPerson1Email)
                                                 .Replace("@@ContactPerson2Email", ContactPerson2Email)
                                                 .Replace("@@PDFUrl", PDFUrl)
                                                 .Replace("@@Attachment0", Attachment0)
                                                 .Replace("@@TaxReturnConsentForm", TaxReturnConsentForm)
                                                 .Replace("@@Message", message)
                                                 .Replace("@@emailSubject", emailSubject);

                            var resultsjson = JsonConvert.DeserializeObject<AdobeESignExpireyModel>(reqbody);
                            for (int i = 0; i < resultsjson.fileInfos.Count(); i++)
                            {
                                if (resultsjson.fileInfos[i].urlFileInfo.url == "" || resultsjson.fileInfos[i].urlFileInfo.url == "0")
                                {
                                    int indexToRemove = i--;
                                    resultsjson.fileInfos = resultsjson.fileInfos.Where((source, index) => index != indexToRemove).ToList();
                                }
                            }
                            for (int i = 0; i < resultsjson.participantSetsInfo.Count(); i++)
                            {

                                if (resultsjson.participantSetsInfo[i].memberInfos[0].email == "")
                                {
                                    int indexToRemove = i--;
                                    resultsjson.participantSetsInfo = resultsjson.participantSetsInfo.Where((source, index) => index != indexToRemove).ToList();
                                }
                            }

                            var newresults = JsonConvert.SerializeObject(resultsjson);
                            Oauthreq1.AddJsonBody(newresults);
                            IRestResponse response = await Oauthreq.ExecuteAsync(Oauthreq1);
                            if (response.StatusCode == HttpStatusCode.Created)
                            {
                                var responseContent = JsonConvert.DeserializeObject<AdobeESignResponse>(response.Content);
                                signingInfo.AgreementId = responseContent.id;
                                signingInfo.ManualSigning = false;

                            }
                            else
                            {
                                responseModel.ErrorMessage = response.Content;
                                responseModel.Status = false;
                                responseModel.StatusCode = response.StatusCode;
                            }
                        }
                        else
                        {
                            CombinedPDFRequest combinePDFRequest = new CombinedPDFRequest();
                            combinePDFRequest.assets = new List<Assete>();
                            Assete asset = new Assete();
                            letterPdfAssetId = letterPdfAssetId.Replace("\"", string.Empty).Replace('"', ' ').ToString();
                            asset.assetID = letterPdfAssetId;

                            combinePDFRequest.assets.Add(asset);
                            if (!string.IsNullOrEmpty(formPdfAssetId))
                            {
                                asset = new Assete();
                                formPdfAssetId = formPdfAssetId.Replace("\"", string.Empty).Replace('"', ' ').ToString();
                                asset.assetID = formPdfAssetId;
                                combinePDFRequest.assets.Add(asset);
                            }
                            if (!string.IsNullOrEmpty(attachment))
                            {
                                AdobeResponse adobeResponse = new AdobeResponse();
                                adobeResponse.AttachemntPDFcontent = attachment;
                                var pdfAssetRes = await _adobeService.UploadPdfFromUrl(adobeResponse, accessToken);
                                if (pdfAssetRes.StatusCode == HttpStatusCode.OK)
                                {
                                    var attachmentAssetId = JsonConvert.SerializeObject(pdfAssetRes.Data);
                                    asset = new Assete();
                                    attachmentAssetId = attachmentAssetId.Replace("\"", string.Empty).Replace('"', ' ').ToString();
                                    asset.assetID = attachmentAssetId;
                                    combinePDFRequest.assets.Add(asset);
                                }
                            }


                            UploadPdfFile uploadPdf = new UploadPdfFile();
                            uploadPdf.PdfUrl = letterPdf;
                            uploadPdf.FileName = engagementLetters?.EngagementLetterId.ToString() + "-" + engagementLetters?.EngagementLetterName ?? string.Empty;

                            if (combinePDFRequest.assets.Count > 1)
                            {
                                ResponseModel resCombinepdf = new ResponseModel();
                                resCombinepdf = await _adobeService.CombinePDF(combinePDFRequest, accessToken);
                                if (resCombinepdf != null && resCombinepdf.Data != null && resCombinepdf.StatusCode == HttpStatusCode.OK)
                                {
                                    string requiredPath = string.Empty;
                                    PdfFileResponse htmlFileResponse = new PdfFileResponse();
                                    ResponseModel responseBlob = new ResponseModel();
                                    uploadPdf.PdfUrl = resCombinepdf?.Data.ToString() ?? string.Empty;
                                }
                            }

                            if (!string.IsNullOrEmpty(uploadPdf.PdfUrl))
                            {
                                var respPDF = await _adobeService.UploadPdfFileFromBlob(uploadPdf);
                                if (respPDF != null && respPDF.StatusCode == HttpStatusCode.OK)
                                {
                                    signingInfo.PdfUrl = respPDF.Data?.ToString() ?? string.Empty;
                                    signingInfo.ManualSigning = true;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        var errorMessage = ex.InnerException?.ToString() + ex.StackTrace?.ToString() + ex.Message?.ToString();
                        responseModel.StatusCode = HttpStatusCode.InternalServerError;
                        responseModel.ErrorMessage = errorMessage;
                        var requestLog = JsonConvert.SerializeObject(engagementLetters);
                        await _bulkSigningRepository.EngagementLetterErrorLog(engagementLetters, requestLog, errorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = ex.InnerException?.ToString() + ex.StackTrace?.ToString() + ex.Message?.ToString();
                responseModel.StatusCode = HttpStatusCode.InternalServerError;
                responseModel.ErrorMessage = errorMessage;
            }
            return signingInfo;
        }
        static string ReplacePlaceholderWithValues(string inputHtml, List<string> values)
        {
            string placeholder = "##ChildEntities##";
            string resultHtml = inputHtml;
            string replacement = "";
            int instCount = 1;
            for (int i = 0; i < values.Count(); i++)
            {
                if (!string.IsNullOrEmpty(values[i]))
                {
                    if (values.Count() == 1)
                    {
                        replacement += "&nbsp; &nbsp; &nbsp;" + values[i] + "<br/>";
                    }
                    else
                    {
                        if (values.Count() == instCount)
                        {
                            replacement += "&nbsp; &nbsp; &nbsp;" + values[i] + "<br/>";
                        }
                        else
                        {
                            replacement += "&nbsp; &nbsp; &nbsp;" + values[i] + ",<br/>";
                        }

                    }
                    instCount++;
                }
            }
            resultHtml = resultHtml.Replace(placeholder, replacement);

            return resultHtml;
        }
    }

}
