﻿using Azure.Storage;
using Azure.Storage.Sas;
using BulkSigning.Business.Interfaces;
using BulkSigning.Models.BulkSigning;
using BulkSigning.Models.CommonModel;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using RestSharp;
using System.Net;
using System.Net.Http.Headers;
using System.Resources;

namespace BulkSigning.Business.Services
{
    public class AdobeService : IAdobeService
    {
        private readonly IConfiguration _configuration;
        public AdobeService(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task<ResponseModel> GetFileFromBlob(int signerCount)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                var ContainerName = _configuration["ContainerName"] ?? string.Empty;
                var FileName1 = _configuration["FileName1"] ?? string.Empty;
                var FileName2 = _configuration["FileName2"] ?? string.Empty;
                var AccountName = _configuration["BloBStorageConfig:AzureAccountName"] ?? string.Empty;
                var AccountKey = _configuration["BloBStorageConfig:AzureAccountKey"] ?? string.Empty;
                string blobConnectionString = _configuration["ConnectionStrings:AzureBlobStorage"] ?? string.Empty;

                CloudStorageAccount backupStorageAccount = CloudStorageAccount.Parse(blobConnectionString);
                var backupBlobClient = backupStorageAccount.CreateCloudBlobClient();
                var backupContainer = backupBlobClient.GetContainerReference(ContainerName);

                if (signerCount == 1)
                {
                    CloudBlockBlob blobFile = backupContainer.GetBlockBlobReference(FileName1);
                    BlobSasBuilder blobSasBuilder = new BlobSasBuilder()
                    {
                        BlobContainerName = ContainerName,
                        BlobName = FileName1,
                        ExpiresOn = DateTime.UtcNow.AddYears(1),//Let SAS token expire after 5 minutes.
                    };
                    blobSasBuilder.SetPermissions(BlobSasPermissions.Read);//User will only be able to read the blob and it's properties
                    var sasToken = blobSasBuilder.ToSasQueryParameters(new StorageSharedKeyCredential(AccountName, AccountKey)).ToString();
                    var sasUrl = blobFile.Uri.AbsoluteUri + "?" + sasToken;
                    responseModel.Data = sasUrl;
                    responseModel.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    CloudBlockBlob blobFile = backupContainer.GetBlockBlobReference(FileName2);
                    BlobSasBuilder blobSasBuilder = new BlobSasBuilder()
                    {
                        BlobContainerName = ContainerName,
                        BlobName = FileName2,
                        ExpiresOn = DateTime.UtcNow.AddYears(1),//Let SAS token expire after 5 minutes.
                    };
                    blobSasBuilder.SetPermissions(BlobSasPermissions.Read);//User will only be able to read the blob and it's properties
                    var sasToken = blobSasBuilder.ToSasQueryParameters(new StorageSharedKeyCredential(AccountName, AccountKey)).ToString();
                    var sasUrl = blobFile.Uri.AbsoluteUri + "?" + sasToken;
                    responseModel.Data = sasUrl;
                    responseModel.StatusCode = HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                responseModel.StatusCode = HttpStatusCode.InternalServerError;
                responseModel.ErrorMessage = "";
            }

            return responseModel;
        }

        public async Task<ResponseModel> GetSignedDocument(string agreementID)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                string authorizationKey = _configuration["AppSettingsAdobe:adobe_integrationkey"] ?? string.Empty;
                var urlLink = new RestClient("https://api.na4.adobesign.com/api/rest/v6/agreements/" + agreementID + "/combinedDocument/url");

                var urlRequest = new RestRequest(Method.GET);
                urlRequest.AddHeader("Authorization", "Bearer " + authorizationKey);
                IRestResponse urlResponse = await urlLink.ExecuteAsync(urlRequest);

                if (urlResponse.StatusCode == HttpStatusCode.OK)
                {
                    var responseOP = JsonConvert.DeserializeObject<SignedDocumentResponse>(urlResponse.Content);
                    responseModel.Data = responseOP.url.ToString();
                    responseModel.StatusCode = HttpStatusCode.OK;
                    responseModel.Status = true;
                }
                else if (urlResponse.StatusCode == HttpStatusCode.NotFound)
                {
                    var resData = await GetSignedDocument(agreementID);
                    return resData;
                }
                else
                {
                    responseModel.ErrorMessage = urlResponse.Content.ToString();
                    responseModel.StatusCode = HttpStatusCode.InternalServerError;
                    responseModel.Status = false;
                }
            }
            catch (Exception ex)
            {
                var errorMessage = ex.InnerException?.ToString() + ex.StackTrace?.ToString() + ex.Message?.ToString();
                responseModel.StatusCode = HttpStatusCode.InternalServerError;
                responseModel.ErrorMessage = errorMessage;
            }
            return responseModel;
        }
        public async Task<ResponseModel> UploadHtmlFileFromBlob(UploadHtmlFile uploadHtmlFile)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                if (!string.IsNullOrEmpty(uploadHtmlFile.HtmlData) && !string.IsNullOrEmpty(uploadHtmlFile.FileName))
                {
                    string accountName = _configuration["BloBStorageConfig:AzureAccountName"] ?? string.Empty;
                    string connectionString = _configuration["ConnectionStrings:AzureBlobStorage"] ?? string.Empty;
                    string accountKey = _configuration["BloBStorageConfig:AzureAccountKey"] ?? string.Empty;
                    string containerName = _configuration["BloBStorageConfig:AzureContainer_Html"] ?? string.Empty;
                    string fileName = uploadHtmlFile.FileName + ".html";

                    //var encodedStr = Base64UrlEncoder.Decode(uploadHtmlFile.HtmlData);

                    var storageCredentials = new StorageCredentials(accountName, accountKey);
                    var storageAccount = new CloudStorageAccount(storageCredentials, true);
                    var connString = storageAccount.ToString(true);
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                    var container = blobClient.GetContainerReference(containerName).GetBlockBlobReference(fileName);
                    CloudBlockBlob blob = container;
                    //blob.Metadata["SetAccessPolicy"] = Azure.Storage.Blobs.Models.PublicAccessType.Blob.ToString();
                    blob.Properties.ContentType = "text/html";
                    string timezone = "UTC+03:00";
                    blob.Metadata["Timezone"] = timezone;

                    await blob.DeleteIfExistsAsync();

                    await blob.UploadTextAsync(uploadHtmlFile.HtmlData);
                    BlobSasTokenRequest request = new BlobSasTokenRequest();
                    request.AccountKey = accountKey;
                    request.AccountName = accountName;
                    request.ConnectionString = connectionString;
                    request.ContainerName = containerName;
                    request.FileName = fileName;

                    responseModel = await GetFileFromBlob(request);
                }
                else
                {
                    string? errorMessage = "Content is null";
                    responseModel.ErrorMessage = errorMessage;
                    responseModel.StatusCode = HttpStatusCode.InternalServerError;
                }
            }
            catch (Exception ex)
            {
                responseModel.StatusCode = HttpStatusCode.InternalServerError;
                responseModel.ErrorMessage = "";
            }
            return responseModel;
        }
        public async Task<ResponseModel> GetFileFromBlob(BlobSasTokenRequest blobSasTokenRequest)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                CloudStorageAccount backupStorageAccount = CloudStorageAccount.Parse(blobSasTokenRequest.ConnectionString);
                var backupBlobClient = backupStorageAccount.CreateCloudBlobClient();
                var backupContainer = backupBlobClient.GetContainerReference(blobSasTokenRequest.ContainerName);

                CloudBlockBlob blobFile = backupContainer.GetBlockBlobReference(blobSasTokenRequest.FileName);
                BlobSasBuilder blobSasBuilder = new BlobSasBuilder()
                {
                    BlobContainerName = blobSasTokenRequest.ContainerName,
                    BlobName = blobSasTokenRequest.FileName,
                    ExpiresOn = DateTime.UtcNow.AddYears(1),//Let SAS token expire after 5 minutes.
                };
                blobSasBuilder.SetPermissions(BlobSasPermissions.Read);//User will only be able to read the blob and it's properties
                var sasToken = blobSasBuilder.ToSasQueryParameters(new StorageSharedKeyCredential(blobSasTokenRequest.AccountName, blobSasTokenRequest.AccountKey)).ToString();
                var sasUrl = blobFile.Uri.AbsoluteUri + "?" + sasToken;
                responseModel.Data = sasUrl;
                responseModel.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                responseModel.StatusCode = HttpStatusCode.InternalServerError;
                responseModel.ErrorMessage = "";
            }

            return responseModel;
        }
        public async Task<ResponseModel> HtmltoPDFConvert(PdfConversionRequest pdfConversionRequest, string accessToken)
        {
            pdfConversionRequest.pdfConversion.inputUrl = pdfConversionRequest.pdfConversion.inputUrl.Replace(" ", "%20");
            ResponseModel responseModel = new ResponseModel();

            string defaultString = string.Empty;
            string x_api_key = _configuration["AppSettingsAdobe:adobe_client_id"] ?? defaultString;
            string htmltopdf_AdobeURL = _configuration["AppSettingsAdobe:HtmltoPDFConvert"] ?? string.Empty;

            var Client = new RestClient(htmltopdf_AdobeURL);
            var jsonData = JsonConvert.SerializeObject(pdfConversionRequest.pdfConversion);
            var request = new RestRequest(Method.POST);

            Random generator = new Random();
            int xrequestid = Convert.ToInt32(generator.Next(1, 1000000).ToString("D6"));

            request.AddHeader("Authorization", "Bearer " + accessToken);
            request.AddHeader("x-api-key", x_api_key);
            request.AddHeader("x-request-id", xrequestid.ToString());
            request.AddParameter("application/json", jsonData, ParameterType.RequestBody);

            IRestResponse Fileresponse = await Client.ExecuteAsync(request);
            if (Fileresponse.StatusCode == HttpStatusCode.Created)
            {
                var dataRes = await GetPdfStatus(xrequestid, accessToken);
                responseModel = dataRes;
            }
            else if (Convert.ToInt32(Fileresponse.StatusCode) == 429)
            {
                responseModel.ErrorMessage = "Maximum API limit is reached, hence letter cant be submitted for signing, please contact the IT Helpdesk team to proceed further";
                responseModel.Status = false;
                responseModel.StatusCode = Fileresponse.StatusCode;
            }
            else
            {
                responseModel.Status = false;
                responseModel.StatusCode = Fileresponse.StatusCode;
            }

            return responseModel;
        }
        public async Task<ResponseModel> GetPdfStatus(int xrequestid, string authorize)
        {
            ResponseModel responseModel = new ResponseModel();

            string defaultString = string.Empty;
            string x_api_key = _configuration["AppSettingsAdobe:adobe_client_id"] ?? defaultString;
            string pdfStatus_AdobeURL = _configuration["AppSettingsAdobe:GetPdfStatus"] ?? string.Empty;
            if (xrequestid > 0 && !string.IsNullOrEmpty(authorize))
            {
                string uri = pdfStatus_AdobeURL;
                var Clients = new RestClient(uri + xrequestid + "/status");
                var statusRequest = new RestRequest(Method.GET);
                statusRequest.AddHeader("Authorization", "Bearer " + authorize);
                statusRequest.AddHeader("x-api-key", x_api_key);
                statusRequest.AddHeader("Content-Type", "application/json");
                IRestResponse statusResponse = await Clients.ExecuteAsync(statusRequest);
                if (statusResponse.StatusCode == HttpStatusCode.OK)
                {
                    var res = statusResponse.Content;
                    var responseContent = JsonConvert.DeserializeObject<AdobePDFStatusResponse>(res);
                    if (responseContent.status.ToLower() == "in progress")
                    {
                        responseModel = await GetPdfStatus(xrequestid, authorize);
                    }
                    else
                    {
                        responseModel.Data = responseContent.asset.downloadUri;
                        responseModel.PdfUrl = responseContent.asset.assetID;
                        responseModel.Status = true;
                        responseModel.StatusCode = HttpStatusCode.OK;
                        return responseModel;
                    }
                }
                else
                {
                    responseModel.ErrorMessage = statusResponse.Content;
                    responseModel.StatusCode = statusResponse.StatusCode;
                    responseModel.Status = false;
                }
            }
            else
            {
                responseModel.ErrorMessage = "AccessToken is null";
                responseModel.StatusCode = HttpStatusCode.BadRequest;
                responseModel.Status = false;
            }

            return responseModel;
        }
        public async Task<ResponseModel> GetAccessToken()
        {
            ResponseModel responseModel = new ResponseModel();

            string defaultString = string.Empty;
            string client_id = _configuration["AppSettingsAdobe:adobe_client_id"] ?? defaultString;
            string client_secret = _configuration["AppSettingsAdobe:adobe_client_secret"] ?? defaultString;
            string accessToken_AdobeURL = _configuration["AppSettingsAdobe:GetAccessToken"] ?? defaultString;
            if (!string.IsNullOrEmpty(client_id) || !string.IsNullOrEmpty(client_secret))
            {
                var Fileclient = new RestClient(accessToken_AdobeURL
                + "client_id=" + client_id + "&client_secret=" + client_secret + "&grant_type=client_credentials&scope=openid, AdobeID, DCAPI");

                var filerequest = new RestRequest(Method.POST);
                IRestResponse Fileresponse = await Fileclient.ExecuteAsync(filerequest);

                if (Fileresponse.StatusCode == HttpStatusCode.OK)
                {
                    var res = Fileresponse.Content;
                    var responseContent = JsonConvert.DeserializeObject<AccessTokenResponse>(res);
                    responseModel.Data = responseContent;
                    responseModel.StatusCode = HttpStatusCode.OK;
                    responseModel.Status = true;
                }
                else
                {
                    responseModel.ErrorMessage = Fileresponse.Content;
                    responseModel.StatusCode = HttpStatusCode.InternalServerError;
                }
            }

            return responseModel;
        }
        public async Task<ResponseModel> CombinePDF(CombinedPDFRequest combinedPDFRequestModel, string accessToken)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                string x_api_key = _configuration["AppSettingsAdobe:adobe_client_id"] ?? string.Empty;
                if (!string.IsNullOrEmpty(accessToken))
                {
                    var serial_JSON = JsonConvert.SerializeObject(combinedPDFRequestModel);
                    var Clients = new RestClient("https://pdf-services-ew1.adobe.io/operation/combinepdf");
                    var combineRequest = new RestRequest(Method.POST);
                    combineRequest.AddHeader("Authorization", "Bearer " + accessToken);
                    combineRequest.AddHeader("x-api-key", x_api_key);
                    combineRequest.AddHeader("Content-Type", "application/json");
                    combineRequest.AddParameter("application/json", serial_JSON, ParameterType.RequestBody);
                    IRestResponse combineResponse = await Clients.ExecuteAsync(combineRequest);
                    if (combineResponse.StatusCode == HttpStatusCode.Created)
                    {
                        for (int i = 0; i < combineResponse.Headers.Count; i++)
                        {
                            if (combineResponse.Headers[i].Name == "Location")
                            {
                                var location = combineResponse.Headers[i].Name == "Location" ? combineResponse.Headers[i].Value?.ToString() : string.Empty;
                                var resData = await GetCombinedPDFUrl(location, accessToken);
                                return resData;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = ex.InnerException?.ToString() + ex.StackTrace?.ToString() + ex.Message?.ToString();
                responseModel.StatusCode = HttpStatusCode.InternalServerError;
                responseModel.ErrorMessage = errorMessage;
            }
            return responseModel;
        }
        public async Task<ResponseModel> GetCombinedPDFUrl(string combinePDFLocation, string authorize)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                if (!string.IsNullOrEmpty(combinePDFLocation) && !string.IsNullOrEmpty(authorize))
                {
                    string x_api_key = _configuration["AppSettingsAdobe:adobe_client_id"] ?? string.Empty;
                    var Clients = new RestClient(combinePDFLocation);
                    var statusRequest = new RestRequest(Method.GET);
                    statusRequest.AddHeader("Authorization", "Bearer " + authorize);
                    statusRequest.AddHeader("x-api-key", x_api_key);
                    statusRequest.AddHeader("Content-Type", "application/json");
                    IRestResponse statusResponse = await Clients.ExecuteAsync(statusRequest);
                    if (statusResponse.StatusCode == HttpStatusCode.OK)
                    {
                        var res = statusResponse.Content;
                        var deserial_Response = JsonConvert.DeserializeObject<CombinedPDFResponseModel>(res);
                        if (deserial_Response.status.ToLower() == "in progress")
                        {
                            var data = await GetCombinedPDFUrl(combinePDFLocation, authorize);
                            responseModel = data;
                        }
                        else
                        {
                            responseModel.Data = deserial_Response.asset.downloadUri;
                            responseModel.PdfUrl = deserial_Response.asset.assetID;
                            var respcombine = JsonConvert.SerializeObject(deserial_Response, Formatting.Indented);
                            responseModel.StatusCode = HttpStatusCode.OK;
                        }
                    }
                    else
                    {
                        responseModel.ErrorMessage = statusResponse.Content;
                        responseModel.StatusCode = statusResponse.StatusCode;
                    }
                }
                else
                {
                    responseModel.ErrorMessage = "ErrorInfo: Combined PDF Location or access token is NULL";
                    responseModel.StatusCode = HttpStatusCode.InternalServerError;
                }
            }
            catch (Exception ex)
            {
                var errorMessage = ex.InnerException?.ToString() + ex.StackTrace?.ToString() + ex.Message?.ToString();
                responseModel.StatusCode = HttpStatusCode.InternalServerError;
                responseModel.ErrorMessage = errorMessage;
            }
            return responseModel;
        }
        public async Task<ResponseModel> GetUploadAssetUrl(string accessToken)
        {
            ResponseModel resobj = new ResponseModel();

            string defaultString = string.Empty;
            string uploadAsset_AdobeURL = _configuration["AppSettingsAdobe:GetUploadAssetUrl"] ?? defaultString;
            string x_api_key = _configuration["AppSettingsAdobe:adobe_client_id"] ?? defaultString;
            if (!string.IsNullOrEmpty(accessToken))
            {
                var Clients = new RestClient(uploadAsset_AdobeURL);
                var combineRequest = new RestRequest(Method.POST);
                combineRequest.AddHeader("Authorization", "Bearer " + accessToken);
                combineRequest.AddHeader("x-api-key", x_api_key);
                var payload = new
                {
                    mediaType = "application/pdf"
                };
                string serial_JSON = JsonConvert.SerializeObject(payload);
                combineRequest.AddParameter("application/json", serial_JSON, ParameterType.RequestBody);
                IRestResponse combineResponse = await Clients.ExecuteAsync(combineRequest);

                if (combineResponse.StatusCode == HttpStatusCode.OK)
                {
                    resobj.Data = combineResponse.Content;
                    resobj.StatusCode = HttpStatusCode.OK;
                }
            }

            return resobj;
        }
        public async Task<ResponseModel> UploadPdfFromUrl(AdobeResponse adobeResponse,string accessToken)
        {
            ResponseModel resobj = new ResponseModel();
            var resUrl = await GetUploadAssetUrl(accessToken);
            if (resUrl != null && resUrl.StatusCode == HttpStatusCode.OK)
            {
                AdobeResponse responseObj = JsonConvert.DeserializeObject<AdobeResponse>((string)resUrl?.Data);
                var uploadUri = responseObj?.uploadUri;
                var assetID = responseObj?.assetID;

                adobeResponse.uploadUri = uploadUri ?? string.Empty;
                adobeResponse.assetID = assetID ?? string.Empty;

                using (HttpClient client = new HttpClient())
                {
                    // Download the PDF file from the provided URL
                    var sourcePdfUrl = adobeResponse.AttachemntPDFcontent;
                    var pdfBytes = await client.GetByteArrayAsync(sourcePdfUrl);

                    // Upload the downloaded PDF to the specified API URL
                    using (var content = new ByteArrayContent(pdfBytes))
                    {
                        content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                        var response = await client.PutAsync(adobeResponse.uploadUri, content);
                        if (response.IsSuccessStatusCode)
                        {
                            resobj.StatusCode = HttpStatusCode.OK;
                            resobj.Data = adobeResponse.assetID;
                        }
                        else
                        {
                            resobj.StatusCode = HttpStatusCode.BadRequest;
                        }
                    }
                }
            }
            return resobj;
        }
        public async Task<ResponseModel> UploadPdfFileFromBlob(UploadPdfFile uploadPdfFile)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                if (!string.IsNullOrEmpty(uploadPdfFile.PdfUrl) && !string.IsNullOrEmpty(uploadPdfFile.FileName))
                {
                    string accountName = _configuration["BloBStorageConfig:AzureAccountName"] ?? string.Empty;
                    string connectionString = _configuration["ConnectionStrings:AzureBlobStorage"] ?? string.Empty;
                    string accountKey = _configuration["BloBStorageConfig:AzureAccountKey"] ?? string.Empty;
                    string containerName = _configuration["BloBStorageConfig:AzureContainer_Html"] ?? string.Empty;
                    string fileName = uploadPdfFile.FileName + ".pdf";
                    byte[] dataByte;
                    using (WebClient webClient = new WebClient())
                    {
                        dataByte = webClient.DownloadData(uploadPdfFile.PdfUrl);
                    }
                    var storageCredentials = new StorageCredentials(accountName, accountKey);
                    var storageAccount = new CloudStorageAccount(storageCredentials, true);
                    var connString = storageAccount.ToString(true);
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                    var container = blobClient.GetContainerReference(containerName).GetBlockBlobReference(fileName);
                    container.Properties.ContentType = "application/pdf";

                    CloudBlockBlob blob = container;
                    blob.Properties.ContentType = "application/pdf";
                    string timezone = "UTC+03:00";
                    blob.Metadata["Timezone"] = timezone;

                    await blob.DeleteIfExistsAsync();

                    await blob.UploadFromByteArrayAsync(dataByte, 0, dataByte.Length);
                    BlobSasTokenRequest request = new BlobSasTokenRequest();
                    request.AccountKey = accountKey;
                    request.AccountName = accountName;
                    request.ConnectionString = connectionString;
                    request.ContainerName = containerName;
                    request.FileName = fileName;

                    responseModel = await GetFileFromBlob(request);
                }
                else
                {
                    responseModel.ErrorMessage = "Pdf url is null or engagement letter name is null";
                    responseModel.StatusCode = HttpStatusCode.InternalServerError;
                }
            }
            catch (Exception ex)
            {
                responseModel.StatusCode = HttpStatusCode.InternalServerError;
                responseModel.ErrorMessage = "";
            }
            return responseModel;
        }
    }
}
