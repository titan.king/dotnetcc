﻿using BulkSigning.Business.Interfaces;
using BulkSigning.Data.DataModel;
using BulkSigning.Models.BulkSigning;
using BulkSigning.Models.CommonModel;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Resources;

namespace EngageCC.BulkSigningAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BulkSigningController : ControllerBase
    {
        private readonly ResourceManager _resourceManager;
        private readonly IBulkSigningService _bulkSigningService;
        public BulkSigningController(ResourceManager resourceManager, IBulkSigningService bulkSigningService)
        {
            _resourceManager = resourceManager;
            _bulkSigningService = bulkSigningService;
        }
        [HttpPost]
        [Route("ResendforESigning")]
        public async Task<ResponseModel> ResendforESigning(EngagementLetterViewModel engagementLetter)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                var getResp = await _bulkSigningService.ProceedForESigning(engagementLetter);
                if(!getResp)
                {
                    response.Status = false;
                    response.StatusCode = HttpStatusCode.Ambiguous;
                }
                else
                {
                    response.Status = true;
                    response.StatusCode = HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return response;
        }
    }
}
