﻿using BulkSigning.Business.Interfaces;
using BulkSigning.Business.Services;
using BulkSigning.Data.Interfaces;
using BulkSigning.Data.Repositories;
using Engage3.Common.Business;
using Engage3.Common.Business.Interfaces;

namespace EngageCC.BulkSigningAPI.Helper
{
    public class DependencyInjectionResolver
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IBulkSigningService, BulkSigningService>();
            services.AddScoped<IAdobeService, AdobeService>();
            services.AddScoped<IGetCurrentDatetime, GetCurrentDateTime>();
        }
        public static void RegisterRepository(IServiceCollection services)
        {
            services.AddScoped<IBulkSigningRepository, BulkSigningRepository>();
        }
    }
}
