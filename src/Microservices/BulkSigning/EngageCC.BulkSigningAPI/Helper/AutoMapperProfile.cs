﻿using AutoMapper;
using BulkSigning.Data.DataModel;
using BulkSigning.Models.BulkSigning;
using BulkSigning.Models.CommonModel;

namespace EngageCC.BulkSigningAPI.Helper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<EngagementLetterList, EngagementLetter>().ReverseMap();
            CreateMap<EngagementLetterResponse, EngagementLetter>().ReverseMap();
            CreateMap<SigningInfo, SigningInfoModel>().ReverseMap();
            CreateMap<SigningInfo, EngagementLetterResponse>().ReverseMap();
            CreateMap<DocumentStatusModel,HistoryLogModel>().ReverseMap();
            CreateMap<HistoryLogModel, HistoryLog>().ReverseMap();
            CreateMap<EngagementLetterViewModel, EngagementLetter>().ReverseMap();
            CreateMap<BulkLetterErrorLogVM, BulkLogger>().ReverseMap();
        }
    }
}
