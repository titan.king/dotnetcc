using BulkSigning.Data.Context;
using EngageCC.BulkSigningAPI.Helper;
using Microsoft.EntityFrameworkCore;
using Serilog.Events;
using Serilog;
using System.Resources;

var builder = WebApplication.CreateBuilder(args);

var configuration = new ConfigurationBuilder()
           .SetBasePath(Directory.GetCurrentDirectory())
           .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
           .Build();

var blobConnectionString = configuration?.GetConnectionString("AzureBlobStorage");

builder.Services.AddSerilog();

var containerName = "engage3cclogs";

var logger = new LoggerConfiguration()
           .WriteTo.AzureBlobStorage(
               connectionString: blobConnectionString,
               storageContainerName: containerName,
               storageFileName: "{yyyy}-{MM}-{dd}/ApplicationLogs.txt",
               restrictedToMinimumLevel: LogEventLevel.Information)
           .CreateLogger();

Log.Logger = logger;

Log.Information("This is an information log.");
Log.Error("This is an error log.");
var connectionString = configuration?.GetConnectionString("EngageDbConnection");
builder.Services.AddCors(
            options => options.AddPolicy("AllowCors",
            builder =>
            {
                builder.WithOrigins("*")
                .AllowAnyOrigin()
                .WithMethods("GET", "PUT", "POST", "DELETE")
                .AllowAnyHeader();
            }));

// Add services to the container.

builder.Services.AddControllers().AddJsonOptions(options =>
               options.JsonSerializerOptions.PropertyNamingPolicy = null);
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddDbContext<BSDBContext>(options =>
    options.UseSqlServer(connectionString));

builder.Services.AddSingleton(provider =>
{
    var builder = new DbContextOptionsBuilder<BSDBContext>();
    builder.UseSqlServer(connectionString);
    return builder.Options;
});
DependencyInjectionResolver.RegisterServices(builder.Services);
DependencyInjectionResolver.RegisterRepository(builder.Services);
builder.Services.AddAutoMapper(typeof(AutoMapperProfile));
builder.Services.AddSingleton<ResourceManager>(ServiceProvider =>
{
    return new ResourceManager("EngageCC.DashboardAPI.Helpers.Resource", typeof(Program).Assembly);
});
var app = builder.Build();

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
    app.UseSwagger();
    app.UseSwaggerUI();
//}
app.UseRouting();
app.UseHttpsRedirection();
app.UseCors("AllowCors");
app.UseAuthorization();

app.MapControllers();

app.UseSerilogRequestLogging();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.Run();
