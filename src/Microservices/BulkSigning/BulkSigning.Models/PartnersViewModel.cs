﻿namespace BulkSigning.Models
{
    public class PartnersViewModel
    {
        public int? PartnerId { get; set; }
        public string? PartnerName { get; set; } = string.Empty;
    }
}