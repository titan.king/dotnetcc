﻿namespace BulkSigning.Models.BulkSigning
{
    public class AdobeSignStatusResponseVM
    {
        public string? id { get; set; }
        public string? name { get; set; }
        public string? groupId { get; set; }
        public string? type { get; set; }
        public Participantsetsinfo[]? participantSetsInfo { get; set; }
        public string? senderEmail { get; set; }
        public DateTime? createdDate { get; set; }
        public DateTime? lastEventDate { get; set; }
        public string? signatureType { get; set; }
        public Externalid? externalId { get; set; }
        public string? locale { get; set; }
        public string? status { get; set; }
        public bool? documentVisibilityEnabled { get; set; }
        public bool? hasFormFieldData { get; set; }
        public bool? hasSignerIdentityReport { get; set; }
        public Agreementsettingsinfo? agreementSettingsInfo { get; set; }
        public string? sendType { get; set; }
        public string? senderSigns { get; set; }
        public bool? documentRetentionApplied { get; set; }
    }
}
