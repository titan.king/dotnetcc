﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkSigning.Models.BulkSigning
{
    public class InsertBulkLettersModel
    {
        public int? DocumentStatusId { get; set; }
        public int? EngageTypeId { get; set; }
        public int? DepartmentId { get; set; }
        public string? TaxYear { get; set;}
        public int? OfficeId { get; set; }
        public int? TemplateVersion { get; set; }
        public string? EngagementLetterName { get; set; }
        public int? TemplateId { get; set; }
    }
}
