﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkSigning.Models.BulkSigning
{
    public class AdobeESignResponse
    {
        public string id { get; set; }
    }
    public class SignedDocumentResponse
    {
        public string url { get; set; }
    }
}
