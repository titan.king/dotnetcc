﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkSigning.Models.BulkSigning
{
    public class UploadPdfFile
    {
        public string PdfUrl { get; set; } = string.Empty;
        public string FileName { get; set; } = string.Empty;
    }
}
