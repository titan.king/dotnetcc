﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BulkSigning.Models.BulkSigning
{
    public class SigningDocumentFields
    {
        public int EngagementLetterId { get; set; }
        public string SigningPartnerName { get; set; }
        public string SigningPartnerSignatureImage { get; set; }
        public string DelegatedName { get; set; }
        public string SPEmail { get; set; }
        public string DelegatedEmail { get; set; }
        public string ContactPerson1Name { get; set; }
        public string ContactPerson1Email { get; set; }
        public string ContactPerson2Name { get; set; }
        public string ContactPerson2Email { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string FileId { get; set; }
        public string DocumentId { get; set; }
        public string Status { get; set; }
        public string embedded_claim_url { get; set; }
        public bool IsDeleted { get; set; }
        public string IsRefreshButtonClicked { get; set; }
        public bool ManualSigning { get; set; }
        public int CreatedById { get; set; }
        public Nullable<int> ModifiedById { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string TransactionStatus { get; set; }
        public string AttachmentFile { get; set; }
        public int TaxYear { get; set; }
        public string Templatename { get; set; }
        public string UploadPDFcontent1 { get; set; }
        public byte[] UploadPDFdocument1 { get; set; }
        public string access_token { get; set; }
        public List<About> GetAbouts { get; set; }
        public string IFrameUrl { get; set; }
        public string document_hash { get; set; }
        public string code { get; set; }
        public string state { get; set; }
        public string Iframestatus { get; set; }
        public string EditedBy { get; set; }
        public string UploadedDocument { get; set; }
        public string PDFbyteArray { get; set; }
        public string PreviewQuestions { get; set; }
        public string PreviewPDFContent { get; set; }
        public string LetterHtmlContent { get; set; }
        public string EngagementLetterName { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorPopupMessage { get; set; }
        public class About
        {
            public string IFrameUrl { get; set; }
        }
        public class Redirect
        {
            public string success { get; set; }
            public string access_token { get; set; }
            public string token_type { get; set; }
            public string expires_in { get; set; }
            public string state { get; set; }
        }

        public class File
        {
            public string name { get; set; }
            public string file_id { get; set; }
            public int pages { get; set; }
            public string status { get; set; }
            public string document_hash { get; set; }
            public int id { get; set; }
            public string embedded_claim_url { get; set; }
            public string embedded_signing_url { get; set; }
        }

        public class InsideFile
        {
            public string name { get; set; }
            public string file_id { get; set; }
            public int pages { get; set; }

        }
        public class sign
        {
            public string XID { get; set; }
            public string YID { get; set; }
            public string Height { get; set; }
            public string Width { get; set; }
            public int Pageid { get; set; }

        }
        public class Signer
        {
            public int id { get; set; }
            public string name { get; set; }
            public string email { get; set; }
            public string role { get; set; }
            public int order { get; set; }
            public string pin { get; set; }
            public string message { get; set; }
            public int signed { get; set; }
            public object signed_timestamp { get; set; }
            public int required { get; set; }
            public int deliver_email { get; set; }
            public string language { get; set; }
            public int declined { get; set; }
            public int removed { get; set; }
            public int bounced { get; set; }
            public int sent { get; set; }
            public int viewed { get; set; }
            public string status { get; set; }
            public string embedded_signing_url { get; set; }
        }

        public class Recipient
        {
            public string name { get; set; }
            public string email { get; set; }
            public string role { get; set; }
            public string message { get; set; }
            public int required { get; set; }
            public string language { get; set; }
        }

        public class Log
        {
            public string @event { get; set; }
            public object signer { get; set; }
            public int timestamp { get; set; }
        }

        public class Meta
        {
            public string some_key { get; set; }
            public string another_key { get; set; }
        }

        public class CustomList
        {
            public string name { get; set; }
            public string file_id { get; set; }
            public int pages { get; set; }
        }

        public class NewRootList
        {

            public List<NewRoot> rootList { get; set; }
        }


        public class NewRoot
        {
            public string document_hash { get; set; }
            public string requester_email { get; set; }
            public string custom_requester_name { get; set; }
            public string custom_requester_email { get; set; }
            public int is_draft { get; set; }
            public int is_template { get; set; }
            public int is_completed { get; set; }
            public int is_archived { get; set; }
            public int is_deleted { get; set; }
            public int is_trashed { get; set; }
            public int is_cancelled { get; set; }
            public int is_expired { get; set; }
            public int embedded { get; set; }
            public int in_person { get; set; }
            public int embedded_signing_enabled { get; set; }
            public int flexible_signing { get; set; }
            public string permission { get; set; }
            public string template_id { get; set; }
            public string title { get; set; }
            public string subject { get; set; }
            public string message { get; set; }
            public int use_signer_order { get; set; }
            public int reminders { get; set; }
            public int require_all_signers { get; set; }
            public string redirect { get; set; }
            public string redirect_decline { get; set; }
            public string client { get; set; }
            public int created { get; set; }
            public string expires { get; set; }
            public List<File> files { get; set; }
            public List<Signer> signers { get; set; }
            public List<Recipient> recipients { get; set; }
            public List<List<object>> fields { get; set; }
            public List<Log> log { get; set; }
            public Meta meta { get; set; }
            public string embedded_claim_url { get; set; }
            public string embedded_signing_url { get; set; }
        }

        public class EngagementLetterDetails
        {
            public string code { get; set; }
            public string state { get; set; }
            public int EngagementLetterId { get; set; }
        }

        public class FlatteningModel
        {
            public string PDFContent { get; set; }
        }
    }

    public class AdobeResponse
    {
        public string uploadUri { get; set; }
        public string assetID { get; set; }
        public string AttachemntPDFcontent { get; set; }
    }
}
