﻿namespace BulkSigning.Models.BulkSigning
{
    public class PdfConversion
    {
        public string inputUrl { get; set; } = string.Empty;
        public string json { get; set; } = string.Empty;
        public bool includeHeaderFooter { get; set; }
        public PageLayout pageLayout { get; set; }
    }
    public class PageLayout
    {
        public string? pageWidth { get; set; }
        public string? pageHeight { get; set; }
    }
    public class PdfConversionRequest
    {
        public PdfConversion pdfConversion { get; set; }
    }
    public class AdobePDFStatusResponse
    {
        public string status { get; set; } = string.Empty;
        public Asset asset { get; set; }
    }
    public class Metadata
    {
        public string type { get; set; } = string.Empty;
        public int size { get; set; }
    }
    public class Asset
    {
        public Metadata metadata { get; set; }
        public string downloadUri { get; set; } = string.Empty;
        public string assetID { get; set; } = string.Empty;
    }
    public class AccessTokenResponse
    {
        public string token_type { get; set; } = string.Empty;
        public string access_token { get; set; } = string.Empty;
        public int expires_in { get; set; }
    }
}
