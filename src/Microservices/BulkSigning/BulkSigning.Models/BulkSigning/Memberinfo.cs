﻿namespace BulkSigning.Models.BulkSigning
{
    public class Memberinfo
    {
        public string? email { get; set; }
        public string? name { get; set; }
        public string? id { get; set; }
        public Securityoption? securityOption { get; set; }
    }
}
