﻿using System.Net;

namespace BulkSigning.Models.BulkSigning
{
    public class HtmlFileResponseModel
    {
        public string? HtmlUrl { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public string? Message { get; set; }
    }
}
