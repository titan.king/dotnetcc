﻿namespace BulkSigning.Models.BulkSigning
{
    public class UploadHtmlFile
    {
        public string HtmlData { get; set; } = string.Empty;
        public string FileName { get; set; } = string.Empty;
    }
}
