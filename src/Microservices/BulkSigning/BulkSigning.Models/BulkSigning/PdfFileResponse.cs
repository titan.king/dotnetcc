﻿using System.Net;

namespace BulkSigning.Models.BulkSigning
{
    public class PdfFileResponse
    {
        public string PdflUrl { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public string Message { get; set; }
    }
}
