﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulkSigning.Models.BulkSigning
{
    public class BatchJSONModel
    {
        public int? ClientId { get; set; }
        public int? BulkLettersId { get; set; }
        public int? BatchId { get; set; }
        public int? DocumentStatusId { get; set; }
        public int? EngageTypeId { get; set; }
        public int? DepartmentId { get; set; }
        public string? TaxYear { get; set; }
        public int? OfficeId { get; set; }
        public int? TemplateVersion { get; set; }
    }
}
