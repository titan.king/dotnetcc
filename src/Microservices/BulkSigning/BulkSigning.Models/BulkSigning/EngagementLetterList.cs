﻿using System.ComponentModel.DataAnnotations;

namespace BulkSigning.Models.BulkSigning
{
    public class EngagementLetterList
    {
        public int EngagementLetterId { get; set; }
        public int BatchId { get; set; }
        public string EngagementLetterName { get; set; } = string.Empty;
        public int OfficeId { get; set; }
        public string OfficeName { get; set; } = string.Empty;
        public int? YearId { get; set; }
        public string? TaxYear { get; set; }
        public int? TemplateId { get; set; }
        public string TemplateName { get; set; } = string.Empty;
        public int? TemplateVersion { get; set; }
        public int? ClientId { get; set; }
        public string ClientName { get; set; } = string.Empty;
        public int? PartnerId { get; set; }
        public string PartnerName { get; set; } = string.Empty;
        public int? AdminId { get; set; }
        public string AdminName { get; set; } = string.Empty;
        public int DocumentStatusId { get; set; }
        public string? DocumentDescription { get; set; } = string.Empty;
        public int DepartmentId { get; set; }
        public string? DepartmentName { get; set; } = string.Empty;
        public int EngageTypeId { get; set; }
        public string? EngageTypeName { get; set; } = string.Empty;
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; } = string.Empty;
        public DateTime? ModifiedOn { get; set; }
        public bool IsDeleted { get; set; } = false;
        public DateTime? DeletedOn { get; set; }
        public string DeletedBy { get; set; } = string.Empty;
        public string? ModifiedBy { get; set; } = string.Empty;
        public int BulkLettersId { get; set; }
        public bool IsProcess { get; set; } = false;
        public bool IsEnqueue { get; set; } = false;
        public bool Is7216Available { get; set; } = false;
        public int? ClientSignatureCount { get; set; }
        public bool IsNewClient { get; set; } = false;
        [MaxLength(255)]
        public string? SpouseFirstName { get; set; }
        [MaxLength(255)]
        public string? SpouseEmailId { get; set; }
        [MaxLength(255)]
        public string? SignatoryEmailId { get; set; }
        [MaxLength(255)]
        public string? SignatoryFirstName { get; set; }
    }
}
