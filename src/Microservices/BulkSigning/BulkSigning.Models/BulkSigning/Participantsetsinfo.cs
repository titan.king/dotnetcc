﻿namespace BulkSigning.Models.BulkSigning
{
    public class Participantsetsinfo
    {
        public string? id { get; set; }
        public Memberinfo[]? memberInfos { get; set; }
        public string? role { get; set; }
        public int? order { get; set; }
    }
}
