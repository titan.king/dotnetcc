﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BulkSigning.Models.CommonModel
{
    public class ResponseModel
    {
        public HttpStatusCode StatusCode { get; set; }
        public bool Status { get; set; }
        public object? Data { get; set; }
        public string? ErrorMessage { get; set; }
        public string PdfUrl { get; set; } = string.Empty;

        public ResponseModel()
        {
        }
        public ResponseModel(HttpStatusCode StatusCode)
        {
            this.StatusCode = StatusCode;
        }
        public ResponseModel(HttpStatusCode StatusCode, string ErrorMessage)
        {
            this.StatusCode = StatusCode;
            this.ErrorMessage = ErrorMessage;
        }
    }
}
