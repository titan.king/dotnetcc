﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ClientData.Models.CommonModel
{
    public class ResponseModel
    {
        public HttpStatusCode StatusCode { get; set; }
        public bool Status { get; set; }
        public object? Data { get; set; }
        public string? ErrorMessage { get; set; }
        public ResponseModel()
        {
        }

        public ResponseModel(HttpStatusCode StatusCode, string erromessage)
        {
            this.StatusCode = StatusCode;
            this.ErrorMessage = erromessage;
        }
    }
}
