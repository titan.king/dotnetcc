﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ClientData.Models.CommonModel
{
    public class ClientDetails
    {
        public object? FieldsData { get; set; }
        public object? ParentandChildData { get; set; }
        public object? PartnersList { get; set; }
        public object? PartnerandClientMappingData { get; set; }

    }

}
