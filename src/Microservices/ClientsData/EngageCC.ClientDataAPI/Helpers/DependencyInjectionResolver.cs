﻿using ClientData.Business.Interface;
using ClientData.Business.Services;
using ClientData.Data.Interfaces;
using ClientData.Data.Repositories;

namespace EngageCC.ClientDataAPI.Helpers
{
    public class DependencyInjectionResolver
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IClientDataService, ClientDataService>();

        }

        public static void RegisterRepository(IServiceCollection services)
        {
            services.AddScoped<IClientDataRepository, ClientDataRepository>();
        }
    }
}
