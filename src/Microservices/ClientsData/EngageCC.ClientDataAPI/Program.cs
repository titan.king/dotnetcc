using EngageCC.ClientDataAPI.Helpers;
using EngageCC.ClientDataAPI.Middleware;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

var configuration = new ConfigurationBuilder()
           .SetBasePath(Directory.GetCurrentDirectory())
           .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
           .Build();

var connectionString = configuration?.GetConnectionString("StarDWHConnection");

builder.Services.AddCors(
            options => options.AddPolicy("AllowCors",
            builder =>
            {
                builder
                .AllowAnyOrigin()
                .WithMethods("GET", "PUT", "POST", "DELETE")
                .AllowAnyHeader();
            }));

builder.Services.AddControllers().AddJsonOptions(options =>
               options.JsonSerializerOptions.PropertyNamingPolicy = null);

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
DependencyInjectionResolver.RegisterServices(builder.Services);
DependencyInjectionResolver.RegisterRepository(builder.Services);
var app = builder.Build();

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
    app.UseSwagger();
    app.UseSwaggerUI();
//}

app.UseHttpsRedirection();
app.UseCors("AllowCors");
app.UseMiddleware<ApiKeyMiddleware>();

app.MapControllers();

app.Run();
