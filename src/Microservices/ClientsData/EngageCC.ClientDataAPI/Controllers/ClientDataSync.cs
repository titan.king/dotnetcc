﻿using AutoMapper;
using ClientData.Business.Interface;
using ClientData.Business.Services;
using ClientData.Models.CommonModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Resources;

namespace EngageCC.ClientDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientDataSync : Controller
    {
        private readonly IClientDataService _clientDataService;
        public ClientDataSync(IClientDataService clientDataService)
        {
            _clientDataService = clientDataService;
        }
        ResponseModel resObj = new ResponseModel();

        [HttpGet]
        [Route("Fields")]
        public async Task<ResponseModel> FieldsData()
        {
            try
            {
                var fetchclientdata = await _clientDataService.FieldsData();
                resObj.Data = fetchclientdata;
                resObj.Status = true;
                resObj.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                //_logger.LogError(ex, _resourceManager.GetString("ErrorMessage"));
                ModelState.AddModelError("SQL Error", ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return resObj;
        }


        [HttpGet]
        [Route("ParentandChildData")]
        public async Task<ResponseModel> GetParentandChildData()
        {
            try
            {
                var fetchclientdata = await _clientDataService.GetParentandChildData();
                resObj.Data = fetchclientdata;
                resObj.Status = true;
                resObj.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                //_logger.LogError(ex, _resourceManager.GetString("ErrorMessage"));
                ModelState.AddModelError("SQL Error", ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return resObj;
        }
        [HttpGet]
        [Route("ParentChildData")]
        public async Task<ResponseModel> GetParentChildData()
        {
            try
            {
                var fetchclientdata = await _clientDataService.GetParentChildData();
                resObj.Data = fetchclientdata;
                resObj.Status = true;
                resObj.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                //_logger.LogError(ex, _resourceManager.GetString("ErrorMessage"));
                ModelState.AddModelError("SQL Error", ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return resObj;
        }

        [HttpGet]
        [Route("Partners")]
        public async Task<ResponseModel> GetPartnersList()
        {
            try
            {
                var fetchclientdata = await _clientDataService.GetPartnersList();
                resObj.Data = fetchclientdata;
                resObj.Status = true;
                resObj.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                //_logger.LogError(ex, _resourceManager.GetString("ErrorMessage"));
                ModelState.AddModelError("SQL Error", ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return resObj;
        }

        [HttpGet]
        [Route("PartnerandClientMapping")]
        public async Task<ResponseModel> GetPartnerandClientMappingData()
        {
            try
            {
                var fetchclientdata = await _clientDataService.GetPartnerandClientMappingData();
                resObj.Data = fetchclientdata;
                resObj.Status = true;
                resObj.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                //_logger.LogError(ex, _resourceManager.GetString("ErrorMessage"));
                ModelState.AddModelError("SQL Error", ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return resObj;
        }
        [HttpGet]
        [Route("PartnerandOffice")]
        public async Task<ResponseModel> GetPartnerandOfficeData()
        {
            try
            {
                var fetchclientdata = await _clientDataService.GetPartnerandofficeData();
                resObj.Data = fetchclientdata;
                resObj.Status = true;
                resObj.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                //_logger.LogError(ex, _resourceManager.GetString("ErrorMessage"));
                ModelState.AddModelError("SQL Error", ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return resObj;
        }

        [HttpGet]
        [Route("OfficeDetails")]
        public async Task<ResponseModel> GetOfficeDetails()
        {
            try
            {
                var fetchclientdata = await _clientDataService.GetOfficeDetails();
                resObj.Data = fetchclientdata;
                resObj.Status = true;
                resObj.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                //_logger.LogError(ex, _resourceManager.GetString("ErrorMessage"));
                ModelState.AddModelError("SQL Error", ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return resObj;
        }
        [HttpGet]
        [Route("ClientDetails")]
        public async Task<ResponseModel> GetClientDetails()
        {
            try
            {
                var fetchclientdata = await _clientDataService.GetClientDetails();
                resObj.Data = fetchclientdata;
                resObj.Status = true;
                resObj.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                //_logger.LogError(ex, _resourceManager.GetString("ErrorMessage"));
                ModelState.AddModelError("SQL Error", ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return resObj;
        }
    }
}
