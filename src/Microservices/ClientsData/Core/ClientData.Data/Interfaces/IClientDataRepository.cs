﻿using ClientData.Models.CommonModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientData.Data.Interfaces
{
    public interface IClientDataRepository
    {
        Task<FieldsData> FieldsData();
        Task<ParentandChildData> GetParentandChildData();
        Task<ParentChildData> GetParentChildData();
        Task<PartnersData> GetPartnersList();
        Task<PartnerandClientMappingData> GetPartnerandClientMappingData();
        Task<PartnerandofficeMapping> GetPartnerandofficeData();
        Task<OfficeDetails> GetOfficeDetails();
        Task<LoadClientDetails> GetClientDetails();
    }
}
