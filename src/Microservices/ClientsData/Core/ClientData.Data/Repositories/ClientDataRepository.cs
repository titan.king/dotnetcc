﻿using ClientData.Data.Interfaces;
using ClientData.Models.CommonModel;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientData.Data.Repositories
{
    public class ClientDataRepository : IClientDataRepository
    {
        public async Task<FieldsData> FieldsData()
        {
            var clientDetails = new FieldsData();
            try
            {
                List<Dictionary<string, object>> records = new List<Dictionary<string, object>>();
                List<string> Lettersdata = new List<string>();
                List<string> newlettersdata = new List<string>();

                var getqueries = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("Queries.json", optional: true, reloadOnChange: true)
                    .Build();


                var configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                    .Build();

                var connectionString = configuration?.GetConnectionString("T2DCitrinConnection");
                string? script = getqueries["queries:GetFieldsData"];

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    //var sqlstring = "Select * from EngagementLetter";
                    
                    //var sqlstring = "SELECT c.client_first_name ClientFirstName,  c.spouse_first_name Spouse, c.client_last_name ClientLastName, c.signer_name SIGNING, c.preparer_name OFFICER,  c.spouse_last_name Lastname_Spouse,  c.client_first_name + ' ' + isnull(c.client_last_name,'') TrustEsatate_Name,  isNull(rtrim(substring(isNull(c.trustee_name,''), 1, charindex(' ', c.trustee_name))),'') TrusteeFiduciary_Name,  isNull(SUBSTRING(c.trustee_name, CHARINDEX('', c.trustee_name) + 1, LEN(c.trustee_name) - CHARINDEX(' ', c.trustee_name)),'') TrusteeFiduciary_Last_Name,  c.trustee_title TrusteeFiduciary_Title,  c.year_end Year,  c.client_first_name + ' ' + isnull(c.client_last_name,'') TrustName,  c.client_first_name + ' ' + isnull(c.client_last_name,'') Trust,  isNull(c.client_first_name,'') + ' ' + isnull(c.client_last_name,'') HusbandFullName,  isnull(c.spouse_first_name,'') + ' ' + isnull(c.spouse_last_name,'') WifeFullName,  c.client_first_name + ' ' + isnull(c.client_last_name,'') EstateName,  c.client_first_name + ' ' + isnull(c.client_last_name,'') Trust_Name,  tab1.AddressDetail Street_Address,  isnull(tab1.city, '') + ', ' + isnull(tab1.County, '') + ' ' + isnull(tab1.PostCode, '') City_Address,  tab1.City City,  tab1.Country State, tab1.SearchName Business_Name, tab1.ClientID ClientID, tab1.SearchName NameofClients, tab1.AddressDetail AddressofClients, tab1.SearchName NameofClient, tab1.SearchName CLIENTNAME, tab1.AddressDetail ADDRESS1, tab1.AddressDetail ADDRESS2, tab1.OfficeID OfficeLocation FROM [T2DCitrin].[dbo].[tblClient] tab1 INNER JOIN [StarDWH].[dbo].[t_axcess_tax_returns] c ON Convert (Varchar(200), tab1.ClientID) = c.client_id";
                    using (SqlCommand command = new SqlCommand(script, connection))
                    {
                        using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                        {
                            DataTable dataTable = new DataTable();
                            adapter.Fill(dataTable);

                            // Iterate through rows and convert each row to a dictionary
                            foreach (DataRow row in dataTable.Rows)
                            {
                                var record = new Dictionary<string, object>();

                                foreach (DataColumn column in dataTable.Columns)
                                {
                                    string columnName = column.ColumnName;
                                    string columnValue = row[column].ToString();

                                    record[columnName] = columnValue;
                                }

                                records.Add(record);
                            }

                            adapter.Dispose();
                        }
                        clientDetails.DataList = records;
                    }
                }
                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return clientDetails;
        }
        public async Task<ParentandChildData> GetParentandChildData()
        {
            var clientDetails = new ParentandChildData();
            try
            {
                List<Dictionary<string, object>> records = new List<Dictionary<string, object>>();
                List<string> Lettersdata = new List<string>();
                List<string> newlettersdata = new List<string>();

                var getqueries = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("Queries.json", optional: true, reloadOnChange: true)
                    .Build();


                var configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                    .Build();

                var connectionString = configuration?.GetConnectionString("T2DCitrinConnection");
                string? script = getqueries["queries:GetParentandChildData"];

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    //var sqlstring = "Select * from EngagementLetter";
                    //var sqlstring = "select * from v_cc_engagement_letter_associated_clients";
                    using (SqlCommand command = new SqlCommand(script, connection))
                    {
                        using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                        {
                            DataTable dataTable = new DataTable();
                            adapter.Fill(dataTable);

                            // Iterate through rows and convert each row to a dictionary
                            foreach (DataRow row in dataTable.Rows)
                            {
                                var record = new Dictionary<string, object>();

                                foreach (DataColumn column in dataTable.Columns)
                                {
                                    string columnName = column.ColumnName;
                                    string columnValue = row[column].ToString();

                                    record[columnName] = columnValue;
                                }

                                records.Add(record);
                            }

                            adapter.Dispose();
                        }
                        clientDetails.DataList = records;
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return clientDetails;
        }
        public async Task<ParentChildData> GetParentChildData()
        {
            var clientDetails = new ParentChildData();
            try
            {
                List<Dictionary<string, object>> records = new List<Dictionary<string, object>>();
                List<string> Lettersdata = new List<string>();
                List<string> newlettersdata = new List<string>();

                var getqueries = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("Queries.json", optional: true, reloadOnChange: true)
                    .Build();


                var configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                    .Build();

                var connectionString = configuration?.GetConnectionString("T2DCitrinConnection");
                string? script = getqueries["queries:GetParentChildData"];

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    //var sqlstring = "Select * from EngagementLetter";
                    //var sqlstring = "select * from v_cc_engagement_letter_associated_clients";
                    using (SqlCommand command = new SqlCommand(script, connection))
                    {
                        using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                        {
                            DataTable dataTable = new DataTable();
                            adapter.Fill(dataTable);

                            // Iterate through rows and convert each row to a dictionary
                            foreach (DataRow row in dataTable.Rows)
                            {
                                var record = new Dictionary<string, object>();

                                foreach (DataColumn column in dataTable.Columns)
                                {
                                    string columnName = column.ColumnName;
                                    string columnValue = row[column].ToString();

                                    record[columnName] = columnValue;
                                }

                                records.Add(record);
                            }

                            adapter.Dispose();
                        }
                        clientDetails.DataList = records;
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return clientDetails;
        }
        public async Task<PartnersData> GetPartnersList()
        {
            var clientDetails = new PartnersData();
            try
            {
                List<Dictionary<string, object>> records = new List<Dictionary<string, object>>();
                List<string> Lettersdata = new List<string>();
                List<string> newlettersdata = new List<string>();

                var getqueries = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("Queries.json", optional: true, reloadOnChange: true)
                    .Build();
                

                var configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                    .Build();

                var connectionString = configuration?.GetConnectionString("T2DCitrinConnection");
                string? script = getqueries["queries:GetPartnersList"];
                //string script = "Select * from EngagementLetter";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                   // var sqlstring = "Select DISTINCT  m.ManagerID PartnerID,m.Name PartnerName From tblClient c Join v_cc_engagement_letter_associated_clients p on c.ClientID = p.parent_client_id Join tblManager m on c.ManagerID = m.ManagerID Join tblCliContactOwnership o on c.ClientID = o.ClientID Join tblContacts co on o.ContactID = co.ContactID ORDER BY PartnerName asc";
                    using (SqlCommand command = new SqlCommand(script, connection))
                    {
                        using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                        {
                            DataTable dataTable = new DataTable();
                            adapter.Fill(dataTable);

                            // Iterate through rows and convert each row to a dictionary
                            foreach (DataRow row in dataTable.Rows)
                            {
                                var record = new Dictionary<string, object>();

                                foreach (DataColumn column in dataTable.Columns)
                                {
                                    string columnName = column.ColumnName;
                                    string columnValue = row[column].ToString();

                                    record[columnName] = columnValue;
                                }

                                records.Add(record);
                            }

                            adapter.Dispose();
                        }
                        clientDetails.DataList = records;
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return clientDetails;
        }

        public async Task<PartnerandClientMappingData> GetPartnerandClientMappingData()
        {
            var clientDetails = new PartnerandClientMappingData();
            try
            {
                List<Dictionary<string, object>> records = new List<Dictionary<string, object>>();

                var getqueries = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("Queries.json", optional: true, reloadOnChange: true)
                    .Build();


                var configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                    .Build();

                var connectionString = configuration?.GetConnectionString("T2DCitrinConnection");
                string? script = getqueries["queries:GetPartnerandClientMappingData"];

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    //var sqlstring = "Select * from EngagementLetter";
                    //var sqlstring = "Select m.ManagerID PartnerID ,m.Name PartnerName, p.parent_client_id ParentClientID,c.SearchName ParentClientName, p.child1 ChildClient, c.AddressDetail, c.City,c.County State, c.PostCode ZipCode, isNull(co.FirstName,'') + ' ' + isNull(co.SecondName,'') ContactName , co.Email ContactEmail From tblClient c Join v_cc_engagement_letter_associated_clients p on c.ClientID = p.parent_client_id Join tblManager m on c.ManagerID = m.ManagerID Join tblCliContactOwnership o on c.ClientID = o.ClientID Join tblContacts co on o.ContactID = co.ContactID";
                    using (SqlCommand command = new SqlCommand(script, connection))
                    {
                        using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                        {
                            DataTable dataTable = new DataTable();
                            adapter.Fill(dataTable);

                            // Iterate through rows and convert each row to a dictionary
                            foreach (DataRow row in dataTable.Rows)
                            {
                                var record = new Dictionary<string, object>();

                                foreach (DataColumn column in dataTable.Columns)
                                {
                                    string columnName = column.ColumnName;
                                    string columnValue = row[column].ToString();

                                    record[columnName] = columnValue;
                                }

                                records.Add(record);
                            }

                            adapter.Dispose();
                        }
                        clientDetails.DataList = records;
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return clientDetails;
        }
        public async Task<PartnerandofficeMapping> GetPartnerandofficeData()
        {
            var clientDetails = new PartnerandofficeMapping();
            try
            {
                List<Dictionary<string, object>> records = new List<Dictionary<string, object>>();

                var getqueries = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("Queries.json", optional: true, reloadOnChange: true)
                    .Build();


                var configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                    .Build();

                var connectionString = configuration?.GetConnectionString("T2DCitrinConnection");
                string? script = getqueries["queries:GetPartnerandOfficeData"];

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    //var sqlstring = "Select * from EngagementLetter";
                    //var sqlstring = "Select m.ManagerID PartnerID ,m.Name PartnerName, p.parent_client_id ParentClientID,c.SearchName ParentClientName, p.child1 ChildClient, c.AddressDetail, c.City,c.County State, c.PostCode ZipCode, isNull(co.FirstName,'') + ' ' + isNull(co.SecondName,'') ContactName , co.Email ContactEmail From tblClient c Join v_cc_engagement_letter_associated_clients p on c.ClientID = p.parent_client_id Join tblManager m on c.ManagerID = m.ManagerID Join tblCliContactOwnership o on c.ClientID = o.ClientID Join tblContacts co on o.ContactID = co.ContactID";
                    using (SqlCommand command = new SqlCommand(script, connection))
                    {
                        using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                        {
                            DataTable dataTable = new DataTable();
                            adapter.Fill(dataTable);

                            // Iterate through rows and convert each row to a dictionary
                            foreach (DataRow row in dataTable.Rows)
                            {
                                var record = new Dictionary<string, object>();

                                foreach (DataColumn column in dataTable.Columns)
                                {
                                    string columnName = column.ColumnName;
                                    string columnValue = row[column].ToString();

                                    record[columnName] = columnValue;
                                }

                                records.Add(record);
                            }

                            adapter.Dispose();
                        }
                        clientDetails.DataList = records;
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return clientDetails;
        }
        public async Task<OfficeDetails> GetOfficeDetails()
        {
            var clientDetails = new OfficeDetails();
            try
            {
                List<Dictionary<string, object>> records = new List<Dictionary<string, object>>();

                var getqueries = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("Queries.json", optional: true, reloadOnChange: true)
                    .Build();


                var configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                    .Build();

                var connectionString = configuration?.GetConnectionString("CCHAccessConnection");
                string? script = getqueries["queries:GetOfficeDetails"];

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    //var sqlstring = "Select * from EngagementLetter";
                    //var sqlstring = "Select m.ManagerID PartnerID ,m.Name PartnerName, p.parent_client_id ParentClientID,c.SearchName ParentClientName, p.child1 ChildClient, c.AddressDetail, c.City,c.County State, c.PostCode ZipCode, isNull(co.FirstName,'') + ' ' + isNull(co.SecondName,'') ContactName , co.Email ContactEmail From tblClient c Join v_cc_engagement_letter_associated_clients p on c.ClientID = p.parent_client_id Join tblManager m on c.ManagerID = m.ManagerID Join tblCliContactOwnership o on c.ClientID = o.ClientID Join tblContacts co on o.ContactID = co.ContactID";
                    using (SqlCommand command = new SqlCommand(script, connection))
                    {
                        using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                        {
                            DataTable dataTable = new DataTable();
                            adapter.Fill(dataTable);

                            // Iterate through rows and convert each row to a dictionary
                            foreach (DataRow row in dataTable.Rows)
                            {
                                var record = new Dictionary<string, object>();

                                foreach (DataColumn column in dataTable.Columns)
                                {
                                    string columnName = column.ColumnName;
                                    string columnValue = row[column].ToString();

                                    record[columnName] = columnValue;
                                }

                                records.Add(record);
                            }

                            adapter.Dispose();
                        }
                        clientDetails.DataList = records;
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return clientDetails;
        }
        public async Task<LoadClientDetails> GetClientDetails()
        {
            var clientDetails = new LoadClientDetails();
            try
            {
                List<Dictionary<string, object>> records = new List<Dictionary<string, object>>();

                var getqueries = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("Queries.json", optional: true, reloadOnChange: true)
                    .Build();


                var configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                    .Build();

                var connectionString = configuration?.GetConnectionString("CCHAccessConnection");
                string? script = getqueries["queries:GetClientsList"];

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    //var sqlstring = "Select * from EngagementLetter";
                    //var sqlstring = "Select m.ManagerID PartnerID ,m.Name PartnerName, p.parent_client_id ParentClientID,c.SearchName ParentClientName, p.child1 ChildClient, c.AddressDetail, c.City,c.County State, c.PostCode ZipCode, isNull(co.FirstName,'') + ' ' + isNull(co.SecondName,'') ContactName , co.Email ContactEmail From tblClient c Join v_cc_engagement_letter_associated_clients p on c.ClientID = p.parent_client_id Join tblManager m on c.ManagerID = m.ManagerID Join tblCliContactOwnership o on c.ClientID = o.ClientID Join tblContacts co on o.ContactID = co.ContactID";
                    using (SqlCommand command = new SqlCommand(script, connection))
                    {
                        using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                        {
                            DataTable dataTable = new DataTable();
                            adapter.Fill(dataTable);

                            // Iterate through rows and convert each row to a dictionary
                            foreach (DataRow row in dataTable.Rows)
                            {
                                var record = new Dictionary<string, object>();

                                foreach (DataColumn column in dataTable.Columns)
                                {
                                    string columnName = column.ColumnName;
                                    string columnValue = row[column].ToString();

                                    record[columnName] = columnValue;
                                }

                                records.Add(record);
                            }

                            adapter.Dispose();
                        }
                        clientDetails.DataList = records;
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return clientDetails;
        }

    }
}
