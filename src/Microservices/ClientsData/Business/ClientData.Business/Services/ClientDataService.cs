﻿using AutoMapper;
using ClientData.Business.Interface;
using ClientData.Data.Interfaces;
using ClientData.Models.CommonModel;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;


namespace ClientData.Business.Services
{
    public class ClientDataService : IClientDataService
    {
        private readonly IClientDataRepository _clientDataRepository;
        
        public ClientDataService(IClientDataRepository clientDataRepository)
        {
            _clientDataRepository = clientDataRepository;
        }
        public async Task<FieldsData> FieldsData()
        {
            return await _clientDataRepository.FieldsData();
            
        }
        public async Task<ParentandChildData> GetParentandChildData()
        {
            return await _clientDataRepository.GetParentandChildData();

        }
        public async Task<ParentChildData> GetParentChildData()
        {
            return await _clientDataRepository.GetParentChildData();

        }
        public async Task<PartnersData> GetPartnersList()
        {
            return await _clientDataRepository.GetPartnersList();

        }
        public async Task<PartnerandClientMappingData> GetPartnerandClientMappingData()
        {
            return await _clientDataRepository.GetPartnerandClientMappingData();

        }
        public async Task<PartnerandofficeMapping> GetPartnerandofficeData()
        {
            return await _clientDataRepository.GetPartnerandofficeData();

        }
        public async Task<OfficeDetails> GetOfficeDetails()
        {
            return await _clientDataRepository.GetOfficeDetails();

        }
        public async Task<LoadClientDetails> GetClientDetails()
        {
            return await _clientDataRepository.GetClientDetails();

        }
    }
}
