﻿using AutoMapper;
using Dashboard.Data.Context;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.Clients;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Fields;
using Engage3.Common.Business.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Dashboard.Data.Repositories
{
    public class ClientRepository : IClientRepository
    {
        private readonly IConfiguration _configuration;
        private readonly DashboardDBContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IGetCurrentDatetime _getCurrentDateTime;
        int TakeValue = 100;
        private readonly DbContextOptions<DashboardDBContext> _dbContextOptions;
        public ClientRepository(DashboardDBContext dbContext, IConfiguration configuration, IMapper mapper, DbContextOptions<DashboardDBContext> dbContextOptions, IGetCurrentDatetime getCurrentDatetime)
        {
            _dbContext = dbContext;
            _configuration = configuration;
            _mapper = mapper;
            _dbContextOptions = dbContextOptions;
            _getCurrentDateTime = getCurrentDatetime;
        }
        public async Task<BulkLettersResponse> AddClientBulkDetails(BulkLettersResponse bulkLetters)
        {
            var returnTypeCode = _dbContext.BulkLetters.FirstOrDefault(x => x.PartnerId == bulkLetters.PartnerId && x.BatchId == bulkLetters.BatchId)?.ReturnTypeCode;
            if (returnTypeCode != null)
            {
                bulkLetters.ReturnTypeCode = returnTypeCode;

                var insertData = _mapper.Map<BulkLetters>(bulkLetters);

                if (insertData != null)
                {
                    using (var _dbContext = new DashboardDBContext(_dbContextOptions))
                    {
                        await _dbContext.BulkLetters.AddAsync(insertData);
                        await _dbContext.SaveChangesAsync();
                        return bulkLetters;
                    }
                }
            }
            else
            {
                bulkLetters.ReturnTypeCode = null;
                return bulkLetters;
            }
            return bulkLetters;
        }
        public async Task<bool> IsNotExistingClientId(int clientId, int batchId)
        {
            bool result = false;
            using (var _dbContext = new DashboardDBContext(_dbContextOptions))
            {
                int existingclientIdCount = await _dbContext.BulkLetters.Where(n => n.ClientId == clientId
            && n.BatchId == batchId
            && !n.IsDeleted).CountAsync();
                if (existingclientIdCount == 0)
                {
                    result = true;
                }
                else { result = false; }
            }
            return result;
        }
        public async Task<bool> IsNotExistingClientName(string clientName, int batchId)
        {
            bool result = false;
            using (var _dbContext = new DashboardDBContext(_dbContextOptions))
            {
                int existingclientIdCount = await _dbContext.BulkLetters.Where(n => n.ClientName == clientName
            && n.BatchId == batchId
            && !n.IsDeleted).CountAsync();
                if (existingclientIdCount == 0)
                {
                    result = true;
                }
                else { result = false; }
            }
            return result;
        }
        public async Task<List<LetterFieldValuesResponse>> AddFieldIdtoList(List<LetterFieldValuesResponse> letterFieldValues)
        {
            List<LetterFieldValuesResponse> letterField = new List<LetterFieldValuesResponse>();
            if (letterFieldValues.Count > 0 && letterFieldValues != null)
            {
                using (var _dbContext = new DashboardDBContext(_dbContextOptions))
                {
                    foreach (var eachField in letterFieldValues)
                    {
                        int? fieldID = _dbContext.Field.FirstOrDefault(x => x.DisplayName == eachField.FieldName && !x.IsDeleted)?.FieldId;
                        eachField.FieldId = fieldID;
                        if (fieldID == null)
                        {
                            int partnerId = eachField.PartnerId ?? 0;
                            if (partnerId != 0)
                            {
                                int partnerOfficeFieldId = await (
                                                            from a in _dbContext.PartnerOfficeInfo
                                                            join b in _dbContext.OfficeInfo on a.OfficeId equals b.OfficeId
                                                            where a.PartnerId == partnerId
                                                            select b.OfficeId).FirstOrDefaultAsync();
                                eachField.FieldId = partnerOfficeFieldId;
                            }

                        }
                    }
                    letterField = letterFieldValues;
                }
            }
            return letterField;
        }
        public async Task<bool> SaveClientData(List<ClientData> clientdata)
        {
            clientdata = clientdata.DistinctBy(a => a.ClientId).ToList();

            List<ClientData> dataToUpdate = new List<ClientData>();
            List<ClientData> dataToAdd = new List<ClientData>();
            var listData = await _dbContext.ClientData.AsNoTracking().ToListAsync();

            if (listData.Any())
            {
                dataToUpdate = clientdata.Except(listData, new ClientDataComparer()).Where(a => listData.Select(x => x.ClientId).Contains(a.ClientId))
                      .Select(b =>
                      {
                          b.ClientId = listData.FirstOrDefault(a => a.ClientId == b.ClientId).ClientId;
                          return b;
                      }).ToList();

                dataToAdd = clientdata.Except(listData, new ClientDataComparer()).Where(a => !listData.Select(x => x.ClientId).Contains(a.ClientId)).ToList();

                if (dataToAdd.Any())
                {
                    await _dbContext.ClientData.AddRangeAsync(dataToAdd);
                }
                if (dataToUpdate.Any())
                {
                    _dbContext.ClientData.UpdateRange(dataToUpdate);
                }
            }
            else
            {
                await _dbContext.ClientData.AddRangeAsync(clientdata);
            }
            await _dbContext.SaveChangesAsync();
            return true;
        }
        public async Task<IEnumerable<t_engletterdata>> GetPartnersNameAsync()
        {
            List<t_engletterdata> partners = new List<t_engletterdata>();
            using (var _dbContext = new DashboardDBContext(_dbContextOptions))
            {
                partners = await _dbContext.t_engletterdata
                                        .Where(c => !string.IsNullOrEmpty(c.PartnerId))
                                        .Select(c => new t_engletterdata { PartnerId = c.PartnerId, PartnerName = c.PartnerName })
                                        .Distinct()
                                        .OrderBy(c => c.PartnerName)
                                        .ToListAsync();
                return partners;
            }
        }
        public async Task<LetterFieldValuesBase> FetchingClientAsync(PartnerRequest partnerRequest)
        {
            LetterFieldValuesBase letterFieldValuesBase = new LetterFieldValuesBase();
            List<BulkLettersResponse> bulkLettersResponse = new List<BulkLettersResponse>();
            List<ExportDataResponse> exportDataResponses = new List<ExportDataResponse>();
            List<BulkLettersResponse> responseList = new List<BulkLettersResponse>();
            List<t_engletterdata> engletterdata = new List<t_engletterdata>();
            string taxYear = _configuration["TaxYear"] ?? string.Empty;
            using (var _dbContext = new DashboardDBContext(_dbContextOptions))
            {
                DocumentStatus? documentStatus = await _dbContext.DocumentStatus.AsNoTracking().FirstOrDefaultAsync(a => a.DocumentStatusId == partnerRequest.DocumentStatusId);
                Template? template = await _dbContext.Template.AsNoTracking().FirstOrDefaultAsync(a => a.TemplateName == partnerRequest.TemplateName);

                if (partnerRequest.PartnersName != null && partnerRequest.ReturnTpeCode != null)
                {

                    var partnerIds = partnerRequest.PartnersName.Select(p => p.PartnerId).ToList();
                    var returnTpes = partnerRequest.ReturnTpeCode.Select(p => p.ReturnType).ToList();
                    bool isLive = Convert.ToBoolean(_configuration["IsLive"]);
                    var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();


                    if (partnerRequest.SignatureCounts == 2)
                    {
                        engletterdata = await _dbContext.t_engletterdata
                                                              .Where(a => partnerIds.Contains(a.PartnerId)
                                                              && a.Single_Joint.ToLower() == "joint")
                                                              .ToListAsync();
                    }
                    else
                    {
                        engletterdata = await _dbContext.t_engletterdata
                                       .Where(a => partnerIds.Contains(a.PartnerId)
                                                   && (a.Single_Joint.ToLower() == "single" || string.IsNullOrEmpty(a.Single_Joint)))
                                       .ToListAsync();
                    }

                    if (engletterdata.Any())
                    {
                        bulkLettersResponse = engletterdata.Where(a => returnTpes.Any(code => a.ELTemplate != null
                        && Regex.IsMatch(a.ELTemplate, $@"\b{Regex.Escape(code)}\b", RegexOptions.IgnoreCase)))
                                                      .Select(joinedData => new BulkLettersResponse
                                                      {
                                                          PartnerName = (!string.IsNullOrEmpty(joinedData.PartnerName) && joinedData.PartnerName != "NULL") ? joinedData.PartnerName : string.Empty,
                                                          OfficeId = !string.IsNullOrEmpty(joinedData.OfficeId) ? Convert.ToInt32(joinedData.OfficeId) : 0,
                                                          Office = joinedData.OfficeName,
                                                          ClientId = !string.IsNullOrEmpty(joinedData.ClientId) ? Convert.ToInt32(joinedData.ClientId) : 0,
                                                          ClientName = joinedData.ClientName,
                                                          SignatoryEmailId = (!string.IsNullOrEmpty(joinedData.SignatoryEmail) && joinedData.SignatoryEmail != "NULL") ? joinedData.SignatoryEmail : string.Empty,
                                                          SignatoryFirstName = (!string.IsNullOrEmpty(joinedData.SignatoryFirstName) && joinedData.SignatoryFirstName != "NULL") ? joinedData.SignatoryFirstName : string.Empty,
                                                          SignatoryLastName = (!string.IsNullOrEmpty(joinedData.SignatoryLastName) && joinedData.SignatoryLastName != "NULL") ? joinedData.SignatoryLastName : string.Empty,
                                                          SignatoryTitle = (!string.IsNullOrEmpty(joinedData.SignatoryTitle) && joinedData.SignatoryTitle != "NULL") ? joinedData.SignatoryTitle : string.Empty,
                                                          SpouseEmailId = (!string.IsNullOrEmpty(joinedData.SpouseEmail) && joinedData.SpouseEmail != "NULL") ? joinedData.SpouseEmail : string.Empty,
                                                          SpouseFirstName = (!string.IsNullOrEmpty(joinedData.SpouseFirstName) && joinedData.SpouseFirstName != "NULL") ? joinedData.SpouseFirstName : string.Empty,
                                                          SpouseLastName = (!string.IsNullOrEmpty(joinedData.SpouseLastName) && joinedData.SpouseLastName != "NULL") ? joinedData.SpouseLastName : string.Empty,
                                                          PartnerId = !string.IsNullOrEmpty(joinedData.PartnerId) ? Convert.ToInt32(joinedData.PartnerId) : 0,
                                                          BatchId = partnerRequest.BatchId,
                                                          TemplateName = partnerRequest.TemplateName,
                                                          DocumentStatusName = documentStatus.Description,
                                                          IsDraft = true,
                                                          IsEsigning = true,
                                                          CreatedBy = partnerRequest.Admin,
                                                          CreatedOn = estTime,
                                                          ModifiedOn = estTime,
                                                          FieldJson = partnerRequest.FieldJson,
                                                          Is7216Available = template.Is7216Available == false ? false :
                                                          template.Is7216Available == true && (string.IsNullOrEmpty(joinedData.Expiration7216Form) || joinedData.Expiration7216Form.ToLower() == "null")
                                                          ? true : false,
                                                          IsCanabisAvailable = false,
                                                          ClientSignatureCount = template.ClientSignatureCount,
                                                          TaxYear = taxYear,
                                                          IsBatchActive = true,
                                                          ModifiedBy = partnerRequest.Admin,
                                                          letterFieldValues = new List<LetterFieldValuesResponse>(),
                                                          OfficePhoneNumber = (!string.IsNullOrEmpty(joinedData.OfficePhoneNumber) && joinedData.OfficePhoneNumber != "NULL") ? joinedData.OfficePhoneNumber : string.Empty,
                                                          OfficeZipCode = (!string.IsNullOrEmpty(joinedData.OfficeZipCode) && joinedData.OfficeZipCode != "NULL") ? joinedData.OfficeZipCode : string.Empty,
                                                          OfficeState = (!string.IsNullOrEmpty(joinedData.OfficeState) && joinedData.OfficeState != "NULL") ? joinedData.OfficeState : string.Empty,
                                                          OfficeCity = (!string.IsNullOrEmpty(joinedData.OfficeCity) && joinedData.OfficeCity != "NULL") ? joinedData.OfficeCity : string.Empty,
                                                          OfficeAddress = (!string.IsNullOrEmpty(joinedData.OfficeAddress) && joinedData.OfficeAddress != "NULL") ? joinedData.OfficeAddress : string.Empty,
                                                          Date = estTime.ToString("yyyy-MM-dd"),
                                                          Address = joinedData.Address,
                                                          ReturnTypeCode = returnTpes.FirstOrDefault(code => joinedData.ELTemplate.Contains(code)),
                                                          City = ExtractCityStateZipInfo(joinedData.City_State_Zip ?? string.Empty).City,
                                                          State = ExtractCityStateZipInfo(joinedData.City_State_Zip ?? string.Empty).State,
                                                          Zip = ExtractCityStateZipInfo(joinedData.City_State_Zip ?? string.Empty).Zip,
                                                          Jurisdiction = joinedData.Jurisdiction,
                                                          IsSignatoryTitle = partnerRequest.IsSignatoryTitle
                                                      }).ToList();
                    }

                    if (bulkLettersResponse.Any() && partnerRequest != null && partnerRequest.TemplateFieldNames.Any())
                    {
                        var childEntityData = await GetChildEntities(engletterdata, returnTpes);

                        var clientIdsWithEmptyFieldValues = bulkLettersResponse.GroupBy(fieldValue => fieldValue.ReturnTypeCode)
                                                            .Select(group => group.Key).ToList();
                        var returnCodeError = partnerRequest.ReturnTpeCode
                                                .Select(item => item.ReturnType)
                                                .Except(clientIdsWithEmptyFieldValues.Select(item => item.ToString()))
                                                .ToList();
                        if (returnCodeError.Any())
                        {
                            var result = string.Join(",", returnCodeError);
                            letterFieldValuesBase.ReturnTypeCodeError = result;
                        }


                        var listOfFields = await _dbContext.Field.Where(a => !a.IsDeleted).ToListAsync();

                        foreach (var response in bulkLettersResponse)
                        {
                            var letterFieldValuesList = new List<LetterFieldValuesResponse>();
                            foreach (var field in partnerRequest.TemplateFieldNames)
                            {
                                if (field.FieldName != "Jurisdiction")
                                {
                                    var resData = typeof(BulkLettersResponse).GetProperties().Where(p => p.Name == field.FieldName).FirstOrDefault();
                                    if (resData != null)
                                    {

                                        var letterFields = new LetterFieldValuesResponse();
                                        letterFields.FieldName = field.FieldName;
                                        letterFields.FieldValue = resData?.GetValue(response)?.ToString().ToLower() != "null" ? resData?.GetValue(response)?.ToString() : string.Empty;
                                        letterFields.ClientId = response.ClientId;
                                        letterFields.FieldId = listOfFields.FirstOrDefault(a => a.FieldName == field.FieldName)?.FieldId ?? 0;
                                        letterFields.PartnerId = response.PartnerId;
                                        letterFields.LetterVersion = 1;
                                        letterFields.DisplayName = listOfFields.FirstOrDefault(a => a.FieldName == field.FieldName)?.DisplayName ?? string.Empty;

                                        letterFieldValuesList.Add(letterFields);
                                    }
                                }
                            }
                            if (letterFieldValuesList.Any())
                            {
                                response.letterFieldValues.AddRange(letterFieldValuesList);
                            }
                            var partnerOffice = new PartnerOfficeViewModel();

                            partnerOffice.PartnerId = response.PartnerId;
                            partnerOffice.PartnerName = response.PartnerName;
                            partnerOffice.FieldId = response.OfficeId;
                            partnerOffice.OfficeName = response.Office;
                            partnerOffice.Jurisdiction = response.Jurisdiction;
                            partnerOffice.OfficeAddress = response.OfficeAddress;
                            partnerOffice.OfficeCity = response.OfficeCity;
                            partnerOffice.OfficeState = response.OfficeState;
                            partnerOffice.OfficeZipCode = response.OfficeZipCode;
                            partnerOffice.OfficePhoneNumber = response.OfficePhoneNumber;


                            if (partnerOffice != null)
                            {
                                BaseData letterFieldValuesPartner = new BaseData();
                                letterFieldValuesPartner = GenerateLetterFieldValuesFromPartnerOffice(partnerOffice, response.ClientId ?? 0, response.PartnerId ?? 0);
                                response.letterFieldValues.AddRange(letterFieldValuesPartner.LetterFieldValues);
                            }

                            LetterFieldValuesResponse letterFieldValuesResponse = new LetterFieldValuesResponse();
                            string childConcatenated = string.Empty;
                            if (childEntityData != null && childEntityData.Any())
                            {
                                var parentChildInfo = childEntityData
                             .Where(a => a.ClientId == response.ClientId.ToString()).FirstOrDefault();

                                if (parentChildInfo != null)
                                {
                                    var childData = _mapper.Map<ChildEntities>(parentChildInfo);
                                    List<string> filteredProperties = GetNonNullOrEmptyProperties(childData);
                                    if (filteredProperties.Any())
                                    {
                                        childConcatenated = JsonConvert.SerializeObject(filteredProperties);
                                    }
                                }
                            }

                            letterFieldValuesResponse.FieldName = "ChildEntities";
                            letterFieldValuesResponse.FieldValue = childConcatenated;
                            letterFieldValuesResponse.ClientId = response.ClientId;
                            letterFieldValuesResponse.FieldId = partnerOffice?.FieldId;
                            letterFieldValuesResponse.PartnerId = partnerOffice?.PartnerId;
                            letterFieldValuesResponse.LetterVersion = 1;
                            response.letterFieldValues.Add(letterFieldValuesResponse);

                            responseList.Add(response);
                        }
                    }
                }
            }

            if (responseList.Any())
            {
                letterFieldValuesBase.BulkLettersResponse = responseList;
            }
            return letterFieldValuesBase;
        }
        static List<string> GetNonNullOrEmptyProperties(ChildEntities model)
        {
            return model.GetType()
        .GetProperties()
        .Select(property =>
        {
            var value = property.GetValue(model);
            return new { Value = value };
        })
        .Where(property => property.Value != null && !string.IsNullOrEmpty(property.Value.ToString()) && property.Value.ToString() != "NULL")
        .Select(property => $"{property.Value}")
        .ToList();
        }
        private async Task<List<ChildEntityModel>> GetChildEntities(List<t_engletterdata> engletterdata, List<string> returnTpes)
        {
            var data = engletterdata.Where(a => returnTpes.Any(code => a.ELTemplate != null
                               && Regex.IsMatch(a.ELTemplate, $@"\b{Regex.Escape(code)}\b", RegexOptions.IgnoreCase))).Select(a =>
             new ChildEntityModel
             {
                 ClientId = a.ClientId,
                 Child1 = a.Child1,
                 Child2 = a.Child2,
                 Child3 = a.Child3,
                 Child4 = a.Child4,
                 Child5 = a.Child5,
                 Child6 = a.Child6,
                 Child7 = a.Child7,
                 Child8 = a.Child8,
                 Child9 = a.Child9,
                 Child10 = a.Child10,
                 Child11 = a.Child11,
                 Child12 = a.Child12,
                 Child13 = a.Child13,
                 Child14 = a.Child14,
                 Child15 = a.Child15,
                 Child16 = a.Child16,
                 Child17 = a.Child17,
                 Child18 = a.Child18,
                 Child19 = a.Child19,
                 Child20 = a.Child20,
                 Child21 = a.Child21,
                 Child22 = a.Child22,
                 Child23 = a.Child23,
                 Child24 = a.Child24,
                 Child25 = a.Child25,
                 Child26 = a.Child26,
                 Child27 = a.Child27,
                 Child28 = a.Child28,
                 Child29 = a.Child29,
                 Child30 = a.Child30,
                 Child31 = a.Child31,
                 Child32 = a.Child32,
                 Child33 = a.Child33,
                 Child34 = a.Child34,
                 Child35 = a.Child35,
                 Child36 = a.Child36,
                 Child37 = a.Child37,
                 Child38 = a.Child38,
                 Child39 = a.Child39,
                 Child40 = a.Child40
             }).ToList();

            return data;
        }
        private static CityStateZipInfo ExtractCityStateZipInfo(string cityStateZip)
        {
            if (!string.IsNullOrEmpty(cityStateZip))
            {
                var parts = cityStateZip.Split(',');
                if (parts?.Length >= 2)
                {
                    var zipParts = parts[parts.Length - 1].Trim().Split(' ');
                    var stateParts = parts[parts.Length - 1].Trim().Split(' ');
                    return new CityStateZipInfo
                    {
                        City = parts[0].Trim(),
                        State = stateParts[0].Trim(),
                        Zip = zipParts.Length >= 2 ? zipParts[zipParts.Length - 1].Trim() : null
                    };
                }
                else
                {
                    return new CityStateZipInfo
                    {
                        City = (parts?.Length > 0) ? parts[0].Trim() : null,
                        State = null,
                        Zip = null
                    };
                }
            }
            return default;
        }
        public async Task<ExportDataResponse> ExportDataAsync(ExportDataRequest exportDataRequest)
        {
            ExportDataResponse exportDatafieldres = new ExportDataResponse();
            List<OneClientExportData> oneClientExports = new List<OneClientExportData>();
            List<TwoClientExportData> twoClientExports = new List<TwoClientExportData>();
            List<OneClientTitleModel> oneClientTitlesres = new List<OneClientTitleModel>();
            List<TwoClientTitleModel> twoClientTitlesres = new List<TwoClientTitleModel>();
            string taxYear = _configuration["TaxYear"] ?? string.Empty;

            //var responseData = await _dbContext.LetterFieldValues.Where(n => n.BulkLettersId == bulkLetterId && n.LetterVersion == _dbContext.LetterFieldValues
            //    .Where(inner => inner.BulkLettersId == bulkLetterId).Max(inner => inner.LetterVersion)).ToListAsync();
            var partnerIdexport = exportDataRequest.PartnersName.Select(p => p.PartnerId).ToList();
            List<ClientsViewModel> clients = JsonConvert.DeserializeObject<List<ClientsViewModel>>(exportDataRequest.ClientName);
            var letterFieldValuesList = new List<LetterFieldValuesResponse>();
            string variableName;
            string variableValue;
            bool isEsigning = false;
            using (var _dbContext = new DashboardDBContext(_dbContextOptions))
            {
                foreach (var client in clients)
                {
                    var letterFields = new LetterFieldValuesResponse();
                    var maxLetterVersion = _dbContext.LetterFieldValues
                                       .Where(lfv => lfv.BulkLettersId == client.BulkLettersId)
                                       .Max(lfv => lfv.LetterVersion);

                    var result = _dbContext.LetterFieldValues
                                   .Where(lfv => lfv.BulkLettersId == client.BulkLettersId && lfv.LetterVersion == maxLetterVersion)
                                   .Join(_dbContext.BulkLetters,
                                       lfv => lfv.BulkLettersId,
                                       bulk => bulk.BulkLettersId,
                                       (lfv, bulk) => new
                                       {
                                           LetterFieldValue = lfv,
                                           BulkLetter = bulk
                                       })
                                   .ToList();

                    Dictionary<string, string> pivotedData = new Dictionary<string, string>();
                    string childentities;

                    // Pivot the data
                    foreach (var fieldValues in result)
                    {
                        pivotedData[fieldValues.LetterFieldValue.FieldName] = fieldValues.LetterFieldValue.FieldValue;

                        if (fieldValues.BulkLetter != null)
                        {

                            if (!string.IsNullOrEmpty(fieldValues.BulkLetter.SignatoryEmailId) && maxLetterVersion == 1)
                            {
                                pivotedData["SignatoryEmailId"] = fieldValues.BulkLetter.SignatoryEmailId.ToString();
                            }
                            else if (fieldValues.LetterFieldValue.FieldName == "SignatoryEmailId")
                            {
                                pivotedData["SignatoryEmailId"] = fieldValues.LetterFieldValue.FieldValue;
                            }

                            if (!string.IsNullOrEmpty(fieldValues.BulkLetter.SpouseEmailId) && maxLetterVersion == 1)
                            {
                                pivotedData["SpouseEmailId"] = fieldValues.BulkLetter.SpouseEmailId.ToString();
                            }
                            else if (fieldValues.LetterFieldValue.FieldName == "SpouseEmailId")
                            {
                                pivotedData["SpouseEmailId"] = fieldValues.LetterFieldValue.FieldValue;
                            }
                            if (fieldValues.BulkLetter.IsEsigning == true)
                            {
                                isEsigning = true;
                            }
                            else if (fieldValues.BulkLetter.IsEsigning == false)
                            {
                                isEsigning = false;
                            }
                        }

                        if (pivotedData.ContainsKey("FieldId"))
                        {
                            pivotedData["OfficeId"] = pivotedData["FieldId"];
                            pivotedData.Remove("FieldId");
                        }

                        if (fieldValues.LetterFieldValue.FieldName == "ChildEntities")
                        {
                            childentities = fieldValues.LetterFieldValue.FieldValue;
                            if (!string.IsNullOrEmpty(childentities))
                            {
                                List<string> deserializedData = JsonConvert.DeserializeObject<List<string>>(childentities);
                                int index = 0;
                                foreach (var item in deserializedData)
                                {
                                    index++;
                                    variableName = "Child" + index;
                                    variableValue = item;
                                    pivotedData[variableName] = variableValue;
                                }
                            }
                        }
                    }

                    if (exportDataRequest.ClientSignatureCount == 1)
                    {
                        if (exportDataRequest.TemplateName == "Trust Tax Eng Letter" || exportDataRequest.TemplateName == "990 Tax Eng Letter" || exportDataRequest.TemplateName == "BAS Eng Letter" || exportDataRequest.TemplateName == "SEC Client Rule Business Tax Eng Letter" || exportDataRequest.TemplateName == "Business Tax Eng Letter")
                        {
                            var keyIgnoreCasepartnerId = pivotedData.Keys.FirstOrDefault(k => k.Equals("PartnerId", StringComparison.OrdinalIgnoreCase));
                            var keyIgnoreCaseofficeId = pivotedData.Keys.FirstOrDefault(k => k.Equals("OfficeId", StringComparison.OrdinalIgnoreCase));
                            var partnerId = Convert.ToInt32(pivotedData[keyIgnoreCasepartnerId]);
                            var officeId = Convert.ToInt32(pivotedData[keyIgnoreCaseofficeId]);
                            var signatureoneres = new OneClientExportData
                            {
                                BatchId = exportDataRequest.BatchId,
                                BulkLettersId = client.BulkLettersId,
                                ClientId = client.ClientId,
                                ClientName = GetPivotedValue(pivotedData, "ClientName"),
                                SignatoryFirstName = GetPivotedValue(pivotedData, "SignatoryFirstName"),
                                SignatoryLastName = GetPivotedValue(pivotedData, "SignatoryLastName"),
                                SignatoryTitle = GetPivotedValue(pivotedData, "SignatoryTitle"),
                                Address = GetPivotedValue(pivotedData, "Address"),
                                City = GetPivotedValue(pivotedData, "City"),
                                State = GetPivotedValue(pivotedData, "State"),
                                Zip = GetPivotedValue(pivotedData, "Zip"),
                                SignatoryEmailId = GetPivotedValue(pivotedData, "SignatoryEmailId"),
                                TaxYear = taxYear,
                                Date = GetPivotedValue(pivotedData, "Date"),
                                IsEsigning = isEsigning ? "Yes" : "No",
                                PartnerId = partnerId,
                                PartnerName = GetPivotedValue(pivotedData, "PartnerName"),
                                OfficeId = officeId,
                                OfficeName = GetPivotedValue(pivotedData, "OfficeName"),
                                OfficeAddress = GetPivotedValue(pivotedData, "OfficeAddress"),
                                OfficeCity = GetPivotedValue(pivotedData, "OfficeCity"),
                                OfficeState = GetPivotedValue(pivotedData, "OfficeState"),
                                OfficeZipCode = GetPivotedValue(pivotedData, "OfficeZipCode"),
                                OfficePhoneNumber = GetPivotedValue(pivotedData, "OfficePhoneNumber"),
                                Jurisdiction = GetPivotedValue(pivotedData, "Jurisdiction"),
                                Child1 = GetPivotedValue(pivotedData, "Child1"),
                                Child2 = GetPivotedValue(pivotedData, "Child2"),
                                Child3 = GetPivotedValue(pivotedData, "Child3"),
                                Child4 = GetPivotedValue(pivotedData, "Child4"),
                                Child5 = GetPivotedValue(pivotedData, "Child5"),
                                Child6 = GetPivotedValue(pivotedData, "Child6"),
                                Child7 = GetPivotedValue(pivotedData, "Child7"),
                                Child8 = GetPivotedValue(pivotedData, "Child8"),
                                Child9 = GetPivotedValue(pivotedData, "Child9"),
                                Child10 = GetPivotedValue(pivotedData, "Child10"),
                                Child11 = GetPivotedValue(pivotedData, "Child11"),
                                Child12 = GetPivotedValue(pivotedData, "Child12"),
                                Child13 = GetPivotedValue(pivotedData, "Child13"),
                                Child14 = GetPivotedValue(pivotedData, "Child14"),
                                Child15 = GetPivotedValue(pivotedData, "Child15"),
                                Child16 = GetPivotedValue(pivotedData, "Child16"),
                                Child17 = GetPivotedValue(pivotedData, "Child17"),
                                Child18 = GetPivotedValue(pivotedData, "Child18"),
                                Child19 = GetPivotedValue(pivotedData, "Child19"),
                                Child20 = GetPivotedValue(pivotedData, "Child20"),
                                Child21 = GetPivotedValue(pivotedData, "Child21"),
                                Child22 = GetPivotedValue(pivotedData, "Child22"),
                                Child23 = GetPivotedValue(pivotedData, "Child23"),
                                Child24 = GetPivotedValue(pivotedData, "Child24"),
                                Child25 = GetPivotedValue(pivotedData, "Child25"),
                                Child26 = GetPivotedValue(pivotedData, "Child26"),
                                Child27 = GetPivotedValue(pivotedData, "Child27"),
                                Child28 = GetPivotedValue(pivotedData, "Child28"),
                                Child29 = GetPivotedValue(pivotedData, "Child29"),
                                Child30 = GetPivotedValue(pivotedData, "Child30"),
                                Child31 = GetPivotedValue(pivotedData, "Child31"),
                                Child32 = GetPivotedValue(pivotedData, "Child32"),
                                Child33 = GetPivotedValue(pivotedData, "Child33"),
                                Child34 = GetPivotedValue(pivotedData, "Child34"),
                                Child35 = GetPivotedValue(pivotedData, "Child35"),
                                Child36 = GetPivotedValue(pivotedData, "Child36"),
                                Child37 = GetPivotedValue(pivotedData, "Child37"),
                                Child38 = GetPivotedValue(pivotedData, "Child38"),
                                Child39 = GetPivotedValue(pivotedData, "Child39"),
                                Child40 = GetPivotedValue(pivotedData, "Child40")
                            };

                            if (exportDatafieldres.oneClientExport == null)
                            {
                                exportDatafieldres.oneClientExport = new List<OneClientExportData>();
                            }

                            exportDatafieldres.oneClientExport.Add(signatureoneres);
                        }
                        else
                        {
                            var keyIgnoreCasepartnerId = pivotedData.Keys.FirstOrDefault(k => k.Equals("PartnerId", StringComparison.OrdinalIgnoreCase));
                            var keyIgnoreCaseofficeId = pivotedData.Keys.FirstOrDefault(k => k.Equals("OfficeId", StringComparison.OrdinalIgnoreCase));
                            var partnerId = Convert.ToInt32(pivotedData[keyIgnoreCasepartnerId]);
                            var officeId = Convert.ToInt32(pivotedData[keyIgnoreCaseofficeId]);
                            var signatureoneres = new OneClientTitleModel
                            {
                                BatchId = exportDataRequest.BatchId,
                                BulkLettersId = client.BulkLettersId,
                                ClientId = client.ClientId,
                                ClientName = GetPivotedValue(pivotedData, "ClientName"),
                                SignatoryFirstName = GetPivotedValue(pivotedData, "SignatoryFirstName"),
                                SignatoryLastName = GetPivotedValue(pivotedData, "SignatoryLastName"),
                                Address = GetPivotedValue(pivotedData, "Address"),
                                City = GetPivotedValue(pivotedData, "City"),
                                State = GetPivotedValue(pivotedData, "State"),
                                Zip = GetPivotedValue(pivotedData, "Zip"),
                                SignatoryEmailId = GetPivotedValue(pivotedData, "SignatoryEmailId"),
                                TaxYear = taxYear,
                                Date = GetPivotedValue(pivotedData, "Date"),
                                IsEsigning = isEsigning ? "Yes" : "No",
                                PartnerId = partnerId,
                                PartnerName = GetPivotedValue(pivotedData, "PartnerName"),
                                OfficeId = officeId,
                                OfficeName = GetPivotedValue(pivotedData, "OfficeName"),
                                OfficeAddress = GetPivotedValue(pivotedData, "OfficeAddress"),
                                OfficeCity = GetPivotedValue(pivotedData, "OfficeCity"),
                                OfficeState = GetPivotedValue(pivotedData, "OfficeState"),
                                OfficeZipCode = GetPivotedValue(pivotedData, "OfficeZipCode"),
                                OfficePhoneNumber = GetPivotedValue(pivotedData, "OfficePhoneNumber"),
                                Jurisdiction = GetPivotedValue(pivotedData, "Jurisdiction"),
                                Child1 = GetPivotedValue(pivotedData, "Child1"),
                                Child2 = GetPivotedValue(pivotedData, "Child2"),
                                Child3 = GetPivotedValue(pivotedData, "Child3"),
                                Child4 = GetPivotedValue(pivotedData, "Child4"),
                                Child5 = GetPivotedValue(pivotedData, "Child5"),
                                Child6 = GetPivotedValue(pivotedData, "Child6"),
                                Child7 = GetPivotedValue(pivotedData, "Child7"),
                                Child8 = GetPivotedValue(pivotedData, "Child8"),
                                Child9 = GetPivotedValue(pivotedData, "Child9"),
                                Child10 = GetPivotedValue(pivotedData, "Child10"),
                                Child11 = GetPivotedValue(pivotedData, "Child11"),
                                Child12 = GetPivotedValue(pivotedData, "Child12"),
                                Child13 = GetPivotedValue(pivotedData, "Child13"),
                                Child14 = GetPivotedValue(pivotedData, "Child14"),
                                Child15 = GetPivotedValue(pivotedData, "Child15"),
                                Child16 = GetPivotedValue(pivotedData, "Child16"),
                                Child17 = GetPivotedValue(pivotedData, "Child17"),
                                Child18 = GetPivotedValue(pivotedData, "Child18"),
                                Child19 = GetPivotedValue(pivotedData, "Child19"),
                                Child20 = GetPivotedValue(pivotedData, "Child20"),
                                Child21 = GetPivotedValue(pivotedData, "Child21"),
                                Child22 = GetPivotedValue(pivotedData, "Child22"),
                                Child23 = GetPivotedValue(pivotedData, "Child23"),
                                Child24 = GetPivotedValue(pivotedData, "Child24"),
                                Child25 = GetPivotedValue(pivotedData, "Child25"),
                                Child26 = GetPivotedValue(pivotedData, "Child26"),
                                Child27 = GetPivotedValue(pivotedData, "Child27"),
                                Child28 = GetPivotedValue(pivotedData, "Child28"),
                                Child29 = GetPivotedValue(pivotedData, "Child29"),
                                Child30 = GetPivotedValue(pivotedData, "Child30"),
                                Child31 = GetPivotedValue(pivotedData, "Child31"),
                                Child32 = GetPivotedValue(pivotedData, "Child32"),
                                Child33 = GetPivotedValue(pivotedData, "Child33"),
                                Child34 = GetPivotedValue(pivotedData, "Child34"),
                                Child35 = GetPivotedValue(pivotedData, "Child35"),
                                Child36 = GetPivotedValue(pivotedData, "Child36"),
                                Child37 = GetPivotedValue(pivotedData, "Child37"),
                                Child38 = GetPivotedValue(pivotedData, "Child38"),
                                Child39 = GetPivotedValue(pivotedData, "Child39"),
                                Child40 = GetPivotedValue(pivotedData, "Child40")
                            };

                            if (exportDatafieldres.oneClientTitlesres == null)
                            {
                                exportDatafieldres.oneClientTitlesres = new List<OneClientTitleModel>();
                            }

                            exportDatafieldres.oneClientTitlesres.Add(signatureoneres);
                        }
                    }
                    else
                    {
                        if (exportDataRequest.TemplateName == "709 Eng Letter" || exportDataRequest.TemplateName == "SEC Client Rule Individual Tax Eng Letter" || exportDataRequest.TemplateName == "Individual Tax Eng Letter")
                        {
                            var exportDataResponse = new TwoClientTitleModel
                            {
                                BatchId = exportDataRequest.BatchId,
                                BulkLettersId = client.BulkLettersId,
                                ClientId = client.ClientId,
                                ClientName = GetPivotedValue(pivotedData, "ClientName"),
                                SignatoryFirstName = GetPivotedValue(pivotedData, "SignatoryFirstName"),
                                SignatoryLastName = GetPivotedValue(pivotedData, "SignatoryLastName"),
                                Address = GetPivotedValue(pivotedData, "Address"),
                                City = GetPivotedValue(pivotedData, "City"),
                                State = GetPivotedValue(pivotedData, "State"),
                                Zip = GetPivotedValue(pivotedData, "Zip"),
                                SpouseFirstName = GetPivotedValue(pivotedData, "SpouseFirstName"),
                                SpouseLastName = GetPivotedValue(pivotedData, "SpouseLastName"),
                                SignatoryEmailId = GetPivotedValue(pivotedData, "SignatoryEmailId"),
                                SpouseEmailId = GetPivotedValue(pivotedData, "SpouseEmailId"),
                                TaxYear = taxYear,
                                Date = GetPivotedValue(pivotedData, "Date"),
                                IsEsigning = isEsigning ? "Yes" : "No",
                                PartnerId = !string.IsNullOrEmpty(GetPivotedValue(pivotedData, "PartnerId")) ? Convert.ToInt32(GetPivotedValue(pivotedData, "PartnerId")) : 0,
                                PartnerName = GetPivotedValue(pivotedData, "PartnerName"),
                                OfficeId = !string.IsNullOrEmpty(GetPivotedValue(pivotedData, "OfficeId")) ? Convert.ToInt32(GetPivotedValue(pivotedData, "OfficeId")) : 0,
                                OfficeName = GetPivotedValue(pivotedData, "OfficeName"),
                                OfficeAddress = GetPivotedValue(pivotedData, "OfficeAddress"),
                                OfficeCity = GetPivotedValue(pivotedData, "OfficeCity"),
                                OfficeState = GetPivotedValue(pivotedData, "OfficeState"),
                                OfficeZipCode = GetPivotedValue(pivotedData, "OfficeZipCode"),
                                OfficePhoneNumber = GetPivotedValue(pivotedData, "OfficePhoneNumber"),
                                Jurisdiction = GetPivotedValue(pivotedData, "Jurisdiction"),
                                Child1 = GetPivotedValue(pivotedData, "Child1"),
                                Child2 = GetPivotedValue(pivotedData, "Child2"),
                                Child3 = GetPivotedValue(pivotedData, "Child3"),
                                Child4 = GetPivotedValue(pivotedData, "Child4"),
                                Child5 = GetPivotedValue(pivotedData, "Child5"),
                                Child6 = GetPivotedValue(pivotedData, "Child6"),
                                Child7 = GetPivotedValue(pivotedData, "Child7"),
                                Child8 = GetPivotedValue(pivotedData, "Child8"),
                                Child9 = GetPivotedValue(pivotedData, "Child9"),
                                Child10 = GetPivotedValue(pivotedData, "Child10"),
                                Child11 = GetPivotedValue(pivotedData, "Child11"),
                                Child12 = GetPivotedValue(pivotedData, "Child12"),
                                Child13 = GetPivotedValue(pivotedData, "Child13"),
                                Child14 = GetPivotedValue(pivotedData, "Child14"),
                                Child15 = GetPivotedValue(pivotedData, "Child15"),
                                Child16 = GetPivotedValue(pivotedData, "Child16"),
                                Child17 = GetPivotedValue(pivotedData, "Child17"),
                                Child18 = GetPivotedValue(pivotedData, "Child18"),
                                Child19 = GetPivotedValue(pivotedData, "Child19"),
                                Child20 = GetPivotedValue(pivotedData, "Child20"),
                                Child21 = GetPivotedValue(pivotedData, "Child21"),
                                Child22 = GetPivotedValue(pivotedData, "Child22"),
                                Child23 = GetPivotedValue(pivotedData, "Child23"),
                                Child24 = GetPivotedValue(pivotedData, "Child24"),
                                Child25 = GetPivotedValue(pivotedData, "Child25"),
                                Child26 = GetPivotedValue(pivotedData, "Child26"),
                                Child27 = GetPivotedValue(pivotedData, "Child27"),
                                Child28 = GetPivotedValue(pivotedData, "Child28"),
                                Child29 = GetPivotedValue(pivotedData, "Child29"),
                                Child30 = GetPivotedValue(pivotedData, "Child30"),
                                Child31 = GetPivotedValue(pivotedData, "Child31"),
                                Child32 = GetPivotedValue(pivotedData, "Child32"),
                                Child33 = GetPivotedValue(pivotedData, "Child33"),
                                Child34 = GetPivotedValue(pivotedData, "Child34"),
                                Child35 = GetPivotedValue(pivotedData, "Child35"),
                                Child36 = GetPivotedValue(pivotedData, "Child36"),
                                Child37 = GetPivotedValue(pivotedData, "Child37"),
                                Child38 = GetPivotedValue(pivotedData, "Child38"),
                                Child39 = GetPivotedValue(pivotedData, "Child39"),
                                Child40 = GetPivotedValue(pivotedData, "Child40")
                            };

                            if (exportDatafieldres.twoClientTitleres == null)
                            {
                                exportDatafieldres.twoClientTitleres = new List<TwoClientTitleModel>();
                            }

                            exportDatafieldres.twoClientTitleres.Add(exportDataResponse);
                        }
                        else
                        {
                            var exportDataResponse = new TwoClientExportData
                            {
                                BatchId = exportDataRequest.BatchId,
                                BulkLettersId = client.BulkLettersId,
                                ClientId = client.ClientId,
                                ClientName = GetPivotedValue(pivotedData, "ClientName"),
                                SignatoryFirstName = GetPivotedValue(pivotedData, "SignatoryFirstName"),
                                SignatoryLastName = GetPivotedValue(pivotedData, "SignatoryLastName"),
                                SignatoryTitle = GetPivotedValue(pivotedData, "SignatoryTitle"),
                                Address = GetPivotedValue(pivotedData, "Address"),
                                City = GetPivotedValue(pivotedData, "City"),
                                State = GetPivotedValue(pivotedData, "State"),
                                Zip = GetPivotedValue(pivotedData, "Zip"),
                                SpouseFirstName = GetPivotedValue(pivotedData, "SpouseFirstName"),
                                SpouseLastName = GetPivotedValue(pivotedData, "SpouseLastName"),
                                SignatoryEmailId = GetPivotedValue(pivotedData, "SignatoryEmailId"),
                                SpouseEmailId = GetPivotedValue(pivotedData, "SpouseEmailId"),
                                TaxYear = taxYear,
                                Date = GetPivotedValue(pivotedData, "Date"),
                                IsEsigning = isEsigning ? "Yes" : "No",
                                PartnerId = !string.IsNullOrEmpty(GetPivotedValue(pivotedData, "PartnerId")) ? Convert.ToInt32(GetPivotedValue(pivotedData, "PartnerId")) : 0,
                                PartnerName = GetPivotedValue(pivotedData, "PartnerName"),
                                OfficeId = !string.IsNullOrEmpty(GetPivotedValue(pivotedData, "OfficeId")) ? Convert.ToInt32(GetPivotedValue(pivotedData, "OfficeId")) : 0,
                                OfficeName = GetPivotedValue(pivotedData, "OfficeName"),
                                OfficeAddress = GetPivotedValue(pivotedData, "OfficeAddress"),
                                OfficeCity = GetPivotedValue(pivotedData, "OfficeCity"),
                                OfficeState = GetPivotedValue(pivotedData, "OfficeState"),
                                OfficeZipCode = GetPivotedValue(pivotedData, "OfficeZipCode"),
                                OfficePhoneNumber = GetPivotedValue(pivotedData, "OfficePhoneNumber"),
                                Jurisdiction = GetPivotedValue(pivotedData, "Jurisdiction"),
                                Child1 = GetPivotedValue(pivotedData, "Child1"),
                                Child2 = GetPivotedValue(pivotedData, "Child2"),
                                Child3 = GetPivotedValue(pivotedData, "Child3"),
                                Child4 = GetPivotedValue(pivotedData, "Child4"),
                                Child5 = GetPivotedValue(pivotedData, "Child5"),
                                Child6 = GetPivotedValue(pivotedData, "Child6"),
                                Child7 = GetPivotedValue(pivotedData, "Child7"),
                                Child8 = GetPivotedValue(pivotedData, "Child8"),
                                Child9 = GetPivotedValue(pivotedData, "Child9"),
                                Child10 = GetPivotedValue(pivotedData, "Child10"),
                                Child11 = GetPivotedValue(pivotedData, "Child11"),
                                Child12 = GetPivotedValue(pivotedData, "Child12"),
                                Child13 = GetPivotedValue(pivotedData, "Child13"),
                                Child14 = GetPivotedValue(pivotedData, "Child14"),
                                Child15 = GetPivotedValue(pivotedData, "Child15"),
                                Child16 = GetPivotedValue(pivotedData, "Child16"),
                                Child17 = GetPivotedValue(pivotedData, "Child17"),
                                Child18 = GetPivotedValue(pivotedData, "Child18"),
                                Child19 = GetPivotedValue(pivotedData, "Child19"),
                                Child20 = GetPivotedValue(pivotedData, "Child20"),
                                Child21 = GetPivotedValue(pivotedData, "Child21"),
                                Child22 = GetPivotedValue(pivotedData, "Child22"),
                                Child23 = GetPivotedValue(pivotedData, "Child23"),
                                Child24 = GetPivotedValue(pivotedData, "Child24"),
                                Child25 = GetPivotedValue(pivotedData, "Child25"),
                                Child26 = GetPivotedValue(pivotedData, "Child26"),
                                Child27 = GetPivotedValue(pivotedData, "Child27"),
                                Child28 = GetPivotedValue(pivotedData, "Child28"),
                                Child29 = GetPivotedValue(pivotedData, "Child29"),
                                Child30 = GetPivotedValue(pivotedData, "Child30"),
                                Child31 = GetPivotedValue(pivotedData, "Child31"),
                                Child32 = GetPivotedValue(pivotedData, "Child32"),
                                Child33 = GetPivotedValue(pivotedData, "Child33"),
                                Child34 = GetPivotedValue(pivotedData, "Child34"),
                                Child35 = GetPivotedValue(pivotedData, "Child35"),
                                Child36 = GetPivotedValue(pivotedData, "Child36"),
                                Child37 = GetPivotedValue(pivotedData, "Child37"),
                                Child38 = GetPivotedValue(pivotedData, "Child38"),
                                Child39 = GetPivotedValue(pivotedData, "Child39"),
                                Child40 = GetPivotedValue(pivotedData, "Child40")
                            };

                            if (exportDatafieldres.twoClientExport == null)
                            {
                                exportDatafieldres.twoClientExport = new List<TwoClientExportData>();
                            }

                            exportDatafieldres.twoClientExport.Add(exportDataResponse);
                        }

                    }


                    string GetPivotedValue(Dictionary<string, string> pivotedData, string key)
                    {
                        if (pivotedData.ContainsKey(key))
                        {
                            return pivotedData[key];
                        }
                        // If the key is not found, return an empty string
                        return string.Empty;
                    }
                }
            }
            return exportDatafieldres;

        }
        public async Task<BulkDraftLetterResponse> SaveImportedData(List<ImportDataViewModel> importData)
        {
            BulkDraftLetterResponse bulkDraftLetterResponse = new BulkDraftLetterResponse();
            ImportDataModel importDataRes = new ImportDataModel();
            try
            {
                using (var _dbContext = new DashboardDBContext(_dbContextOptions))
                {
                    BulkLetters bulkData = await _dbContext.BulkLetters.Where(a => a.BulkLettersId == importData.FirstOrDefault().BulkLettersId).FirstOrDefaultAsync();
                    var templatename = bulkData.TemplateName;
                    List<EditFieldsValues> editFieldValues = new List<EditFieldsValues>();
                    List<EditFieldsValues> editFieldValuesResponse = new List<EditFieldsValues>();

                    var filedData = await _dbContext.Field.Where(a => !a.IsDeleted).ToListAsync();


                    var listOflatestVersion = _dbContext.LetterFieldValues.Where(a => importData.Select(c => c.BulkLettersId).ToList().Contains(a.BulkLettersId ?? 0)).ToList();

                    var bulkLettersData = new List<BulkLetters>();
                    var FieldValues = new List<LetterFieldValues>();
                    foreach (var item in importData)
                    {
                        int latestVersin = listOflatestVersion.OrderByDescending(l => l.LetterVersion)
                                           .FirstOrDefault(a => a.BulkLettersId == item.BulkLettersId)?.LetterVersion ?? 0;


                        BulkLetters objBulkLetters = new BulkLetters();
                        List<string> childEntities = new List<string>();
                        var itemType = item.GetType();
                        var properties = itemType.GetProperties();
                        foreach (var property in properties)
                        {
                            var fieldName = property.Name;
                            var fields = filedData?.FirstOrDefault(a => a.FieldName == fieldName);
                            var fieldValue = property.GetValue(item)?.ToString();
                            if (fieldName != "BulkLettersId" && fieldName != "BatchId" && fieldName != "ClientId" && fieldName != "IsEsigning")
                            {
                                if (fieldName.Contains("Child"))
                                {
                                    if (!string.IsNullOrEmpty(fieldValue))
                                    {
                                        childEntities.Add(fieldValue);
                                    }
                                }
                                else
                                {
                                    if (fieldName == "Date" && !string.IsNullOrEmpty(fieldValue))
                                    {
                                        string datevalue = fieldValue;
                                        try
                                        {
                                            DateTime dateTime = Convert.ToDateTime(datevalue);
                                            fieldValue = dateTime.ToString("yyyy-MM-dd");
                                        }
                                        catch (Exception ex)
                                        {
                                        }
                                    }
                                    if (bulkData != null && bulkData.ClientSignatureCount == 2)
                                    {
                                        int signatoryCount = 0;
                                        if (fieldName == "SignatoryTitle")
                                        {
                                            signatoryCount = listOflatestVersion
                                                .Where(a => a.FieldName == fieldName && a.BulkLettersId == item.BulkLettersId).Count();

                                            if (signatoryCount > 0)
                                            {
                                                FieldValues.Add(new LetterFieldValues
                                                {
                                                    BulkLettersId = item.BulkLettersId,
                                                    ClientId = item.ClientId,
                                                    PartnerId = item.PartnerId,
                                                    FieldId = (fields != null && fields.FieldId > 0) ? fields.FieldId : item.BulkLettersId,
                                                    FieldName = fieldName,
                                                    FieldValue = (fieldValue != null && fieldValue.ToLower() != "null") ? fieldValue : string.Empty,
                                                    LetterVersion = latestVersin + 1,
                                                    DisplayName = (fields != null && !string.IsNullOrEmpty(fields.DisplayName)) ? fields.DisplayName : fieldName,
                                                });
                                            }

                                        }
                                        else
                                        {
                                            FieldValues.Add(new LetterFieldValues
                                            {
                                                BulkLettersId = item.BulkLettersId,
                                                ClientId = item.ClientId,
                                                PartnerId = item.PartnerId,
                                                FieldId = (fields != null && fields.FieldId > 0) ? fields.FieldId : item.BulkLettersId,
                                                FieldName = fieldName,
                                                FieldValue = (fieldValue != null && fieldValue.ToLower() != "null") ? fieldValue : string.Empty,
                                                LetterVersion = latestVersin + 1,
                                                DisplayName = (fields != null && !string.IsNullOrEmpty(fields.DisplayName)) ? fields.DisplayName : fieldName,
                                            });
                                        }
                                    }
                                    else
                                    {
                                        if (fieldName != "SpouseFirstName" && fieldName != "SpouseLastName" && fieldName != "SpouseEmailId")
                                        {

                                            if (fieldName == "SignatoryTitle")
                                            {
                                                int signatoryCount = 0;
                                                signatoryCount = listOflatestVersion
                                                    .Where(a => a.FieldName == fieldName && a.BulkLettersId == item.BulkLettersId).Count();

                                                if (signatoryCount > 0)
                                                {
                                                    FieldValues.Add(new LetterFieldValues
                                                    {
                                                        BulkLettersId = item.BulkLettersId,
                                                        ClientId = item.ClientId,
                                                        PartnerId = item.PartnerId,
                                                        FieldId = (fields != null && fields.FieldId > 0) ? fields.FieldId : item.BulkLettersId,
                                                        FieldName = fieldName,
                                                        FieldValue = (fieldValue != null && fieldValue.ToLower() != "null") ? fieldValue : string.Empty,
                                                        LetterVersion = latestVersin + 1,
                                                        DisplayName = (fields != null && !string.IsNullOrEmpty(fields.DisplayName)) ? fields.DisplayName : fieldName,
                                                    });
                                                }

                                            }
                                            else
                                            {
                                                FieldValues.Add(new LetterFieldValues
                                                {
                                                    BulkLettersId = item.BulkLettersId,
                                                    ClientId = item.ClientId,
                                                    PartnerId = item.PartnerId,
                                                    FieldId = (fields != null && fields.FieldId > 0) ? fields.FieldId : item.BulkLettersId,
                                                    FieldName = fieldName,
                                                    FieldValue = (fieldValue != null && fieldValue.ToLower() != "null") ? fieldValue : string.Empty,
                                                    LetterVersion = latestVersin + 1,
                                                    DisplayName = (fields != null && !string.IsNullOrEmpty(fields.DisplayName)) ? fields.DisplayName : fieldName,
                                                });
                                            }
                                        }
                                    }
                                }

                                if (fieldName.Contains("TaxYear"))
                                {
                                    objBulkLetters.BulkLettersId = item.BulkLettersId;
                                    objBulkLetters.TaxYear = fieldValue;
                                }
                                if (fieldName.Contains("ClientName"))
                                {
                                    objBulkLetters.BulkLettersId = item.BulkLettersId;
                                    objBulkLetters.ClientName = fieldValue;
                                }
                                if (fieldName.Contains("PartnerName"))
                                {
                                    objBulkLetters.BulkLettersId = item.BulkLettersId;
                                    objBulkLetters.PartnerName = fieldValue;
                                }
                                if (fieldName.Contains("Office"))
                                {
                                    objBulkLetters.BulkLettersId = item.BulkLettersId;
                                    objBulkLetters.Office = fieldValue;
                                }
                                if (fieldName.Contains("SignatoryFirstName"))
                                {
                                    objBulkLetters.BulkLettersId = item.BulkLettersId;
                                    objBulkLetters.SignatoryFirstName = fieldValue;
                                }
                                if (fieldName.Contains("SignatoryLastName"))
                                {
                                    objBulkLetters.BulkLettersId = item.BulkLettersId;
                                    objBulkLetters.SignatoryLastName = fieldValue;
                                }
                                if (fieldName.Contains("SignatoryEmailId"))
                                {
                                    objBulkLetters.BulkLettersId = item.BulkLettersId;
                                    objBulkLetters.SignatoryEmailId = fieldValue;
                                }
                                if (bulkData != null && bulkData.ClientSignatureCount == 2)
                                {
                                    if (fieldName.Contains("SpouseEmailId"))
                                    {
                                        objBulkLetters.BulkLettersId = item.BulkLettersId;
                                        objBulkLetters.SpouseEmailId = fieldValue;
                                    }
                                    if (fieldName.Contains("SpouseFirstName"))
                                    {
                                        objBulkLetters.BulkLettersId = item.BulkLettersId;
                                        objBulkLetters.SpouseFirstName = fieldValue;
                                    }
                                    if (fieldName.Contains("SpouseLastName"))
                                    {
                                        objBulkLetters.BulkLettersId = item.BulkLettersId;
                                        objBulkLetters.SpouseLastName = fieldValue;
                                    }
                                }
                            }

                            if (fieldName.Contains("IsEsigning"))
                            {
                                objBulkLetters.BulkLettersId = item.BulkLettersId;
                                objBulkLetters.IsEsigning = fieldValue != null && fieldValue.ToLower() == "yes" ? true : false;
                            }
                        }

                        if (childEntities != null && childEntities.Count > 0)
                        {
                            string childConcatenated = JsonConvert.SerializeObject(childEntities);
                            var childFields = filedData?.FirstOrDefault(a => a.FieldName == "ChildEntities");
                            FieldValues.Add(new LetterFieldValues
                            {
                                BulkLettersId = item.BulkLettersId,
                                ClientId = item.ClientId,
                                PartnerId = item.PartnerId,
                                FieldId = (childFields != null && childFields.FieldId > 0) ? childFields.FieldId : item.BulkLettersId,
                                FieldName = "ChildEntities",
                                FieldValue = childConcatenated,
                                LetterVersion = latestVersin + 1,
                                DisplayName = (childFields != null && !string.IsNullOrEmpty(childFields.DisplayName)) ? childFields.DisplayName : "Child Entities",
                            });
                        }

                        if (objBulkLetters.BulkLettersId > 0)
                        {
                            bulkLettersData.Add(objBulkLetters);
                        }
                    }
                    if (FieldValues.Any())
                    {

                        var resFieldData = await InsertEditFieldValues(FieldValues);
                        if (bulkLettersData.Any() && resFieldData == true)
                        {
                            bulkDraftLetterResponse = await UpdateBulkLetterFieldValues(bulkLettersData);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

            return bulkDraftLetterResponse;
        }
        public async Task<bool> InsertEditFieldValues(List<LetterFieldValues> letterFieldValues)
        {
            using (var _dbContext = new DashboardDBContext(_dbContextOptions))
            {
                await _dbContext.AddRangeAsync(letterFieldValues);
                await _dbContext.SaveChangesAsync();
                return true;
            }
        }
        public async Task<string> GetTypeCodeList(int templateId)
        {
            string majorReturnType = string.Empty;
            using (var _dbContext = new DashboardDBContext(_dbContextOptions))
            {
                if (templateId > 0)
                {
                    var getTypeCode = await _dbContext.TemplateLetterCodeMapping.FirstOrDefaultAsync(x => x.TemplateId == templateId);
                    majorReturnType = getTypeCode?.MajorReturnType ?? string.Empty;
                    return majorReturnType;
                }
                return majorReturnType;
            }
        }
        public async Task<(int, bool)> GetAddClientData(string templateName)
        {
            int dataCount = 0;
            bool is7216 = false;
            using (var dbContext = new DashboardDBContext(_dbContextOptions))
            {
                if (!string.IsNullOrEmpty(templateName))
                {
                    var template = await dbContext.Template.FirstOrDefaultAsync(t => t.TemplateName == templateName);

                    if (template != null)
                    {
                        dataCount = template.ClientSignatureCount ?? 0;
                        is7216 = template.Is7216Available;
                    }
                }
            }

            return (dataCount, is7216);
        }
        public async Task<BulkDraftLetterResponse> UpdateBulkLetterFieldValues(List<BulkLetters> bulkLetterFieldValues)
        {
            using (var _dbContext = new DashboardDBContext(_dbContextOptions))
            {
                int batchId = 0;
                BulkDraftLetterResponse bulkDraftLetterResponse = new BulkDraftLetterResponse();
                List<BulkLetters> requestData = new List<BulkLetters>();
                var existingData = await _dbContext.BulkLetters.
                                   Where(a => bulkLetterFieldValues.Select(x => x.BulkLettersId).
                                   Contains(a.BulkLettersId)).ToListAsync();

                if (existingData.Any())
                {
                    foreach (var item in existingData)
                    {
                        batchId = item.BatchId;
                        var objectData = bulkLetterFieldValues.Where(a => a.BulkLettersId == item.BulkLettersId).FirstOrDefault();
                        if (objectData != null)
                        {
                            item.ClientName = objectData.ClientName;
                            item.PartnerName = objectData.PartnerName;
                            item.Office = objectData.Office;
                            item.SignatoryFirstName = objectData.SignatoryFirstName;
                            item.SignatoryLastName = objectData.SignatoryLastName;
                            item.SignatoryEmailId = objectData.SignatoryEmailId;
                            if (item.ClientSignatureCount == 2)
                            {
                                item.SpouseEmailId = objectData.SpouseEmailId;
                                item.SpouseFirstName = objectData.SpouseFirstName;
                                item.SpouseLastName = objectData.SpouseLastName;
                            }
                            item.TaxYear = objectData.TaxYear;
                            item.IsEsigning = objectData.IsEsigning;
                            requestData.Add(item);
                        }
                    }
                }

                if (requestData.Any())
                {
                    _dbContext.UpdateRange(requestData);
                    await _dbContext.SaveChangesAsync();

                }
                List<DraftLetters> draftLetters = _mapper.Map<List<DraftLetters>>(requestData);
                if (draftLetters != null)
                {
                    var templateId = _dbContext.Template.FirstOrDefault(b => b.TemplateName == draftLetters.FirstOrDefault().TemplateName).TemplateId;
                    draftLetters.ForEach(a => a.TemplateId = templateId);

                    bulkDraftLetterResponse.DraftLetters = draftLetters;
                    bulkDraftLetterResponse.BatchId = batchId;
                }
                return bulkDraftLetterResponse;
            }
        }
        private BaseData GenerateLetterFieldValuesFromPartnerOffice(PartnerOfficeViewModel partnerOffice, int clientId, int partnerId)
        {
            BaseData letterFieldValuesBase = new BaseData();
            var letterFieldValuesList = new List<LetterFieldValuesResponse>();

            foreach (PropertyInfo property in typeof(PartnerOfficeViewModel).GetProperties())
            {

                var letterFields = new LetterFieldValuesResponse
                {
                    FieldName = property.Name,
                    FieldValue = property.GetValue(partnerOffice)?.ToString() ?? string.Empty,
                    ClientId = clientId,
                    FieldId = partnerOffice.FieldId,
                    PartnerId = partnerOffice.PartnerId,
                    LetterVersion = 1
                };
                letterFieldValuesList.Add(letterFields);

            }

            letterFieldValuesBase.LetterFieldValues = letterFieldValuesList;
            return letterFieldValuesBase;
        }
        public async Task<List<BulkLetters>> SaveFetchingLetterAsync(List<BulkLetters> bulkLettersListRes, int batchId, PartnerRequest partnerRequest)
        {

            if (bulkLettersListRes != null && bulkLettersListRes.Any())
            {
                if (batchId > 0)
                {
                    bulkLettersListRes.ForEach(a => a.BatchId = batchId);
                }
            }
            using (var _dbContext = new DashboardDBContext(_dbContextOptions))
            {
                var existingBulkLetters = await _dbContext.BulkLetters
                .Where(b => b.BatchId == batchId)
                .ToListAsync();

                List<BulkLetters> deletedData = new List<BulkLetters>();
                if (bulkLettersListRes.Any())
                {
                    deletedData = existingBulkLetters.Where(item1 =>
                               !bulkLettersListRes.Any(item2 =>
                                   item2.PartnerId.ToString() == item1.PartnerId.ToString()
                                   && item2.ReturnTypeCode == item1.ReturnTypeCode
                               ) && !item1.IsDeleted).ToList();
                }
                else
                {

                    deletedData = existingBulkLetters.Where(a => !partnerRequest.PartnersName.Select(x => x.PartnerId.ToString()).Contains(a.PartnerId.ToString())).ToList();
                    if (deletedData.Any())
                    {
                        deletedData = deletedData.Where(a => partnerRequest.ReturnTpeCode.Select(x => x.ReturnType).Contains(a.ReturnTypeCode)).Where(a => a.IsDeleted == false).ToList();
                    }
                    else
                    {
                        deletedData = existingBulkLetters.Where(a => !partnerRequest.ReturnTpeCode.Select(x => x.ReturnType).Contains(a.ReturnTypeCode)).Where(a => a.IsDeleted == false).ToList();
                    }
                }

                if (deletedData.Any())
                {
                    var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
                    deletedData = deletedData.Select(a =>
                    {
                        a.IsBatchDelete = false;
                        a.IsDeleted = true;
                        a.DeletedOn = estTime;
                        return a;
                    }).ToList();

                    _dbContext.BulkLetters.UpdateRange(deletedData);
                }

                List<BulkLetters> addData = new List<BulkLetters>();
                List<BulkLetters> updateData = new List<BulkLetters>();

                if (bulkLettersListRes.Any())
                {
                    updateData = existingBulkLetters.Where(item1 =>
                           bulkLettersListRes.Any(item2 =>
                               item2.PartnerId.ToString() == item1.PartnerId.ToString()
                               && item2.ReturnTypeCode == item1.ReturnTypeCode)
                           && item1.IsDeleted && !item1.IsBatchDelete).ToList();
                }

                if (updateData.Any())
                {
                    var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
                    updateData = updateData.Select(a =>
                    {
                        a.IsDeleted = false;
                        a.DeletedBy = "";
                        a.ModifiedOn = estTime;
                        return a;
                    }).ToList();

                    _dbContext.BulkLetters.UpdateRange(updateData);
                }

                if (bulkLettersListRes.Any())
                {
                    if (existingBulkLetters.Any())
                    {
                        addData = bulkLettersListRes.Where(item1 =>
                                            !existingBulkLetters.Any(item2 =>
                                                item2.PartnerId.ToString() == item1.PartnerId.ToString()
                                                && item2.ReturnTypeCode == item1.ReturnTypeCode
                                            )).ToList();
                    }
                    else
                    {
                        addData = bulkLettersListRes;
                    }
                    if (addData.Any())
                    {
                        await _dbContext.BulkLetters.AddRangeAsync(addData);
                    }
                }
                if (addData.Any() || updateData.Any() || deletedData.Any())
                {
                    await _dbContext.SaveChangesAsync();
                }

                bulkLettersListRes = await _dbContext.BulkLetters.Where(a => a.BatchId == batchId
                && !a.IsDeleted).ToListAsync();
            }

            return bulkLettersListRes ?? new List<BulkLetters>();
        }
        public async Task<IEnumerable<BatchIdsResponse>> GetBatchIdsAsync()
        {
            using (var _dbContext = new DashboardDBContext(_dbContextOptions))
            {
                IEnumerable<BatchIdsResponse> batchIdsResponses = await _dbContext.BulkLetters.Where(a => !a.IsDeleted).Select(a =>
            new BatchIdsResponse
            {
                BatchId = a.BatchId,

            }).Distinct().OrderByDescending(a => a.BatchId).ToListAsync();

                return batchIdsResponses;
            }
        }
        public async Task<BulkDraftLetterResponse> GetBatchLettersByIdAsync(int batchId)
        {
            BulkDraftLetterResponse bulkDraftLetterResponse = new BulkDraftLetterResponse();
            using (var _dbContext = new DashboardDBContext(_dbContextOptions))
            {
                List<BulkLetters>? batchIdsResponses = await _dbContext.BulkLetters.Where(a => a.BatchId == batchId && !a.IsDeleted)
                .OrderByDescending(a => a.ModifiedOn).ToListAsync();
                List<DraftLetters> draftLetters = _mapper.Map<List<DraftLetters>>(batchIdsResponses);
                if (draftLetters.Count > 0)
                {
                    var templateId = _dbContext.Template.FirstOrDefault(b => b.TemplateName == draftLetters.FirstOrDefault().TemplateName).TemplateId;
                    draftLetters.ForEach(a => a.TemplateId = templateId);


                    bulkDraftLetterResponse.DraftLetters = draftLetters;

                    List<PartnersNames> partnersNames = await _dbContext.BulkLetters.Where(a => a.BatchId == batchId && !a.IsDeleted)
                         .Select(a => new PartnersNames
                         {
                             PartnerId = a.PartnerId.ToString(),
                             PartnerName = a.PartnerName
                         }).Distinct().ToListAsync();

                    List<ReturnTypeModel> returnTyps = await _dbContext.BulkLetters.Where(a => a.BatchId == batchId && !a.IsDeleted)
                        .Select(a => new ReturnTypeModel
                        {
                            ReturnType = a.ReturnTypeCode.ToString()
                        }).Distinct().ToListAsync();

                    string result = string.Empty;
                    var data = draftLetters?.Select(a => a.BulkLettersId).ToList();
                    if (data != null && data.Any())
                    {
                        var latestVersions = _dbContext.LetterFieldValues.Where(a => data.Contains(a.BulkLettersId ?? 0))
                                             .GroupBy(e => e.ClientId)
                                             .Select(group => group.OrderByDescending(e => e.LetterVersion).FirstOrDefault())
                                             .ToList();

                        if (latestVersions != null)
                        {
                            var bulkIds = latestVersions?.Select(a => a.BulkLettersId).ToList();
                            var ids = latestVersions?.Select(a => new { a.BulkLettersId, a.LetterVersion }).ToList();

                            var allLetterFieldValues = _dbContext.LetterFieldValues.Where(s => bulkIds.Contains(s.BulkLettersId)).ToList();

                            if (allLetterFieldValues.Any())
                            {
                                var clientIdsWithEmptyFieldValues = allLetterFieldValues
                                                                               .Where(a => ids.Any(id => id.BulkLettersId == a.BulkLettersId && id.LetterVersion == a.LetterVersion)
                                                                                 && (string.IsNullOrEmpty(a.FieldValue) && a.FieldName != "ChildEntities")
                                                                                 && (string.IsNullOrEmpty(a.FieldValue) && a.FieldName != "SignatoryTitle"))
                                                                               .GroupBy(fieldValue => fieldValue.ClientId)
                                                                               .Select(group => group.Key)
                                                                               .ToList();

                                result = string.Join(",", clientIdsWithEmptyFieldValues);
                            }

                        }
                    }

                    bulkDraftLetterResponse.PartnersNames = partnersNames;
                    bulkDraftLetterResponse.BatchId = batchId;
                    bulkDraftLetterResponse.ClientIdErrorList = result;
                    bulkDraftLetterResponse.GetCurrentDate = DateTime.Now.Date.ToString("yyyy-MM-dd");
                    bulkDraftLetterResponse.ReturnType = returnTyps;
                }
            }
            return bulkDraftLetterResponse;
        }
        public async Task<BulkDraftLetterResponse> GetImportBatchLettersByIdAsync(int batchId)
        {
            BulkDraftLetterResponse bulkDraftLetterResponse = new BulkDraftLetterResponse();
            using (var _dbContext = new DashboardDBContext(_dbContextOptions))
            {
                List<BulkLetters>? batchIdsResponses = await _dbContext.BulkLetters.Where(a => a.BatchId == batchId && !a.IsDeleted)
                .OrderByDescending(a => a.ModifiedOn).ToListAsync();
                List<DraftLetters> draftLetters = _mapper.Map<List<DraftLetters>>(batchIdsResponses);
                if (draftLetters != null)
                {
                    var templateId = _dbContext.Template.FirstOrDefault(b => b.TemplateName == draftLetters.FirstOrDefault().TemplateName).TemplateId;
                    draftLetters.ForEach(a => a.TemplateId = templateId);


                    bulkDraftLetterResponse.DraftLetters = draftLetters;

                    string result = string.Empty;
                    var data = draftLetters?.Select(a => a.BulkLettersId).ToList();
                    if (data != null && data.Any())
                    {
                        var latestVersions = _dbContext.LetterFieldValues.Where(a => data.Contains(a.BulkLettersId ?? 0))
                                             .GroupBy(e => e.ClientId)
                                             .Select(group => group.OrderByDescending(e => e.LetterVersion).FirstOrDefault())
                                             .ToList();

                        if (latestVersions != null)
                        {
                            var bulkIds = latestVersions?.Select(a => a.BulkLettersId).ToList();
                            var ids = latestVersions?.Select(a => new { a.BulkLettersId, a.LetterVersion }).ToList();

                            var allLetterFieldValues = _dbContext.LetterFieldValues.Where(s => bulkIds.Contains(s.BulkLettersId)).ToList();

                            if (allLetterFieldValues.Any())
                            {
                                var clientIdsWithEmptyFieldValues = allLetterFieldValues
                                                                               .Where(a => ids.Any(id => id.BulkLettersId == a.BulkLettersId && id.LetterVersion == a.LetterVersion)
                                                                                 && (string.IsNullOrEmpty(a.FieldValue) && a.FieldName != "ChildEntities")
                                                                                 && (string.IsNullOrEmpty(a.FieldValue) && a.FieldName != "SignatoryTitle"))
                                                                               .GroupBy(fieldValue => fieldValue.ClientId)
                                                                               .Select(group => group.Key)
                                                                               .ToList();

                                result = string.Join(",", clientIdsWithEmptyFieldValues);
                            }

                        }
                    }

                    bulkDraftLetterResponse.BatchId = batchId;
                    bulkDraftLetterResponse.ClientIdErrorList = result;
                }
            }
            return bulkDraftLetterResponse;
        }
        public async Task<IEnumerable<BulkLetterBatchResponse>> GetAllBatchLetterAsync()
        {
            using (var _dbContext = new DashboardDBContext(_dbContextOptions))
            {
                var queryResultss = await _dbContext.BulkLetters
                                .Where(a => !a.IsDeleted)
                                .Select(a => new
                                {
                                    a.BatchId,
                                    a.CreatedBy,
                                    a.PartnerName,
                                    a.Office,
                                    a.TemplateName,
                                    a.DocumentStatusName,
                                    LastUpdatedOn = a.ModifiedOn ?? a.CreatedOn,
                                    a.PartnerId,
                                    a.ModifiedBy
                                }).ToListAsync();

                var finalResult = queryResultss
                                   .GroupBy(a => a.BatchId)
                                   .Select(group => new BulkLetterBatchResponse
                                   {
                                       BatchId = group.Key,
                                       Admin = group.First().ModifiedBy,
                                       PartnerName = group.First().PartnerName,
                                       Office = group.First().Office,
                                       TemplateName = group.First().TemplateName,
                                       DocumentStatus = group.First().DocumentStatusName,
                                       LastUpdatedOn = group.Max(a => a.LastUpdatedOn).ToString("MM/dd/yyyy hh:mm tt"),
                                       NoOfLetters = group.Count(),
                                       PartnerCount = group.Select(a => a.PartnerId).Distinct().Count(),
                                       UpdatedOn = group.Max(a => a.LastUpdatedOn)
                                   })
                                   .OrderByDescending(a => a.UpdatedOn)
                                   .ToList();

                return finalResult;
            }
        }
        public async Task<BulkLetters> CheckExistingBatchId(int batchId)
        {
            BulkLetters? bulkLetters = new BulkLetters();
            if (batchId > 0)
            {
                using (var _dbContext = new DashboardDBContext(_dbContextOptions))
                {
                    var data = await _dbContext.BulkLetters.Where(a => a.BatchId == batchId).FirstOrDefaultAsync();
                    if (data != null)
                    {
                        bulkLetters = await _dbContext.BulkLetters.OrderByDescending(a => a.BatchId).FirstOrDefaultAsync();
                    }
                }
            }

            return bulkLetters ?? new BulkLetters();
        }
        public async Task<string> CheckFieldValues(List<int> bulkLetterIds)
        {
            string result = string.Empty;
            if (bulkLetterIds != null && bulkLetterIds.Any())
            {
                using (var _dbContext = new DashboardDBContext(_dbContextOptions))
                {
                    var latestVersions = _dbContext.LetterFieldValues.Where(a => bulkLetterIds.Contains(a.BulkLettersId ?? 0))
                                     .GroupBy(e => e.ClientId)
                                     .Select(group => group.OrderByDescending(e => e.LetterVersion).FirstOrDefault())
                                     .ToList();

                    if (latestVersions != null)
                    {
                        var bulkIds = latestVersions?.Select(a => a.BulkLettersId).ToList();
                        var ids = latestVersions?.Select(a => new { a.BulkLettersId, a.LetterVersion }).ToList();

                        var allLetterFieldValues = _dbContext.LetterFieldValues.Where(s => bulkIds.Contains(s.BulkLettersId)).ToList();

                        if (allLetterFieldValues.Any())
                        {
                            var clientIdsWithEmptyFieldValues = allLetterFieldValues
                                                                            .Where(a => ids.Any(id => id.BulkLettersId == a.BulkLettersId && id.LetterVersion == a.LetterVersion)
                                                                              && (string.IsNullOrEmpty(a.FieldValue) && a.FieldName != "ChildEntities")
                                                                              && (string.IsNullOrEmpty(a.FieldValue) && a.FieldName != "SignatoryTitle"))
                                                                            .GroupBy(fieldValue => fieldValue.ClientId)
                                                                            .Select(group => group.Key)
                                                                            .ToList();

                            result = string.Join(",", clientIdsWithEmptyFieldValues);
                        }

                    }
                }
            }
            return result;
        }
        public async Task<string> CheckBatchIsActive(int BatchId, string userName)
        {

            string activeUser = string.Empty;
            using (var _dbContext = new DashboardDBContext(_dbContextOptions))
            {
                var data = await _dbContext.BulkLetters.FirstOrDefaultAsync(a => a.BatchId == BatchId && a.IsBatchActive == true);

                if (data != null)
                {
                    var res = data.ModifiedBy ?? data.CreatedBy;
                    if (res != userName)
                    {
                        activeUser = data.ModifiedBy ?? data.CreatedBy;
                    }
                }
                if (string.IsNullOrEmpty(activeUser))
                {
                    var existingData = await _dbContext.BulkLetters.Where(a => a.BatchId == BatchId).ToListAsync();
                    if (existingData.Any())
                    {
                        existingData.ForEach(a =>
                        {
                            a.ModifiedBy = userName;
                            a.IsBatchActive = true;
                        });
                        _dbContext.BulkLetters.UpdateRange(existingData);
                        await _dbContext.SaveChangesAsync();
                    }
                }
            }
            return activeUser;
        }
        public async Task<bool> UpdateIsBatchActive(ActiveUserRequest activeUserRequest)
        {
            using (var _dbContext = new DashboardDBContext(_dbContextOptions))
            {
                var existingData = await _dbContext.BulkLetters.Where(a => a.BatchId == activeUserRequest.BatchId).ToListAsync();
                if (existingData.Any())
                {
                    var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
                    existingData.ForEach(a =>
                    {
                        a.ModifiedBy = activeUserRequest.UserName;
                        a.IsBatchActive = false;
                        a.ModifiedOn = estTime;
                    });
                    _dbContext.BulkLetters.UpdateRange(existingData);
                    await _dbContext.SaveChangesAsync();
                    return true;
                }
            }
            return false;
        }
        public async Task<LetterFieldValuesList> UpdateExistingPartnersRecords(DuplicatePartnersRequest duplicatePartners)
        {
            LetterFieldValuesList letterFieldValuesList = new LetterFieldValuesList();

            if (duplicatePartners.DuplicatePartners.Any())
            {
                if (duplicatePartners.DuplicatePartners.Any())
                {
                    var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
                    using (var _dbContext = new DashboardDBContext(_dbContextOptions))
                    {
                        List<BulkLetters> listOfLetters = new List<BulkLetters>();
                        foreach (var item in duplicatePartners.DuplicatePartners)
                        {
                            List<BulkLetters> resData = _dbContext.BulkLetters.
                                Where(a => a.PartnerId == item.PartnerId && a.ClientId == item.ClientId && a.BatchId == duplicatePartners.BatchId).ToList();

                            listOfLetters.AddRange(resData);

                        }

                        if (listOfLetters.Any())
                        {
                            listOfLetters = listOfLetters.Select(a =>
                            {
                                a.IsDeleted = true;
                                a.DeletedOn = estTime;
                                return a;
                            }).ToList();

                            _dbContext.BulkLetters.UpdateRange(listOfLetters);

                            await _dbContext.SaveChangesAsync();
                        }
                    }
                }
            }

            List<PartnersNames> partnersNames = await _dbContext.BulkLetters
                .Where(a => a.BatchId == duplicatePartners.BatchId && !a.IsDeleted)
               .Select(a => new PartnersNames
               {
                   PartnerId = a.PartnerId.ToString(),
                   PartnerName = a.PartnerName
               }).Distinct().ToListAsync();

            List<BulkLetters> bulkLettersListRes = await _dbContext.BulkLetters.Where(a => a.BatchId == duplicatePartners.BatchId && !a.IsDeleted).ToListAsync();
            List<BulkLettersResponse> resLetters = _mapper.Map<List<BulkLettersResponse>>(bulkLettersListRes);
            letterFieldValuesList.PartnerNames = partnersNames;
            letterFieldValuesList.BulkLettersResponse = resLetters;
            return letterFieldValuesList;
        }
    }
    public class ClientDataComparer : IEqualityComparer<ClientData>
    {
        public bool Equals(ClientData x, ClientData y)
        {
            return x.row_num == y.row_num
                && x.ClientId == y.ClientId
                && x.OfficeId == y.OfficeId
                 && x.PartnerId == y.PartnerId
                 && x.PartnerName == y.PartnerName
                 && x.OfficeName == y.OfficeName
                 && x.OfficeAddress == y.OfficeAddress
                 && x.OfficeCity == y.OfficeCity
                 && x.OfficeState == y.OfficeState
                 && x.OfficePhoneNumber == y.OfficePhoneNumber
                && x.OfficeZipCode == y.OfficeZipCode
                 && x.Date == y.Date
                && x.ClientName == y.ClientName
                 && x.Address == y.Address
                 && x.City == y.City
                 && x.State == y.State
                && x.Zip == y.Zip
                 && x.SignatoryEmail == y.SignatoryEmail
                 && x.SpouseEmail == y.SpouseEmail
                 && x.SignatoryFirstName == y.SignatoryFirstName
                 && x.SignatoryLastName == y.SignatoryLastName
                 && x.SignatoryTitle == y.SignatoryTitle
                            && x.SpouseLastName == y.SpouseLastName
                                && x.Jurisdiction == y.Jurisdiction
                                && x.FiscalYear == y.FiscalYear
                               && x.ELTemplate == y.ELTemplate
                                && x.Expiration7216Form == y.Expiration7216Form;


        }
        public int GetHashCode(ClientData obj)
        {
            return new
            {
                obj.row_num,
                obj.ClientId,
                obj.OfficeId,
                obj.PartnerId,
                obj.PartnerName,
                obj.OfficeName,
                obj.OfficeAddress,
                obj.OfficeCity,
                obj.OfficeState,
                obj.OfficePhoneNumber,
                obj.OfficeZipCode,
                obj.Date,
                obj.ClientName,
                obj.Address,
                obj.City,
                obj.State,
                obj.Zip,
                obj.SignatoryEmail,
                obj.SpouseEmail,
                obj.SignatoryFirstName,
                obj.SignatoryLastName,
                obj.SignatoryTitle,
                obj.SpouseFirstName,
                obj.SpouseLastName,
                obj.Jurisdiction,
                obj.FiscalYear,
                obj.ELTemplate,
                obj.Expiration7216Form
            }.GetHashCode();
        }
    }
}
