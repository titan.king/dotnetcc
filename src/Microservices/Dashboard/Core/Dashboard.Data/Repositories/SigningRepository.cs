﻿using AutoMapper;
using Dashboard.Data.Context;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Dashboard.Models.Signing;
using Engage3.Common.Business.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Resources;

namespace Dashboard.Data.Repositories
{
    public class SigningRepository : ISigningRepository
    {
        private readonly DbContextOptions<DashboardDBContext> _dbContextOptions;
        private readonly DashboardDBContext _dbContext;
        private readonly ResourceManager _resourceManager;
        private readonly IMapper _mapper;
        private readonly IGetCurrentDatetime _getCurrentDatetime;
        public SigningRepository(DbContextOptions<DashboardDBContext> dbContextOptions, DashboardDBContext dbContext, ResourceManager resourceManager, IMapper mapper, IGetCurrentDatetime getCurrentDatetime)
        {
            _dbContextOptions = dbContextOptions;
            _dbContext = dbContext;
            _resourceManager = resourceManager;
            _mapper = mapper;
            _getCurrentDatetime = getCurrentDatetime;
        }

        public async Task<EngagementLetter?> GetEngagementLetterStatus(int engLetterId)
        {

            var query = await _dbContext.EngagementLetter
                        .Where(a => a.EngagementLetterId == engLetterId && !a.IsDeleted).FirstOrDefaultAsync();

            return query;
        }

        public async Task<IEnumerable<int>> GetAllEngagementLetter()
        {
            var query = await _dbContext.EngagementLetter
                        .Where(n => n.DocumentStatusId == 2 && !n.IsDeleted)
                        .Select(n => n.EngagementLetterId)
                        .ToListAsync();
            return query;
        }

        public async Task<string?> GetPdfUrl(int letterId)
        {
            var query = await _dbContext.SigningInfo
                        .Where(n => n.EngagementLetterId == letterId && !n.IsDeleted)
                        .Select(n => n.PdfUrl)
                        .FirstOrDefaultAsync();
            return query;
        }

        public async Task<IEnumerable<SigningInfo>> GetAgreementId()
        {
            var query = await _dbContext.SigningInfo
                             .Where(n => !n.IsDeleted)
                             .OrderBy(n => n.SigningInfoId).ToListAsync();
            return query;
        }

        public async Task<EngagementLetter> UpdateSignedStatusEngagementLetter(EngagementLetter engLetter)
        {
            if (engLetter.EngagementLetterId > 0 && engLetter.DocumentStatusId > 0)
            {
                var estTime = await _getCurrentDatetime.GetCurrentEstDatetime();
                engLetter.ModifiedOn = estTime;
                _dbContext.EngagementLetter.Update(engLetter);
                await _dbContext.SaveChangesAsync();
            }
            return engLetter;
        }

        public async Task<int?> GetHistoryLogVersion(int engId)
        {
            var version = await _dbContext.HistoryLog
                                .Where(n => n.EngagementLetterId == engId)
                                .OrderByDescending(n => n.Version).Select(n => n.Version).FirstOrDefaultAsync();
            return version;
        }

        public async Task<EmailTemplate?> GetEmailTemplate(int? statusId)
        {
            var emailTemp = await _dbContext.EmailTemplate
                          .Where(x => x.EmailTemplateId == statusId)
                          .FirstOrDefaultAsync();

            return emailTemp;
        }

        public async Task<int> GetDocumentStatusId(string description)
        {
            int statusId = await _dbContext.DocumentStatus
                            .Where(x => x.Description == description && !x.IsDeleted)
                            .Select(x => x.DocumentStatusId).FirstOrDefaultAsync();

            return statusId;
        }

        public async Task<int> GetLetterStatusReport(int engId)
        {
            int engagementIdExists = await _dbContext.LettersStatusReports
                                      .Where(x => x.EngagementLetterId == engId).Select(x => x.LettersStatusReportId).FirstOrDefaultAsync();

            return engagementIdExists;
        }

        public async Task InsertHistoryLog(HistoryLog historyLog)
        {
            if (historyLog != null)
            {
                await _dbContext.HistoryLog.AddAsync(historyLog);
                await _dbContext.SaveChangesAsync();
            }
        }

        public async Task<string?> GetHistoryLogStatus(int engId)
        {
            var status = await _dbContext.HistoryLog.Where(n => n.EngagementLetterId == engId).OrderByDescending(n => n.Version)
                         .Select(n => n.Status).FirstOrDefaultAsync();
            return status;
        }
        public async Task<LetterStatusRequest> InsertLetterStatusReports(LetterStatusRequest letterStatusRequest)
        {
            var insertData = _mapper.Map<LettersStatusReports>(letterStatusRequest);
            if (insertData != null && insertData.LettersStatusReportId == 0)
            {
                await _dbContext.LettersStatusReports.AddAsync(insertData);
                await _dbContext.SaveChangesAsync();
            }
            return letterStatusRequest;
        }

        public async Task<LetterStatusRequest> UpdateLetterStatusReports(LetterStatusRequest letterStatusRequest)
        {
            var insertData = _mapper.Map<LettersStatusReports>(letterStatusRequest);
            if (insertData != null && insertData.LettersStatusReportId > 0)
            {
                _dbContext.LettersStatusReports.Update(insertData);
                await _dbContext.SaveChangesAsync();
            }
            return letterStatusRequest;
        }

        public async Task<EngagementLetterList> GetEngagmentLettersByID(int id)
        {
            var letterbyidlist = await (from a in _dbContext.EngagementLetter
                                        join c in _dbContext.Department on a.DepartmentId equals c.DepartmentId
                                        join e in _dbContext.EngageType on a.EngageTypeId equals e.EngageTypeId
                                        join g in _dbContext.DocumentStatus on a.DocumentStatusId equals g.DocumentStatusId
                                        where a.IsDeleted == false && a.EngagementLetterId == id
                                        orderby a.ModifiedOn descending
                                        select new EngagementLetterList
                                        {
                                            EngagementLetterId = a.EngagementLetterId,
                                            BatchId = a.BatchId,
                                            BulkLettersId = a.BulkLettersId,
                                            OfficeId = a.OfficeId,
                                            OfficeName = a.OfficeName ?? string.Empty,
                                            AdminId = a.AdminId,
                                            AdminName = a.AdminName ?? string.Empty,
                                            EngagementLetterName = a.EngagementLetterName ?? string.Empty,
                                            ClientId = a.ClientId,
                                            ClientName = a.ClientName ?? string.Empty,
                                            DepartmentId = a.DepartmentId,
                                            DepartmentName = c.DepartmentName,
                                            EngageTypeId = a.EngageTypeId,
                                            EngageTypeName = e.EngageTypeName,
                                            PartnerId = a.PartnerId,
                                            PartnerName = a.PartnerName ?? string.Empty,
                                            CreatedBy = a.CreatedBy ?? string.Empty,
                                            ModifiedOn = a.ModifiedOn,
                                            DocumentStatusId = a.DocumentStatusId,
                                            DocumentDescription = g.Description,
                                            TemplateName = a.TemplateName ?? string.Empty,
                                            TemplateId = a.TemplateId,
                                            TemplateVersion = a.TemplateVersion,
                                            YearId = a.YearId,
                                            TaxYear = a.TaxYear,
                                            SignatoryEmailId = a.SignatoryEmailId ?? string.Empty,
                                            ReturnTypeCode = a.ReturnTypeCode ?? string.Empty,
                                            ClientSignatureCount = a.ClientSignatureCount
                                        }).FirstOrDefaultAsync();
            return letterbyidlist;
        }
        public async Task<EmailTemplate?> GetEmailContent(int StatusId)
        {
            if (StatusId <= 0)
            {
                return null;
            }

            using (var _dbContext = new DashboardDBContext(_dbContextOptions))
            {
                return await _dbContext.EmailTemplate.FirstOrDefaultAsync(n => n.DocumentStatusId == StatusId);
            }
        }
    }
}

