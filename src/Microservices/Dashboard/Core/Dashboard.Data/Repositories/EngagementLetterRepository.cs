﻿using AutoMapper;
using Dashboard.Data.Context;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Models.EngagementLetter;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;
using Engage3.Common.Business;
using Engage3.Common.Business.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Globalization;

namespace Dashboard.Data.Repositories
{
    public class EngagementLetterRepository : IEngagementLetterRepository
    {
        private readonly DbContextOptions<DashboardDBContext> _dbContextOptions;
        private readonly DashboardDBContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IGetCurrentDatetime _getCurrentDateTime;
        public EngagementLetterRepository(DbContextOptions<DashboardDBContext> dbContextOptions, DashboardDBContext dbContext, IMapper mapper, IGetCurrentDatetime getCurrentDateTime)
        {
            _dbContextOptions = dbContextOptions;
            _dbContext = dbContext;
            _mapper = mapper;
            _getCurrentDateTime = getCurrentDateTime;
        }
        public async Task<IEnumerable<EngagementLetter>> GetEngagementLettersSearchFilter()
        {
            try
            {
                var query = await _dbContext.EngagementLetter.OrderByDescending(x => x.EngagementLetterId)
                    .Where(x => x.IsDeleted == false)
                    .Select(x => new EngagementLetter
                    {
                        EngagementLetterId = x.EngagementLetterId,
                        EngagementLetterName = x.EngagementLetterName ?? string.Empty,
                        BatchId = x.BatchId,
                        OfficeId = x.OfficeId,
                        OfficeName = x.OfficeName ?? string.Empty,
                        TaxYear = x.TaxYear ?? string.Empty,
                        TemplateId = x.TemplateId,
                        TemplateName = x.TemplateName ?? string.Empty,
                        ClientId = x.ClientId,
                        ClientName = x.ClientName ?? string.Empty,
                        PartnerId = x.PartnerId,
                        PartnerName = x.PartnerName ?? string.Empty,
                        AdminName = x.AdminName ?? string.Empty,
                        DocumentStatusId = x.DocumentStatusId,
                        EngageTypeId = x.EngageTypeId,
                        DepartmentId = x.DepartmentId,
                        CreatedBy = x.CreatedBy ?? string.Empty,
                        CreatedOn = x.CreatedOn,
                        ModifiedBy = x.ModifiedBy ?? string.Empty,
                        ModifiedOn = x.ModifiedOn,
                        BulkLettersId = x.BulkLettersId,
                        ClientSignatureCount = x.ClientSignatureCount,
                        Is7216Available = x.Is7216Available,
                        IsEnqueue = x.IsEnqueue,
                        IsNewClient = x.IsNewClient,
                        IsProcess = x.IsProcess,
                        SignatoryEmailId = x.SignatoryEmailId ?? string.Empty,
                        SignatoryFirstName = x.SignatoryFirstName ?? string.Empty,
                        SignatoryLastName = x.SignatoryLastName ?? string.Empty,
                        SpouseEmailId = x.SpouseEmailId ?? string.Empty,
                        SpouseFirstName = x.SpouseFirstName ?? string.Empty,
                        SpouseLastName = x.SpouseLastName ?? string.Empty,
                        //IsEsigning = x.IsEsigning != null ? x.IsEsigning : false,
                        OtherEntityDetails = x.OtherEntityDetails ?? string.Empty,
                        PrimarySignerLastName = x.PrimarySignerLastName ?? string.Empty,
                        AdminEmail = x.AdminEmail ?? string.Empty,
                        PartnerEmail = x.PartnerEmail ?? string.Empty,
                        SignatoryTitle = x.SignatoryTitle ?? string.Empty
                    }).ToListAsync();

                return query;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<string> GetSigningInfoDetail(int engLetterId)
        {
            var agreementId = await _dbContext.SigningInfo
                                .Where(n => n.EngagementLetterId == engLetterId)
                                .Select(n => n.AgreementId)
                                .FirstOrDefaultAsync();

            return agreementId;
        }


        public async Task<EngagementLetter> GetEngagementLetterStatus(int engLetterId)
        {

            var query = await _dbContext.EngagementLetter
                        .Where(a => a.EngagementLetterId == engLetterId && !a.IsDeleted).FirstOrDefaultAsync();

            return query;
        }

        public async Task<EngagementLetter> UpdateSignedStatusEngagementLetter(EngagementLetter engLetter)
        {
            if (engLetter.EngagementLetterId > 0 && engLetter.DocumentStatusId > 0)
            {
                var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();

                engLetter.ModifiedOn = estTime;
                _dbContext.EngagementLetter.Update(engLetter);
                await _dbContext.SaveChangesAsync();
            }
            return engLetter;
        }

        public async Task<IEnumerable<Department>> GetDepartmentSearchFilter()
        {
            try
            {
                var query = await _dbContext.Department.ToListAsync();

                return query;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public async Task<IEnumerable<EngageType>> GetEngageTypeSearchFilter()
        {
            try
            {
                var query = await _dbContext.EngageType.ToListAsync();

                return query;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<IEnumerable<DocumentStatus>> GetDocumentStatusSearchFilter()
        {
            try
            {
                var query = await _dbContext.DocumentStatus.ToListAsync();

                return query;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<IEnumerable<EngagementLetterList>> GetEngagementLetters()
        {
            var fieldsList = new List<EngagementLetterList>();
            try
            {
                using (var _dbContext = new DashboardDBContext(_dbContextOptions))
                {
                    fieldsList = await (from a in _dbContext.EngagementLetter
                                        join c in _dbContext.Department on a.DepartmentId equals c.DepartmentId
                                        join e in _dbContext.EngageType on a.EngageTypeId equals e.EngageTypeId
                                        join g in _dbContext.DocumentStatus on a.DocumentStatusId equals g.DocumentStatusId
                                        where a.IsDeleted == false
                                        orderby a.ModifiedOn descending
                                        select new EngagementLetterList
                                        {
                                            EngagementLetterId = a.EngagementLetterId,
                                            BatchId = a.BatchId,
                                            BulkLettersId = a.BulkLettersId,
                                            OfficeId = a.OfficeId,
                                            OfficeName = a.OfficeName ?? string.Empty,
                                            AdminId = a.AdminId,
                                            AdminName = a.AdminName ?? string.Empty,
                                            EngagementLetterName = a.EngagementLetterName ?? string.Empty,
                                            ClientId = a.ClientId,
                                            ClientName = a.ClientName ?? string.Empty,
                                            DepartmentId = a.DepartmentId,
                                            DepartmentName = c.DepartmentName,
                                            EngageTypeId = a.EngageTypeId,
                                            EngageTypeName = e.EngageTypeName,
                                            PartnerId = a.PartnerId,
                                            PartnerName = a.PartnerName ?? string.Empty,
                                            CreatedBy = a.CreatedBy ?? string.Empty,
                                            ModifiedOn = a.ModifiedOn,
                                            DocumentStatusId = a.DocumentStatusId,
                                            DocumentDescription = g.Description,
                                            TemplateName = a.TemplateName ?? string.Empty,
                                            TemplateId = a.TemplateId,
                                            TemplateVersion = a.TemplateVersion,
                                            YearId = a.YearId,
                                            TaxYear = a.TaxYear,
                                            SignatoryEmailId = a.SignatoryEmailId ?? string.Empty
                                        }).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return fieldsList;
        }
        public async Task<EngagementLetterList> GetEngagementLettersById(int bulkletterId)
        {
            var fieldsList = new EngagementLetterList();
            try
            {
                using (var _dbContext = new DashboardDBContext(_dbContextOptions))
                {
                    fieldsList = await (from a in _dbContext.EngagementLetter
                                        join c in _dbContext.Department on a.DepartmentId equals c.DepartmentId
                                        join e in _dbContext.EngageType on a.EngageTypeId equals e.EngageTypeId
                                        join g in _dbContext.DocumentStatus on a.DocumentStatusId equals g.DocumentStatusId
                                        where a.IsDeleted == false && a.BulkLettersId == bulkletterId
                                        orderby a.ModifiedOn descending
                                        select new EngagementLetterList
                                        {
                                            EngagementLetterId = a.EngagementLetterId,
                                            BatchId = a.BatchId,
                                            BulkLettersId = a.BulkLettersId,
                                            OfficeId = a.OfficeId,
                                            OfficeName = a.OfficeName ?? string.Empty,
                                            AdminId = a.AdminId,
                                            AdminName = a.AdminName ?? string.Empty,
                                            EngagementLetterName = a.EngagementLetterName ?? string.Empty,
                                            ClientId = a.ClientId,
                                            ClientName = a.ClientName ?? string.Empty,
                                            DepartmentId = a.DepartmentId,
                                            DepartmentName = c.DepartmentName,
                                            EngageTypeId = a.EngageTypeId,
                                            EngageTypeName = e.EngageTypeName,
                                            PartnerId = a.PartnerId,
                                            PartnerName = a.PartnerName ?? string.Empty,
                                            CreatedBy = a.CreatedBy ?? string.Empty,
                                            ModifiedOn = a.ModifiedOn,
                                            DocumentStatusId = a.DocumentStatusId,
                                            DocumentDescription = g.Description,
                                            TemplateName = a.TemplateName ?? string.Empty,
                                            TemplateId = a.TemplateId,
                                            TemplateVersion = a.TemplateVersion,
                                            YearId = a.YearId,
                                            TaxYear = a.TaxYear,
                                            SignatoryEmailId = a.SignatoryEmailId ?? string.Empty,
                                            SignatoryFirstName = a.SignatoryFirstName ?? string.Empty,
                                            SpouseEmailId = a.SpouseEmailId ?? string.Empty,
                                            SpouseFirstName = a.SpouseFirstName ?? string.Empty,
                                            ClientSignatureCount = a.ClientSignatureCount ?? 0,
                                            OtherEntityDetails = a.OtherEntityDetails ?? string.Empty,
                                            PrimarySignerLastName = a.PrimarySignerLastName ?? string.Empty
                                        }).FirstOrDefaultAsync();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return fieldsList;
        }
        public async Task<IEnumerable<EngagementLetterSearchList>> GetFilteredSearchLetters(EngagementLetterSearchList Filter)
        {
            var resultList = new List<EngagementLetterSearchList>();
            using (var _dbContext = new DashboardDBContext(_dbContextOptions))
            {
                resultList = await _dbContext.EngagementLetter
                                            .GroupJoin(
                                            _dbContext.Department,
                                            a => a.DepartmentId,
                                            d => d.DepartmentId,
                                            (a, de) => new { a, de }
                                            )
                                            .SelectMany(
                                            x => x.de.DefaultIfEmpty(),
                                            (x, e) => new { x.a, e }
                                            )
                                            .GroupJoin(
                                            _dbContext.DocumentStatus,
                                            x => x.a.DocumentStatusId,
                                            l => l.DocumentStatusId,
                                            (x, lm) => new { x.a, x.e, lm }
                                            )
                                            .SelectMany(
                                                x => x.lm.DefaultIfEmpty(),
                                                (x, m) => new { x.a, x.e, m }
                                            )
                                            .GroupJoin(
                                            _dbContext.EngageType,
                                            j => j.a.EngageTypeId,
                                            k => k.EngageTypeId,
                                            (j, kl) => new { j.a, j.e, j.m, kl }
                                            )
                                            .SelectMany(
                                                j => j.kl.DefaultIfEmpty(),
                                                (j, n) => new { j.a, j.e, j.m, n }
                                            )
                                            .Where(x =>
                                                (Filter.EngagementLetterId == 0 || x.a.EngagementLetterId == Filter.EngagementLetterId) &&
                                                (Filter.ClientId == 0 || x.a.ClientId == Filter.ClientId) &&
                                                (string.IsNullOrEmpty(Filter.ClientName) || x.a.ClientName == Filter.ClientName) &&
                                                (string.IsNullOrEmpty(Filter.OfficeName) || x.a.OfficeName == Filter.OfficeName) &&
                                                (string.IsNullOrEmpty(Filter.TemplateName) || x.a.TemplateName == Filter.TemplateName) &&
                                                (string.IsNullOrEmpty(Filter.TaxYear) || x.a.TaxYear == Filter.TaxYear) &&
                                                (Filter.DepartmentId == 0 || x.a.DepartmentId == Filter.DepartmentId) &&
                                                (Filter.EngageTypeId == 0 || x.a.EngageTypeId == Filter.EngageTypeId) &&
                                                (string.IsNullOrEmpty(Filter.AdminName) || x.a.AdminName == Filter.AdminName) &&
                                                (Filter.DocumentStatusId == 0 || x.a.DocumentStatusId == Filter.DocumentStatusId) &&
                                                (string.IsNullOrEmpty(Filter.PartnerName) || x.a.PartnerName == Filter.PartnerName) &&
                                                (string.IsNullOrEmpty(Filter.EngagementLetterName) || x.a.EngagementLetterName.Contains(Filter.EngagementLetterName)) &&
                                                !x.a.IsDeleted
                                            )
                                            .OrderByDescending(x => x.a.ModifiedOn)
                                            .Select(x => new EngagementLetterSearchList
                                            {
                                                EngagementLetterId = x.a.EngagementLetterId,
                                                BatchId = x.a.BatchId,
                                                EngagementLetterName = x.a.EngagementLetterName,
                                                ClientName = x.a.ClientName,
                                                TaxYear = x.a.TaxYear,
                                                DepartmentName = x.e.DepartmentName,
                                                PartnerName = x.a.PartnerName,
                                                DocumentDescription = x.m.Description,
                                                EngageTypeName = x.n.EngageTypeName,
                                                CreatedBy = x.a.CreatedBy,
                                                LastModifieds = x.a.ModifiedOn,
                                                OfficeName = x.a.OfficeName,
                                                AdminName = x.a.AdminName,
                                                TemplateName = x.a.TemplateName
                                            })
                                            .ToListAsync();

                return resultList;
            }
        }

        public async Task<bool> DeleteEngagementLetter(DeleteEngagementLetterResModel engLetterId)
        {
            var byEngId = await _dbContext.EngagementLetter.FirstOrDefaultAsync(b => b.EngagementLetterId == engLetterId.EngagementLetterId);
            if (byEngId != null)
            {
                var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
                byEngId.IsDeleted = true;
                byEngId.DeletedBy = engLetterId.DeletedBy;
                byEngId.DeletedOn = estTime;
                byEngId.ModifiedOn = estTime;
                await _dbContext.SaveChangesAsync();
            }
            return true;
        }
        public async Task<List<HistoryLogResponseModel>> GetHistoryLogById(int engagementLetterId)
        {
            using (var _dbContext = new DashboardDBContext(_dbContextOptions))
            {
                var getHistoryLogList = await _dbContext.HistoryLog.GroupJoin(
                                            _dbContext.EngagementLetter,
                                            a => a.EngagementLetterId,
                                            d => d.EngagementLetterId,
                                            (a, de) => new { a, de }
                                            )
                                            .SelectMany(
                                            x => x.de.DefaultIfEmpty(),
                                            (x, e) => new { x.a, e }
                                            ).Where(x => x.a.EngagementLetterId == engagementLetterId).OrderBy(n => n.a.Version)
                                            .Select(x => new HistoryLogResponseModel
                                            {
                                                EngagementLetterId = x.a.EngagementLetterId,
                                                BatchId = x.a.BatchId,
                                                EngagementLetterName = x.a.EngagementLetterName,
                                                Status = x.a.Status,
                                                EditedBy = x.a.EditedBy,
                                                Version = x.a.Version,
                                                ModifiedOn = x.a.LastModified != null ? x.a.LastModified.Value.ToString("MM/dd/yyyy hh:mm tt", CultureInfo.InvariantCulture) : null,
                                                ClientEmailId = x.a.ClientEmailId,
                                                ReasonforDecline = x.a.ReasonforDecline,
                                                DeclineTimestamp = x.a.DeclineTimestamp != null ? x.a.DeclineTimestamp.Value.ToString("MM/dd/yyyy hh:mm tt", CultureInfo.InvariantCulture) : null,
                                                PDFUrl = x.a.PDFUrl,
                                                BulkLettersId = x.e.BulkLettersId,
                                                TemplateId = x.e.TemplateId,
                                                TemplateName = x.e.TemplateName,
                                                ClientId = x.e.ClientId,
                                                ClientName = x.e.ClientName
                                            }).ToListAsync();
                
                return getHistoryLogList;
            }
        }
        public async Task<bool> SaveEngagementLetterEditFields(EditFieldValuesResponse editField)
        {
            bool response = false;
            if (editField != null && editField.EditFieldsValues.Any())
            {
                int latestVersion = 0;
                var bulkData = await _dbContext.BulkLetters.FirstOrDefaultAsync(a => a.BulkLettersId == editField.BulkLettersId);
                var getLatestVersion = _dbContext.LetterFieldValues.OrderByDescending(n => n.LetterVersion).FirstOrDefault(n => n.BulkLettersId == editField.BulkLettersId)?.LetterVersion ?? 0;
                var getEngagementLetter = await _dbContext.EngagementLetter.FirstOrDefaultAsync(a => a.BulkLettersId == editField.BulkLettersId);

                var listOfBulkLetterFields = _mapper.Map<List<LetterFieldValues>>(editField.EditFieldsValues);

                if (bulkData != null && listOfBulkLetterFields.Any())
                {
                    latestVersion = getLatestVersion + 1;
                    listOfBulkLetterFields.ForEach(a => a.LetterVersion = latestVersion);
                    await _dbContext.LetterFieldValues.AddRangeAsync(listOfBulkLetterFields);

                    var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
                    bulkData.ModifiedOn = estTime;
                    bulkData.ClientSignatureCount = editField.ClientSignatureCount;
                    bulkData.Is7216Available = editField.Is7216Available;
                    bulkData.BulkLettersId = editField.BulkLettersId;
                    bulkData.IsEsigning = editField.IsEsigning;
                    bulkData.ModifiedBy = editField.EditedBy ?? string.Empty;

                    await _dbContext.SaveChangesAsync();
                    response = true;
                }
                if (getEngagementLetter != null && getEngagementLetter.EngagementLetterId > 0)
                {
                    var getClientName = listOfBulkLetterFields.Where(n => n.FieldName == "ClientName").Select(n => n.FieldValue).FirstOrDefault();
                    string replacedLetterName = getEngagementLetter.TaxYear + " " + getClientName + " " + getEngagementLetter.TemplateName;

                    var getTaxYear = listOfBulkLetterFields.Where(n => n.FieldName == "TaxYear").Select(n => n.FieldValue).FirstOrDefault();
                    var getSignatoryTitle = listOfBulkLetterFields.Where(n => n.FieldName == "SignatoryTitle").Select(n => n.FieldValue).FirstOrDefault();
                    var getSignatoryLastName = listOfBulkLetterFields.Where(n => n.FieldName == "SignatoryLastName").Select(n => n.FieldValue).FirstOrDefault();
                    var getSignatoryFirstName = listOfBulkLetterFields.Where(n => n.FieldName == "SignatoryFirstName").Select(n => n.FieldValue).FirstOrDefault();
                    var getsignatoryEmailId = listOfBulkLetterFields.Where(n => n.FieldName == "SignatoryEmailId").Select(n => n.FieldValue).FirstOrDefault();
                    var getPartnerName = listOfBulkLetterFields.Where(n => n.FieldName == "PartnerName").Select(n => n.FieldValue).FirstOrDefault();
                    var getOfficeName = listOfBulkLetterFields.Where(n => n.FieldName == "OfficeName").Select(n => n.FieldValue).FirstOrDefault();
                    var getspouseEmailId = listOfBulkLetterFields.Where(n => n.FieldName == "SpouseEmailId").Select(n => n.FieldValue).FirstOrDefault();
                    var getspouseFirstName = listOfBulkLetterFields.Where(n => n.FieldName == "SpouseFirstName").Select(n => n.FieldValue).FirstOrDefault();
                    var getspouselastName = listOfBulkLetterFields.Where(n => n.FieldName == "SpouseLastName").Select(n => n.FieldValue).FirstOrDefault();

                    getEngagementLetter.EngagementLetterName = replacedLetterName ?? string.Empty;
                    getEngagementLetter.TaxYear = getTaxYear ?? string.Empty;
                    getEngagementLetter.SignatoryTitle = getSignatoryTitle ?? string.Empty;
                    getEngagementLetter.SignatoryLastName = getSignatoryLastName ?? string.Empty;
                    getEngagementLetter.SignatoryFirstName = getSignatoryFirstName ?? string.Empty;
                    getEngagementLetter.SignatoryEmailId = getsignatoryEmailId ?? string.Empty;
                    getEngagementLetter.PartnerName = getPartnerName ?? string.Empty;
                    getEngagementLetter.OfficeName = getOfficeName ?? string.Empty;
                    getEngagementLetter.Is7216Available = editField.Is7216Available;
                    getEngagementLetter.IsEsigning = editField.IsEsigning ?? false;
                    getEngagementLetter.ClientSignatureCount = editField.ClientSignatureCount;
                    getEngagementLetter.ModifiedBy = editField.EditedBy ?? string.Empty;
                    getEngagementLetter.AdminName = editField.EditedBy ?? string.Empty;
                    getEngagementLetter.SpouseEmailId = getspouseEmailId ?? string.Empty;
                    getEngagementLetter.SpouseLastName = getspouselastName ?? string.Empty;
                    getEngagementLetter.SpouseFirstName = getspouseFirstName ?? string.Empty;

                    var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
                    getEngagementLetter.ModifiedOn = estTime;
                    getEngagementLetter.ClientName = getClientName ?? string.Empty;

                    await _dbContext.SaveChangesAsync();
                    response = true;
                }
                if (getEngagementLetter != null)
                {
                    var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
                    var getLatestLogVersionRecord = await _dbContext.HistoryLog.OrderByDescending(n => n.Version).FirstOrDefaultAsync(n => n.EngagementLetterId == getEngagementLetter.EngagementLetterId);
                    var getLatestLogVersion = getLatestLogVersionRecord?.Version;
                    HistoryLogResponseModel historyLog = new HistoryLogResponseModel();
                    historyLog.EngagementLetterId = getEngagementLetter.EngagementLetterId;
                    historyLog.EngagementLetterName = getEngagementLetter.EngagementLetterName;
                    historyLog.BatchId = getEngagementLetter.BatchId;
                    historyLog.EditedBy = editField.EditedBy;
                    historyLog.Status = "Fields Edited";
                    historyLog.Version = getLatestLogVersion + 1;
                    historyLog.LastModified = estTime;
                    var logResp = _mapper.Map<HistoryLog>(historyLog);
                    await _dbContext.AddAsync(logResp);
                    await _dbContext.SaveChangesAsync();
                    response = true;
                }
            }

            return response;
        }
    }
}
