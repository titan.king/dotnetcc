﻿using AutoMapper;
using Dashboard.Data.Context;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Engage3.Common.Business.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Dashboard.Data.Repositories
{
    public class TemplateRepository : ITemplateRespository
    {
        private readonly DashboardDBContext _dbContext;
        private readonly IMapper _mapper;
        private readonly DbContextOptions<DashboardDBContext> _dbContextOptions;
        private readonly IGetCurrentDatetime _getCurrentDatetime;
        public TemplateRepository(DashboardDBContext dbContext, IMapper mapper, DbContextOptions<DashboardDBContext> dbContextOptions, IGetCurrentDatetime getCurrentDatetime)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _dbContextOptions = dbContextOptions;
            _getCurrentDatetime = getCurrentDatetime;
        }

        public async Task<TemplateListModel> SaveTemplate(TemplateListModel template)
        {
            var tempData = _mapper.Map<Template>(template);

            if (tempData.TemplateId == 0)
            {
                var estTime = await _getCurrentDatetime.GetCurrentEstDatetime();
                tempData.CreatedOn = estTime;
                tempData.ModifiedOn = estTime;
                await _dbContext.Template.AddAsync(tempData);
                await _dbContext.SaveChangesAsync();

                if (tempData.TemplateId > 0)
                {
                    template.TemplateId = tempData.TemplateId;
                    template = await SaveTemplateVersion(template);
                    if (template.TemplateId > 0 && template.TemplateVersionId > 0)
                    {
                        await SaveMasterTable(template);
                        if (template.AttachmentJson != null || template.AttachmentJson != string.Empty)
                        {
                            await SaveAttachments(template);
                        }
                    }
                }
            }
            return template;
        }

        public async Task<TemplateListModel> SaveTemplateVersion(TemplateListModel template)
        {
            int templateId = template.TemplateId;
            int templateVersionNumber = 1;
            var insertTemplateVersion = new TemplateVersion
            {
                TemplateId = templateId,
                TemplateLogic = template.TemplateLogic,
                VersionNumber = templateVersionNumber
            };
            await _dbContext.TemplateVersion.AddAsync(insertTemplateVersion);
            await _dbContext.SaveChangesAsync();
            template.TemplateVersionId = insertTemplateVersion.TemplateVersionId;
            return template;
        }
        public async Task<MasterTemplate> SaveMasterTable(TemplateListModel template)
        {
            int templateID = template.TemplateId;
            var insertMasterTemplate = new MasterTemplate
            {
                TemplateId = templateID,
                Template = template.TemplateHtml ?? string.Empty,
                TemplateName = template.TemplateName ?? string.Empty,
                TemplateVersionId = template.TemplateVersionId
            };
            await _dbContext.MasterTemplate.AddAsync(insertMasterTemplate);
            await _dbContext.SaveChangesAsync();
            return insertMasterTemplate;
        }
        public async Task<TemplateAttachments> SaveAttachments(TemplateListModel template)
        {
            int templateID = template.TemplateId;
            var estTime = await _getCurrentDatetime.GetCurrentEstDatetime();
            var insertTemplateAttachments = new TemplateAttachments
            {
                TemplateId = templateID,
                AttachmentJson = template.AttachmentJson,
                CreatedOn = estTime,
                ModifiedOn = estTime
            };
            await _dbContext.TemplateAttachments.AddAsync(insertTemplateAttachments);
            await _dbContext.SaveChangesAsync();
            return insertTemplateAttachments;
        }

        public async Task<TemplateListModel> UpdateTemplate(TemplateListModel template)
        {
            var tempData = _mapper.Map<Template>(template);
          
            if (tempData.TemplateId > 0)
            {
                var estTime = await _getCurrentDatetime.GetCurrentEstDatetime();
                tempData.ModifiedOn = estTime;
                _dbContext.Template.Update(tempData);
                await _dbContext.SaveChangesAsync();

                template.TemplateId = tempData.TemplateId;
                await UpdateTemplateVersion(template);

            }

            return template;
        }

        public async Task<TemplateListModel> UpdateTemplateVersion(TemplateListModel template)
        {
            if (template.TemplateId > 0)
            {
                var existingData = await _dbContext.TemplateVersion.OrderByDescending(n => n.VersionNumber).FirstOrDefaultAsync(n => n.TemplateId == template.TemplateId);

                int templateId = template.TemplateId;
                int existVersionNum = (int)existingData.VersionNumber;
                int templateVersionNumber = existVersionNum + 1;
                var insertTemplateVersion = new TemplateVersion
                {
                    TemplateId = templateId,
                    TemplateLogic = template.TemplateLogic,
                    VersionNumber = templateVersionNumber
                };
                await _dbContext.TemplateVersion.AddAsync(insertTemplateVersion);
                await _dbContext.SaveChangesAsync();
                template.TemplateVersionId = insertTemplateVersion.TemplateVersionId;
            }
            return template;
        }
        public async Task<MasterTemplate> UpdateMasterTable(TemplateListModel template)
        {
            int templateID = template.TemplateId;
            var insertMasterTemplate = new MasterTemplate
            {
                TemplateId = templateID,
                Template = template.TemplateHtml ?? string.Empty,
                TemplateName = template.TemplateName ?? string.Empty,
                TemplateVersionId = template.TemplateVersionId
            };
            await _dbContext.MasterTemplate.AddAsync(insertMasterTemplate);
            await _dbContext.SaveChangesAsync();
            return insertMasterTemplate;
        }
        public async Task<TemplateAttachments> UpdateAttachments(TemplateListModel template)
        {
            var existingData = await _dbContext.TemplateAttachments.OrderByDescending(n => n.TemplateAttachmentsId).FirstOrDefaultAsync(n => n.TemplateId == template.TemplateId);
            int templateID = template.TemplateId;
            var estTime = await _getCurrentDatetime.GetCurrentEstDatetime();
            var insertTemplateAttachments = new TemplateAttachments
            {
                TemplateId = templateID,
                AttachmentJson = template.AttachmentJson,
                CreatedOn = existingData.CreatedOn,
                ModifiedOn = estTime
            };
            _dbContext.TemplateAttachments.Update(insertTemplateAttachments);
            await _dbContext.SaveChangesAsync();
            return insertTemplateAttachments;
        }

        public async Task<IEnumerable<DepartmentsList>> GetDepartments()
        {
            var getList = new List<DepartmentsList>();
            getList = await _dbContext.Department
                        .Where(n => !n.IsDeleted)
                        .OrderBy(n => n.DepartmentName).Select(n => new DepartmentsList
                        {
                            DepartmentId = n.DepartmentId,
                            Department = n.DepartmentName ?? string.Empty
                        }).ToListAsync();
            return getList;
        }
        public async Task<IEnumerable<TemplateListModel>> GetTemplates()
        {
            var getList = new List<TemplateListModel>();
            getList = await (from A in _dbContext.Template
                             where !A.IsDeleted
                             join B in _dbContext.Department on A.DepartmentId equals B.DepartmentId into departmentGroup
                             from B in departmentGroup.DefaultIfEmpty()
                             join D in _dbContext.EngageType on A.EngageTypeId equals D.EngageTypeId into engageTypeGroup
                             from D in engageTypeGroup.DefaultIfEmpty()
                             join F in _dbContext.Status on A.StatusId equals F.StatusId into statusGroup
                             from F in statusGroup.DefaultIfEmpty()
                             join G in _dbContext.TemplateVersion on A.TemplateId equals G.TemplateId
                             join H in
                             (
                                 from T in _dbContext.TemplateVersion
                                 group T by T.TemplateId into templateVersionGroup
                                 select new { Id = templateVersionGroup.Max(t => t.TemplateVersionId) }
                             ) on G.TemplateVersionId equals H.Id
                             orderby A.ModifiedOn descending
                             select new TemplateListModel
                             {
                                 TemplateId = A.TemplateId,
                                 TemplateName = A.TemplateName,
                                 TemplateDescription = A.TemplateDescription,
                                 DepartmentName = B.DepartmentName ?? string.Empty,
                                 EngageTypeName = D.EngageTypeName ?? string.Empty,
                                 StatusName = F.StatusName ?? string.Empty,
                                 ChangeNotes = A.ChangeNotes ?? string.Empty,
                                 TemplateLogic = G.TemplateLogic,
                                 VersionNumber = G.VersionNumber,
                                 CreatedBy = A.CreatedBy,
                                 DepartmentId = A.DepartmentId,
                                 EngageTypeId = A.EngageTypeId,
                                 AttachmentCount = A.AttachmentCount,
                                 Is7216Available = A.Is7216Available,
                                 ClientSignatureCount = A.ClientSignatureCount,
                             }).ToListAsync();

            return getList;
        }
        public async Task<TemplateListModel> GetTemplateById(int templateId)
        {
            var getList = new TemplateListModel();
            string getAttachment = await _dbContext.TemplateAttachments.OrderByDescending(ad => ad.TemplateAttachmentsId).Where(ad => ad.TemplateId == templateId).Select(ad => ad.AttachmentJson).FirstOrDefaultAsync();
            getList = await (from A in _dbContext.Template
                             join B in _dbContext.Department on A.DepartmentId equals B.DepartmentId into departmentGroup
                             from B in departmentGroup.DefaultIfEmpty()
                             join D in _dbContext.EngageType on A.EngageTypeId equals D.EngageTypeId into engageTypeGroup
                             from D in engageTypeGroup.DefaultIfEmpty()
                             join F in _dbContext.Status on A.StatusId equals F.StatusId into statusGroup
                             from F in statusGroup.DefaultIfEmpty()
                             join H in _dbContext.TemplateAttachments on A.TemplateId equals H.TemplateId into attachmentsGroup
                             from H in attachmentsGroup.DefaultIfEmpty()
                             join G in _dbContext.TemplateVersion on A.TemplateId equals G.TemplateId
                             where !A.IsDeleted && A.TemplateId == templateId
                             orderby G.VersionNumber descending
                             select new TemplateListModel
                             {
                                 TemplateId = A.TemplateId,
                                 TemplateName = A.TemplateName,
                                 TemplateDescription = A.TemplateDescription,
                                 DepartmentId = B.DepartmentId,
                                 DepartmentName = B.DepartmentName ?? string.Empty,
                                 EngageTypeId = D.EngageTypeId,
                                 EngageTypeName = D.EngageTypeName ?? string.Empty,
                                 StatusId = F.StatusId,
                                 StatusName = F.StatusName ?? string.Empty,
                                 ChangeNotes = A.ChangeNotes,
                                 TemplateLogic = G.TemplateLogic,
                                 VersionNumber = G.VersionNumber,
                                 CreatedBy = A.CreatedBy,
                                 AttachmentCount = A.AttachmentCount,
                                 AttachmentJson = getAttachment,
                                 ClientSignatureCount = A.ClientSignatureCount,
                                 Is7216Available = A.Is7216Available
                             }).FirstOrDefaultAsync();
            return getList;
        }
        public async Task<IEnumerable<EngageTypesList>> GetEngageTypes(int DepartmentId)
        {
            var getEngageList = new List<EngageTypesList>();
            getEngageList = await (from a in _dbContext.EngageType
                                   join b in _dbContext.Department on a.DepartmentId equals b.DepartmentId
                                   where b.DepartmentId == DepartmentId
                                   orderby a.EngageTypeName ascending
                                   select new EngageTypesList
                                   {
                                       EngageTypeId = a.EngageTypeId,
                                       EngageTypeName = a.EngageTypeName ?? string.Empty
                                   }).ToListAsync();
            return getEngageList;
        }
        public async Task<IEnumerable<StatusListModel>> GetStatus()
        {
            var getList = new List<StatusListModel>();
            getList = await (from a in _dbContext.Status
                             orderby a.StatusName ascending
                             select new StatusListModel
                             {
                                 StatusId = a.StatusId,
                                 StatusName = a.StatusName
                             }).ToListAsync();
            return getList;
        }
        public async Task<bool> DeleteTemplate(TemplateListModel deleteModel)
        {
            bool isNotDelAtleastOnce = false;
            foreach (var item in deleteModel.DeleteId)
            {
                using (var _dbContext = new DashboardDBContext(_dbContextOptions))
                {
                    var templateName = await _dbContext.Template.FirstOrDefaultAsync(n => n.TemplateId == item);
                    string getTemplateName = templateName?.TemplateName ?? string.Empty;
                    if (!string.IsNullOrEmpty(getTemplateName))
                    {
                        var idCountFromBulkLetters = await _dbContext.BulkLetters.CountAsync(n => n.TemplateName == getTemplateName);
                        var templateIdCount = _dbContext.EngagementLetter.Count(n => n.TemplateId == item);
                        if (templateIdCount == 0 && idCountFromBulkLetters == 0)
                        {
                            var deleteId = await _dbContext.Template.FirstOrDefaultAsync(b => b.TemplateId == item);

                            if (deleteId != null)
                            {
                                var estTime = await _getCurrentDatetime.GetCurrentEstDatetime();
                                deleteId.IsDeleted = true;
                                deleteId.DeletedBy = deleteModel.DeletedBy;
                                deleteId.ModifiedOn = estTime;
                                deleteId.DeletedOn = estTime;
                                await _dbContext.SaveChangesAsync();
                            }
                            else { }
                        }
                        else
                        {
                            isNotDelAtleastOnce = true;
                        }
                    }
                }
            }
            if (!isNotDelAtleastOnce)
            { return true; }
            else
            { return false; }
        }
        public async Task<IEnumerable<int>> GetTemplatesIdList()
        {
            var getList = new List<int>();
            getList = await (from a in _dbContext.Template
                             join b in _dbContext.Status on a.StatusId equals b.StatusId
                             orderby a.TemplateId descending
                             select (a.TemplateId)).ToListAsync();
            return getList;
        }
    }
}
