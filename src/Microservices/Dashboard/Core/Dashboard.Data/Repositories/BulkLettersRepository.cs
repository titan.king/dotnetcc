﻿using AutoMapper;
using Dashboard.Data.Context;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;
using Engage3.Common.Business.Interfaces;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Resources;

namespace Dashboard.Data.Repositories
{
    public class BulkLettersRepository : IBulkLettersRepository
    {
        private readonly DbContextOptions<DashboardDBContext> _dbContextOptions;
        private readonly DashboardDBContext _dbContext;
        private readonly ResourceManager _resourceManager;
        private readonly IMapper _mapper;
        private readonly IGetCurrentDatetime _getCurrentDateTime;
        public BulkLettersRepository(DbContextOptions<DashboardDBContext> dbContextOptions, DashboardDBContext dbContext, ResourceManager resourceManager, IMapper mapper, IGetCurrentDatetime getCurrentDatetime)
        {
            _dbContextOptions = dbContextOptions;
            _dbContext = dbContext;
            _resourceManager = resourceManager;
            _mapper = mapper;
            _getCurrentDateTime = getCurrentDatetime;
        }

        public async Task<BatchRequestResponse> AddBatchRequestDetails(BatchRequestResponse batchRequest)
        {

            var insertData = _mapper.Map<BatchRequest>(batchRequest);
            if (insertData != null && insertData.BatchRequestId == 0)
            {
                var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
                insertData.CreatedOn = estTime;
                await _dbContext.BatchRequest.AddAsync(insertData);
                await _dbContext.SaveChangesAsync();
                batchRequest = _mapper.Map<BatchRequestResponse>(insertData);
                await UpdateBulkLetterStatus(insertData.BatchId, insertData.CreatedBy);
            }
            return batchRequest;

        }
        public async Task UpdateBulkLetterStatus(int BatchId, string modifiedBy)
        {
            string documentStatus = _resourceManager.GetString("UpdateBulkLetterstatusforAddBatchRequest") ?? string.Empty;
            var getBulkLettersList = await _dbContext.BulkLetters.Where(n => n.BatchId == BatchId).ToListAsync();
            if (getBulkLettersList.Any() && !string.IsNullOrEmpty(documentStatus))
            {
                getBulkLettersList.ForEach(a =>
                {
                    a.DocumentStatusName = documentStatus;
                    a.ModifiedBy = modifiedBy;
                });
                await _dbContext.SaveChangesAsync();
            }

        }
        public async Task<bool> IsBatchrequestIdExists(int batchId)
        {
            var isExists = await _dbContext.BatchRequest.Where(n => n.BatchId == batchId).CountAsync();
            if (isExists > 0)
            {
                return true;
            }
            else { return false; }
        }
        public async Task<bool> InsertAttachmentsBulkLetter(BulkLetterEditAttachmentsResponseModel editAttachmentsModel)
        {

            bool transactionStatus = false;
            if (editAttachmentsModel != null && editAttachmentsModel.BulkLetterAttachmentId == 0)
            {
                var attachmentVersion = _dbContext.BulkLetterAttachments.OrderByDescending(n => n.BulkLetterAttachmentId)
                                    .FirstOrDefault(n => n.BatchId == editAttachmentsModel.BatchId && n.TemplateId == editAttachmentsModel.TemplateId)?.VersionNumber ?? 0;
                if (attachmentVersion != 0)
                {
                    editAttachmentsModel.VersionNumber = attachmentVersion + 1;
                }
                else
                {
                    editAttachmentsModel.VersionNumber = 2;
                }
                var insertData = _mapper.Map<BulkLetterAttachments>(editAttachmentsModel);

                await _dbContext.BulkLetterAttachments.AddAsync(insertData);
                await _dbContext.SaveChangesAsync();
                transactionStatus = true;
            }

            return transactionStatus;
        }

        public async Task<BulkLetterEditAttachmentsResponseModel> GetBulkLetterAttachmentById(int batchId, int templateId)
        {
            BulkLetterEditAttachmentsResponseModel bulkLetter = new BulkLetterEditAttachmentsResponseModel();
            var getBulkLetterattachments = _dbContext.BulkLetterAttachments.OrderByDescending(n => n.VersionNumber)
                                            .Where(n => n.BatchId == batchId && n.TemplateId == templateId)
                                            .Select(n => new BulkLetterEditAttachmentsResponseModel
                                            {
                                                BulkLetterAttachmentId = n.BulkLetterAttachmentId,
                                                BatchId = n.BatchId,
                                                TemplateId = n.TemplateId,
                                                AttachmentsJSON = n.AttachmentsJSON,
                                                AttachmentsURL = n.AttachmentsURL,
                                                VersionNumber = n.VersionNumber
                                            }).FirstOrDefault();
            if (getBulkLetterattachments != null)
            {
                var serialAttach = JsonConvert.SerializeObject(getBulkLetterattachments);
                var deSerial = JsonConvert.DeserializeObject<BulkLetterEditAttachmentsResponseModel>(serialAttach);
                bulkLetter = deSerial;
            }

            if (getBulkLetterattachments == null && templateId != 0)
            {
                var latestTemplateJSON = _dbContext.TemplateAttachments.OrderByDescending(n => n.TemplateAttachmentsId)
                    .Where(n => n.TemplateId == templateId)
                    .Select(n => new BulkLetterEditAttachmentsResponseModel
                    {
                        BulkLetterAttachmentId = n.TemplateAttachmentsId,
                        TemplateId = n.TemplateId,
                        AttachmentsJSON = n.AttachmentJson
                    }).FirstOrDefault();
                if (latestTemplateJSON != null)
                {
                    var serialAttach = JsonConvert.SerializeObject(latestTemplateJSON);
                    var deSerial = JsonConvert.DeserializeObject<BulkLetterEditAttachmentsResponseModel>(serialAttach);
                    bulkLetter = deSerial;
                }
            }
            return bulkLetter;
        }
        public async Task<bool> DeleteByBulkLetterId(DeleteBulkLetterResModel letterId)
        {
            var byClient = await _dbContext.BulkLetters.FirstOrDefaultAsync(b => b.BulkLettersId == letterId.BulkLetterId);
            if (byClient != null)
            {
                var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
                byClient.IsDeleted = true;
                byClient.DeletedBy = letterId.DeletedBy;
                byClient.DeletedOn = estTime;
                byClient.ModifiedOn = estTime;
                byClient.ModifiedBy = letterId.DeletedBy;
                byClient.IsBatchDelete = true;
                await _dbContext.SaveChangesAsync();
            }
            return true;
        }

        public async Task<bool> RemoveClientData(RemoveClientDataResModel Bulkletter)
        {
            var existingData = await _dbContext.BulkLetters
                            .Where(b => Bulkletter.BulkLetterId.Contains(b.BulkLettersId))
                            .ToListAsync();

            if (existingData.Any())
            {
                existingData.ForEach(a =>
                {
                    DateTime utcNow = DateTime.UtcNow;
                    TimeZoneInfo easternTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                    DateTime estTime = TimeZoneInfo.ConvertTimeFromUtc(utcNow, easternTimeZone);
                    a.DeletedBy = Bulkletter.DeletedBy;
                    a.DeletedOn = estTime;
                    a.IsDeleted = true;
                    a.IsBatchDelete = true;
                });
                _dbContext.BulkLetters.UpdateRange(existingData);
                await _dbContext.SaveChangesAsync();
            }
            return true;
        }

        public async Task<bool> DeleteByBatchId(DeleteByBatchResModel batchId)
        {
            List<int> byBatch = new List<int>();
            byBatch = (from letter in _dbContext.BulkLetters
                       where letter.BatchId == batchId.BatchId
                       select letter.BulkLettersId)
                               .ToList();
            foreach (var item in byBatch)
            {
                var byClient = await _dbContext.BulkLetters.FirstOrDefaultAsync(b => b.BulkLettersId == item);
                if (byClient != null)
                {
                    var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
                    byClient.IsDeleted = true;
                    byClient.DeletedBy = batchId.DeletedBy;
                    byClient.DeletedOn = estTime;
                    byClient.ModifiedOn = estTime;
                    byClient.ModifiedBy = batchId.DeletedBy;
                    await _dbContext.SaveChangesAsync();
                }
            }
            return true;
        }
        public async Task<BulkLetterFieldsResponseModel> InsertFieldsList(BulkLetterFieldsResponseModel fieldsList)
        {
            BulkLetterFieldsResponseModel responseModel = new BulkLetterFieldsResponseModel();
            var letterVersion = "";
            if (fieldsList != null && fieldsList.FieldList != null)
            {
                foreach (var item in fieldsList.FieldList)
                {
                    item.FieldId = _dbContext.Field.FirstOrDefault(x => x.FieldName == item.FieldName && !x.IsDeleted)?.FieldId;
                    if (item.FieldId != null && item.FieldId > 0)
                    {
                        var insertData = _mapper.Map<LetterFieldValues>(item);
                        insertData.ClientId = fieldsList.ClientId;
                        insertData.PartnerId = fieldsList.PartnerId;
                        insertData.BulkLettersId = fieldsList.BulkLettersId;
                        await _dbContext.LetterFieldValues.AddAsync(insertData);
                        await _dbContext.SaveChangesAsync();
                    }
                }
            }
            return responseModel;
        }
        public async Task<List<PartnerOfficeResponse>> GetPartnerOfficeListAsync(int batchId)
        {
            List<PartnerOfficeResponse> listOfPartnerOffice = await _dbContext.BulkLetters
                .Where(a => a.BatchId == batchId && !a.IsDeleted)
                 .GroupBy(a => new { a.PartnerName, a.Office })
                 .Select(group => new PartnerOfficeResponse
                 {
                     PartnerName = group.Key.PartnerName ?? string.Empty,
                     Office = group.Key.Office ?? string.Empty,
                 }).ToListAsync();
            return listOfPartnerOffice;
        }
        public async Task<EditFieldValuesResponse> GetBulkLetterFieldsById(int bulkLetterId)
        {
            EditFieldValuesResponse response = new EditFieldValuesResponse();
            response.EditFieldsValues = new List<EditFieldsValues>();
            if (bulkLetterId > 0)
            {
                var bulkData = await _dbContext.BulkLetters.FirstOrDefaultAsync(a => a.BulkLettersId == bulkLetterId);

                var responseData = await _dbContext.LetterFieldValues.Where(n => n.BulkLettersId == bulkLetterId && n.LetterVersion == _dbContext.LetterFieldValues
                .Where(inner => inner.BulkLettersId == bulkLetterId).Max(inner => inner.LetterVersion)).ToListAsync();

                var listOfBulkLetterFields = _mapper.Map<List<EditFieldsValues>>(responseData);
                response.EditFieldsValues = listOfBulkLetterFields;
                response.ClientSignatureCount = bulkData?.ClientSignatureCount ?? 0;
                response.Is7216Available = bulkData.Is7216Available;
                response.BulkLettersId = bulkLetterId;
                response.IsEsigning = bulkData?.IsEsigning;
            }
            return response;
        }

        public async Task<bool> SaveEditFields(EditFieldValuesResponse editField)
        {
            try
            {
                bool response = false;
                if (editField != null && editField.EditFieldsValues.Any())
                {
                    int latestVersion = 0;
                    var bulkData = await _dbContext.BulkLetters.FirstOrDefaultAsync(a => a.BulkLettersId == editField.BulkLettersId);
                    var getLatestVersion = _dbContext.LetterFieldValues.OrderByDescending(n => n.LetterVersion).FirstOrDefault(n => n.BulkLettersId == editField.BulkLettersId)?.LetterVersion ?? 0;


                    var listOfBulkLetterFields = _mapper.Map<List<LetterFieldValues>>(editField.EditFieldsValues);

                    if (bulkData != null && listOfBulkLetterFields.Any())
                    {
                        latestVersion = getLatestVersion + 1;
                        listOfBulkLetterFields.ForEach(a => a.LetterVersion = latestVersion);
                        await _dbContext.LetterFieldValues.AddRangeAsync(listOfBulkLetterFields);

                        var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
                        bulkData.ModifiedOn = estTime;
                        bulkData.ClientSignatureCount = editField.ClientSignatureCount;
                        bulkData.Is7216Available = editField.Is7216Available;
                        bulkData.BulkLettersId = editField.BulkLettersId;
                        bulkData.IsEsigning = editField.IsEsigning;
                        bulkData.ModifiedBy = editField.EditedBy ?? string.Empty;

                        await _dbContext.SaveChangesAsync();

                        response = true;
                    }
                    //Update BulkLetters Table
                    if (listOfBulkLetterFields.Any() && bulkData != null && bulkData.BulkLettersId > 0)
                    {
                        var getClientName = listOfBulkLetterFields.Where(n => n.FieldName == "ClientName").Select(n => n.FieldValue).FirstOrDefault();

                        var getTaxYear = listOfBulkLetterFields.Where(n => n.FieldName == "TaxYear").Select(n => n.FieldValue).FirstOrDefault();
                        var getSignatoryTitle = listOfBulkLetterFields.Where(n => n.FieldName == "SignatoryTitle").Select(n => n.FieldValue).FirstOrDefault();
                        var getSignatoryLastName = listOfBulkLetterFields.Where(n => n.FieldName == "SignatoryLastName").Select(n => n.FieldValue).FirstOrDefault();
                        var getSignatoryFirstName = listOfBulkLetterFields.Where(n => n.FieldName == "SignatoryFirstName").Select(n => n.FieldValue).FirstOrDefault();
                        var getsignatoryEmailId = listOfBulkLetterFields.Where(n => n.FieldName == "SignatoryEmailId").Select(n => n.FieldValue).FirstOrDefault();
                        var getPartnerName = listOfBulkLetterFields.Where(n => n.FieldName == "PartnerName").Select(n => n.FieldValue).FirstOrDefault();
                        var getOfficeName = listOfBulkLetterFields.Where(n => n.FieldName == "OfficeName").Select(n => n.FieldValue).FirstOrDefault();
                        var getspouseEmailId = listOfBulkLetterFields.Where(n => n.FieldName == "SpouseEmailId").Select(n => n.FieldValue).FirstOrDefault();
                        var getspouseFirstName = listOfBulkLetterFields.Where(n => n.FieldName == "SpouseFirstName").Select(n => n.FieldValue).FirstOrDefault();
                        var getspouselastName = listOfBulkLetterFields.Where(n => n.FieldName == "SpouseLastName").Select(n => n.FieldValue).FirstOrDefault();

                        bulkData.TaxYear = getTaxYear ?? string.Empty;
                        bulkData.SignatoryTitle = getSignatoryTitle ?? string.Empty;
                        bulkData.SignatoryLastName = getSignatoryLastName ?? string.Empty;
                        bulkData.SignatoryFirstName = getSignatoryFirstName ?? string.Empty;
                        bulkData.SignatoryEmailId = getsignatoryEmailId ?? string.Empty;
                        bulkData.SpouseEmailId = getspouseEmailId ?? string.Empty;
                        bulkData.SpouseFirstName = getspouseFirstName ?? string.Empty;
                        bulkData.SpouseLastName = getspouselastName ?? string.Empty;
                        bulkData.PartnerName = getPartnerName ?? string.Empty;
                        bulkData.Office = getOfficeName ?? string.Empty;
                        bulkData.Is7216Available = editField.Is7216Available;
                        bulkData.IsEsigning = editField.IsEsigning ?? false;
                        bulkData.ClientSignatureCount = editField.ClientSignatureCount;
                        bulkData.ModifiedBy = editField.EditedBy ?? string.Empty;
                        var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
                        bulkData.ModifiedOn = estTime;
                        bulkData.ClientName = getClientName ?? string.Empty;

                        await _dbContext.SaveChangesAsync();
                        response = true;
                    }
                }
                return response;
            }
            catch (Exception ex) { }

            return false;
        }
        public async Task<string> GetTemplateName(BulkLetterEditAttachmentsResponseModel responseModel)
        {
            if (responseModel != null && responseModel.TemplateId > 0)
            {
                using (var _dbContext = new DashboardDBContext(_dbContextOptions))
                {
                    var templateName = await _dbContext.Template.Where(x => x.TemplateId == responseModel.TemplateId && !x.IsDeleted).Select(x => x.TemplateName).FirstOrDefaultAsync();
                    if (!string.IsNullOrEmpty(templateName))
                    {
                        return templateName;
                    }
                }
            }
            return string.Empty;
        }
        public async Task<string> GetBulkAttachments(int batchId, int templateId)
        {
            using (var _dbContext = new DashboardDBContext(_dbContextOptions))
            {
                var getbulkAttachmentsString = await _dbContext.BulkLetterAttachments.OrderByDescending(n => n.VersionNumber)
                                        .FirstOrDefaultAsync(n => n.BatchId == batchId && n.TemplateId == templateId);

                if (getbulkAttachmentsString != null)
                { return getbulkAttachmentsString.AttachmentsJSON ?? string.Empty; }
                else { return string.Empty; }
            }
        }
        public async Task<string> GetTemplateAttachments(int templateId)
        {
            using (var _dbContext = new DashboardDBContext(_dbContextOptions))
            {
                var gettemplateAttachments = await _dbContext.TemplateAttachments.OrderByDescending(n => n.TemplateAttachmentsId)
                                        .FirstOrDefaultAsync(n => n.TemplateId == templateId);
                if (gettemplateAttachments != null)
                { return gettemplateAttachments.AttachmentJson ?? string.Empty; }
                else
                { return string.Empty; }
            }
        }
        public async Task<ResponseModel> GetTemplateForIndv(int getById, bool isBulkLetterId)
        {
            ResponseModel response = new ResponseModel();
            using (var _dbContext = new DashboardDBContext(_dbContextOptions))
            {
                if (getById > 0 && isBulkLetterId == false)
                {
                    var getTemplate = await _dbContext.Template.FirstOrDefaultAsync(n => n.TemplateId == getById);
                    response.Data = getTemplate;
                }
                else if(getById > 0 && isBulkLetterId == true)
                {
                    var getBulkLetter = await _dbContext.BulkLetters.FirstOrDefaultAsync(x => x.BulkLettersId == getById);
                    response.Data = getBulkLetter;
                }
            }
            return response;
        }
    }
}
