﻿using AutoMapper;
using Dashboard.Data.Context;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Models.ResponseModel;
using Engage3.Common.Business.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Data;

namespace Dashboard.Data.Repositories
{
    public class BlocksRepository : IBlocksRepository
    {
        private readonly DbContextOptions<DashboardDBContext> _dbContextOptions;
        private readonly DashboardDBContext _dbContext;
        private readonly IGetCurrentDatetime _getCurrentDateTime;
        public BlocksRepository(DbContextOptions<DashboardDBContext> dbContextOptions, DashboardDBContext dbContext, IGetCurrentDatetime getCurrentDatetime)
        {
            _dbContextOptions = dbContextOptions;
            _dbContext = dbContext;
            _getCurrentDateTime = getCurrentDatetime;
        }

        public async Task<Block> AddBlocks(Block blocks)
        {
            if (blocks.BlockId == 0)
            {

                var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
                blocks.CreatedOn = estTime;
                blocks.ModifiedOn = estTime;
                await _dbContext.Block.AddAsync(blocks);
                await _dbContext.SaveChangesAsync();
            }
            return blocks;
        }

        public async Task<Block> UpdateBlocks(Block blocks)
        {
            if (blocks.BlockId > 0)
            {
                var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
                blocks.DeletedOn = null;
                blocks.ModifiedOn = estTime;
                _dbContext.Block.Update(blocks);
                await _dbContext.SaveChangesAsync();
            }
            return blocks;
        }

        public async Task<List<TemplateListResponseModel>> GetConnectedTemplates()
        {
            var connectedtemplatelist = new List<TemplateListResponseModel>();
            var relatedentities = new List<string>();
            List<string> questionlist = new List<string>();

            connectedtemplatelist = await (from A in _dbContext.Template
                                           where A.IsDeleted == false
                                           orderby A.ModifiedOn descending
                                           let latestVersionId = _dbContext.TemplateVersion
                                                                  .Where(tv => tv.TemplateId == A.TemplateId)
                                                                  .Max(tv => tv.TemplateVersionId)
                                           join B in _dbContext.Department on A.DepartmentId equals B.DepartmentId into departments
                                           from B in departments.DefaultIfEmpty()
                                           join D in _dbContext.EngageType on A.EngageTypeId equals D.EngageTypeId
                                           join F in _dbContext.DocumentStatus on A.StatusId equals F.DocumentStatusId
                                           join G in _dbContext.TemplateVersion on A.TemplateId equals G.TemplateId
                                           where G.TemplateVersionId == latestVersionId
                                           select new TemplateListResponseModel()
                                           {
                                               TemplateId = A.TemplateId,
                                               TemplateName = A.TemplateName,
                                               TemplateDescription = A.TemplateDescription,
                                               DepartmentName = (B != null) ? B.DepartmentName : null,
                                               EngageTypeName = (D != null) ? D.EngageTypeName : null,
                                               StatusId = F.DocumentStatusId,
                                               ChangeNotes = A.ChangeNotes,
                                               TemplateLogic = G.TemplateLogic,
                                               CreatedBy = A.CreatedBy,
                                               DepartmentId = A.DepartmentId,
                                               EngageTypeId = A.EngageTypeId
                                           }).ToListAsync();
            return connectedtemplatelist;
        }

        public async Task<BlocksCommonModel> GetBlocksById(int blockId)
        {
            using (var _dbContext = new DashboardDBContext(_dbContextOptions))
            {
                BlocksCommonModel commonModel = new BlocksCommonModel();
                commonModel.BlocksById = await (from blk in _dbContext.Block
                                                join blksts in _dbContext.Status on blk.StatusId equals blksts.StatusId
                                                where blk.BlockId == blockId && !blk.IsDeleted
                                                select new BlocksList
                                                {
                                                    BlockId = blk.BlockId,
                                                    BlockName = blk.BlockName,
                                                    Description = blk.Description,
                                                    Content = blk.Content,
                                                    ChangeNotes = blk.ChangeNotes,
                                                    StatusId = blksts.StatusId,
                                                    StatusName = blksts.StatusName,
                                                    ConnectedTemplates = ""
                                                }).ToListAsync();

                //--------------------CONNECTED TEMPLATE START-----------------------------
                var connectedtemplatelist = new List<TemplateListResponseModel>();
                var relatedentities = new List<string>();
                List<string> questionlist = new List<string>();

                var data = await (from A in _dbContext.Template
                                  where A.IsDeleted == false
                                  orderby A.ModifiedOn descending
                                  let latestVersionId = _dbContext.TemplateVersion
                                                         .Where(tv => tv.TemplateId == A.TemplateId)
                                                         .Max(tv => tv.TemplateVersionId)
                                  join B in _dbContext.Department on A.DepartmentId equals B.DepartmentId into departments
                                  from B in departments.DefaultIfEmpty()
                                  join D in _dbContext.EngageType on A.EngageTypeId equals D.EngageTypeId
                                  join F in _dbContext.DocumentStatus on A.StatusId equals F.DocumentStatusId
                                  join G in _dbContext.TemplateVersion on A.TemplateId equals G.TemplateId
                                  where G.TemplateVersionId == latestVersionId
                                  select new TemplateListResponseModel()
                                  {
                                      TemplateId = A.TemplateId,
                                      TemplateName = A.TemplateName,
                                      TemplateDescription = A.TemplateDescription,
                                      DepartmentName = (B != null) ? B.DepartmentName : null,
                                      EngageTypeName = (D != null) ? D.EngageTypeName : null,
                                      StatusId = F.DocumentStatusId,
                                      ChangeNotes = A.ChangeNotes,
                                      TemplateLogic = G.TemplateLogic,
                                      CreatedBy = A.CreatedBy,
                                      DepartmentId = A.DepartmentId,
                                      EngageTypeId = A.EngageTypeId
                                  }).ToListAsync();
                commonModel.ConnectedTemplate = data;
                return commonModel;
            }
        }

        public async Task<IEnumerable<BlockIdListResponseModel>> GetBlocksIdList()
        {
            var blockIds = new List<BlockIdListResponseModel>();
            blockIds = await _dbContext.Block
                       .OrderByDescending(a => a.BlockId)
                       .Join(_dbContext.Status,
                        a => a.StatusId,
                        b => b.StatusId,
                        (a, b) => new BlockIdListResponseModel
                        {
                            BlockId = a.BlockId
                        }).ToListAsync();
            return blockIds;
        }
        public async Task<IEnumerable<BlocksList>> GetBlocks()
        {
            var getBlockList = new List<BlocksList>();
            getBlockList = await (from a in _dbContext.Block
                                  join b in _dbContext.Status on a.StatusId equals b.StatusId
                                  where !a.IsDeleted
                                  select new BlocksList
                                  {
                                      BlockId = a.BlockId,
                                      BlockName = a.BlockName,
                                      Description = a.Description,
                                      StatusId = a.StatusId,
                                      StatusName = b.StatusName,
                                      ChangeNotes = a.ChangeNotes,
                                      Content = a.Content,
                                      ModifiedOn = a.ModifiedOn
                                  }).OrderByDescending(a => a.ModifiedOn).ToListAsync();
            return getBlockList;
        }

        public async Task<IEnumerable<ActiveBlocksList>> GetActiveBlocksList()
        {
            var activeBlocks = new List<ActiveBlocksList>();
            activeBlocks = await (from a in _dbContext.Block
                                  join b in _dbContext.Status on a.StatusId equals b.StatusId
                                  where !a.IsDeleted && b.StatusName == "Active"
                                  select new ActiveBlocksList
                                  {
                                      BlockId = a.BlockId,
                                      BlockName = a.BlockName,
                                      Description = a.Description,
                                      StatusName = b.StatusName,
                                      ChangeNotes = a.ChangeNotes,
                                      Content = a.Content
                                  }).OrderByDescending(a => a.BlockId).ToListAsync();
            return activeBlocks;
        }

        public async Task<bool> DeleteBlock(DeleteBlockResModel blockId)
        {
            var blockIds = blockId.BlockId[0];
            var block = await _dbContext.Block.FirstOrDefaultAsync(b => b.BlockId == blockIds);
            if (block != null)
            {
                var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
                block.IsDeleted = true;
                block.DeletedBy = blockId.DeletedBy;
                block.DeletedOn = estTime;
                block.ModifiedOn = estTime;
                await _dbContext.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public async Task<BlockStatusListResModel> Load()
        {
            var blockStatus = new BlockStatusListResModel();

            blockStatus.BlockStatus = await _dbContext.Status
                                    .Where(s => s.StatusId == 1 || s.StatusId == 2)
                                    .Select(s => new BlockStatus
                                    {
                                        StatusId = s.StatusId,
                                        StatusName = s.StatusName
                                    }).ToListAsync();
            return blockStatus;
        }

        public async Task<bool> IsValidBlockName(BlocksResponseModel block)
        {
            var result = true;
            var resultList = new IsValidBlockNameResModel();
            if (block.BlockId == 0)
            {
                var count = _dbContext.Block
                    .Where(b => b.IsDeleted == false &&
                                b.BlockName.Replace("\t", "").Replace("\n", "").Replace("\r", "").Replace(" ", "") ==
                                block.BlockName.Replace("\t", "").Replace("\n", "").Replace("\r", "").Replace(" ", "")).Count();
                if (count > 0)
                {
                    resultList.IsValid = false;
                }
                else
                {
                    resultList.IsValid = true;
                }
            }
            else
            {
                var count = _dbContext.Block
                            .Where(b => b.IsDeleted == false &&
                                        b.BlockId != block.BlockId &&
                                        b.BlockName.Replace("\t", "").Replace("\n", "").Replace("\r", "").Replace(" ", "") ==
                                        block.BlockName.Replace("\t", "").Replace("\n", "").Replace("\r", "").Replace(" ", "")).Count();

                if (count > 0)
                {
                    resultList.IsValid = false;
                }
                else
                {
                    resultList.IsValid = true;
                }
            }
            return resultList.IsValid;
        }
    }
}
