﻿using AutoMapper;
using Dashboard.Data.Context;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Models.Clients;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;
using Engage3.Common.Business.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Resources;

namespace Dashboard.Data.Repositories
{
    public class FieldsRepository : IFieldsRepository
    {
        private readonly DbContextOptions<DashboardDBContext> _dbContextOptions;
        private readonly DashboardDBContext _dbContext;
        private readonly ResourceManager _resourceManager;
        private readonly IMapper _mapper;
        private readonly IGetCurrentDatetime _getCurrentDatetime;
        public FieldsRepository(DbContextOptions<DashboardDBContext> dbContextOptions, DashboardDBContext dbContext,
            ResourceManager resourceManager, IMapper mapper, IGetCurrentDatetime getCurrentDatetime)
        {
            _dbContextOptions = dbContextOptions;
            _dbContext = dbContext;
            _resourceManager = resourceManager;
            _mapper = mapper;
            _getCurrentDatetime = getCurrentDatetime;
        }
        public async Task<IEnumerable<FieldListModel>> GetFields()
        {
            var getFieldList = new List<FieldListModel>();
            getFieldList = await (from a in _dbContext.Field
                                  join b in _dbContext.Status on a.StatusId equals b.StatusId
                                  join c in _dbContext.FieldDataTypes on a.FieldDataTypeId equals c.FieldDataTypeId into fieldDataTypesGroup
                                  from c in fieldDataTypesGroup.DefaultIfEmpty()
                                  where !a.IsDeleted
                                  select new FieldListModel
                                  {
                                      FieldName = a.FieldName,
                                      FieldId = a.FieldId,
                                      DisplayName = a.DisplayName,
                                      DirectInput = a.DirectInput,
                                      ChangeNotes = a.ChangeNotes,
                                      StatusId = a.StatusId,
                                      StatusName = b.StatusName,
                                      FieldDataTypeId = a.FieldDataTypeId,
                                      FieldDataType = c.FieldDataType,
                                      HintText = a.HintText,
                                      ModifiedOn = a.ModifiedOn,
                                      CreatedOn = a.CreatedOn,
                                      MDDColumnNames = a.MDDColumnNames,
                                      ModifiedBy = a.ModifiedBy,
                                      MDDColumnData = a.MDDColumnData,
                                      MDDLookup = a.MDDLookup,
                                      InputType = (a.DirectInput == true) ? _resourceManager.GetString("InputType1") : (a.MDDLookup == true) ? _resourceManager.GetString("InputType2") : null,
                                      CreatedBy = a.CreatedBy
                                  }).OrderByDescending(a => a.ModifiedOn).ToListAsync();

            return getFieldList;
        }
        public async Task<GetFieldsByIdList> GetFieldsById(int fieldId)
        {
            var mylist = new List<BlocksList>();
            GetFieldsByIdList getFieldsById = new GetFieldsByIdList();
            getFieldsById.FieldsListById = await (from a in _dbContext.Field
                                                  join b in _dbContext.Status on a.StatusId equals b.StatusId
                                                  join c in _dbContext.FieldDataTypes on a.FieldDataTypeId equals c.FieldDataTypeId into fieldDataTypesGroup
                                                  from c in fieldDataTypesGroup.DefaultIfEmpty()
                                                  join d in _dbContext.ColumnNames on a.ColumnId equals d.ColumnNamesId into column
                                                  from d in column.DefaultIfEmpty()
                                                  where a.FieldId == fieldId && !a.IsDeleted
                                                  select new FieldListModel
                                                  {
                                                      FieldName = a.FieldName,
                                                      FieldId = a.FieldId,
                                                      DisplayName = a.DisplayName,
                                                      DirectInput = a.DirectInput,
                                                      ChangeNotes = a.ChangeNotes,
                                                      StatusId = a.StatusId,
                                                      StatusName = b.StatusName,
                                                      FieldDataTypeId = a.FieldDataTypeId,
                                                      FieldDataType = c.FieldDataType,
                                                      HintText = a.HintText,
                                                      ModifiedOn = a.ModifiedOn,
                                                      CreatedOn = a.CreatedOn,
                                                      MDDColumnNames = d.ColumnName,
                                                      ModifiedBy = a.ModifiedBy,
                                                      MDDColumnData = a.MDDColumnData,
                                                      MDDLookup = a.MDDLookup,
                                                      ColumnId = a.ColumnId,
                                                      CreatedBy = a.CreatedBy,
                                                      InputType = (a.DirectInput == true) ? _resourceManager.GetString("InputType1") : (a.MDDLookup == true) ? _resourceManager.GetString("InputType2") : null
                                                  }).ToListAsync();

            //--------------------------------Connected Blocks Starts ---------------------------------------------------------

            mylist = await (from blk in _dbContext.Block
                            where (!blk.IsDeleted)
                            select new BlocksList
                            {
                                Content = blk.Content,
                                BlockName = blk.BlockName
                            }).ToListAsync();

            getFieldsById.FieldsConnectedBlocks = mylist;

            return getFieldsById;
        }

        public async Task<IEnumerable<FieldListModel>> GetActiveFields()
        {
            var getActiveFieldList = new List<FieldListModel>();
            getActiveFieldList = await (from a in _dbContext.Field
                                        join b in _dbContext.Status on a.StatusId equals b.StatusId
                                        join c in _dbContext.FieldDataTypes on a.FieldDataTypeId equals c.FieldDataTypeId into fieldDataTypesGroup
                                        from c in fieldDataTypesGroup.DefaultIfEmpty()
                                        join d in _dbContext.ColumnNames on a.ColumnId equals d.ColumnNamesId into column
                                        from d in column.DefaultIfEmpty()
                                        where !a.IsDeleted && b.StatusName == "Active"
                                        select new FieldListModel
                                        {
                                            FieldName = a.FieldName,
                                            FieldId = a.FieldId,
                                            DisplayName = a.DisplayName,
                                            DirectInput = a.DirectInput,
                                            ChangeNotes = a.ChangeNotes,
                                            StatusId = a.StatusId,
                                            StatusName = b.StatusName,
                                            FieldDataTypeId = a.FieldDataTypeId,
                                            FieldDataType = c.FieldDataType,
                                            HintText = a.HintText,
                                            ModifiedOn = a.ModifiedOn,
                                            CreatedOn = a.CreatedOn,
                                            MDDColumnNames = a.MDDColumnNames,
                                            ModifiedBy = a.ModifiedBy,
                                            MDDColumnData = a.MDDColumnData,
                                            MDDLookup = a.MDDLookup
                                        }).OrderByDescending(a => a.ModifiedOn).ToListAsync();
            return getActiveFieldList;
        }
        public async Task<FieldsResponse> GetActiveClientFields(int clientId, int partnerId)
        {
            FieldsResponse getActiveClientFieldList = new FieldsResponse();
            getActiveClientFieldList.FieldList = await (from a in _dbContext.Field
                                                        join b in _dbContext.Status on a.StatusId equals b.StatusId
                                                        join c in _dbContext.FieldDataTypes on a.FieldDataTypeId equals c.FieldDataTypeId into fieldDataTypesGroup
                                                        from c in fieldDataTypesGroup.DefaultIfEmpty()
                                                        join d in _dbContext.ColumnNames on a.ColumnId equals d.ColumnNamesId into column
                                                        from d in column.DefaultIfEmpty()
                                                        where !a.IsDeleted && b.StatusName == "Active"
                                                        orderby a.ModifiedOn descending
                                                        select new FieldListModel
                                                        {
                                                            FieldName = a.FieldName,
                                                            FieldId = a.FieldId,
                                                            DisplayName = a.DisplayName,
                                                            DirectInput = a.DirectInput,
                                                            ChangeNotes = a.ChangeNotes,
                                                            StatusId = a.StatusId,
                                                            StatusName = b.StatusName,
                                                            FieldDataTypeId = a.FieldDataTypeId,
                                                            FieldDataType = c.FieldDataType,
                                                            HintText = a.HintText,
                                                            ModifiedOn = a.ModifiedOn,
                                                            CreatedOn = a.CreatedOn,
                                                            MDDColumnNames = a.MDDColumnNames,
                                                            ModifiedBy = a.ModifiedBy,
                                                            MDDColumnData = a.MDDColumnData,
                                                            MDDLookup = a.MDDLookup
                                                        }).ToListAsync();


            ClientFields? clientFieldsRes = await _dbContext.ClientFields.FirstOrDefaultAsync(a => a.ClientId == clientId);
            getActiveClientFieldList.ClientFields = _mapper.Map<ClientFieldsViewModel>(clientFieldsRes);

            PartnerOfficeResModel? PartnerOffice = await (from a in _dbContext.t_engletterdata
                                                          where a.PartnerId == partnerId.ToString()
                                                          select new PartnerOfficeResModel
                                                          {
                                                              PartnerId = !string.IsNullOrEmpty(a.PartnerId) ? Convert.ToInt32(a.PartnerId) : 0,
                                                              PartnerName = a.PartnerName,
                                                              OfficeName = a.OfficeName,
                                                              OfficeAddress = a.OfficeAddress,
                                                              OfficeCity = a.OfficeCity,
                                                              OfficeState = a.OfficeState,
                                                              FieldId= !string.IsNullOrEmpty(a.OfficeId) ? Convert.ToInt32(a.OfficeId) : 0,
                                                              OfficeZipCode = !string.IsNullOrEmpty(a.OfficeZipCode) ? a.OfficeZipCode : string.Empty,
                                                              OfficePhoneNumber = !string.IsNullOrEmpty(a.OfficePhoneNumber) ? a.OfficePhoneNumber : string.Empty,
                                                              Jurisdiction = a.Jurisdiction
                                                          }).FirstOrDefaultAsync();

            //var jurisdictionData = await _dbContext.Jurisdiction
            //                        .Where(a => a.CitrinDatabase == PartnerOffice.OfficeName)
            //                        .Select(a => a.JurisdictionData)
            //                        .FirstOrDefaultAsync();

            //PartnerOffice.Jurisdiction = jurisdictionData ?? string.Empty;

            getActiveClientFieldList.PartnerOffice = PartnerOffice;

            return getActiveClientFieldList;
        }

        public async Task<IEnumerable<FieldsIdListResModel>> GetFieldsIdList()
        {
            var getActiveFieldList = new List<FieldsIdListResModel>();
            getActiveFieldList = await (from a in _dbContext.Field
                                        join b in _dbContext.Status on a.StatusId equals b.StatusId
                                        join c in _dbContext.FieldDataTypes on a.FieldDataTypeId equals c.FieldDataTypeId into fieldDatatypeGroup
                                        from c in fieldDatatypeGroup.DefaultIfEmpty()
                                        orderby a.FieldId descending
                                        select new FieldsIdListResModel
                                        {
                                            FieldId = a.FieldId
                                        }).ToListAsync();
            return getActiveFieldList;
        }

        public async Task<FieldsStatusResModel> LoadStatus()
        {
            var fieldsStatus = new FieldsStatusResModel();

            fieldsStatus.FieldsStatus = await _dbContext.Status
                                 .Where(s => s.StatusId == 1 || s.StatusId == 2)
                                 .Select(s => new FieldsStatus
                                 {
                                     StatusId = s.StatusId,
                                     StatusName = s.StatusName
                                 }).ToListAsync();

            fieldsStatus.FieldsDataType = await _dbContext.FieldDataTypes
                                            .GroupBy(dt => dt.FieldDataType)
                                            .Select(dt => new FieldsDataType
                                            {
                                                FieldDataTypeId = dt.First().FieldDataTypeId,
                                                FieldType = dt.Key
                                            }).ToListAsync();
            return fieldsStatus;
        }

        public async Task<DeleteFieldResModel> DeleteFields(DeleteFieldResModel fieldId)
        {
            var fieldIds = fieldId.FieldId[0];
            var field = await _dbContext.Field.FirstOrDefaultAsync(b => b.FieldId == fieldIds);
            if (field != null)
            {
                var estTime = await _getCurrentDatetime.GetCurrentEstDatetime();
                field.IsDeleted = true;
                field.DeletedBy = fieldId.DeletedBy;
                field.ModifiedOn = estTime;
                field.DeletedOn = estTime;
                await _dbContext.SaveChangesAsync();
            }
            return fieldId;
        }

        public async Task<Field> AddField(Field field)
        {
            if (field.FieldId == 0)
            {
                var estTime = await _getCurrentDatetime.GetCurrentEstDatetime();
                field.CreatedOn = estTime;
                field.ModifiedOn = estTime;
                await _dbContext.Field.AddAsync(field);
                await _dbContext.SaveChangesAsync();
            }

            return field;
        }

        public async Task<Field> UpdateField(Field field)
        {
            if (field.FieldId > 0)
            {
                var estTime = await _getCurrentDatetime.GetCurrentEstDatetime();
                field.ModifiedOn = estTime;
                _dbContext.Field.Update(field);
                await _dbContext.SaveChangesAsync();
            }
            return field;
        }

        public async Task<bool> IsValidFieldName(FieldListModel field)
        {
            var resultList = new IsValidFieldNameResModel();
            if (field.FieldId == 0)
            {
                var count = _dbContext.Field
                           .Where(f => f.FieldName == field.FieldName && !f.IsDeleted)
                           .Count();
                if (count > 0)
                {
                    resultList.IsValid = false;
                }
                else
                {
                    resultList.IsValid = true;
                }
            }
            else
            {
                var count = _dbContext.Field
                            .Where(f => f.FieldName == field.FieldName && !f.IsDeleted && f.FieldId != field.FieldId)
                            .Count();
                if (count > 0)
                {
                    resultList.IsValid = false;
                }
                else
                {
                    resultList.IsValid = true;
                }

            }
            return resultList.IsValid;
        }

        public async Task<IEnumerable<ColumnNamesResModel>> GetColumnNames()
        {
            var resultList = new List<ColumnNamesResModel>();
            resultList = await _dbContext.ColumnNames
                               .OrderBy(cn => cn.ColumnName)
                               .Select(cn => new ColumnNamesResModel
                               {
                                   ColumnId = cn.ColumnNamesId,
                                   ColumnNames = cn.ColumnName
                               }).ToListAsync();
            return resultList;
        }
    }
}
