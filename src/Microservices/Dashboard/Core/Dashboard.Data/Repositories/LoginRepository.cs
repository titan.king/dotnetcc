﻿using Dashboard.Data.Context;
using Dashboard.Data.Interfaces;
using Dashboard.Models.Login;
using Dashboard.Models.ResponseModel;
using Microsoft.EntityFrameworkCore;

namespace Dashboard.Data.Repositories
{
    public class LoginRepository : ILoginRepository
    {
        private readonly DbContextOptions<DashboardDBContext> _dbContextOptions;
        private readonly DashboardDBContext _dbContext;
        public LoginRepository(DbContextOptions<DashboardDBContext> dbContextOptions, DashboardDBContext dbContext)
        {
            _dbContextOptions = dbContextOptions;
            _dbContext = dbContext;
        }

        public async Task<LoginViewModel> GetRoles(LoginViewModel permissions)
        {
            if (permissions.GroupIds != null && _dbContext != null)
            {
                var listGroupIDs = permissions.GroupIds.Split(',').ToList();
                if (listGroupIDs != null)
                {
                    var roles = await _dbContext.Roles
                                    .Join(_dbContext.AzureGroup, a => a.RoleId, d => d.AzureGroupId, (a, d) => new { Roles = a, AzureGroup = d })
                                    .Where(ad => listGroupIDs.Contains(ad.AzureGroup.GroupId ?? string.Empty))
                                    .Select(ad => ad.Roles.RoleName)
                                    .Distinct()
                                    .ToListAsync();

                    permissions.Roles = roles;
                }
            }
            return permissions;
        }
        public async Task<LoginListModel> GetRolesandScreenList(LoginListModel loginViewModel)
        {
            var loginListModelRes = new LoginListModel();

            if (loginViewModel.GroupIds != null)
            {
                var GroupIds = loginViewModel.GroupIds.Split(',').ToList();
                if (GroupIds != null)
                {
                    var roles = await _dbContext.Roles.Join(_dbContext.AzureGroup, a => a.RoleId, d => d.AzureGroupId, (a, d) => new { Roles = a, AzureGroup = d })
                                                             .Where(ad => GroupIds.Contains(ad.AzureGroup.GroupId ?? string.Empty))
                                                             .Select(ad => ad.Roles.RoleName)
                                                             .Distinct()
                                                             .ToListAsync();

                    var screenNames = await _dbContext.RolePermissionMapping
                                                      .Join(_dbContext.Screens, a => a.ScreenId, b => b.ScreenId, (a, b) => new { a, b })
                                                      .Join(_dbContext.Roles, ab => ab.a.RoleId, c => c.RoleId, (ab, c) => new { ab.a, ab.b, c })
                                                      .Join(_dbContext.AzureGroup, abc => abc.c.AzureGroupId, d => d.AzureGroupId, (abc, d) => new { abc.a, abc.b, abc.c, d })
                                                      .Where(abcdd => GroupIds.Contains(abcdd.d.GroupId ?? string.Empty))
                                                      .Select(abcdd => abcdd.b.ScreenName)
                                                      .Distinct()
                                                      .ToListAsync();

                    loginListModelRes.Roles = roles;
                    loginListModelRes.ScreenNames = screenNames;

                }
            }
            return loginListModelRes;
        }

    }
}
