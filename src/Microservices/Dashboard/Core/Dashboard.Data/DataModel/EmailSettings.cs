﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Data.DataModel
{
    public class EmailSettings
    {
        [Key]
        public int EmailSettingsId { get; set; }
        public string? ServerName { get; set; }
        public int? Port { get; set; }
        public string? SecurityDetails { get; set; }
        public string? UserName { get; set; }
        public string? Password { get; set; }
        public int? StatusId { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
