﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Data.DataModel
{
    public class SignerStatus
    {
        [Key]
        public int SigningStatusId { get; set; }
        public int? EngagementLetterId { get; set; }
        public int? BatchId { get; set; }
        [MaxLength(255)]
        public string? SigningStatus { get; set; }
        public bool IsDeleted { get; set; }
        [MaxLength(255)]
        public string? SignerEmailId { get; set; }
        [MaxLength(40)]
        public string? SignerName { get; set; }
        public DateTime DeletedOn { get; set; }
        public DateTime CreatedOn { get; set; }
        [MaxLength(40)]
        public string? CreatedBy { get; set; }
    }
}
