﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Data.DataModel
{
    public class OfficeInfo
    {
        [Key]
        public int OfficeInfoId { get; set; }
        [Required]
        public int OfficeId { get; set; }
        [MaxLength(255)]
        public string? Name { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? Office { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? Address { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? City { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? State { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? ZipCode { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? StateSignatureNameAddress { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? StateSignatureCity { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? StateSignatureState { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? StateSignatureZipCode { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? PhoneNumber { get; set; } = string.Empty;
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public DateTime? ModifiedOn { get; set; }
        public bool? IsDeleted { get; set; } = false;
        public DateTime? DeletedOn { get; set; }
    }
}
