﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Data.DataModel
{
    public class EmailTemplate
    {
        [Key]
        public int EmailTemplateId { get; set; }
        public int? DocumentStatusId { get; set; }
        public string? EmailSubject { get; set; }
        public string? EmailContent { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
