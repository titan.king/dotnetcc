﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Data.DataModel
{
    public class ClientData
    {
        [Key]
        public int Id { get; set; }
        public string row_num { get; set; }
        public string ClientId { get; set; }
        public string OfficeId { get; set; }
        public string PartnerId { get; set; }
        public string PartnerName { get; set; }
        public string OfficeName { get; set; }
        public string OfficeAddress { get; set; }
        public string OfficeCity { get; set; }
        public string OfficeState { get; set; }
        public string OfficePhoneNumber { get; set; }
        public string OfficeZipCode { get; set; }
        public string Date { get; set; }
        public string ClientName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string SignatoryEmail { get; set; }
        public string SpouseEmail { get; set; }
        public string SignatoryFirstName { get; set; }
        public string SignatoryLastName { get; set; }
        public string SignatoryTitle { get; set; }
        public string SpouseFirstName { get; set; }
        public string SpouseLastName { get; set; }
        public string Jurisdiction { get; set; }
        public string FiscalYear { get; set; }
        public string ELTemplate { get; set; }
        public string Expiration7216Form { get; set; }
    }
}
