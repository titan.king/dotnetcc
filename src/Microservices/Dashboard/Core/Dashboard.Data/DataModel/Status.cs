﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Data.DataModel
{
    public class Status
    {
        [Key]
        public int StatusId { get; set; }
        public string? StatusName { get; set; } = string.Empty;
    }
}
