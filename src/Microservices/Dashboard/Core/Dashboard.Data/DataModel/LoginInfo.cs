﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Data.DataModel
{
    public class LoginInfo
    {
        [Key]
        public int LoginInfoId { get; set; }
        [MaxLength(255)]
        public string? UserName { get; set; } = string.Empty;
        [MaxLength(50)]
        public string? UserObjectId { get; set; } = string.Empty;
    }
}
