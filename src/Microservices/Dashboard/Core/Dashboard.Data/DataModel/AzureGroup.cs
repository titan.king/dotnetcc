﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Data.DataModel
{
    public class AzureGroup
    {
        [Key]
        public int AzureGroupId { get; set; }
        [MaxLength(255)]
        public string? GroupId { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? GroupName { get; set; } = string.Empty;
    }
}
