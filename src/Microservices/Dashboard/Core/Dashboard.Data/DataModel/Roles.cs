﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dashboard.Data.DataModel
{
    public class Roles
    {
        [Key]
        public int RoleId { get; set; }
        [MaxLength(255)]
        public string? RoleName { get; set; } = string.Empty;
        public int AzureGroupId { get; set; }

        [ForeignKey("AzureGroupId")]
        public virtual AzureGroup AzureGroup { get; set; }

    }
}
