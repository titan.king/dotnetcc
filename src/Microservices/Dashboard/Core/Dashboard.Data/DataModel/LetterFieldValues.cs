﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dashboard.Data.DataModel
{
    public class LetterFieldValues
    {
        [Key]
        public int LetterFieldValuesId { get; set; }
        public int? ClientId { get; set; }
        public int? PartnerId { get; set; }
        public int? BulkLettersId { get; set; }
        [ForeignKey("BulkLettersId")]
        public virtual BulkLetters BulkLetters { get; set; }
        public int? FieldId { get; set; }
        public string? FieldValue { get; set; } = string.Empty;
        public string? FieldName { get; set; } = string.Empty;
        public int LetterVersion { get; set; }
        public string? DisplayName { get; set; } = string.Empty;
    }
}
