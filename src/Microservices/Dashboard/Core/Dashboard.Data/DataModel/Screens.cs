﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Data.DataModel
{
    public class Screens
    {
        [Key]
        public int ScreenId { get; set; }
        [MaxLength(255)]
        public string? ScreenName { get; set; } = string.Empty;
    }
}
