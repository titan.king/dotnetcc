﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dashboard.Data.DataModel
{
    public class Block
    {
        [Key]
        public int BlockId { get; set; }
        [MaxLength(255)]
        public string BlockName { get; set; } = string.Empty;
        [MaxLength(255)]
        public string Description { get; set; } = string.Empty;
        public string Content { get; set; } = string.Empty;
        public string ChangeNotes { get; set; } = string.Empty;
        public bool IsDeleted { get; set; } = false;
        public int StatusId { get; set; }
        [ForeignKey("StatusId")]
        public virtual Status Status { get; set; } 
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        [MaxLength(40)]
        public string? CreatedBy { get; set; } = string.Empty;
        [MaxLength(40)]
        public string? ModifiedBy { get; set; } = string.Empty;
        public DateTime? DeletedOn { get; set; }
        [MaxLength(40)]
        public string? DeletedBy { get; set; } = string.Empty;
    }
}
