﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Data.DataModel
{
    public class Partners
    {
        [Key]
        public int PartnersId { get; set; }
        public int? PartnerId { get; set; }
        [MaxLength(255)]
        public string? PartnerName { get; set; } = string.Empty;
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool? IsDeleted { get; set; } = false;
        public DateTime? DeletedOn { get; set; }
    }
}
