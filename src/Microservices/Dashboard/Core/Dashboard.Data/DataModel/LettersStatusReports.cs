﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Data.DataModel
{
    public class LettersStatusReports
    {
        [Key]
        public int LettersStatusReportId { get; set; }
        public int EngagementLetterId { get; set; }
        public string? EngagementLetterName { get; set; } = string.Empty;
        public int? ClientId { get; set; }
        [MaxLength(255)]
        public string? ClientName { get; set; } = string.Empty;
        public int? PartnerId { get; set; }
        [MaxLength(255)]
        public string? PartnerName { get; set; } = string.Empty;
        public int OfficeId { get; set; }
        [MaxLength(255)]
        public string? OfficeName { get; set; } = string.Empty;
        public int? YearId { get; set; }
        [MaxLength(50)]
        public string? TaxYear { get; set; }
        public string? Date { get; set; }
        [MaxLength(255)]
        public string? Department { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? Type { get; set; } = string.Empty;
        public int? SignersCount { get; set; }
        [MaxLength(255)]
        public string? OtherEntityDetails { get; set; } = string.Empty;
        public bool? IsEsigning { get; set; }
        public bool? IsManualSigning { get; set; }
        public bool Is7216Available { get; set; } = false;
        public bool IsNewClient { get; set; } = false;
        [MaxLength(255)]
        public string? ExpiryDate7216 { get; set; }
        [MaxLength(255)]
        public string? EngagementLetterStatus { get; set; }
        public string? EngagementLetterPDF { get; set; }
        public bool? IsLetterUploaded { get; set; }
        public bool? IsLetterStatusUpdated { get; set; }
        public bool? Is7216StatusUpdated { get; set; }
        public bool? Is7216ExpiryStatusUpdated { get; set; }
        [MaxLength(50)]
        public string? Bot_Status { get; set; }
        public int? BotExtracted7216SignCount { get; set; }
        public string? PrimarySignerFirstName { get; set; }
        public string? PrimarySignerLastName { get; set; }
        public string? DateandTime { get; set; }
        public string? RecordStatus { get; set; }
        public string? Exception { get; set; }
        public string? FormData { get; set; }
        public bool? IsCanabisAvailable { get; set; }
        public string? ReturnTypeCode { get; set; }
    }
}
