﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Data.DataModel
{
    public class PartnerClientMapping
    {
        [Key]
        public int PartnerClientMappingId { get; set; }
        public int? PartnerId { get; set; }
        [MaxLength(255)]
        public string? PartnerName { get; set; } = string.Empty;
        public int? ParentClientId { get; set; }
        [MaxLength(255)]
        public string? ParentClientName { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? ChildClient { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? AddressDetail { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? City { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? State { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? ZipCode { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? ContactName { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? ContactEmail { get; set; } = string.Empty;
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool? IsDeleted { get; set; } = false;
        public DateTime? DeletedOn { get; set; }
    }
}
