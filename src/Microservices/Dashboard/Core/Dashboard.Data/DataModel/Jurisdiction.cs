﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Data.DataModel
{
    public class Jurisdiction
    {
        [Key]
        public int? JurisdictionId { get; set; }
        [MaxLength(255)]
        public string CitrinDatabase { get; set; } = string.Empty;
        [MaxLength(255)]
        public string ReceivedFromCitrin { get; set; } = string.Empty;
        [MaxLength(255)]
        public string JurisdictionData { get; set; } = string.Empty;
    }
}
