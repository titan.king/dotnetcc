﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Data.DataModel
{
    public class MasterTemplate
    {
        [Key]
        public int MasterTemplateId { get; set; }
        [Required]
        public int TemplateId { get; set; }
        [Required]
        [MaxLength(255)]
        public string TemplateName { get; set; } = string.Empty;
        [Required]
        public int TemplateVersionId { get; set; }
        [Required]
        public string Template { get; set; } = string.Empty;
    }
}
