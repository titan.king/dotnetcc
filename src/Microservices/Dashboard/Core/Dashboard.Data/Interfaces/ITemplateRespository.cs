﻿using Dashboard.Data.DataModel;
using Dashboard.Models.ResponseModel;
using Dashboard.Models.Template;

namespace Dashboard.Data.Interfaces
{
    public interface ITemplateRespository
    {
        Task<TemplateListModel> SaveTemplate(TemplateListModel template);
        Task<TemplateListModel> UpdateTemplate(TemplateListModel Template);
        Task<IEnumerable<DepartmentsList>> GetDepartments();
        Task<IEnumerable<TemplateListModel>> GetTemplates();
        Task<TemplateListModel> GetTemplateById(int templateId);
        Task<IEnumerable<EngageTypesList>> GetEngageTypes(int DepartmentId);
        Task<IEnumerable<StatusListModel>> GetStatus();
        Task<bool> DeleteTemplate(TemplateListModel deleteModel);
        Task<IEnumerable<int>> GetTemplatesIdList();
        Task<MasterTemplate> UpdateMasterTable(TemplateListModel templateList);
        Task<TemplateAttachments> UpdateAttachments(TemplateListModel template);
    }
}
