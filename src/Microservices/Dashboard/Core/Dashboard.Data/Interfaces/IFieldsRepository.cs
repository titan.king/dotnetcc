﻿using Dashboard.Data.DataModel;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;

namespace Dashboard.Data.Interfaces
{
    public interface IFieldsRepository
    {
        Task<IEnumerable<FieldListModel>> GetFields();
        Task<GetFieldsByIdList> GetFieldsById(int fieldId);
        Task<Field> AddField(Field field);
        Task<Field> UpdateField(Field field);
        Task<IEnumerable<FieldListModel>> GetActiveFields();
        Task<IEnumerable<FieldsIdListResModel>> GetFieldsIdList();
        Task<FieldsStatusResModel> LoadStatus();
        Task<DeleteFieldResModel> DeleteFields(DeleteFieldResModel fieldId);
        Task<bool> IsValidFieldName(FieldListModel block);
        Task<IEnumerable<ColumnNamesResModel>> GetColumnNames();
        Task<FieldsResponse> GetActiveClientFields(int clientId, int partnerId);
    }
}
