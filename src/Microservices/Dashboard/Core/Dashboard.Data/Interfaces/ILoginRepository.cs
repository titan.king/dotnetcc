﻿using Dashboard.Models.Login;
using Dashboard.Models.ResponseModel;

namespace Dashboard.Data.Interfaces
{
    public interface ILoginRepository
    {
        Task<LoginViewModel> GetRoles(LoginViewModel permissions);
        Task<LoginListModel> GetRolesandScreenList(LoginListModel loginViewModel);
    }
}
