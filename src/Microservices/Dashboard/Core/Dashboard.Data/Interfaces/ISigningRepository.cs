﻿using Dashboard.Data.DataModel;
using Dashboard.Models.ResponseModel;
using Dashboard.Models.Signing;

namespace Dashboard.Data.Interfaces
{
    public interface ISigningRepository
    {
        Task<EngagementLetter?> GetEngagementLetterStatus(int engLetterId);
        Task<IEnumerable<SigningInfo>> GetAgreementId();
        Task<IEnumerable<int>> GetAllEngagementLetter();
        Task<EngagementLetter> UpdateSignedStatusEngagementLetter(EngagementLetter engLetter);
        Task<string?> GetPdfUrl(int letterId);
        Task<int?> GetHistoryLogVersion(int engId);
        Task InsertHistoryLog(HistoryLog historyLog);
        Task<string?> GetHistoryLogStatus(int engId);
        Task<LetterStatusRequest> InsertLetterStatusReports(LetterStatusRequest letterStatusRequest);
        Task<LetterStatusRequest> UpdateLetterStatusReports(LetterStatusRequest letterStatusRequest);
        Task<EngagementLetterList> GetEngagmentLettersByID(int id);
        Task<int> GetLetterStatusReport(int engId);
        Task<EmailTemplate> GetEmailContent(int StatusId);
        Task<EmailTemplate?> GetEmailTemplate(int? statusId);
        Task<int> GetDocumentStatusId(string description);
    }
}
