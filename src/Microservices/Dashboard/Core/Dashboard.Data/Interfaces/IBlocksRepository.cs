﻿using Dashboard.Data.DataModel;
using Dashboard.Models.Blocks;
using Dashboard.Models.ResponseModel;

namespace Dashboard.Data.Interfaces
{
    public interface IBlocksRepository
    {
        Task<Block> AddBlocks(Block blocks);
        Task<Block> UpdateBlocks(Block blocks);
        Task<bool> IsValidBlockName(BlocksResponseModel block);
        Task<BlocksCommonModel> GetBlocksById(int blockId);
        Task<IEnumerable<BlockIdListResponseModel>> GetBlocksIdList();
        Task<IEnumerable<BlocksList>> GetBlocks();
        Task<IEnumerable<ActiveBlocksList>> GetActiveBlocksList();
        Task<bool> DeleteBlock(DeleteBlockResModel blockId);
        Task<BlockStatusListResModel> Load();
        Task<List<TemplateListResponseModel>> GetConnectedTemplates();
    }
}
