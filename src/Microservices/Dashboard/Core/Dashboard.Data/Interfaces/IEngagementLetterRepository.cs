﻿using Dashboard.Data.DataModel;
using Dashboard.Models.EngagementLetter;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;

namespace Dashboard.Data.Interfaces
{
    public interface IEngagementLetterRepository
    {

        Task<IEnumerable<EngagementLetter>> GetEngagementLettersSearchFilter();
        Task<IEnumerable<Department>> GetDepartmentSearchFilter();
        Task<IEnumerable<EngageType>> GetEngageTypeSearchFilter();
        Task<IEnumerable<DocumentStatus>> GetDocumentStatusSearchFilter();
        Task<IEnumerable<EngagementLetterList>> GetEngagementLetters();
        Task<IEnumerable<EngagementLetterSearchList>> GetFilteredSearchLetters(EngagementLetterSearchList engagementLetterList);
        Task<bool> DeleteEngagementLetter(DeleteEngagementLetterResModel engLetterId);
        Task<string> GetSigningInfoDetail(int engLetterId);
        Task<EngagementLetter> GetEngagementLetterStatus(int engLetterId);
        Task<EngagementLetter> UpdateSignedStatusEngagementLetter(EngagementLetter engLetter);
        Task<List<HistoryLogResponseModel>> GetHistoryLogById(int engagementLetterId);
        Task<EngagementLetterList> GetEngagementLettersById(int bulkletterId);
        Task<bool> SaveEngagementLetterEditFields(EditFieldValuesResponse editField);
    }
}
