﻿using Dashboard.Data.DataModel;
using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.Clients;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;

namespace Dashboard.Data.Interfaces
{
    public interface IBulkLettersRepository
    {
        Task<bool> DeleteByBulkLetterId(DeleteBulkLetterResModel clientId);
        Task<bool> DeleteByBatchId(DeleteByBatchResModel batchId);
        Task<bool> InsertAttachmentsBulkLetter(BulkLetterEditAttachmentsResponseModel editAttachmentsViewModel);
        Task<BulkLetterEditAttachmentsResponseModel> GetBulkLetterAttachmentById(int batchId, int templateId);
        Task<BulkLetterFieldsResponseModel> InsertFieldsList(BulkLetterFieldsResponseModel fieldsList);
        Task<List<PartnerOfficeResponse>> GetPartnerOfficeListAsync(int batchId);
        Task<EditFieldValuesResponse> GetBulkLetterFieldsById(int bulkLetterId);
        Task<bool> SaveEditFields(EditFieldValuesResponse editField);
        Task<BatchRequestResponse> AddBatchRequestDetails(BatchRequestResponse batchRequest);
        Task<bool> IsBatchrequestIdExists(int batchId);
        Task<string> GetTemplateName(BulkLetterEditAttachmentsResponseModel responseModel);
        Task<string> GetBulkAttachments(int batchId, int templateId);
        Task<string> GetTemplateAttachments(int templateId);
        Task<ResponseModel> GetTemplateForIndv(int getById, bool isBulkLetterId);
        Task<bool> RemoveClientData(RemoveClientDataResModel clientId);
    }
}
