﻿using Dashboard.Data.DataModel;
using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.Clients;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Fields;

namespace Dashboard.Data.Interfaces
{
    public interface IClientRepository
    {
        Task<IEnumerable<t_engletterdata>> GetPartnersNameAsync();
        Task<LetterFieldValuesBase> FetchingClientAsync(PartnerRequest partnerRequest);
        Task<ExportDataResponse> ExportDataAsync(ExportDataRequest exportDataRequest);
        Task<List<BulkLetters>> SaveFetchingLetterAsync(List<BulkLetters> bulkLettersListRes, int batchId, PartnerRequest partnerRequest);
        Task<IEnumerable<BatchIdsResponse>> GetBatchIdsAsync();
        Task<IEnumerable<BulkLetterBatchResponse>> GetAllBatchLetterAsync();
        Task<BulkDraftLetterResponse> GetBatchLettersByIdAsync(int batchId);
        Task<BulkLetters> CheckExistingBatchId(int batchId);
        Task<BulkLettersResponse> AddClientBulkDetails(BulkLettersResponse bulkLetters);
        Task<List<LetterFieldValuesResponse>> AddFieldIdtoList(List<LetterFieldValuesResponse> letterFieldValues);
        Task<bool> IsNotExistingClientId(int clientId, int batchId);
        Task<bool> IsNotExistingClientName(string clientName, int batchId);
        Task<string> CheckFieldValues(List<int> bulkLetterIds);
        Task<string> CheckBatchIsActive(int BatchId, string userName);
        Task<bool> UpdateIsBatchActive(ActiveUserRequest activeUserRequest);
        Task<bool> SaveClientData(List<ClientData> clientData);
        Task<LetterFieldValuesList> UpdateExistingPartnersRecords(DuplicatePartnersRequest duplicatePartners);
        Task<BulkDraftLetterResponse> SaveImportedData(List<ImportDataViewModel> importData);
        Task<(int, bool)> GetAddClientData(string templateName);
        Task<BulkDraftLetterResponse> GetImportBatchLettersByIdAsync(int batchId);
        Task<string> GetTypeCodeList(int templateId);
    }
}
