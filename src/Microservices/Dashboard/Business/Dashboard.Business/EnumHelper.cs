﻿namespace Dashboard.Business
{
    public class EnumHelper
    {
        public enum Status
        {
            CANCELLED,
            REJECTED
        }

        public enum DocumentStatus
        {
            WithAdminForReview = 4,
            Completed = 1
        }
    }
}
