﻿using Dashboard.Models.ResponseModel;

namespace Dashboard.Business.Interfaces
{
    public interface IBlocksService
    {
        Task <Tuple<BlocksResponseModel , string>> AddBlocks(BlocksResponseModel blocks);
        Task<Tuple<BlocksResponseModel, string>> UpdateBlocks(BlocksResponseModel blocks);
        Task<List<BlocksList>> GetBlocksById(int blockId);
        Task<IEnumerable<BlockIdListResponseModel>> GetBlocksIdList();
        Task<IEnumerable<BlocksList>> GetBlocks();
        Task<IEnumerable<ActiveBlocksList>> GetActiveBlocksList();
        Task<DeleteBlockResModel> DeleteBlock(DeleteBlockResModel block);
        Task<BlockStatusListResModel> Load();
    }
}
