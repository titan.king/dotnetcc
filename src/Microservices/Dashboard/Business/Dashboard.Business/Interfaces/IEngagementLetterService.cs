﻿using Dashboard.Models.EngagementLetter;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;

namespace Dashboard.Business.Interfaces
{
    public interface IEngagementLetterService
    {
        Task<EngagementLetterSearchFilterViewModel> GetEngagementLetterFilterFields();
        Task<IEnumerable<EngagementLetterList>> GetEngagementLetters();
        Task<IEnumerable<EngagementLetterListViewModel>> GetFilteredSearchLetters(EngagementLetterSearchList engagementLetterList);
        Task<bool> DeleteEngagementLetter(DeleteEngagementLetterResModel engLetterId);
        Task<List<HistoryLogResponseModel>> GetHistoryLogById(int engagementLetterId);
        Task<EngagementLetterList> GetEngagementLettersById(int bulkletterId);
        Task<bool> SaveEngagementLetterEditFields(EditFieldValuesResponse editField);
    }
}
