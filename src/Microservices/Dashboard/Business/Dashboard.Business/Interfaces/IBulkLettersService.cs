﻿using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;

namespace Dashboard.Business.Interfaces
{
    public interface IBulkLettersService
    {     
        Task<bool> DeleteByBulkLetterId(DeleteBulkLetterResModel clientId);
        Task<bool> DeleteByBatchId(DeleteByBatchResModel batchId);
        Task<BulkLetterFieldsResponseModel> InsertFieldsList(BulkLetterFieldsResponseModel fieldsList);
        Task<bool> InsertAttachmentsBulkLetter(BulkLetterEditAttachmentsResponseModel editAttachmentsViewModel);
        Task<BulkLetterEditAttachmentsResponseModel> GetBulkLetterAttachmentById(int batchId, int templateId);
        Task<List<PartnerOfficeResponse>> GetPartnerOfficeListAsync(int batchId);
        Task<EditFieldValuesResponse> GetBulkLetterFieldsById(int bulkLetterId);
        Task<bool> SaveEditFields(EditFieldValuesResponse editField);
        Task<ResponseModel> UploadHTMLLetter(LetterHTMLRequestModel letterHtmlContent);
        Task<List<string>> UploadAttachment(AttachmentPdfFile pdfDetails);
        Task<ResponseModel> CombinePDFReq(CombinePDFReq combinePDF);
        Task<List<AttachmentBase64String>> GetAttachmentsList( int batchId, int templateId);
        Task<ResponseModel> UploadPdfFileFromBlob(UploadPdfFile uploadPdfFile);
        Task<ResponseModel> AddBatchRequestDetails(BatchRequestResponse batchRequest);
        Task<string> Get7216FormAssetId(BulkLettersCombinePdfViewModel viewModel);
        Task<bool> RemoveClientData(RemoveClientDataResModel clientId);

    }
}
