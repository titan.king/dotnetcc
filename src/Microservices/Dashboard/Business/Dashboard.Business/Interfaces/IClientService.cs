﻿using Dashboard.Data.DataModel;
using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.Clients;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Fields;
using Microsoft.AspNetCore.Http;

namespace Dashboard.Business.Interfaces
{
    public interface IClientService
    {
        Task<IEnumerable<t_engletterdata>> GetPartnersNameAsync();
        Task<LetterFieldValuesList> FetchingClientAsync(PartnerRequest partnerRequest);
        Task<ExportDataResponse> ExportDataAsync(ExportDataRequest exportDataRequest);
        Task<IEnumerable<BatchIdsResponse>> GetBatchIdsAsync();
        Task<IEnumerable<BulkLetterBatchResponse>> GetAllBatchLetterAsync();
        Task<BulkDraftLetterResponse> GetBatchLettersByIdAsync(int batchId);
        Task<ResponseModel> AddClientBulkDetails(BulkLettersResponse bulkLetters);
        Task<string> CheckBatchIsActive(int BatchId,string userName);
        Task<bool> UpdateIsBatchActive(ActiveUserRequest activeUserRequest);
        Task<bool> SaveClientData(List<ClientData> clientData);
        Task<LetterFieldValuesList> UpdateExistingPartnersRecords(DuplicatePartnersRequest duplicatePartners);
        Task<BulkDraftLetterResponse> ImportData(IFormFile fromFiles);
        Task<List<ReturnTypeModel>> GetTypeCodeList(int templateId);
        Task<string> CheckFieldValues(List<int> bulkLetterIds);
    }
}
