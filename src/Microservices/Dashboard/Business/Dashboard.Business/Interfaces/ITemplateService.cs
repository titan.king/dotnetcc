﻿using Dashboard.Models.CommonModel;
using Dashboard.Models.ResponseModel;
using Dashboard.Models.Template;
using System.Threading.Tasks;

namespace Dashboard.Business.Interfaces
{
    public interface ITemplateService
    {
        Task<TemplateViewModel> SaveTemplate(TemplateViewModel template);
        Task<TemplateViewModel> UpdateTemplate(TemplateViewModel Template);
        Task<IEnumerable<TemplateListModel>> GetTemplates();
        Task<TemplateListModel> GetTemplateById(int TemplateId);
        Task<bool> DeleteTemplate(TemplateViewModel deleteModel);
        Task<IEnumerable<DepartmentsList>> GetDepartments();
        Task<IEnumerable<EngageTypesList>> GetEngageTypes(int DepartmentId);
        Task<IEnumerable<StatusListModel>> GetStatus();
        Task<IEnumerable<int>> GetTemplatesIdList();
        Task<Tuple<bool, string>> IsValid(TemplateViewModel Template);
        Task<PagedJsResponseModel> GeneratePagedJsHtml(PagedJsRequestModel pagedJsRequest);
        Task<string> GetAttachmentAsset(string Attachmentfile);
        Task<ResponseModel> HtmltoPDFConvert(PdfConversionRequest pdfConversionRequest);
        Task<ResponseModel> GetAccessToken();
        Task<List<string>> UploadAttachment(AttachmentPdfFile pdfDetails);
        Task<ResponseModel> UploadHTMLLetter(LetterHTMLRequestModel letterHtmlContent);
        Task<ResponseModel> CombinePDFReq(CombinePDFReq combinePDF);
    }
}
