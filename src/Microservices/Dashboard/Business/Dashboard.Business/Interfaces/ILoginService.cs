﻿using Dashboard.Models.Login;
using Dashboard.Models.ResponseModel;

namespace Dashboard.Business.Interfaces
{
    public interface ILoginService
    {
        Task<LoginViewModel> GetRoles(LoginViewModel permissions);
        Task<LoginViewModel> GetRolesandScreenList(LoginListModel loginViewModel);
    }
}
