﻿using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;

namespace Dashboard.Business.Interfaces
{
    public interface IFieldsService
    {
        Task<IEnumerable<FieldListModel>> GetFields();
        Task<IEnumerable<FieldListModel>> GetActiveFields();
        Task<IEnumerable<FieldsIdListResModel>> GetFieldsIdList();
        Task<FieldsStatusResModel> LoadStatus();
        Task<Tuple<FieldListModel , string>> AddField(FieldListModel field);
        Task<Tuple<FieldListModel, string>> UpdateField(FieldListModel Field);
        Task<GetFieldsByIdList> GetFieldsById(int fieldId);
        Task<DeleteFieldsViewModel> DeleteFields(DeleteFieldsViewModel deleteId);
        Task<IEnumerable<ColumnNamesResModel>> GetColumnNames();
        Task<FieldsResponse> GetActiveClientFields(int clientId, int partnerId);
    }
}
