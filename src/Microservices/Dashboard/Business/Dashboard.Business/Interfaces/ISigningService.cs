﻿using Dashboard.Models.CommonModel;
using Dashboard.Models.ResponseModel;
using Dashboard.Models.Signing;

namespace Dashboard.Business.Interfaces
{
    public interface ISigningService
    {
        Task<ResponseModel> CancelSigning(CancleSigningResModel signingViewModel);
        Task<ManualRefreshResModel> RefreshedSignedLetterStatus(ManualRefreshResModel signingDocument);
        Task<ResponseModel> AutoRefreshStatus();
        Task<string> GetPdfUrl(int letterId);
        Task<ResponseModel> UploadFinalVersionPDF(UploadLetterResModel uploadLetterRes);
    }
}
