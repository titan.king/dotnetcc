﻿using Dashboard.Business.Interfaces;
using Dashboard.Data.Interfaces;
using Dashboard.Models.EngagementLetter;
using Dashboard.Data.DataModel;
using Dashboard.Models.ResponseModel;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Dashboard.Data.Repositories;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System.Net;
using Dashboard.Models.Signing;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Fields;

namespace Dashboard.Business.Services
{
    public class EngagementLetterService : IEngagementLetterService
    {
        private readonly IEngagementLetterRepository _engagementLetterRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public EngagementLetterService(IEngagementLetterRepository engagementLetterRepository, IMapper mapper, IConfiguration configuration)
        {
            _engagementLetterRepository = engagementLetterRepository;
            _mapper = mapper;
            _configuration = configuration;
        }

        public async Task<EngagementLetterSearchFilterViewModel> GetEngagementLetterFilterFields()
        {
            EngagementLetterSearchFilterViewModel searchFilterResult = new EngagementLetterSearchFilterViewModel();
            IEnumerable<EngagementLetter> engamentLetters = await _engagementLetterRepository.GetEngagementLettersSearchFilter();
            IEnumerable<Department> departments = await _engagementLetterRepository.GetDepartmentSearchFilter();
            IEnumerable<EngageType> engageTypes = await _engagementLetterRepository.GetEngageTypeSearchFilter();
            IEnumerable<DocumentStatus> documentStatus = await _engagementLetterRepository.GetDocumentStatusSearchFilter();

            searchFilterResult.EngagementLetterIds =(from n in engamentLetters
                                                 join d in documentStatus on n.DocumentStatusId equals d.DocumentStatusId
                                                 join e in departments on n.DepartmentId equals e.DepartmentId
                                                 where !n.IsDeleted
                                                 orderby n.EngagementLetterId
                                                 select new EngagementLetterIdsViewModel
                                                 {
                                                    EngagementLetterId = n.EngagementLetterId,
                                                    PartnerName = n.PartnerName ?? string.Empty,
                                                    CreatedBy = n.CreatedBy ?? string.Empty,
                                                    Status = d.Description ?? string.Empty,
                                                    OfficeName = n.OfficeName ?? string.Empty,
                                                    AdminName = n.AdminName ?? string.Empty,
                                                    TemplateName = n.TemplateName ?? string.Empty,
                                                    clientId = n.ClientId ,
                                                    DepartmentId = e.DepartmentId,
                                                    EngageTypeId = n.EngageTypeId,
                                                    TaxYear = n.TaxYear

                                                 }).ToList();

            searchFilterResult.EngagementLetterNames = engamentLetters.OrderBy(n => n.EngagementLetterId)
                                                    .Select(n => new EngagementLetterNamesViewModel
                                                    {
                                                        EngagementLetterId = n.EngagementLetterId,
                                                        EngagementLetterName = n.EngagementLetterName ?? string.Empty,
                                                        PartnerName = n.PartnerName ?? string.Empty,
                                                        CreatedBy = n.CreatedBy ?? string.Empty
                                                    }).ToList();

            searchFilterResult.ClientNames =   engamentLetters.OrderBy(n => n.ClientId) 
                                            .Select(n => new ClientNamesViewModel
                                            {
                                                ClientId =n.ClientId, 
                                                ClientName = n.ClientId + " - " + n.ClientName
                                            }).ToList();
       
            searchFilterResult.Years = engamentLetters
                                    .OrderBy(n => n.YearId) 
                                    .GroupBy(n => n.TaxYear) 
                                    .Select(group => new TaxYearsViewModel
                                    {
                                        YearId = group.FirstOrDefault()?.YearId, 
                                        TaxYear = group.Key
                                    })
                                    .ToList();

            searchFilterResult.PartnerNames = engamentLetters.OrderBy(n => n.PartnerId)
                                                               .Select(n => new SigningPartnersViewModel
                                                               {
                                                                   PartnerId = n.PartnerId,   
                                                                   PartnerName = n.PartnerName ?? string.Empty
                                                               }).ToList();

            searchFilterResult.OfficeNames = engamentLetters.OrderBy(n => n.OfficeId)
                                                        .Select(n => new OfficeDetailsViewModel
                                                        {
                                                            OfficeId = n.OfficeId,
                                                            OfficeName = n.OfficeName ?? string.Empty
                                                        }).ToList();

            searchFilterResult.AdminNames = engamentLetters.OrderBy(n => n.AdminId)
                                                      .Select(n => new AdminDetailsViewModel
                                                      {
                                                          AdminId = n.AdminId,
                                                          AdminName = n.AdminName ?? string.Empty
                                                      }).ToList();

            searchFilterResult.TemplateNames = engamentLetters.OrderBy(n => n.TemplateName)
                                                      .GroupBy(a => a.TemplateName)
                                                      .Select(n => new TemplateNamesViewModel
                                                      {
                                                          TemplateId = n.FirstOrDefault()?.TemplateId,
                                                          TemplateName = n.Key ?? string.Empty  
                                                      }).ToList();    

            searchFilterResult.Types = (from a in engamentLetters
                                     join b in engageTypes on a.EngageTypeId equals b.EngageTypeId
                                     group b by new { b.EngageTypeId, b.EngageTypeName } into typeGroup
                                     orderby typeGroup.Key.EngageTypeId
                                     select new EngageTypesViewModel
                                     {
                                         EngageTypeId = typeGroup.Key.EngageTypeId,
                                         EngageTypeName = typeGroup.Key.EngageTypeName,
                                     }).ToList();

            searchFilterResult.LetterStatus = (from a in engamentLetters
                                            join b in documentStatus on a.DocumentStatusId equals b.DocumentStatusId
                                            group b by new { b.DocumentStatusId,b.Description } into statusGroup
                                            orderby statusGroup.Key.DocumentStatusId                                           
                                            select new LetterStatusViewModel
                                            {
                                                DocumentStatusId = statusGroup.Key.DocumentStatusId,
                                                Status = statusGroup.Key.Description,
                                            }).ToList();

            searchFilterResult.Departments = (from a in engamentLetters
                                           join b in departments on a.DepartmentId equals b.DepartmentId
                                           group b by new { b.DepartmentId, b.DepartmentName }  into departmentGroup
                                           orderby departmentGroup.Key.DepartmentId                                          
                                            select new DepartmentsViewModel
                                            {
                                                DepartmentId = departmentGroup.Key.DepartmentId,
                                                Department = departmentGroup.Key.DepartmentName
                                            }).ToList();
          
            return searchFilterResult;
        }
        public async Task<IEnumerable<EngagementLetterListViewModel>> GetFilteredSearchLetters(EngagementLetterSearchList engagementLetterList)
        {
            var searchList = new List<EngagementLetterListViewModel>();
            var resData = await _engagementLetterRepository.GetFilteredSearchLetters(engagementLetterList);
            searchList = _mapper.Map<List<EngagementLetterListViewModel>>(resData);
            return searchList;
        }
        public async Task<IEnumerable<EngagementLetterList>> GetEngagementLetters()
        {
            return await _engagementLetterRepository.GetEngagementLetters();
        }
        public async Task<EngagementLetterList> GetEngagementLettersById(int bulklettersId)
        {
            return await _engagementLetterRepository.GetEngagementLettersById(bulklettersId);
        }

        public async Task<bool> DeleteEngagementLetter(DeleteEngagementLetterResModel engLetterId)
        {
            if (engLetterId != null)
            {     
                SigningInfo signingInfo = new SigningInfo();
                signingInfo.EngagementLetterId = engLetterId.EngagementLetterId;
                var signingInfoDetail = await _engagementLetterRepository.GetSigningInfoDetail(engLetterId.EngagementLetterId);
                var adobeStstus =await RefreshedSignedLetterStatus(engLetterId.EngagementLetterId, signingInfoDetail);
                if(adobeStstus.Data?.ToString() != "SIGNED")
                {
                    AdobeCancleSignRequest cancleSignRequest = new AdobeCancleSignRequest();
                    var agreementId = signingInfoDetail;
                    var authorizationKey = _configuration["AppSettingsAdobe:adobe_integrationkey"];
                    var fileRes = new RestClient(_configuration["AppSettingsAdobe:RefreshStatus"] + agreementId + "/events");

                    var eventRequest = new RestRequest(Method.GET);
                    eventRequest.AddHeader("Authorization", "Bearer " + authorizationKey);
                    IRestResponse eventRes = await fileRes.ExecuteAsync(eventRequest);

                    var deleteLetter = await _engagementLetterRepository.DeleteEngagementLetter(engLetterId);
                }
                else
                {
                    return false;
                }
            }
            return true;
        }

        public async Task<ResponseModel> RefreshedSignedLetterStatus(int engId, string agreementId)
        {
            ResponseModel resobj = new ResponseModel();
            EngagementLetter engagementLetters = await _engagementLetterRepository.GetEngagementLetterStatus(engId);

            if (engagementLetters.DocumentStatusId == 2)
            {
                string? agreementID = agreementId;

                if (!string.IsNullOrEmpty(agreementID))
                {
                    string? authorizationKey = _configuration["AppSettingsAdobe:adobe_integrationkey"];

                    var Fileclient = new RestClient(_configuration["AppSettingsAdobe:RefreshStatus"] + agreementID);

                    var filerequest = new RestRequest(Method.GET);
                    filerequest.AddHeader("Authorization", "Bearer " + authorizationKey);
                    IRestResponse Fileresponse = await Fileclient.ExecuteAsync(filerequest);
                    if (Fileresponse.StatusCode == HttpStatusCode.OK)
                    {
                        var res = Fileresponse.Content;
                        var responseContent = JsonConvert.DeserializeObject<AdobeSignStatusResponseVM>(res);
                        string? agreementStatus = responseContent?.status;
                        var adobeSignerCount = responseContent?.participantSetsInfo?.Count();
                        if (agreementStatus == "SIGNED")
                        {
                            var statusId = 1;
                            engagementLetters.DocumentStatusId = statusId;
                            var updateStatus = await _engagementLetterRepository.UpdateSignedStatusEngagementLetter(engagementLetters);
                        }
                        resobj.Data = agreementStatus??string.Empty;
                        resobj.Status = true;
                    }
                }
            }
            return resobj;
        }
        public async Task<List<HistoryLogResponseModel>> GetHistoryLogById(int engagementLetterId)
        {
            return await _engagementLetterRepository.GetHistoryLogById(engagementLetterId);
        }
        public async Task<bool> SaveEngagementLetterEditFields(EditFieldValuesResponse editField)
        {
            return await _engagementLetterRepository.SaveEngagementLetterEditFields(editField);
        }
    }
}

