﻿using AutoMapper;
using Azure.Storage;
using Azure.Storage.Sas;
using Dashboard.Business.Interfaces;
using Dashboard.Data.Context;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.ResponseModel;
using Dashboard.Models.Template;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using RestSharp;
using System.Net;
using System.Net.Http.Headers;
using System.Resources;
using System.Text;
using System.Text.RegularExpressions;

namespace Dashboard.Business.Services
{
    public class TemplateService : ITemplateService
    {
        private readonly ITemplateRespository _templateRespository;
        private readonly IMapper _mapper;
        private readonly DashboardDBContext _dbContext;
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ResourceManager _resourceManager;
        public TemplateService(ITemplateRespository TemplateRespository, IMapper mapper, DashboardDBContext dbContext, IConfiguration configuration, IHttpContextAccessor httpContextAccessor, ResourceManager resourceManager)
        {
            _templateRespository = TemplateRespository;
            _mapper = mapper;
            _dbContext = dbContext;
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
            _resourceManager = resourceManager;
        }

        public async Task<TemplateViewModel> SaveTemplate(TemplateViewModel template)
        {
            var templateReq = _mapper.Map<TemplateListModel>(template);
            PagedJsRequestModel pagedJsRequest = new PagedJsRequestModel();
            if (template != null)
            {
                byte[] bytes = Convert.FromBase64String(template.TemplateHtml);
                string utf8String = Encoding.UTF8.GetString(bytes);
                pagedJsRequest.payload = utf8String;
                var pagedJsHtmlResponse = await GeneratePagedJsHtml(pagedJsRequest);
                if (pagedJsHtmlResponse.StatusCode == HttpStatusCode.OK)
                {
                    var serial = JsonConvert.SerializeObject(pagedJsHtmlResponse.Data);
                    var pagedJsHtml = JsonConvert.DeserializeObject<PagedJsResponseModel>(serial);
                    if (pagedJsHtml != null)
                    {
                        templateReq.TemplateHtml = pagedJsHtml.payload;
                    }
                }
                var getResult = await GetCombinedAttachmentURL(templateReq);
                templateReq.AttachmentURL = getResult.ToString();
            }
            
            var inserttemplateRes = await _templateRespository.SaveTemplate(templateReq);
            return _mapper.Map<TemplateViewModel>(inserttemplateRes);
        }
        public async Task<TemplateViewModel> UpdateTemplate(TemplateViewModel Template)
        {
            var templateReq = _mapper.Map<TemplateListModel>(Template);
            PagedJsRequestModel pagedJsRequest = new PagedJsRequestModel();
            if (Template != null)
            {
                byte[] bytes = Convert.FromBase64String(Template.TemplateHtml);
                string utf8String = Encoding.UTF8.GetString(bytes);
                pagedJsRequest.payload = utf8String;
                var pagedJsHtmlResponse = await GeneratePagedJsHtml(pagedJsRequest);
                if (pagedJsHtmlResponse.StatusCode == HttpStatusCode.OK)
                {
                    var serial = JsonConvert.SerializeObject(pagedJsHtmlResponse.Data);
                    var pagedJsHtml = JsonConvert.DeserializeObject<PagedJsResponseModel>(serial);
                    if (pagedJsHtml != null)
                    {
                        templateReq.TemplateHtml = pagedJsHtml.payload;
                    }
                }
                var getResult = await GetCombinedAttachmentURL(templateReq);
                templateReq.AttachmentURL = getResult.ToString();
            }
            
            var updateTemplateRes = await _templateRespository.UpdateTemplate(templateReq);
            if (updateTemplateRes.TemplateId > 0 && updateTemplateRes.TemplateVersionId > 0)
            {
                if (!string.IsNullOrEmpty(updateTemplateRes.TemplateHtml))
                {
                    await _templateRespository.UpdateMasterTable(updateTemplateRes);
                }

                if (!string.IsNullOrEmpty(updateTemplateRes.AttachmentJson))
                {
                    await _templateRespository.UpdateAttachments(updateTemplateRes);
                }
            }
            return _mapper.Map<TemplateViewModel>(updateTemplateRes);
        }
        public async Task<string> GetCombinedAttachmentURL(TemplateListModel templateReq)
        {
            if (templateReq != null && !string.IsNullOrEmpty(templateReq.AttachmentJson) && templateReq.AttachmentJson != "[]")
            {
                List<AttachmentBase64String> attachmentBase64s = new List<AttachmentBase64String>();
                var deSerialJSON = JsonConvert.DeserializeObject<List<AttachmentBase64String>>(templateReq.AttachmentJson);
                if (deSerialJSON.Count > 0)
                {
                    AttachmentPdfFile attachmentPdf = new AttachmentPdfFile();
                    attachmentPdf.UploadAttachmentJSON = deSerialJSON;
                    var attachmentRes = await UploadAttachment(attachmentPdf);
                    CombinePDFReq combinePDF = new CombinePDFReq();
                    List<string> assetIDList = new List<string>();
                    if (attachmentRes != null)
                    {
                        int i = 0;
                        foreach (var item in attachmentRes)
                        {
                            assetIDList.Add(item.ToString());
                            if (i == 0)
                            { combinePDF.Attachment1 = item.Replace("\"", string.Empty).Replace('"', ' ').ToString(); i++; }
                            else if (i == 1)
                            { combinePDF.Attachment2 = item.Replace("\"", string.Empty).Replace('"', ' ').ToString(); i++; }
                            else if (i == 2)
                            { combinePDF.Attachment3 = item.Replace("\"", string.Empty).Replace('"', ' ').ToString(); i++; }
                            else if (i == 3)
                            { combinePDF.Attachment4 = item.Replace("\"", string.Empty).Replace('"', ' ').ToString(); i++; }
                            else if (i == 4)
                            { combinePDF.Attachment5 = item.Replace("\"", string.Empty).Replace('"', ' ').ToString(); i++; }
                        }
                    }
                    combinePDF.TemplateName = templateReq.TemplateName ?? string.Empty;
                    var combinePDFResponse = await CombinePDFReq(combinePDF);
                    if (combinePDFResponse != null)
                    {
                        templateReq.AttachmentURL = combinePDFResponse?.Data?.ToString();
                    }
                    else
                    {
                        templateReq.AttachmentURL = string.Empty;
                    }
                }
            }
            return templateReq?.AttachmentURL??string.Empty;
        }
        public async Task<IEnumerable<TemplateListModel>> GetTemplates()
        {
            var listModel = new List<TemplateListModel>();
            var tempListRes = await _templateRespository.GetTemplates();
            listModel = _mapper.Map<List<TemplateListModel>>(tempListRes);
            return listModel;
        }

        public async Task<TemplateListModel> GetTemplateById(int TemplateId)
        {
            var templateById = new TemplateListModel();
            var templateByIdResp = await _templateRespository.GetTemplateById(TemplateId);
            templateById = _mapper.Map<TemplateListModel>(templateByIdResp);
            return templateById;
        }

        public async Task<bool> DeleteTemplate(TemplateViewModel deleteModel)
        {
            var deleteReq = _mapper.Map<TemplateListModel>(deleteModel);
            var deleteResp = await _templateRespository.DeleteTemplate(deleteReq);
            if (deleteResp == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<IEnumerable<DepartmentsList>> GetDepartments()
        {
            var listModel = new List<DepartmentsList>();
            var deptListRes = await _templateRespository.GetDepartments();
            listModel = _mapper.Map<List<DepartmentsList>>(deptListRes);
            return listModel;
        }

        public async Task<IEnumerable<EngageTypesList>> GetEngageTypes(int departmentId)
        {
            var deptId = new List<EngageTypesList>();
            var departmentIdResp = await _templateRespository.GetEngageTypes(departmentId);
            deptId = _mapper.Map<List<EngageTypesList>>(departmentIdResp);
            return deptId;
        }

        public async Task<IEnumerable<StatusListModel>> GetStatus()
        {
            var listModel = new List<StatusListModel>();
            var statusListRes = await _templateRespository.GetStatus();
            listModel = _mapper.Map<List<StatusListModel>>(statusListRes);
            return listModel;
        }
        public async Task<Tuple<bool, string>> IsValid(TemplateViewModel Template)
        {

            var result = true;
            var TemplateName = string.Empty;
            TemplateName = Template.TemplateName;
            string ValidationErrorMessage = string.Empty;
            bool isvalidTemplateName = await IsValidTemplateName(Template);
            if (!isvalidTemplateName)
            {
                ValidationErrorMessage = "Template Name already exists.";
                result = false;
            }

            return new Tuple<bool, string>(result, ValidationErrorMessage);
        }

        private async Task<bool> IsValidTemplateName(TemplateViewModel Template)
        {
            int count = 0;
            bool isValid = false;
            string templateName = Template.TemplateName;
            int templateId = Template.TemplateId;
            if (Template.TemplateId == 0 && templateName != null)
            {
                count = _dbContext.Template
                                .Where(t =>
                                    t.TemplateName.Replace("\t", "")
                                                        .Replace("\n", "")
                                                        .Replace("\r", "")
                                                        .Replace(" ", "") == templateName.Replace("\t", "")
                                                .Replace("\n", "")
                                                .Replace("\r", "")
                                                .Replace(" ", "")
                                )
                                .Where(t => !t.IsDeleted)
                                .Count();
                if (count > 0)
                {
                    isValid = false;
                }
                else
                {
                    isValid = true;
                }
            }
            else
            {
                count = _dbContext.Template
                                .Where(t =>
                                    t.TemplateName.Replace("\t", "")
                                                        .Replace("\n", "")
                                                        .Replace("\r", "")
                                                        .Replace(" ", "") == templateName.Replace("\t", "")
                                                .Replace("\n", "")
                                                .Replace("\r", "")
                                                .Replace(" ", "")
                                )
                                .Where(t => !t.IsDeleted && t.TemplateId != templateId)
                                .Count();
                if (count > 0)
                {
                    isValid = false;
                }
                else
                {
                    isValid = true;
                }
            }
            return isValid;
        }
        private async Task<bool> IsValidTemplate(TemplateViewModel Template)
        {
            int count = 0;
            bool isValid = false;
            string templateName = Template.TemplateName;
            int departmentId = Template.DepartmentId;
            int engagetypeID = Template.EngageTypeId;
            int templateId = Template.TemplateId;
            if (templateId == 0)
            {
                count = _dbContext.Template
                                .Where(t => t.DepartmentId == departmentId && t.EngageTypeId == engagetypeID && !t.IsDeleted)
                                .Count();
                if (count > 0)
                {
                    templateName = await _dbContext.Template
                        .Where(t => t.DepartmentId == departmentId && t.EngageTypeId == engagetypeID && !t.IsDeleted)
                        .Select(t => t.TemplateName)
                        .FirstOrDefaultAsync();
                    isValid = false;
                }
                else { isValid = true; }
            }
            else
            {
                count = _dbContext.Template
                                .Where(t => t.DepartmentId == departmentId && t.EngageTypeId == engagetypeID && !t.IsDeleted && t.TemplateId != templateId)
                                .Count();
                if (count > 0)
                {
                    templateName = await _dbContext.Template
                        .Where(t => t.DepartmentId == departmentId && t.EngageTypeId == engagetypeID && !t.IsDeleted && t.TemplateId != templateId)
                        .Select(t => t.TemplateName)
                        .FirstOrDefaultAsync();
                    isValid = false;
                }
                else { isValid = true; }
            }
            return isValid;
        }

        public async Task<IEnumerable<int>> GetTemplatesIdList()
        {
            return await _templateRespository.GetTemplatesIdList();
        }

        public async Task<PagedJsResponseModel> GeneratePagedJsHtml(PagedJsRequestModel pagedJsRequest)
        {
            var ree = JsonConvert.SerializeObject(pagedJsRequest);
            ResponseModel resobj = new ResponseModel();
            PagedJsResponseModel pagedJsResponseModel = new PagedJsResponseModel();

            if (!string.IsNullOrEmpty(pagedJsRequest.payload))
            {
                string pagedJS_AdobeURL = _configuration["AppSettingsAdobe:GeneratePagedJS"] ?? string.Empty;
                var Clients = new RestClient(pagedJS_AdobeURL);
                var jsonData = JsonConvert.SerializeObject(pagedJsRequest);
                var request = new RestRequest(Method.POST);
                request.AddHeader("x-api-key", "c97c4b0d-cb47-494e-babf-f50162ca06d6");
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", jsonData, ParameterType.RequestBody);

                IRestResponse pagedJsResponse = await Clients.ExecuteAsync(request);
                if (pagedJsResponse.StatusCode == HttpStatusCode.OK)
                {
                    var res = pagedJsResponse.Content;
                    var responseContent = JsonConvert.DeserializeObject<PagedJsResponseModel>(res);
                    if (responseContent != null)
                    {
                        HtmlDocument doc = new HtmlDocument();
                        doc.LoadHtml(responseContent.payload);
                        doc.DocumentNode.ChildNodes[1].ChildNodes[2].ChildNodes[0].InnerHtml = string.Empty;
                        doc.DocumentNode.ChildNodes[1].ChildNodes[2].ChildNodes[2].InnerHtml = string.Empty;
                        doc.DocumentNode.InnerHtml = doc.DocumentNode.OuterHtml.Replace("height: calc(100% - var(--pagedjs-footnotes-height));", "height: calc(110% - var(--pagedjs-footnotes-height));");
                        responseContent.payload = doc.DocumentNode.OuterHtml;
                        pagedJsResponseModel.Data = responseContent;
                        pagedJsResponseModel.StatusCode = HttpStatusCode.OK;

                        //string searchText = "Dte_es_:signer1:date";
                        //var datas = doc.DocumentNode.ChildNodes[1].ChildNodes[2].ChildNodes[1].ChildNodes.Where(a => a.Name != "#text")?.ToArray();
                        //List<int> indices = new List<int>();
                        //if (datas != null)
                        //{
                        //    for (int i = 0; i < datas.Length; i++)
                        //    {
                        //        if (datas[i] != null && !string.IsNullOrEmpty(datas[i].InnerHtml) && datas[i].InnerHtml.Contains(searchText))
                        //        {
                        //            indices.Add(i);
                        //        }
                        //    }
                        //    if (indices.Count() > 0)
                        //    {
                        //        HtmlNode targetNode = datas[0];
                        //        HtmlNodeCollection divNodes = targetNode.SelectNodes("//div[contains(@class, 'pagedjs_margin pagedjs_margin-top-center hasContent')]");
                        //        if (divNodes != null)
                        //        {
                        //            int count = 0;
                        //            foreach (HtmlNode divNode in divNodes)
                        //            {
                        //                if (indices.Count() > 0 && count > indices[0])
                        //                {
                        //                    // Remove the 'hasContent' class from the class attribute
                        //                    divNode.SetAttributeValue("class", divNode.GetAttributeValue("class", "").Replace("hasContent", "").Trim());
                        //                }
                        //                count++;
                        //            }
                        //        }
                        //        divNodes = targetNode.SelectNodes("//div[contains(@class, 'pagedjs_margin pagedjs_margin-top-right hasContent')]");
                        //        if (divNodes != null)
                        //        {
                        //            int count = 0;
                        //            foreach (HtmlNode divNode in divNodes)
                        //            {
                        //                if (indices.Count() > 0 && count > indices[0])
                        //                {
                        //                    // Remove the 'hasContent' class from the class attribute
                        //                    divNode.SetAttributeValue("class", divNode.GetAttributeValue("class", "").Replace("hasContent", "").Trim());
                        //                }
                        //                count++;
                        //            }
                        //        }
                        //        divNodes = targetNode.SelectNodes("//div[contains(@class, 'pagedjs_margin pagedjs_margin-top-left hasContent')]");
                        //        if (divNodes != null)
                        //        {
                        //            int count = 0;
                        //            foreach (HtmlNode divNode in divNodes)
                        //            {
                        //                if (count == 0)
                        //                {
                        //                    // Remove the 'hasContent' class from the class attribute
                        //                    divNode.SetAttributeValue("class", divNode.GetAttributeValue("class", "").Replace("hasContent", "").Trim());
                        //                }
                        //                if (indices.Count() > 0 && count > indices[0])
                        //                {
                        //                    // Remove the 'hasContent' class from the class attribute
                        //                    divNode.SetAttributeValue("class", divNode.GetAttributeValue("class", "").Replace("hasContent", "").Trim());
                        //                }
                        //                count++;
                        //            }
                        //        }
                        //    }
                        //    doc.DocumentNode.InnerHtml = doc.DocumentNode.OuterHtml.Replace("height: calc(100% - var(--pagedjs-footnotes-height));", "height: calc(110% - var(--pagedjs-footnotes-height));");
                        //    responseContent.payload = doc.DocumentNode.OuterHtml;
                        //    pagedJsResponseModel.Data = responseContent;
                        //    pagedJsResponseModel.StatusCode = HttpStatusCode.OK;
                        //}
                        //pagedJsResponseModel = responseContent;
                    }

                }
                else
                {
                }
            }
            else
            {
                resobj.ErrorMessage = "Html content is null";
                resobj.StatusCode = HttpStatusCode.BadRequest;
            }

            return pagedJsResponseModel;
        }
        public async Task<string> GetAttachmentAsset(string Attachmentfile)
        {
            ResponseModel resobj = new ResponseModel();
            string attachmentassetID = string.Empty;
            var AttachmentPDFdocument = Attachmentfile;
            var full_authorize = await GetUploadAssetUrl();
            if (full_authorize.StatusCode == HttpStatusCode.OK)
            {
                AdobeResponse response = JsonConvert.DeserializeObject<AdobeResponse>((string)full_authorize.Data);
                var uploadUri = response?.uploadUri;
                var assetID = response?.assetID;

                AdobeResponse adobeResponse = new AdobeResponse();
                adobeResponse.uploadUri = uploadUri;
                adobeResponse.assetID = assetID;
                adobeResponse.AttachemntPDFcontent = AttachmentPDFdocument;
                var mergePdf = await UploadAsset(adobeResponse);
                if (mergePdf.StatusCode == HttpStatusCode.OK)
                {
                    var serializedAssetId = JsonConvert.SerializeObject(mergePdf.Data);
                    attachmentassetID = serializedAssetId;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                }
            }
            return attachmentassetID;
        }
        public async Task<ResponseModel> GetUploadAssetUrl()
        {
            ResponseModel resobj = new ResponseModel();

            string defaultString = string.Empty;
            string uploadAsset_AdobeURL = _configuration["AppSettingsAdobe:GetUploadAssetUrl"] ?? defaultString;
            string x_api_key = _configuration["AppSettingsAdobe:adobe_client_id"] ?? defaultString;
            var full_authorize = await GetAccessToken();
            if (full_authorize.StatusCode == HttpStatusCode.OK)
            {
                var serial = JsonConvert.SerializeObject(full_authorize.Data);
                var authorize_token = JsonConvert.DeserializeObject<AccessTokenResponse>(serial);
                var authorize = authorize_token?.access_token;
                var Clients = new RestClient(uploadAsset_AdobeURL);
                var combineRequest = new RestRequest(Method.POST);
                combineRequest.AddHeader("Authorization", "Bearer " + authorize);
                combineRequest.AddHeader("x-api-key", x_api_key);
                var payload = new
                {
                    mediaType = "application/pdf"
                };
                string serial_JSON = JsonConvert.SerializeObject(payload);
                combineRequest.AddParameter("application/json", serial_JSON, ParameterType.RequestBody);
                IRestResponse combineResponse = await Clients.ExecuteAsync(combineRequest);

                if (combineResponse.StatusCode == HttpStatusCode.OK)
                {
                    resobj.Data = combineResponse.Content;
                    resobj.StatusCode = HttpStatusCode.OK;
                }
            }

            return resobj;
        }
        public async Task<ResponseModel> UploadAsset(AdobeResponse adobeResponse)
        {
            ResponseModel resobj = new ResponseModel();

            string apiUrl = adobeResponse.uploadUri;
            string base64File = adobeResponse.AttachemntPDFcontent;
            byte[] fileBytes = Convert.FromBase64String(base64File);
            using (HttpClient client = new HttpClient())
            {
                using (var content = new ByteArrayContent(fileBytes))
                {
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                    var response = await client.PutAsync(apiUrl, content);

                    if (response.IsSuccessStatusCode == true)
                    {
                        resobj.StatusCode = HttpStatusCode.OK;
                        resobj.Data = adobeResponse.assetID;
                    }
                    else
                    {
                        resobj.StatusCode = HttpStatusCode.BadRequest;
                    }
                }
            }

            return resobj;
        }
        public async Task<ResponseModel> GetAccessToken()
        {
            ResponseModel responseModel = new ResponseModel();

            string defaultString = string.Empty;
            string client_id = _configuration["AppSettingsAdobe:adobe_client_id"] ?? defaultString;
            string client_secret = _configuration["AppSettingsAdobe:adobe_client_secret"] ?? defaultString;
            string accessToken_AdobeURL = _configuration["AppSettingsAdobe:GetAccessToken"] ?? defaultString;
            if (!string.IsNullOrEmpty(client_id) || !string.IsNullOrEmpty(client_secret))
            {
                var Fileclient = new RestClient(accessToken_AdobeURL
                + "client_id=" + client_id + "&client_secret=" + client_secret + "&grant_type=client_credentials&scope=openid, AdobeID, DCAPI");

                var filerequest = new RestRequest(Method.POST);
                IRestResponse Fileresponse = await Fileclient.ExecuteAsync(filerequest);

                if (Fileresponse.StatusCode == HttpStatusCode.OK)
                {
                    var res = Fileresponse.Content;
                    var responseContent = JsonConvert.DeserializeObject<AccessTokenResponse>(res);
                    responseModel.Data = responseContent;
                    responseModel.StatusCode = HttpStatusCode.OK;
                    responseModel.Status = true;
                }
                else
                {
                    responseModel.ErrorMessage = Fileresponse.Content;
                    responseModel.StatusCode = HttpStatusCode.InternalServerError;
                }
            }

            return responseModel;
        }
        public async Task<ResponseModel> HtmltoPDFConvert(PdfConversionRequest pdfConversionRequest)
        {
            pdfConversionRequest.pdfConversion.inputUrl = pdfConversionRequest.pdfConversion.inputUrl.Replace(" ", "%20");
            ResponseModel responseModel = new ResponseModel();

            string defaultString = string.Empty;
            string x_api_key = _configuration["AppSettingsAdobe:adobe_client_id"] ?? defaultString;
            string htmltopdf_AdobeURL = _configuration["AppSettingsAdobe:HtmltoPDFConvert"] ?? string.Empty;
            var getAccessToken = await GetAccessToken();
            if (getAccessToken.StatusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(x_api_key))
            {
                var serial = JsonConvert.SerializeObject(getAccessToken.Data);
                var authorize_token = JsonConvert.DeserializeObject<AccessTokenResponse>(serial);
                var authorize = authorize_token.access_token;
                var Client = new RestClient(htmltopdf_AdobeURL);
                var jsonData = JsonConvert.SerializeObject(pdfConversionRequest.pdfConversion);
                var request = new RestRequest(Method.POST);

                Random generator = new Random();
                int xrequestid = Convert.ToInt32(generator.Next(1, 1000000).ToString("D6"));

                request.AddHeader("Authorization", "Bearer " + authorize);
                request.AddHeader("x-api-key", x_api_key);
                request.AddHeader("x-request-id", xrequestid.ToString());
                request.AddParameter("application/json", jsonData, ParameterType.RequestBody);

                IRestResponse Fileresponse = await Client.ExecuteAsync(request);
                if (Fileresponse.StatusCode == HttpStatusCode.Created)
                {
                    var dataRes = await GetPdfStatus(xrequestid, authorize);
                    responseModel = dataRes;
                }
                else if (Convert.ToInt32(Fileresponse.StatusCode) == 429)
                {
                    responseModel.ErrorMessage = "Maximum API limit is reached, hence letter cant be submitted for signing, please contact the IT Helpdesk team to proceed further";
                    responseModel.Status = false;
                    responseModel.StatusCode = Fileresponse.StatusCode;
                }
                else
                {
                    responseModel.Status = false;
                    responseModel.StatusCode = Fileresponse.StatusCode;
                }
            }
            else
            {
                return getAccessToken;
            }

            return responseModel;
        }

        public async Task<ResponseModel> GetPdfStatus(int xrequestid, string authorize)
        {
            ResponseModel responseModel = new ResponseModel();

            string defaultString = string.Empty;
            string x_api_key = _configuration["AppSettingsAdobe:adobe_client_id"] ?? defaultString;
            string pdfStatus_AdobeURL = _configuration["AppSettingsAdobe:GetPdfStatus"] ?? string.Empty;
            if (xrequestid > 0 && !string.IsNullOrEmpty(authorize))
            {
                string uri = pdfStatus_AdobeURL;
                var Clients = new RestClient(uri + xrequestid + "/status");
                var statusRequest = new RestRequest(Method.GET);
                statusRequest.AddHeader("Authorization", "Bearer " + authorize);
                statusRequest.AddHeader("x-api-key", x_api_key);
                statusRequest.AddHeader("Content-Type", "application/json");
                IRestResponse statusResponse = await Clients.ExecuteAsync(statusRequest);
                if (statusResponse.StatusCode == HttpStatusCode.OK)
                {
                    var res = statusResponse.Content;
                    var responseContent = JsonConvert.DeserializeObject<AdobePDFStatusResponse>(res);
                    if (responseContent.status.ToLower() == "in progress")
                    {
                        responseModel = await GetPdfStatus(xrequestid, authorize);
                    }
                    else
                    {
                        responseModel.Data = responseContent.asset.downloadUri;
                        responseModel.PdfUrl = responseContent.asset.assetID;
                        responseModel.Status = true;
                        responseModel.StatusCode = HttpStatusCode.OK;
                        return responseModel;
                    }
                }
                else
                {
                    responseModel.ErrorMessage = statusResponse.Content;
                    responseModel.StatusCode = statusResponse.StatusCode;
                    responseModel.Status = false;
                }
            }
            else
            {
                responseModel.ErrorMessage = "AccessToken is null";
                responseModel.StatusCode = HttpStatusCode.BadRequest;
                responseModel.Status = false;
            }

            return responseModel;
        }
        public async Task<List<string>> UploadAttachment(AttachmentPdfFile pdfDetails)
        {
            ResponseModel responseModel = new ResponseModel();
            List<string> attachmentAssetID = new List<string>();

            if (pdfDetails != null && pdfDetails.UploadAttachmentJSON.Count > 0)
            {
                foreach (var item in pdfDetails.UploadAttachmentJSON)
                {
                    string attachmentBase64 = item.Base64String;
                    var assetRes = await GetAttachmentAsset(attachmentBase64);
                    attachmentAssetID.Add(assetRes);
                }
            }

            return attachmentAssetID;
        }
        public async Task<ResponseModel> UploadHTMLLetter(LetterHTMLRequestModel letterHtmlContent)
        {
            ResponseModel resobj = new ResponseModel();
            PagedJsRequestModel pagedJsRequest = new PagedJsRequestModel();
            byte[] bytes = Convert.FromBase64String(letterHtmlContent.LetterHtmlContent);
            string utf8String = Encoding.UTF8.GetString(bytes);
            pagedJsRequest.payload = utf8String;
            PagedJsResponseModel responseContent = new PagedJsResponseModel();

            responseContent = await GeneratePagedJsHtml(pagedJsRequest);
            var serial = JsonConvert.SerializeObject(responseContent.Data);
            var pagedJsHtml = JsonConvert.DeserializeObject<PagedJsResponseModel>(serial);
            if (pagedJsHtml != null || !string.IsNullOrEmpty(pagedJsHtml?.payload))
            {
                //Starting HTML to URL Process
                var decodedStr = Base64UrlEncoder.Encode(pagedJsHtml.payload);
                HtmlFileResponseModel htmlFileResponse = new HtmlFileResponseModel();

                if (!string.IsNullOrEmpty(decodedStr) && !string.IsNullOrEmpty(letterHtmlContent.TemplateName))
                {
                    UploadHtmlFile uploadHtmlFile = new UploadHtmlFile();
                    uploadHtmlFile.FileName = letterHtmlContent.TemplateName;
                    uploadHtmlFile.Base64Html = decodedStr;
                    var htmlResp = await UploadHtmlFileFromBlob(uploadHtmlFile);

                    htmlFileResponse.StatusCode = HttpStatusCode.OK;
                    htmlFileResponse.Message = "Success";
                    resobj.Data = htmlFileResponse;
                    resobj.StatusCode = HttpStatusCode.OK;
                    resobj.Status = true;

                    //Starting HTML to PDF Process
                    if (!string.IsNullOrEmpty(htmlResp?.Data?.ToString()))
                    {
                        PdfConversionRequest pdfConversionRequest = new PdfConversionRequest();
                        htmlResp.Data = htmlResp?.Data?.ToString()?.Replace(" ", "%20");
                        var pdfConversion = new PdfConversion();
                        pdfConversion.inputUrl = htmlResp?.Data?.ToString() ?? string.Empty;

                        pdfConversion.includeHeaderFooter = true;
                        pdfConversion.pageLayout = new PageLayout { pageWidth = "11", pageHeight = "8.5" };
                        pdfConversion.json = "{}";

                        pdfConversionRequest.pdfConversion = pdfConversion;
                        var htmlResponse = await HtmltoPDFConvert(pdfConversionRequest);
                        if (htmlResponse.Data != null && htmlResponse.StatusCode == HttpStatusCode.OK)
                        {
                            resobj.Data = htmlResponse.PdfUrl;
                            resobj.Status = true;
                            resobj.StatusCode = HttpStatusCode.OK;
                        }
                        else
                        {
                            resobj.Data = htmlResponse;
                            resobj.Status = false;
                            resobj.ErrorMessage = "ErrorInfo: HTML to PDF conversion response is NULL";
                            resobj.StatusCode = HttpStatusCode.InternalServerError;
                        }
                    }
                    else
                    {
                        resobj.ErrorMessage = "HtmlURL path is null";
                        resobj.StatusCode = HttpStatusCode.BadRequest;
                    }
                }
                else
                {
                    htmlFileResponse.Message = "data is null";
                    resobj.Data = htmlFileResponse;
                }
            }
            else
            {
                resobj.ErrorMessage = "Empty PagedJS Response";
                resobj.StatusCode = HttpStatusCode.BadRequest;
            }
            return resobj;
        }
        public async Task<ResponseModel> CombinePDFReq(CombinePDFReq combinePDF)
        {
            ResponseModel response = new ResponseModel();
            CombinedPDFRequestModel combinePDFRequest = new CombinedPDFRequestModel();
            combinePDFRequest.assets = new List<Assete>();
            Assete asset = new Assete();
            if(!string.IsNullOrEmpty(combinePDF.LetterHTMLAssetID1))
            {
                asset.assetID = combinePDF.LetterHTMLAssetID1;
                combinePDFRequest.assets.Add(asset);
            }
            if (!string.IsNullOrEmpty(combinePDF.Attachment1))
            {
                asset = new Assete();
                asset.assetID = combinePDF.Attachment1;
                combinePDFRequest.assets.Add(asset);
            }
            if (!string.IsNullOrEmpty(combinePDF.Attachment2))
            {
                asset = new Assete();
                asset.assetID = combinePDF.Attachment2;
                combinePDFRequest.assets.Add(asset);
            }
            if (!string.IsNullOrEmpty(combinePDF.Attachment3))
            {
                asset = new Assete();
                asset.assetID = combinePDF.Attachment3;
                combinePDFRequest.assets.Add(asset);
            }
            if (!string.IsNullOrEmpty(combinePDF.Attachment4))
            {
                asset = new Assete();
                asset.assetID = combinePDF.Attachment4;
                combinePDFRequest.assets.Add(asset);
            }
            if (!string.IsNullOrEmpty(combinePDF.Attachment4))
            {
                asset = new Assete();
                asset.assetID = combinePDF.Attachment5;
                combinePDFRequest.assets.Add(asset);
            }

            ResponseModel resCombinepdf = new ResponseModel();
            if (combinePDFRequest.assets.Count > 1)
            {
                resCombinepdf = await CombinePDF(combinePDFRequest);
            }
            else
            {
                resCombinepdf.Status = false;
                resCombinepdf.StatusCode = HttpStatusCode.NoContent;
            }

            if (resCombinepdf != null && resCombinepdf.Data != null && resCombinepdf.StatusCode == HttpStatusCode.OK)
            {
                string requiredPath = string.Empty;
                HtmlFileResponseModel htmlFileResponse = new HtmlFileResponseModel();

                UploadPdfFile uploadPdf = new UploadPdfFile();
                uploadPdf.PdfUrl = resCombinepdf.Data.ToString();
                uploadPdf.FileName = combinePDF.TemplateName;
                var respPDF = await UploadPdfFileFromBlob(uploadPdf);
                response.Data = respPDF.Data;
                response.Status = true;
                response.StatusCode = HttpStatusCode.OK;
            }

            return response;
        }
        public async Task<ResponseModel> CombinePDF(CombinedPDFRequestModel combinedPDFRequestModel)
        {
            ResponseModel responseModel = new ResponseModel();

            string defaultString = string.Empty;
            string x_api_key = _configuration["AppSettingsAdobe:adobe_client_id"] ?? defaultString;
            string combinepdf_AdobeURL = _configuration["AppSettingsAdobe:CombinePDF"] ?? defaultString;
            var full_authorize = await GetAccessToken();
            if (full_authorize.StatusCode == HttpStatusCode.OK)
            {
                var serial = JsonConvert.SerializeObject(full_authorize.Data);
                var authorize_token = JsonConvert.DeserializeObject<AccessTokenResponse>(serial);
                var authorize = authorize_token?.access_token;
                var serial_JSON = JsonConvert.SerializeObject(combinedPDFRequestModel);
                var Clients = new RestClient(combinepdf_AdobeURL);
                var combineRequest = new RestRequest(Method.POST);
                combineRequest.AddHeader("Authorization", "Bearer " + authorize);
                combineRequest.AddHeader("x-api-key", x_api_key);
                combineRequest.AddHeader("Content-Type", "application/json");
                combineRequest.AddParameter("application/json", serial_JSON, ParameterType.RequestBody);
                IRestResponse combineResponse = await Clients.ExecuteAsync(combineRequest);
                if (combineResponse.StatusCode == HttpStatusCode.Created)
                {
                    for (int i = 0; i < combineResponse.Headers.Count; i++)
                    {
                        if (combineResponse.Headers[i].Name == "Location")
                        {
                            var location = combineResponse.Headers[i].Name == "Location" ? combineResponse.Headers[i].Value?.ToString() : string.Empty;
                            var resData = await GetCombinedPDFUrl(location, authorize);
                            return resData;
                        }
                    }
                }
                else
                {
                    responseModel.ErrorMessage = combineResponse.Content;
                    responseModel.StatusCode = full_authorize.StatusCode;
                }
            }
            else
            {
                return full_authorize;
            }

            return responseModel;
        }
        private async Task<ResponseModel> GetCombinedPDFUrl(string combinePDFLocation, string authorize)
        {
            ResponseModel responseModel = new ResponseModel();

            if (!string.IsNullOrEmpty(combinePDFLocation) && !string.IsNullOrEmpty(authorize))
            {
                string defaultString = string.Empty;
                string x_api_key = _configuration["AppSettingsAdobe:adobe_client_id"] ?? defaultString;
                var Clients = new RestClient(combinePDFLocation);
                var statusRequest = new RestRequest(Method.GET);
                statusRequest.AddHeader("Authorization", "Bearer " + authorize);
                statusRequest.AddHeader("x-api-key", x_api_key);
                statusRequest.AddHeader("Content-Type", "application/json");
                IRestResponse statusResponse = await Clients.ExecuteAsync(statusRequest);
                if (statusResponse.StatusCode == HttpStatusCode.OK)
                {
                    var res = statusResponse.Content;
                    var deserial_Response = JsonConvert.DeserializeObject<CombinedPDFResponseModel>(res);
                    if (deserial_Response?.status.ToLower() == "in progress")
                    {
                        var data = await GetCombinedPDFUrl(combinePDFLocation, authorize);
                        responseModel = data;
                    }
                    else
                    {
                        responseModel.Data = deserial_Response?.asset.downloadUri;
                        responseModel.PdfUrl = deserial_Response?.asset.assetID ?? string.Empty;
                        responseModel.StatusCode = HttpStatusCode.OK;
                    }
                }
                else
                {
                    responseModel.ErrorMessage = statusResponse.Content;
                    responseModel.StatusCode = statusResponse.StatusCode;
                }
            }
            else
            {
                string? errorMessage = _resourceManager.GetString("CombinePDFErrorMessage");
                responseModel.ErrorMessage = errorMessage;
                responseModel.StatusCode = HttpStatusCode.InternalServerError;
            }

            return responseModel;
        }

        public async Task<ResponseModel> UploadHtmlFileFromBlob(UploadHtmlFile uploadHtmlFile)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                if (!string.IsNullOrEmpty(uploadHtmlFile.Base64Html) && !string.IsNullOrEmpty(uploadHtmlFile.FileName))
                {
                    string accountName = _configuration["BloBStorageConfig:AzureAccountName"] ?? string.Empty;
                    string connectionString = _configuration["ConnectionStrings:AzureBlobStorage"] ?? string.Empty;
                    string accountKey = _configuration["BloBStorageConfig:AzureAccountKey"] ?? string.Empty;
                    string containerName = _configuration["BloBStorageConfig:AzureContainer_Html"] ?? string.Empty;
                    string fileName = uploadHtmlFile.FileName + ".html";

                    var encodedStr = Base64UrlEncoder.Decode(uploadHtmlFile.Base64Html);

                    var storageCredentials = new StorageCredentials(accountName, accountKey);
                    var storageAccount = new CloudStorageAccount(storageCredentials, true);
                    var connString = storageAccount.ToString(true);
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                    var container = blobClient.GetContainerReference(containerName).GetBlockBlobReference(fileName);
                    CloudBlockBlob blob = container;
                    //blob.Metadata["SetAccessPolicy"] = Azure.Storage.Blobs.Models.PublicAccessType.Blob.ToString();
                    blob.Properties.ContentType = "text/html";
                    string timezone = "UTC+03:00";
                    blob.Metadata["Timezone"] = timezone;

                    await blob.DeleteIfExistsAsync();

                    await blob.UploadTextAsync(encodedStr);
                    BlobSasTokenRequest request = new BlobSasTokenRequest();
                    request.AccountKey = accountKey;
                    request.AccountName = accountName;
                    request.ConnectionString = connectionString;
                    request.ContainerName = containerName;
                    request.FileName = fileName;

                    responseModel = await GetFileFromBlob(request);
                }
                else
                {
                    string? errorMessage = _resourceManager.GetString("BlobfromHtmlErrorMessage");
                    responseModel.ErrorMessage = errorMessage;
                    responseModel.StatusCode = HttpStatusCode.InternalServerError;
                }
            }
            catch (Exception ex)
            {
                responseModel.StatusCode = HttpStatusCode.InternalServerError;
                responseModel.ErrorMessage = "";
            }
            return responseModel;
        }
        public async Task<ResponseModel> UploadPdfFileFromBlob(UploadPdfFile uploadPdfFile)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                if (!string.IsNullOrEmpty(uploadPdfFile.PdfUrl) && !string.IsNullOrEmpty(uploadPdfFile.FileName))
                {
                    string accountName = _configuration["BloBStorageConfig:AzureAccountName"] ?? string.Empty;
                    string connectionString = _configuration["ConnectionStrings:AzureBlobStorage"] ?? string.Empty;
                    string accountKey = _configuration["BloBStorageConfig:AzureAccountKey"] ?? string.Empty;
                    string containerName = _configuration["BloBStorageConfig:AzureContainer_Html"] ?? string.Empty;
                    string fileName = uploadPdfFile.FileName + ".pdf";
                    byte[] dataByte;
                    using (WebClient webClient = new WebClient())
                    {
                        dataByte = webClient.DownloadData(uploadPdfFile.PdfUrl);
                    }
                    var storageCredentials = new StorageCredentials(accountName, accountKey);
                    var storageAccount = new CloudStorageAccount(storageCredentials, true);
                    var connString = storageAccount.ToString(true);
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                    var container = blobClient.GetContainerReference(containerName).GetBlockBlobReference(fileName);
                    container.Properties.ContentType = "application/pdf";

                    CloudBlockBlob blob = container;
                    blob.Properties.ContentType = "application/pdf";
                    string timezone = "UTC+03:00";
                    blob.Metadata["Timezone"] = timezone;

                    await blob.DeleteIfExistsAsync();

                    await blob.UploadFromByteArrayAsync(dataByte, 0, dataByte.Length);
                    BlobSasTokenRequest request = new BlobSasTokenRequest();
                    request.AccountKey = accountKey;
                    request.AccountName = accountName;
                    request.ConnectionString = connectionString;
                    request.ContainerName = containerName;
                    request.FileName = fileName;

                    responseModel = await GetFileFromBlob(request);
                }
                else
                {
                    responseModel.ErrorMessage = "Pdf url is null or engagement letter name is null";
                    responseModel.StatusCode = HttpStatusCode.InternalServerError;
                }
            }
            catch (Exception ex)
            {
                responseModel.StatusCode = HttpStatusCode.InternalServerError;
                responseModel.ErrorMessage = "";
            }
            return responseModel;
        }
        public async Task<ResponseModel> GetFileFromBlob(BlobSasTokenRequest blobSasTokenRequest)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                CloudStorageAccount backupStorageAccount = CloudStorageAccount.Parse(blobSasTokenRequest.ConnectionString);
                var backupBlobClient = backupStorageAccount.CreateCloudBlobClient();
                var backupContainer = backupBlobClient.GetContainerReference(blobSasTokenRequest.ContainerName);

                CloudBlockBlob blobFile = backupContainer.GetBlockBlobReference(blobSasTokenRequest.FileName);
                BlobSasBuilder blobSasBuilder = new BlobSasBuilder()
                {
                    BlobContainerName = blobSasTokenRequest.ContainerName,
                    BlobName = blobSasTokenRequest.FileName,
                    ExpiresOn = DateTime.UtcNow.AddYears(1),//Let SAS token expire after 5 minutes.
                };
                blobSasBuilder.SetPermissions(BlobSasPermissions.Read);//User will only be able to read the blob and it's properties
                var sasToken = blobSasBuilder.ToSasQueryParameters(new StorageSharedKeyCredential(blobSasTokenRequest.AccountName, blobSasTokenRequest.AccountKey)).ToString();
                var sasUrl = blobFile.Uri.AbsoluteUri + "?" + sasToken;
                responseModel.Data = sasUrl;
                responseModel.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                responseModel.StatusCode = HttpStatusCode.InternalServerError;
                responseModel.ErrorMessage = "";
            }

            return responseModel;
        }
    }
}
