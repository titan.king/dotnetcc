﻿using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Data.Interfaces;
using Dashboard.Models.Login;
using Dashboard.Models.ResponseModel;

namespace Dashboard.Business.Services
{
    public class LoginService : ILoginService
    {
        private readonly ILoginRepository _loginRespository;
        private readonly IMapper _mapper;

        public LoginService(ILoginRepository loginRepository, IMapper mapper)
        {
            _loginRespository = loginRepository;
            _mapper = mapper;
        }

        public async Task<LoginViewModel> GetRoles(LoginViewModel permissions)
        {
            return await _loginRespository.GetRoles(permissions);
        }
        public async Task<LoginViewModel> GetRolesandScreenList(LoginListModel loginViewModel)
        {
            LoginViewModel permissionModel = new LoginViewModel();
            var resData = await _loginRespository.GetRolesandScreenList(loginViewModel);
            permissionModel = _mapper.Map<LoginViewModel>(resData);
            return permissionModel;
        }
    }
}
