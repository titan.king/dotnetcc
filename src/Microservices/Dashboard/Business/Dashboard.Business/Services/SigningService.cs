﻿using AutoMapper;
using Azure.Storage;
using Azure.Storage.Sas;
using Dashboard.Business.Interfaces;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.ResponseModel;
using Dashboard.Models.Signing;
using Engage3.Common.Business;
using Engage3.Common.Business.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using RestSharp;
using System.Net;
using System.Resources;

namespace Dashboard.Business.Services
{
    public class SigningService : ISigningService
    {
        private readonly ISigningRepository _signingRepository;
        private readonly IMapper _mapper;
        private readonly ResourceManager _resourceManager;
        private readonly IConfiguration _configuration;
        private readonly EmailHelper _emailHelper;
        private readonly IGetCurrentDatetime _getCurrentDatetime;

        public SigningService(ISigningRepository signingRepository, ResourceManager resourceManager, IMapper mapper, IConfiguration configuration, EmailHelper emailHelper, IGetCurrentDatetime getCurrentDateTime)
        {
            _configuration = configuration;
            _mapper = mapper;
            _signingRepository = signingRepository;
            _resourceManager = resourceManager;
            _emailHelper = emailHelper;
            _getCurrentDatetime = getCurrentDateTime;
        }
        ResponseModel resobj = new ResponseModel();
        public async Task<ResponseModel> CancelSigning(CancleSigningResModel signingViewModel)
        {
            string EditedBy = string.Empty;
            string? agreementID = string.Empty;
            IEnumerable<SigningInfo> signingInfo = await _signingRepository.GetAgreementId();
            EngagementLetter? engagementLetters = await _signingRepository.GetEngagementLetterStatus(signingViewModel.EngagementLetterId);
            int? historyLogVersion = await _signingRepository.GetHistoryLogVersion(signingViewModel.EngagementLetterId);
            var delegatedName = signingInfo
                               .FirstOrDefault(x => x.EngagementLetterId == signingViewModel.EngagementLetterId && !x.IsDeleted)?.DelegatedName;

            var signingPartnerName = signingInfo
                                .FirstOrDefault(x => x.EngagementLetterId == signingViewModel.EngagementLetterId && !x.IsDeleted)?.SigningPartnerName;
            agreementID = signingInfo.OrderByDescending(x => x.SigningInfoId).FirstOrDefault
                                (x => x.EngagementLetterId == signingViewModel.EngagementLetterId && !x.IsDeleted)?.AgreementId;        
            CheckSigningStatusResModel signingStatus = new CheckSigningStatusResModel();
            signingStatus.EngagementLetterId = signingViewModel.EngagementLetterId;
            signingStatus.AgreementId = agreementID;
            signingStatus.DelegatedName = delegatedName;
            signingStatus.SigningPartnerName = signingPartnerName;
            signingStatus.Version = historyLogVersion;
            signingStatus.ModifiedBy = signingViewModel.ModifiedBy;

            var adobeStstus = await RefreshSignedStatusToComplete(signingStatus);

            if (engagementLetters?.IsEsigning == true)
            {
                if (adobeStstus.Data?.ToString() != "SIGNED" && adobeStstus.Data?.ToString() != "Completed")
                {
                    AdobeCancleSignRequest cancleSignRequest = new AdobeCancleSignRequest();
                    cancleSignRequest.agreementID = agreementID;
                    cancleSignRequest.state = "CANCELLED";
                    if (cancleSignRequest != null && cancleSignRequest.state == "CANCELLED")
                    {
                        string? authorizationKey = _configuration["AppSettingsAdobe:adobe_integrationkey"];

                        var Fileclient = new RestClient(_configuration["AppSettingsAdobe:RefreshStatus"] + cancleSignRequest.agreementID + "/state");
                        var serial_JSON = JsonConvert.SerializeObject(cancleSignRequest);
                        var filerequest = new RestRequest(Method.PUT);
                        filerequest.AddHeader("Authorization", "Bearer " + authorizationKey);
                        filerequest.AddParameter("application/json", serial_JSON, ParameterType.RequestBody);
                        IRestResponse Fileresponse = await Fileclient.ExecuteAsync(filerequest);
                        if (Fileresponse.StatusCode == HttpStatusCode.NoContent)
                        {
                            var statusId = EnumHelper.DocumentStatus.WithAdminForReview;
                            engagementLetters.DocumentStatusId =(int)statusId;
                            engagementLetters.ModifiedBy = signingViewModel.ModifiedBy;
                            engagementLetters.AdminName = signingViewModel.ModifiedBy;
                            var updateStatus = await _signingRepository.UpdateSignedStatusEngagementLetter(engagementLetters);

                            string agreementStatus = "Cancelled the Signing Request";

                            if (historyLogVersion == null)
                            {
                                historyLogVersion = 0;
                            }
                            historyLogVersion += 1;

                            if (delegatedName != "")
                            {
                                EditedBy = delegatedName ?? string.Empty;
                            }
                            if (signingPartnerName != "")
                            {
                                EditedBy = signingPartnerName ?? string.Empty;
                            }

                            HistoryLog insertCancelStatus = new HistoryLog();
                            insertCancelStatus.EngagementLetterId = signingViewModel.EngagementLetterId;
                            insertCancelStatus.Version = historyLogVersion;
                            insertCancelStatus.EditedBy = signingViewModel.ModifiedBy;
                            insertCancelStatus.BatchId = engagementLetters.BatchId;
                            insertCancelStatus.EngagementLetterName = engagementLetters.EngagementLetterName;
                            insertCancelStatus.Status = agreementStatus;
                            insertCancelStatus.ClientEmailId = null;
                            insertCancelStatus.ReasonforDecline = null;
                            insertCancelStatus.DeclineTimestamp = null;
                            insertCancelStatus.Downloaded = null;
                            insertCancelStatus.Delegated = false;
                            insertCancelStatus.PDFUrl = null;
                            await _signingRepository.InsertHistoryLog(insertCancelStatus);

                            EngagementLetter? engagementStatus = await _signingRepository.GetEngagementLetterStatus(signingViewModel.EngagementLetterId);

                            if (engagementStatus?.DocumentStatusId == 4)
                            {
                                int? UpdatedLogVersion = await _signingRepository.GetHistoryLogVersion(signingViewModel.EngagementLetterId);
                                if (UpdatedLogVersion == null)
                                {
                                    UpdatedLogVersion = 0;
                                }
                                UpdatedLogVersion += 1;

                                var Status = "With Admin for Review";
                                HistoryLog insertAdminReview = new HistoryLog();
                                insertAdminReview.EngagementLetterId = signingViewModel.EngagementLetterId;
                                insertAdminReview.Version = UpdatedLogVersion;
                                insertAdminReview.EditedBy = signingViewModel.ModifiedBy;

                                var estTime = await _getCurrentDatetime.GetCurrentEstDatetime();

                                insertAdminReview.LastModified = estTime;
                                insertAdminReview.BatchId = engagementLetters.BatchId;
                                insertAdminReview.EngagementLetterName = engagementLetters.EngagementLetterName;
                                insertAdminReview.Status = Status;
                                insertAdminReview.ClientEmailId = null;
                                insertAdminReview.ReasonforDecline = null;
                                insertAdminReview.DeclineTimestamp = null;
                                insertAdminReview.Downloaded = null;
                                insertAdminReview.Delegated = false;
                                insertAdminReview.PDFUrl = null;
                                await _signingRepository.InsertHistoryLog(insertAdminReview);
                            }
                            resobj.Status = true;
                            resobj.StatusCode = HttpStatusCode.OK;
                        }
                        else
                        {
                            resobj.ErrorMessage = Fileresponse.Content.ToString();
                            resobj.StatusCode = HttpStatusCode.InternalServerError;
                        }
                    }
                }
                else
                {
                    resobj.ErrorMessage = "Letter has been already signed";
                    resobj.StatusCode = HttpStatusCode.NotAcceptable;
                    resobj.Status = false;
                    resobj.PdfUrl = adobeStstus.PdfUrl;
                }
            }
            else if (engagementLetters?.IsEsigning == false)
            {
                var statusId = EnumHelper.DocumentStatus.WithAdminForReview; 
                engagementLetters.DocumentStatusId = (int)statusId;
                engagementLetters.IsEnqueue = false;
                engagementLetters.IsProcess = false;
                engagementLetters.ModifiedBy = signingViewModel.ModifiedBy;
                engagementLetters.AdminName = signingViewModel.ModifiedBy;
                var updateStatus = await _signingRepository.UpdateSignedStatusEngagementLetter(engagementLetters);

                string agreementStatus = "Cancelled the Signing Request";

                if (historyLogVersion == null)
                {
                    historyLogVersion = 0;
                }
                historyLogVersion += 1;

                if (delegatedName != "")
                {
                    EditedBy = delegatedName ?? string.Empty;
                }
                if (signingPartnerName != "")
                {
                    EditedBy = signingPartnerName ?? string.Empty;
                }

                HistoryLog insertCancelStatus = new HistoryLog();
                insertCancelStatus.EngagementLetterId = signingViewModel.EngagementLetterId;
                insertCancelStatus.Version = historyLogVersion;
                insertCancelStatus.EditedBy = signingViewModel.ModifiedBy;
                insertCancelStatus.BatchId = engagementLetters.BatchId;
                insertCancelStatus.EngagementLetterName = engagementLetters.EngagementLetterName;
                insertCancelStatus.Status = agreementStatus;
                insertCancelStatus.ClientEmailId = null;
                insertCancelStatus.ReasonforDecline = null;
                insertCancelStatus.DeclineTimestamp = null;
                insertCancelStatus.Downloaded = null;
                insertCancelStatus.Delegated = false;
                insertCancelStatus.PDFUrl = null;
                await _signingRepository.InsertHistoryLog(insertCancelStatus);

                EngagementLetter? engagementStatus = await _signingRepository.GetEngagementLetterStatus(signingViewModel.EngagementLetterId);

                if (engagementStatus?.DocumentStatusId == 4)
                {
                    int? UpdatedLogVersion = await _signingRepository.GetHistoryLogVersion(signingViewModel.EngagementLetterId);
                    if (UpdatedLogVersion == null)
                    {
                        UpdatedLogVersion = 0;
                    }
                    UpdatedLogVersion += 1;

                    var Status = "With Admin for Review";
                    HistoryLog insertAdminReview = new HistoryLog();
                    insertAdminReview.EngagementLetterId = signingViewModel.EngagementLetterId;
                    insertAdminReview.Version = UpdatedLogVersion;
                    insertAdminReview.EditedBy = signingViewModel.ModifiedBy;
                    insertAdminReview.BatchId = engagementLetters.BatchId;
                    insertAdminReview.EngagementLetterName = engagementLetters.EngagementLetterName;
                    insertAdminReview.Status = Status;
                    insertAdminReview.ClientEmailId = null;
                    insertAdminReview.ReasonforDecline = null;
                    insertAdminReview.DeclineTimestamp = null;
                    insertAdminReview.Downloaded = null;
                    insertAdminReview.Delegated = false;
                    insertAdminReview.PDFUrl = null;
                    await _signingRepository.InsertHistoryLog(insertAdminReview);

                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                }
            }
            return resobj;
        }


        public async Task<ResponseModel> RefreshSignedStatusToComplete(CheckSigningStatusResModel changeStatus)
        {
            string? EditedBy = string.Empty;
            ResponseModel resobj = new ResponseModel();
            EngagementLetter? engagementLetters = await _signingRepository.GetEngagementLetterStatus(changeStatus.EngagementLetterId);
            IEnumerable<SigningInfo> signingInfo = await _signingRepository.GetAgreementId();
            if (engagementLetters?.DocumentStatusId == 2)
            {
                string? agreementID = changeStatus.AgreementId;

                if (!string.IsNullOrEmpty(agreementID))
                {
                    string? authorizationKey = _configuration["AppSettingsAdobe:adobe_integrationkey"];

                    var Fileclient = new RestClient(_configuration["AppSettingsAdobe:RefreshStatus"] + agreementID);

                    var filerequest = new RestRequest(Method.GET);
                    filerequest.AddHeader("Authorization", "Bearer " + authorizationKey);
                    IRestResponse Fileresponse = await Fileclient.ExecuteAsync(filerequest);
                    if (Fileresponse.StatusCode == HttpStatusCode.OK)
                    {
                        var res = Fileresponse.Content;
                        var responseContent = JsonConvert.DeserializeObject<AdobeSignStatusResponseVM>(res);
                        string? agreementStatus = responseContent?.status;
                        var adobeSignerCount = responseContent?.participantSetsInfo?.Count();
                        if (agreementStatus == "SIGNED")
                        {
                            var statusId = 1;
                            engagementLetters.DocumentStatusId = statusId;
                            engagementLetters.ModifiedBy = changeStatus.ModifiedBy;
                            var updateStatus = await _signingRepository.UpdateSignedStatusEngagementLetter(engagementLetters);

                            var getdocumentURL = await GetSignedDocument(agreementID);
                            if (getdocumentURL.StatusCode == HttpStatusCode.OK)
                            {
                                var docFileURL = JsonConvert.SerializeObject(getdocumentURL);
                                var completeDocFileURL = JsonConvert.DeserializeObject<ResponseModel>(docFileURL);

                                //HtmlFileResponseModel htmlFileResponse = new HtmlFileResponseModel();

                                UploadPdfFile uploadPdf = new UploadPdfFile();
                                uploadPdf.PdfUrl = completeDocFileURL?.Data?.ToString() ?? string.Empty;
                                uploadPdf.FileName = changeStatus.EngagementLetterId + "-" + engagementLetters.EngagementLetterName ?? string.Empty;

                                var respPDF = await UploadPdfFileFromBlob(uploadPdf);
                                resobj.PdfUrl = respPDF.PdfUrl;

                                if (respPDF.PdfUrl != null)
                                {
                                    int? historyLogVersion = changeStatus.Version;
                                    if (historyLogVersion == null)
                                    {
                                        historyLogVersion = 0;
                                    }
                                    historyLogVersion += 1;

                                    if (agreementStatus == "SIGNED")
                                    {
                                        agreementStatus = "Completed";
                                    }                                   

                                    var estTime = await _getCurrentDatetime.GetCurrentEstDatetime();
                                    HistoryLog insertHistoryLog = new HistoryLog();
                                    insertHistoryLog.EngagementLetterId = changeStatus.EngagementLetterId;
                                    insertHistoryLog.Version = historyLogVersion;
                                    insertHistoryLog.EditedBy = changeStatus.ModifiedBy;
                                    insertHistoryLog.LastModified = estTime;
                                    insertHistoryLog.BatchId = engagementLetters.BatchId;
                                    insertHistoryLog.EngagementLetterName = engagementLetters.EngagementLetterName;
                                    insertHistoryLog.Status = agreementStatus;
                                    insertHistoryLog.ClientEmailId = null;
                                    insertHistoryLog.ReasonforDecline = null;
                                    insertHistoryLog.DeclineTimestamp = null;
                                    insertHistoryLog.Downloaded = null;
                                    insertHistoryLog.Delegated = false;
                                    insertHistoryLog.PDFUrl = respPDF.PdfUrl;

                                    await _signingRepository.InsertHistoryLog(insertHistoryLog);
                                }                               
                            }
                        }
                        resobj.Data = agreementStatus ?? string.Empty;
                        resobj.Status = true;
                    }
                }
            }
            return resobj;
        }

        public async Task<ResponseModel> AutoRefreshStatus()
        {
            string EditedBy = string.Empty;

            ResponseModel resobj = new ResponseModel();
            IEnumerable<int> engagementLetters = await _signingRepository.GetAllEngagementLetter();
            IEnumerable<SigningInfo> signingInfo = await _signingRepository.GetAgreementId();
            foreach (var item in engagementLetters)
            {
                int? historyLogVersion = await _signingRepository.GetHistoryLogVersion(item);
                var engagementLetterList = await _signingRepository.GetEngagmentLettersByID(item);

                string? agreementID =signingInfo.OrderByDescending(x => x.SigningInfoId).FirstOrDefault
                                 (x => x.EngagementLetterId == item && !x.IsDeleted)?.AgreementId;             
                string? get7216ExpiryDate = signingInfo.FirstOrDefault
                                     (x => x.EngagementLetterId == item && !x.IsDeleted)?.ExpiryDate7216;
                
                if (!string.IsNullOrEmpty(agreementID))
                {
                    string? authorizationKey = _configuration["AppSettingsAdobe:adobe_integrationkey"];

                    var Fileclient = new RestClient(_configuration["AppSettingsAdobe:RefreshStatus"] + agreementID);

                    var filerequest = new RestRequest(Method.GET);
                    filerequest.AddHeader("Authorization", "Bearer " + authorizationKey);
                    IRestResponse Fileresponse = await Fileclient.ExecuteAsync(filerequest);
                    if (Fileresponse.StatusCode == HttpStatusCode.OK)
                    {
                        var res = Fileresponse.Content;
                        var responseContent = JsonConvert.DeserializeObject<AdobeSignStatusResponseVM>(res);
                        string? agreementStatus = responseContent?.status;
                        var adobeSignerCount = responseContent?.participantSetsInfo?.Count();
                        if (agreementStatus == "SIGNED" & engagementLetterList.DocumentStatusId != 1 )
                        {
                            EngagementLetter? engagementLetter = await _signingRepository.GetEngagementLetterStatus(item);
                            var statusId = 1;
                            engagementLetter.DocumentStatusId = statusId;
                            var updateStatus = await _signingRepository.UpdateSignedStatusEngagementLetter(engagementLetter);


                            var getdocumentURL = await GetSignedDocument(agreementID);
                            if (getdocumentURL.StatusCode == HttpStatusCode.OK)
                            {
                                var docFileURL = JsonConvert.SerializeObject(getdocumentURL);
                                var completeDocFileURL = JsonConvert.DeserializeObject<ResponseModel>(docFileURL);

                                HtmlFileResponseModel htmlFileResponse = new HtmlFileResponseModel();

                                UploadPdfFile uploadPdf = new UploadPdfFile();
                                uploadPdf.PdfUrl = completeDocFileURL?.Data?.ToString() ?? string.Empty;
                                uploadPdf.FileName = item + "-" + engagementLetter.EngagementLetterName ?? string.Empty; ;
                                var respPDF = await UploadPdfFileFromBlob(uploadPdf);
                                resobj.PdfUrl = respPDF.PdfUrl;

                                // Construct the URL
                                string apiUrl = $"https://secure.na4.adobesign.com/api/rest/v6/agreements/{agreementID}/formData";

                                var FileClient = new RestClient(apiUrl);
                                var filerequests = new RestRequest(Method.GET);
                                filerequests.AddHeader("Authorization", "Bearer " + authorizationKey);

                                IRestResponse FileResponse = await FileClient.ExecuteAsync(filerequests);
                                string content = string.Empty;
                                if (FileResponse.StatusCode == HttpStatusCode.OK)
                                {
                                    content = FileResponse.Content;
                                    content = content.Replace("\\", "");
                                }

                                // Completed Letters Insertion for Bot Integration with CCH Starts //
                                int checkLetterId = await _signingRepository.GetLetterStatusReport(engagementLetterList.EngagementLetterId);
                             
                                var engagementletterpdfurl = resobj.PdfUrl;
                                LetterStatusRequest letterStatusRequest = new LetterStatusRequest();
                                if (engagementletterpdfurl != null)
                                {                                   
                                   if(checkLetterId > 0)
                                    {
                                        letterStatusRequest.LettersStatusReportId = checkLetterId;
                                        letterStatusRequest.EngagementLetterId = engagementLetterList.EngagementLetterId;
                                        letterStatusRequest.EngagementLetterName = engagementLetterList.EngagementLetterName;
                                        letterStatusRequest.ClientId = engagementLetterList.ClientId;
                                        letterStatusRequest.ClientName = engagementLetterList.ClientName;
                                        letterStatusRequest.PartnerId = engagementLetterList.PartnerId;
                                        letterStatusRequest.PartnerName = engagementLetterList.PartnerName;
                                        letterStatusRequest.OfficeId = engagementLetterList.OfficeId;
                                        letterStatusRequest.OfficeName = engagementLetterList.OfficeName;
                                        letterStatusRequest.Type = engagementLetterList.EngageTypeName;
                                        letterStatusRequest.Department = engagementLetterList.DepartmentName;
                                        letterStatusRequest.TaxYear = engagementLetterList.TaxYear;
                                        letterStatusRequest.SignersCount = engagementLetterList.ClientSignatureCount;
                                        letterStatusRequest.OtherEntityDetails = engagementLetterList.OtherEntityDetails;
                                        letterStatusRequest.IsEsigning = engagementLetterList.IsEsigning;
                                        letterStatusRequest.Is7216Available = engagementLetterList.Is7216Available;
                                        letterStatusRequest.IsNewClient = engagementLetterList.IsNewClient;
                                        letterStatusRequest.EngagementLetterPDF = engagementletterpdfurl;
                                        letterStatusRequest.ExpiryDate7216 = get7216ExpiryDate;
                                        letterStatusRequest.EngagementLetterStatus = "Completed";
                                        letterStatusRequest.PrimarySignerFirstName = engagementLetterList.SignatoryFirstName;
                                        letterStatusRequest.PrimarySignerLastName = engagementLetterList.PrimarySignerLastName;
                                        letterStatusRequest.FormData = content;
                                        letterStatusRequest.ReturnTypeCode = engagementLetterList.ReturnTypeCode;
                                        var updateLetterStatus = await _signingRepository.UpdateLetterStatusReports(letterStatusRequest);
                                    }
                                    else
                                    {
                                        letterStatusRequest.EngagementLetterId = engagementLetterList.EngagementLetterId;
                                        letterStatusRequest.EngagementLetterName = engagementLetterList.EngagementLetterName;
                                        letterStatusRequest.ClientId = engagementLetterList.ClientId;
                                        letterStatusRequest.ClientName = engagementLetterList.ClientName;
                                        letterStatusRequest.PartnerId = engagementLetterList.PartnerId;
                                        letterStatusRequest.PartnerName = engagementLetterList.PartnerName;
                                        letterStatusRequest.OfficeId = engagementLetterList.OfficeId;
                                        letterStatusRequest.OfficeName = engagementLetterList.OfficeName;
                                        letterStatusRequest.Type = engagementLetterList.EngageTypeName;
                                        letterStatusRequest.Department = engagementLetterList.DepartmentName;
                                        letterStatusRequest.TaxYear = engagementLetterList.TaxYear;
                                        letterStatusRequest.SignersCount = engagementLetterList.ClientSignatureCount;
                                        letterStatusRequest.OtherEntityDetails = engagementLetterList.OtherEntityDetails;
                                        letterStatusRequest.IsEsigning = engagementLetterList.IsEsigning;
                                        letterStatusRequest.Is7216Available = engagementLetterList.Is7216Available;
                                        letterStatusRequest.IsNewClient = engagementLetterList.IsNewClient;
                                        letterStatusRequest.EngagementLetterPDF = engagementletterpdfurl;
                                        letterStatusRequest.ExpiryDate7216 = get7216ExpiryDate;
                                        letterStatusRequest.EngagementLetterStatus = "Completed";
                                        letterStatusRequest.PrimarySignerFirstName = engagementLetterList.SignatoryFirstName;
                                        letterStatusRequest.PrimarySignerLastName = engagementLetterList.PrimarySignerLastName;
                                        letterStatusRequest.FormData = content;
                                        letterStatusRequest.ReturnTypeCode = engagementLetterList.ReturnTypeCode;
                                        var insertLetterStatus = await _signingRepository.InsertLetterStatusReports(letterStatusRequest);
                                    }                                   
                                }
                                //End
                                if (respPDF.PdfUrl != null)
                                {
                                    if (historyLogVersion == null)
                                    {
                                        historyLogVersion = 0;
                                    }
                                    historyLogVersion += 1;

                                    if (agreementStatus == "SIGNED")
                                    {
                                        agreementStatus = "Completed";
                                    }

                                    var estTime = await _getCurrentDatetime.GetCurrentEstDatetime();

                                    HistoryLog insertHistoryLog = new HistoryLog();
                                    insertHistoryLog.EngagementLetterId = item;
                                    insertHistoryLog.Version = historyLogVersion;
                                    insertHistoryLog.EditedBy = engagementLetter.ModifiedBy;
                                    insertHistoryLog.LastModified = estTime;
                                    insertHistoryLog.BatchId = engagementLetter.BatchId;
                                    insertHistoryLog.EngagementLetterName = engagementLetter.EngagementLetterName;
                                    insertHistoryLog.Status = agreementStatus;
                                    insertHistoryLog.ClientEmailId = null;
                                    insertHistoryLog.ReasonforDecline = null;
                                    insertHistoryLog.DeclineTimestamp = null;
                                    insertHistoryLog.Downloaded = null;
                                    insertHistoryLog.Delegated = false;
                                    insertHistoryLog.PDFUrl = respPDF.PdfUrl;

                                    var insertLogs = _signingRepository.InsertHistoryLog(insertHistoryLog);
                                }                
                            }
                        }
                        else if (agreementStatus == _resourceManager.GetString("AdobeStatus"))
                        {
                            var fileRes = new RestClient(_configuration["AppSettingsAdobe:RefreshStatus"] + agreementID + "/events");
                            var eventRequest = new RestRequest(Method.GET);
                            eventRequest.AddHeader("Authorization", "Bearer " + authorizationKey);
                            IRestResponse eventRes = await fileRes.ExecuteAsync(eventRequest);
                            if (eventRes.StatusCode == HttpStatusCode.OK)
                            {
                                var res1 = eventRes.Content;
                                var eventResponse = JsonConvert.DeserializeObject<AdobeAgreementEventVM>(res1);
                                var eventList = eventResponse?.events?.ToList();
                                var eventListType = eventList?.Where(x => x.type == "REJECTED").FirstOrDefault();

                                if (eventListType != null && eventListType.type == _resourceManager.GetString("AdobeReject"))
                                {
                                    EngagementLetter? engagementLetter = await _signingRepository.GetEngagementLetterStatus(item);
                                    if (engagementLetter != null && engagementLetter.DocumentStatusId != 4)
                                    {
                                        var statusId = EnumHelper.DocumentStatus.WithAdminForReview;
                                        engagementLetter.DocumentStatusId = (int)statusId;
                                        var updateStatus = await _signingRepository.UpdateSignedStatusEngagementLetter(engagementLetter);

                                        agreementStatus = "Client declined the Signing";

                                        if (historyLogVersion == null)
                                        {
                                            historyLogVersion = 0;
                                        }
                                        historyLogVersion += 1;

                                        var declineTime = Convert.ToDateTime(eventListType.date);
                                        var utcTime = declineTime.ToUniversalTime();
                                        TimeZoneInfo easternTimeZones = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                                        DateTime estTimenNow = TimeZoneInfo.ConvertTimeFromUtc(utcTime, easternTimeZones);

                                        HistoryLog insertDecline = new HistoryLog();
                                        insertDecline.EngagementLetterId = item;
                                        insertDecline.Version = historyLogVersion;
                                        insertDecline.EditedBy = engagementLetter.ModifiedBy;
                                        insertDecline.LastModified = estTimenNow;
                                        insertDecline.BatchId = engagementLetter.BatchId;
                                        insertDecline.EngagementLetterName = engagementLetter.EngagementLetterName;
                                        insertDecline.Status = agreementStatus;
                                        insertDecline.ClientEmailId = eventListType.actingUserEmail;
                                        insertDecline.ReasonforDecline = eventListType.comment;
                                        insertDecline.DeclineTimestamp = estTimenNow;
                                        insertDecline.Downloaded = null;
                                        insertDecline.Delegated = false;
                                        insertDecline.PDFUrl = null;

                                        await _signingRepository.InsertHistoryLog(insertDecline);

                                        EngagementLetter? engagementStatus = await _signingRepository.GetEngagementLetterStatus(item);

                                        if (engagementStatus?.DocumentStatusId == 4)
                                        {
                                            var estTime = await _getCurrentDatetime.GetCurrentEstDatetime();

                                            int? UpdatedLogVersion = await _signingRepository.GetHistoryLogVersion(item);
                                            if (UpdatedLogVersion == null)
                                            {
                                                UpdatedLogVersion = 0;
                                            }
                                            UpdatedLogVersion += 1;

                                            var Status = "With Admin for Review";
                                            HistoryLog insertAdminReview = new HistoryLog();
                                            insertAdminReview.EngagementLetterId = item;
                                            insertAdminReview.Version = UpdatedLogVersion;
                                            insertAdminReview.EditedBy = engagementLetter.ModifiedBy;
                                            insertAdminReview.LastModified = estTime;
                                            insertAdminReview.BatchId = engagementLetter.BatchId;
                                            insertAdminReview.EngagementLetterName = engagementLetter.EngagementLetterName;
                                            insertAdminReview.Status = Status;
                                            insertAdminReview.ClientEmailId = null;
                                            insertAdminReview.ReasonforDecline = null;
                                            insertAdminReview.DeclineTimestamp = null;
                                            insertAdminReview.Downloaded = null;
                                            insertAdminReview.Delegated = false;
                                            insertAdminReview.PDFUrl = null;
                                            await _signingRepository.InsertHistoryLog(insertAdminReview);
                                        }
                                    }

                                }
                            }
                        }
                        else
                        {
                            var fileRes = new RestClient(_configuration["AppSettingsAdobe:RefreshStatus"] + agreementID + "/events");
                            var eventRequest = new RestRequest(Method.GET);
                            eventRequest.AddHeader("Authorization", "Bearer " + authorizationKey);
                            IRestResponse eventRes = await fileRes.ExecuteAsync(eventRequest);
                            if (eventRes.StatusCode == HttpStatusCode.OK)
                            {
                                var res1 = eventRes.Content;
                                var eventResponse = JsonConvert.DeserializeObject<AdobeAgreementEventVM>(res1);
                                var eventList = eventResponse?.events?.ToList();
                                var eventListType = eventList?.Where(x => x.type == "EMAIL_BOUNCED").FirstOrDefault();

                                if (eventListType != null)
                                {
                                    EngagementLetter? engagementLetter = await _signingRepository.GetEngagementLetterStatus(item);
                                    if (engagementLetter != null)
                                    {
                                        var statusId = 10;
                                        engagementLetter.DocumentStatusId = statusId;
                                        var updateStatus = await _signingRepository.UpdateSignedStatusEngagementLetter(engagementLetter);

                                        agreementStatus = "Invalid Email Id";

                                        if (historyLogVersion == null)
                                        {
                                            historyLogVersion = 0;
                                        }
                                        historyLogVersion += 1;

                                        HistoryLog insertDecline = new HistoryLog();
                                        insertDecline.EngagementLetterId = item;
                                        insertDecline.Version = historyLogVersion;
                                        insertDecline.EditedBy = engagementLetter.ModifiedBy;
                                        insertDecline.LastModified = DateTime.UtcNow;
                                        insertDecline.BatchId = engagementLetter.BatchId;
                                        insertDecline.EngagementLetterName = engagementLetter.EngagementLetterName;
                                        insertDecline.Status = agreementStatus;
                                        insertDecline.ClientEmailId = eventListType.actingUserEmail;
                                        insertDecline.ReasonforDecline = eventListType.comment;
                                        insertDecline.DeclineTimestamp = Convert.ToDateTime(eventListType.date);
                                        insertDecline.Downloaded = null;
                                        insertDecline.Delegated = false;
                                        insertDecline.PDFUrl = null;

                                        await _signingRepository.InsertHistoryLog(insertDecline);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        resobj.ErrorMessage = Fileresponse.Content.ToString();
                        resobj.StatusCode = HttpStatusCode.InternalServerError;
                        resobj.Status = false;
                    }
                }
            }
            return resobj;
        }

        public async Task<ManualRefreshResModel> RefreshedSignedLetterStatus(ManualRefreshResModel signingDocument)
        {
            string EditedBy = string.Empty;
            EngagementLetter? engagementLetters = await _signingRepository.GetEngagementLetterStatus(signingDocument.EngagementLetterId);
            int? historyLogVersion = await _signingRepository.GetHistoryLogVersion(signingDocument.EngagementLetterId);
            var engagementLetterList = await _signingRepository.GetEngagmentLettersByID(signingDocument.EngagementLetterId);
            IEnumerable<SigningInfo> signingInfo = await _signingRepository.GetAgreementId();
            
            if (engagementLetters?.DocumentStatusId == 2)
            {
                string? agreementID = signingInfo.OrderByDescending(x => x.SigningInfoId).FirstOrDefault
                                 (x => x.EngagementLetterId == signingDocument.EngagementLetterId && !x.IsDeleted)?.AgreementId;             
                string? get7216ExpiryDate = signingInfo.FirstOrDefault
                                     (x => x.EngagementLetterId == signingDocument.EngagementLetterId && !x.IsDeleted)?.ExpiryDate7216;
                if (!string.IsNullOrEmpty(agreementID))
                {
                    string? authorizationKey = _configuration["AppSettingsAdobe:adobe_integrationkey"];

                    var Fileclient = new RestClient(_configuration["AppSettingsAdobe:RefreshStatus"] + agreementID);

                    var filerequest = new RestRequest(Method.GET);
                    filerequest.AddHeader("Authorization", "Bearer " + authorizationKey);
                    IRestResponse Fileresponse = await Fileclient.ExecuteAsync(filerequest);
                    if (Fileresponse.StatusCode == HttpStatusCode.OK)
                    {
                        var res = Fileresponse.Content;
                        var responseContent = JsonConvert.DeserializeObject<AdobeSignStatusResponseVM>(res);
                        string? agreementStatus = responseContent?.status;
                        var adobeSignerCount = responseContent?.participantSetsInfo?.Count();
                        if (agreementStatus == "SIGNED" && engagementLetters.DocumentStatusId != 1)
                        {
                            var statusId = 1;
                            engagementLetters.DocumentStatusId = statusId;
                            engagementLetters.ModifiedBy = signingDocument.ModifiedBy;
                            var updateStatus = await _signingRepository.UpdateSignedStatusEngagementLetter(engagementLetters);

                            var getdocumentURL = await GetSignedDocument(agreementID);
                            if (getdocumentURL.StatusCode == HttpStatusCode.OK)
                            {
                                var docFileURL = JsonConvert.SerializeObject(getdocumentURL);
                                var completeDocFileURL = JsonConvert.DeserializeObject<ResponseModel>(docFileURL);

                                HtmlFileResponseModel htmlFileResponse = new HtmlFileResponseModel();

                                UploadPdfFile uploadPdf = new UploadPdfFile();
                                uploadPdf.PdfUrl = completeDocFileURL?.Data?.ToString() ?? string.Empty;
                                uploadPdf.FileName = signingDocument.EngagementLetterId + "-" + engagementLetters.EngagementLetterName ?? string.Empty;
                                var respPDF = await UploadPdfFileFromBlob(uploadPdf);
                                signingDocument.PdfUrl = respPDF.PdfUrl;


                                // Construct the URL
                                string apiUrl = $"https://secure.na4.adobesign.com/api/rest/v6/agreements/{agreementID}/formData";

                                var FileClient = new RestClient(apiUrl);
                                var filerequests = new RestRequest(Method.GET);
                                filerequests.AddHeader("Authorization", "Bearer " + authorizationKey);

                                IRestResponse FileResponse = await FileClient.ExecuteAsync(filerequests);
                                string content = string.Empty;
                                if (FileResponse.StatusCode == HttpStatusCode.OK)
                                {
                                    content = FileResponse.Content;
                                    content = content.Replace("\\", "");
                                }

                                //Completed Letters Insertion for Bot Integration with CCH Starts //
                                int checkLetterId = await _signingRepository.GetLetterStatusReport(engagementLetterList.EngagementLetterId);
                                LetterStatusRequest letterStatusRequest = new LetterStatusRequest();
                                if (signingDocument.PdfUrl != null)
                                {
                                    if (checkLetterId > 0)
                                    {
                                        letterStatusRequest.LettersStatusReportId = checkLetterId;
                                        letterStatusRequest.EngagementLetterId = engagementLetterList.EngagementLetterId;
                                        letterStatusRequest.EngagementLetterName = engagementLetterList.EngagementLetterName;
                                        letterStatusRequest.ClientId = engagementLetterList.ClientId;
                                        letterStatusRequest.ClientName = engagementLetterList.ClientName;
                                        letterStatusRequest.PartnerId = engagementLetterList.PartnerId;
                                        letterStatusRequest.PartnerName = engagementLetterList.PartnerName;
                                        letterStatusRequest.OfficeId = engagementLetterList.OfficeId;
                                        letterStatusRequest.OfficeName = engagementLetterList.OfficeName;
                                        letterStatusRequest.Type = engagementLetterList.EngageTypeName;
                                        letterStatusRequest.Department = engagementLetterList.DepartmentName;
                                        letterStatusRequest.TaxYear = engagementLetterList.TaxYear;
                                        letterStatusRequest.SignersCount = engagementLetterList.ClientSignatureCount;
                                        letterStatusRequest.OtherEntityDetails = engagementLetterList.OtherEntityDetails;
                                        letterStatusRequest.IsEsigning = engagementLetterList.IsEsigning;
                                        letterStatusRequest.Is7216Available = engagementLetterList.Is7216Available;
                                        letterStatusRequest.IsNewClient = engagementLetterList.IsNewClient;
                                        letterStatusRequest.EngagementLetterPDF = signingDocument.PdfUrl;
                                        letterStatusRequest.ExpiryDate7216 = get7216ExpiryDate;
                                        letterStatusRequest.EngagementLetterStatus = "Completed";
                                        letterStatusRequest.PrimarySignerFirstName = engagementLetterList.SignatoryFirstName;
                                        letterStatusRequest.PrimarySignerLastName = engagementLetterList.PrimarySignerLastName;
                                        letterStatusRequest.FormData = content;
                                        letterStatusRequest.ReturnTypeCode = engagementLetterList.ReturnTypeCode;
                                        var updateLetterStatus = await _signingRepository.UpdateLetterStatusReports(letterStatusRequest);
                                    }
                                    else
                                    {
                                        letterStatusRequest.EngagementLetterId = engagementLetterList.EngagementLetterId;
                                        letterStatusRequest.EngagementLetterName = engagementLetterList.EngagementLetterName;
                                        letterStatusRequest.ClientId = engagementLetterList.ClientId;
                                        letterStatusRequest.ClientName = engagementLetterList.ClientName;
                                        letterStatusRequest.PartnerId = engagementLetterList.PartnerId;
                                        letterStatusRequest.PartnerName = engagementLetterList.PartnerName;
                                        letterStatusRequest.OfficeId = engagementLetterList.OfficeId;
                                        letterStatusRequest.OfficeName = engagementLetterList.OfficeName;
                                        letterStatusRequest.Type = engagementLetterList.EngageTypeName;
                                        letterStatusRequest.Department = engagementLetterList.DepartmentName;
                                        letterStatusRequest.TaxYear = engagementLetterList.TaxYear;
                                        letterStatusRequest.SignersCount = engagementLetterList.ClientSignatureCount;
                                        letterStatusRequest.OtherEntityDetails = engagementLetterList.OtherEntityDetails;
                                        letterStatusRequest.IsEsigning = engagementLetterList.IsEsigning;
                                        letterStatusRequest.Is7216Available = engagementLetterList.Is7216Available;
                                        letterStatusRequest.IsNewClient = engagementLetterList.IsNewClient;
                                        letterStatusRequest.EngagementLetterPDF = signingDocument.PdfUrl;
                                        letterStatusRequest.ExpiryDate7216 = get7216ExpiryDate;
                                        letterStatusRequest.EngagementLetterStatus = "Completed";
                                        letterStatusRequest.PrimarySignerFirstName = engagementLetterList.SignatoryFirstName;
                                        letterStatusRequest.PrimarySignerLastName = engagementLetterList.PrimarySignerLastName;
                                        letterStatusRequest.FormData = content;
                                        letterStatusRequest.ReturnTypeCode = engagementLetterList.ReturnTypeCode;
                                        var updateletterstats = await _signingRepository.InsertLetterStatusReports(letterStatusRequest);
                                    }                                       
                                }
                                //End
                                if (respPDF.PdfUrl != null)
                                {
                                    if (historyLogVersion == null)
                                    {
                                        historyLogVersion = 0;
                                    }
                                    historyLogVersion += 1;

                                    if (agreementStatus == "SIGNED")
                                    {
                                        agreementStatus = "Completed";
                                    }
  
                                    HistoryLog insertHistoryLog = new HistoryLog();
                                    insertHistoryLog.EngagementLetterId = signingDocument.EngagementLetterId;
                                    insertHistoryLog.Version = historyLogVersion;
                                    insertHistoryLog.EditedBy = signingDocument.ModifiedBy;
                                    var estTime = await _getCurrentDatetime.GetCurrentEstDatetime();
                                    insertHistoryLog.LastModified = estTime;
                                    insertHistoryLog.BatchId = engagementLetters.BatchId;
                                    insertHistoryLog.EngagementLetterName = engagementLetters.EngagementLetterName;
                                    insertHistoryLog.Status = agreementStatus;
                                    insertHistoryLog.ClientEmailId = null;
                                    insertHistoryLog.ReasonforDecline = null;
                                    insertHistoryLog.DeclineTimestamp = null;
                                    insertHistoryLog.Downloaded = null;
                                    insertHistoryLog.Delegated = false;
                                    insertHistoryLog.PDFUrl = respPDF.PdfUrl;

                                    var insertLogs = _signingRepository.InsertHistoryLog(insertHistoryLog);
                                }
                            }
                        }
                        else if (agreementStatus == _resourceManager.GetString("AdobeStatus"))
                        {
                            var fileRes = new RestClient(_configuration["AppSettingsAdobe:RefreshStatus"] + agreementID + "/events");
                            var eventRequest = new RestRequest(Method.GET);
                            eventRequest.AddHeader("Authorization", "Bearer " + authorizationKey);
                            IRestResponse eventRes = await fileRes.ExecuteAsync(eventRequest);
                            if (eventRes.StatusCode == HttpStatusCode.OK)
                            {
                                var res1 = eventRes.Content;
                                var eventResponse = JsonConvert.DeserializeObject<AdobeAgreementEventVM>(res1);
                                var eventList = eventResponse?.events?.ToList();
                                var eventListType = eventList?.Where(x => x.type == "REJECTED").FirstOrDefault();

                                if (eventListType != null && eventListType.type == _resourceManager.GetString("AdobeReject"))
                                {
                                    if (engagementLetters!= null && engagementLetters.DocumentStatusId != 4)
                                    {
                                        var statusId = EnumHelper.DocumentStatus.WithAdminForReview;
                                        engagementLetters.DocumentStatusId = (int)statusId;
                                        engagementLetters.ModifiedBy = signingDocument.ModifiedBy;
                                        var updateStatus = await _signingRepository.UpdateSignedStatusEngagementLetter(engagementLetters);

                                        agreementStatus = "Client declined the Signing";

                                        if (historyLogVersion == null)
                                        {
                                            historyLogVersion = 0;
                                        }
                                        historyLogVersion += 1;

                                        var declineTime = Convert.ToDateTime(eventListType.date);
                                        var utcTime = declineTime.ToUniversalTime();
                                        TimeZoneInfo easternTimeZones = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                                        DateTime estTimenNow = TimeZoneInfo.ConvertTimeFromUtc(utcTime, easternTimeZones);

                                        HistoryLog insertDecline = new HistoryLog();
                                        insertDecline.EngagementLetterId = signingDocument.EngagementLetterId;
                                        insertDecline.Version = historyLogVersion;
                                        insertDecline.EditedBy = signingDocument.ModifiedBy;
                                        insertDecline.LastModified = estTimenNow;
                                        insertDecline.BatchId = engagementLetters.BatchId;
                                        insertDecline.EngagementLetterName = engagementLetters.EngagementLetterName;
                                        insertDecline.Status = agreementStatus;
                                        insertDecline.ClientEmailId = eventListType.actingUserEmail;
                                        insertDecline.ReasonforDecline = eventListType.comment;
                                        insertDecline.DeclineTimestamp = estTimenNow;
                                        insertDecline.Downloaded = null;
                                        insertDecline.Delegated = false;
                                        insertDecline.PDFUrl = null;

                                        await _signingRepository.InsertHistoryLog(insertDecline);

                                        EngagementLetter? engagementStatus = await _signingRepository.GetEngagementLetterStatus(signingDocument.EngagementLetterId);

                                        if (engagementStatus?.DocumentStatusId == 4)
                                        {
                                            int? UpdatedLogVersion = await _signingRepository.GetHistoryLogVersion(signingDocument.EngagementLetterId);
                                            if (UpdatedLogVersion == null)
                                            {
                                                UpdatedLogVersion = 0;
                                            }
                                            UpdatedLogVersion += 1;

                                            var Status = "With Admin for Review";
                                            HistoryLog insertAdminReview = new HistoryLog();
                                            insertAdminReview.EngagementLetterId = signingDocument.EngagementLetterId;
                                            insertAdminReview.Version = UpdatedLogVersion;
                                            insertAdminReview.EditedBy = signingDocument.ModifiedBy;

                                            var estTime = await _getCurrentDatetime.GetCurrentEstDatetime();
                                            insertAdminReview.LastModified = estTime;
                                            insertAdminReview.BatchId = engagementLetters.BatchId;
                                            insertAdminReview.EngagementLetterName = engagementLetters.EngagementLetterName;
                                            insertAdminReview.Status = Status;
                                            insertAdminReview.ClientEmailId = null;
                                            insertAdminReview.ReasonforDecline = null;
                                            insertAdminReview.DeclineTimestamp = null;
                                            insertAdminReview.Downloaded = null;
                                            insertAdminReview.Delegated = false;
                                            insertAdminReview.PDFUrl = null;
                                            await _signingRepository.InsertHistoryLog(insertAdminReview);
                                        }
                                    }                                  
                                }
                            }
                        }
                        else
                        {
                            var fileRes = new RestClient(_configuration["AppSettingsAdobe:RefreshStatus"] + agreementID + "/events");
                            var eventRequest = new RestRequest(Method.GET);
                            eventRequest.AddHeader("Authorization", "Bearer " + authorizationKey);
                            IRestResponse eventRes = await fileRes.ExecuteAsync(eventRequest);
                            if (eventRes.StatusCode == HttpStatusCode.OK)
                            {
                                var res1 = eventRes.Content;
                                var eventResponse = JsonConvert.DeserializeObject<AdobeAgreementEventVM>(res1);
                                var eventList = eventResponse?.events?.ToList();
                                var eventListType = eventList?.Where(x => x.type == "EMAIL_BOUNCED").FirstOrDefault();

                                if (eventListType != null)
                                {
                                    var statusId = 10;
                                    engagementLetters.DocumentStatusId = statusId;
                                    engagementLetters.ModifiedBy = signingDocument.ModifiedBy;
                                    var updateStatus = await _signingRepository.UpdateSignedStatusEngagementLetter(engagementLetters);

                                    agreementStatus = "Invalid Email Id";

                                    if (historyLogVersion == null)
                                    {
                                        historyLogVersion = 0;
                                    }
                                    historyLogVersion += 1;
                                  
                                    HistoryLog insertDecline = new HistoryLog();
                                    insertDecline.EngagementLetterId = signingDocument.EngagementLetterId;
                                    insertDecline.Version = historyLogVersion;
                                    insertDecline.EditedBy = signingDocument.ModifiedBy;
                                    var estTime = await _getCurrentDatetime.GetCurrentEstDatetime();
                                    insertDecline.LastModified = estTime;
                                    insertDecline.BatchId = engagementLetters.BatchId;
                                    insertDecline.EngagementLetterName = engagementLetters.EngagementLetterName;
                                    insertDecline.Status = agreementStatus;
                                    insertDecline.ClientEmailId = eventListType.actingUserEmail;
                                    insertDecline.ReasonforDecline = eventListType.comment;
                                    insertDecline.DeclineTimestamp = Convert.ToDateTime(eventListType.date);
                                    insertDecline.Downloaded = null;
                                    insertDecline.Delegated = false;
                                    insertDecline.PDFUrl = null;

                                    await _signingRepository.InsertHistoryLog(insertDecline);
                                }
                            }
                        }

                    }
                }
            }
            return signingDocument;
        }

        public async Task<ResponseModel> UploadFinalVersionPDF(UploadLetterResModel uploadLetterRes)
        {
           
            if (uploadLetterRes?.EngagementLetterId > 0)
            {
                string EditedBy = string.Empty;
                string? agreementID;
                uploadLetterRes.UploadPDFdocument = Convert.FromBase64String(uploadLetterRes.UploadPDFContent);
                IEnumerable<SigningInfo> signingInfo = await _signingRepository.GetAgreementId();
                var engagementLetterList = await _signingRepository.GetEngagmentLettersByID(uploadLetterRes.EngagementLetterId);
                var delegatedName = signingInfo.FirstOrDefault(x => x.EngagementLetterId == uploadLetterRes.EngagementLetterId && !x.IsDeleted)?.DelegatedName;

                var signingPartnerName = signingInfo.FirstOrDefault(x => x.EngagementLetterId == uploadLetterRes.EngagementLetterId && !x.IsDeleted)?.SigningPartnerName;

                string? get7216ExpiryDate = signingInfo.FirstOrDefault
                                    (x => x.EngagementLetterId == uploadLetterRes.EngagementLetterId && !x.IsDeleted)?.ExpiryDate7216;

                EngagementLetter? engagementStatus = await _signingRepository.GetEngagementLetterStatus(uploadLetterRes.EngagementLetterId);

                int? historyLogVersion = await _signingRepository.GetHistoryLogVersion(uploadLetterRes.EngagementLetterId);

                agreementID = signingInfo.OrderByDescending(x => x.SigningInfoId).FirstOrDefault
                                 (x => x.EngagementLetterId == uploadLetterRes.EngagementLetterId && !x.IsDeleted)?.AgreementId;


                var estTime = await _getCurrentDatetime.GetCurrentEstDatetime();
                if (engagementStatus?.IsEsigning == true)
                {
                    CheckSigningStatusResModel signingStatus = new CheckSigningStatusResModel();
                    signingStatus.EngagementLetterId = uploadLetterRes.EngagementLetterId;
                    signingStatus.AgreementId = agreementID;
                    signingStatus.DelegatedName = delegatedName;
                    signingStatus.SigningPartnerName = signingPartnerName;
                    signingStatus.Version = historyLogVersion;
                    signingStatus.ModifiedBy = uploadLetterRes.EditedBy;

                    var adobeStstus = await RefreshSignedStatusToComplete(signingStatus);
                    uploadLetterRes.PdfUrl = adobeStstus.PdfUrl;
                    uploadLetterRes.AdobeStatus = adobeStstus.Data?.ToString() ?? string.Empty;
                }
                if (uploadLetterRes.AdobeStatus == "OUT_FOR_SIGNATURE" || engagementStatus?.IsEsigning == false)
                {
                    AdobeCancleSignRequest cancleSignRequest = new AdobeCancleSignRequest();
                    cancleSignRequest.agreementID = agreementID;
                    cancleSignRequest.state = "CANCELLED";
                    if (cancleSignRequest != null && cancleSignRequest.state == "CANCELLED")
                    {
                        string? authorizationKey = _configuration["AppSettingsAdobe:adobe_integrationkey"];

                        var Fileclient = new RestClient(_configuration["AppSettingsAdobe:RefreshStatus"] + cancleSignRequest.agreementID + "/state");
                        var serial_JSON = JsonConvert.SerializeObject(cancleSignRequest);
                        var filerequest = new RestRequest(Method.PUT);
                        filerequest.AddHeader("Authorization", "Bearer " + authorizationKey);
                        filerequest.AddParameter("application/json", serial_JSON, ParameterType.RequestBody);
                        IRestResponse Fileresponse = await Fileclient.ExecuteAsync(filerequest);
                        if (Fileresponse.StatusCode == HttpStatusCode.NoContent)
                        {
                            uploadLetterRes.AdobeStatus = "CANCELLED";
                            string agreementStatus = "E-Signing Cancelled & Letter Uploaded Manually";

                            if (historyLogVersion == null)
                            {
                                historyLogVersion = 0;
                            }
                            historyLogVersion += 1;

                            if (delegatedName != "")
                            {
                                EditedBy = delegatedName ?? string.Empty;
                            }
                            if (signingPartnerName != "")
                            {
                                EditedBy = signingPartnerName ?? string.Empty;
                            }

                         

                            HistoryLog insertCancelStatus = new HistoryLog();
                            insertCancelStatus.EngagementLetterId = uploadLetterRes.EngagementLetterId;
                            insertCancelStatus.Version = historyLogVersion;
                            insertCancelStatus.EditedBy = uploadLetterRes.EditedBy;

                          
                            insertCancelStatus.LastModified = estTime;
                            insertCancelStatus.BatchId = engagementStatus.BatchId;
                            insertCancelStatus.EngagementLetterName = engagementStatus.EngagementLetterName;
                            insertCancelStatus.Status = agreementStatus;
                            insertCancelStatus.ClientEmailId = null;
                            insertCancelStatus.ReasonforDecline = null;
                            insertCancelStatus.DeclineTimestamp = null;
                            insertCancelStatus.Downloaded = null;
                            insertCancelStatus.Delegated = false;
                            insertCancelStatus.PDFUrl = null;
                            await _signingRepository.InsertHistoryLog(insertCancelStatus);
                        }
                    }

                    var engagementLetterName = engagementStatus?.EngagementLetterName;

                    UploadPdfFile uploadPdf = new UploadPdfFile();
                    uploadPdf.PdfUrl = uploadLetterRes.UploadPDFContent ?? string.Empty;
                    uploadPdf.FileName = engagementStatus?.EngagementLetterName ?? string.Empty;
                    var respPDF = await UploadBase64ToBlob(uploadPdf);
                    resobj.PdfUrl = respPDF.PdfUrl;

                    //Completed Letters Insertion for Bot Integration with CCH Starts //

                    LetterStatusRequest letterStatusRequest = new LetterStatusRequest();
                    if (uploadLetterRes.PdfUrl != null)
                    {
                        letterStatusRequest.EngagementLetterId = engagementLetterList.EngagementLetterId;
                        letterStatusRequest.EngagementLetterName = engagementLetterList.EngagementLetterName;
                        letterStatusRequest.ClientId = engagementLetterList.ClientId;
                        letterStatusRequest.ClientName = engagementLetterList.ClientName;
                        letterStatusRequest.PartnerId = engagementLetterList.PartnerId;
                        letterStatusRequest.PartnerName = engagementLetterList.PartnerName;
                        letterStatusRequest.OfficeId = engagementLetterList.OfficeId;
                        letterStatusRequest.OfficeName = engagementLetterList.OfficeName;
                        letterStatusRequest.Type = engagementLetterList.EngageTypeName;
                        letterStatusRequest.Department = engagementLetterList.DepartmentName;
                        letterStatusRequest.TaxYear = engagementLetterList.TaxYear;
                        letterStatusRequest.SignersCount = engagementLetterList.ClientSignatureCount;
                        letterStatusRequest.OtherEntityDetails = engagementLetterList.OtherEntityDetails;
                        letterStatusRequest.IsEsigning = engagementLetterList.IsEsigning;
                        letterStatusRequest.Is7216Available = engagementLetterList.Is7216Available;
                        letterStatusRequest.IsNewClient = engagementLetterList.IsNewClient;
                        letterStatusRequest.EngagementLetterPDF = uploadLetterRes.PdfUrl;
                        letterStatusRequest.ExpiryDate7216 = get7216ExpiryDate;
                        letterStatusRequest.EngagementLetterStatus = "Completed";
                        letterStatusRequest.PrimarySignerFirstName = engagementLetterList.SignatoryFirstName;
                        letterStatusRequest.PrimarySignerLastName = engagementLetterList.PrimarySignerLastName;
                        var updateletterstats = await _signingRepository.InsertLetterStatusReports(letterStatusRequest);
                    }
                    //End

                    int? historyLogVersions = await _signingRepository.GetHistoryLogVersion(uploadLetterRes.EngagementLetterId);

                    if (historyLogVersions == null)
                    {
                        historyLogVersions = 0;
                    }
                    historyLogVersions += 1;
                    EditedBy = uploadLetterRes.EditedBy;

                    var status = "Completed";
                    HistoryLog insertAdminReview = new HistoryLog();
                    insertAdminReview.EngagementLetterId = uploadLetterRes.EngagementLetterId;
                    insertAdminReview.Version = historyLogVersions;
                    insertAdminReview.EditedBy = EditedBy;

                    insertAdminReview.LastModified = estTime;
                    insertAdminReview.BatchId = engagementStatus?.BatchId;
                    insertAdminReview.EngagementLetterName = engagementStatus?.EngagementLetterName;
                    insertAdminReview.Status = status;
                    insertAdminReview.ClientEmailId = null;
                    insertAdminReview.ReasonforDecline = null;
                    insertAdminReview.DeclineTimestamp = null;
                    insertAdminReview.Downloaded = null;
                    insertAdminReview.Delegated = false;
                    insertAdminReview.PDFUrl = respPDF.PdfUrl;
                    await _signingRepository.InsertHistoryLog(insertAdminReview);

                    var statusId = 1;
                    engagementStatus.DocumentStatusId = statusId;
                    engagementStatus.ModifiedBy = uploadLetterRes.EditedBy;
                    engagementStatus.AdminName = uploadLetterRes.EditedBy;
                    var updateStatus = await _signingRepository.UpdateSignedStatusEngagementLetter(engagementStatus);
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;  
                }
                else
                {                    
                    resobj.ErrorMessage = "Document already signed";
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NoContent;                   
                }
            }
            return resobj;
        }

        public async Task<ResponseModel> UploadBase64ToBlob(UploadPdfFile uploadPdfFile)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                if (!string.IsNullOrEmpty(uploadPdfFile.PdfUrl) && !string.IsNullOrEmpty(uploadPdfFile.FileName))
                {
                    string accountName = _configuration["BloBStorageConfig:AzureAccountName"] ?? string.Empty;
                    string connectionString = _configuration["ConnectionStrings:AzureBlobStorage"] ?? string.Empty;
                    string accountKey = _configuration["BloBStorageConfig:AzureAccountKey"] ?? string.Empty;
                    string containerName = _configuration["BloBStorageConfig:AzureContainer_Html"] ?? string.Empty;
                    string fileName = uploadPdfFile.FileName + ".pdf";
                    byte[] dataByte = Convert.FromBase64String(uploadPdfFile.PdfUrl);
                    var storageCredentials = new StorageCredentials(accountName, accountKey);
                    var storageAccount = new CloudStorageAccount(storageCredentials, true);
                    var connString = storageAccount.ToString(true);
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                    var container = blobClient.GetContainerReference(containerName).GetBlockBlobReference(fileName);
                    container.Properties.ContentType = "application/pdf";

                    CloudBlockBlob blob = container;
                    blob.Properties.ContentType = "application/pdf";
                    string timezone = "UTC+03:00";
                    blob.Metadata["Timezone"] = timezone;

                    await blob.DeleteIfExistsAsync();

                    await blob.UploadFromByteArrayAsync(dataByte, 0, dataByte.Length);
                    BlobSasTokenRequest request = new BlobSasTokenRequest();
                    request.AccountKey = accountKey;
                    request.AccountName = accountName;
                    request.ConnectionString = connectionString;
                    request.ContainerName = containerName;
                    request.FileName = fileName;

                    responseModel = await GetFileFromBlob(request);
                }
                else
                {
                    responseModel.ErrorMessage = "Pdf url is null or engagement letter name is null";
                    responseModel.StatusCode = HttpStatusCode.InternalServerError;
                }
            }
            catch (Exception ex)
            {
                var errorMessage = ex.InnerException?.ToString() + ex.StackTrace?.ToString() + ex.Message?.ToString();
                responseModel.StatusCode = HttpStatusCode.InternalServerError;
                responseModel.ErrorMessage = errorMessage;
            }
            return responseModel;
        }

        public async Task<ResponseModel> UploadPdfFileFromBlob(UploadPdfFile uploadPdfFile)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                if (!string.IsNullOrEmpty(uploadPdfFile.PdfUrl) && !string.IsNullOrEmpty(uploadPdfFile.FileName))
                {
                    string accountName = _configuration["BloBStorageConfig:AzureAccountName"] ?? string.Empty;
                    string connectionString = _configuration["ConnectionStrings:AzureBlobStorage"] ?? string.Empty;
                    string accountKey = _configuration["BloBStorageConfig:AzureAccountKey"] ?? string.Empty;
                    string containerName = _configuration["BloBStorageConfig:AzureContainer_Html"] ?? string.Empty;
                    string fileName = uploadPdfFile.FileName + ".pdf";
                    byte[] dataByte;
                    using (WebClient webClient = new WebClient())
                    {
                        dataByte = webClient.DownloadData(uploadPdfFile.PdfUrl);
                    }
                    var storageCredentials = new StorageCredentials(accountName, accountKey);
                    var storageAccount = new CloudStorageAccount(storageCredentials, true);
                    var connString = storageAccount.ToString(true);
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                    var container = blobClient.GetContainerReference(containerName).GetBlockBlobReference(fileName);
                    container.Properties.ContentType = "application/pdf";

                    CloudBlockBlob blob = container;
                    blob.Properties.ContentType = "application/pdf";
                    string timezone = "UTC+03:00";
                    blob.Metadata["Timezone"] = timezone;

                    await blob.DeleteIfExistsAsync();

                    await blob.UploadFromByteArrayAsync(dataByte, 0, dataByte.Length);
                    BlobSasTokenRequest request = new BlobSasTokenRequest();
                    request.AccountKey = accountKey;
                    request.AccountName = accountName;
                    request.ConnectionString = connectionString;
                    request.ContainerName = containerName;
                    request.FileName = fileName;

                    responseModel = await GetFileFromBlob(request);
                }
                else
                {
                    responseModel.ErrorMessage = "Pdf url is null or engagement letter name is null";
                    responseModel.StatusCode = HttpStatusCode.InternalServerError;
                }
            }
            catch (Exception ex)
            {
                var errorMessage = ex.InnerException?.ToString() + ex.StackTrace?.ToString() + ex.Message?.ToString();
                responseModel.StatusCode = HttpStatusCode.InternalServerError;
                responseModel.ErrorMessage = errorMessage;
            }
            return responseModel;
        }

        public async Task<ResponseModel> GetFileFromBlob(BlobSasTokenRequest blobSasTokenRequest)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                CloudStorageAccount backupStorageAccount = CloudStorageAccount.Parse(blobSasTokenRequest.ConnectionString);
                var backupBlobClient = backupStorageAccount.CreateCloudBlobClient();
                var backupContainer = backupBlobClient.GetContainerReference(blobSasTokenRequest.ContainerName);

                CloudBlockBlob blobFile = backupContainer.GetBlockBlobReference(blobSasTokenRequest.FileName);
                BlobSasBuilder blobSasBuilder = new BlobSasBuilder()
                {
                    BlobContainerName = blobSasTokenRequest.ContainerName,
                    BlobName = blobSasTokenRequest.FileName,
                    ExpiresOn = DateTime.UtcNow.AddYears(1),//Let SAS token expire after 5 minutes.
                };
                blobSasBuilder.SetPermissions(BlobSasPermissions.Read);
                var sasToken = blobSasBuilder.ToSasQueryParameters(new StorageSharedKeyCredential(blobSasTokenRequest.AccountName, blobSasTokenRequest.AccountKey)).ToString();
                var sasUrl = blobFile.Uri.AbsoluteUri + "?" + sasToken;
                responseModel.PdfUrl = sasUrl;
                responseModel.StatusCode = HttpStatusCode.OK;
                responseModel.Status = true;
            }
            catch (Exception ex)
            {
                var errorMessage = ex.InnerException?.ToString() + ex.StackTrace?.ToString() + ex.Message?.ToString();
                responseModel.StatusCode = HttpStatusCode.InternalServerError;
                responseModel.ErrorMessage = errorMessage;
            }

            return responseModel;
        }

        public async Task<ResponseModel> GetSignedDocument(string agreementID)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                string? authorizationKey = _configuration["AppSettingsAdobe:adobe_integrationkey"];
                var urlLink = new RestClient(_configuration["AppSettingsAdobe:RefreshStatus"] + agreementID + "/combinedDocument/url?attachAuditReport=true");

                var urlRequest = new RestRequest(Method.GET);
                urlRequest.AddHeader("Authorization", "Bearer " + authorizationKey);
                IRestResponse urlResponse = await urlLink.ExecuteAsync(urlRequest);

                if (urlResponse.StatusCode == HttpStatusCode.OK)
                {
                    var responseOP = JsonConvert.DeserializeObject<SignedDocumentResponse>(urlResponse.Content);
                    responseModel.Data = responseOP?.url?.ToString();
                    responseModel.StatusCode = HttpStatusCode.OK;
                    responseModel.Status = true;
                }
                else if (urlResponse.StatusCode == HttpStatusCode.NotFound)
                {
                    var resData = await GetSignedDocument(agreementID);
                    return resData;
                }
                else
                {
                    responseModel.ErrorMessage = urlResponse.Content.ToString();
                    responseModel.StatusCode = HttpStatusCode.InternalServerError;
                    responseModel.Status = false;
                }
            }
            catch (Exception ex)
            {
                var errorMessage = ex.InnerException?.ToString() + ex.StackTrace?.ToString() + ex.Message?.ToString();
                responseModel.StatusCode = HttpStatusCode.InternalServerError;
                responseModel.ErrorMessage = errorMessage;
            }
            return responseModel;
        }

        public async Task<string?> GetPdfUrl(int letterId)
        {
            var pdfUrl = await _signingRepository.GetPdfUrl(letterId);
            return pdfUrl;
        }
    }
}
