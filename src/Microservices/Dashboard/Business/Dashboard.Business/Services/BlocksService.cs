﻿using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Data.Repositories;
using Dashboard.Models.ResponseModel;
using Newtonsoft.Json;
using static Dashboard.Models.Blocks.TemplateJSON;

namespace Dashboard.Business.Services
{
    public class BlocksService : IBlocksService
    {
        private readonly IBlocksRepository _blockRepository;
        private readonly IMapper _mapper;

        public BlocksService(IBlocksRepository blockRepository, IMapper mapper)
        {
            _blockRepository = blockRepository;
            _mapper = mapper;
        }

        public async Task<Tuple<BlocksResponseModel, string>> AddBlocks(BlocksResponseModel blocks)
        {
            BlocksResponseModel blocksResponseModel = new BlocksResponseModel();
            var validationErrorMessage = string.Empty;
            var res = await IsValid(blocks);

            if (res.Item1 == true)
            {
                var blocker = _mapper.Map<Block>(blocks);
                var blkAdd = await _blockRepository.AddBlocks(blocker);
                blocksResponseModel = _mapper.Map<BlocksResponseModel>(blkAdd);
            }

            return new Tuple<BlocksResponseModel, string> (blocksResponseModel,res.Item2);
        }

        public async Task<Tuple<bool, string>> IsValid(BlocksResponseModel block)
        {
            var validationResult = await _blockRepository.IsValidBlockName(block);
            string validationErrorMessage = string.Empty;

            if (validationResult == false)
            {
                validationErrorMessage = "Block Name already exists.";
            }

            return new Tuple<bool, string>(validationResult, validationErrorMessage);
        }

        public async Task<Tuple<BlocksResponseModel, string>> UpdateBlocks(BlocksResponseModel blocks)
        {
            BlocksResponseModel blocksResponseModel = new BlocksResponseModel();
            var res = await IsValid(blocks);

            if (res.Item1 == true)
            {
                var blocker = _mapper.Map<Block>(blocks);
                var blkAdd = await _blockRepository.UpdateBlocks(blocker);
                blocksResponseModel = _mapper.Map<BlocksResponseModel>(blkAdd);               
            }
            return new Tuple<BlocksResponseModel, string>(blocksResponseModel, res.Item2);
        }

        public async Task<DeleteBlockResModel> DeleteBlock(DeleteBlockResModel block)
        {
            var blockById = new List<BlocksList>();
            foreach (var item in block.BlockId)
            {
                blockById = await GetBlocksById(item);
                if (blockById?.FirstOrDefault()?.ConnectedTemplates == "")
                {
                    block.BlockId[0] = item;
                    var blocks = _mapper.Map<DeleteBlockResModel>(block);
                    var isDeleted = await _blockRepository.DeleteBlock(block);
                    block.IsDeleted = isDeleted;
                }
                else
                {
                    block.IsDeleted = false;
                }
            }
            return block;
        }

        public async Task<IEnumerable<ActiveBlocksList>> GetActiveBlocksList()
        {
            return await _blockRepository.GetActiveBlocksList();
        }

        public async Task<IEnumerable<BlocksList>> GetBlocks()
        {
            return await _blockRepository.GetBlocks();
        }

        public async Task<List<BlocksList>> GetBlocksById(int blockId)
        {
            string? Questioncompare1;
            var dataBlockList = await _blockRepository.GetBlocksById(blockId);
            if (dataBlockList != null)
            {
                Questioncompare1 = dataBlockList.BlocksById?.FirstOrDefault()?.BlockName;
                var relatedEntities = new List<String>();
                string? cp = "";
                List<string> Questionlist = new List<string>();
                foreach (var item in dataBlockList.ConnectedTemplate)
                {


                    List<string> values = new List<string>();
                    cp = item.TemplateLogic;
                    var results = JsonConvert.DeserializeObject<Rootobject>(cp);

                    for (int i = 0; i < results?.pages[0].elements.Length; i++)
                    {
                        values.Add(results.pages[0].elements[i].name);
                    }

                    for (int i = 0; i < values.Count; i++)
                    {
                        if (String.Equals(values[i], Questioncompare1))
                        {

                            Questionlist.Add(item.TemplateName);
                        }
                    }
                }

                relatedEntities = Questionlist.Distinct().ToList();
                string ConnectedTemplates = (string.Join(",", relatedEntities));
                dataBlockList.BlocksById.FirstOrDefault().ConnectedTemplates = ConnectedTemplates;
            }
            return dataBlockList.BlocksById;
        }

        public async Task<IEnumerable<BlockIdListResponseModel>> GetBlocksIdList()
        {
            return await _blockRepository.GetBlocksIdList();
        }
        
        public async Task<BlockStatusListResModel> Load()
        {
            return await _blockRepository.Load();
        }
    }
}
