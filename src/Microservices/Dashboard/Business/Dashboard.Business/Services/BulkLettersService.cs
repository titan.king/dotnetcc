﻿using AutoMapper;
using Azure.Storage;
using Azure.Storage.Sas;
using Dashboard.Business.Interfaces;
using Dashboard.Data.Context;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using RestSharp;
using System.Net;
using System.Net.Http.Headers;
using System.Resources;
using System.Text;

namespace Dashboard.Business.Services
{
    public class BulkLettersService : IBulkLettersService
    {
        private readonly IBulkLettersRepository _bulkLettersRepository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly ResourceManager _resourceManager;
        private readonly DashboardDBContext _dbContext;
        public BulkLettersService(IBulkLettersRepository bulkLettersRepository, IMapper mapper, DashboardDBContext dbContext, IConfiguration configuration, IHttpContextAccessor httpContextAccessor, ResourceManager resourceManager)
        {
            _bulkLettersRepository = bulkLettersRepository;
            _mapper = mapper;
            _configuration = configuration;
            _resourceManager = resourceManager;
            _dbContext = dbContext;
        }
        public async Task<ResponseModel> GetAccessToken()
        {
            ResponseModel responseModel = new ResponseModel();

            string defaultString = string.Empty;
            string client_id = _configuration["AppSettingsAdobe:adobe_client_id"] ?? defaultString;
            string client_secret = _configuration["AppSettingsAdobe:adobe_client_secret"] ?? defaultString;
            string accessToken_AdobeURL = _configuration["AppSettingsAdobe:GetAccessToken"] ?? defaultString;
            if (!string.IsNullOrEmpty(client_id) || !string.IsNullOrEmpty(client_secret))
            {
                var Fileclient = new RestClient(accessToken_AdobeURL
                + "client_id=" + client_id + "&client_secret=" + client_secret + "&grant_type=client_credentials&scope=openid, AdobeID, DCAPI");

                var filerequest = new RestRequest(Method.POST);
                IRestResponse Fileresponse = await Fileclient.ExecuteAsync(filerequest);

                if (Fileresponse.StatusCode == HttpStatusCode.OK)
                {
                    var res = Fileresponse.Content;
                    var responseContent = JsonConvert.DeserializeObject<AccessTokenResponse>(res);
                    responseModel.Data = responseContent;
                    responseModel.StatusCode = HttpStatusCode.OK;
                    responseModel.Status = true;
                }
                else
                {
                    responseModel.ErrorMessage = Fileresponse.Content;
                    responseModel.StatusCode = HttpStatusCode.InternalServerError;
                }
            }

            return responseModel;
        }
        public async Task<PagedJsResponseModel> GeneratePagedJsHtml(PagedJsRequestModel pagedJsRequest)
        {
            var ree = JsonConvert.SerializeObject(pagedJsRequest);
            ResponseModel resobj = new ResponseModel();
            PagedJsResponseModel pagedJsResponseModel = new PagedJsResponseModel();

            if (!string.IsNullOrEmpty(pagedJsRequest.payload))
            {
                string pagedJS_AdobeURL = _configuration["AppSettingsAdobe:GeneratePagedJS"] ?? string.Empty;
                var Clients = new RestClient(pagedJS_AdobeURL);
                var jsonData = JsonConvert.SerializeObject(pagedJsRequest);
                var request = new RestRequest(Method.POST);
                request.AddHeader("x-api-key", "c97c4b0d-cb47-494e-babf-f50162ca06d6");
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", jsonData, ParameterType.RequestBody);

                IRestResponse pagedJsResponse = await Clients.ExecuteAsync(request);
                if (pagedJsResponse.StatusCode == HttpStatusCode.OK)
                {
                    var res = pagedJsResponse.Content;
                    var responseContent = JsonConvert.DeserializeObject<PagedJsResponseModel>(res);
                    if (responseContent != null)
                    {
                        HtmlDocument doc = new HtmlDocument();
                        doc.LoadHtml(responseContent.payload);
                        doc.DocumentNode.ChildNodes[1].ChildNodes[2].ChildNodes[0].InnerHtml = string.Empty;
                        doc.DocumentNode.ChildNodes[1].ChildNodes[2].ChildNodes[2].InnerHtml = string.Empty;
                        doc.DocumentNode.InnerHtml = doc.DocumentNode.OuterHtml.Replace("height: calc(100% - var(--pagedjs-footnotes-height));", "height: calc(110% - var(--pagedjs-footnotes-height));");
                        responseContent.payload = doc.DocumentNode.OuterHtml;
                        pagedJsResponseModel.Data = responseContent;
                        pagedJsResponseModel.StatusCode = HttpStatusCode.OK;

                        //string searchText = "Dte_es_:signer1:date";
                        //var datas = doc.DocumentNode.ChildNodes[1].ChildNodes[2].ChildNodes[1].ChildNodes.Where(a => a.Name != "#text")?.ToArray();
                        //List<int> indices = new List<int>();
                        //if (datas != null)
                        //{
                        //    for (int i = 0; i < datas.Length; i++)
                        //    {
                        //        if (datas[i] != null && !string.IsNullOrEmpty(datas[i].InnerHtml) && datas[i].InnerHtml.Contains(searchText))
                        //        {
                        //            indices.Add(i);
                        //        }
                        //    }
                        //    if (indices.Count() > 0)
                        //    {
                        //        HtmlNode targetNode = datas[0];
                        //        HtmlNodeCollection divNodes = targetNode.SelectNodes("//div[contains(@class, 'pagedjs_margin pagedjs_margin-top-center hasContent')]");
                        //        if (divNodes != null)
                        //        {
                        //            int count = 0;
                        //            foreach (HtmlNode divNode in divNodes)
                        //            {
                        //                if (indices.Count() > 0 && count > indices[0])
                        //                {
                        //                    // Remove the 'hasContent' class from the class attribute
                        //                    divNode.SetAttributeValue("class", divNode.GetAttributeValue("class", "").Replace("hasContent", "").Trim());
                        //                }
                        //                count++;
                        //            }
                        //        }
                        //        divNodes = targetNode.SelectNodes("//div[contains(@class, 'pagedjs_margin pagedjs_margin-top-right hasContent')]");
                        //        if (divNodes != null)
                        //        {
                        //            int count = 0;
                        //            foreach (HtmlNode divNode in divNodes)
                        //            {
                        //                if (indices.Count() > 0 && count > indices[0])
                        //                {
                        //                    // Remove the 'hasContent' class from the class attribute
                        //                    divNode.SetAttributeValue("class", divNode.GetAttributeValue("class", "").Replace("hasContent", "").Trim());
                        //                }
                        //                count++;
                        //            }
                        //        }
                        //        divNodes = targetNode.SelectNodes("//div[contains(@class, 'pagedjs_margin pagedjs_margin-top-left hasContent')]");
                        //        if (divNodes != null)
                        //        {
                        //            int count = 0;
                        //            foreach (HtmlNode divNode in divNodes)
                        //            {
                        //                if (count == 0)
                        //                {
                        //                    // Remove the 'hasContent' class from the class attribute
                        //                    divNode.SetAttributeValue("class", divNode.GetAttributeValue("class", "").Replace("hasContent", "").Trim());
                        //                }
                        //                if (indices.Count() > 0 && count > indices[0])
                        //                {
                        //                    // Remove the 'hasContent' class from the class attribute
                        //                    divNode.SetAttributeValue("class", divNode.GetAttributeValue("class", "").Replace("hasContent", "").Trim());
                        //                }
                        //                count++;
                        //            }
                        //        }
                        //    }
                        //    doc.DocumentNode.InnerHtml = doc.DocumentNode.OuterHtml.Replace("height: calc(100% - var(--pagedjs-footnotes-height));", "height: calc(110% - var(--pagedjs-footnotes-height));");
                        //    responseContent.payload = doc.DocumentNode.OuterHtml;
                        //    pagedJsResponseModel.Data = responseContent;
                        //    pagedJsResponseModel.StatusCode = HttpStatusCode.OK;
                        //}
                        //pagedJsResponseModel = responseContent;
                    }

                }
                else
                {
                }
            }
            else
            {
                resobj.ErrorMessage = "Html content is null";
                resobj.StatusCode = HttpStatusCode.BadRequest;
            }

            return pagedJsResponseModel;
        }
        public async Task<bool> InsertAttachmentsBulkLetter(BulkLetterEditAttachmentsResponseModel editAttachmentsViewModel)
        {
            bool insertresponse = false;
            string attachmentURL = string.Empty;
            if (editAttachmentsViewModel != null && !string.IsNullOrEmpty(editAttachmentsViewModel?.AttachmentsJSON) && editAttachmentsViewModel.TemplateId != 0)
            {
                var templateName = await _bulkLettersRepository.GetTemplateName(editAttachmentsViewModel);
                if (!string.IsNullOrEmpty(templateName))
                {
                    attachmentURL = await GetCombinedAttachmentURL(editAttachmentsViewModel.AttachmentsJSON, templateName);
                    editAttachmentsViewModel.AttachmentsURL = attachmentURL;
                   var respInsert = await _bulkLettersRepository.InsertAttachmentsBulkLetter(editAttachmentsViewModel);
                    insertresponse = respInsert;
                }
            }
            return insertresponse;
        }
        public async Task<ResponseModel> HtmltoPDFConvert(PdfConversionRequest pdfConversionRequest)
        {
            pdfConversionRequest.pdfConversion.inputUrl = pdfConversionRequest.pdfConversion.inputUrl.Replace(" ", "%20");
            ResponseModel responseModel = new ResponseModel();

            string defaultString = string.Empty;
            string x_api_key = _configuration["AppSettingsAdobe:adobe_client_id"] ?? defaultString;
            string htmltopdf_AdobeURL = _configuration["AppSettingsAdobe:HtmltoPDFConvert"] ?? string.Empty;
            var getAccessToken = await GetAccessToken();
            if (getAccessToken.StatusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(x_api_key))
            {
                var serial = JsonConvert.SerializeObject(getAccessToken.Data);
                var authorize_token = JsonConvert.DeserializeObject<AccessTokenResponse>(serial);
                var authorize = authorize_token.access_token;
                var Client = new RestClient(htmltopdf_AdobeURL);
                var jsonData = JsonConvert.SerializeObject(pdfConversionRequest.pdfConversion);
                var request = new RestRequest(Method.POST);

                Random generator = new Random();
                int xrequestid = Convert.ToInt32(generator.Next(1, 1000000).ToString("D6"));

                request.AddHeader("Authorization", "Bearer " + authorize);
                request.AddHeader("x-api-key", x_api_key);
                request.AddHeader("x-request-id", xrequestid.ToString());
                request.AddParameter("application/json", jsonData, ParameterType.RequestBody);

                IRestResponse Fileresponse = await Client.ExecuteAsync(request);
                if (Fileresponse.StatusCode == HttpStatusCode.Created)
                {
                    var dataRes = await GetPdfStatus(xrequestid, authorize);
                    responseModel = dataRes;
                }
                else if (Convert.ToInt32(Fileresponse.StatusCode) == 429)
                {
                    responseModel.ErrorMessage = "Maximum API limit is reached, hence letter cant be submitted for signing, please contact the IT Helpdesk team to proceed further";
                    responseModel.Status = false;
                    responseModel.StatusCode = Fileresponse.StatusCode;
                }
                else
                {
                    responseModel.Status = false;
                    responseModel.StatusCode = Fileresponse.StatusCode;
                }
            }
            else
            {
                return getAccessToken;
            }

            return responseModel;
        }
        public async Task<ResponseModel> GetPdfStatus(int xrequestid, string authorize)
        {
            ResponseModel responseModel = new ResponseModel();

            string defaultString = string.Empty;
            string x_api_key = _configuration["AppSettingsAdobe:adobe_client_id"] ?? defaultString;
            string pdfStatus_AdobeURL = _configuration["AppSettingsAdobe:GetPdfStatus"] ?? string.Empty;
            if (xrequestid > 0 && !string.IsNullOrEmpty(authorize))
            {
                string uri = pdfStatus_AdobeURL;
                var Clients = new RestClient(uri + xrequestid + "/status");
                var statusRequest = new RestRequest(Method.GET);
                statusRequest.AddHeader("Authorization", "Bearer " + authorize);
                statusRequest.AddHeader("x-api-key", x_api_key);
                statusRequest.AddHeader("Content-Type", "application/json");
                IRestResponse statusResponse = await Clients.ExecuteAsync(statusRequest);
                if (statusResponse.StatusCode == HttpStatusCode.OK)
                {
                    var res = statusResponse.Content;
                    var responseContent = JsonConvert.DeserializeObject<AdobePDFStatusResponse>(res);
                    if (responseContent.status.ToLower() == "in progress")
                    {
                        responseModel = await GetPdfStatus(xrequestid, authorize);
                    }
                    else
                    {
                        responseModel.Data = responseContent.asset.downloadUri;
                        responseModel.PdfUrl = responseContent.asset.assetID;
                        responseModel.Status = true;
                        responseModel.StatusCode = HttpStatusCode.OK;
                        return responseModel;
                    }
                }
                else
                {
                    responseModel.ErrorMessage = statusResponse.Content;
                    responseModel.StatusCode = statusResponse.StatusCode;
                    responseModel.Status = false;
                }
            }
            else
            {
                responseModel.ErrorMessage = "AccessToken is null";
                responseModel.StatusCode = HttpStatusCode.BadRequest;
                responseModel.Status = false;
            }

            return responseModel;
        }
        public async Task<ResponseModel> UploadHTMLLetter(LetterHTMLRequestModel letterHtmlContent)
        {
            ResponseModel resobj = new ResponseModel();
            PagedJsRequestModel pagedJsRequest = new PagedJsRequestModel();
            byte[] bytes = Convert.FromBase64String(letterHtmlContent.LetterHtmlContent);
            string utf8String = Encoding.UTF8.GetString(bytes);
            pagedJsRequest.payload = utf8String;
            PagedJsResponseModel responseContent = new PagedJsResponseModel();

            responseContent = await GeneratePagedJsHtml(pagedJsRequest);
            var serial = JsonConvert.SerializeObject(responseContent.Data);
            var pagedJsHtml = JsonConvert.DeserializeObject<PagedJsResponseModel>(serial);
            if (pagedJsHtml != null || !string.IsNullOrEmpty(pagedJsHtml?.payload))
            {
                //Starting HTML to URL Process
                var decodedStr = Base64UrlEncoder.Encode(pagedJsHtml.payload);
                HtmlFileResponseModel htmlFileResponse = new HtmlFileResponseModel();

                if (!string.IsNullOrEmpty(decodedStr) && !string.IsNullOrEmpty(letterHtmlContent.TemplateName))
                {
                    UploadHtmlFile uploadHtmlFile = new UploadHtmlFile();
                    uploadHtmlFile.FileName = letterHtmlContent.TemplateName;
                    uploadHtmlFile.Base64Html = decodedStr;
                    var htmlResp = await UploadHtmlFileFromBlob(uploadHtmlFile);

                    htmlFileResponse.StatusCode = HttpStatusCode.OK;
                    htmlFileResponse.Message = "Success";
                    resobj.Data = htmlFileResponse;
                    resobj.StatusCode = HttpStatusCode.OK;
                    resobj.Status = true;

                    //Starting HTML to PDF Process
                    if (!string.IsNullOrEmpty(htmlResp?.Data?.ToString()))
                    {
                        PdfConversionRequest pdfConversionRequest = new PdfConversionRequest();
                        htmlResp.Data = htmlResp?.Data?.ToString()?.Replace(" ", "%20");
                        var pdfConversion = new PdfConversion();
                        pdfConversion.inputUrl = htmlResp?.Data?.ToString() ?? string.Empty;

                        pdfConversion.includeHeaderFooter = true;
                        pdfConversion.pageLayout = new PageLayout { pageWidth = "11", pageHeight = "8.5" };
                        pdfConversion.json = "{}";

                        pdfConversionRequest.pdfConversion = pdfConversion;
                        var htmlResponse = await HtmltoPDFConvert(pdfConversionRequest);
                        if (htmlResponse.Data != null && htmlResponse.StatusCode == HttpStatusCode.OK)
                        {
                            resobj.Data = htmlResponse.PdfUrl;
                            resobj.PdfUrl = htmlResponse.Data.ToString();
                            resobj.Status = true;
                            resobj.StatusCode = HttpStatusCode.OK;
                        }
                        else
                        {
                            resobj.Data = htmlResponse;
                            resobj.Status = false;
                            resobj.ErrorMessage = "ErrorInfo: HTML to PDF conversion response is NULL";
                            resobj.StatusCode = HttpStatusCode.InternalServerError;
                        }
                    }
                    else
                    {
                        resobj.ErrorMessage = "HtmlURL path is null";
                        resobj.StatusCode = HttpStatusCode.BadRequest;
                    }
                }
                else
                {
                    htmlFileResponse.Message = "data is null";
                    resobj.Data = htmlFileResponse;
                }
            }
            else
            {
                resobj.ErrorMessage = "Empty PagedJS Response";
                resobj.StatusCode = HttpStatusCode.BadRequest;
            }
            return resobj;
        }
        public async Task<string> GetCombinedAttachmentURL(string attachmentJSON, string templateName)
        {
            string attachURL = string.Empty;
            AttachmentPdfFile attachmentPdf = new AttachmentPdfFile();
            CombinePDFReq combinePDF = new CombinePDFReq();
            combinePDF.TemplateName = templateName;
            List<string> assetIDList = new List<string>();
            var convDeserial = JsonConvert.DeserializeObject<List<AttachmentBase64String>>(attachmentJSON);
            attachmentPdf.UploadAttachmentJSON = convDeserial;
            var attachmentRes = await UploadAttachment(attachmentPdf);
            if (attachmentRes != null)
            {
                int i = 0;
                foreach (var item in attachmentRes)
                {
                    assetIDList.Add(item.ToString());
                    if (i == 0)
                    { combinePDF.Attachment1 = item.Replace("\"", string.Empty).Replace('"', ' ').ToString(); i++; }
                    else if (i == 1)
                    { combinePDF.Attachment2 = item.Replace("\"", string.Empty).Replace('"', ' ').ToString(); i++; }
                    else if (i == 2)
                    { combinePDF.Attachment3 = item.Replace("\"", string.Empty).Replace('"', ' ').ToString(); i++; }
                    else if (i == 3)
                    { combinePDF.Attachment4 = item.Replace("\"", string.Empty).Replace('"', ' ').ToString(); i++; }
                    else if (i == 4)
                    { combinePDF.Attachment5 = item.Replace("\"", string.Empty).Replace('"', ' ').ToString(); i++; }
                }
                var combinePDFResponse = await CombinePDFReq(combinePDF);
                if (combinePDFResponse != null && combinePDFResponse.Data != null)
                {
                    attachURL = combinePDFResponse.Data.ToString();
                }
                else
                {
                    attachURL = string.Empty;
                }

            }
            return attachURL;
        }
        public async Task<List<string>> UploadAttachment(AttachmentPdfFile pdfDetails)
        {
            ResponseModel responseModel = new ResponseModel();
            List<string> attachmentAssetID = new List<string>();

            if (pdfDetails != null && pdfDetails.UploadAttachmentJSON.Count > 0)
            {
                foreach (var item in pdfDetails.UploadAttachmentJSON)
                {
                    string attachmentBase64 = item.Base64String;
                    var assetRes = await GetAttachmentAsset(attachmentBase64);
                    attachmentAssetID.Add(assetRes);
                }
            }

            return attachmentAssetID;
        }
        public async Task<string> GetAttachmentAsset(string Attachmentfile)
        {
            ResponseModel resobj = new ResponseModel();
            string attachmentassetID = string.Empty;
            var AttachmentPDFdocument = Attachmentfile;
            var full_authorize = await GetUploadAssetUrl();
            if (full_authorize.StatusCode == HttpStatusCode.OK)
            {
                AdobeResponse response = JsonConvert.DeserializeObject<AdobeResponse>((string)full_authorize.Data);
                var uploadUri = response?.uploadUri;
                var assetID = response?.assetID;

                AdobeResponse adobeResponse = new AdobeResponse();
                adobeResponse.uploadUri = uploadUri;
                adobeResponse.assetID = assetID;
                adobeResponse.AttachemntPDFcontent = AttachmentPDFdocument;
                var mergePdf = await UploadAsset(adobeResponse);
                if (mergePdf.StatusCode == HttpStatusCode.OK)
                {
                    var serializedAssetId = JsonConvert.SerializeObject(mergePdf.Data);
                    attachmentassetID = serializedAssetId;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                }
            }
            return attachmentassetID;
        }
        public async Task<ResponseModel> GetUploadAssetUrl()
        {
            ResponseModel resobj = new ResponseModel();

            string defaultString = string.Empty;
            string uploadAsset_AdobeURL = _configuration["AppSettingsAdobe:GetUploadAssetUrl"] ?? defaultString;
            string x_api_key = _configuration["AppSettingsAdobe:adobe_client_id"] ?? defaultString;
            var full_authorize = await GetAccessToken();
            if (full_authorize.StatusCode == HttpStatusCode.OK)
            {
                var serial = JsonConvert.SerializeObject(full_authorize.Data);
                var authorize_token = JsonConvert.DeserializeObject<AccessTokenResponse>(serial);
                var authorize = authorize_token?.access_token;
                var Clients = new RestClient(uploadAsset_AdobeURL);
                var combineRequest = new RestRequest(Method.POST);
                combineRequest.AddHeader("Authorization", "Bearer " + authorize);
                combineRequest.AddHeader("x-api-key", x_api_key);
                var payload = new
                {
                    mediaType = "application/pdf"
                };
                string serial_JSON = JsonConvert.SerializeObject(payload);
                combineRequest.AddParameter("application/json", serial_JSON, ParameterType.RequestBody);
                IRestResponse combineResponse = await Clients.ExecuteAsync(combineRequest);

                if (combineResponse.StatusCode == HttpStatusCode.OK)
                {
                    resobj.Data = combineResponse.Content;
                    resobj.StatusCode = HttpStatusCode.OK;
                }
            }

            return resobj;
        }
        public async Task<ResponseModel> UploadAsset(AdobeResponse adobeResponse)
        {
            ResponseModel resobj = new ResponseModel();

            string apiUrl = adobeResponse.uploadUri;
            string base64File = adobeResponse.AttachemntPDFcontent;
            byte[] fileBytes = Convert.FromBase64String(base64File);
            using (HttpClient client = new HttpClient())
            {
                using (var content = new ByteArrayContent(fileBytes))
                {
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                    var response = await client.PutAsync(apiUrl, content);

                    if (response.IsSuccessStatusCode == true)
                    {
                        resobj.StatusCode = HttpStatusCode.OK;
                        resobj.Data = adobeResponse.assetID;
                    }
                    else
                    {
                        resobj.StatusCode = HttpStatusCode.BadRequest;
                    }
                }
            }

            return resobj;
        }
        public async Task<ResponseModel> CombinePDFReq(CombinePDFReq combinePDF)
        {
            ResponseModel response = new ResponseModel();
            CombinedPDFRequestModel combinePDFRequest = new CombinedPDFRequestModel();
            combinePDFRequest.assets = new List<Assete>();
            Assete asset = new Assete();
            asset = new Assete();
            if(!string.IsNullOrEmpty(combinePDF.LetterHTMLAssetID1))
            {
                asset.assetID = combinePDF.LetterHTMLAssetID1;
                combinePDFRequest.assets.Add(asset);
            }
            if(!string.IsNullOrEmpty(combinePDF.AssetId7216Form))
            {
                asset = new Assete();
                asset.assetID = combinePDF.AssetId7216Form;
                combinePDFRequest.assets.Add(asset);
            }
            if (!string.IsNullOrEmpty(combinePDF.Attachment1))
            {
                asset = new Assete();
                asset.assetID = combinePDF.Attachment1;
                combinePDFRequest.assets.Add(asset);
            }
            if (!string.IsNullOrEmpty(combinePDF.Attachment2))
            {
                asset = new Assete();
                asset.assetID = combinePDF.Attachment2;
                combinePDFRequest.assets.Add(asset);
            }
            if (!string.IsNullOrEmpty(combinePDF.Attachment3))
            {
                asset = new Assete();
                asset.assetID = combinePDF.Attachment3;
                combinePDFRequest.assets.Add(asset);
            }
            if (!string.IsNullOrEmpty(combinePDF.Attachment4))
            {
                asset = new Assete();
                asset.assetID = combinePDF.Attachment4;
                combinePDFRequest.assets.Add(asset);
            }
            if (!string.IsNullOrEmpty(combinePDF.Attachment4))
            {
                asset = new Assete();
                asset.assetID = combinePDF.Attachment5;
                combinePDFRequest.assets.Add(asset);
            }

            ResponseModel resCombinepdf = new ResponseModel();
            if (combinePDFRequest.assets.Count > 1)
            {
                resCombinepdf = await CombinePDF(combinePDFRequest);
            }
            else
            {
                resCombinepdf.Status = false;
                resCombinepdf.StatusCode = HttpStatusCode.NoContent;
            }

            if (resCombinepdf != null && resCombinepdf.Data != null && resCombinepdf.StatusCode == HttpStatusCode.OK)
            {
                string requiredPath = string.Empty;
                HtmlFileResponseModel htmlFileResponse = new HtmlFileResponseModel();

                UploadPdfFile uploadPdf = new UploadPdfFile();
                uploadPdf.PdfUrl = resCombinepdf.Data.ToString();
                uploadPdf.FileName = combinePDF.TemplateName;
                var respPDF = await UploadPdfFileFromBlob(uploadPdf);
                response.Data = respPDF.Data;
                response.Status = true;
                response.StatusCode = HttpStatusCode.OK;
            }

            return response;
        }
        public async Task<ResponseModel> CombinePDF(CombinedPDFRequestModel combinedPDFRequestModel)
        {
            ResponseModel responseModel = new ResponseModel();

            string defaultString = string.Empty;
            string x_api_key = _configuration["AppSettingsAdobe:adobe_client_id"] ?? defaultString;
            string combinepdf_AdobeURL = _configuration["AppSettingsAdobe:CombinePDF"] ?? defaultString;
            var full_authorize = await GetAccessToken();
            if (full_authorize.StatusCode == HttpStatusCode.OK)
            {
                var serial = JsonConvert.SerializeObject(full_authorize.Data);
                var authorize_token = JsonConvert.DeserializeObject<AccessTokenResponse>(serial);
                var authorize = authorize_token?.access_token;
                var serial_JSON = JsonConvert.SerializeObject(combinedPDFRequestModel);
                var Clients = new RestClient(combinepdf_AdobeURL);
                var combineRequest = new RestRequest(Method.POST);
                combineRequest.AddHeader("Authorization", "Bearer " + authorize);
                combineRequest.AddHeader("x-api-key", x_api_key);
                combineRequest.AddHeader("Content-Type", "application/json");
                combineRequest.AddParameter("application/json", serial_JSON, ParameterType.RequestBody);
                IRestResponse combineResponse = await Clients.ExecuteAsync(combineRequest);
                if (combineResponse.StatusCode == HttpStatusCode.Created)
                {
                    for (int i = 0; i < combineResponse.Headers.Count; i++)
                    {
                        if (combineResponse.Headers[i].Name == "Location")
                        {
                            var location = combineResponse.Headers[i].Name == "Location" ? combineResponse.Headers[i].Value?.ToString() : string.Empty;
                            var resData = await GetCombinedPDFUrl(location, authorize);
                            return resData;
                        }
                    }
                }
                else
                {
                    responseModel.ErrorMessage = combineResponse.Content;
                    responseModel.StatusCode = full_authorize.StatusCode;
                }
            }
            else
            {
                return full_authorize;
            }

            return responseModel;
        }
        private async Task<ResponseModel> GetCombinedPDFUrl(string combinePDFLocation, string authorize)
        {
            ResponseModel responseModel = new ResponseModel();

            if (!string.IsNullOrEmpty(combinePDFLocation) && !string.IsNullOrEmpty(authorize))
            {
                string defaultString = string.Empty;
                string x_api_key = _configuration["AppSettingsAdobe:adobe_client_id"] ?? defaultString;
                var Clients = new RestClient(combinePDFLocation);
                var statusRequest = new RestRequest(Method.GET);
                statusRequest.AddHeader("Authorization", "Bearer " + authorize);
                statusRequest.AddHeader("x-api-key", x_api_key);
                statusRequest.AddHeader("Content-Type", "application/json");
                IRestResponse statusResponse = await Clients.ExecuteAsync(statusRequest);
                if (statusResponse.StatusCode == HttpStatusCode.OK)
                {
                    var res = statusResponse.Content;
                    var deserial_Response = JsonConvert.DeserializeObject<CombinedPDFResponseModel>(res);
                    if (deserial_Response?.status.ToLower() == "in progress")
                    {
                        var data = await GetCombinedPDFUrl(combinePDFLocation, authorize);
                        responseModel = data;
                    }
                    else
                    {
                        responseModel.Data = deserial_Response?.asset.downloadUri;
                        responseModel.PdfUrl = deserial_Response?.asset.assetID ?? string.Empty;
                        responseModel.StatusCode = HttpStatusCode.OK;
                    }
                }
                else
                {
                    responseModel.ErrorMessage = statusResponse.Content;
                    responseModel.StatusCode = statusResponse.StatusCode;
                }
            }
            else
            {
                string? errorMessage = _resourceManager.GetString("CombinePDFErrorMessage");
                responseModel.ErrorMessage = errorMessage;
                responseModel.StatusCode = HttpStatusCode.InternalServerError;
            }

            return responseModel;
        }
        public async Task<ResponseModel> UploadPdfFileFromBlob(UploadPdfFile uploadPdfFile)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                if (!string.IsNullOrEmpty(uploadPdfFile.PdfUrl) && !string.IsNullOrEmpty(uploadPdfFile.FileName))
                {
                    string accountName = _configuration["BloBStorageConfig:AzureAccountName"] ?? string.Empty;
                    string connectionString = _configuration["ConnectionStrings:AzureBlobStorage"] ?? string.Empty;
                    string accountKey = _configuration["BloBStorageConfig:AzureAccountKey"] ?? string.Empty;
                    string containerName = _configuration["BloBStorageConfig:AzureContainer_Html"] ?? string.Empty;
                    string fileName = uploadPdfFile.FileName + ".pdf";
                    byte[] dataByte;
                    using (WebClient webClient = new WebClient())
                    {
                        dataByte = webClient.DownloadData(uploadPdfFile.PdfUrl);
                    }
                    var storageCredentials = new StorageCredentials(accountName, accountKey);
                    var storageAccount = new CloudStorageAccount(storageCredentials, true);
                    var connString = storageAccount.ToString(true);
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                    var container = blobClient.GetContainerReference(containerName).GetBlockBlobReference(fileName);
                    container.Properties.ContentType = "application/pdf";

                    CloudBlockBlob blob = container;
                    blob.Properties.ContentType = "application/pdf";
                    string timezone = "UTC+03:00";
                    blob.Metadata["Timezone"] = timezone;

                    await blob.DeleteIfExistsAsync();

                    await blob.UploadFromByteArrayAsync(dataByte, 0, dataByte.Length);
                    BlobSasTokenRequest request = new BlobSasTokenRequest();
                    request.AccountKey = accountKey;
                    request.AccountName = accountName;
                    request.ConnectionString = connectionString;
                    request.ContainerName = containerName;
                    request.FileName = fileName;

                    responseModel = await GetFileFromBlob(request);
                }
                else
                {
                    responseModel.ErrorMessage = "Pdf url is null or engagement letter name is null";
                    responseModel.StatusCode = HttpStatusCode.InternalServerError;
                }
            }
            catch (Exception ex)
            {
                responseModel.StatusCode = HttpStatusCode.InternalServerError;
                responseModel.ErrorMessage = "";
            }
            return responseModel;
        }
        public async Task<ResponseModel> UploadHtmlFileFromBlob(UploadHtmlFile uploadHtmlFile)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                if (!string.IsNullOrEmpty(uploadHtmlFile.Base64Html) && !string.IsNullOrEmpty(uploadHtmlFile.FileName))
                {
                    string accountName = _configuration["BloBStorageConfig:AzureAccountName"] ?? string.Empty;
                    string connectionString = _configuration["ConnectionStrings:AzureBlobStorage"] ?? string.Empty;
                    string accountKey = _configuration["BloBStorageConfig:AzureAccountKey"] ?? string.Empty;
                    string containerName = _configuration["BloBStorageConfig:AzureContainer_Html"] ?? string.Empty;
                    string fileName = uploadHtmlFile.FileName + ".html";

                    string encodedStr = string.Empty;
                    if (uploadHtmlFile.IsNotToDecode == true)
                    {
                        encodedStr = uploadHtmlFile.Base64Html;
                    }
                    else
                    {
                        encodedStr = Base64UrlEncoder.Decode(uploadHtmlFile.Base64Html);
                    }

                    var storageCredentials = new StorageCredentials(accountName, accountKey);
                    var storageAccount = new CloudStorageAccount(storageCredentials, true);
                    var connString = storageAccount.ToString(true);
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                    var container = blobClient.GetContainerReference(containerName).GetBlockBlobReference(fileName);
                    CloudBlockBlob blob = container;
                    //blob.Metadata["SetAccessPolicy"] = Azure.Storage.Blobs.Models.PublicAccessType.Blob.ToString();
                    blob.Properties.ContentType = "text/html";
                    string timezone = "UTC+03:00";
                    blob.Metadata["Timezone"] = timezone;

                    await blob.DeleteIfExistsAsync();

                    await blob.UploadTextAsync(encodedStr);
                    BlobSasTokenRequest request = new BlobSasTokenRequest();
                    request.AccountKey = accountKey;
                    request.AccountName = accountName;
                    request.ConnectionString = connectionString;
                    request.ContainerName = containerName;
                    request.FileName = fileName;

                    responseModel = await GetFileFromBlob(request);
                }
                else
                {
                    string? errorMessage = _resourceManager.GetString("BlobfromHtmlErrorMessage");
                    responseModel.ErrorMessage = errorMessage;
                    responseModel.StatusCode = HttpStatusCode.InternalServerError;
                }
            }
            catch (Exception ex)
            {
                responseModel.StatusCode = HttpStatusCode.InternalServerError;
                responseModel.ErrorMessage = "";
            }
            return responseModel;
        }
        public async Task<ResponseModel> GetFileFromBlob(BlobSasTokenRequest blobSasTokenRequest)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                CloudStorageAccount backupStorageAccount = CloudStorageAccount.Parse(blobSasTokenRequest.ConnectionString);
                var backupBlobClient = backupStorageAccount.CreateCloudBlobClient();
                var backupContainer = backupBlobClient.GetContainerReference(blobSasTokenRequest.ContainerName);

                CloudBlockBlob blobFile = backupContainer.GetBlockBlobReference(blobSasTokenRequest.FileName);
                BlobSasBuilder blobSasBuilder = new BlobSasBuilder()
                {
                    BlobContainerName = blobSasTokenRequest.ContainerName,
                    BlobName = blobSasTokenRequest.FileName,
                    ExpiresOn = DateTime.UtcNow.AddYears(1),//Let SAS token expire after 5 minutes.
                };
                blobSasBuilder.SetPermissions(BlobSasPermissions.Read);//User will only be able to read the blob and it's properties
                var sasToken = blobSasBuilder.ToSasQueryParameters(new StorageSharedKeyCredential(blobSasTokenRequest.AccountName, blobSasTokenRequest.AccountKey)).ToString();
                var sasUrl = blobFile.Uri.AbsoluteUri + "?" + sasToken;
                responseModel.Data = sasUrl;
                responseModel.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                responseModel.StatusCode = HttpStatusCode.InternalServerError;
                responseModel.ErrorMessage = "";
            }

            return responseModel;
        }
        public async Task<BulkLetterEditAttachmentsResponseModel> GetBulkLetterAttachmentById(int batchId, int templateId)
        {
            return await _bulkLettersRepository.GetBulkLetterAttachmentById(batchId, templateId);
        }
        public async Task<List<AttachmentBase64String>> GetAttachmentsList(int batchId, int templateId)
        {
            List<AttachmentBase64String> attachmentBase64s = new List<AttachmentBase64String>();
            var getbulkAttachments = await _bulkLettersRepository.GetBulkAttachments(batchId, templateId);
            if (getbulkAttachments != "[]" && getbulkAttachments != string.Empty && getbulkAttachments != null)
            {
                var deserialJSON = JsonConvert.DeserializeObject<List<AttachmentBase64String>>(getbulkAttachments);
                if (deserialJSON != null)
                {
                    attachmentBase64s = deserialJSON;
                }
            }
            else
            {
                var gettemplateAttachments = await _bulkLettersRepository.GetTemplateAttachments(templateId);
                if(!string.IsNullOrEmpty(gettemplateAttachments))
                {
                    var deserialJSON = JsonConvert.DeserializeObject<List<AttachmentBase64String>>(gettemplateAttachments);
                    if(deserialJSON != null)
                    {
                        attachmentBase64s = deserialJSON;
                    }
                }
            }
            return attachmentBase64s;
        }

        public async Task<bool> DeleteByBulkLetterId(DeleteBulkLetterResModel clientId)
        {
            return await _bulkLettersRepository.DeleteByBulkLetterId(clientId);
        }

        public async Task<bool> DeleteByBatchId(DeleteByBatchResModel batchId)
        {
            return await _bulkLettersRepository.DeleteByBatchId(batchId);
        }
        public async Task<BulkLetterFieldsResponseModel> InsertFieldsList(BulkLetterFieldsResponseModel fieldsList)
        {
            BulkLetterFieldsResponseModel bulkLetterFields = new BulkLetterFieldsResponseModel();
            var respFieldListInsert = await _bulkLettersRepository.InsertFieldsList(fieldsList);
            return bulkLetterFields;
        }

        public async Task<List<PartnerOfficeResponse>> GetPartnerOfficeListAsync(int batchId)
        {
            return await _bulkLettersRepository.GetPartnerOfficeListAsync(batchId);
        }
        public async Task<EditFieldValuesResponse> GetBulkLetterFieldsById(int bulkLetterId)
        {
            return await _bulkLettersRepository.GetBulkLetterFieldsById(bulkLetterId);
        }
        public async Task<bool> SaveEditFields(EditFieldValuesResponse editField)
        {
            return await _bulkLettersRepository.SaveEditFields(editField);
        }
        public async Task<ResponseModel> AddBatchRequestDetails(BatchRequestResponse batchRequest)
        {
            ResponseModel response = new ResponseModel();
            bool isExistsBatchId = await _bulkLettersRepository.IsBatchrequestIdExists(batchRequest.BatchId);
            if (!isExistsBatchId)
            {
                var resp = await _bulkLettersRepository.AddBatchRequestDetails(batchRequest);
                response.Data = resp;
                response.Status = true;
                response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Data = null;
                response.Status = false;
                response.StatusCode = HttpStatusCode.Ambiguous;
            }
            return response;
        }
        public async Task<string> Get7216FormAssetId(BulkLettersCombinePdfViewModel viewModel)
        {
            string response = string.Empty;
            var accessToken = await GetAccessToken();
            if (accessToken != null && accessToken.StatusCode == HttpStatusCode.OK && accessToken.Data != null)
            {
                var serial = JsonConvert.SerializeObject(accessToken.Data);
                var authorize_token = JsonConvert.DeserializeObject<AccessTokenResponse>(serial);
                string authorize = authorize_token?.access_token ?? string.Empty;
                if (viewModel != null)
                {
                    int getById = 0;
                    bool isBulkLetterId = false;
                    if (viewModel.BulkLettersId > 0)
                    {
                        getById = viewModel.BulkLettersId;
                        isBulkLetterId = true;
                    }
                    else
                    {
                        getById = viewModel.TemplateId;
                        isBulkLetterId = false;
                    }
                    var getTemplate = await _bulkLettersRepository.GetTemplateForIndv(getById, isBulkLetterId);
                    if (getTemplate.Data != null)
                    {
                        int? getClientSignatureCount = 0;
                        bool? get7216Availability = false;
                        if(isBulkLetterId)
                        {
                            var serialResp = JsonConvert.SerializeObject(getTemplate.Data);
                            var deSerialResp = JsonConvert.DeserializeObject<BulkLetters>(serialResp);
                            get7216Availability = deSerialResp.Is7216Available;
                            getClientSignatureCount = deSerialResp.ClientSignatureCount;
                        }
                        else
                        {
                            var serialResp = JsonConvert.SerializeObject(getTemplate.Data);
                            var deSerialResp = JsonConvert.DeserializeObject<Template>(serialResp);
                            get7216Availability = deSerialResp.Is7216Available;
                            getClientSignatureCount = deSerialResp.ClientSignatureCount;
                        }
                        
                        if (get7216Availability == true)
                        {
                            ResponseModel dataRes = new ResponseModel();
                            if (getClientSignatureCount == 1)
                            {
                                dataRes = await GetFileFromBlob(1);
                            }
                            else if (getClientSignatureCount == 2)
                            {
                                dataRes = await GetFileFromBlob(2);
                            }

                            if (dataRes != null && dataRes.StatusCode == HttpStatusCode.OK && dataRes.Data != null)
                            {
                                string consentFormPath = dataRes?.Data?.ToString() ?? string.Empty;
                                if (!string.IsNullOrEmpty(consentFormPath))
                                {
                                    using (HttpClient client = new HttpClient())
                                    {
                                        HttpResponseMessage responseUrl = await client.GetAsync(consentFormPath);
                                        if (responseUrl.IsSuccessStatusCode)
                                        {
                                            string htmlContent = await responseUrl.Content.ReadAsStringAsync();

                                            int form7216Taxyear = Convert.ToInt32(_configuration["Form7216_TaxYear"]);
                                            var year = 2013 + form7216Taxyear;
                                            string get7216ExpiryDate = "December 31, " + year;

                                            string modifiedContent = htmlContent.Replace("{{consentDate}}", "December 31, " + year);
                                            modifiedContent = modifiedContent.Replace("##ClientId##", viewModel.ClientId.ToString());
                                            modifiedContent = modifiedContent.Replace("##ClientName##", viewModel?.ClientName?.ToString());
                                            UploadHtmlFile uploadHtmlFile = new UploadHtmlFile();

                                            uploadHtmlFile.Base64Html = modifiedContent;
                                            uploadHtmlFile.FileName = viewModel?.TemplateName + "_Form7216";
                                            uploadHtmlFile.IsNotToDecode = true;
                                            var uploadRes = await UploadHtmlFileFromBlob(uploadHtmlFile);
                                            if (uploadRes != null && uploadRes.StatusCode == HttpStatusCode.OK && uploadRes.Data != null)
                                            {
                                                PdfConversionRequest pdfConversionRequest = new PdfConversionRequest();
                                                var pdfConversion = new PdfConversion();

                                                pdfConversion.inputUrl = uploadRes?.Data?.ToString() ?? string.Empty;
                                                pdfConversion.includeHeaderFooter = true;
                                                pdfConversion.pageLayout = new PageLayout { pageWidth = "11", pageHeight = "8.5" };
                                                pdfConversion.json = "{}";

                                                pdfConversionRequest.pdfConversion = pdfConversion;

                                                var pdfResponse = await HtmltoPDFConvert(pdfConversionRequest, authorize);
                                                if (pdfResponse.StatusCode == HttpStatusCode.OK && pdfResponse.Data != null)
                                                {
                                                    response = pdfResponse?.PdfUrl?.ToString() ?? string.Empty;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return response;
        }
        public async Task<ResponseModel> GetFileFromBlob(int signerCount)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                var ContainerName = _configuration["ContainerName"] ?? string.Empty;
                var FileName1 = _configuration["FileName1"] ?? string.Empty;
                var FileName2 = _configuration["FileName2"] ?? string.Empty;
                var AccountName = _configuration["BloBStorageConfig:AzureAccountName"] ?? string.Empty;
                var AccountKey = _configuration["BloBStorageConfig:AzureAccountKey"] ?? string.Empty;
                string blobConnectionString = _configuration["ConnectionStrings:AzureBlobStorage"] ?? string.Empty;

                CloudStorageAccount backupStorageAccount = CloudStorageAccount.Parse(blobConnectionString);
                var backupBlobClient = backupStorageAccount.CreateCloudBlobClient();
                var backupContainer = backupBlobClient.GetContainerReference(ContainerName);

                if (signerCount == 1)
                {
                    CloudBlockBlob blobFile = backupContainer.GetBlockBlobReference(FileName1);
                    BlobSasBuilder blobSasBuilder = new BlobSasBuilder()
                    {
                        BlobContainerName = ContainerName,
                        BlobName = FileName1,
                        ExpiresOn = DateTime.UtcNow.AddYears(1),//Let SAS token expire after 5 minutes.
                    };
                    blobSasBuilder.SetPermissions(BlobSasPermissions.Read);//User will only be able to read the blob and it's properties
                    var sasToken = blobSasBuilder.ToSasQueryParameters(new StorageSharedKeyCredential(AccountName, AccountKey)).ToString();
                    var sasUrl = blobFile.Uri.AbsoluteUri + "?" + sasToken;
                    responseModel.Data = sasUrl;
                    responseModel.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    CloudBlockBlob blobFile = backupContainer.GetBlockBlobReference(FileName2);
                    BlobSasBuilder blobSasBuilder = new BlobSasBuilder()
                    {
                        BlobContainerName = ContainerName,
                        BlobName = FileName2,
                        ExpiresOn = DateTime.UtcNow.AddYears(1),//Let SAS token expire after 5 minutes.
                    };
                    blobSasBuilder.SetPermissions(BlobSasPermissions.Read);//User will only be able to read the blob and it's properties
                    var sasToken = blobSasBuilder.ToSasQueryParameters(new StorageSharedKeyCredential(AccountName, AccountKey)).ToString();
                    var sasUrl = blobFile.Uri.AbsoluteUri + "?" + sasToken;
                    responseModel.Data = sasUrl;
                    responseModel.StatusCode = HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                responseModel.StatusCode = HttpStatusCode.InternalServerError;
                responseModel.ErrorMessage = "";
            }

            return responseModel;
        }
        public async Task<ResponseModel> HtmltoPDFConvert(PdfConversionRequest pdfConversionRequest, string accessToken)
        {
            pdfConversionRequest.pdfConversion.inputUrl = pdfConversionRequest.pdfConversion.inputUrl.Replace(" ", "%20");
            ResponseModel responseModel = new ResponseModel();

            string defaultString = string.Empty;
            string x_api_key = _configuration["AppSettingsAdobe:adobe_client_id"] ?? defaultString;
            string htmltopdf_AdobeURL = _configuration["AppSettingsAdobe:HtmltoPDFConvert"] ?? string.Empty;

            var Client = new RestClient(htmltopdf_AdobeURL);
            var jsonData = JsonConvert.SerializeObject(pdfConversionRequest.pdfConversion);
            var request = new RestRequest(Method.POST);

            Random generator = new Random();
            int xrequestid = Convert.ToInt32(generator.Next(1, 1000000).ToString("D6"));

            request.AddHeader("Authorization", "Bearer " + accessToken);
            request.AddHeader("x-api-key", x_api_key);
            request.AddHeader("x-request-id", xrequestid.ToString());
            request.AddParameter("application/json", jsonData, ParameterType.RequestBody);

            IRestResponse Fileresponse = await Client.ExecuteAsync(request);
            if (Fileresponse.StatusCode == HttpStatusCode.Created)
            {
                var dataRes = await GetPdfStatus(xrequestid, accessToken);
                responseModel = dataRes;
            }
            else if (Convert.ToInt32(Fileresponse.StatusCode) == 429)
            {
                responseModel.ErrorMessage = "Maximum API limit is reached, hence letter cant be submitted for signing, please contact the IT Helpdesk team to proceed further";
                responseModel.Status = false;
                responseModel.StatusCode = Fileresponse.StatusCode;
            }
            else
            {
                responseModel.Status = false;
                responseModel.StatusCode = Fileresponse.StatusCode;
            }

            return responseModel;
        }

        public async Task<bool> RemoveClientData(RemoveClientDataResModel clientId)
        {
            return await _bulkLettersRepository.RemoveClientData(clientId);
        }
    }
}
