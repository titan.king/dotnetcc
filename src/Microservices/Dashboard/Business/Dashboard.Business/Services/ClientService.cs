﻿using AutoMapper;
using Azure.Storage;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Azure.Storage.Sas;
using Dashboard.Business.Interfaces;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.Clients;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;
using Engage3.Common.Business.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;

namespace Dashboard.Business.Services
{
    public class ClientService : IClientService
    {
        private readonly IClientRepository _clientRepository;
        private readonly IMapper _mapper;
        private readonly Microsoft.AspNetCore.Hosting.IHostingEnvironment _environment;
        private readonly IExcelImport _excelimport;
        private readonly IConfiguration _configuration;
        private readonly IGetCurrentDatetime _getCurrentDateTime;
        public ClientService(IClientRepository clientRepository, IMapper mapper, Microsoft.AspNetCore.Hosting.IHostingEnvironment environment, IExcelImport importExcel, IConfiguration configuration, IGetCurrentDatetime getCurrentDateTime)
        {
            _clientRepository = clientRepository;
            _mapper = mapper;
            _environment = environment;
            _excelimport = importExcel;
            _configuration = configuration;
            _getCurrentDateTime = getCurrentDateTime;
        }
        public async Task<bool> SaveClientData(List<ClientData> clientdata)
        {
            return await _clientRepository.SaveClientData(clientdata);
        }
        public async Task<IEnumerable<t_engletterdata>> GetPartnersNameAsync()
        {
            return await _clientRepository.GetPartnersNameAsync();
        }
        public async Task<LetterFieldValuesList> FetchingClientAsync(PartnerRequest partnerRequest)
        {
            LetterFieldValuesBase letterFieldValuesBase = new LetterFieldValuesBase();
            LetterFieldValuesList letterFieldValuesList = new LetterFieldValuesList();
            List<BulkLettersResponse> bulkEngagmentLetters = new List<BulkLettersResponse>();

            letterFieldValuesBase = await _clientRepository.FetchingClientAsync(partnerRequest);

            IEnumerable<BulkLetters> bulkLetters = _mapper.Map<IEnumerable<BulkLetters>>(letterFieldValuesBase.BulkLettersResponse);

            int batchId = 0;
            if (partnerRequest.IsNewBulk)
            {
                BulkLetters bulkDataBatch = await _clientRepository.CheckExistingBatchId(partnerRequest.BatchId);
                if (bulkDataBatch.BatchId > 0)
                {
                    batchId = bulkDataBatch.BatchId + 1;
                }
            }
            if (batchId == 0)
            {
                batchId = partnerRequest.BatchId;
            }
            var data = await _clientRepository.SaveFetchingLetterAsync(bulkLetters.ToList(), batchId, partnerRequest);

            bulkEngagmentLetters = _mapper.Map<List<BulkLettersResponse>>(data);
            if (bulkEngagmentLetters.Any())
            {
                var bulkLetterIds = bulkEngagmentLetters.Select(a => a.BulkLettersId).ToList();
                string errorClients = await _clientRepository.CheckFieldValues(bulkLetterIds);
                letterFieldValuesList.ClientIdErrorList = errorClients;
            }

            letterFieldValuesList.BulkLettersResponse = bulkEngagmentLetters;
            letterFieldValuesList.GetCurrentDate = DateTime.Now.Date.ToString("yyyy-MM-dd");

            if (letterFieldValuesBase.BulkLettersResponse == null)
            {
                List<string> stringList = partnerRequest.ReturnTpeCode.Select(model => model.ReturnType).ToList();
                var result = string.Join(",", stringList);
                letterFieldValuesBase.ReturnTypeCodeError = result;
            }
            else
            {
                letterFieldValuesList.ReturnTypeCodeError = letterFieldValuesBase.ReturnTypeCodeError;
            }



            return letterFieldValuesList;
        }

        public async Task<ExportDataResponse> ExportDataAsync(ExportDataRequest exportDataRequest)
        {
            return await _clientRepository.ExportDataAsync(exportDataRequest);
        }
        public async Task<BulkDraftLetterResponse> ImportData(IFormFile file)
        {
            BulkDraftLetterResponse bulkDraftLetterResponse = new BulkDraftLetterResponse();
            string accountName = _configuration["BloBStorageConfig:AzureAccountName"] ?? string.Empty;
            string connectionString = _configuration["ConnectionStrings:AzureBlobStorage"] ?? string.Empty;
            string accountKey = _configuration["BloBStorageConfig:AzureAccountKey"] ?? string.Empty;
            string containerName = _configuration["BloBStorageConfig:AzureContainer_Html"] ?? string.Empty;
            string saasurlexpiry = _configuration["BloBStorageConfig:SaasUrlExpiry"] ?? string.Empty;

            double saasurlexpirydays = Double.Parse(saasurlexpiry);

            BlobServiceClient blobServiceClient = new BlobServiceClient(connectionString);
            BlobContainerClient containerClient = blobServiceClient.GetBlobContainerClient(containerName);
            string blobName = file.FileName;
            BlobClient blobClient = containerClient.GetBlobClient(blobName);

            // Define content type and timezone settings
            BlobHttpHeaders httpHeaders = new BlobHttpHeaders
            {
                ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // Set the content type for XLSX files
            };

            BlobUploadOptions options = new BlobUploadOptions
            {
                HttpHeaders = httpHeaders,
            };

            // Define custom metadata (including timezone information)
            Dictionary<string, string> customMetadata = new Dictionary<string, string>
            {
                { "Timezone", "UTC+03:00" }
            };

            // Set the custom metadata
            options.Metadata = customMetadata;

            if (await blobClient.ExistsAsync())
            {
                await blobClient.DeleteAsync();
            }

            // Upload the file to Azure Blob Storage
            using (Stream stream = file.OpenReadStream())
            {
                await blobClient.UploadAsync(stream, true);
            }

            BlobSasTokenRequest request = new BlobSasTokenRequest();
            request.AccountKey = accountKey;
            request.AccountName = accountName;
            request.ConnectionString = connectionString;
            request.ContainerName = containerName;
            request.FileName = blobName;

            string requestLog = JsonConvert.SerializeObject(request, Formatting.Indented);
            CloudStorageAccount backupStorageAccount = CloudStorageAccount.Parse(request.ConnectionString);
            var backupBlobClient = backupStorageAccount.CreateCloudBlobClient();
            var backupContainer = backupBlobClient.GetContainerReference(request.ContainerName);

            CloudBlockBlob blobFile = backupContainer.GetBlockBlobReference(request.FileName);
            BlobSasBuilder blobSasBuilder = new BlobSasBuilder()
            {
                BlobContainerName = request.ContainerName,
                BlobName = request.FileName,
                ExpiresOn = DateTime.UtcNow.AddDays(saasurlexpirydays),
            };
            blobSasBuilder.SetPermissions(BlobSasPermissions.Read);//User will only be able to read the blob and it's properties
            var sasToken = blobSasBuilder.ToSasQueryParameters(new StorageSharedKeyCredential(request.AccountName, request.AccountKey)).ToString();
            var sasUrl = blobFile.Uri.AbsoluteUri + "?" + sasToken;

            string bloburl = sasUrl;
            // string bloburl = "https://engage3datastorage.blob.core.windows.net/engage3devcontainer/BATCHID_1410_LetterFieldDetails%20(2).xlsx?sp=r&st=2023-10-31T15:15:08Z&se=2023-10-31T23:15:08Z&sv=2022-11-02&sr=b&sig=5lEbIMeZdOgjkYPrjBfjFYd%2FRVi0a1apDHgiGfg504E%3D";

            var listObj = _excelimport.ReadFromExcel<List<ImportDataViewModel>>(bloburl, true);
            if (listObj.Any())
            {
                bulkDraftLetterResponse = await _clientRepository.SaveImportedData(listObj);
                //bulkDraftLetterResponse = await _clientRepository.GetImportBatchLettersByIdAsync(listObj.FirstOrDefault().BatchId);
            }

            return bulkDraftLetterResponse;
        }
        public async Task<IEnumerable<BatchIdsResponse>> GetBatchIdsAsync()
        {
            return await _clientRepository.GetBatchIdsAsync();
        }
        public async Task<IEnumerable<BulkLetterBatchResponse>> GetAllBatchLetterAsync()
        {
            return await _clientRepository.GetAllBatchLetterAsync();
        }
        public async Task<BulkDraftLetterResponse> GetBatchLettersByIdAsync(int batchId)
        {
            return await _clientRepository.GetBatchLettersByIdAsync(batchId);
        }
        public async Task<ResponseModel> AddClientBulkDetails(BulkLettersResponse bulkLetters)
        {
            ResponseModel response = new ResponseModel();
            if (bulkLetters.letterFieldValues.Count() > 0)
            {
                bulkLetters.letterFieldValues = await _clientRepository.AddFieldIdtoList(bulkLetters.letterFieldValues);
                var getSignatoryTitle = bulkLetters.letterFieldValues.FirstOrDefault(n => n.FieldName == "SignatoryTitle")?.FieldValue;
                var getSignatoryLastName = bulkLetters.letterFieldValues.FirstOrDefault(n => n.FieldName == "SignatoryLastName")?.FieldValue;
                var getOfficeId = bulkLetters.letterFieldValues.FirstOrDefault(n => n.FieldName == "OfficeID")?.FieldValue;
                var getTemplateData = await _clientRepository.GetAddClientData(bulkLetters.TemplateName);
                var getSignatureCount = getTemplateData.Item1;
                var get7216availablility = getTemplateData.Item2;
                int? convertOfficeIdtoInt = Convert.ToInt32(getOfficeId);
                if (getSignatureCount == 2)
                {
                    var spouseFirstName = bulkLetters.letterFieldValues.FirstOrDefault(n => n.FieldName == "SpouseFirstName")?.FieldValue;
                    var spouseLastName = bulkLetters.letterFieldValues.FirstOrDefault(n => n.FieldName == "SpouseLastName")?.FieldValue;
                    var spouseEmailId = bulkLetters.letterFieldValues.FirstOrDefault(n => n.FieldName == "SpouseEmailId")?.FieldValue;

                    bulkLetters.SpouseFirstName = spouseFirstName;
                    bulkLetters.SpouseLastName = spouseLastName;
                    bulkLetters.SpouseEmailId = spouseEmailId;
                }
                bulkLetters.OfficeId = convertOfficeIdtoInt;
                bulkLetters.SignatoryTitle = getSignatoryTitle;
                bulkLetters.SignatoryLastName = getSignatoryLastName;
                bulkLetters.IsBatchActive = true;
                bulkLetters.ClientSignatureCount = getSignatureCount;
                bulkLetters.Is7216Available = get7216availablility;
            }
            if (bulkLetters != null)
            {
                int clientId = bulkLetters.ClientId ?? 0;
                string clientName = bulkLetters.ClientName ?? string.Empty;
                int batchId = bulkLetters.BatchId;
                if (clientId != 0)
                {
                    bool isValidClient = await _clientRepository.IsNotExistingClientId(clientId, batchId);
                    bool isValidClientName = await _clientRepository.IsNotExistingClientName(clientName, batchId);
                    if (isValidClient && isValidClientName)
                    {

                        var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
                        bulkLetters.ModifiedOn = estTime;
                        var getBulkInsertResp = await _clientRepository.AddClientBulkDetails(bulkLetters);
                        if (getBulkInsertResp != null && getBulkInsertResp.ReturnTypeCode != null)
                        {
                            response.Data = getBulkInsertResp;
                            response.Status = true;
                            response.StatusCode = System.Net.HttpStatusCode.OK;
                        }
                        else
                        {
                            response.Data = getBulkInsertResp;
                            response.Status = false;
                            response.StatusCode = System.Net.HttpStatusCode.NoContent;
                            return response;
                        }
                    }
                    else
                    {
                        response.Status = false;
                        response.StatusCode = System.Net.HttpStatusCode.Ambiguous;
                        return response;
                    }
                }
            }
            return response;
        }
        public async Task<string> CheckBatchIsActive(int BatchId, string userName)
        {
            return await _clientRepository.CheckBatchIsActive(BatchId, userName);
        }
        public async Task<bool> UpdateIsBatchActive(ActiveUserRequest activeUserRequest)
        {
            return await _clientRepository.UpdateIsBatchActive(activeUserRequest);
        }
        public async Task<LetterFieldValuesList> UpdateExistingPartnersRecords(DuplicatePartnersRequest duplicatePartners)
        {
            return await _clientRepository.UpdateExistingPartnersRecords(duplicatePartners);
        }
        public async Task<List<ReturnTypeModel>> GetTypeCodeList(int templateId)
        {
            List<ReturnTypeModel> optResponse = new List<ReturnTypeModel>();
            var respRepo = await _clientRepository.GetTypeCodeList(templateId);
            if (!string.IsNullOrEmpty(respRepo))
            {
                var splitString = respRepo.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                               .Select(s => s.Trim()) // Trim spaces from each value
                               .ToArray();
                var jsonArr = splitString.Select(value => new ReturnTypeModel { ReturnType = value }).ToArray();
                if (jsonArr.Length > 0)
                {
                    string json = JsonConvert.SerializeObject(jsonArr);
                    optResponse = JsonConvert.DeserializeObject<List<ReturnTypeModel>>(json);
                    return optResponse;
                }
            }
            return optResponse;
        }
        public async Task<string> CheckFieldValues(List<int> bulkLetterIds)
        {
            return await _clientRepository.CheckFieldValues(bulkLetterIds);
        }
    }
}
