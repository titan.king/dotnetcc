﻿using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Data.DataModel;
using Dashboard.Data.Interfaces;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;
using System.Text.RegularExpressions;

namespace Dashboard.Business.Services
{
    public class FieldsService : IFieldsService
    {
        private readonly IFieldsRepository _fieldsRepository;
        private readonly IMapper _mapper;

        public FieldsService(IFieldsRepository fieldsRepository, IMapper mapper)
        {
            _fieldsRepository = fieldsRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<FieldListModel>> GetFields()
        {
            var listModel = new List<FieldListModel>();
            var fieldListRes = await _fieldsRepository.GetFields();
            listModel = _mapper.Map<List<FieldListModel>>(fieldListRes);
            return listModel;
        }
        public async Task<GetFieldsByIdList> GetFieldsById(int fieldId)
        {
            string[] values;
            List<string> blklist = new List<string>();
            string[] blockname;
            string fieldcompare1;

            var fieldById = new GetFieldsByIdList();
            GetFieldsByIdList fieldByIdResp = await _fieldsRepository.GetFieldsById(fieldId);
            if (fieldByIdResp != null)
            {
                fieldcompare1 = fieldByIdResp.FieldsListById.FirstOrDefault()?.FieldName ?? string.Empty;

                foreach (var item in fieldByIdResp.FieldsConnectedBlocks)
                {
                    item.Content = HtmlToPlainText(item.Content);
                    values = item.Content.Split(',');
                    for (int i = 0; i < values.Length; i++)
                    {
                        values[i] = values[i].Trim();

                        if (String.Equals(values[i], fieldcompare1))
                        {
                            blklist.Add(item.BlockName);
                        }
                    }

                }
            }
            blockname = blklist.ToArray();
            string BlockNames = string.Join(",", blockname);
            fieldByIdResp.FieldsListById.FirstOrDefault().ConnectedBlocks = BlockNames;


            return fieldByIdResp;
        }

        private static string HtmlToPlainText(string html)
        {
            const string tagWhiteSpace = @"(span>|$)(\W|\n|\r)+<span";//matches one or more (white space or line breaks) between '>' and '<'
            const string stripFormatting = @"<[^>]*(>|$)";//match any character between '<' and '>', even when end tag is missing
            const string lineBreak = @"<(br|BR)\s{0,1}\/{0,1}>";//matches: <br>,<br/>,<br />,<BR>,<BR/>,<BR />
            var lineBreakRegex = new Regex(lineBreak, RegexOptions.Multiline);
            var stripFormattingRegex = new Regex(stripFormatting, RegexOptions.Multiline);
            var tagWhiteSpaceRegex = new Regex(tagWhiteSpace, RegexOptions.Multiline);

            var text = html;

            var spanIndex = text.IndexOf("<span class");
            var spanClosingIndex = text.IndexOf("##</span>");
            var fieldNameFirstIndex = 0;
            var fieldNameLastIndex = 0;
            var startIndex = 0;
            var resultLst = new List<string>();

            var spanContents = string.Empty;
            while (spanIndex != -1 && spanClosingIndex != -1)
            {
                spanIndex = text.IndexOf("<span class", startIndex);
                spanClosingIndex = text.IndexOf("##</span>", startIndex);
                if ((spanIndex != -1 && spanClosingIndex != -1))
                {
                    spanContents = text.Substring(spanIndex, spanClosingIndex - spanIndex + 7);

                    fieldNameFirstIndex = spanContents.IndexOf("##");
                    fieldNameLastIndex = spanContents.LastIndexOf("##");

                    resultLst.Add(spanContents.Substring(fieldNameFirstIndex + 2, fieldNameLastIndex - fieldNameFirstIndex - 2));
                    startIndex = spanClosingIndex + 7;
                }
            }
            var list = resultLst;


            var allBlocknames = new List<String>();
            var relatedEntities = new List<String>();
            foreach (var item in list)
            {
                String itemString = item.ToString();
                allBlocknames.Add(itemString);
            }
            relatedEntities = allBlocknames.Distinct().ToList();
            string field = (string.Join(",", relatedEntities));
            return field;
        }
        public async Task<IEnumerable<FieldListModel>> GetActiveFields()
        {
            var listModel = new List<FieldListModel>();
            var activeFieldListRes = await _fieldsRepository.GetActiveFields();
            listModel = _mapper.Map<List<FieldListModel>>(activeFieldListRes);
            return listModel;
        }

        public async Task<Tuple<FieldListModel, string>> AddField(FieldListModel field)
        {
            FieldListModel result = new FieldListModel();
            var validationErrorMessage = string.Empty;
            var res = await IsValid(field);
            if (res.Item1 == true)
            {
                var maptoField = _mapper.Map<Field>(field);
                var fieldRes = await _fieldsRepository.AddField(maptoField);
                result = _mapper.Map<FieldListModel>(fieldRes);

            }
            return new Tuple<FieldListModel, string>(result, res.Item2);
        }

        public async Task<Tuple<FieldListModel, string>> UpdateField(FieldListModel fields)
        {
            FieldListModel result = new FieldListModel();
            var validationErrorMessage = string.Empty;
            var res = await IsValid(fields);
            if (res.Item1 == true)
            {
                var maptoField = _mapper.Map<Field>(fields);
                var fieldRes = await _fieldsRepository.UpdateField(maptoField);
                result = _mapper.Map<FieldListModel>(fieldRes);
            }
            return new Tuple<FieldListModel, string>(result, res.Item2);
        }

        public async Task<IEnumerable<FieldsIdListResModel>> GetFieldsIdList()
        {
            return await _fieldsRepository.GetFieldsIdList();
        }
        public async Task<DeleteFieldsViewModel> DeleteFields(DeleteFieldsViewModel deleteId)
        {
            var fieldById = new GetFieldsByIdList();
            foreach (var item in deleteId.FieldId)
            {
                fieldById = await GetFieldsById(item);
                if(fieldById?.FieldsListById?.FirstOrDefault()?.ConnectedBlocks == "")
                {                  
                    deleteId.FieldId[0] = item;                  
                    var fields = _mapper.Map<DeleteFieldResModel>(deleteId);
                    var deleteField = await _fieldsRepository.DeleteFields(fields);
                }
                else
                {
                    deleteId.IsDeleted = false; 
                }
            }
            return deleteId;
        }

        public async Task<Tuple<bool, string>> IsValid(FieldListModel Field)
        {
            var validationResult = await _fieldsRepository.IsValidFieldName(Field);
            string validationErrorMessage = string.Empty;
            if (validationResult == false)
            {
                validationErrorMessage = "Field Name already exists.";
            }

            return new Tuple<bool, string>(validationResult, validationErrorMessage);
        }

        public async Task<FieldsStatusResModel> LoadStatus()
        {
            return await _fieldsRepository.LoadStatus();
        }

        public async Task<IEnumerable<ColumnNamesResModel>> GetColumnNames()
        {
            return await _fieldsRepository.GetColumnNames();
        }
        public async Task<FieldsResponse> GetActiveClientFields(int clientId, int partnerId)
        {
            return await _fieldsRepository.GetActiveClientFields(clientId, partnerId);
        }
    }
}
