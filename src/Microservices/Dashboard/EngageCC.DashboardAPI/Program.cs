using Dashboard.Data.Context;
using EngageCC.DashboardAPI.Helpers;
using Microsoft.EntityFrameworkCore;
using System.IO.Abstractions;
using System.Resources;
using Serilog;
using Serilog.Events;

var builder = WebApplication.CreateBuilder(args);
var configuration = new ConfigurationBuilder()
       .SetBasePath(Directory.GetCurrentDirectory())
       .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
       .Build();

var blobConnectionString = configuration?.GetConnectionString("AzureBlobStorage");

builder.Services.AddSerilog();

var containerName = "engage3cclogs";

var logger = new LoggerConfiguration()          
           .WriteTo.AzureBlobStorage(
               connectionString: blobConnectionString,
               storageContainerName: containerName,
               storageFileName: "{yyyy}-{MM}-{dd}/ApplicationLogs.txt",              
               restrictedToMinimumLevel: LogEventLevel.Information)
           .CreateLogger();

Log.Logger = logger;

Log.Information("This is an information log.");
Log.Error("This is an error log.");


    var connectionString = configuration?.GetConnectionString("EngageDbConnection");

    builder.Services.AddCors(
                options => options.AddPolicy("AllowCors",
                builder =>
                {
                    builder
                    .AllowAnyOrigin()
                    .WithMethods("GET", "PUT", "POST", "DELETE")
                    .AllowAnyHeader();
                }));

    builder.Services.AddControllers().AddJsonOptions(options =>
                   options.JsonSerializerOptions.PropertyNamingPolicy = null);

    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen();


    builder.Services.AddDbContext<DashboardDBContext>(options =>
        options.UseSqlServer(connectionString));
    builder.Services.AddSingleton<BlobStorageConfiguration>();

    builder.Services.AddSingleton(provider =>
    {
        var builder = new DbContextOptionsBuilder<DashboardDBContext>();
        builder.UseSqlServer(connectionString);
        return builder.Options;
    });

    DependencyInjectionResolver.RegisterServices(builder.Services);
    DependencyInjectionResolver.RegisterRepository(builder.Services);
    builder.Services.AddAutoMapper(typeof(AutoMapperProfile));

    builder.Services.AddSingleton<ResourceManager>(ServiceProvider =>
    {
        return new ResourceManager("EngageCC.DashboardAPI.Helpers.Resource", typeof(Program).Assembly);
    });
    builder.Services.AddSingleton<IFileSystem, FileSystem>();
    var app = builder.Build();

    app.UseSwagger();
    app.UseSwaggerUI();

    app.UseRouting();
    app.UseHttpsRedirection();
    app.UseCors("AllowCors");
    app.UseAuthorization();

    app.MapControllers();

app.UseSerilogRequestLogging();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.Run();
   

