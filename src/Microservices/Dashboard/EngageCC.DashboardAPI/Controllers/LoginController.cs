﻿using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.Clients;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Login;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Filters;
using Mailjet.Client.Resources;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Identity.Client;
using Newtonsoft.Json;
using System.Net;
using System.Resources;
using System.Text;

namespace EngageCC.DashboardAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[TypeFilter(typeof(AuthorizationFilter))]

    public class LoginController : ControllerBase
    {
        private readonly string clientId = "";
        private readonly string clientSecret = "";
        private readonly string scope = "";
        private readonly string tenantId = "";
        private readonly string authority = "";

        private readonly IConfiguration _configuration;
        private readonly ILoginService _loginService;
        private readonly ILogger<LoginController> _logger;
        private readonly ResourceManager _resourceManager;
        private readonly IMapper _mapper;
      


        public LoginController(
            ILoginService loginService,
            ILogger<LoginController> logger,
            ResourceManager resourceManager,
            IMapper mapper,
            IConfiguration configuration)
        {
            _loginService = loginService;
            _logger = logger;
            _resourceManager = resourceManager;
            _mapper = mapper;
            _configuration = configuration;

            var azureAdOptions = _configuration.GetSection("AzureAd");
            tenantId = azureAdOptions["TenantId"];
            clientId = azureAdOptions["ClientId"];
            clientSecret = azureAdOptions["ClientSecret"];
            scope = azureAdOptions["Scope"];
            authority = "https://login.microsoftonline.com/" + tenantId;

        }

        [HttpPost]
        [Route("GetRoles")]
        public async Task<ResponseModel> GetRoles(LoginViewModel permissions)
        {
            ResponseModel resobj = new ResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(permissions, Formatting.Indented);
                _logger.LogInformation("Request Log for GetRoles():", DateTime.Now.ToString() + "-" + requestLog);
                LoginViewModel loginViewModel = new LoginViewModel();
                loginViewModel = await _loginService.GetRoles(permissions);
                if (loginViewModel != null)
                {
                    resobj.Data = loginViewModel;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(loginViewModel, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetRoles():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NoContent;
                }
                return resobj;
            }

            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpPost]
        [Route("GetRolesandScreenList")]
        public async Task<ResponseModel> GetRolesandScreenList(LoginViewModel loginViewModel)
        {
            ResponseModel resobj = new ResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(loginViewModel, Formatting.Indented);
                _logger.LogInformation("Request Log for GetRolesandScreenList():", DateTime.Now.ToString() + "-" + requestLog);
                var mapResult = _mapper.Map<LoginListModel>(loginViewModel);
                var permissionList = await _loginService.GetRolesandScreenList(mapResult);
                if (permissionList != null)
                {
                    resobj.Data = permissionList;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(permissionList, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetRolesandScreenList():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NoContent;
                }
                return resobj;
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [HttpGet]
        [Route("LoginAuth")]
        public async Task<ResponseModel> LoginADAuth()
        {
            ResponseModel responseModel = new ResponseModel();

            try
            {
                var confidentialClientApplication = ConfidentialClientApplicationBuilder
                    .Create(clientId)
                    .WithClientSecret(clientSecret)
                    .WithAuthority(new Uri(authority))
                    .Build();

                var scopes = new string[] { scope };

                var authResult = await confidentialClientApplication.AcquireTokenForClient(scopes).ExecuteAsync();

                var accessToken = authResult.AccessToken;
                responseModel.Data = accessToken;
                responseModel.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                string errorMsg = "Message: " + ex.Message + " StackTrace: " + ex.StackTrace + " InnerException: " + ex.InnerException;
                Console.WriteLine("Error: " + errorMsg);
                responseModel.StatusCode = HttpStatusCode.InternalServerError;
                responseModel.ErrorMessage = ex.Message;
            }


            return responseModel;
        }

        [HttpGet]
        [Route("LoginADAuthWithUser")]
        public async Task<TokenResponse> LoginADAuthWithUser(string username, string password)
        { 
            try
            {
                return await GetAccessTokenAsync(tenantId, clientId, clientSecret, username, password, scope);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private async Task<TokenResponse> GetAccessTokenAsync(string tenantId, string clientId, string clientSecret, string username, string password, string scope)
        {
            var tokenEndpoint = $"https://login.microsoftonline.com/{tenantId}/oauth2/v2.0/token";
            var body = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("client_id", clientId),
                new KeyValuePair<string, string>("scope", scope),
                new KeyValuePair<string, string>("client_secret", clientSecret),
                new KeyValuePair<string, string>("grant_type", "password"),
                new KeyValuePair<string, string>("username", username),
                new KeyValuePair<string, string>("password", password)
            };
            using (HttpClient client = new HttpClient())
            {
                var tokenRequest = new HttpRequestMessage(HttpMethod.Post, tokenEndpoint) { Content = new FormUrlEncodedContent(body) };
                var tokenResponse = await client.SendAsync(tokenRequest);
                if (tokenResponse.IsSuccessStatusCode)
                {
                    string tokenResponseContent = await tokenResponse.Content.ReadAsStringAsync();

                    var accessToken = System.Text.Json.JsonSerializer.Deserialize<TokenResponse>(tokenResponseContent);
                    return accessToken;
                }
                else
                {
                    return new TokenResponse()
                    {
                        access_token = tokenResponse.ReasonPhrase
                    };
                }
                
            } 
        }
    }

    public class TokenResponse
    {
        public string token_type { get; set; }
        public string scope { get; set; }
        public int expires_in { get; set; }
        public int ext_expires_in { get; set; }
        public string access_token { get; set; }
    }
}
