﻿using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Filters;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using System.Resources;

namespace EngageCC.DashboardAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [TypeFilter(typeof(AuthorizationFilter))]
    public class FieldsController : ControllerBase
    {
        private readonly IFieldsService _fieldsService;
        private readonly ILogger<FieldsController> _logger;
        private readonly ResourceManager _resourceManager;
        private readonly IMapper _mapper;
        public FieldsController(IFieldsService fieldsService, ResourceManager resourceManager, ILogger<FieldsController> logger, IMapper mapper)
        {
            _fieldsService = fieldsService;
            _resourceManager = resourceManager;
            _mapper = mapper;
            _logger = logger;
        }
        ResponseModel resobj = new ResponseModel();

        [HttpGet]
        [Route("GetFields")]
        public async Task<ResponseModel> GetFields()
        {
            try
            {
                var fieldsList = new List<FieldViewModel>();
                var fieldlistRes = await _fieldsService.GetFields();
                if (fieldlistRes != null)
                {
                    fieldsList = _mapper.Map<List<FieldViewModel>>(fieldlistRes);

                    resobj.Data = fieldsList;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(fieldsList, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetFields():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NoContent;
                }

                return resobj;
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }

        }

        [HttpGet]
        [Route("GetActiveFields")]
        public async Task<ResponseModel> GetActiveFields()
        {

            try
            {
                var fieldsList = new List<FieldViewModel>();
                var activeFieldList = await _fieldsService.GetActiveFields();
                if (activeFieldList != null)
                {
                    fieldsList = _mapper.Map<List<FieldViewModel>>(activeFieldList);

                    resobj.Data = fieldsList;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(fieldsList, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetActiveFields():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NoContent;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }

            return resobj;
        }

        [HttpGet]
        [Route("GetFieldsIdList")]
        public async Task<ResponseModel> GetFieldsIdList()
        {
            try
            {
                var fieldsList = new List<FieldsIdListViewModel>();
                var fieldsIdList = await _fieldsService.GetFieldsIdList();
                if (fieldsIdList != null)
                {
                    fieldsList = _mapper.Map<List<FieldsIdListViewModel>>(fieldsIdList);
                    resobj.Data = fieldsList;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(fieldsList, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetFieldsIdList():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NoContent;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }
            return resobj;
        }

        [HttpPost]
        [Route("AddField")]
        public async Task<ResponseModel> AddField(FieldViewModel field)
        {
            try
            {
                FieldViewModel obj = new FieldViewModel();
                string requestLog = JsonConvert.SerializeObject(field, Formatting.Indented);
                _logger.LogInformation("Request Log for AddField():", DateTime.Now.ToString() + "-" + requestLog);
                if (ModelState.IsValid)
                {
                    var fields = _mapper.Map<FieldListModel>(field);
                    var returnObj = await _fieldsService.AddField(fields);
                    if (returnObj != null)
                    {
                        if (returnObj.Item2 == string.Empty)
                        {
                            obj = _mapper.Map<FieldViewModel>(returnObj.Item1);
                            resobj.Data = obj;
                            resobj.Status = true;
                            resobj.StatusCode = HttpStatusCode.OK;
                            string responseLog = JsonConvert.SerializeObject(obj, Formatting.Indented);
                            _logger.LogInformation("Response Log for AddField():", DateTime.Now.ToString() + "-" + responseLog);
                        }
                        else
                        {
                            resobj.Status = false;
                            resobj.ErrorMessage = returnObj.Item2;
                            resobj.StatusCode = HttpStatusCode.Ambiguous;
                        }
                    }
                    else
                    {
                        resobj.Status = false;
                        resobj.StatusCode = HttpStatusCode.NotAcceptable;
                    }
                }
                else
                {
                    List<string> check = new List<string>();
                    foreach (var state in ModelState)
                    {
                        foreach (var error in state.Value.Errors)
                        {
                            check.Add(error.ErrorMessage);
                        }
                    }
                    resobj.ErrorMessage = string.Join(",", check);
                    resobj.StatusCode = HttpStatusCode.NoContent;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }
            return resobj;
        }

        [HttpPut]
        [Route("UpdateField")]
        public async Task<ResponseModel> UpdateField(FieldViewModel field)
        {
            try
            {
                FieldViewModel obj = new FieldViewModel();
                string requestLog = JsonConvert.SerializeObject(field, Formatting.Indented);
                _logger.LogInformation("Request Log for UpdateField():", DateTime.Now.ToString() + "-" + requestLog);
                if (ModelState.IsValid)
                {
                    var ValidationErrorMessage = string.Empty;
                    var returnObj = await _fieldsService.UpdateField(_mapper.Map<FieldListModel>(field));
                    if (returnObj != null)
                    {
                        if (returnObj.Item2 == string.Empty)
                        {
                            obj = _mapper.Map<FieldViewModel>(returnObj.Item1);
                            resobj.Data = obj;
                            resobj.Status = true;
                            resobj.StatusCode = HttpStatusCode.OK;
                            string responseLog = JsonConvert.SerializeObject(obj, Formatting.Indented);
                            _logger.LogInformation("Response Log for UpdateField():", DateTime.Now.ToString() + "-" + responseLog);
                        }
                        else
                        {
                            resobj.Status = false;
                            resobj.ErrorMessage = returnObj.Item2;
                            resobj.StatusCode = HttpStatusCode.Ambiguous;
                        }
                    }
                    else
                    {
                        resobj.Status = false;
                        resobj.StatusCode = HttpStatusCode.NotAcceptable;
                    }
                }
                else
                {
                    List<string> check = new List<string>();
                    foreach (var state in ModelState)
                    {
                        foreach (var error in state.Value.Errors)
                        {
                            check.Add(error.ErrorMessage);
                        }
                    }
                    resobj.ErrorMessage = string.Join(",", check);
                    resobj.StatusCode = HttpStatusCode.NoContent;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }

            return resobj;
        }

        [HttpPost]
        [Route("GetFieldsById")]
        public async Task<ResponseModel> GetFieldsById(FieldsGetByIdViewModel fieldId)
        {
            try
            {
                string requestLog = JsonConvert.SerializeObject(fieldId, Formatting.Indented);
                _logger.LogInformation("Request Log for GetFieldsById():", DateTime.Now.ToString() + "-" + requestLog);
                var fieldIdValue = fieldId.FieldId;
                var fieldById = new List<FieldsGetByIdViewModel>();
                var fieldsById = await _fieldsService.GetFieldsById(fieldIdValue);
                if (fieldsById != null)
                {
                    fieldById = _mapper.Map<List<FieldsGetByIdViewModel>>(fieldsById.FieldsListById);
                    resobj.Data = fieldById;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(fieldById, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetFieldsById():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NoContent;
                }
               
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }
            return resobj;
        }

        [HttpGet]
        [Route("LoadStatus")]
        public async Task<ResponseModel> LoadStatus()
        {
            try
            {
                var fieldsStatus = new FieldsStatusResModel();
                var status = await _fieldsService.LoadStatus();
                if (status != null)
                {
                    fieldsStatus = _mapper.Map<FieldsStatusResModel>(status);
                    resobj.Data = fieldsStatus;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(fieldsStatus, Formatting.Indented);
                    _logger.LogInformation("Response Log for LoadStatus():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else { resobj.Status = false; }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }
            return resobj;
        }

        [HttpPut]
        [Route("DeleteFields")]
        public async Task<ResponseModel> DeleteFields(DeleteFieldsViewModel deleteId)
        {
            try
            {
                var fieldsId = deleteId.FieldId;
                var delete = await _fieldsService.DeleteFields(deleteId);
                if (delete.IsDeleted == true)
                {
                    resobj.Data = delete;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(delete, Formatting.Indented);
                    _logger.LogInformation("Response Log for DeleteFields():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NotAcceptable;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }
            return resobj;
        }

        [HttpGet]
        [Route("GetColumnNames")]
        public async Task<ResponseModel> GetColumnNames()
        {
            try
            {
                var columnInfo = new List<ColumnNamesViewModel>();
                var columnNames = await _fieldsService.GetColumnNames();
                if (columnNames != null)
                {
                    columnInfo = _mapper.Map<List<ColumnNamesViewModel>>(columnNames);
                    resobj.Data = columnInfo;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(columnInfo, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetColumnNames():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else { resobj.Status = false; }
                resobj.Status = true;
                resobj.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }
            return resobj;
        }

        [HttpGet]
        [Route("ActiveClientFields")]
        public async Task<ResponseModel> GetActiveClientFields(int clientId, int partnerId)
        {
            try
            {
                if (clientId > 0)
                {
                    FieldsResponse fieldsRes = await _fieldsService.GetActiveClientFields(clientId, partnerId);
                    if (fieldsRes != null)
                    {
                        resobj.Data = fieldsRes;
                        resobj.Status = true;
                        resobj.StatusCode = HttpStatusCode.OK;
                        string responseLog = JsonConvert.SerializeObject(fieldsRes, Formatting.Indented);
                        _logger.LogInformation("Response Log for GetActiveClientFields():", DateTime.Now.ToString() + "-" + responseLog);
                    }
                    else
                    {
                        resobj.Status = false;
                        resobj.StatusCode = HttpStatusCode.InternalServerError;
                    }
                }
                else
                {
                    resobj.ErrorMessage = "Client id is empty";
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NoContent;
                    _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + resobj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }
            return resobj;
        }
    }
}
