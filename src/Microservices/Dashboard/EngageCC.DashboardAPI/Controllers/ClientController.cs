﻿using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Data.DataModel;
using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.Clients;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Fields;
using EngageCC.DashboardAPI.Filters;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;
using System.Net;
using System.Resources;

namespace EngageCC.DashboardAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[TypeFilter(typeof(AuthorizationFilter))]
    public class ClientController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;
        private readonly IClientService _clientService;
        private readonly ILogger<ClientController> _logger;
        private readonly ResourceManager _resourceManager;
        string ApiKey = string.Empty;
        string BaseUrl = string.Empty;
        public ClientController(IMapper mapper, IConfiguration configuration, IClientService clientService, ILogger<ClientController> Logger, ResourceManager resourceManager)
        {
            _mapper = mapper;
            _configuration = configuration;
            _clientService = clientService;
            _logger = Logger;
            _resourceManager = resourceManager;
            ApiKey = _configuration["XApiKey"] ?? string.Empty;
            BaseUrl = _configuration["ClientBaseUrl"] ?? string.Empty;
        }
        [HttpGet]
        [Route("PartnersName")]
        public async Task<ResponseModel> GetPartnersNameAsync()
        {
            ResponseModel resobj = new ResponseModel();
            try
            {
                IEnumerable<t_engletterdata> partnersNameList = await _clientService.GetPartnersNameAsync();
                IEnumerable<PartnerClientData> partnersVMs = _mapper.Map<IEnumerable<PartnerClientData>>(partnersNameList);
                if (partnersVMs != null)
                {
                    resobj.Data = partnersVMs;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(partnersVMs, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetPartnersNameAsync():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NoContent;
                }

            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                resobj.Data = ex.Message;
                resobj.Status = false;
                resobj.StatusCode = HttpStatusCode.InternalServerError;
            }
            return resobj;
        }

        [HttpPost]
        [Route("FetchingClient")]
        public async Task<ResponseModel> FetchingClientAsync(PartnerRequest partnerRequest)
        {
            ResponseModel resobj = new ResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(partnerRequest, Formatting.Indented);
                _logger.LogInformation("Request Log for FetchingClientAsync():", DateTime.Now.ToString() + "-" + requestLog);
                if (partnerRequest.BatchId > 0)
                {
                    LetterFieldValuesList batchLetters = await _clientService.FetchingClientAsync(partnerRequest);
                    if (batchLetters != null && batchLetters.BulkLettersResponse.Any())
                    {
                        resobj.Data = batchLetters;
                        resobj.Status = true;
                        resobj.StatusCode = HttpStatusCode.OK;
                        string responseLog = JsonConvert.SerializeObject(batchLetters, Formatting.Indented);
                        _logger.LogInformation("Response Log for FetchingClientAsync():", DateTime.Now.ToString() + "-" + responseLog);
                    }
                    else
                    {
                        resobj.Status = false;
                        resobj.ErrorMessage = "No client data";
                        resobj.StatusCode = HttpStatusCode.NoContent;
                        _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + resobj.ErrorMessage);
                    }
                }
                else
                {
                    resobj.Status = false;
                    resobj.ErrorMessage = "Batch id is null";
                    resobj.StatusCode = HttpStatusCode.NoContent;
                    _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + resobj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                resobj.Data = ex.Message;
                resobj.Status = false;
                resobj.StatusCode = HttpStatusCode.InternalServerError;
            }
            return resobj;
        }

        [HttpPost]
        [Route("ExportData")]
        public async Task<ResponseModel> ExportDataAsync(ExportDataRequest exportDataRequest)
        {
            ResponseModel resobj = new ResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(exportDataRequest, Formatting.Indented);
                _logger.LogInformation("Request Log for ExportData():", DateTime.Now.ToString() + "-" + requestLog);
                if (exportDataRequest.BatchId > 0)
                {
                    ExportDataResponse batchLetters = await _clientService.ExportDataAsync(exportDataRequest);
                    if (batchLetters != null)
                    {
                        resobj.Data = batchLetters;
                        resobj.Status = true;
                        resobj.StatusCode = HttpStatusCode.OK;
                        string responseLog = JsonConvert.SerializeObject(batchLetters, Formatting.Indented);
                        _logger.LogInformation("Response Log for ExportData():", DateTime.Now.ToString() + "-" + responseLog);
                    }
                    else
                    {
                        resobj.Status = false;
                        resobj.ErrorMessage = "No client data";
                        resobj.StatusCode = HttpStatusCode.NoContent;
                        _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + resobj.ErrorMessage);
                    }
                }
                else
                {
                    resobj.Status = false;
                    resobj.ErrorMessage = "Batch id is null";
                    resobj.StatusCode = HttpStatusCode.NoContent;
                    _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + resobj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                resobj.Data = ex.Message;
                resobj.Status = false;
                resobj.StatusCode = HttpStatusCode.InternalServerError;
            }
            return resobj;
        }

        [HttpPost]
        [Route("ImportData")]
        public async Task<ResponseModel> Import(IFormFile file)
        {
            ResponseModel resobj = new ResponseModel();
            try
            {
                var allowedExtensions = new[] { ".xlsx", ".xls" };
                var fileExtension = Path.GetExtension(file.FileName).ToLowerInvariant();

                if (!allowedExtensions.Contains(fileExtension))
                {
                    resobj.Status = false;
                    resobj.ErrorMessage = "Invalid file format. Please upload an Excel file (xlsx or xls).";
                    resobj.StatusCode = HttpStatusCode.NoContent;
                }

                BulkDraftLetterResponse imported = await _clientService.ImportData(file);

                if (imported != null)
                {
                    resobj.Data = imported;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("Import Error" + DateTime.Now.ToString() + "-" + ex.Message);
                resobj.ErrorMessage = ex.Message;
            }
            return resobj;
        }

        [HttpGet]
        [Route("ClientData")]
        public async Task<ResponseModel> GetClientDataAsync()
        {
            ResponseModel resobj = new ResponseModel();
            try
            {
                var clientsend1 = new RestClient(BaseUrl + "ClientDetails");

                clientsend1.Timeout = -1;
                var requestsend1 = new RestRequest(Method.GET);
                requestsend1.AddHeader("Content-Type", "application/json");
                requestsend1.AddHeader("XApiKey", ApiKey);

                IRestResponse responsesend1 = clientsend1.Execute(requestsend1);
                var adc007 = responsesend1.Content;
                var jsonString = responsesend1.Content;

                ClientsResponse? response = JsonConvert.DeserializeObject<ClientsResponse>(jsonString);

                if (response != null && response.Data != null && response.StatusCode == HttpStatusCode.OK
                    && !string.IsNullOrEmpty(response.Data.DataList.ToString()))
                {
                    List<ClientDataObject>? resList = JsonConvert.DeserializeObject<List<ClientDataObject>>(response.Data.DataList.ToString());
                    List<ClientData> resData = new List<ClientData>();
                    if (resList != null && resList.Count > 0)
                    {
                        resData = resList.Select(source => new ClientData
                        {
                            row_num = source.row_num,
                            ClientId = source.ClientId,
                            OfficeId = source.OfficeId,
                            PartnerId = source.PartnerId,
                            PartnerName = source.PartnerName,
                            OfficeName = source.Office,
                            OfficeAddress = source.OfficeAddress,
                            OfficeCity = source.OfficeCity,
                            OfficeState = source.OfficeState,
                            OfficePhoneNumber = source.PhoneNumber,
                            OfficeZipCode = source.OfficeZipCode,
                            Date = source.Date,
                            ClientName = source.ClientName,
                            Address = source.Address,
                            City = source.City,
                            State = source.State,
                            Zip = source.Zip,
                            SignatoryEmail = source.SignatoryEmail,
                            SpouseEmail = source.SpouseEmail,
                            SignatoryFirstName = source.SignatoryFirstName,
                            SignatoryLastName = source.SignatoryLastName,
                            SignatoryTitle = source.SignatoryTitle,
                            SpouseFirstName = source.SpouseFirstName,
                            SpouseLastName = source.SpouseLastName,
                            Jurisdiction = source.Jurisdiction,
                            FiscalYear = source.FiscalYear,
                            ELTemplate = source.ELTemplate,
                            Expiration7216Form = source.Expiration7216Form
                        }).ToList();
                    }

                    var saveRes = await _clientService.SaveClientData(resData);

                    resobj.Data = resData;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(resData, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetClientDataAsync():", DateTime.Now.ToString() + "-" + responseLog);
                }

            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                resobj.ErrorMessage = ex.Message;
                resobj.Status = false;
                resobj.StatusCode = HttpStatusCode.InternalServerError;
            }
            return resobj;
        }

        [HttpGet]
        [Route("BatchIds")]
        public async Task<ResponseModel> GetBatchIdsAsync()
        {
            ResponseModel resobj = new ResponseModel();
            try
            {
                IEnumerable<BatchIdsResponse> batchIdsResponse = await _clientService.GetBatchIdsAsync();
                if (batchIdsResponse.Any())
                {
                    resobj.Data = batchIdsResponse;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(batchIdsResponse, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetBatchIdsAsync():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NoContent;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                resobj.ErrorMessage = ex.Message;
                resobj.Status = false;
                resobj.StatusCode = HttpStatusCode.InternalServerError;
            }
            return resobj;
        }

        [HttpGet]
        [Route("BatchLettersById")]
        public async Task<ResponseModel> GetBatchLettersByIdAsync(int batchId)
        {
            ResponseModel resobj = new ResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(batchId, Formatting.Indented);
                _logger.LogInformation("Request Log for GetBatchLettersByIdAsync():", DateTime.Now.ToString() + "-" + requestLog);
                if (batchId > 0)
                {
                    BulkDraftLetterResponse batchIdsResponse = await _clientService.GetBatchLettersByIdAsync(batchId);
                    if (batchIdsResponse.BatchId > 0) 
                    {
                        resobj.Data = batchIdsResponse;
                        resobj.Status = true;
                        resobj.StatusCode = HttpStatusCode.OK;
                        string responseLog = JsonConvert.SerializeObject(batchIdsResponse, Formatting.Indented);
                        _logger.LogInformation("Response Log for GetBatchLettersByIdAsync():", DateTime.Now.ToString() + "-" + responseLog);
                    }
                    else
                    {
                        resobj.Data = batchIdsResponse;
                        resobj.Status = false;
                        resobj.StatusCode = HttpStatusCode.NoContent;
                        string responseLog = JsonConvert.SerializeObject(batchIdsResponse, Formatting.Indented);
                        _logger.LogInformation("Response Log for GetBatchLettersByIdAsync():", DateTime.Now.ToString() + "-" + responseLog);
                    }
                   
                }
                else
                {
                    resobj.Status = false;
                    resobj.ErrorMessage = "Batch id is null";
                    resobj.StatusCode = HttpStatusCode.NoContent;
                    _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + resobj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                resobj.ErrorMessage = ex.Message;
                resobj.Status = false;
                resobj.StatusCode = HttpStatusCode.InternalServerError;
            }
            return resobj;
        }
        [HttpGet]
        [Route("BatchLetters")]
        public async Task<ResponseModel> GetAllBatchLetterAsync()
        {
            ResponseModel resobj = new ResponseModel();
            try
            {
                IEnumerable<BulkLetterBatchResponse> batchIdsResponse = await _clientService.GetAllBatchLetterAsync();
                if (batchIdsResponse.Any())
                {
                    resobj.Data = batchIdsResponse;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(batchIdsResponse, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetAllBatchLetterAsync():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NoContent;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                resobj.ErrorMessage = ex.Message;
                resobj.Status = false;
                resobj.StatusCode = HttpStatusCode.InternalServerError;
            }
            return resobj;
        }
        [HttpPost]
        [Route("AddClientBulkDetails")]
        public async Task<ResponseModel> AddClientBulkDetails(AddClientsViewModel bulkLetters)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(bulkLetters, Formatting.Indented);
                _logger.LogInformation("Request Log for AddClientBulkDetails():", DateTime.Now.ToString() + "-" + requestLog);
                var addClientData = _mapper.Map<BulkLettersResponse>(bulkLetters);
                response = await _clientService.AddClientBulkDetails(addClientData);
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            if (response.Data != null)
            {
                string responseLog = JsonConvert.SerializeObject(response.Data, Formatting.Indented);
                _logger.LogInformation("Response Log for AddClientBulkDetails():", DateTime.Now.ToString() + "-" + responseLog);
            }
            return response;
        }

        [HttpPost]
        [Route("UpdateIsBatchActive")]
        public async Task<ResponseModel> UpdateIsBatchActiveAsync(ActiveUserRequest activeUserRequest)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(activeUserRequest, Formatting.Indented);
                _logger.LogInformation("Request Log for UpdateIsBatchActiveAsync():", DateTime.Now.ToString() + "-" + requestLog);

                bool res = await _clientService.UpdateIsBatchActive(activeUserRequest);
                if (res)
                {
                    response.Data = res;
                    response.Status = true;
                    response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Status = false;
                    response.StatusCode = HttpStatusCode.InternalServerError;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), errorMsg);

                response.ErrorMessage = ex.Message;
                response.Status = false;
                response.StatusCode = HttpStatusCode.InternalServerError;
            }
            if (response.Data != null)
            {
                string responseLog = JsonConvert.SerializeObject(response.Data, Formatting.Indented);
                _logger.LogInformation("Response Log for UpdateIsBatchActiveAsync():", DateTime.Now.ToString() + "-" + responseLog);
            }
            return response;
        }
        [HttpGet]
        [Route("EditBatchLettersById")]
        public async Task<ResponseModel> EditBatchLettersByIdAsync(int batchId, string userName)
        {
            ResponseModel resobj = new ResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(batchId, Formatting.Indented);
                _logger.LogInformation("Request Log for GetBatchLettersByIdAsync():", DateTime.Now.ToString() + "-" + requestLog);
                if (batchId > 0 && !string.IsNullOrEmpty(userName))
                {
                    string isActiveUser = await _clientService.CheckBatchIsActive(batchId, userName);

                    if (string.IsNullOrEmpty(isActiveUser))
                    {
                        BulkDraftLetterResponse batchIdsResponse = await _clientService.GetBatchLettersByIdAsync(batchId);
                        resobj.Data = batchIdsResponse;
                        resobj.Status = true;
                        resobj.StatusCode = HttpStatusCode.OK;
                        string responseLog = JsonConvert.SerializeObject(batchIdsResponse, Formatting.Indented);
                        _logger.LogInformation("Response Log for GetBatchLettersByIdAsync():", DateTime.Now.ToString() + "-" + responseLog);
                    }
                    else
                    {
                        resobj.Data = isActiveUser;
                        resobj.Status = false;
                        resobj.StatusCode = HttpStatusCode.NotModified;
                    }

                }
                else
                {
                    resobj.Status = false;
                    resobj.ErrorMessage = "Batch id is null";
                    resobj.StatusCode = HttpStatusCode.NoContent;
                    _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + resobj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                resobj.ErrorMessage = ex.Message;
                resobj.Status = false;
                resobj.StatusCode = HttpStatusCode.InternalServerError;
            }
            return resobj;
        }

        [HttpPost]
        [Route("UpdateExistingPartnersRecords")]
        public async Task<ResponseModel> UpdateExistingPartnersRecords(DuplicatePartnersRequest duplicatePartners)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(duplicatePartners, Formatting.Indented);
                _logger.LogInformation("Request Log for UpdateExistingPartnersRecords():", DateTime.Now.ToString() + "-" + requestLog);

                LetterFieldValuesList res = await _clientService.UpdateExistingPartnersRecords(duplicatePartners);
                if (res != null)
                {
                    response.Data = res;
                    response.Status = true;
                    response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Status = false;
                    response.StatusCode = HttpStatusCode.InternalServerError;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), errorMsg);

                response.ErrorMessage = ex.Message;
                response.Status = false;
                response.StatusCode = HttpStatusCode.InternalServerError;
            }
            if (response.Data != null)
            {
                string responseLog = JsonConvert.SerializeObject(response.Data, Formatting.Indented);
                _logger.LogInformation("Response Log for UpdateExistingPartnersRecords():", DateTime.Now.ToString() + "-" + responseLog);
            }
            return response;
        }
        [HttpGet]
        [Route("GetReturnType")]
        public async Task<ResponseModel> GetTypeCodeList(int templateId)
        {
            ResponseModel response = new ResponseModel();
            try
            {
               var resp = await _clientService.GetTypeCodeList(templateId);
                if(resp != null && resp.Any())
                {
                    response.Data = resp;
                    response.Status = true;
                    response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Status = false;
                    response.StatusCode = HttpStatusCode.Ambiguous;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            if (response.Data != null)
            {
                string responseLog = JsonConvert.SerializeObject(response.Data, Formatting.Indented);
                _logger.LogInformation("Response Log for AddClientBulkDetails():", DateTime.Now.ToString() + "-" + responseLog);
            }
            return response;
        }
    }
}
