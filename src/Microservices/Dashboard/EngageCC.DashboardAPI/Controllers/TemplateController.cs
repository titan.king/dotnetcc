﻿using AutoMapper;
using Azure;
using Azure.Storage;
using Azure.Storage.Blobs;
using Azure.Storage.Sas;
using Dashboard.Business.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.EngagementLetter;
using Dashboard.Models.ResponseModel;
using Dashboard.Models.Signing;
using Dashboard.Models.Template;
using EngageCC.DashboardAPI.Filters;
using EngageCC.DashboardAPI.Helpers;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Newtonsoft.Json;
using RestSharp;
using System.IO.Abstractions;
using System.Net;
using System.Reflection;
using System.Resources;
using System.Text;

namespace EngageCC.DashboardAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [TypeFilter(typeof(AuthorizationFilter))]
    public class TemplateController : ControllerBase
    {
        private readonly ITemplateService _templateService;
        private readonly ILogger<TemplateController> _logger;
        private readonly ResourceManager _resourceManager;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IConfiguration _configuration;
        private readonly IFileSystem _fileSystem;
        private readonly IWebHostEnvironment _env;
        private readonly BlobServiceClient _blobServiceClient;
        public TemplateController(ITemplateService TemplateService, ILogger<TemplateController> Logger, IConfiguration configuration, ResourceManager resourceManager, IMapper mapper, IHttpContextAccessor httpContextAccessor, IFileSystem fileSystem, IWebHostEnvironment env, BlobStorageConfiguration blobStorageConfig)
        {
            _templateService = TemplateService;
            _logger = Logger;
            _resourceManager = resourceManager;
            _mapper = mapper;
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _fileSystem = fileSystem;
            _env = env;
            _blobServiceClient = string.IsNullOrEmpty(blobStorageConfig.ConnectionString) ? null : new BlobServiceClient(blobStorageConfig.ConnectionString);
        }

        [HttpPost]
        [Route("AddTemplate")]
        public async Task<ResponseModel> AddTemplate(TemplateViewModel Template)
        {
            ResponseModel resobj = new ResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(Template, Formatting.Indented);
                _logger.LogInformation("Request Log for AddTemplate():", DateTime.Now.ToString() + "-" + requestLog);
                string templogic = "";
                List<string> values = new List<string>();
                List<string> quesvalues = new List<string>();
                List<string> blockvalus = new List<string>();
                List<string> htmlvals = new List<string>();
                templogic = Template.TemplateLogic ?? string.Empty;
                if (templogic.Contains("pages"))
                {
                    var results = JsonConvert.DeserializeObject<TemplateRootobjectViewModel>(templogic);

                    for (int i = 0; i < results?.Pages?[0].Elements?.Length; i++)
                    {
                        var pageVisibleIf = results?.Pages[0]?.Elements?[i].PageVisibleIf;
                        if (pageVisibleIf != null)
                        {
                            values.Add(pageVisibleIf);
                        }

                        var pageType = results?.Pages[0]?.Elements?[i].PageType;
                        if (pageType != null)
                        {
                            blockvalus.Add(pageType);
                        }
                    }

                    var valuess = values.Count();
                    foreach (var item in values)
                    {
                        if (item == null)
                        {
                            quesvalues.Add(item);
                        }
                    }

                    if (quesvalues.Count == valuess)
                    {
                        resobj.StatusCode = HttpStatusCode.NoContent;
                    }
                    else
                    {
                        if (ModelState.IsValid)
                        {
                            var ValidationErrorMessage = string.Empty;
                            var isValidObj = await _templateService.IsValid(Template);
                            if (isValidObj.Item1 == true && isValidObj.Item2 == string.Empty)
                            {
                                var savetemplateRes = await _templateService.SaveTemplate(Template);
                                resobj.Data = savetemplateRes;
                                resobj.StatusCode = HttpStatusCode.OK;
                                resobj.Status = true;
                                string responseLog = JsonConvert.SerializeObject(savetemplateRes, Formatting.Indented);
                                _logger.LogInformation("Response Log for AddTemplate():", DateTime.Now.ToString() + "-" + responseLog);
                            }
                            else
                            {
                                resobj.Status = isValidObj.Item1;
                                resobj.StatusCode = HttpStatusCode.NotAcceptable;
                                resobj.ErrorMessage = isValidObj.Item2;
                            }
                        }
                        else
                        {
                            var check = new List<string>();
                            foreach (var state in ModelState)
                            {
                                foreach (var error in state.Value.Errors)
                                {
                                    check.Add(error.ErrorMessage);
                                }
                            }
                            resobj.ErrorMessage = string.Join(",", check);
                            resobj.StatusCode = HttpStatusCode.BadRequest;
                        }
                    }
                }
                else
                {
                    resobj.Status = false;
                    resobj.ErrorMessage = "Template Logic requested is in Incorrect Format";
                    resobj.StatusCode = HttpStatusCode.BadRequest;
                    _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + resobj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return resobj;
        }

        [HttpPut]
        [Route("UpdateTemplate")]
        public async Task<ResponseModel> UpdateTemplate(TemplateViewModel Template)
        {
            ResponseModel resobj = new ResponseModel();
            try
            {
                {
                    string requestLog = JsonConvert.SerializeObject(Template, Formatting.Indented);
                    _logger.LogInformation("Request Log for UpdateTemplate():", DateTime.Now.ToString() + "-" + requestLog);
                    string templogic = "";
                    List<string> values = new List<string>();
                    List<string> quesvalues = new List<string>();
                    List<string> blockvalus = new List<string>();
                    List<string> htmlvals = new List<string>();
                    templogic = Template.TemplateLogic ?? string.Empty;
                    if (templogic.Contains("pages"))
                    {
                        var results = JsonConvert.DeserializeObject<TemplateRootobjectViewModel>(templogic);

                        for (int i = 0; i < results?.Pages?[0].Elements?.Length; i++)
                        {
                            var pageVisibleIf = results.Pages?[0].Elements?[i].PageVisibleIf;
                            if (pageVisibleIf != null)
                            {
                                values.Add(pageVisibleIf);
                            }

                            var pageType = results?.Pages?[0]?.Elements?[i].PageType;
                            if (pageType != null)
                            {
                                blockvalus.Add(pageType);
                            }
                        }
                        var valuess = values.Count();
                        foreach (var item in values)
                        {
                            if (item == null)
                            {
                                quesvalues.Add(item);
                            }
                        }

                        if (quesvalues.Count == valuess)
                        {
                            resobj.StatusCode = HttpStatusCode.NoContent;
                        }
                        else
                        {
                            if (ModelState.IsValid)
                            {
                                var ValidationErrorMessage = string.Empty;
                                var isValidObj = await _templateService.IsValid(Template);
                                if (isValidObj.Item1 == true && isValidObj.Item2 == string.Empty)
                                {
                                    var updateTemplateRes = await _templateService.UpdateTemplate(Template);
                                    resobj.Data = updateTemplateRes;
                                    resobj.StatusCode = HttpStatusCode.OK;
                                    resobj.Status = true;
                                    string responseLog = JsonConvert.SerializeObject(updateTemplateRes, Formatting.Indented);
                                    _logger.LogInformation("Response Log for UpdateTemplate():", DateTime.Now.ToString() + "-" + responseLog);
                                }
                                else
                                {
                                    resobj.Status = isValidObj.Item1;
                                    resobj.StatusCode = HttpStatusCode.NotAcceptable;
                                    resobj.ErrorMessage = isValidObj.Item2;
                                }
                            }

                            else
                            {
                                var check = new List<string>();
                                foreach (var state in ModelState)
                                {
                                    foreach (var error in state.Value.Errors)
                                    {
                                        check.Add(error.ErrorMessage);
                                    }
                                }
                                resobj.ErrorMessage = string.Join(",", check);
                                resobj.StatusCode = HttpStatusCode.BadRequest;
                            }
                        }
                    }
                    else
                    {
                        resobj.Status = false;
                        resobj.ErrorMessage = "Template Logic requested is in Incorrect Format";
                        resobj.StatusCode = HttpStatusCode.BadRequest;
                        _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + resobj.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return resobj;
        }

        [HttpGet]
        [Route("GetTemplatesIdList")]
        public async Task<ResponseModel> GetTemplatesIdList()
        {
            ResponseModel resobj = new ResponseModel();
            try
            {
                var templateIDList = new List<int>();
                templateIDList = (List<int>)await _templateService.GetTemplatesIdList();
                if (templateIDList != null)
                {
                    resobj.Data = templateIDList;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(templateIDList, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetTemplatesIdList():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NoContent;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return resobj;
        }

        [HttpGet]
        [Route("GetTemplates")]
        public async Task<ResponseModel> GetTemplates()
        {
            ResponseModel resobj = new ResponseModel();
            try
            {
                var templateReq = new List<TemplateViewModel>();
                var templateListRes = await _templateService.GetTemplates();
                if (templateListRes != null)
                {
                    resobj.Data = templateListRes;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(templateListRes, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetTemplates():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NoContent;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return resobj;
        }

        [HttpGet]
        [Route("GetTemplateById/{TemplateId}")]
        public async Task<ResponseModel> GetTemplateById(int TemplateId)
        {
            ResponseModel resobj = new ResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(TemplateId, Formatting.Indented);
                _logger.LogInformation("Request Log for GetTemplateById():", DateTime.Now.ToString() + "-" + requestLog);
                var templateById = new TemplateViewModel();
                templateById.TemplateId = TemplateId;
                var templateRes = _mapper.Map<TemplateListModel>(templateById);
                templateRes = await _templateService.GetTemplateById(TemplateId);
                if (templateRes != null)
                {
                    resobj.Data = templateRes;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(templateRes, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetTemplateById():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NoContent;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return resobj;
        }

        [HttpPut]
        [Route("DeleteTemplate")]
        public async Task<ResponseModel> DeleteTemplate(TemplateViewModel deleteModel)
        {
            ResponseModel resobj = new ResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(deleteModel, Formatting.Indented);
                _logger.LogInformation("Request Log for DeleteTemplate():", DateTime.Now.ToString() + "-" + requestLog);
                var response = await _templateService.DeleteTemplate(deleteModel);
                if (response == true)
                {
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    resobj.Data = response;
                    string responseLog = JsonConvert.SerializeObject(response, Formatting.Indented);
                    _logger.LogInformation("Response Log for DeleteTemplate():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NotAcceptable;
                    resobj.Data = response;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return resobj;
        }

        [HttpGet]
        [Route("GetDepartments")]
        public async Task<ResponseModel> GetDepartments()
        {
            ResponseModel resobj = new ResponseModel();
            var deptList = new List<DepartmentsViewModel>();
            try
            {
                var listResp = await _templateService.GetDepartments();
                deptList = _mapper.Map<List<DepartmentsViewModel>>(listResp);
                if (deptList != null)
                {
                    resobj.Data = deptList;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(deptList, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetDepartments():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NoContent;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return resobj;
        }

        [HttpGet]
        [Route("GetEngageTypes/{DepartmentId}")]
        public async Task<ResponseModel> GetEngageTypes(int departmentId)
        {
            ResponseModel resobj = new ResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(departmentId, Formatting.Indented);
                _logger.LogInformation("Request Log for GetEngageTypes():", DateTime.Now.ToString() + "-" + requestLog);
                var engageReqId = new EngageTypesViewModel();
                //var templateRes = _mapper.Map<IEnumerable<EngageTypesList>>(departmentId);
                var templateRes = await _templateService.GetEngageTypes(departmentId);
                if (templateRes != null)
                {
                    resobj.Data = templateRes;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(templateRes, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetEngageTypes():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NoContent;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return resobj;
        }

        [HttpGet]
        [Route("GetStatus")]
        public async Task<ResponseModel> GetStatus()
        {
            ResponseModel resobj = new ResponseModel();
            var statusList = new List<StatusViewModel>();
            try
            {
                var listResp = await _templateService.GetStatus();
                statusList = _mapper.Map<List<StatusViewModel>>(listResp);
                if (statusList != null)
                {
                    resobj.Data = statusList;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(statusList, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetStatus():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NoContent;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return resobj;
        }

        [HttpPost]
        [Route("GetPreview")]
        public async Task<ResponseModel> GetPreview(PreviewModel previewModel)
        {
            ResponseModel resobj = new ResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(previewModel, Formatting.Indented);
                _logger.LogInformation("Request Log for GetPreview():", DateTime.Now.ToString() + "-" + requestLog);
                PagedJsRequestModel pagedJsRequest = new PagedJsRequestModel();
                if (previewModel != null)
                {
                    byte[] bytes = Convert.FromBase64String(previewModel.letterHTMLPreviewContent);
                    string utf8String = Encoding.UTF8.GetString(bytes);
                    pagedJsRequest.payload = utf8String;
                    var pagedJsHtmlResponse = await _templateService.GeneratePagedJsHtml(pagedJsRequest);
                    if (pagedJsHtmlResponse.StatusCode == HttpStatusCode.OK)
                    {
                        var serial = JsonConvert.SerializeObject(pagedJsHtmlResponse.Data);
                        var pagedJsHtml = JsonConvert.DeserializeObject<PagedJsResponseModel>(serial);
                        if (pagedJsHtml != null)
                        {
                            byte[] bytess = Encoding.UTF8.GetBytes(pagedJsHtml.payload);
                            string base64String = Convert.ToBase64String(bytess);
                            resobj.Data = base64String;
                            resobj.StatusCode = HttpStatusCode.OK;
                            resobj.Status = true;
                            string responseLog = JsonConvert.SerializeObject(base64String, Formatting.Indented);
                            _logger.LogInformation("Response Log for GetPreview():", DateTime.Now.ToString() + "-" + responseLog);
                        }
                    }
                    else
                    {
                        resobj.Status = false;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return resobj;
        }

        [HttpPost]
        [Route("GetCombinedPDFURL")]
        public async Task<ResponseModel> GetCombinedPDFURL(CombinedPdfUrlModel combinedPdf)
        {
            ResponseModel resobj = new ResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(combinedPdf, Formatting.Indented);
                _logger.LogInformation("Request Log for GetCombinedPDFURL():", DateTime.Now.ToString() + "-" + requestLog);
                LetterHTMLRequestModel letterHTML = new LetterHTMLRequestModel();
                AttachmentPdfFile attachmentPdf = new AttachmentPdfFile();
                CombinePDFReq combinePDF = new CombinePDFReq();
                letterHTML.TemplateName = combinedPdf.TemplateName;
                letterHTML.LetterHtmlContent = combinedPdf.LetterHtmlContent;
                List<string> assetIDList = new List<string>();
                attachmentPdf.UploadAttachmentJSON = combinedPdf.UploadAttachmentJSON;
                var letterHTMLRes = await _templateService.UploadHTMLLetter(letterHTML);
                if (letterHTMLRes != null && letterHTMLRes.Data != null)
                {
                    string letterHTMLResponse = letterHTMLRes.Data.ToString() ?? string.Empty;
                    assetIDList.Add(letterHTMLResponse);
                    combinePDF.LetterHTMLAssetID1 = letterHTMLResponse;
                }
                var attachmentRes = await _templateService.UploadAttachment(attachmentPdf);
                if (attachmentRes != null)
                {
                    int i = 0;
                    foreach (var item in attachmentRes)
                    {
                        assetIDList.Add(item.ToString());
                        if (i == 0)
                        { combinePDF.Attachment1 = item.Replace("\"", string.Empty).Replace('"', ' ').ToString(); i++; }
                        else if (i == 1)
                        { combinePDF.Attachment2 = item.Replace("\"", string.Empty).Replace('"', ' ').ToString(); i++; }
                        else if (i == 2)
                        { combinePDF.Attachment3 = item.Replace("\"", string.Empty).Replace('"', ' ').ToString(); i++; }
                        else if (i == 3)
                        { combinePDF.Attachment4 = item.Replace("\"", string.Empty).Replace('"', ' ').ToString(); i++; }
                        else if (i == 4)
                        { combinePDF.Attachment5 = item.Replace("\"", string.Empty).Replace('"', ' ').ToString(); i++; }
                    }
                }
                combinePDF.TemplateName = combinedPdf.TemplateName;
                var combinePDFResponse = await _templateService.CombinePDFReq(combinePDF);
                if (combinePDFResponse != null)
                {
                    resobj.Data = combinePDFResponse;
                    string responseLog = JsonConvert.SerializeObject(combinePDFResponse, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetCombinedPDFURL():", DateTime.Now.ToString() + "-" + responseLog);
                }
               
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return resobj;
        }
        [HttpGet]
        [Route("GetCurrentDirectory")]
        public ResponseModel GetCurrentDirectory()
        {
            ResponseModel response = new ResponseModel();
            try
            {
                string directoryName = Directory.GetCurrentDirectory();
                response.PdfUrl = "GetCurrentDirectory: " + directoryName;
                string pathCreate = directoryName + "\\Preview";
                if (!Directory.Exists(pathCreate))
                {
                    Directory.CreateDirectory(pathCreate);
                }
                response.Data = pathCreate;
                response.PdfUrl = pathCreate;
                string responseLog = JsonConvert.SerializeObject(directoryName, Formatting.Indented);
                _logger.LogInformation("Response Log for GetCurrentDirectory():", DateTime.Now.ToString() + "-" + responseLog);
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return response;
        }
        [HttpGet]
        [Route("GetDirectory")]
        public ResponseModel GetDirectory()
        {
            ResponseModel response = new ResponseModel();
            try
            {
                //string wwwrootPath = Path.Combine(_env.WebRootPath, "PreviewPdf");
                string wwwrootPath = Path.Combine(_env.ContentRootPath, "PreviewPdf");

                // Create the folder if it doesn't exist
                if (!Directory.Exists(wwwrootPath))
                {
                    Directory.CreateDirectory(wwwrootPath);
                }

                response.Data = wwwrootPath;
                response.PdfUrl = wwwrootPath;
                string responseLog = JsonConvert.SerializeObject(wwwrootPath, Formatting.Indented);
                _logger.LogInformation("Response Log for GetDirectory():", DateTime.Now.ToString() + "-" + responseLog);
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                response.Data = ex.Message;
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return response;
        }
        [HttpPost]
        [Route("GetDirectoryCheck")]
        public async Task<ResponseModel> GetDirectoryCheck(LetterHTMLRequestModel letterHtmlContent)
        {
            ResponseModel resobj = new ResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(letterHtmlContent, Formatting.Indented);
                _logger.LogInformation("Request Log for GetDirectoryCheck():", DateTime.Now.ToString() + "-" + requestLog);
                PagedJsRequestModel pagedJsRequest = new PagedJsRequestModel();
                byte[] bytes = Convert.FromBase64String(letterHtmlContent.LetterHtmlContent);
                string utf8String = Encoding.UTF8.GetString(bytes);
                pagedJsRequest.payload = utf8String;
                PagedJsResponseModel responseContent = new PagedJsResponseModel();
                var pagedjsResp = await GeneratePagedJsHtml(pagedJsRequest);
                var serialPagedjs = JsonConvert.SerializeObject(pagedjsResp.Data);
                responseContent = JsonConvert.DeserializeObject<PagedJsResponseModel>(serialPagedjs);
                var serial = JsonConvert.SerializeObject(responseContent?.Data);
                var pagedJsHtml = JsonConvert.DeserializeObject<PagedJsResponseModel>(serial);
                if (pagedJsHtml != null && !string.IsNullOrEmpty(pagedJsHtml.payload))
                {
                    //Starting HTML to URL Process
                    var decodedStr = Base64UrlEncoder.Encode(pagedJsHtml.payload);
                    var encodedStr = Base64UrlEncoder.Decode(decodedStr);
                    byte[] testByteData = Encoding.UTF8.GetBytes(pagedJsHtml.payload);
                    string resultTestData = Encoding.UTF8.GetString(testByteData);
                    string containerName = "engage3devcontainer";
                    BlobContainerClient containerClient = _blobServiceClient.GetBlobContainerClient(containerName);
                    containerClient.CreateIfNotExists();
                    try
                    {
                        // Create the container if it doesn't exist (uncomment if needed)
                        // containerClient.CreateIfNotExists();

                        string directryName = "previewpdf";
                        string fileName = "samplehtmldeployed.html";
                        BlobClient blobClient = containerClient.GetBlobClient($"{directryName}/{fileName}");

                        using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(resultTestData)))
                        {
                            // Upload the string content to the blob
                            var resp = await blobClient.UploadAsync(stream, overwrite: true);
                            resobj.Data = "Upload Successful";
                            resobj.StatusCode = HttpStatusCode.OK;
                            resobj.Status = true;
                            string responseLog = JsonConvert.SerializeObject(resp, Formatting.Indented);
                            _logger.LogInformation("Response Log for GetDirectoryCheck():", DateTime.Now.ToString() + "-" + responseLog);
                        }

                    }
                    catch (RequestFailedException ex)
                    {
                        string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                        _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                        // Handle Azure Storage exceptions here
                        Console.WriteLine($"Azure Storage Exception: {ex.Message}");
                        resobj.Data = ex.StackTrace;
                        ModelState.AddModelError("Error: ", ex.StackTrace ?? ex.Message);
                        ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                        return new ResponseModel(HttpStatusCode.InternalServerError, ex.StackTrace?.ToString() ?? ex.Message);
                    }
                    catch (Exception ex)
                    {
                        string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                        _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                        // Handle other exceptions here
                        Console.WriteLine($"Exception: {ex.Message}");
                        resobj.Data = ex.StackTrace;
                        ModelState.AddModelError("Error: ", ex.StackTrace ?? ex.Message);
                        ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                        return new ResponseModel(HttpStatusCode.InternalServerError, ex.StackTrace?.ToString() ?? ex.Message);
                    }

                    HtmlFileResponseModel htmlFileResponse = new HtmlFileResponseModel();

                    htmlFileResponse.StatusCode = HttpStatusCode.OK;
                    htmlFileResponse.Message = "Success";
                    resobj.Data = htmlFileResponse;
                    resobj.StatusCode = HttpStatusCode.OK;
                    resobj.Status = true;
                    string responseLog1 = JsonConvert.SerializeObject(htmlFileResponse, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetDirectoryCheck():", DateTime.Now.ToString() + "-" + responseLog1);
                }
                else
                {
                    resobj.ErrorMessage = "Empty PagedJS Response";
                    resobj.StatusCode = HttpStatusCode.BadRequest;
                    _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + resobj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                resobj.Data = ex.StackTrace;
                ModelState.AddModelError("Error: ", ex.StackTrace ?? ex.Message);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.StackTrace?.ToString() ?? ex.Message);
            }
            return resobj;
        }
        [HttpPost]
        [Route("GeneratePagedJsHtml")]
        public async Task<ResponseModel> GeneratePagedJsHtml(PagedJsRequestModel pagedJsRequest)
        {
            var ree = JsonConvert.SerializeObject(pagedJsRequest);
            ResponseModel resobj = new ResponseModel();
            PagedJsResponseModel pagedJsResponseModel = new PagedJsResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(pagedJsRequest, Formatting.Indented);
                _logger.LogInformation("Request Log for GeneratePagedJsHtml():", DateTime.Now.ToString() + "-" + requestLog);
                if (!string.IsNullOrEmpty(pagedJsRequest.payload))
                {
                    string pagedJS_AdobeURL = _configuration["AppSettingsAdobe:GeneratePagedJS"] ?? string.Empty;
                    var Clients = new RestClient(pagedJS_AdobeURL);
                    var jsonData = JsonConvert.SerializeObject(pagedJsRequest);
                    var request = new RestRequest(Method.POST);
                    request.AddHeader("x-api-key", "c97c4b0d-cb47-494e-babf-f50162ca06d6");
                    request.AddHeader("Content-Type", "application/json");
                    request.AddParameter("application/json", jsonData, ParameterType.RequestBody);

                    IRestResponse pagedJsResponse = await Clients.ExecuteAsync(request);
                    if (pagedJsResponse.StatusCode == HttpStatusCode.OK)
                    {
                        var res = pagedJsResponse.Content;
                        var responseContent = JsonConvert.DeserializeObject<PagedJsResponseModel>(res);
                        if (responseContent != null)
                        {
                            HtmlDocument doc = new HtmlDocument();
                            doc.LoadHtml(responseContent.payload);
                            doc.DocumentNode.ChildNodes[1].ChildNodes[2].ChildNodes[0].InnerHtml = string.Empty;
                            doc.DocumentNode.ChildNodes[1].ChildNodes[2].ChildNodes[2].InnerHtml = string.Empty;
                            doc.DocumentNode.InnerHtml = doc.DocumentNode.OuterHtml.Replace("height: calc(100% - var(--pagedjs-footnotes-height));", "height: calc(110% - var(--pagedjs-footnotes-height));");
                            responseContent.payload = doc.DocumentNode.OuterHtml;
                            pagedJsResponseModel.Data = responseContent;
                            pagedJsResponseModel.StatusCode = HttpStatusCode.OK;
                            
                            //string searchText = "Dte_es_:signer1:date";
                            //var datas = doc.DocumentNode.ChildNodes[1].ChildNodes[2].ChildNodes[1].ChildNodes.Where(a => a.Name != "#text")?.ToArray();
                            //if (datas != null)
                            //{
                            //    List<int> indices = new List<int>();
                            //    for (int i = 0; i < datas.Length; i++)
                            //    {
                            //        if (datas[i] != null && !string.IsNullOrEmpty(datas[i].InnerHtml) && datas[i].InnerHtml.Contains(searchText))
                            //        {
                            //            indices.Add(i);
                            //        }
                            //    }
                            //    if (indices.Count() > 0)
                            //    {
                            //        HtmlNode targetNode = datas[0];
                            //        HtmlNodeCollection divNodes = targetNode.SelectNodes("//div[contains(@class, 'pagedjs_margin pagedjs_margin-top-center hasContent')]");
                            //        if (divNodes != null)
                            //        {
                            //            int count = 0;
                            //            foreach (HtmlNode divNode in divNodes)
                            //            {
                            //                if (indices.Count() > 0 && count > indices[0])
                            //                {
                            //                    // Remove the 'hasContent' class from the class attribute
                            //                    divNode.SetAttributeValue("class", divNode.GetAttributeValue("class", "").Replace("hasContent", "").Trim());
                            //                }
                            //                count++;
                            //            }
                            //        }
                            //        divNodes = targetNode.SelectNodes("//div[contains(@class, 'pagedjs_margin pagedjs_margin-top-right hasContent')]");
                            //        if (divNodes != null)
                            //        {
                            //            int count = 0;
                            //            foreach (HtmlNode divNode in divNodes)
                            //            {
                            //                if (indices.Count() > 0 && count > indices[0])
                            //                {
                            //                    // Remove the 'hasContent' class from the class attribute
                            //                    divNode.SetAttributeValue("class", divNode.GetAttributeValue("class", "").Replace("hasContent", "").Trim());
                            //                }
                            //                count++;
                            //            }
                            //        }
                            //        divNodes = targetNode.SelectNodes("//div[contains(@class, 'pagedjs_margin pagedjs_margin-top-left hasContent')]");
                            //        if (divNodes != null)
                            //        {
                            //            int count = 0;
                            //            foreach (HtmlNode divNode in divNodes)
                            //            {
                            //                if (count == 0)
                            //                {
                            //                    // Remove the 'hasContent' class from the class attribute
                            //                    divNode.SetAttributeValue("class", divNode.GetAttributeValue("class", "").Replace("hasContent", "").Trim());
                            //                }
                            //                if (indices.Count() > 0 && count > indices[0])
                            //                {
                            //                    // Remove the 'hasContent' class from the class attribute
                            //                    divNode.SetAttributeValue("class", divNode.GetAttributeValue("class", "").Replace("hasContent", "").Trim());
                            //                }
                            //                count++;
                            //            }
                            //        }
                            //    }
                            //    doc.DocumentNode.InnerHtml = doc.DocumentNode.OuterHtml.Replace("height: calc(100% - var(--pagedjs-footnotes-height));", "height: calc(110% - var(--pagedjs-footnotes-height));");
                            //    responseContent.payload = doc.DocumentNode.OuterHtml;
                            //    pagedJsResponseModel.Data = responseContent;
                            //    pagedJsResponseModel.StatusCode = HttpStatusCode.OK;
                            //    //pagedJsResponseModel = responseContent;
                            //    resobj.Data = pagedJsResponseModel;
                            //    resobj.StatusCode = HttpStatusCode.OK;
                            //    string responseLog = JsonConvert.SerializeObject(pagedJsResponseModel, Formatting.Indented);
                            //    _logger.LogInformation("Response Log for GeneratePagedJsHtml():", DateTime.Now.ToString() + "-" + responseLog);
                            //}

                        }

                    }
                    else
                    {

                        resobj.ErrorMessage = pagedJsResponse.Content;
                        resobj.StatusCode = pagedJsResponse.StatusCode;
                    }
                }
                else
                {
                    resobj.ErrorMessage = "Html content is null";
                    resobj.StatusCode = HttpStatusCode.BadRequest;
                    _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + resobj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                resobj.Data = ex.StackTrace;
                ModelState.AddModelError("Error: ", ex.StackTrace ?? ex.Message);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.StackTrace?.ToString() ?? ex.Message);
            }
            return resobj;
        }
        [HttpPost]
        [Route("UploadHtmlFileFromBlob")]
        public async Task<ResponseModel> UploadHtmlFileFromBlob(UploadHtmlFile uploadHtmlFile)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(uploadHtmlFile, Formatting.Indented);
                _logger.LogInformation("Request Log for UploadHtmlFileFromBlob():", DateTime.Now.ToString() + "-" + requestLog);
                if (!string.IsNullOrEmpty(uploadHtmlFile.Base64Html) && !string.IsNullOrEmpty(uploadHtmlFile.FileName))
                {
                    string accountName = _configuration["BloBStorageConfig:AzureAccountName"] ?? string.Empty;
                    string connectionString = _configuration["ConnectionStrings:AzureBlobStorage"] ?? string.Empty;
                    string accountKey = _configuration["BloBStorageConfig:AzureAccountKey"] ?? string.Empty;
                    string containerName = _configuration["BloBStorageConfig:AzureContainer_Html"] ?? string.Empty;
                    string fileName = uploadHtmlFile.FileName + ".html";

                    var encodedStr = Base64UrlEncoder.Decode(uploadHtmlFile.Base64Html);

                    var storageCredentials = new StorageCredentials(accountName, accountKey);
                    var storageAccount = new CloudStorageAccount(storageCredentials, true);
                    var connString = storageAccount.ToString(true);
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                    var container = blobClient.GetContainerReference(containerName).GetBlockBlobReference(fileName);
                    CloudBlockBlob blob = container;
                    //blob.Metadata["SetAccessPolicy"] = Azure.Storage.Blobs.Models.PublicAccessType.Blob.ToString();
                    blob.Properties.ContentType = "text/html";
                    string timezone = "UTC+03:00";
                    blob.Metadata["Timezone"] = timezone;

                    await blob.DeleteIfExistsAsync();

                    await blob.UploadTextAsync(encodedStr);
                    BlobSasTokenRequest request = new BlobSasTokenRequest();
                    request.AccountKey = accountKey;
                    request.AccountName = accountName;
                    request.ConnectionString = connectionString;
                    request.ContainerName = containerName;
                    request.FileName = fileName;

                    responseModel = await GetFileFromBlob(request);
                    string responseLog = JsonConvert.SerializeObject(responseModel, Formatting.Indented);
                    _logger.LogInformation("Response Log for UploadHtmlFileFromBlob():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    string? errorMessage = _resourceManager.GetString("BlobfromHtmlErrorMessage");
                    responseModel.ErrorMessage = errorMessage;
                    responseModel.StatusCode = HttpStatusCode.Ambiguous;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                responseModel.StatusCode = HttpStatusCode.InternalServerError;
                responseModel.ErrorMessage = "";
            }
            return responseModel;
        }
        [HttpPost]
        [Route("GetFileFromBlob")]
        public async Task<ResponseModel> GetFileFromBlob(BlobSasTokenRequest blobSasTokenRequest)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(blobSasTokenRequest, Formatting.Indented);
                _logger.LogInformation("Request Log for GetFileFromBlob():", DateTime.Now.ToString() + "-" + requestLog);
                CloudStorageAccount backupStorageAccount = CloudStorageAccount.Parse(blobSasTokenRequest.ConnectionString);
                var backupBlobClient = backupStorageAccount.CreateCloudBlobClient();
                var backupContainer = backupBlobClient.GetContainerReference(blobSasTokenRequest.ContainerName);

                CloudBlockBlob blobFile = backupContainer.GetBlockBlobReference(blobSasTokenRequest.FileName);
                BlobSasBuilder blobSasBuilder = new BlobSasBuilder()
                {
                    BlobContainerName = blobSasTokenRequest.ContainerName,
                    BlobName = blobSasTokenRequest.FileName,
                    ExpiresOn = DateTime.UtcNow.AddYears(1),//Let SAS token expire after 5 minutes.
                };
                blobSasBuilder.SetPermissions(BlobSasPermissions.Read);//User will only be able to read the blob and it's properties
                var sasToken = blobSasBuilder.ToSasQueryParameters(new StorageSharedKeyCredential(blobSasTokenRequest.AccountName, blobSasTokenRequest.AccountKey)).ToString();
                var sasUrl = blobFile.Uri.AbsoluteUri + "?" + sasToken;
                responseModel.Data = sasUrl;
                responseModel.StatusCode = HttpStatusCode.OK;
                string responseLog = JsonConvert.SerializeObject(sasUrl, Formatting.Indented);
                _logger.LogInformation("Response Log for GetFileFromBlob():", DateTime.Now.ToString() + "-" + responseLog);
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                responseModel.StatusCode = HttpStatusCode.InternalServerError;
                responseModel.ErrorMessage = "";
            }

            return responseModel;
        }
        [HttpPost]
        [Route("UploadHTMLLetterPdf")]
        public async Task<ResponseModel> UploadHTMLLetterPdf(LetterHTMLRequestModel letterHtmlContent)
        {
            string requestLog = JsonConvert.SerializeObject(letterHtmlContent, Formatting.Indented);
            _logger.LogInformation("Request Log for UploadHTMLLetterPdf():", DateTime.Now.ToString() + "-" + requestLog);
            ResponseModel resobj = new ResponseModel();
            PagedJsRequestModel pagedJsRequest = new PagedJsRequestModel();
            byte[] bytes = Convert.FromBase64String(letterHtmlContent.LetterHtmlContent);
            string utf8String = Encoding.UTF8.GetString(bytes);
            pagedJsRequest.payload = utf8String;
            PagedJsResponseModel responseContent = new PagedJsResponseModel();

            responseContent = await _templateService.GeneratePagedJsHtml(pagedJsRequest);
            var serial = JsonConvert.SerializeObject(responseContent.Data);
            var pagedJsHtml = JsonConvert.DeserializeObject<PagedJsResponseModel>(serial);
            if (pagedJsHtml != null || !string.IsNullOrEmpty(pagedJsHtml?.payload))
            {
                //Starting HTML to URL Process
                var decodedStr = Base64UrlEncoder.Encode(pagedJsHtml.payload);
                HtmlFileResponseModel htmlFileResponse = new HtmlFileResponseModel();

                if (!string.IsNullOrEmpty(decodedStr) && !string.IsNullOrEmpty(letterHtmlContent.TemplateName))
                {
                    UploadHtmlFile uploadHtmlFile = new UploadHtmlFile();
                    uploadHtmlFile.FileName = letterHtmlContent.TemplateName;
                    uploadHtmlFile.Base64Html = decodedStr;
                    var htmlResp = await UploadHtmlFileFromBlob(uploadHtmlFile);

                    htmlFileResponse.StatusCode = HttpStatusCode.OK;
                    htmlFileResponse.Message = "Success";
                    resobj.Data = htmlFileResponse;
                    resobj.StatusCode = HttpStatusCode.OK;
                    resobj.Status = true;

                    //Starting HTML to PDF Process
                    if (htmlResp != null && !string.IsNullOrEmpty(htmlResp?.Data?.ToString()))
                    {
                        PdfConversionRequest pdfConversionRequest = new PdfConversionRequest();
                        string HtmlUrl = htmlResp.Data.ToString()?.Replace(" ", "%20") ?? string.Empty;
                        var pdfConversion = new PdfConversion();
                        pdfConversion.inputUrl = HtmlUrl;

                        pdfConversion.includeHeaderFooter = true;
                        pdfConversion.pageLayout = new PageLayout { pageWidth = "11", pageHeight = "8.5" };
                        pdfConversion.json = "{}";

                        pdfConversionRequest.pdfConversion = pdfConversion;
                        var htmlResponse = await _templateService.HtmltoPDFConvert(pdfConversionRequest);
                        if (htmlResponse.Data != null && htmlResponse.StatusCode == HttpStatusCode.OK)
                        {
                            UploadPdfFile uploadPdf = new UploadPdfFile();
                            uploadPdf.PdfUrl = htmlResponse?.Data?.ToString() ?? string.Empty;
                            uploadPdf.FileName = letterHtmlContent.TemplateName;
                            var respPDF = await UploadPdfFileFromBlob(uploadPdf);
                            resobj.Data = respPDF.Data;
                            resobj.Status = true;
                            resobj.StatusCode = HttpStatusCode.OK;
                            string responseLog = JsonConvert.SerializeObject(respPDF.Data, Formatting.Indented);
                            _logger.LogInformation("Response Log for UploadHTMLLetterPdf():", DateTime.Now.ToString() + "-" + responseLog);
                        }
                        else
                        {
                            string? errorMessage = _resourceManager.GetString("HTMLtoPdfConversionErrorMessage");
                            resobj.Data = htmlResponse;
                            resobj.Status = false;
                            resobj.ErrorMessage = errorMessage;
                            resobj.StatusCode = HttpStatusCode.NoContent;
                        }
                    }
                    else
                    {
                        resobj.ErrorMessage = "HtmlURL path is null";
                        resobj.StatusCode = HttpStatusCode.BadRequest;
                        _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + resobj.ErrorMessage);
                    }
                }
                else
                {
                    htmlFileResponse.Message = "data is null";
                    resobj.Data = htmlFileResponse;
                }
            }
            else
            {
                resobj.ErrorMessage = "Empty PagedJS Response";
                resobj.StatusCode = HttpStatusCode.BadRequest;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + resobj.ErrorMessage);
            }
            return resobj;
        }
        [HttpPost]
        [Route("UploadPdfFileFromBlob")]
        public async Task<ResponseModel> UploadPdfFileFromBlob(UploadPdfFile uploadPdfFile)
        {
            ResponseModel responseModel = new ResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(uploadPdfFile, Formatting.Indented);
                _logger.LogInformation("Request Log for UploadPdfFileFromBlob():", DateTime.Now.ToString() + "-" + requestLog);
                if (!string.IsNullOrEmpty(uploadPdfFile.PdfUrl) && !string.IsNullOrEmpty(uploadPdfFile.FileName))
                {
                    string accountName = _configuration["BloBStorageConfig:AzureAccountName"] ?? string.Empty;
                    string connectionString = _configuration["ConnectionStrings:AzureBlobStorage"] ?? string.Empty;
                    string accountKey = _configuration["BloBStorageConfig:AzureAccountKey"] ?? string.Empty;
                    string containerName = _configuration["BloBStorageConfig:AzureContainer_Html"] ?? string.Empty;
                    string fileName = uploadPdfFile.FileName + ".pdf";
                    byte[] dataByte;
                    using (WebClient webClient = new WebClient())
                    {
                        dataByte = webClient.DownloadData(uploadPdfFile.PdfUrl);
                    }
                    var storageCredentials = new StorageCredentials(accountName, accountKey);
                    var storageAccount = new CloudStorageAccount(storageCredentials, true);
                    var connString = storageAccount.ToString(true);
                    CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                    var container = blobClient.GetContainerReference(containerName).GetBlockBlobReference(fileName);
                    container.Properties.ContentType = "application/pdf";

                    CloudBlockBlob blob = container;
                    blob.Properties.ContentType = "application/pdf";
                    string timezone = "UTC+03:00";
                    blob.Metadata["Timezone"] = timezone;

                    await blob.DeleteIfExistsAsync();

                    await blob.UploadFromByteArrayAsync(dataByte, 0, dataByte.Length);
                    BlobSasTokenRequest request = new BlobSasTokenRequest();
                    request.AccountKey = accountKey;
                    request.AccountName = accountName;
                    request.ConnectionString = connectionString;
                    request.ContainerName = containerName;
                    request.FileName = fileName;

                    responseModel = await GetFileFromBlob(request);
                    string responseLog = JsonConvert.SerializeObject(responseModel.Data, Formatting.Indented);
                    _logger.LogInformation("Response Log for UploadPdfFileFromBlob():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    responseModel.ErrorMessage = "Pdf url is null or engagement letter name is null";
                    responseModel.StatusCode = HttpStatusCode.NoContent;
                    _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + responseModel.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                responseModel.StatusCode = HttpStatusCode.InternalServerError;
                responseModel.ErrorMessage = "";
            }
            return responseModel;
        }

    }
}
