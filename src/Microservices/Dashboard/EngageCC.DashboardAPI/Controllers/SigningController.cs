﻿using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.CommonModel;
using Dashboard.Models.ResponseModel;
using Dashboard.Models.Signing;
using EngageCC.DashboardAPI.Filters;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using System.Reflection;
using System.Resources;

namespace EngageCC.DashboardAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [TypeFilter(typeof(AuthorizationFilter))]
    public class SigningController : ControllerBase 
    {
        private readonly ISigningService _signingService;
        private readonly IConfiguration _configuration;
        private readonly ResourceManager _resourceManager;
        private readonly IMapper _mapper;
        private readonly ILogger<SigningController> _logger;
        ResponseModel resobj = new ResponseModel();
        public SigningController(ISigningService signingService, IConfiguration configuration, ResourceManager resourceManager, IMapper mapper, ILogger<SigningController> logger)
        {
            _signingService = signingService;
            _configuration = configuration;
            _resourceManager = resourceManager;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpPost]
        [Route("CancelSigning")]
        public async Task<ResponseModel> CancelSigning(CancleSigningResModel signingViewModel)
        {
            try
            {
                string requestLog = JsonConvert.SerializeObject(signingViewModel, Formatting.Indented);
                _logger.LogInformation("Request Log for CancelSigning():", DateTime.Now.ToString() + "-" + requestLog);
                var refreshStatus = await _signingService.CancelSigning(signingViewModel);
                if (refreshStatus.Status != false)
                {
                    resobj.Data = refreshStatus;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(refreshStatus, Formatting.Indented);
                    _logger.LogInformation("Response Log for CancelSigning():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.Ambiguous;
                    resobj.PdfUrl = refreshStatus.PdfUrl;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return resobj;
        }
       
        [HttpPost]
        [Route("RefreshedSignedLetterStatus")]
        public async Task<ResponseModel> RefreshedSignedLetterStatus(ManualRefreshResModel signingDocument)
        {
            try
            {
                string requestLog = JsonConvert.SerializeObject(signingDocument, Formatting.Indented);
                _logger.LogInformation("Request Log for RefreshedSignedLetterStatus():", DateTime.Now.ToString() + "-" + requestLog);
                var signinginfo = _mapper.Map<ManualRefreshResModel>(signingDocument);
                var refreshStatus = await _signingService.RefreshedSignedLetterStatus(signinginfo);
                if (refreshStatus != null)
                {                  
                    resobj.Data = refreshStatus.PdfUrl;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(refreshStatus.PdfUrl, Formatting.Indented);
                    _logger.LogInformation("Response Log for RefreshedSignedLetterStatus():", DateTime.Now.ToString() + "-" + responseLog);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return resobj;
        }

        [HttpGet]
        [Route("AutoRefreshStatus")]
        public async Task<ResponseModel> AutoRefreshStatus()
        {
            try
            {
                var refreshStatus = await _signingService.AutoRefreshStatus();
                if (refreshStatus != null)
                {
                    resobj.Data = refreshStatus;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(refreshStatus, Formatting.Indented);
                    _logger.LogInformation("Response Log for AutoRefreshStatus():", DateTime.Now.ToString() + "-" + responseLog);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return resobj;
        }

        [HttpGet]
        [Route("GetPdfUrl/{engagementLetterId}")]
        public async Task<ResponseModel> GetPdfUrl(int engagementLetterId)
        {
            try
            {
                var pdfUrl = await _signingService.GetPdfUrl(engagementLetterId);
                if (pdfUrl != null)
                {
                    resobj.Data = pdfUrl;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(pdfUrl, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetPdfUrl():", DateTime.Now.ToString() + "-" + responseLog);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return resobj;
        }

        [HttpPost]
        [Route("UploadFinalVersionPDF")]
        public async Task<ResponseModel> UploadFinalVersionPDF(UploadLetterResModel uploadLetterRes)
        {
            try
            {
                string requestLog = JsonConvert.SerializeObject(uploadLetterRes, Formatting.Indented);
                _logger.LogInformation("Request Log for UploadFinalVersionPDF():", DateTime.Now.ToString() + "-" + requestLog);
                var refreshStatus = await _signingService.UploadFinalVersionPDF(uploadLetterRes);
                if (refreshStatus.Status != false)
                {
                    resobj.Data = refreshStatus.PdfUrl;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(refreshStatus.PdfUrl, Formatting.Indented);
                    _logger.LogInformation("Response Log for UploadFinalVersionPDF():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.Ambiguous;
                    resobj.PdfUrl = refreshStatus.PdfUrl;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return resobj;
        }
    }
}
       