﻿using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Business.Services;
using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.Clients;
using Dashboard.Models.CommonModel;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Filters;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using System.Resources;

namespace EngageCC.DashboardAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [TypeFilter(typeof(AuthorizationFilter))]
    public class BulkLettersController : ControllerBase
    {
        private readonly IBulkLettersService _bulkLettersService;
        private readonly ILogger<BulkLettersController> _logger;
        private readonly ResourceManager _resourceManager;
        private readonly IMapper _mapper;
        private readonly IClientService _clientService;
        public BulkLettersController(IBulkLettersService bulkLettersService, ILogger<BulkLettersController> Logger,
            ResourceManager resourceManager, IMapper mapper, IClientService clientService)
        {
            _bulkLettersService = bulkLettersService;
            _logger = Logger;
            _resourceManager = resourceManager;
            _mapper = mapper;
            _clientService = clientService;
        }
        ResponseModel resobj = new ResponseModel();

        [HttpPost]
        [Route("AddBatchRequestDetails")]
        public async Task<ResponseModel> AddBatchRequestDetails(BatchRequestViewModel batchRequest)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                string result = string.Empty;
                string requestLog = JsonConvert.SerializeObject(batchRequest, Formatting.Indented);
                _logger.LogInformation("Request Log for AddBatchRequestDetails():", DateTime.Now.ToString() + "-" + requestLog);

                var batchJsonData = JsonConvert.DeserializeObject<List<BatchJsonViewModel>>(batchRequest.BatchJson);
                var bulkLetterIds = batchJsonData?.Select(a => a.BulkLettersId ?? 0).ToList();
                if (bulkLetterIds != null && bulkLetterIds.Any())
                {
                    result = await _clientService.CheckFieldValues(bulkLetterIds);
                }

                if (!string.IsNullOrEmpty(result))
                {
                    response.Data = result;
                    response.Status = false;
                    response.StatusCode = HttpStatusCode.NoContent;
                    return response;
                }
                else
                {
                    var batchRequestResponse = _mapper.Map<BatchRequestResponse>(batchRequest);
                    var res = await _bulkLettersService.AddBatchRequestDetails(batchRequestResponse);
                    if (res.Status == false)
                    {
                        response.Data = "Already this " + batchRequestResponse.BatchId + " batch id is created some other user";
                        response.Status = false;
                        response.StatusCode = res.StatusCode;
                        return response;
                    }
                    else
                    {
                        response.Data = res.Data;
                        response.Status = true;
                        response.StatusCode = HttpStatusCode.OK;
                        string responseLog = JsonConvert.SerializeObject(res.Data, Formatting.Indented);
                        _logger.LogInformation("Response Log for AddBatchRequestDetails():", DateTime.Now.ToString() + "-" + responseLog);
                        return response;
                    }
                }

            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            //return response;
        }

        [HttpPost]
        [Route("InsertAttachmentsBulkLetter")]
        public async Task<ResponseModel> InsertAttachmentsBulkLetter(BulkLetterEditAttachmentsViewModel editAttachmentsViewModel)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(editAttachmentsViewModel, Formatting.Indented);
                _logger.LogInformation("Request Log for InsertAttachmentsBulkLetter():", DateTime.Now.ToString() + "-" + requestLog);
                var editAttachmentsMap = _mapper.Map<BulkLetterEditAttachmentsResponseModel>(editAttachmentsViewModel);
                var respData = await _bulkLettersService.InsertAttachmentsBulkLetter(editAttachmentsMap);
                if (!respData)
                {
                    response.Status = false;
                    response.StatusCode = HttpStatusCode.Ambiguous;
                }
                else
                {
                    response.Status = true;
                    response.StatusCode = HttpStatusCode.OK;
                }
                string responseLog = JsonConvert.SerializeObject(respData, Formatting.Indented);
                _logger.LogInformation("Response Log for InsertAttachmentsBulkLetter():", DateTime.Now.ToString() + "-" + responseLog);
                response.Data = respData;
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return response;
        }
        [HttpGet]
        [Route("GetBulkLetterAttachmentById")]
        public async Task<ResponseModel> GetBulkLetterAttachmentById(int batchId, int templateId)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                var resp = await _bulkLettersService.GetBulkLetterAttachmentById(batchId, templateId);
                if (resp != null)
                {
                    response.Data = resp;
                    response.Status = true;
                    response.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(resp, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetBulkLetterAttachmentById():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    response.Data = resp;
                    response.Status = false;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return response;
        }
        [HttpPut]
        [Route("DeleteByBulkLetterId")]
        public async Task<ResponseModel> DeleteByBulkLetterId(DeleteBulkLetterViewModel deleteId)
        {
            try
            {
                string requestLog = JsonConvert.SerializeObject(deleteId, Formatting.Indented);
                _logger.LogInformation("Request Log for DeleteByBulkLetterId():", DateTime.Now.ToString() + "-" + requestLog);
                var clientId = _mapper.Map<DeleteBulkLetterResModel>(deleteId);
                var delete = await _bulkLettersService.DeleteByBulkLetterId(clientId);
                if (delete != false)
                {
                    resobj.Data = delete;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(delete, Formatting.Indented);
                    _logger.LogInformation("Response Log for DeleteByBulkLetterId():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NotAcceptable;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }
            return resobj;
        }

        [HttpPut]
        [Route("DeleteByBatchId")]
        public async Task<ResponseModel> DeleteByBatchId(DeleteByBatchViewModel deleteId)
        {
            try
            {
                string requestLog = JsonConvert.SerializeObject(deleteId, Formatting.Indented);
                _logger.LogInformation("Request Log for DeleteByBatchId():", DateTime.Now.ToString() + "-" + requestLog);
                var batchId = _mapper.Map<DeleteByBatchResModel>(deleteId);
                var delete = await _bulkLettersService.DeleteByBatchId(batchId);
                if (delete != false)
                {
                    resobj.Data = delete;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(delete, Formatting.Indented);
                    _logger.LogInformation("Response Log for DeleteByBatchId():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NotAcceptable;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }
            return resobj;
        }
        [HttpPost]
        [Route("InsertFieldsList")]
        public async Task<ResponseModel> InsertFieldsList(BulkLetterFieldsViewModel fieldsList)
        {
            try
            {
                string requestLog = JsonConvert.SerializeObject(fieldsList, Formatting.Indented);
                _logger.LogInformation("Request Log for InsertFieldsList():", DateTime.Now.ToString() + "-" + requestLog);
                var batchId = _mapper.Map<BulkLetterFieldsResponseModel>(fieldsList);
                if (batchId != null)
                {
                    await _bulkLettersService.InsertFieldsList(batchId);
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NoContent;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }
            return resobj;
        }

        [HttpGet]
        [Route("PartnerOfficeByBatchId")]
        public async Task<ResponseModel> GetPartnerOfficeListAsync(int batchId)
        {
            try
            {
                string requestLog = JsonConvert.SerializeObject(batchId, Formatting.Indented);
                _logger.LogInformation("Request Log for GetPartnerOfficeListAsync():", DateTime.Now.ToString() + "-" + requestLog);
                if (batchId > 0)
                {
                    List<PartnerOfficeResponse> partnerOfficeResponses = await _bulkLettersService.GetPartnerOfficeListAsync(batchId);
                    resobj.Data = partnerOfficeResponses;
                    resobj.StatusCode = HttpStatusCode.OK;
                    resobj.Status = true;
                    string responseLog = JsonConvert.SerializeObject(partnerOfficeResponses, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetPartnerOfficeListAsync():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.ErrorMessage = "BatchId is null";
                    resobj.StatusCode = HttpStatusCode.BadRequest;
                    resobj.Status = false;
                    _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + resobj.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                resobj.ErrorMessage = ex.Message;
                resobj.StatusCode = HttpStatusCode.InternalServerError;
                resobj.Status = false;
            }
            return resobj;
        }
        [HttpGet]
        [Route("GetBulkLetterFieldsById")]
        public async Task<ResponseModel> GetBulkLetterFieldsById(int bulkLetterId)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(bulkLetterId, Formatting.Indented);
                _logger.LogInformation("Request Log for GetBulkLetterFieldsById():", DateTime.Now.ToString() + "-" + requestLog);
                EditFieldValuesResponse valuesResponse = await _bulkLettersService.GetBulkLetterFieldsById(bulkLetterId);
                if (valuesResponse != null)
                {
                    response.Data = valuesResponse;
                    response.Status = true;
                    response.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(valuesResponse, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetBulkLetterFieldsById():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    response.Status = false;
                    response.StatusCode = HttpStatusCode.BadRequest;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                resobj.ErrorMessage = ex.Message;
                resobj.StatusCode = HttpStatusCode.InternalServerError;
                resobj.Status = false;
            }
            return response;
        }

        [HttpPost]
        [Route("SaveEditFields")]
        public async Task<ResponseModel> SaveEditFields(EditFieldValuesResponse editField)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(editField, Formatting.Indented);
                _logger.LogInformation("Request Log for SaveEditFields():", DateTime.Now.ToString() + "-" + requestLog);
                bool valuesResponse = await _bulkLettersService.SaveEditFields(editField);
                if (valuesResponse)
                {
                    response.Data = valuesResponse;
                    response.Status = true;
                    response.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(valuesResponse, Formatting.Indented);
                    _logger.LogInformation("Response Log for SaveEditFields():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    response.Status = false;
                    response.StatusCode = HttpStatusCode.BadRequest;
                }

            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return response;
        }
        [HttpPost]
        [Route("GetCombinedPDFURL")]
        public async Task<ResponseModel> GetCombinedPDFURL(BulkLettersCombinePdfViewModel combinedPdf)
        {
            ResponseModel resobj = new ResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(combinedPdf, Formatting.Indented);
                _logger.LogInformation("Request Log for GetCombinedPDFURL():", DateTime.Now.ToString() + "-" + requestLog);
                LetterHTMLRequestModel letterHTML = new LetterHTMLRequestModel();
                AttachmentPdfFile attachmentPdf = new AttachmentPdfFile();
                CombinePDFReq combinePDF = new CombinePDFReq();
                letterHTML.TemplateName = combinedPdf.TemplateName;
                letterHTML.LetterHtmlContent = combinedPdf.LetterHtmlContent;
                List<string> assetIDList = new List<string>();

                var letterHTMLRes = await _bulkLettersService.UploadHTMLLetter(letterHTML);
                if (letterHTMLRes != null && letterHTMLRes.Data != null)
                {
                    string letterHTMLResponse = letterHTMLRes.Data.ToString() ?? string.Empty;
                    assetIDList.Add(letterHTMLResponse);
                    combinePDF.LetterHTMLAssetID1 = letterHTMLResponse;
                }
                var get7216AssetId = await _bulkLettersService.Get7216FormAssetId(combinedPdf);
                if (!string.IsNullOrEmpty(get7216AssetId))
                {
                    combinePDF.AssetId7216Form = get7216AssetId.Replace("\"", string.Empty).Replace('"', ' ').ToString();
                }
                int batchId = combinedPdf.BatchId;
                int templateId = combinedPdf.TemplateId;
                var getAttachments = await _bulkLettersService.GetAttachmentsList(batchId, templateId);

                if (getAttachments.Count() > 0)
                {
                    attachmentPdf.UploadAttachmentJSON = getAttachments;

                    var attachmentRes = await _bulkLettersService.UploadAttachment(attachmentPdf);
                    if (attachmentRes != null)
                    {
                        int i = 0;
                        foreach (var item in attachmentRes)
                        {
                            assetIDList.Add(item.ToString());
                            if (i == 0)
                            { combinePDF.Attachment1 = item.Replace("\"", string.Empty).Replace('"', ' ').ToString(); i++; }
                            else if (i == 1)
                            { combinePDF.Attachment2 = item.Replace("\"", string.Empty).Replace('"', ' ').ToString(); i++; }
                            else if (i == 2)
                            { combinePDF.Attachment3 = item.Replace("\"", string.Empty).Replace('"', ' ').ToString(); i++; }
                            else if (i == 3)
                            { combinePDF.Attachment4 = item.Replace("\"", string.Empty).Replace('"', ' ').ToString(); i++; }
                            else if (i == 4)
                            { combinePDF.Attachment5 = item.Replace("\"", string.Empty).Replace('"', ' ').ToString(); i++; }
                        }
                    }
                    combinePDF.TemplateName = combinedPdf.TemplateName;
                    var combinePDFResponse = await _bulkLettersService.CombinePDFReq(combinePDF);
                    if (combinePDFResponse != null)
                    {
                        resobj.Data = combinePDFResponse.Data;
                        resobj.Status = true;
                        resobj.StatusCode = HttpStatusCode.OK;
                        string responseLog = JsonConvert.SerializeObject(combinePDFResponse.Data, Formatting.Indented);
                        _logger.LogInformation("Response Log for GetCombinedPDFURL():", DateTime.Now.ToString() + "-" + responseLog);
                    }
                    else
                    {
                        resobj.Status = false;
                        resobj.StatusCode = HttpStatusCode.NoContent;
                        resobj.ErrorMessage = combinePDFResponse?.Data?.ToString() ?? string.Empty;
                    }

                }
                else if (!string.IsNullOrEmpty(combinePDF.AssetId7216Form))
                {
                    combinePDF.TemplateName = combinedPdf.TemplateName;
                    var combinePDFResponse = await _bulkLettersService.CombinePDFReq(combinePDF);
                    if (combinePDFResponse != null)
                    {
                        resobj.Data = combinePDFResponse.Data;
                        resobj.Status = true;
                        resobj.StatusCode = HttpStatusCode.OK;
                        string responseLog = JsonConvert.SerializeObject(combinePDFResponse.Data, Formatting.Indented);
                        _logger.LogInformation("Response Log for GetCombinedPDFURL():", DateTime.Now.ToString() + "-" + responseLog);
                    }
                    else
                    {
                        resobj.Status = false;
                        resobj.StatusCode = HttpStatusCode.NoContent;
                        resobj.ErrorMessage = combinePDFResponse?.Data?.ToString() ?? string.Empty;
                    }
                }
                else
                {
                    var getadobeURL = letterHTMLRes?.PdfUrl.ToString();
                    UploadPdfFile uploadPdf = new UploadPdfFile();
                    uploadPdf.PdfUrl = getadobeURL ?? string.Empty;
                    uploadPdf.FileName = combinedPdf.TemplateName;
                    var respPDF = await _bulkLettersService.UploadPdfFileFromBlob(uploadPdf);
                    if (respPDF != null)
                    {
                        resobj.Data = respPDF.Data;
                        resobj.Status = true;
                        resobj.StatusCode = HttpStatusCode.OK;
                        string responseLog = JsonConvert.SerializeObject(respPDF.Data, Formatting.Indented);
                        _logger.LogInformation("Response Log for GetCombinedPDFURL():", DateTime.Now.ToString() + "-" + responseLog);
                    }
                    else
                    {
                        resobj.Status = false;
                        resobj.StatusCode = HttpStatusCode.Ambiguous;
                        resobj.ErrorMessage = respPDF?.Data?.ToString() ?? string.Empty;
                    }
                }

            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return resobj;
        }

        [HttpPut]
        [Route("RemoveClientData")]
        public async Task<ResponseModel> RemoveClientData(RemoveClientDataViewModel clientData)
        {
            try
            {
                string requestLog = JsonConvert.SerializeObject(clientData, Formatting.Indented);
                _logger.LogInformation("Request Log for RemoveClientData():", DateTime.Now.ToString() + "-" + requestLog);
                var request = _mapper.Map<RemoveClientDataResModel>(clientData);
                var delete = await _bulkLettersService.RemoveClientData(request);
                if (delete != false)
                {
                    resobj.Data = delete;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(delete, Formatting.Indented);
                    _logger.LogInformation("Response Log for DeleteByBulkLetterId():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NotAcceptable;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }
            return resobj;
        }
    }
}
