﻿using AutoMapper;
using Dashboard.Business.Interfaces;
using Dashboard.Models.Blocks;
using Dashboard.Models.CommonModel;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Filters;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Serilog;
using System.Net;
using System.Resources;
using System.Text.RegularExpressions;

namespace EngageCC.DashboardAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [TypeFilter(typeof(AuthorizationFilter))]
    public class BlocksController : ControllerBase
    {
        private IBlocksService _blocksService;
        private readonly ILogger<BlocksController> _logger;
        private readonly ResourceManager _resourceManager;
        private readonly IMapper _mapper;

        public BlocksController(IBlocksService blocksService, ILogger<BlocksController> logger, ResourceManager resourceManager, IMapper mapper)
        {
            _blocksService = blocksService;
            _logger = logger;
            _resourceManager = resourceManager;
            _mapper = mapper;
        }
        ResponseModel resobj = new ResponseModel();

        [HttpPost]
        [Route("AddBlocks")]
        public async Task<ResponseModel> AddBlocks(BlocksViewModel blocks)
        {
            try
            {
                string requestLog = JsonConvert.SerializeObject(blocks, Formatting.Indented);
                _logger.LogInformation("Request Log for AddBlocks():", DateTime.Now.ToString() + "-" + requestLog);
                var item = blocks.Content;
                item = HtmlToPlainText(item);
                if (item == "\n")
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NoContent;
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        var block = new BlocksViewModel();
                        var blocker = _mapper.Map<BlocksResponseModel>(blocks);
                        var blockAdd = await _blocksService.AddBlocks(blocker);
                        if (blockAdd != null)
                        {
                            if (blockAdd.Item2 == string.Empty)
                            {
                                block = _mapper.Map<BlocksViewModel>(blockAdd.Item1);
                                resobj.Data = block;
                                resobj.Status = true;
                                resobj.StatusCode = HttpStatusCode.OK;
                                string responseLog = JsonConvert.SerializeObject(block, Formatting.Indented);
                                _logger.LogInformation("Response Log for AddBlocks():", DateTime.Now.ToString() + "-" + responseLog);
                            }
                            else
                            {
                                resobj.Status = false;
                                resobj.ErrorMessage = blockAdd.Item2;
                                resobj.StatusCode = HttpStatusCode.Ambiguous;
                            }
                        }
                        else
                        {
                            resobj.Status = false;
                            resobj.ErrorMessage = blockAdd?.Item2;
                            resobj.StatusCode = HttpStatusCode.NotAcceptable;
                        }
                    }
                    else
                    {
                        List<string> check = new List<string>();
                        foreach (var state in ModelState)
                        {
                            foreach (var error in state.Value.Errors)
                            {
                                check.Add(error.ErrorMessage);
                            }
                        }
                        resobj.ErrorMessage = string.Join(",", check);
                        resobj.StatusCode = HttpStatusCode.BadRequest;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }
            return resobj;
        }


        [HttpPut]
        [Route("UpdateBlocks")]
        public async Task<ResponseModel> UpdateBlocks(BlocksViewModel blocks)
        {
            try
            {
                string requestLog = JsonConvert.SerializeObject(blocks, Formatting.Indented);
                _logger.LogInformation("Request Log for UpdateBlocks():", DateTime.Now.ToString() + "-" + requestLog);
                var item = blocks.Content;
                item = HtmlToPlainText(item);
                if (item == "\n")
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NoContent;
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        var block = new BlocksViewModel();
                        var blocker = _mapper.Map<BlocksResponseModel>(blocks);
                        var blockAdd = await _blocksService.UpdateBlocks(blocker);
                        if (blockAdd != null)
                        {
                            if (blockAdd.Item2 == string.Empty)
                            {
                                block = _mapper.Map<BlocksViewModel>(blockAdd.Item1);
                                resobj.Data = block;
                                resobj.Status = true;
                                resobj.StatusCode = HttpStatusCode.OK;
                                string responseLog = JsonConvert.SerializeObject(blocks, Formatting.Indented);
                                _logger.LogInformation("Response Log for UpdateBlocks():", DateTime.Now.ToString() + "-" + responseLog);
                            }
                            else
                            {
                                resobj.Status = false;
                                resobj.ErrorMessage = blockAdd.Item2;
                                resobj.StatusCode = HttpStatusCode.Ambiguous;
                            }
                        }
                        else
                        {
                            resobj.Status = false;
                            resobj.StatusCode = HttpStatusCode.NotAcceptable;
                        }
                    }
                    else
                    {
                        List<string> check = new List<string>();
                        foreach (var state in ModelState)
                        {
                            foreach (var error in state.Value.Errors)
                            {
                                check.Add(error.ErrorMessage);
                            }
                        }
                        resobj.ErrorMessage = string.Join(",", check);
                        resobj.StatusCode = HttpStatusCode.BadRequest;
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }
            return resobj;
        }

        [HttpGet]
        [Route("GetBlocksIdList")]
        public async Task<ResponseModel> GetBlocksIdList()
        {
            try
            {
                var blocksId = new List<BlockIdListViewModel>();
                var blocksIdList = await _blocksService.GetBlocksIdList();
                if (blocksIdList != null)
                {
                    blocksId = _mapper.Map<List<BlockIdListViewModel>>(blocksIdList);
                    resobj.Data = blocksIdList;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(blocksIdList, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetBlocksIdList():", DateTime.Now.ToString() + "-" + responseLog);
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }
            return resobj;
        }

        [HttpGet]
        [Route("GetBlocks")]
        public async Task<ResponseModel> GetBlocks()
        {
            try
            {
                var blocksList = new List<BlocksListViewModel>();
                var blocksListResult = await _blocksService.GetBlocks();
                if (blocksListResult != null)
                {
                    blocksList = _mapper.Map<List<BlocksListViewModel>>(blocksListResult);
                    resobj.Data = blocksListResult;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(blocksListResult, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetBlocks():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }
            return resobj;
        }

        [HttpGet]
        [Route("GetActiveBlocks")]
        public async Task<ResponseModel> GetActiveBlocks()
        {
            try
            {
                var activeBlocksList = new List<ActiveBlockListViewModel>();
                var activeBlocksRes = await _blocksService.GetActiveBlocksList();
                if (activeBlocksRes.Any())
                {
                    activeBlocksList = _mapper.Map<List<ActiveBlockListViewModel>>(activeBlocksRes);
                    resobj.Data = activeBlocksList;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(activeBlocksList, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetActiveBlocks():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else { resobj.Status = false; }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }
            return resobj;
        }

        [HttpPost]
        [Route("GetBlocksById")]
        public async Task<ResponseModel> GetBlocksById(GetBlockByIdViewModel blocksId)
        {
            try
            {
                var blockId = blocksId.BlockId;
                var blocksList = new List<GetBlockByIdViewModel>();
                var blockById = await _blocksService.GetBlocksById(blockId);
                if (blockById != null)
                {
                    blocksList = _mapper.Map<List<GetBlockByIdViewModel>>(blockById);
                    resobj.Data = blockById;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(blockById, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetBlocksById():", DateTime.Now.ToString() + "-" + responseLog);

                }
                else { resobj.Status = false; }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }
            return resobj;
        }

        [HttpPut]
        [Route("DeleteBlock")]
        public async Task<ResponseModel> DeleteBlock(DeleteBlockViewModel blockId)
        {
            try
            {
                string requestLog = JsonConvert.SerializeObject(blockId, Formatting.Indented);
                _logger.LogInformation("Request Log for DeleteBlock():", DateTime.Now.ToString() + "-" + requestLog);
                var blocksList = new DeleteBlockViewModel();
                var blocker = _mapper.Map<DeleteBlockResModel>(blockId);
                var result = await _blocksService.DeleteBlock(blocker);
                if (result.IsDeleted != false)
                {
                    resobj.Data = result;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.LogInformation("Response Log for DeleteBlock():", DateTime.Now.ToString() + "-" + responseLog);
                }                
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NotAcceptable;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                ModelState.AddModelError(_resourceManager.GetString("ErrorMessage"), ex.Message);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }
            return resobj;
        }

        [HttpGet]
        [Route("Load")]
        public async Task<ResponseModel> Load()
        {
            var blockStatus = new BlocksStatusViewModel();
            try
            {
                var block = await _blocksService.Load();
                if (block != null)
                {
                    blockStatus = _mapper.Map<BlocksStatusViewModel>(block);
                    resobj.Data = block;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(block, Formatting.Indented);
                    _logger.LogInformation("Response Log for Load():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else { resobj.Status = false; }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }
            return resobj;
        }

        private static string HtmlToPlainText(string html)
        {
            const string tagWhiteSpace = @"(>|$)(\W|\n|\r)+<";//matches one or more (white space or line breaks) between '>' and '<'
            const string stripFormatting = @"<[^>]*(>|$)";//match any character between '<' and '>', even when end tag is missing
            const string lineBreak = @"<(br|BR)\s{0,1}\/{0,1}>";//matches: <br>,<br/>,<br />,<BR>,<BR/>,<BR />
            var lineBreakRegex = new Regex(lineBreak, RegexOptions.Multiline);
            var stripFormattingRegex = new Regex(stripFormatting, RegexOptions.Multiline);
            var tagWhiteSpaceRegex = new Regex(tagWhiteSpace, RegexOptions.Multiline);

            var text = html;
            //Decode html specific characters
            text = System.Net.WebUtility.HtmlDecode(text);
            //Remove tag whitespace/line breaks
            text = tagWhiteSpaceRegex.Replace(text, "><");
            //Replace <br /> with line breaks
            text = lineBreakRegex.Replace(text, Environment.NewLine);
            //Strip formatting
            text = stripFormattingRegex.Replace(text, string.Empty);

            var list = text.ToList();

            var allBlocknames = new List<String>();
            var relatedEntities = new List<String>();
            foreach (var item in list)
            {
                String itemString = item.ToString();

                var abc = itemString.Replace("#", "");

                allBlocknames.Add(abc);
            }
            relatedEntities = allBlocknames.Distinct().ToList();
            string field = (string.Join(",", relatedEntities));
            return field;
        }
    }
}
