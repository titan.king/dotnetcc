﻿using AutoMapper;
using Azure;
using Dashboard.Business.Interfaces;
using Dashboard.Business.Services;
using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.CommonModel;
using Dashboard.Models.EngagementLetter;
using Dashboard.Models.Fields;
using Dashboard.Models.ResponseModel;
using EngageCC.DashboardAPI.Filters;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using System.Reflection;
using System.Resources;

namespace EngageCC.DashboardAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [TypeFilter(typeof(AuthorizationFilter))]
    public class EngagementLetterController : ControllerBase
    {
        private readonly IEngagementLetterService _engagementLetterService;
        private readonly ILogger<EngagementLetterController> _logger;
        private readonly ResourceManager _resourceManager;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration; 

        public EngagementLetterController(IEngagementLetterService EngagementLetterService, ILogger<EngagementLetterController> Logger,
            ResourceManager resourceManager, IMapper mapper, IConfiguration configuration)
        {
            _engagementLetterService = EngagementLetterService;
            _logger = Logger;
            _resourceManager = resourceManager;
            _mapper = mapper;
            _configuration = configuration;
        }
        ResponseModel resobj = new ResponseModel();

        [HttpGet]
        [Route("GetEngagementLetterFilterFields")]
        public async Task<ResponseModel> GetEngagementLetterFilterFields()
        {
            try
            {
                var fieldsList = new EngagementLetterSearchFilterViewModel();
                fieldsList = await _engagementLetterService.GetEngagementLetterFilterFields();
                if (fieldsList != null)
                {
                    resobj.Data = fieldsList;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(fieldsList, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetEngagementLetterFilterFields():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NoContent;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }
            return resobj;
        }
        [HttpPost]
        [Route("GetFilteredSearchLetters")]
        public async Task<ResponseModel> GetFilteredSearchLetters(EngagementLetterListViewModel engagementLetterList)
        {
            ResponseModel resobj = new ResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(engagementLetterList, Formatting.Indented);
                _logger.LogInformation("Request Log for GetFilteredSearchLetters():", DateTime.Now.ToString() + "-" + requestLog);
                var mapResult = _mapper.Map<EngagementLetterSearchList>(engagementLetterList);
                var searchListResult = await _engagementLetterService.GetFilteredSearchLetters(mapResult);
                if (searchListResult != null)
                {
                    resobj.Data = searchListResult;
                    resobj.StatusCode = HttpStatusCode.OK;
                    resobj.Status = true;
                    string responseLog = JsonConvert.SerializeObject(searchListResult, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetFilteredSearchLetters():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NoContent;
                }
                return resobj;
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("GetEngagementLetters")]
        public async Task<ResponseModel> GetEngagementLetters()
        {
            try
            {
                var fieldsList = new List<GetEngagementLetterListViewModel>();
                var fieldsListRes = await _engagementLetterService.GetEngagementLetters();
                fieldsList = _mapper.Map<List<GetEngagementLetterListViewModel>>(fieldsListRes);
                if (fieldsList != null)
                {
                    resobj.Data = fieldsList;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(fieldsList, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetEngagementLetters():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NoContent;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }
            return resobj;
        }
        [HttpGet]
        [Route("GetEngagementLettersById")]
        public async Task<ResponseModel> GetEngagementLettersById(int bulklettersId)
        {
            try
            {
                var engagementLetter = new GetEngagementLetterListViewModel();
                var getResp = await _engagementLetterService.GetEngagementLettersById(bulklettersId);
                engagementLetter = _mapper.Map<GetEngagementLetterListViewModel>(getResp);
                if (engagementLetter != null)
                {
                    resobj.Data = engagementLetter;
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(engagementLetter, Formatting.Indented);
                    _logger.LogInformation("Response Log for GetEngagementLettersById():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NoContent;
                }
            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }
            return resobj;
        }
        [HttpPut]
        [Route("DeleteEngagementLetter")]
        public async Task<ResponseModel> DeleteEngagementLetter(DeleteEngagementLetterViewModel engLetterId)
        {
            try
            {
                string requestLog = JsonConvert.SerializeObject(engLetterId, Formatting.Indented);
                _logger.LogInformation("Request Log for DeleteEngagementLetter():", DateTime.Now.ToString() + "-" + requestLog);
                var engLetter = new DeleteEngagementLetterViewModel();
                var engLetterMap = _mapper.Map<DeleteEngagementLetterResModel>(engLetterId);
                var engLetterDelete = await _engagementLetterService.DeleteEngagementLetter(engLetterMap);
                if (engLetterDelete != false)
                {                  
                    resobj.Status = true;
                    resobj.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(engLetterDelete, Formatting.Indented);
                    _logger.LogInformation("Response Log for DeleteEngagementLetter():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    resobj.Status = false;
                    resobj.StatusCode = HttpStatusCode.NotAcceptable;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, _resourceManager.GetString("ErrorMessage"));
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }
            return resobj;
        }
        [HttpGet]
        [Route("GetHistoryLogById")]
        public async Task<ResponseModel> GetHistoryLogById(int engagementLetterId)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                if (engagementLetterId > 0)
                {
                    var getResp = await _engagementLetterService.GetHistoryLogById(engagementLetterId);
                    var retResponse = _mapper.Map<List<HistoryLogViewModel>>(getResp);
                    if(retResponse.Any())
                    {
                        response.Data = retResponse;
                        response.Status = true;
                        response.StatusCode = HttpStatusCode.OK;
                    }
                    else
                    {
                        response.Status = false;
                        response.StatusCode = HttpStatusCode.NoContent;
                    }
                }
            }
            catch(Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError);
            }
        return response;
        }
        [HttpPost]
        [Route("SaveEngagementLetterEditFields")]
        public async Task<ResponseModel> SaveEngagementLetterEditFields(EditFieldValuesResponse editField)
        {
            ResponseModel response = new ResponseModel();
            try
            {
                string requestLog = JsonConvert.SerializeObject(editField, Formatting.Indented);
                _logger.LogInformation("Request Log for SaveEngagementLetterEditFields():", DateTime.Now.ToString() + "-" + requestLog);
                bool valuesResponse = await _engagementLetterService.SaveEngagementLetterEditFields(editField);
                if (valuesResponse)
                {
                    response.Data = valuesResponse;
                    response.Status = true;
                    response.StatusCode = HttpStatusCode.OK;
                    string responseLog = JsonConvert.SerializeObject(valuesResponse, Formatting.Indented);
                    _logger.LogInformation("Response Log for SaveEngagementLetterEditFields():", DateTime.Now.ToString() + "-" + responseLog);
                }
                else
                {
                    response.Status = false;
                    response.StatusCode = HttpStatusCode.BadRequest;
                }

            }
            catch (Exception ex)
            {
                string errorMsg = "Messgae: " + ex.Message + "StackTrace: " + ex.StackTrace + "InnerException: " + ex.InnerException;
                _logger.LogError(_resourceManager.GetString("ErrorMessage") + DateTime.Now.ToString() + "-" + errorMsg);
                return new ResponseModel(HttpStatusCode.InternalServerError, ex.Message);
            }
            return response;
        }
    }
}
