﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.IdentityModel.Protocols;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace EngageCC.DashboardAPI.Filters
{
    public class AuthorizationFilter : IAuthorizationFilter
    {
        private readonly IConfiguration _configuration;
        public AuthorizationFilter(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            try
            {
                var tenantID = _configuration["ADAuthTokens:TenantID"];
                var audience = _configuration["ADAuthTokens:Audience"];

                tenantID = tenantID?.ToLower();
                var token = context.HttpContext.Request.Headers["Authorization"].ToString().Replace("bearer ", "");
                if (!string.IsNullOrEmpty(token))
                {
                    IConfigurationManager<OpenIdConnectConfiguration> configurationManager = new ConfigurationManager<OpenIdConnectConfiguration>(
                        $"https://login.microsoftonline.com/{tenantID}/v2.0/.well-known/openid-configuration",
                        new OpenIdConnectConfigurationRetriever());

                    OpenIdConnectConfiguration openIdConfig = configurationManager.GetConfigurationAsync(CancellationToken.None).Result;

                    TokenValidationParameters validationParameters = new TokenValidationParameters
                    {
                        ValidIssuer = openIdConfig.Issuer,
                        ValidAudience = audience,
                        IssuerSigningKeys = openIdConfig.SigningKeys,
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.Zero
                    };

                    JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();

                    ClaimsPrincipal claimsPrincipal = tokenHandler.ValidateToken(token, validationParameters, out _);
                    // Token is valid
                }
                else
                {
                    context.Result = new Microsoft.AspNetCore.Mvc.BadRequestResult();
                }
            }
            catch (Exception)
            {
                context.Result = new Microsoft.AspNetCore.Mvc.UnauthorizedResult();
            }
        }
    }
}
