﻿using Dashboard.Data.Interfaces;
using Dashboard.Data.Repositories;
using EngageCC.DashboardAPI.Filters;
using Dashboard.Business.Services;
using Dashboard.Business.Interfaces;
using Engage3.Common.Business;
using Engage3.Common.Business.Interfaces;
using Dashboard.Data.Context;

namespace EngageCC.DashboardAPI.Helpers
{
    public class DependencyInjectionResolver
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<AuthorizationFilter>();
            services.AddScoped<ISigningService, SigningService>();
            services.AddScoped<IFieldsService, FieldsService>();
            services.AddScoped<ITemplateService, TemplateService>();
            services.AddScoped<ILoginService, LoginService>();
            services.AddScoped<IEngagementLetterService, EngagementLetterService>();
            services.AddScoped<IBlocksService, BlocksService>();
            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<IBulkLettersService, BulkLettersService>();
            services.AddHttpContextAccessor();
            services.AddScoped<BlobStorageConfiguration>();
            services.AddScoped<EmailHelper>();
            services.AddScoped<IExcelImport, ImportExcel>();
            services.AddScoped< IGetCurrentDatetime, GetCurrentDateTime>();

            services.AddScoped<DashboardDBContext>();
        }

        public static void RegisterRepository(IServiceCollection services)
        {

            services.AddScoped<IFieldsRepository, FieldsRepository>();
            services.AddScoped<ITemplateRespository, TemplateRepository>();
            services.AddScoped<ILoginRepository, LoginRepository>();
            services.AddScoped<IEngagementLetterRepository, EngagementLetterRepository>();
            services.AddScoped<IBlocksRepository, BlocksRepository>();
            services.AddScoped<IClientRepository, ClientRepository>();
            services.AddScoped<IBulkLettersRepository, BulkLettersRepository>();
            services.AddScoped<ISigningRepository, SigningRepository>();
        }
    }
}
