﻿using AutoMapper;
using Dashboard.Data.DataModel;
using Dashboard.Models.Blocks;
using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.Clients;
using Dashboard.Models.EngagementLetter;
using Dashboard.Models.Fields;
using Dashboard.Models.Login;
using Dashboard.Models.ResponseModel;
using Dashboard.Models.Template;
using Dashboard.Data.DataModel;
using Microsoft.AspNetCore.SignalR.Protocol;
using Dashboard.Models.Blocks;
using Dashboard.Models.Clients;
using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.Signing;
using Dashboard.Models.CommonModel;
using Engage3.Common.Business.Models;

namespace EngageCC.DashboardAPI.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<GetEngagementLetterListViewModel, EngagementLetterList>().ReverseMap();
            CreateMap<EngagementLetterListViewModel, EngagementLetterSearchList>().ReverseMap();
            CreateMap<DeleteEngagementLetterViewModel, DeleteEngagementLetterResModel>().ReverseMap();

            CreateMap<LoginViewModel, LoginListModel>().ReverseMap();
            CreateMap<FieldViewModel, FieldListModel>().ReverseMap();
            CreateMap<Field, FieldListModel>().ReverseMap();
            CreateMap<FieldsStatusViewModel, FieldsStatusResModel>().ReverseMap();
            CreateMap<FieldsIdListViewModel, FieldsIdListResModel>().ReverseMap();
            CreateMap<DeleteFieldsViewModel, DeleteFieldResModel>().ReverseMap();
            CreateMap<ColumnNamesViewModel, ColumnNamesResModel>().ReverseMap();
            CreateMap<FieldsGetByIdViewModel, FieldListModel>().ReverseMap();

            CreateMap<Template, TemplateViewModel>().ReverseMap();
            CreateMap<TemplateVersion, TemplateViewModel>().ReverseMap();
            CreateMap<TemplateViewModel, TemplateListModel>().ReverseMap();
            CreateMap<DepartmentsViewModel, DepartmentsList>().ReverseMap();
            CreateMap<EngageTypesViewModel, EngageTypesList>().ReverseMap();
            CreateMap<StatusViewModel, StatusListModel>().ReverseMap();

            CreateMap<DeleteBulkLetterViewModel, DeleteBulkLetterResModel>().ReverseMap();
            CreateMap<DeleteByBatchViewModel, DeleteByBatchResModel>().ReverseMap();

            CreateMap<GetBlockByIdViewModel, BlocksList>().ReverseMap();
            CreateMap<DeleteBlockViewModel, DeleteBlockResModel>().ReverseMap();
            CreateMap<BlocksListViewModel, BlocksList>().ReverseMap();
            CreateMap<ActiveBlocksList, ActiveBlockListViewModel>().ReverseMap();
            CreateMap<Block, BlocksCommonModel>().ReverseMap();
            CreateMap<Block, BlocksResponseModel>().ReverseMap();
            CreateMap<Block, BlocksList>().ReverseMap();
            CreateMap<BlocksList, BlocksCommonModel>().ReverseMap();
            CreateMap<BlocksList, BlocksResponseModel>().ReverseMap();
            CreateMap<BlockIdListViewModel, BlockIdListResponseModel>().ReverseMap();
            CreateMap<BlocksStatusViewModel, BlockStatusListResModel>().ReverseMap();
            CreateMap<BlocksViewModel, BlocksResponseModel>().ReverseMap();
            CreateMap<ConnectedTemplatesResModel, BlocksResponseModel>().ReverseMap();
            CreateMap<TemplateViewModel, MasterTemplate>()
   .ForMember(dest => dest.Template, opt => opt.MapFrom(src => src.TemplateLogic)).ReverseMap();
            CreateMap<Template, TemplateListModel>().ReverseMap();
            CreateMap<t_engletterdata, PartnerClientData>().ReverseMap();
            CreateMap<BulkLettersResponse, BulkLetters>().ReverseMap();
            CreateMap<DraftLetters, BulkLetters>().ReverseMap();
            CreateMap<ClientFields, ClientFieldsViewModel>().ReverseMap();
            CreateMap<BulkLetterFieldsViewModel, BulkLetterFieldsResponseModel>().ReverseMap();
            CreateMap<LetterFieldValues, BulkLetterFieldsResponseModel>().ReverseMap();
            CreateMap<LetterFieldValues, LetterFieldValuesResponse>().ReverseMap();
            CreateMap<BulkLetterEditAttachmentsViewModel, BulkLetterEditAttachmentsResponseModel>().ReverseMap();
            CreateMap<BulkLetterAttachments, BulkLetterEditAttachmentsResponseModel>().ReverseMap();
            CreateMap<AddClientsViewModel, BulkLettersResponse>().ReverseMap();
            CreateMap<EditFieldsValues, LetterFieldValues>().ReverseMap();
            CreateMap<BatchRequestResponse, BatchRequest>().ReverseMap();
            CreateMap<BatchRequestViewModel, BatchRequestResponse>().ReverseMap();
            CreateMap<BulkLetters, EngagementLetter>().ReverseMap();

            CreateMap<SigningViewModel, SigningResModel>().ReverseMap();
            CreateMap<ManualRefreshViewModel, ManualRefreshResModel>().ReverseMap();
            CreateMap<HistoryLogViewModel, HistoryLogResponseModel>().ReverseMap();
            CreateMap<HistoryLogResponseModel, HistoryLog>().ReverseMap();
            CreateMap<EmailTemplate, EmailTemplateContent>().ReverseMap();
            CreateMap<CommonMailViewModel, CommonMailModel>().ReverseMap();
            CreateMap<EmailTempContent, EmailTemplateContent>().ReverseMap();
            CreateMap<LetterStatusRequest, LettersStatusReports>().ReverseMap();
            CreateMap<ChildEntityModel, ChildEntities>().ReverseMap();
            CreateMap<RemoveClientDataViewModel, RemoveClientDataResModel>().ReverseMap();

        }
    }
}
