﻿using OfficeOpenXml;
using System.Reflection;

namespace EngageCC.DashboardAPI.Helpers
{
    public class ExportExcel
    {
        public byte[] ExportToExcel<T>(List<T> data, string excelFileName, string protectionPassword)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            using (ExcelPackage package = new ExcelPackage(memoryStream))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Sheet1");

                PropertyInfo[] properties = typeof(T).GetProperties();

                for (int col = 0; col < properties.Length; col++)
                {
                    worksheet.Cells[1, col + 1].Value = properties[col].Name;
                }

                int row = 2;
                foreach (var item in data)
                {
                    for (int col = 0; col < properties.Length; col++)
                    {
                        worksheet.Cells[row, col + 1].Value = properties[col].GetValue(item);
                    }
                    row++;
                }

                // Protect only the first column and the first row
                for (int col = 2; col <= properties.Length + 1; col++)
                {
                    worksheet.Column(col).Style.Locked = true;
                }

                for (int rowIdx = 2; rowIdx <= data.Count + 1; rowIdx++)
                {
                    worksheet.Row(rowIdx).Style.Locked = true;
                }

                worksheet.Protection.IsProtected = true;

                worksheet.Column(2).Style.Locked = false;
                worksheet.Column(3).Style.Locked = false;
                worksheet.Column(4).Style.Locked = false;

                worksheet.Column(2).AutoFit();
                worksheet.Column(3).AutoFit();
                worksheet.Column(4).AutoFit();

                package.Save();
                return memoryStream.ToArray();
            }
        }
    }
}
