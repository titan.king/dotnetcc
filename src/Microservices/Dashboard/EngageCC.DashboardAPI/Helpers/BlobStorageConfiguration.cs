﻿namespace EngageCC.DashboardAPI.Helpers
{
    public class BlobStorageConfiguration
    {
        public string ConnectionString { get; }

        public BlobStorageConfiguration(IConfiguration configuration)
        {
            ConnectionString = configuration.GetConnectionString("AzureBlobStorage")??string.Empty;
        }

    }
}
