﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.Login
{
    public class ScreenPermissionsViewModel
    {
        public int ScreenId { get; set; }
        [Required(ErrorMessage = "ScreenName is required.")]
        public string ScreenName { get; set; } = string.Empty;
        public int ActionId { get; set; }
        public string ActionName { get; set; } = string.Empty;
        public bool isHavingPermission { get; set; }
    }
}
