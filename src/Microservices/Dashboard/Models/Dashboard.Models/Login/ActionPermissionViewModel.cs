﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.Login
{
    public class ActionPermissionViewModel
    {
        public int ScreenId { get; set; }
        [Required(ErrorMessage = "Screen Name is required.")]
        public string ScreenName { get; set; } = string.Empty;
        public string Permissions { get; set; } = string.Empty;
    }
}
