﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Models.Login
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Client ID is required.")]
        public string ClientId { get; set; } = string.Empty;
        public string GroupIds { get; set; } = string.Empty;
        public int UserId { get; set; }
        public List<string>? ScreenNames { get; set; }
        public List<string>? Roles { get; set; }
    }
}
