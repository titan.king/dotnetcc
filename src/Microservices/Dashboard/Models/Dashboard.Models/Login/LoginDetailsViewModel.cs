﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Models.Login
{
    public class LoginDetailsViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "User ID is required.")]
        public string UserId { get; set; } = string.Empty;
        public DateTime LoggedInTime { get; set; }
        public string LoggedInUserName { get; set; } = string.Empty;
        public string LoggedInInstance { get; set; } = string.Empty;
        public bool? LoggedOff { get; set; }
        public DateTime? LoggedOffTime { get; set; }
        public int LoggedIn { get; set; }
    }
}
