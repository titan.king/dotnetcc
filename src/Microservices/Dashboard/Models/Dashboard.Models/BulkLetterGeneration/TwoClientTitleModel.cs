﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.BulkLetterGeneration
{
    public class TwoClientTitleModel
    {
        public int BatchId { get; set; }
        public int BulkLettersId { get; set; }
        public int? ClientId { get; set; }
        public string? ClientName { get; set; }
        public string? SignatoryFirstName { get; set; }
        public string? SignatoryLastName { get; set; }
        public string? Address { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public string? Zip { get; set; }
        public string? SpouseFirstName { get; set; }
        public string? SpouseLastName { get; set; }
        public string? SignatoryEmailId { get; set; }
        [MaxLength(255)]
        public string? SpouseEmailId { get; set; }
        public string? TaxYear { get; set; }
        public string? Date { get; set; }
        public string? IsEsigning { get; set; }
        public int? PartnerId { get; set; }
        public string? PartnerName { get; set; }
        public int? OfficeId { get; set; }
        public string? OfficeName { get; set; }
        public string? OfficeAddress { get; set; }
        public string? OfficeCity { get; set; }
        public string? OfficeState { get; set; }
        public string? OfficeZipCode { get; set; }
        public string? OfficePhoneNumber { get; set; }
        public string? Jurisdiction { get; set; }
        public string? Child1 { get; set; }
        public string? Child2 { get; set; }
        public string? Child3 { get; set; }
        public string? Child4 { get; set; }
        public string? Child5 { get; set; }
        public string? Child6 { get; set; }
        public string? Child7 { get; set; }
        public string? Child8 { get; set; }
        public string? Child9 { get; set; }
        public string? Child10 { get; set; }
        public string? Child11 { get; set; }
        public string? Child12 { get; set; }
        public string? Child13 { get; set; }
        public string? Child14 { get; set; }
        public string? Child15 { get; set; }
        public string? Child16 { get; set; }
        public string? Child17 { get; set; }
        public string? Child18 { get; set; }
        public string? Child19 { get; set; }
        public string? Child20 { get; set; }
        public string? Child21 { get; set; }
        public string? Child22 { get; set; }
        public string? Child23 { get; set; }
        public string? Child24 { get; set; }
        public string? Child25 { get; set; }
        public string? Child26 { get; set; }
        public string? Child27 { get; set; }
        public string? Child28 { get; set; }
        public string? Child29 { get; set; }
        public string? Child30 { get; set; }
        public string? Child31 { get; set; }
        public string? Child32 { get; set; }
        public string? Child33 { get; set; }
        public string? Child34 { get; set; }
        public string? Child35 { get; set; }
        public string? Child36 { get; set; }
        public string? Child37 { get; set; }
        public string? Child38 { get; set; }
        public string? Child39 { get; set; }
        public string? Child40 { get; set; }
    }
}
