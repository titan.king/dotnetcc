﻿using Dashboard.Models.Clients;

namespace Dashboard.Models.BulkLetterGeneration
{
    public class BulkDraftLetterResponse
    {
        public List<DraftLetters> DraftLetters { get; set; }
        public List<PartnersNames> PartnersNames { get; set; }
        public int BatchId { get; set; }
        public string ClientIdErrorList { get; set; }
        public string? GetCurrentDate { get; set; }
        public List<ReturnTypeModel> ReturnType { get; set; }
    }
    public class DraftLetters
    {
        public int BulkLettersId { get; set; }
        public int? ClientId { get; set; }
        public string? ClientName { get; set; }
        public string? Office { get; set; }
        public string? SignatoryEmailId { get; set; }
        public string? SignatoryFirstName { get; set; }
        public string? PartnerName { get; set; }
        public int? ClientSignatureCount { get; set; }
        public int BatchId { get; set; }
        public string? TemplateName { get; set; }
        public string? FieldJson { get; set; }
        public string? DocumentStatusName { get; set; }
        public bool IsDraft { get; set; } = false;
        public bool IsDeleted { get; set; } = false;
        public bool IsUpdated { get; set; } = false;
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string CreatedBy { get; set; } = string.Empty;
        public string? ModifiedBy { get; set; } = string.Empty;
        public DateTime? DeletedOn { get; set; }
        public string? DeletedBy { get; set; } = string.Empty;
        public int TemplateId { get; set; }
        public int? OfficeId { get; set; }
        public string? TaxYear { get; set; }
        public bool? IsCanabisAvailable { get; set; }
        public string? ReturnTypeCode { get; set; }
    }
    public class PartnersNames
    {
        public string? PartnerId { get; set; }
        public string? PartnerName { get; set; } = string.Empty;
    }
}
