﻿namespace Dashboard.Models.BulkLetterGeneration
{
    public class PartnerOfficeResponse
    {
        public string PartnerName { get; set; }
        public string Office { get; set; }
    }
}
