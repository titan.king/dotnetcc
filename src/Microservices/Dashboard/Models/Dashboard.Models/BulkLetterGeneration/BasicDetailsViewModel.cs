﻿namespace Dashboard.Models
{
    public class BasicDetailsViewModel
	{
		public int BatchId { get; set; }	

		public string PartnerName { get; set; } = string.Empty;

        public string AdminName { get; set; } = string.Empty;

        public string TemplateName { get; set; } = string.Empty;
    }
}
