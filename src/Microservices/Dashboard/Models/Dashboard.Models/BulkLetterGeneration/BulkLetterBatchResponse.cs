﻿namespace Dashboard.Models.BulkLetterGeneration
{
    public class BulkLetterBatchResponse
    {
        public int BatchId { get; set; }
        public string? Admin { get; set; }
        public string? PartnerName { get; set; }
        public string? Office { get; set; }
        public string? TemplateName { get; set; }
        public int NoOfLetters { get; set; }
        public string LastUpdatedOn { get; set; } = string.Empty;
        public string? DocumentStatus { get; set; }
        public int PartnerCount { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}
