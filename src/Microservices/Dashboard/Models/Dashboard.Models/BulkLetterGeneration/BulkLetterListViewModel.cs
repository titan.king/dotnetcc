﻿namespace Dashboard.Models
{
    public class BulkLetterListViewModel
    {   
        public int RowId { get; set; }
        public int ClientId { get; set; }
        public string ClientName { get; set; } = string.Empty;
        public string OfficeName { get; set; } = string.Empty;
        public string? SignatoryEmailId { get; set; }
        public string? SignatoryFirstName { get; set; }
        public string PartnerName { get; set; } = string.Empty;

    }
}
