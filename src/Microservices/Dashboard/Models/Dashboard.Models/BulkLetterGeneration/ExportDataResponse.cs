﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.BulkLetterGeneration
{
    public class ExportDataResponse
    {
        
        public List<TwoClientExportData> twoClientExport { get; set; }
        public List<OneClientExportData> oneClientExport { get; set; }
        public List<OneClientTitleModel> oneClientTitlesres { get; set; }
        public List<TwoClientTitleModel> twoClientTitleres { get; set; }

    }

}
