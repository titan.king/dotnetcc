﻿using System;

namespace Dashboard.Models
{
	public class BatchDetailsViewModel
    {
		public int BatchId { get; set; }

		public string PartnerName { get; set; } = string.Empty;

		public string OfficeName { get; set; } = string.Empty;

		public string TemplateName { get; set; } = string.Empty;

		public int LetterCount { get; set;}

		public DateTime LastUpdateOn { get; set;}

		public string BatchStatus { get; set; } = string.Empty;
	}
}
