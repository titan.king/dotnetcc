﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.BulkLetterGeneration
{
    public class BulkLetterFieldsViewModel
    {
        public Fieldlist[]? FieldList { get; set; }
        public string Esigning { get; set; } = string.Empty;
        public string Is7216Available { get; set; } = string.Empty;
        public int? ClientSignatureCount { get; set; }
        public int? ClientId { get; set; }
        public int? PartnerId { get; set; }
        public int? BulkLettersId { get; set; }
        public int? LetterVersion { get; set; }
    }
}
