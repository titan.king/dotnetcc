﻿using Dashboard.Models.Fields;
using System.ComponentModel.DataAnnotations;

namespace Dashboard.Models.BulkLetterGeneration
{
    public class BulkLettersResponse
    {
        public int BulkLettersId { get; set; }
        public int? ClientId { get; set; }
        public int? PartnerId { get; set; }
        public string? ClientName { get; set; }
        public int? OfficeId { get; set; }
        public string? Office { get; set; }
        public string? SignatoryEmailId { get; set; }
        public string? SignatoryFirstName { get; set; }
        public string? SignatoryLastName { get; set; }
        public string? SignatoryTitle { get; set; }
        public string? SpouseEmail { get; set; }
        public string? SpouseLastName { get; set; }
        public string? PartnerName { get; set; }
        public int BatchId { get; set; }
        public string? TemplateName { get; set; }
        public string? FieldJson { get; set; }
        public string? DocumentStatusName { get; set; }
        public bool IsDraft { get; set; } = false;
        public bool IsDeleted { get; set; } = false;
        public bool IsUpdated { get; set; } = false;
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string CreatedBy { get; set; } = string.Empty;
        public string? ModifiedBy { get; set; } = string.Empty;
        public DateTime? DeletedOn { get; set; }
        public string? DeletedBy { get; set; } = string.Empty;
        public bool Is7216Available { get; set; } = false;
        public int? ClientSignatureCount { get; set; }
        public bool IsNewClient { get; set; } = false;
        [MaxLength(255)]
        public string? SpouseFirstName { get; set; }
        [MaxLength(255)]
        public string? SpouseEmailId { get; set; }
        public bool? IsEsigning { get; set; }
        public string? TaxYear { get; set; }
        public List<LetterFieldValuesResponse> letterFieldValues { get; set; }
        public bool IsBatchActive { get; set; } = false;
        public bool IsBatchDelete { get; set; } = false;

        public string? OfficePhoneNumber { get; set; }
        public string? OfficeZipCode { get; set; }
        public string? OfficeState { get; set; }
        public string? OfficeCity { get; set; }
        public string? OfficeAddress { get; set; }
        public string? OfficeName { get; set; }
        public string? Date { get; set; }
        public string? Address { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public string? Zip { get; set; }
        public string? Jurisdiction { get; set; }
        public bool? IsCanabisAvailable { get; set; }
        public string? ReturnTypeCode { get; set; }
        public string? City_State_Zip { get; set; }
        public bool? IsSignatoryTitle { get; set; }
    }
}
