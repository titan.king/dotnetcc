﻿namespace Dashboard.Models.BulkLetterGeneration
{
    public class ActiveUserRequest
    {
        public int BatchId { get; set; }
        public string UserName { get; set; } = string.Empty;
    }
}
