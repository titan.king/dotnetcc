﻿namespace Dashboard.Models.BulkLetterGeneration
{
    public class CityStateZipInfo
    {
        public string? City { get; set; }
        public string? State { get; set; }
        public string? Zip { get; set; }
    }
}
