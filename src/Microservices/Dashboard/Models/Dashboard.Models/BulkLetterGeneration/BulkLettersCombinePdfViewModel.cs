﻿using Dashboard.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.BulkLetterGeneration
{
    public class BulkLettersCombinePdfViewModel
    {
        public string LetterHtmlContent { get; set; } = string.Empty;
        public int BatchId { get; set; }
        public int BulkLettersId { get; set; }
        public int TemplateId { get; set; }
        public string TemplateName { get; set; } = string.Empty;
        public int? ClientId { get; set; }
        public string ClientName { get; set; } = string.Empty;
        public List<AttachmentBase64String>? UploadAttachmentJSON { get; set; }
    }
}
