﻿using System.Net;

namespace Dashboard.Models.CommonModel
{
    public class ResponseModel
    {
        public HttpStatusCode StatusCode { get; set; }
        public bool Status { get; set; }
        public object? Data { get; set; }
        public string? ErrorMessage { get; set; }
        public string PdfUrl { get; set; } = string.Empty;

        public ResponseModel()
        { 
        }
        public ResponseModel(HttpStatusCode StatusCode)
        {
            this.StatusCode = StatusCode;
        }
        public ResponseModel(HttpStatusCode StatusCode, string ErrorMessage)
        {
            this.StatusCode = StatusCode;
            this.ErrorMessage = ErrorMessage;
        }
    }
}
