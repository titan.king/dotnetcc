﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.CommonModel
{
    public class CommonMailViewModel
    {
        public int DocumentStatusId { get; set; }
        public string FromMail { get; set; }
        public string FromName { get; set; }
        public string Subject { get; set; }
        public string ToIds { get; set; }
        public string LetterName { get; set; }
        public string StatusContent { get; set; }
        public string LinkContent { get; set; }
        public int EngagementLetterId { get; set; }
        public string pdfContent { get; set; }
        public string SubjectAppend { get; set; }
        public string MJAPIKey { get; set; }
        public string MJAPISecret { get; set; }
        public string Changesubject { get; set; }
        public EmailTemplateContent? EmailTemplateContent { get; set; }
    }
    public class EmailTemplateContent
    {
        public int EmailTemplateId { get; set; }
        public int DocumentStatusId { get; set; }
        public string EmailSubject { get; set; }
        public string EmailContent { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
