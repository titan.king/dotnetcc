﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.ResponseModel
{
    public class AttachmentBase64String
    {
        public int Attachment { get; set; }
        public string Filename { get; set; } = string.Empty;
        public string FileId { get; set; } = string.Empty;
        public string Base64String { get; set; } = string.Empty;
        public string attachPdfScr { get; set; } = string.Empty;
    }
}
