﻿using System.Net;

namespace Dashboard.Models.ResponseModel
{
    public class EditBlocksResModel
    {
        public int EngagementLetterId { get; set; }
        public string BlockText { get; set; } = string.Empty;
        public string BlockName { get; set; } = string.Empty;
        public string Content { get; set; } = string.Empty; 
        public DateTime ModifiedOn { get; set; }
        public bool EditedOnlyForThisLetter { get; set; }
        public int EditedBlockID { get; set; }
        public string EditedHtmlContent { get; set; } = string.Empty;
        public string EditedColourHtmlContent { get; set; } = string.Empty;
        public string ModifiedColourjsonContent { get; set; } = string.Empty;
        public string TransactionStatus { get; set; } = string.Empty;
        public int? DepartmentId { get; set; }
        public int? EngageTypeId { get; set; }
        public int? DocumentId { get; set; }
        public string TemplateJSON { get; set; } = string.Empty;
        public string RemovedTagsOnTemplateJSON { get; set; } = string.Empty;
        public bool ColorTagsRemovedforTempJSON { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public string ErrorMessage { get; set; } = string.Empty;
        public string ErrorPopupMessage { get; set; } = string.Empty;
    }
}
