﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.ResponseModel
{
    public class Metadatas
    {
        public string type { get; set; } = string.Empty;
        public int size { get; set; }
    }
}
