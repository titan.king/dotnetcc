﻿namespace Dashboard.Models.ResponseModel
{
    public class ColumnNamesResModel
    {
        public int ColumnId { get; set; }
        public string? ColumnNames { get; set; } = string.Empty;
    }
}
