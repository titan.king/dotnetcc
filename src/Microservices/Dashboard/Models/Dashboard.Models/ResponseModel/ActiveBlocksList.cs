﻿namespace Dashboard.Models.ResponseModel
{
    public class ActiveBlocksList
    {
        public int BlockId { get; set; }
        public string BlockName { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string? StatusName { get; set; } = string.Empty;
        public string ChangeNotes { get; set; } = string.Empty;
        public string Content { get; set; } = string.Empty;
    }
}
