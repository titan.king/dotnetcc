﻿using System.Net;

namespace Dashboard.Models.ResponseModel
{
    public class SigningResModel
    {
        public int EngagementLetterId { get; set; }
        public string? SigningPartnerName { get; set; }
        public string? SigningPartnerSignatureImage { get; set; }
        public string? DelegatedName { get; set; }
        public string? SPEmail { get; set; }
        public string? DelegatedEmail { get; set; }
        public string? ContactPerson1Name { get; set; }
        public string? ContactPerson1Email { get; set; }
        public string? ContactPerson2Name { get; set; }
        public string? ContactPerson2Email { get; set; }
        public string? Title { get; set; }
        public string? Message { get; set; }
        public string? FileId { get; set; }
        public string? DocumentId { get; set; }
        public string? Status { get; set; }
        public bool? IsDeleted { get; set; }
        public bool? ManualSigning { get; set; }
        public string? CreatedBy { get; set; }
        public string? ModifiedBy { get; set; }
        public string? DeletedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public DateTime? DeletedOn { get; set; }
        public string? document_hash { get; set; }
        public string? EngagementLetterName { get; set; }
        public string? AdobeSignLink { get; set; }
        public string? AdobeIntegrationKey { get; set; }
        public string? AdobeDeclineLink { get; set; }
        public string? AgreementStatus { get; set; }
        public string? SigningRejectState { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public string? ErrorMessage { get; set; }

    }
}
