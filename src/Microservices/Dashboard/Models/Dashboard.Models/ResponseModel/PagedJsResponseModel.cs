﻿using System.Net;

namespace Dashboard.Models.ResponseModel
{
    public class PagedJsResponseModel
    {
        public string success { get; set; } = string.Empty;
        public object? Data { get; set; }
        public string payload { get; set; } = string.Empty;
        public HttpStatusCode StatusCode { get; set; }
    }
}
