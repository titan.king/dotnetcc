﻿namespace Dashboard.Models.ResponseModel
{
    public class FieldsDataType
    {
        public int FieldDataTypeId { get; set; }
        public string FieldType { get; set; } = string.Empty;
    }
}
