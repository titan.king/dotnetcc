﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.ResponseModel
{
    public class Asset
    {
        public Metadata metadata { get; set; }
        public string downloadUri { get; set; } = string.Empty;
        public string assetID { get; set; } = string.Empty;
    }
}
