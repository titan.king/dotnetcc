﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.ResponseModel
{
    public class EngageTypesList
    {
        public int EngageTypeId { get; set; }
        
        public string EngageTypeName { get; set; } = string.Empty;
    }
}
