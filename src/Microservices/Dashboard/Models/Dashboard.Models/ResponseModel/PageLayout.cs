﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.ResponseModel
{
    public class PageLayout
    {
        public string pageWidth { get; set; }
        public string pageHeight { get; set; }
    }
}
