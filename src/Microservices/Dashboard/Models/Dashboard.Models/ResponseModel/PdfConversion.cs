﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.ResponseModel
{
    public class PdfConversion
    {
        public string inputUrl { get; set; } = string.Empty;
        public string json { get; set; } = string.Empty;
        public bool includeHeaderFooter { get; set; }
        public PageLayout pageLayout { get; set; }
    }
}
