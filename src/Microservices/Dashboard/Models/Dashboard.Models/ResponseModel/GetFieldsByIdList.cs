﻿namespace Dashboard.Models.ResponseModel
{
    public class GetFieldsByIdList
    {
        public List<FieldListModel> FieldsListById {  get; set; }
        public List<BlocksList> FieldsConnectedBlocks { get; set; }
    }
}
