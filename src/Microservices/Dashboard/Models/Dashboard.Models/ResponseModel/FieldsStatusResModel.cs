﻿namespace Dashboard.Models.ResponseModel
{
    public class FieldsStatusResModel
    {
        public List<FieldsStatus>? FieldsStatus { get; set; }
        public List<FieldsDataType>? FieldsDataType { get; set; }
    }
}
