﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Models.ResponseModel
{
    public class DepartmentsList
    {
        public int DepartmentId { get; set; }

        public string Department { get; set; } = string.Empty;
    }
}
