﻿namespace Dashboard.Models.ResponseModel
{
    public class DeleteBulkLetterResModel
    {
        public int BulkLetterId { get; set; }
        public string DeletedBy { get; set; } = string.Empty;
        public bool IsDeleted { get; set; }
        public DateTime DeletedOn { get; set; }
    }
}
