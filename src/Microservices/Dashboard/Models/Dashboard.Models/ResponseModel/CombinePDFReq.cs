﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.ResponseModel
{
    public class CombinePDFReq
    {
        public string TemplateName { get; set; } = string.Empty;
        public string LetterHTMLAssetID1 { get; set; } = string.Empty;
        public string Attachment1 { get; set; } = string.Empty;
        public string Attachment2 { get; set; } = string.Empty;
        public string Attachment3 { get; set; } = string.Empty;
        public string Attachment4 { get; set; } = string.Empty;
        public string Attachment5 { get; set; } = string.Empty;
        public string AssetId7216Form { get; set; } = string.Empty;
        public int LetterId { get; set; }
    }
}
