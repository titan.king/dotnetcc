﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.ResponseModel
{
    public class AttachmentPdfFile
    {
        public List<AttachmentBase64String> UploadAttachmentJSON { get; set; }
    }
}
