﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.ResponseModel
{
    public class AdobePDFStatusResponse
    {
        public string status { get; set; } = string.Empty;
        public Asset asset { get; set; }
    }
}
