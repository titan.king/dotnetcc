﻿namespace Dashboard.Models.ResponseModel
{
    public class CancleSigningResModel
    {
        public int EngagementLetterId { get; set; }
        public string ModifiedBy { get; set; } = string.Empty;
        public string PdfUrl { get; set; } = string.Empty;
    }
}
