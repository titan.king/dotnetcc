﻿namespace Dashboard.Models.ResponseModel
{
    public class ConnectedTemplatesResModel
    {
        public int TemplateId { get; set; }
        public string TemplateName { get; set; } = string.Empty;
        public string TemplateDescription { get; set; } = string.Empty;
        public string DepartmentName { get; set; } = string.Empty;
        public string EngageTypeName { get; set; } = string.Empty;
        public string StatusName { get; set; } = string.Empty;
        public string ChangeNotes { get; set; } = string.Empty;
        public string Content { get; set; } = string.Empty;
        public string TemplateLogic { get; set; } = string.Empty;
        public string CreatedBy { get; set; } = string.Empty;   
        public int DepartmentId { get; set; }
        public int EngageTypeId { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
