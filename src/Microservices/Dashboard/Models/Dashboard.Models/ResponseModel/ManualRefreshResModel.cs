﻿namespace Dashboard.Models.ResponseModel
{
    public class ManualRefreshResModel
    {
        public int EngagementLetterId { get; set; }
        public string IsRefreshButtonClicked { get; set; } = string.Empty;  
        public string PdfUrl { get; set; } = string.Empty;
        public string ModifiedBy { get; set; } = string.Empty;


    }
}
