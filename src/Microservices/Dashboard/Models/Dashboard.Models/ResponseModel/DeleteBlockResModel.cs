﻿namespace Dashboard.Models.ResponseModel
{
    public class DeleteBlockResModel
    {
        public int[] BlockId { get; set; }
        public string DeletedBy { get; set; } = string.Empty;
        public bool IsDeleted { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
