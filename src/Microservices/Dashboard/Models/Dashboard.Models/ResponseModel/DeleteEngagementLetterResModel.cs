﻿namespace Dashboard.Models.ResponseModel
{
    public class DeleteEngagementLetterResModel
    {
        public int EngagementLetterId { get; set; }
        public string DeletedBy { get; set; } = string.Empty;
        public bool IsDeleted { get; set; }
        public DateTime DeletedOn { get; set; } = DateTime.Now;
    }
}
