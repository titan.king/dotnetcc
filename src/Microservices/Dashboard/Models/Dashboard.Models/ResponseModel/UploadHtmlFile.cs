﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.ResponseModel
{
    public class UploadHtmlFile
    {
        public string Base64Html { get; set; } = string.Empty;
        public string FileName { get; set; } = string.Empty;
        public bool? IsNotToDecode { get; set; } = false;
    }
}
