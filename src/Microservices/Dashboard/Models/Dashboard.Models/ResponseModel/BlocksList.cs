﻿namespace Dashboard.Models.ResponseModel
{
    public class BlocksList
    {
        public int BlockId { get; set; }
        public string BlockName { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string Content { get; set; } = string.Empty;
        public string ChangeNotes { get; set; } = string.Empty; 
        public bool IsDeleted { get; set; }
        public int StatusId { get; set; }
        public string? StatusName { get; set; } = string.Empty; 
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string CreatedBy { get; set; } = string.Empty;
        public string DeletedBy { get; set; } = string.Empty;
        public DateTime? DeletedOn { get; set; }
        public string ModifiedBy { get; set; } = string.Empty;
        public string ConnectedTemplates { get; set; } = string.Empty;
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set;} = string.Empty;
        public int EngageTypeId { get; set; }
        public string EngageTypeName { get; set; } = string.Empty;

    }
}
