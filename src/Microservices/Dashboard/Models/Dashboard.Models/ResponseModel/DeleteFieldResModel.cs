﻿namespace Dashboard.Models.ResponseModel
{
    public class DeleteFieldResModel
    {
        public int[] FieldId { get; set; }
        public string DeletedBy { get; set; } = string.Empty;
        public bool IsDeleted { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
