﻿namespace Dashboard.Models.ResponseModel
{
    public class PdfConversionRequest
    {
        public int EngagementLetterId { get; set; }
        public PdfConversion? pdfConversion { get; set; }
    }
}
