﻿namespace Dashboard.Models.ResponseModel
{
    public class UploadLetterResModel
    {
        public int EngagementLetterId { get; set; }
        public string UploadPDFContent { get; set; } = string.Empty;
        public string EditedBy { get; set; } = string.Empty;
        public string PDFbyteArray { get; set; } = string.Empty;
        public string PdfUrl { get; set; } = string.Empty;
        public byte[]? UploadPDFdocument { get; set; }
        public string AdobeStatus { get; set; } = string.Empty;


    }
}
