﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.ResponseModel
{
    public class HistoryLogResponseModel
    {
        public int HistoryLogId { get; set; }
        public int? EngagementLetterId { get; set; }
        public string? EngagementLetterName { get; set; }
        public int? BatchId { get; set; }
        public string? Status { get; set; }
        public string? EditedBy { get; set; }
        public int? Version { get; set; }
        public DateTime? LastModified { get; set; }
        public string? ModifiedOn { get; set; }
        public DateTime? Downloaded { get; set; }
        public bool? Delegated { get; set; }
        public string? ClientEmailId { get; set; }
        public string? ReasonforDecline { get; set; }
        public string? DeclineTimestamp { get; set; }
        public string? PDFUrl { get; set; }
        public int? BulkLettersId { get; set; }
        public int? TemplateId { get; set; }
        public string? TemplateName { get; set; }
        public int? ClientId { get; set; }
        public string? ClientName { get; set; } = string.Empty;
    }
}
