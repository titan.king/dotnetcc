﻿namespace Dashboard.Models.ResponseModel
{
    public class BlockIdListResponseModel
    {
        public int BlockId { get; set; }
    }
}
