﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.ResponseModel
{
    public class TemplateListResponseModel
    {
        public int TemplateId { get; set; }
        public string? TemplateName { get; set; } = string.Empty;
        public string? TemplateDescription { get; set; } = string.Empty;
        public int DepartmentId { get; set; }
        public string? DepartmentName { get; set; } = string.Empty;
        public int EngageTypeId { get; set; }
        public string? EngageTypeName { get; set; } = string.Empty;
        public int StatusId { get; set; }
        public string? StatusName { get; set; } = string.Empty;
        public string? ChangeNotes { get; set; } = string.Empty;
        public bool IsDeleted { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string CreatedBy { get; set; } = string.Empty;
        public string? TemplateLogic { get; set; } = string.Empty;
        public string ConnectedTemplates { get; set; } = string.Empty;

    }
}
