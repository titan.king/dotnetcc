﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.ResponseModel
{
    public class AdobeResponse
    {
        public string uploadUri { get; set; } = string.Empty;
        public string assetID { get; set; } = string.Empty;
        public string AttachemntPDFcontent { get; set; } = string.Empty;
    }
}
