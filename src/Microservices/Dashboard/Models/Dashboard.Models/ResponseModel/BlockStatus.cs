﻿namespace Dashboard.Models.ResponseModel
{
    public class BlockStatus
    {
        public int StatusId { get; set; }
        public string? StatusName { get; set; } = string.Empty;  
    }
}
