﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.ResponseModel
{
    public class ResponseUrlModel
    {
        public string PdfUrl { get; set; } = string.Empty;
    }
}
