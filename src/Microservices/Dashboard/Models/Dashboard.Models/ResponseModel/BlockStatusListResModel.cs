﻿namespace Dashboard.Models.ResponseModel
{
    public class BlockStatusListResModel
    {    
       public List<BlockStatus>? BlockStatus { get; set; }
    }
}
