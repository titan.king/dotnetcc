﻿namespace Dashboard.Models.ResponseModel
{
    public class FieldsStatus
    {
       public int StatusId { get; set; }
       public string? StatusName { get; set; } = string.Empty;
    }
}
