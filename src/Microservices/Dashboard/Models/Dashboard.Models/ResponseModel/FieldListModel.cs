﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Models.ResponseModel
{
    public class FieldListModel
    {
        public int FieldId { get; set; }
        public string? FieldName { get; set; }
        public string? DisplayName { get; set; }
        public int? FieldDataTypeId { get; set; }
        public string? FieldDataType { get; set; } 
        public string? HintText { get; set; }
        public string? ChangeNotes { get; set; }
        public bool IsDeleted { get; set; }
        public int StatusId { get; set; }
        public string? StatusName { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string? TransactionStatus { get; set; }
        public string? ConnectedBlocks { get; set; }
        public bool DirectInput { get; set; }
        public bool MDDLookup { get; set; }
        public int? ColumnId { get; set; }
        public string? MDDColumnNames { get; set; }
        public object? MDDColumnData { get; set; }
        public string? ModifiedBy { get; set; }
        public string? CreatedBy { get; set; }
        public string? InputType { get; set; } 

    }
}
