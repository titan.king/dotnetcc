﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.ResponseModel
{
    public class LoginListModel
    {
        public string ClientId { get; set; } = string.Empty;
        public string GroupIds { get; set; } = string.Empty;
        public int UserId { get; set; }
        public List<string>? ScreenNames { get; set; } = new List<string>();
        public List<string>? Roles { get; set; } = new List<string>();
    }
}
