﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.ResponseModel
{
    public class AccessTokenResponse
    {
        public string token_type { get; set; } = string.Empty;
        public string access_token { get; set; } = string.Empty;
        public int expires_in { get; set; }
    }
}
