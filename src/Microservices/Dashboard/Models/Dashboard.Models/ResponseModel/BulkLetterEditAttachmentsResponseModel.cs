﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.ResponseModel
{
    public class BulkLetterEditAttachmentsResponseModel
    {
        public int BulkLetterAttachmentId { get; set; }
        public int BatchId { get; set; }
        public int TemplateId { get; set; }
        public string? AttachmentsJSON { get; set; } = string.Empty;
        public string? AttachmentsURL { get; set; } = string.Empty;
        public int? VersionNumber { get; set; }
    }
}
