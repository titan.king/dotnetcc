﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.ResponseModel
{
    public class LetterHTMLRequestModel
    {
        public string LetterHtmlContent { get; set; } = string.Empty;
        public string TemplateName { get; set; } = string.Empty;
        public int LetterId { get;set; }
    }
}
