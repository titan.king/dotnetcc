﻿namespace Dashboard.Models.ResponseModel
{
    public class BlocksCommonModel
    {
        public List<BlocksList> BlocksById { get; set; }
        public List<TemplateListResponseModel> ConnectedTemplate { get; set; }
    }
}
