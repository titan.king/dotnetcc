﻿namespace Dashboard.Models.ResponseModel
{
    public class CheckSigningStatusResModel
    {
       public int EngagementLetterId { get; set; }
       public string? EngagementLetterName { get; set; }
       public int? Version { get; set; }
       public string? PdfURL { get; set; }
       public string? DelegatedName { get; set; }
       public string? SigningPartnerName { get; set; }
       public string? AgreementId { get; set; }
       public string ModifiedBy { get; set; } = string.Empty;
    }
}
