﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Models.ManageSignature
{
    public class ManageSignatureViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Signing Partner is required.")]
        public string SigningPartner { get; set; } = string.Empty;
        public string Status { get; set; } = string.Empty;
        public string ChangeNotes { get; set; } = string.Empty;
        public string SignatureImage { get; set; } = string.Empty;
        public DateTime ModifiedOn { get; set; }
        public bool IsDeleted { get; set; }
        public string ImageName { get; set; } = string.Empty;
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; } = string.Empty;
        public string ModifiedBy { get; set; } = string.Empty;
        public string TransactionStatus { get; set; } = string.Empty;
    }
}
