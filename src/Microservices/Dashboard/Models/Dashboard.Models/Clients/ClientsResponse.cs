﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.Clients
{
    public class ClientsResponse
    {
        public HttpStatusCode StatusCode { get; set; }
        public bool Status { get; set; }
        public Data? Data { get; set; }
        public string? ErrorMessage { get; set; }
    }
    public class Data
    {
        public object DataList { get; set; }
    }
}
