﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.Clients
{
    public class ExportDataRequest
    {
        public List<PartnersViewModel>? PartnersName { get; set; }
        public string? ClientName { get; set; }
        public List<TemplateExportFields>? TemplateFieldNames { get; set; }
        public string Admin { get; set; } = string.Empty;
        public string TemplateName { get; set; } = string.Empty;
        public int BatchId { get; set; }
        public int DocumentStatusId { get; set; }
        public string FieldJson { get; set; } = string.Empty;
        public bool IsNewBulk { get; set; } = true;
        public int ClientSignatureCount { get; set; }
    }
    public class TemplateExportFields
    {
        public string FieldName { get; set; } = string.Empty;
    }
}
