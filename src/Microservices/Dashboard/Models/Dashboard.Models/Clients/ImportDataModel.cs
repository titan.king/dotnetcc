﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.Clients
{
    public class ImportDataModel
    {
        public int BatchId { get; set; }
        public int BulkLettersId { get; set; }
        public int? ClientId { get; set; }
        public int? PartnerId { get; set; }
        public string? PartnerName { get; set; }
        public string? ClientName { get; set; }
        public int? OfficeId { get; set; }
        public string? OfficeName { get; set; }
        public string? SignatoryEmailId { get; set; }
        public string? SignatoryFirstName { get; set; }
        public string? SignatoryLastName { get; set; }
        public string? SignatoryTitle { get; set; }
        public string? SpouseLastName { get; set; }
        public string? SpouseFirstName { get; set; }
        [MaxLength(255)]
        public string? SpouseEmailId { get; set; }
        public bool? IsEsigning { get; set; }
        public string? TaxYear { get; set; }
        public string? OfficePhoneNumber { get; set; }
        public string? OfficeZipCode { get; set; }
        public string? OfficeState { get; set; }
        public string? OfficeCity { get; set; }
        public string? OfficeAddress { get; set; }
        public string? Date { get; set; }
        public string? Address { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public string? Zip { get; set; }
        public string? Jurisdiction { get; set; }
        public string? ChildEntities { get; set; }
    }
}
