﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Models.Clients
{
    public class PartnerClientMappingObject
    {
        public object PartnerID { get; set; } = string.Empty;
        public object PartnerName { get; set; } = string.Empty;
        public object ParentClientID { get; set; } = string.Empty;
        public object ParentClientName { get; set; } = string.Empty;
        public object ChildClient { get; set; } = string.Empty;
        public object AddressDetail { get; set; } = string.Empty;
        public object City { get; set; } = string.Empty;
        public object State { get; set; } = string.Empty;
        public object ZipCode { get; set; } = new object();
        public object ContactName { get; set; } = string.Empty;
        public object ContactEmail { get; set; } = string.Empty;
    }
}
