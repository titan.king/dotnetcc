﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.Clients
{
    public class PartnerClientData
    {
        public string? PartnerId { get; set; } = string.Empty;
        public string? PartnerName { get; set; } = string.Empty;
    }
}
