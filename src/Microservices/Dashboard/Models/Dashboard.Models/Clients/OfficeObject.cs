﻿namespace Dashboard.Models.Clients
{
    public class OfficeObject
    {
        public object id { get; set; } = string.Empty;
        public object name { get; set; } = string.Empty;
        public object office { get; set; } = string.Empty;
        public object address { get; set; } = string.Empty;
        public object city { get; set; } = string.Empty;
        public object state { get; set; } = string.Empty;
        public object zip_code { get; set; } = string.Empty;
        public object state_signature_name_address { get; set; } = string.Empty;
        public object state_signature_city { get; set; } = string.Empty;
        public object state_signature_state { get; set; } = string.Empty;
        public object state_signature_zip_code { get; set; } = string.Empty;
        public object phone_number { get; set; } = string.Empty;
    }
}
