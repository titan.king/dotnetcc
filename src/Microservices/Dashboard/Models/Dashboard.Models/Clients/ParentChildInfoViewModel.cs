﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.Clients
{
    public class ParentChildInfoViewModel
    {
        public int ParentChildInfoId { get; set; }
        public int? ParentClientId { get; set; }
        public string? Child1 { get; set; } = string.Empty;
        public string? Child2 { get; set; } = string.Empty;
        public string? Child3 { get; set; } = string.Empty;
        public string? Child4 { get; set; } = string.Empty;
        public string? Child5 { get; set; } = string.Empty;
        public string? Child6 { get; set; } = string.Empty;
        public string? Child7 { get; set; } = string.Empty;
        public string? Child8 { get; set; } = string.Empty;
        public string? Child9 { get; set; } = string.Empty;
        public string? Child10 { get; set; } = string.Empty;
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool? IsDeleted { get; set; } = false;
        public DateTime? DeletedOn { get; set; }
        public string ChildConcatenated { get; set; } = string.Empty;
    }
}
