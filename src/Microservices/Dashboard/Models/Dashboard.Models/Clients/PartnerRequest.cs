﻿namespace Dashboard.Models.Clients
{
    public class PartnerRequest
    {
        public List<PartnersViewModel>? PartnersName { get; set; }
        public List<TemplateFields>? TemplateFieldNames { get; set; }
        public string Admin { get; set; } = string.Empty;
        public string TemplateName { get; set; } = string.Empty;
        public int BatchId { get; set; }
        public int DocumentStatusId { get; set; }
        public string FieldJson { get; set; } = string.Empty;
        public bool IsNewBulk { get; set; } = true;
        public List<ReturnTpeCode>? ReturnTpeCode { get; set; }
        public bool? IsSignatoryTitle { get; set; }
        public int SignatureCounts { get; set; }
    }

    public class TemplateFields
    {
        public string FieldName { get; set; } = string.Empty;
    }
    public class ReturnTpeCode
    {
        public string? ReturnType { get; set; }
    }
}
