﻿namespace Dashboard.Models.Clients
{
    public class ParentChildObject
    {
        public object parent_client_id { get; set; } = string.Empty;
        public object child1 { get; set; } = string.Empty;
        public object child2 { get; set; } = string.Empty;
        public object child3 { get; set; } = string.Empty;
        public object child4 { get; set; } = string.Empty;
        public object child5 { get; set; } = string.Empty;
        public object child6 { get; set; } = string.Empty;
        public object child7 { get; set; } = string.Empty;
        public object child8 { get; set; } = string.Empty;
        public object child9 { get; set; } = string.Empty;
        public object child10 { get; set; } = string.Empty;
    }
}
