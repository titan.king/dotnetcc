﻿namespace Dashboard.Models.Clients
{
    public class ChildEntityModel
    {
        public string? ClientId { get; set; }
        public string? Child1 { get; set; }
        public string? Child2 { get; set; }
        public string? Child3 { get; set; }
        public string? Child4 { get; set; }
        public string? Child5 { get; set; }
        public string? Child6 { get; set; }
        public string? Child7 { get; set; }
        public string? Child8 { get; set; }
        public string? Child9 { get; set; }
        public string? Child10 { get; set; }
        public string? Child11 { get; set; }
        public string? Child12 { get; set; }
        public string? Child13 { get; set; }
        public string? Child14 { get; set; }
        public string? Child15 { get; set; }
        public string? Child16 { get; set; }
        public string? Child17 { get; set; }
        public string? Child18 { get; set; }
        public string? Child19 { get; set; }
        public string? Child20 { get; set; }
        public string? Child21 { get; set; }
        public string? Child22 { get; set; }
        public string? Child23 { get; set; }
        public string? Child24 { get; set; }
        public string? Child25 { get; set; }
        public string? Child26 { get; set; }
        public string? Child27 { get; set; }
        public string? Child28 { get; set; }
        public string? Child29 { get; set; }
        public string? Child30 { get; set; }
        public string? Child31 { get; set; }
        public string? Child32 { get; set; }
        public string? Child33 { get; set; }
        public string? Child34 { get; set; }
        public string? Child35 { get; set; }
        public string? Child36 { get; set; }
        public string? Child37 { get; set; }
        public string? Child38 { get; set; }
        public string? Child39 { get; set; }
        public string? Child40 { get; set; }
    }
    public class ChildEntities
    {
        public string? Child1 { get; set; }
        public string? Child2 { get; set; }
        public string? Child3 { get; set; }
        public string? Child4 { get; set; }
        public string? Child5 { get; set; }
        public string? Child6 { get; set; }
        public string? Child7 { get; set; }
        public string? Child8 { get; set; }
        public string? Child9 { get; set; }
        public string? Child10 { get; set; }
        public string? Child11 { get; set; }
        public string? Child12 { get; set; }
        public string? Child13 { get; set; }
        public string? Child14 { get; set; }
        public string? Child15 { get; set; }
        public string? Child16 { get; set; }
        public string? Child17 { get; set; }
        public string? Child18 { get; set; }
        public string? Child19 { get; set; }
        public string? Child20 { get; set; }
        public string? Child21 { get; set; }
        public string? Child22 { get; set; }
        public string? Child23 { get; set; }
        public string? Child24 { get; set; }
        public string? Child25 { get; set; }
        public string? Child26 { get; set; }
        public string? Child27 { get; set; }
        public string? Child28 { get; set; }
        public string? Child29 { get; set; }
        public string? Child30 { get; set; }
        public string? Child31 { get; set; }
        public string? Child32 { get; set; }
        public string? Child33 { get; set; }
        public string? Child34 { get; set; }
        public string? Child35 { get; set; }
        public string? Child36 { get; set; }
        public string? Child37 { get; set; }
        public string? Child38 { get; set; }
        public string? Child39 { get; set; }
        public string? Child40 { get; set; }
    }
}
