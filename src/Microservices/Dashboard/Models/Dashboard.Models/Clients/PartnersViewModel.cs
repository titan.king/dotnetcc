﻿namespace Dashboard.Models.Clients
{
    public class PartnersViewModel
    {
        public string? PartnerId { get; set; }
        public string? PartnerName { get; set; } = string.Empty;
    }
}
