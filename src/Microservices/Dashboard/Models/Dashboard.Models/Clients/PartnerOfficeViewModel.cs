﻿namespace Dashboard.Models.Clients
{
    public class PartnerOfficeViewModel
    {
        public int? PartnerId { get; set; }
        public string? PartnerName { get; set; } = string.Empty;
        public int? FieldId { get; set; }
        public string? OfficeName { get; set; } = string.Empty;
        public string? Jurisdiction { get; set; }
        public string? OfficeAddress { get; set; } = string.Empty;
        public string? OfficeCity { get; set; } = string.Empty;
        public string? OfficeState { get; set; } = string.Empty;
        public string? OfficeZipCode { get; set; } = string.Empty;
        public string? OfficePhoneNumber { get; set; } = string.Empty;
       
    }
}
