﻿namespace Dashboard.Models.Clients
{
    public class BatchIdsResponse
    {
        public int BatchId { get; set; }
    }
}
