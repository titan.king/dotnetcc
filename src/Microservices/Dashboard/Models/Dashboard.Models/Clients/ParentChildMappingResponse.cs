﻿namespace Dashboard.Models.Clients
{
    public class ParentChildMappingResponse
    {
        public string? ChildClientName { get; set; }
    }
}
