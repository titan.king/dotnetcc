﻿namespace Dashboard.Models.Clients
{
    public class DuplicatePartners
    {
        public int? PartnerId { get; set; }
        public string? TaxYear { get; set; }
        public string? TemplateName { get; set; }
        public int? ClientId { get; set; }
        public string? ClientName { get; set; }
    }

    public class DuplicatePartnersRequest
    {
        public int? BatchId { get; set; }
        public List<DuplicatePartners> DuplicatePartners { get; set; }
    }
}
