﻿namespace Dashboard.Models.Clients
{
    public class ClientFieldsObject
    {
        public object ClientFirstName { get; set; } = string.Empty;
        public object Spouse { get; set; } = string.Empty;
        public object ClientLastName { get; set; } = string.Empty;
        public object Signing { get; set; } = string.Empty;
        public object Officer { get; set; } = string.Empty;
        public object Lastname_Spouse { get; set; } = string.Empty;
        public object TrustEsatate_Name { get; set; } = string.Empty;
        public object TrusteeFiduciary_Name { get; set; } = string.Empty;
        public object TrusteeFiduciary_Last_Name { get; set; } = string.Empty;
        public object TrusteeFiduciary_Title { get; set; } = string.Empty;
        public object Year { get; set; } = string.Empty;
        public object TrustName { get; set; } = string.Empty;
        public object Trust { get; set; } = string.Empty;
        public object HusbandFullName { get; set; } = string.Empty;
        public object WifeFullName { get; set; } = string.Empty;
        public object EstateName { get; set; } = string.Empty;
        public object Trust_Name { get; set; } = string.Empty;
        public object Street_Address { get; set; } = string.Empty;
        public object City_Address { get; set; } = string.Empty;
        public object City { get; set; } = string.Empty;
        public object State { get; set; } = string.Empty;
        public object Business_Name { get; set; } = string.Empty;
        public object ClientId { get; set; } = string.Empty;
        public object NameofClients { get; set; } = string.Empty;
        public object AddressofClients { get; set; } = string.Empty;
        public object NameofClient { get; set; } = string.Empty;
        public object ClientName { get; set; } = string.Empty;
        public object Address1 { get; set; } = string.Empty;
        public object Address2 { get; set; } = string.Empty;
        public object OfficeLocation { get; set; } = string.Empty;
    }
}
