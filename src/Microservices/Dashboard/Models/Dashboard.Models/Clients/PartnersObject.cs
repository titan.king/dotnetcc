﻿namespace Dashboard.Models.Clients
{
    public class PartnersObject
    {
        public object PartnerID { get; set; } = string.Empty;
        public object PartnerName { get; set; } = string.Empty;
    }
}
