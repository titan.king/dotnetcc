﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.Clients
{
    public class BatchRequestViewModel
    {
        public int BatchRequestId { get; set; }
        public int BatchId { get; set; }
        public string BatchJson { get; set; } = string.Empty;
        public bool IsProcess { get; set; }
        public string Status { get; set; } = string.Empty;
        public int TemplateId { get; set; }
        public string TemplateName { get; set; } = string.Empty;
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string? CreatedBy { get; set; } = string.Empty;
    }

    public class BatchJsonViewModel
    {
        public int? ClientId { get; set; }
        public int? BulkLettersId { get; set; }
        public int BatchId { get; set; }
        public int DocumentStatusId { get; set; }
        public int EngageTypeId { get; set; }
        public int DepartmentId { get; set; }

    }
}
