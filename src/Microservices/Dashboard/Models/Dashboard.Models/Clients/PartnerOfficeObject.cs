﻿namespace Dashboard.Models.Clients
{
    public class PartnerOfficeObject
    {
        public Object PartnerId { get; set; } = string.Empty;
        public Object PartnerName { get; set; } = string.Empty;
        public Object OfficeID { get; set; } = string.Empty;
        public Object OfficeName { get; set; } = string.Empty;
    }
}
