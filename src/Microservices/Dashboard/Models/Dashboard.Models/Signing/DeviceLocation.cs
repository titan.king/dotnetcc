﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.Signing
{
    public class DeviceLocation
    {
        public string? latitude { get; set; }
        public string? longitude { get; set; }
    }
}
