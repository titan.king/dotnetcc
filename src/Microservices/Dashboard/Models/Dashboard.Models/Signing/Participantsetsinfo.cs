﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.Signing
{
    public class Participantsetsinfo
    {
        public string? id { get; set; }
        public Memberinfo[]? memberInfos { get; set; }
        public string? role { get; set; }
        public int? order { get; set; }
    }
}
