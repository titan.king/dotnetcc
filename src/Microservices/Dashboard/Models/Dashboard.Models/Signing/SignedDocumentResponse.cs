﻿namespace Dashboard.Models.Signing
{
    public class SignedDocumentResponse
    {
        public string? url { get; set; }
    }
}
