﻿namespace Dashboard.Models.Signing
{
    public class HTMLFileConversionViewModel
    {
        public string? Base64HtmlData { get; set; }
        public int EngagementLetterId { get; set; }
        public string? EngagementLetterName { get; set; }
        public string? Host { get; set; }
        public string? PathChange { get; set; }
    }
}
