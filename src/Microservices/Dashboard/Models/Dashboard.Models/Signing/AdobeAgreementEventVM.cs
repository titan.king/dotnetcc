﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.Signing
{
    public class AdobeAgreementEventVM
    {
        public IList<Events>? events { get; set; }
    }
}
