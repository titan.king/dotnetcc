﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.Signing
{
    public class Events
    {
        public string? actingUserEmail { get; set; }
        public string? actingUserIpAddress { get; set; }
        public string? actingUserName { get; set; }
        public string? date { get; set; }
        public string? description { get; set; }
        public string? device { get; set; }
        public DeviceLocation? deviceLocation { get; set; }
        public string? devicePhoneNumber { get; set; }
        public DigitalSignatureInfo? digitalSignatureInfo { get; set; }
        public ElectronicSealInfo? electronicSealInfo { get; set; }
        public string? initiatingUserEmail { get; set; }
        public string? initiatingUserName { get; set; }
        public string? participantEmail { get; set; }
        public string? participantId { get; set; }
        public string? participantRole { get; set; }
        public string? synchronizationId { get; set; }
        public string? type { get; set; }
        public string? vaultEventId { get; set; }
        public string? vaultProviderName { get; set; }
        public string? versionId { get; set; }
        public string? comment { get; set; }
        public string? id { get; set; }
        public IList<ReminderParticipants>? reminderParticipants { get; set; }
        public string? replacedParticipantEmail { get; set; }
        public string? replacedParticipantName { get; set; }
        public string? signerNewName { get; set; }
        public string? signerOldName { get; set; }
        public string? signingReason { get; set; }
    }
}
