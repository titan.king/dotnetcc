﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.Signing
{
    public class ReminderParticipants
    {
        public string? email { get; set; }
        public string? participantId { get; set; }
        public string? name { get; set; }
    }
}
