﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.Signing
{
    public class Memberinfo
    {
        public string? email { get; set; }
        public string? name { get; set; }
        public string? id { get; set; }
        public Securityoption? securityOption { get; set; }
    }
}
