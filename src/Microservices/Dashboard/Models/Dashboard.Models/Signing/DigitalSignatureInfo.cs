﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.Signing
{
    public class DigitalSignatureInfo
    {
        public string? company { get; set; }
        public string? email { get; set; }
        public string? name { get; set; }
        public string? certificateIssuer { get; set; }
        public string? cloudProviderIp { get; set; }
        public string? cloudProviderName { get; set; }
        public string? cloudProviderUrl { get; set; }
    }
}
