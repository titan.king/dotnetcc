﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.Signing
{
    public class AdobeCancleSignRequest
    {
        public string? state { get; set; }
        public string? agreementID { get; set; }
    }
}
