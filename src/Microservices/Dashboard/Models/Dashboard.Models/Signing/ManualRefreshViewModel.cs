﻿namespace Dashboard.Models.Signing
{
    public class ManualRefreshViewModel
    {
        public int EngagementLetterId { get; set; }
        public string IsRefreshButtonClicked { get; set; } = string.Empty;
    }
}
