﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.Signing
{
    public class Agreementsettingsinfo
    {
        public bool? canEditFiles { get; set; }
        public bool? canEditElectronicSeals { get; set; }
        public bool? canEditAgreementSettings { get; set; }
        public bool? showAgreementReminderSentEvents { get; set; }
        public bool? hipaaEnabled { get; set; }
        public bool? showDocumentsViewedEvents { get; set; }
    }
}
