﻿using System.Net;

namespace Dashboard.Models.Signing
{
    public class HtmlFileResponse
    {
        public string? HtmlUrl { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public string? Message { get; set; }
    }
}
