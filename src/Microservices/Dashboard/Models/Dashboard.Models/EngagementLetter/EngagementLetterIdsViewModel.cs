﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Models.EngagementLetter
{
    public class EngagementLetterIdsViewModel
    {
        public int EngagementLetterId { get; set; }
        [Required(ErrorMessage = "SigningPartnerName is required")]
        public string PartnerName { get; set; } = string.Empty;
        public string CreatedBy { get; set; } = string.Empty;
        public string Status { get; set; } = string.Empty;
        public string OfficeName { get; set; } = string.Empty;
        public string AdminName { get; set; } = string.Empty;
        public string TemplateName { get; set; } = string.Empty;
        public int? clientId { get; set; } 
        public int DepartmentId { get; set; } 
        public string? TaxYear { get; set; }
        public int? EngageTypeId { get; set; }

    }
}