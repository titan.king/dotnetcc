﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.EngagementLetter
{
    public class StatusViewModel
    {
        public int StatusId { get; set; }
        public string? StatusName { get; set; } = string.Empty;
    }
}
