﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.EngagementLetter
{
    public class EngagementLetterListViewModel
    {
        public int EngagementLetterId { get; set; }
        public int BatchId { get;set; }
        public string? EngagementLetterName { get; set; }
        public string? ClientName { get; set; }
        public int? ClientId { get; set; }
        public int? TaxYear { get; set; }
        public int? DepartmentId { get; set; }
        public int? EngageTypeId { get; set; }
        public string? DepartmentName { get; set; }
        public string? EngageGroupName { get; set; }
        public string? EngageTypeName { get; set; }
        public string? EngageBasisName { get; set; }
        public string? OfficeName { get; set; }
        public string? TemplateName { get; set; }
        public string? PartnerName { get; set; }
        public int? DocumentStatusId { get; set; }
        public string? DocumentDescription { get; set; }
        public string? CreatedBy { get; set; }
        public string? AdminName { get; set; }
        public DateTime? LastModifieds { get; set; }
        public string? LastModified { get; set; }
    }
}
