﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Models.EngagementLetter
{
    public class ClientNamesViewModel
    {
        public int? ClientId { get; set; }
        [Required(ErrorMessage = "Admin Name is required")]
        public string ClientName { get; set; } = string.Empty;
    }
}
