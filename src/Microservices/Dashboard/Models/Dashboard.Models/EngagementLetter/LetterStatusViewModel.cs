﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Models.EngagementLetter
{
    public class LetterStatusViewModel
    {
        public int DocumentStatusId { get; set; }
        [Required(ErrorMessage = "Status is required")]
        public string Status { get; set; } = string.Empty;
    }
}