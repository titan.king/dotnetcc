﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Models.EngagementLetter
{
    public class TaxYearsViewModel
    {
        public int? YearId { get; set; }
        [Required(ErrorMessage = "TaxYear is required")]
        public string? TaxYear { get; set; }
    }
}