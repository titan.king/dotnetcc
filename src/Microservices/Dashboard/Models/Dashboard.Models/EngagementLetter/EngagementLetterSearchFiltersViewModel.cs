﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Models.EngagementLetter
{
    public class EngagementLetterSearchFilterViewModel
    {
        [Required(ErrorMessage = "EngagementLetterIds is required")]
        public List<EngagementLetterIdsViewModel>? EngagementLetterIds { get; set; }
        public List<EngagementLetterNamesViewModel>? EngagementLetterNames { get; set; }
        public List<ClientNamesViewModel>? ClientNames { get; set; }
        public List<TaxYearsViewModel>? Years { get; set; }
        public List<DepartmentsViewModel>? Departments { get; set; }
        public List<EngageTypesViewModel>? Types { get; set; }
        public List<SigningPartnersViewModel>? PartnerNames { get; set; }
        public List<AdminDetailsViewModel>? AdminNames { get; set; }
        public List<OfficeDetailsViewModel>? OfficeNames { get; set; }
        public List<LetterStatusViewModel>? LetterStatus { get; set; }
        public List<TemplateNamesViewModel>? TemplateNames { get; set; }

    }
}