﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Models.EngagementLetter
{
    public class OfficeDetailsViewModel
    {
        public int OfficeId { get; set; }
        [Required(ErrorMessage = "Status is required")]
        public string OfficeName { get; set; } = string.Empty;
    }
}