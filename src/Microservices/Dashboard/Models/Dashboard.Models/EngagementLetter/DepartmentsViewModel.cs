﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Models.EngagementLetter
{
    public class DepartmentsViewModel
    {
        public int DepartmentId { get; set; }

        public string Department { get; set; } = string.Empty;
    }
}
