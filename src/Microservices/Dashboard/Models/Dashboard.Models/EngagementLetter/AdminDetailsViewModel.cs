﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Models.EngagementLetter
{
    public class AdminDetailsViewModel
    {
        public int? AdminId { get; set; }
        [Required(ErrorMessage = "Admin Name is required")]
        public string AdminName { get; set; } = string.Empty;
    }
}