﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Models.EngagementLetter
{
    public class SigningPartnersViewModel
    {
        public int? PartnerId { get; set; }
        [Required(ErrorMessage = "PartnerName is required")]
        public string PartnerName { get; set; } = string.Empty;
    }
}