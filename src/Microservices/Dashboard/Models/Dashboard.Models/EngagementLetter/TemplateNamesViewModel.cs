﻿namespace Dashboard.Models.EngagementLetter
{
    public class TemplateNamesViewModel
    {
        public int? TemplateId { get; set; }
        public string TemplateName { get; set; } = string.Empty;
        public int? TemplateVersion { get; set; }
    }
}
