﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Models.EngagementLetter
{
    public class EngagementLetterNamesViewModel
    {
        public int EngagementLetterId { get; set; }
        [Required(ErrorMessage = "EngagementLetter Name is required")]
        public string EngagementLetterName { get; set; } = string.Empty;
        public string PartnerName { get; set; } = string.Empty;
        public string CreatedBy { get; set; } = string.Empty;
    }
}