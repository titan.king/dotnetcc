﻿namespace Dashboard.Models.EngagementLetter
{
    public class DeleteEngagementLetterViewModel
    {
        public int EngagementLetterId { get; set; }
        public string DeletedBy { get; set; } = string.Empty;
        public bool IsDeleted { get; set; }
        public DateTime DeletedOn { get; set; } = DateTime.Now;
    }
}
