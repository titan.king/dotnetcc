﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Models.EngagementLetter
{
    public class EngageTypesViewModel
    {
        public int EngageTypeId { get; set; }
        public string EngageTypeName { get; set; } = string.Empty;
    }
}