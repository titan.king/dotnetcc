﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Models.Blocks
{
    public class BlocksListViewModel
    {
        public int BlockId { get; set; }
        [Required(ErrorMessage = "Block Name is required")]
        public string BlockName { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string Content { get; set; } = string.Empty;
        public string ChangeNotes { get; set; } = string.Empty;
        public bool IsDeleted { get; set; }
        public int StatusId { get; set; }
        public string StatusName { get; set; } = string.Empty;
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool? QCRequired { get; set; }
        public bool? EditFreeBlock { get; set; }
        public string CreatedBy { get; set; } = string.Empty;
        public string DeletedBy { get; set; } = string.Empty;
        public DateTime? DeletedOn { get; set; }
        public string ModifiedBy { get; set; } = string.Empty;
        public string ConnectedTemplates { get; set; } = string.Empty;
    }
}
