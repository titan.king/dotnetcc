﻿namespace Dashboard.Models.Blocks
{
    public class BlockIdListViewModel
    {
        public int BlockId { get; set; }
    }
}
