﻿using System.Runtime.Serialization;

namespace Dashboard.Models.Blocks
{
    public class TemplateJSON
    {
        public class Rootobject
        {
            public Page[] pages { get; set; }
        }

        public class Page
        {
            public string name { get; set; }
            public Element[] elements { get; set; }
        }
        [DataContract]
        public class Element
        {
            [DataMember]
            public string type { get; set; }
            [DataMember]
            public string name { get; set; }
            [DataMember]
            public string visibleIf { get; set; }
            [DataMember]
            public bool startWithNewLine { get; set; }
            [DataMember]
            public string title { get; set; }
            [DataMember]
            public string titleLocation { get; set; }
            [DataMember]
            public bool readOnly { get; set; }
            [DataMember]
            public string html { get; set; }
            [DataMember]
            public bool hideNumber { get; set; }
            [DataMember]
            public string[] choices { get; set; }

            [DataMember]
            public Nullable<bool> visible { get; set; }
        }
    }
}
