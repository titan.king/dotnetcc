﻿using Dashboard.Models.ResponseModel;

namespace Dashboard.Models.Blocks
{
    public class BlocksStatusViewModel
    {
        public List<BlockStatus>? BlockStatus { get; set; }
    }
}
