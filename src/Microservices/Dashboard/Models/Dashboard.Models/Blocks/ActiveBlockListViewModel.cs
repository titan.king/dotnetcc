﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.Blocks
{
    public class ActiveBlockListViewModel
    {
        public int BlockId { get; set; }
        public string BlockName { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string Status { get; set; } = string.Empty;
        public string ChangeNotes { get; set; } = string.Empty;
        public string Content { get; set; } = string.Empty;
    }
}
