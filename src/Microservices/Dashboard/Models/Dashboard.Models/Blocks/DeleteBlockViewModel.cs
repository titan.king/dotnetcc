﻿namespace Dashboard.Models.Blocks
{
    public class DeleteBlockViewModel
    {
        public int[] BlockId { get; set; }
        public string DeletedBy { get; set; } = string.Empty;
        public bool IsDeleted { get; set; }
        public DateTime ModifiedOn { get; set; }    
    }
}
