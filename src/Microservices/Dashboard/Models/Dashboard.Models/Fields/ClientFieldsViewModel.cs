﻿namespace Dashboard.Models.Fields
{
    public class ClientFieldsViewModel
    {
        public int ClientFieldsId { get; set; }
        public string? ClientFirstName { get; set; } = string.Empty;
        public string? Spouse { get; set; } = string.Empty;
        public string? ClientLastName { get; set; } = string.Empty;
        public string? Signing { get; set; } = string.Empty;
        public string? Officer { get; set; } = string.Empty;
        public string? Lastname_Spouse { get; set; } = string.Empty;
        public string? TrustEsatate_Name { get; set; } = string.Empty;
        public string? TrusteeFiduciary_Name { get; set; } = string.Empty;
        public string? TrusteeFiduciary_Last_Name { get; set; } = string.Empty;
        public string? TrusteeFiduciary_Title { get; set; } = string.Empty;
        public string? Year { get; set; }
        public string? TrustName { get; set; } = string.Empty;
        public string? Trust { get; set; } = string.Empty;
        public string? HusbandFullName { get; set; } = string.Empty;
        public string? WifeFullName { get; set; } = string.Empty;
        public string? EstateName { get; set; } = string.Empty;
        public string? Trust_Name { get; set; } = string.Empty;
        public string? Street_Address { get; set; } = string.Empty;
        public string? City_Address { get; set; } = string.Empty;
        public string? City { get; set; } = string.Empty;
        public string? State { get; set; } = string.Empty;
        public string? Business_Name { get; set; } = string.Empty;
        public int? ClientId { get; set; }
        public string? NameofClients { get; set; } = string.Empty;
        public string? AddressofClients { get; set; } = string.Empty;
        public string? NameofClient { get; set; } = string.Empty;
        public string? ClientName { get; set; } = string.Empty;
        public string? Address1 { get; set; } = string.Empty;
        public string? Address2 { get; set; } = string.Empty;
        public int? OfficeLocation { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool? IsDeleted { get; set; } = false;
        public DateTime? DeletedOn { get; set; }
    }
}
