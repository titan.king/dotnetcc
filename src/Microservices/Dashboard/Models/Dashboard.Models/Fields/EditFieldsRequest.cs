﻿namespace Dashboard.Models.Fields
{
    public class EditFieldsRequest
    {
        public List<EditFields>? EditFields { get; set; }
        public bool ESigning { get; set; }
        public bool Is7216Available { get; set; } = false;
        public int? ClientSignatureCount { get; set; }
    }
    public class EditFields
    {
        public string FieldName { get; set; } = string.Empty;
        public string FieldValue { get; set; } = string.Empty;
    }
}
