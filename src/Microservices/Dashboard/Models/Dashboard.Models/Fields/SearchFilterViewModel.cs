﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Models
{
    public class SearchFilterViewModel
    {
        public int EngagementLetterId { get; set; }

        public string EngagementLetterName { get; set; } = string.Empty;

        public string OfficeName { get; set; } = string.Empty;

        [Required(ErrorMessage = "TaxYear is required.")]
        public int? TaxYear { get; set; }

        public string TemplateName { get; set; } = string.Empty;

        public string CilentName { get; set; } = string.Empty;

        public string PartnerName { get; set; } = string.Empty;

        public string Department { get; set; } = string.Empty;

        public string AdminName { get; set; } = string.Empty;

        public string LetterStatus { get; set; } = string.Empty;    
    }          
}
