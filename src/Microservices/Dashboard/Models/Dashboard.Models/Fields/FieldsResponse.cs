﻿using Dashboard.Models.Clients;
using Dashboard.Models.ResponseModel;

namespace Dashboard.Models.Fields
{
    public class FieldsResponse
    {
        public List<FieldListModel> FieldList { get; set; }
        public ClientFieldsViewModel ClientFields { get; set; }
        public PartnerOfficeResModel PartnerOffice { get; set; }
    }
}
