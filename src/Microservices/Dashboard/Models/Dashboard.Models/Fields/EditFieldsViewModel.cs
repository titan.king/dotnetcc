﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Models
{
    public class EditFieldsViewModel
    {
        [Required(ErrorMessage = "Client Id is required.")]
        public int? ClientId { get; set; }

        public string ClientName { get; set; } = string.Empty;

        public string PartnerName { get; set; } = string.Empty;

        public string ContactPerson1 { get; set; } = string.Empty;

        public string ContactPerson1EmailId { get; set; } = string.Empty;

        public string ContactPerson1Title { get; set; } = string.Empty;

        public string ContactPerson2 { get; } = string.Empty;

        public string ContactPerson2EmailId { get; set; } = string.Empty;

        public string ContactPerson2Title { get; set; } = string.Empty;

        public string Address1 { get; set; } = string.Empty;

        public string Address2 { get; set; } = string.Empty;

        public string City { get; set; } = string.Empty;

        public string State { get; set; } = string.Empty;

        public string ZipCode { get; set; } = string.Empty;

        public DateTime CreatedOnDate { get; set;}

        public int TaxYear { get; set;}

        public string OtherEntities { get; set; } = string.Empty;
    }
}
