﻿namespace Dashboard.Models.Fields
{
    public class ColumnNamesViewModel
    {
        public int ColumnId { get; set; }
        public string? ColumnNames { get; set; } = string.Empty;
    }
}
