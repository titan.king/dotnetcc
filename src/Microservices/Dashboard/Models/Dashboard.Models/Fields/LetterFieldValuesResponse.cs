﻿using Dashboard.Models.BulkLetterGeneration;
using Dashboard.Models.Clients;
using System.ComponentModel.DataAnnotations;

namespace Dashboard.Models.Fields
{
    public class LetterFieldValuesResponse
    {
        public int LetterFieldValuesId { get; set; }
        public int? ClientId { get; set; }
        public int? PartnerId { get; set; }
        public int? BulkLettersId { get; set; }
        public int? FieldId { get; set; }
        public string FieldValue { get; set; } = string.Empty;
        public string FieldName { get; set; } = string.Empty;
        public int LetterVersion { get; set; }
        public string? DisplayName { get; set; } = string.Empty;
    }

    public class EditFieldValuesResponse
    {
        public List<EditFieldsValues> EditFieldsValues { get; set; }
        public bool Is7216Available { get; set; } = false;
        public int ClientSignatureCount { get; set; }
        public int BulkLettersId { get; set; }
        public bool? IsEsigning { get; set; }
        public string? EditedBy { get; set; }
    }
    public class EditFieldsValues
    {
        public int LetterFieldValuesId { get; set; }
        public int? ClientId { get; set; }
        public int? PartnerId { get; set; }
        public int? BulkLettersId { get; set; }
        public int? FieldId { get; set; }
        public string FieldValue { get; set; } = string.Empty;
        public string FieldName { get; set; } = string.Empty;
        public int LetterVersion { get; set; }
        public string? DisplayName { get; set; } = string.Empty;
    }

    public class FieldErrorResponse
    {
        public int? ClientId { get; set; }
    }

    public class LetterFieldValuesBase
    {
        public List<DuplicatePartners> DuplicatePartners { get; set; }
        public List<FieldErrorResponse> FieldErrorResponses { get; set; }
        public List<BulkLettersResponse> BulkLettersResponse { get; set; }
        public string? ReturnTypeCodeError { get; set; }
    }
    public class BaseData
    {
        public List<LetterFieldValuesResponse> LetterFieldValues { get; set; }
        public List<FieldErrorResponse> FieldErrorResponses { get; set; }
    }

    public class LetterFieldValuesList
    {
        public string ClientIdErrorList { get; set; }
        public string? GetCurrentDate { get; set; }
        public List<BulkLettersResponse> BulkLettersResponse { get; set; }
        public List<DuplicatePartners> DuplicatePartners { get; set; }
        public List<PartnersNames> PartnerNames { get; set; }
        public PartnerErrors PartnerErrors { get; set; }
        public string? ReturnTypeCodeError { get; set; }
    }
    public class PartnerErrors
    {
        public string? TaxYear { get; set; }
        public string? TemplateName { get; set; }
        public string ExistingClients { get; set; }
    }
}
