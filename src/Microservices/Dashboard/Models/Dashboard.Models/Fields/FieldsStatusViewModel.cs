﻿using Dashboard.Models.ResponseModel;

namespace Dashboard.Models.Fields
{
    public class FieldsStatusViewModel
    {
        public List<FieldsStatus>? FieldsStatus { get; set; }
        public List<FieldsDataType>? FieldsDataType { get; set; }

    }
}
