﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Models
{
    public class FieldsValuesViewModel
    {
        public int BatchId { get; set; }

        public int LetterId { get; set; }

        public string LetterName { get; set; } = string.Empty;

        public string Office { get; set; } = string.Empty;

        [Required(ErrorMessage = "TaxYear is required.")]
        public int? TaxYear { get; set; }

        public string TemplateName { get; set; } = string.Empty;

        public string ClientName { get; set; } = string.Empty;

        public string PartnerName { get; set; } = string.Empty;

        public string AdminName { get; set; } = string.Empty;   

        public string LetterStatus { get; set; } = string.Empty;
    }
}