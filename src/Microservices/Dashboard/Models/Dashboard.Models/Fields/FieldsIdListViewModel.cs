﻿namespace Dashboard.Models.Fields
{
    public class FieldsIdListViewModel
    {
        public int FieldId { get; set; }
    }
}
