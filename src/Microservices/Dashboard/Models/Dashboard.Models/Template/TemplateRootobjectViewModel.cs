﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Models.Template
{
    public class TemplateRootobjectViewModel
    {
        [Required(ErrorMessage = "Pages required.")]
        public TemplatePageViewModel[]? Pages { get; set; }
    }    
}
