﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dashboard.Models.Template
{
    public class TemplateApprovalViewModel
    {
        public int Id { get; set; }
        public int StatusId { get; set; }

        [Required(ErrorMessage = "Comments required")]
        public string ApprovalComments { get; set; } = string.Empty;
        public Nullable<int> ModifiedById { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string TransactionStatus { get; set; } = string.Empty;
    }
}
