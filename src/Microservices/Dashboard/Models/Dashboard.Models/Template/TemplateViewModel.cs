﻿using System.ComponentModel.DataAnnotations;

namespace Dashboard.Models.Template
{
    public class TemplateViewModel
    {
        public int TemplateId { get; set; }
        [MaxLength(255)]
        [Required(ErrorMessage = "TemplateName is required")]
        public string? TemplateName { get; set; } = string.Empty;
        public string? TemplateDescription { get; set; } = string.Empty;
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; } = string.Empty;
        public int EngageTypeId { get; set; }
        public int StatusId { get; set; }
        [MaxLength(500)]
        public string? ChangeNotes { get; set; } = string.Empty;
        public bool IsDeleted { get; set; } = false;
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string CreatedBy { get; set; } = string.Empty;
        public string ModifiedBy { get; set; } = string.Empty;
        public string DeletedBy { get; set; } = string.Empty;
        public int TemplateVersionId { get; set; }
        public string? TemplateLogic { get; set; } = string.Empty;
        public int? VersionNumber { get; set; }
        public int MasterTemplateId { get; set; }
        public int? AttachmentCount { get; set; }
        public string? AttachmentJson { get; set; } = string.Empty;
        public bool Is7216Available { get; set; } = false;
        public int? ClientSignatureCount { get; set; }
        public int[] DeleteId { get; set; }
        public string? TemplateHtml { get; set; } = string.Empty;
        public string? AttachmentURL { get; set; } = string.Empty;
    }

}
