﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Dashboard.Models.Template.TemplateJSONElementViewModel;

namespace Dashboard.Models.Template
{
    public class TemplatePageViewModel
    {
        [Required(ErrorMessage = "Page Name is required.")]
        public string PageName { get; set; } = string.Empty;
        public TemplateJSONElementViewModel[]? Elements { get; set; }
    }
}
