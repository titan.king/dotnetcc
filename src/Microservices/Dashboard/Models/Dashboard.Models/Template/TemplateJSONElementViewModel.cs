﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Dashboard.Models.Template
{
    [DataContract]
    public class TemplateJSONElementViewModel
    {
        [DataMember]
        public string PageType { get; set; } = string.Empty;

        [DataMember]
        [Required(ErrorMessage = "Page Name is required.")]
        public string PageName { get; set; } = string.Empty;

        [DataMember]
        public string PageVisibleIf { get; set; } = string.Empty;

        [DataMember]
        public bool PageStartWithNewLine { get; set; }

        [DataMember]
        public string PageTitle { get; set; } = string.Empty;

        [DataMember]
        public string PageTitleLocation { get; set; } = string.Empty;

        [DataMember]
        public bool PageReadOnly { get; set; }

        [DataMember]
        public string PageHtml { get; set; } = string.Empty;

        [DataMember]
        public bool HideNumber { get; set; }

        [DataMember]
        public string[]? Choices { get; set; }

        [DataMember]
        public bool PageVisible { get; set; }
    }
}
