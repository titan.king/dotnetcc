﻿using Engage3.Common.Business.Interfaces;
using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Net;
using System.Resources;
using System.Text;
using LicenseContext = OfficeOpenXml.LicenseContext;

namespace Engage3.Common.Business
{
    public class ImportExcel : IExcelImport
    {
        private readonly ResourceManager _resourceManager;
        public ImportExcel(ResourceManager resourceManager)
        {
            _resourceManager = resourceManager;
        }
        public T ReadFromExcel<T>(string blobUrl, bool hasHeader = true)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            using (var excelPack = new ExcelPackage())
            {
                //Load excel stream
                //using (var stream = File.OpenRead(path))
                //{
                //    excelPack.Load(stream);   
                //}

                using (var webClient = new WebClient())
                {
                    byte[] excelBytes = webClient.DownloadData(blobUrl);
                    using (var excelStream = new MemoryStream(excelBytes))
                    {
                        excelPack.Load(excelStream);
                    }
                }

                var ws = excelPack.Workbook.Worksheets[0];

                DataTable excelasTable = new DataTable();
                foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                {
                    //Get colummn details
                    if (!string.IsNullOrEmpty(firstRowCell.Text))
                    {
                        string firstColumn = string.Format("Column {0}", firstRowCell.Start.Column);
                        excelasTable.Columns.Add(hasHeader ? firstRowCell.Text : firstColumn);
                    }
                }
                var startRow = hasHeader ? 2 : 1;
                //Get row details
                for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                {
                    var wsRow = ws.Cells[rowNum, 1, rowNum, excelasTable.Columns.Count];
                    DataRow row = excelasTable.Rows.Add();
                    foreach (var cell in wsRow)
                    {
                        row[cell.Start.Column - 1] = cell.Text;
                    }
                }
               
                var generatedType = JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(excelasTable));
                return (T)Convert.ChangeType(generatedType, typeof(T));
            }
        }
    }
}
