﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Engage3.Common.Business.Interfaces
{
    public interface IGetCurrentDatetime
    {
        Task<DateTime> GetCurrentEstDatetime();
    }
}
