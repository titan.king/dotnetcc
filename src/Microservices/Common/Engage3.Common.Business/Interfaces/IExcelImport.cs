﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Engage3.Common.Business.Interfaces
{
    public interface IExcelImport
    {
        T ReadFromExcel<T>(string path, bool hasHeader = true);
    }
}
