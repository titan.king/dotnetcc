﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Engage3.Common.Business.Models
{
    public class CommonMailModel
    {
        public int DocumentStatusId { get; set; }
        public string FromMail { get; set; }
        public string FromName { get; set; }
        public string Subject { get; set; } = null;
        public string ToIds { get; set; }
        public string LetterName { get; set; }
        public string StatusContent { get; set; }
        public string LinkContent { get; set; }
        public int EngagementLetterId { get; set; }
        public string pdfContent { get; set; }
        public string SubjectAppend { get; set; }
        public string MJAPIKey { get; set; }
        public string MJAPISecret { get; set; }
        public string Changesubject { get; set; }
        public EmailTempContent EmailTemplateContent { get; set; }
    } 
}
