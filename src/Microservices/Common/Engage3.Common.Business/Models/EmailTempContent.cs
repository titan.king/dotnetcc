﻿using System;

namespace Engage3.Common.Business.Models
{
    public class EmailTempContent
    {

        public int EmailTemplateId { get; set; }
        public int DocumentStatusId { get; set; }
        public string EmailSubject { get; set; }
        public string EmailContent { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
