﻿using Engage3.Common.Business.Models;
using Mailjet.Client;
using Mailjet.Client.Resources;
using Newtonsoft.Json.Linq;
using System;
using System.Resources;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Engage3.Common.Business
{
    public class EmailHelper
    {
        private readonly ResourceManager _resourceManager;
        public EmailHelper(ResourceManager resourceManager)
        {
            _resourceManager = resourceManager;
        }
        public async Task<int> SendMailwithAttachment(CommonMailModel mailObj)
        {
            int sentStatus = 0;
            try
            {
                string toEmail = mailObj.ToIds ?? string.Empty;
                string statusName = string.Empty;
                statusName = mailObj.StatusContent ?? string.Empty;

                if (!string.IsNullOrEmpty(toEmail))
                {
                    if (!string.IsNullOrEmpty(mailObj.ToIds) && !string.IsNullOrEmpty(mailObj.StatusContent))
                    {
                        string apiKey = mailObj.MJAPIKey ?? string.Empty;
                        string apiSecret = mailObj.MJAPISecret ?? string.Empty;
                        MailjetClient client = new MailjetClient(apiKey, apiSecret);
                        string letter_Id = string.Empty;
                        string subject_Append = mailObj.SubjectAppend ?? string.Empty;
                        if (mailObj.EngagementLetterId == 0)
                        {
                            letter_Id = string.Empty;
                        }
                        else
                        {
                            letter_Id = mailObj.EngagementLetterId.ToString();
                        }

                        var mailresult = mailObj.EmailTemplateContent;
                        if (mailresult != null && mailObj.LetterName != null)
                        {
                            Regex yourRegex = new Regex(@"\##([^\}]+)\##");
                            string mailContent = mailresult.EmailContent ?? string.Empty;

                            mailContent = mailContent.Replace("##EngagementLetterName##", mailObj.LetterName).Replace("##StatusContent##", statusName).Replace("##LinkContent##", mailObj.LinkContent);
                            string mailSubject = string.Empty;
                            string switch_MailSubject = mailObj.Changesubject;
                            if (switch_MailSubject == "true")
                            {
                                if (subject_Append != null && subject_Append != string.Empty)
                                {
                                    mailSubject = "(" + subject_Append + ")" + " (" + letter_Id + ")" + " - " + mailresult.EmailSubject;
                                }
                                else
                                {
                                    mailSubject = " (" + letter_Id + ") " + mailresult.EmailSubject;
                                }
                            }
                            else
                            {
                                mailSubject = mailresult.EmailSubject ?? string.Empty;
                            }
                            mailSubject = yourRegex.Replace(mailSubject, mailObj.LetterName);
                            if (!string.IsNullOrEmpty(mailContent)
                                && !string.IsNullOrEmpty(mailSubject)
                                && !string.IsNullOrEmpty(mailObj.ToIds)
                                && !string.IsNullOrEmpty(mailObj.FromMail))
                            {
                                mailObj.FromMail = (mailObj.FromMail).Trim();
                                mailObj.ToIds = (mailObj.ToIds).Trim();
                                MailjetRequest request = new MailjetRequest
                                {
                                    Resource = Send.Resource,
                                }
                                   .Property(Send.FromEmail, mailObj.FromMail)
                               .Property(Send.Subject, mailSubject)
                               .Property(Send.TextPart, mailContent)
                                .Property(Send.HtmlPart, mailContent)
                               .Property(Send.Attachments, new JArray {
                                   new JObject {
                                       {"ContentType", "application/pdf"},
                                       {"Filename", mailObj.LetterName + ".pdf"},
                                       {"Content", mailObj.pdfContent}
                                   }
                               })
                               .Property(Send.To, mailObj.ToIds);
                                MailjetResponse response = await client.PostAsync(request).ConfigureAwait(false);
                                if (!response.IsSuccessStatusCode)
                                {
                                    sentStatus = 3;
                                }
                                else
                                {
                                    sentStatus = 1;
                                }
                            }
                            else
                            {
                                sentStatus = 2;
                            }
                        }
                        else
                        {
                            sentStatus = 2;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally { }
            return sentStatus;
        }
    }
}
