﻿using Engage3.Common.Business.Interfaces;
using System;
using System.Globalization;
using System.Threading.Tasks;

namespace Engage3.Common.Business
{
    public class GetCurrentDateTime : IGetCurrentDatetime
    {
        public async Task<DateTime> GetCurrentEstDatetime()
        {
            DateTime utcNow = DateTime.UtcNow;
            TimeZoneInfo easternTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            DateTime estTime = TimeZoneInfo.ConvertTimeFromUtc(utcNow, easternTimeZone);
            CultureInfo cultureInfo = new CultureInfo("en-US"); //Using US English culture
            string formattedTime = estTime.ToString(cultureInfo);
            var responseDate = DateTime.ParseExact(formattedTime, "MM/dd/yyyy h:mm:ss tt", CultureInfo.CreateSpecificCulture("en-US"));
            return responseDate;
        }
    }
}
