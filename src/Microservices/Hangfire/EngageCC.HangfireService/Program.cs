using Engage3.Common.Business.Interfaces;
using Engage3.Common.Business;
using EngageCC.DataContext;
using EngageCC.HangfireService.Helper;
using Hangfire;
using Hangfire.Data.Context;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

var configuration = new ConfigurationBuilder()
           .SetBasePath(Directory.GetCurrentDirectory())
           .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
           .Build();

var connectionString = configuration?.GetConnectionString("EngageHangfireDbConnection");
var engageDbString = configuration?.GetConnectionString("EngageDbConnection");

builder.Services.AddCors(
            options => options.AddPolicy("AllowCors",
            builder =>
            {
                builder
                .AllowAnyOrigin()
                .WithMethods("GET", "PUT", "POST", "DELETE")
                .AllowAnyHeader();
            }));

builder.Services.AddControllers();

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddScoped<IGetCurrentDatetime, GetCurrentDateTime>();

builder.Services.AddHangfire(x =>
{
    x.UseSqlServerStorage(connectionString);
    var server = new BackgroundJobServer(new BackgroundJobServerOptions
    {
        ServerName = "ECC"
    });
});

builder.Logging.AddAzureWebAppDiagnostics().AddConsole().ClearProviders();

builder.Services.AddDbContext<HFDBContext>(options =>
    options.UseSqlServer(connectionString), ServiceLifetime.Scoped);

builder.Services.AddDbContext<DataContext>(options =>
    options.UseSqlServer(engageDbString), ServiceLifetime.Scoped);
DependencyInjectionResolver.RegisterServices(builder.Services);
DependencyInjectionResolver.RegisterRepository(builder.Services);


builder.Services.AddAutoMapper(typeof(AutoMapperProfile));
var app = builder.Build();

if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

using (var scope = app.Services.CreateScope())
{
    var dbContext = scope.ServiceProvider
        .GetRequiredService<DataContext>();

    // Here is the migration executed
    dbContext.Database.Migrate();
}

app.UseCors("AllowCors");
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();
app.UseHangfireDashboard("/hangfire", new
DashboardOptions
{
    Authorization = new[] { new HFAuthorization() }
});
//RecurringJob.AddOrUpdate<IHangfireCCServices>(a => a.ProceedForESigningAndManualSigning(), "*/1 * * * *");
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

//app.MapRazorPages();

app.Run();
