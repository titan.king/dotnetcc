﻿using Hangfire;
using Hangfire.Business.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace EngageCC.HangfireService.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class BulkLetterController : ControllerBase
    {
        private readonly IRecurringJobManager _recurringJobManager;
        private readonly IHangfireCCServices _hangfireCCServices;
        private readonly IConfiguration _configuration;
        public BulkLetterController(IRecurringJobManager recurringJobManager, IHangfireCCServices hangfireCCServices, IConfiguration configuration)
        {
            _recurringJobManager = recurringJobManager;
            _hangfireCCServices = hangfireCCServices;
            _configuration = configuration;
        }

        [HttpGet]
        [Route("EnableBulkLetterBatchProcess")]
        public async Task<IActionResult> EnableBulkLetterBatchProcess()
        {
            var jobMinute = _configuration["JobMinute"];
            _recurringJobManager.AddOrUpdate("BulkLetterJob",
                () => _hangfireCCServices.SaveEngamentLetters(), jobMinute);

            //var data = await _hangfireCCServices.SaveEngamentLetters();
            return Ok("BulkLetter BatchProcess job started");
        }

        [HttpGet]
        [Route("EnableESigningBatchProcess")]
        public async Task<IActionResult> EnableESigningBatchProcess()
        {
            var jobMinute = _configuration["JobMinute"];
            _recurringJobManager.AddOrUpdate("ESigningJob",
                () => _hangfireCCServices.ProceedForESigningAndManualSigning(), jobMinute);

            //var data = await _hangfireCCServices.ProceedForESigningAndManualSigning();
            return Ok("ESigning BatchProcess job started");
        }
    }
}
