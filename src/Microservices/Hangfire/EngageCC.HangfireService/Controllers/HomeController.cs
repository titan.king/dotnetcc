﻿using Microsoft.AspNetCore.Mvc;

namespace EngageCC.HangfireService.Controllers
{
    public class HomeController : Controller
    {
            
        public IActionResult Index()
        {
            return View();
        }
    }
}
