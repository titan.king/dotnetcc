﻿using AutoMapper;
using EngageCC.DataContext.DataModel;
using Hangfire.Models.Signing;

namespace EngageCC.HangfireService.Helper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<EngagementLetterResponse, EngagementLetter>().ReverseMap();
            CreateMap<SigningInfo, SigningInfoModel>().ReverseMap();
            CreateMap<SigningInfoModel, EngagementLetterResponse>().ReverseMap();
            CreateMap<HistoryLog, HistoryLogModel>().ReverseMap();
            CreateMap<DocumentStatusModel, HistoryLogModel>()
   .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.DocumentStatusName)).ReverseMap();
            CreateMap<BulkLetterErrorLogVM, BulkLogger>().ReverseMap();
        }
    }
}
