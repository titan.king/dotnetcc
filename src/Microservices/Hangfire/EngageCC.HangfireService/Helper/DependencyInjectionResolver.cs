﻿using Hangfire.Business.Interfaces;
using Hangfire.Business.Services;
using Hangfire.Data.Interfaces;
using Hangfire.Data.Repositories;

namespace EngageCC.HangfireService.Helper
{
    public class DependencyInjectionResolver
    {
        public static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IHangfireCCServices, HangfireCCServices>();
            services.AddScoped<IAdobeService, AdobeService>();
        }
        public static void RegisterRepository(IServiceCollection services)
        {
            services.AddScoped<IHangfireCCRepository, HangfireCCRepository>();
        }
    }
}
