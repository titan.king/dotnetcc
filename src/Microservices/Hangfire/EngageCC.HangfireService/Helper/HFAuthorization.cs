﻿using Hangfire.Dashboard;

namespace EngageCC.HangfireService.Helper
{
    public class HFAuthorization : IDashboardAuthorizationFilter
    {
        public HFAuthorization()
        {

        }
        public bool Authorize(DashboardContext context)
        {
            var httpContext = context.GetHttpContext();

            // Allow all authenticated users to see the Dashboard (potentially dangerous).
            return true;//httpContext.User.Identity.IsAuthenticated;
        }
    }
}