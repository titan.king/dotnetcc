﻿using EngageCC.DataContext.DataModel;
using Hangfire.Models.Signing;

namespace Hangfire.Data.Interfaces
{
    public interface IHangfireCCRepository
    {
        Task<bool> InsertEngagementLetterTable(List<EngagementLetterResponse> engagementLetters);
        Task<BatchRequest> GetBatchRequest(string status, bool isProcess);
        Task<List<BulkLetters>> GetBulkLettersList(List<int?> bulkletterIds);
        Task<bool> UpdateEngagementLetterStatus(int batchId);
        Task UpdateBatchRequestIsProcess(int batchId);
        Task<List<EngagementLetter>> GetEngagementLettersList();
        Task<string> GetMasterTemplate(int templateId);
        Task<string> GetTemplateAttachments(int templateId, int batchId);
        Task<List<LetterFieldValues>> GetLetterFieldValues(int bulkLetterId);
        Task<bool> SaveSigningInfos(List<SigningInfo> signingInfos);
        Task<bool> UpdateEngagementLetter(List<EngagementLetter> engagementLetters);
        Task<string> GetSignatoryLastName(int bulklettersId, int? clientId);
        Task<string> GetChildEntityName(int bulklettersId, int? clientId);
        Task<List<HistoryLogModel>> GetHistoryLogVersion(List<EngagementLetterResponse> engagementLetters);
        Task<List<SigningInfoModel>> GetAgreementIdsList(List<SigningInfo> signingInfos);
        Task<bool> UpdateHistoryLog(List<HistoryLogModel> historyLog);
        Task<string> GetEngagementLetterName(int letterId);
        Task<int> GetClientSignatureCountfromTemplate(int templateId);
        Task EngagementLetterErrorLog(EngagementLetter eng, string requestLog, string erorMessage);
        Task EsigningEngagementLetterErrorLog(SigningInfoModel agreement, string requestLog, string errorMessage);
        Task SaveEngagementLetterErrorLog(int batchId, string requestLog, string errorMessage);
    }
}
