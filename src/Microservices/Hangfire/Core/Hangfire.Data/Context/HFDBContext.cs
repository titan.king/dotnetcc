﻿using Microsoft.EntityFrameworkCore;

namespace Hangfire.Data.Context
{
    public class HFDBContext : DbContext
    {
        public HFDBContext(DbContextOptions<HFDBContext> options) : base(options)
        {
        }
    }
}
