﻿using AutoMapper;
using Engage3.Common.Business.Interfaces;
using EngageCC.DataContext;
using EngageCC.DataContext.DataModel;
using Hangfire.Data.Interfaces;
using Hangfire.Models.Signing;
using Microsoft.EntityFrameworkCore;

namespace Hangfire.Data.Repositories
{
    public class HangfireCCRepository : IHangfireCCRepository
    {
        private readonly DataContext _dbContext;
        private readonly DbContextOptions<DataContext> _dbContextOptions;
        private readonly IMapper _mapper;
        private readonly IGetCurrentDatetime _getCurrentDateTime;
        public HangfireCCRepository(DataContext dbContext, DbContextOptions<DataContext> dbContextOptions, IMapper mapper, IGetCurrentDatetime getCurrentDatetime)
        {
            _dbContext = dbContext;
            _dbContextOptions = dbContextOptions;
            _mapper = mapper;
            _getCurrentDateTime = getCurrentDatetime;
        }
        public async Task<bool> InsertEngagementLetterTable(List<EngagementLetterResponse> engagementLetters)
        {
            try
            {
                using (var _dbContext = new DataContext(_dbContextOptions))
                {
                    var tempData = _mapper.Map<List<EngagementLetter>>(engagementLetters);
                    if (tempData != null)
                    {
                        await _dbContext.EngagementLetter.AddRangeAsync(tempData);
                        await _dbContext.SaveChangesAsync();

                        return true;
                    }
                }
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<BatchRequest> GetBatchRequest(string status, bool isProcess)
        {
            try
            {
                using (var _dbContext = new DataContext(_dbContextOptions))
                {
                    BatchRequest? batchRequests = new BatchRequest();

                    batchRequests = await _dbContext.BatchRequest
                   .Where(n => n.Status == status && n.IsProcess == isProcess).FirstOrDefaultAsync();

                    if (batchRequests != null)
                    {
                        batchRequests.IsProcess = true;
                        await _dbContext.SaveChangesAsync();
                    }
                    return batchRequests ?? new BatchRequest();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task UpdateBatchRequestIsProcess(int batchId)
        {
            using (var _dbContext = new DataContext(_dbContextOptions))
            {
                var existingData = await _dbContext.BatchRequest.FirstOrDefaultAsync(n => n.BatchId == batchId);
                if (existingData != null && existingData.BatchId > 0)
                {
                    var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
                    existingData.IsProcess = true;
                    existingData.ModifiedOn = estTime;
                    _dbContext.Update(existingData);
                    await _dbContext.SaveChangesAsync();
                }
            }
        }
        public async Task<List<BulkLetters>> GetBulkLettersList(List<int?> bulkletterIds)
        {
            try
            {
                using (var _dbContext = new DataContext(_dbContextOptions))
                {
                    var records = await _dbContext.BulkLetters.Where(n => bulkletterIds.Contains(n.BulkLettersId)).ToListAsync();
                    return records;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<bool> UpdateEngagementLetterStatus(int batchId)
        {
            try
            {
                using (var _dbContext = new DataContext(_dbContextOptions))
                {
                    var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
                    var existingData = await _dbContext.BulkLetters.Where(n => n.BatchId == batchId).ToListAsync();
                    if (existingData != null && existingData.Count > 0)
                    {
                        
                        foreach (var item in existingData)
                        {
                            item.IsDraft = false;
                            item.DocumentStatusName = "Completed - Batch";
                            item.ModifiedOn = estTime;
                        }
                        _dbContext.UpdateRange(existingData);
                        await _dbContext.SaveChangesAsync();
                    }
                    var existBatchReqData = await _dbContext.BatchRequest.FirstOrDefaultAsync(n => n.BatchId == batchId);
                    if (existBatchReqData != null && existBatchReqData.BatchId > 0)
                    {
                        existBatchReqData.Status = "Completed - Batch";
                        existBatchReqData.ModifiedOn = estTime;
                        _dbContext.Update(existBatchReqData);
                        await _dbContext.SaveChangesAsync();
                        return true;
                    }
                }
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<List<EngagementLetter>> GetEngagementLettersList()
        {
            try
            {
                using (var _dbContext = new DataContext(_dbContextOptions))
                {
                    List<EngagementLetter> getEngageLetterList = await _dbContext.EngagementLetter
                        .Where(n => n.IsProcess == false && n.IsEnqueue == false && n.IsDeleted == false).Take(100).ToListAsync();
                    getEngageLetterList.ForEach(a => a.IsEnqueue = true);

                    await _dbContext.SaveChangesAsync();

                    return getEngageLetterList;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<string> GetMasterTemplate(int templateId)
        {
            try
            {
                using (var _dbContext = new DataContext(_dbContextOptions))
                {
                    var getMasterTemplate = await _dbContext.MasterTemplate.OrderByDescending(n => n.TemplateVersionId).FirstOrDefaultAsync(n => n.TemplateId == templateId);
                    return getMasterTemplate?.Template ?? string.Empty;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<int> GetClientSignatureCountfromTemplate(int templateId)
        {
            try
            {
                using (var _dbContext = new DataContext(_dbContextOptions))
                {
                    if (templateId > 0)
                    {
                        var getSignatureCount = await _dbContext.Template.FirstOrDefaultAsync(n => n.TemplateId == templateId);

                        return getSignatureCount?.ClientSignatureCount ?? 0;
                    }
                    return 0;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<string> GetTemplateAttachments(int templateId, int batchId)
        {
            try
            {
                using (var _dbContext = new DataContext(_dbContextOptions))
                {
                    var resData = await _dbContext.BulkLetterAttachments
                                                 .FirstOrDefaultAsync(n => n.TemplateId == templateId && n.BatchId == batchId && n.VersionNumber ==
                                                     _dbContext.BulkLetterAttachments
                                                         .Where(subQuery => subQuery.TemplateId == n.TemplateId && subQuery.BatchId == n.BatchId)
                                                         .Max(subQuery => subQuery.VersionNumber));

                    if (resData != null && !string.IsNullOrEmpty(resData.AttachmentsURL))
                    {
                        return resData?.AttachmentsURL ?? string.Empty;
                    }
                    var getTemplateAttachments = await _dbContext.Template.Where(n => n.TemplateId == templateId).FirstOrDefaultAsync();
                    return getTemplateAttachments?.AttachmentURL ?? string.Empty;
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        public async Task<List<LetterFieldValues>> GetLetterFieldValues(int bulkLetterId)
        {
            try
            {
                using (var _dbContext = new DataContext(_dbContextOptions))
                {

                    var latestRecords = await _dbContext.LetterFieldValues
                                              .Where(n => n.BulkLettersId == bulkLetterId && n.LetterVersion ==
                                                  _dbContext.LetterFieldValues
                                                      .Where(subQuery => subQuery.BulkLettersId == n.BulkLettersId)
                                                      .Max(subQuery => subQuery.LetterVersion))
                                              .ToListAsync();
                    return latestRecords;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> SaveSigningInfos(List<SigningInfo> signingInfos)
        {
            try
            {
                if (signingInfos.Any())
                {
                    using (var _dbContext = new DataContext(_dbContextOptions))
                    {
                        await _dbContext.SigningInfo.AddRangeAsync(signingInfos);
                        await _dbContext.SaveChangesAsync();
                        return true;
                    }
                }
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<string> GetSignatoryLastName(int bulklettersId, int? clientId)
        {
            try
            {
                using (var _dbContext = new DataContext(_dbContextOptions))
                {
                    if (bulklettersId > 0 && clientId > 0)
                    {
                        var latestSignatoryName = await _dbContext.LetterFieldValues
                                                        .Where(n => n.BulkLettersId == bulklettersId && n.ClientId == clientId)
                                                        .OrderByDescending(n => n.LetterVersion)
                                                        .Where(n => n.FieldName == "SignatoryLastName")
                                                        .Select(n => n.FieldValue)
                                                        .FirstOrDefaultAsync();
                        if (!string.IsNullOrEmpty(latestSignatoryName))
                        {
                            return latestSignatoryName;
                        }
                        else
                        {
                            return string.Empty;
                        }
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<string> GetChildEntityName(int bulklettersId, int? clientId)
        {
            try
            {
                using (var _dbContext = new DataContext(_dbContextOptions))
                {
                    if (bulklettersId > 0 && clientId > 0)
                    {
                        var latestSignatoryName = await _dbContext.LetterFieldValues
                                                        .Where(n => n.BulkLettersId == bulklettersId && n.ClientId == clientId)
                                                        .OrderByDescending(n => n.LetterVersion)
                                                        .Where(n => n.FieldName == "ChildEntities")
                                                        .Select(n => n.FieldValue)
                                                        .FirstOrDefaultAsync();
                        if (!string.IsNullOrEmpty(latestSignatoryName))
                        {
                            return latestSignatoryName ?? string.Empty;
                        }
                        else
                        {
                            return string.Empty;
                        }
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> UpdateEngagementLetter(List<EngagementLetter> engagementLetters)
        {
            try
            {
                if (engagementLetters.Any())
                {
                    using (var _dbContext = new DataContext(_dbContextOptions))
                    {

                        var existingData = await _dbContext.EngagementLetter
                                          .Where(a => engagementLetters.Select(x => x.BulkLettersId).Contains(a.BulkLettersId))
                                          .ToListAsync();

                        existingData.ForEach(a =>
                        {
                            a.IsProcess = true;
                            a.IsEnqueue = false;
                            a.DocumentStatusId = a.IsEsigning == true ? 2 : 3;
                        });

                        _dbContext.EngagementLetter.UpdateRange(existingData);
                        await _dbContext.SaveChangesAsync();

                    }
                }
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<bool> UpdateHistoryLog(List<HistoryLogModel> historyLog)
        {
            try
            {
                if (historyLog.Any())
                {
                    var mapRecordsList = _mapper.Map<List<HistoryLog>>(historyLog);
                    using (var _dbContext = new DataContext(_dbContextOptions))
                    {
                        await _dbContext.HistoryLog.AddRangeAsync(mapRecordsList);
                        await _dbContext.SaveChangesAsync();
                        return true;
                    }
                }
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<List<HistoryLogModel>> GetHistoryLogVersion(List<EngagementLetterResponse> engagementLetters)
        {
            try
            {
                using (var _dbContext = new DataContext(_dbContextOptions))
                {
                    List<HistoryLogModel> logModels = new List<HistoryLogModel>();
                    List<int> engagementLetterIds = engagementLetters.Select(e => e.EngagementLetterId).ToList();
                    int documentStatusIds = engagementLetters.Select(x => x.DocumentStatusId).FirstOrDefault();
                    var letterIds = await _dbContext.HistoryLog
                                    .Where(a => engagementLetterIds.Contains(a.EngagementLetterId ?? 0))
                                    .GroupBy(a => a.EngagementLetterId)
                                    .Select(group => new HistoryLogModel
                                    {
                                        EngagementLetterId = group.Key,
                                        Version = group.Max(a => a.Version) + 1
                                    })
                                    .ToListAsync();
                    if (letterIds.Count == 0)
                    {
                        // If no records were found, assign Version = 1 to all EngagementLetterIds.
                        var uniqueEngagementLetterIds = engagementLetterIds.Distinct();
                        var defaultLetterIds = uniqueEngagementLetterIds.Select(id => new HistoryLogModel
                        {
                            EngagementLetterId = id,
                            Version = 1
                        });

                        logModels.AddRange(defaultLetterIds);
                    }
                    else
                    {
                        logModels.AddRange(letterIds);
                    }
                    if (documentStatusIds == 9)
                    {
                        int eSigningStatusId = 2;
                        var getStatus = await _dbContext.DocumentStatus
                                        .Where(n => n.DocumentStatusId == eSigningStatusId)
                                        .Select(n => n.Description).FirstOrDefaultAsync();
                        if (!string.IsNullOrEmpty(getStatus))
                        {
                            foreach (var item in logModels)
                            {
                                item.Status = getStatus;
                            }
                        }
                    }
                    else if (documentStatusIds == 8)
                    {
                        int manualSigningStatusId = 3;
                        var getStatus = await _dbContext.DocumentStatus
                                        .Where(n => n.DocumentStatusId == manualSigningStatusId)
                                        .Select(n => n.Description).FirstOrDefaultAsync();
                        if (!string.IsNullOrEmpty(getStatus))
                        {
                            foreach (var item in logModels)
                            {
                                item.Status = getStatus;
                            }
                        }
                    }
                    return logModels;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<List<SigningInfoModel>> GetAgreementIdsList(List<SigningInfo> signingInfos)
        {
            using (var _dbContext = new DataContext(_dbContextOptions))
            {
                List<SigningInfo> signersAgreementList = new List<SigningInfo>();
                List<int> engagementLetterIds = signingInfos.Select(e => e.EngagementLetterId).ToList();
                var agreementIds = await _dbContext.SigningInfo
                                .Where(a => engagementLetterIds.Contains(a.EngagementLetterId) && a.ManualSigning == false)
                                .Select(a => new SigningInfo
                                {
                                    EngagementLetterId = a.EngagementLetterId,
                                    AgreementId = a.AgreementId
                                })
                                .ToListAsync();
                signersAgreementList.AddRange(agreementIds);
                var respModel = _mapper.Map<List<SigningInfoModel>>(signersAgreementList);
                var getEngagementLetterName = await _dbContext.EngagementLetter
                                                .Where(n => engagementLetterIds.Contains(n.EngagementLetterId))
                                                .Select(n => new SigningInfoModel
                                                {
                                                    EngagementLetterName = n.EngagementLetterName
                                                })
                                                .ToListAsync();
                respModel.AddRange(getEngagementLetterName);

                return respModel;
            }
        }
        public async Task<string> GetEngagementLetterName(int letterId)
        {
            try
            {
                string engagementLetterName = string.Empty;
                using (var _dbContext = new DataContext(_dbContextOptions))
                {
                    if (letterId > 0)
                    {
                        var getName = await _dbContext.EngagementLetter.FirstOrDefaultAsync(n => n.EngagementLetterId == letterId);
                        engagementLetterName = getName?.EngagementLetterName ?? string.Empty;
                        return engagementLetterName;
                    }
                }
                return engagementLetterName;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public async Task EngagementLetterErrorLog(EngagementLetter eng, string requestLog, string errorMessage)
        {
            if (eng != null)
            {
                var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
                var errorLogEntry = _mapper.Map<BulkLogger>(new BulkLetterErrorLogVM
                {
                    EngagementLetterId = eng.EngagementLetterId,
                    BatchId = eng.BatchId,
                    ClientId = eng.ClientId,
                    RequestLog = requestLog,
                    CreatedOn = eng.CreatedOn,
                    ModifiedOn = estTime,
                    ErrorMessage = errorMessage
                });

                using (var _dbContext = new DataContext(_dbContextOptions))
                {
                    _dbContext.BulkLoggers.Add(errorLogEntry);
                    await _dbContext.SaveChangesAsync();
                }
            }
        }

        public async Task EsigningEngagementLetterErrorLog(SigningInfoModel agreement, string requestLog, string errorMessage)
        {
            if (agreement != null)
            {
                var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
                var errorLogEntry = _mapper.Map<BulkLogger>(new BulkLetterErrorLogVM
                {
                    EngagementLetterId = agreement.EngagementLetterId,
                    RequestLog = requestLog,
                    CreatedOn = estTime,
                    ModifiedOn = estTime,
                    ErrorMessage = errorMessage
                });

                using (var _dbContext = new DataContext(_dbContextOptions))
                {
                    _dbContext.BulkLoggers.Add(errorLogEntry);
                    await _dbContext.SaveChangesAsync();
                }
            }
        }

        public async Task SaveEngagementLetterErrorLog(int batchId, string requestLog, string errorMessage)
        {
            var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
            var errorLogEntry = _mapper.Map<BulkLogger>(new BulkLetterErrorLogVM
            {
                BatchId = batchId,
                RequestLog = requestLog,
                CreatedOn = estTime,
                ModifiedOn = estTime,
                ErrorMessage = errorMessage
            });

            using (var _dbContext = new DataContext(_dbContextOptions))
            {
                _dbContext.BulkLoggers.Add(errorLogEntry);
                await _dbContext.SaveChangesAsync();
            }
        }
    }
}
