﻿namespace Hangfire.Models.Signing
{
    public class UploadPdfFile
    {
        public string PdfUrl { get; set; } = string.Empty;
        public string FileName { get; set; } = string.Empty;
    }
}
