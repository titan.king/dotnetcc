﻿namespace Hangfire.Models.Signing
{
    public class BatchJSONModel
    {
        public int? ClientId { get; set; }
        public int? BulkLettersId { get; set; }
        public int? BatchId { get; set; }
        public int? DocumentStatusId { get; set; }
        public int? EngageTypeId { get; set; }
        public int? DepartmentId { get; set; }
        public string? TaxYear { get; set; }
        public int? OfficeId { get; set; }
        public int? TemplateVersion { get; set; }
        public string? AdminName { get; set; }
        public string? AdminEmail { get; set; }
        public string? PartnerEmail { get; set; }
    }
}
