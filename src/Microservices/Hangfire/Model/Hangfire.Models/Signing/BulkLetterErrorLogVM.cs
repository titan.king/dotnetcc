﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangfire.Models.Signing
{
    public class BulkLetterErrorLogVM
    {
        public int EngagementLetterId { get; set; }
        public int BatchId { get; set; }
        public int? ClientId { get; set; }
        public string? RequestLog { get; set; }
        public string ErrorMessage { get; set; } = string.Empty;
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }

    }
}
