﻿namespace Hangfire.Models.Signing
{
    public class AdobeESignModel
    {
        public List<FileInfo> fileInfos { get; set; }
        public string name { get; set; }
        public List<ParticipantSetsInfo> participantSetsInfo { get; set; }
        public string signatureType { get; set; }
        public ExternalId externalId { get; set; }
        public string state { get; set; }
        public string message { get; set; }
        public string expirationTime { get; set; }
    }
    public class ExternalId
    {
        public string id { get; set; }
    }

    public class FileInfo
    {
        public UrlFileInfo urlFileInfo { get; set; }
    }

    public class MemberInfo
    {
        public string email { get; set; }
    }

    public class ParticipantSetsInfo
    {
        public List<MemberInfo> memberInfos { get; set; }
        public int order { get; set; }
        public string role { get; set; }
    }
    public class UrlFileInfo
    {
        public string mimeType { get; set; }
        public string name { get; set; }
        public string url { get; set; }
    }
    public class AdobeESignExpireyModel
    {
        public List<FileInfo> fileInfos { get; set; }
        public string name { get; set; }
        public List<ParticipantSetsInfo> participantSetsInfo { get; set; }
        public string signatureType { get; set; }
        public ExternalId externalId { get; set; }
        public string state { get; set; }
        public string message { get; set; }
    }
}
