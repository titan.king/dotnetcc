﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangfire.Models.Signing
{
    public class DocumentStatusModel
    {
        public int DocumentStatusId { get; set; }
        public string? DocumentStatusName { get; set;}
        public int? EngagementLetterId { get; set;}
    }
}
