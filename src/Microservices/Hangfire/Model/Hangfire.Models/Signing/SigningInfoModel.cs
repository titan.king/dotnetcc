﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangfire.Models.Signing
{
    public class SigningInfoModel
    {
        public int SigningInfoId { get; set; }
        public int EngagementLetterId { get; set; }
        public string? EngagementLetterName { get; set; }
        public string? SigningPartnerName { get; set; } = string.Empty;
        public string? DelegatedName { get; set; } = string.Empty;
        public string? SPEmail { get; set; } = string.Empty;
        public string? ContactPerson1Name { get; set; } = string.Empty;
        public string? ContactPerson1Email { get; set; } = string.Empty;
        public string? ContactPerson2Name { get; set; } = string.Empty;
        public string? ContactPerson2Email { get; set; } = string.Empty;
        public string? Title { get; set; } = string.Empty;
        public string? Message { get; set; } = string.Empty;
        public bool IsDeleted { get; set; } = false;
        public string? CreatedBy { get; set; } = string.Empty;
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool ManualSigning { get; set; } = false;
        public string? FileId { get; set; } = string.Empty;
        public string? DelegatedBy { get; set; } = string.Empty;
        public string? AgreementId { get; set; } = string.Empty;
        public string? ModifiedBy { get; set; } = string.Empty;
        public DateTime? DeletedOn { get; set; }
        public string? DeletedBy { get; set; } = string.Empty;
        public string? PdfUrl { get; set; }
        public int? DocumentStatusId { get; set; }
    }
}
