﻿namespace Hangfire.Models.Signing
{
    public class AdobeESignResponse
    {
        public string id { get; set; }
    }
    public class SignedDocumentResponse
    {
        public string url { get; set; }
    }
}
