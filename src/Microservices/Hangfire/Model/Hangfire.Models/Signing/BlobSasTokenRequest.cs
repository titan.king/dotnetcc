﻿namespace Hangfire.Models.Signing
{
    public class BlobSasTokenRequest
    {
        public string AccountKey { get; set; } = string.Empty;
        public string AccountName { get; set; } = string.Empty;
        public string ConnectionString { get; set; } = string.Empty;
        public string ContainerName { get; set; } = string.Empty;
        public string FileName { get; set; } = string.Empty;
    }
}
