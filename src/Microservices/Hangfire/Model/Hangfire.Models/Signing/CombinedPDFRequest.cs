﻿namespace Hangfire.Models.Signing
{
    public class CombinedPDFRequest
    {
        public List<Assete> assets { get; set; }
    }
    public class Assete
    {
        public string assetID { get; set; } = string.Empty;
    }
    public class CombinedPDFResponseModel
    {
        public string status { get; set; } = string.Empty;
        public Assetes asset { get; set; }
    }
    public class Assetes
    {
        public Metadatas metadata { get; set; }
        public string downloadUri { get; set; } = string.Empty;
        public string assetID { get; set; } = string.Empty;
    }
    public class Metadatas
    {
        public string type { get; set; } = string.Empty;
        public int size { get; set; }
    }
}
