﻿using System.Net;

namespace Hangfire.Models.Signing
{
    public class PdfFileResponse
    {
        public string PdflUrl { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public string Message { get; set; }
    }
}
