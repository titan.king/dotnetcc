﻿using EngageCC.DataContext.DataModel;

namespace Hangfire.Business.Interfaces
{
    public interface IHangfireCCServices
    {
        Task<bool> SaveEngamentLetters();
        Task<bool> ProceedForESigningAndManualSigning();
        Task<List<SigningInfo>> SendForESigning(EngagementLetter engagementLetter, SigningInfo signingInfo, string letterPdf, string attachment, string TaxReturnConsentForm);
        Task<List<SigningInfo>> SendForManualSigning(EngagementLetter engagementLetter, SigningInfo signingInfo, string letterPdf, string attachment,string letterPdfAssetId, string formPdfAssetId, string accessToken);
    }
}
