﻿using AutoMapper;
using Azure.Core;
using Engage3.Common.Business.Interfaces;
using EngageCC.DataContext.DataModel;
using Hangfire.Business.Interfaces;
using Hangfire.Data.Interfaces;
using Hangfire.Models.Signing;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System.Collections.Generic;
using System.Data.Common;
using System.Net;
using System.Security.AccessControl;

namespace Hangfire.Business.Services
{
    public class HangfireCCServices : IHangfireCCServices
    {
        private readonly IAdobeService _adobeService;
        private readonly IHangfireCCRepository _hangfireCCRepository;
        ResponseModel resobj = new ResponseModel();
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IGetCurrentDatetime _getCurrentDateTime;
        public HangfireCCServices(IHangfireCCRepository hangfireCCRepository, IMapper mapper, IConfiguration configuration, IAdobeService adobeService, IGetCurrentDatetime getCurrentDatetime)
        {
            _mapper = mapper;
            _hangfireCCRepository = hangfireCCRepository;
            _configuration = configuration;
            _adobeService = adobeService;
            _getCurrentDateTime = getCurrentDatetime;
        }
        public async Task<bool> SaveEngamentLetters()
        {
            bool isSuccessful = false;
            string statusString = "InProgress";
            bool isProcess = false;
            var getBulkLetters = await _hangfireCCRepository.GetBatchRequest(statusString, isProcess);
            try
            {
                if (getBulkLetters != null && getBulkLetters.BatchId > 0)
                {
                    var serialBatchRequest = JsonConvert.SerializeObject(getBulkLetters);
                    var getBatchJSON = JsonConvert.DeserializeObject<BatchRequest>(serialBatchRequest);
                    int templateId = getBatchJSON.TemplateId;
                    var getBulkLetterJSON = JsonConvert.DeserializeObject<List<BatchJSONModel>>(getBatchJSON?.BatchJson);
                    if (getBulkLetterJSON.Count > 0)
                    {
                        InsertBulkLettersModel lettersModel = new InsertBulkLettersModel();
                        lettersModel.DocumentStatusId = getBulkLetterJSON[0]?.DocumentStatusId ?? 0;
                        lettersModel.DepartmentId = getBulkLetterJSON[0]?.DepartmentId ?? 0;
                        lettersModel.EngageTypeId = getBulkLetterJSON[0]?.EngageTypeId ?? 0;
                        lettersModel.OfficeId = getBulkLetterJSON[0]?.OfficeId ?? 0;
                        lettersModel.TaxYear = getBulkLetterJSON[0]?.TaxYear ?? string.Empty;
                        lettersModel.TemplateVersion = getBulkLetterJSON[0]?.TemplateVersion ?? 0;
                        lettersModel.TemplateId = templateId;
                        List<int?> bulkLetterIds = getBulkLetterJSON.Select(item => item.BulkLettersId).ToList();
                        List<EngagementLetterResponse> bulkLettersResData = new List<EngagementLetterResponse>();

                        if (bulkLetterIds.Count > 0)
                        {

                            int letterBatchCount = 10;
                            int batchSize = (int)Math.Ceiling((double)bulkLetterIds.Count / letterBatchCount);

                            var tasks = new List<Task>();

                            for (int i = 0; i < letterBatchCount; i++)
                            {
                                try
                                {
                                    var batch = bulkLetterIds.Skip(i * batchSize).Take(batchSize).ToList();
                                    List<BulkLetters> getBulkLettersList = await _hangfireCCRepository.GetBulkLettersList(batch);
                                    if (getBulkLettersList.Any())
                                    {
                                        tasks.Add(Task.Run(async () => bulkLettersResData.AddRange(await ArrageEngagementLetterData(getBulkLettersList, lettersModel, getBulkLetterJSON))));
                                        //bulkLettersResData.AddRange(await ArrageEngagementLetterData(getBulkLettersList, lettersModel, getBulkLetterJSON));
                                    }
                                }
                                catch (Exception ex)
                                {
                                    var errorMessage = ex.InnerException?.ToString() + ex.StackTrace?.ToString() + ex.Message?.ToString();
                                    var requestLog = JsonConvert.SerializeObject(getBulkLetterJSON);
                                    int batchId = getBatchJSON.BatchId;
                                    await _hangfireCCRepository.SaveEngagementLetterErrorLog(batchId, requestLog, errorMessage);
                                }
                            }
                            await Task.WhenAll(tasks);
                            if (bulkLettersResData.Count() > 0)
                            {
                                try
                                {
                                    bool isInserted = await _hangfireCCRepository.InsertEngagementLetterTable(bulkLettersResData);
                                    if (isInserted)
                                    {
                                        int batchIdtoUpdate = getBatchJSON.BatchId;
                                        if (batchIdtoUpdate > 0)
                                        {
                                            var isStatusUpdated = await _hangfireCCRepository.UpdateEngagementLetterStatus(batchIdtoUpdate);
                                            if (isStatusUpdated)
                                            {
                                                isSuccessful = isStatusUpdated;
                                            }
                                            else { isSuccessful = isStatusUpdated; }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    var errorMessage = ex.InnerException?.ToString() + ex.StackTrace?.ToString() + ex.Message?.ToString();
                                    var requestLog = JsonConvert.SerializeObject(getBulkLetters);
                                    int batchId = getBatchJSON.BatchId;
                                    await _hangfireCCRepository.SaveEngagementLetterErrorLog(batchId, requestLog, errorMessage);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var errorMessage = ex.InnerException?.ToString() + ex.StackTrace?.ToString() + ex.Message?.ToString();
                var requestLog = JsonConvert.SerializeObject(getBulkLetters);
                int batchId = getBulkLetters.BatchId;
                await _hangfireCCRepository.SaveEngagementLetterErrorLog(batchId, requestLog, errorMessage);
            }

            return isSuccessful;
        }
        private async Task<List<EngagementLetterResponse>> ArrageEngagementLetterData(List<BulkLetters> getBulkLettersList, InsertBulkLettersModel lettersModel, List<BatchJSONModel> batchJSONs)
        {
            var engagementLetterModel = new List<EngagementLetterResponse>();
            try
            {
                foreach (var item in getBulkLettersList)
                {

                    var getAdminEmail = batchJSONs.FirstOrDefault(n => n.ClientId == item.ClientId && n.BulkLettersId == item.BulkLettersId)?.AdminEmail;
                    var getPartnerEmail = batchJSONs.FirstOrDefault(n => n.ClientId == item.ClientId && n.BulkLettersId == item.BulkLettersId)?.PartnerEmail;
                    string getOtherEntityDetails = await _hangfireCCRepository.GetChildEntityName(item.BulkLettersId, item.ClientId);
                    string getPrimarySignerLastName = await _hangfireCCRepository.GetSignatoryLastName(item.BulkLettersId, item.ClientId);
                    int taxYear = !string.IsNullOrEmpty(lettersModel.TaxYear) ? Convert.ToInt32(lettersModel.TaxYear) : Convert.ToInt32(_configuration["TaxYear"]);
                    string engagementLetterName = taxYear + " " + item.ClientName + " " + item.TemplateName;
                    var letterModel = new EngagementLetterResponse()
                    {
                        BatchId = item.BatchId,
                        OfficeId = lettersModel.OfficeId ?? 0,
                        EngagementLetterName = engagementLetterName,
                        TaxYear = taxYear,
                        OfficeName = item.Office,
                        TemplateId = lettersModel.TemplateId,
                        TemplateName = item.TemplateName,
                        BulkLettersId = item.BulkLettersId,
                        ClientId = item.ClientId,
                        ClientName = item.ClientName,
                        PartnerName = item.PartnerName,
                        PartnerId = item.PartnerId,
                        AdminName = item.ModifiedBy,
                        DocumentStatusId = item.IsEsigning == true ? 9 : 8,
                        EngageTypeId = lettersModel.EngageTypeId ?? 0,
                        DepartmentId = lettersModel.DepartmentId ?? 0,
                        TemplateVersion = lettersModel.TemplateVersion ?? 0,
                        CreatedOn = item.CreatedOn,
                        CreatedBy = item.CreatedBy,
                        ModifiedOn = item.ModifiedOn,
                        ModifiedBy = item.ModifiedBy,
                        DeletedOn = item.DeletedOn,
                        DeletedBy = item.DeletedBy,
                        IsEnqueue = item.IsEnqueue,
                        IsProcess = item.IsProcess,
                        ClientSignatureCount = item.ClientSignatureCount,
                        Is7216Available = item.Is7216Available,
                        IsNewClient = item.IsNewClient,
                        SignatoryEmailId = item.SignatoryEmailId,
                        SignatoryFirstName = item.SignatoryFirstName,
                        PrimarySignerLastName = getPrimarySignerLastName,
                        SpouseEmailId = item.SpouseEmailId,
                        SpouseFirstName = item.SpouseFirstName,
                        IsEsigning = item.IsEsigning,
                        OtherEntityDetails = getOtherEntityDetails,
                        AdminEmail = getAdminEmail ?? string.Empty,
                        PartnerEmail = getPartnerEmail ?? string.Empty,
                        ReturnTypeCode = item.ReturnTypeCode
                    };
                    engagementLetterModel.Add(letterModel);

                }
                return engagementLetterModel;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<bool> ProceedForESigningAndManualSigning()
        {
            List<SigningInfo> signingInfosList = new List<SigningInfo>();
            try
            {
                var accessToken = await _adobeService.GetAccessToken();
                if (accessToken != null && accessToken.StatusCode == HttpStatusCode.OK && accessToken.Data != null)
                {

                    var serial = JsonConvert.SerializeObject(accessToken.Data);
                    var authorize_token = JsonConvert.DeserializeObject<AccessTokenResponse>(serial);
                    string authorize = authorize_token?.access_token ?? string.Empty;
                    List<EngagementLetter> engagementLetters = await _hangfireCCRepository.GetEngagementLettersList();

                    int letterBatchCount = 10;
                    int batchSize = (int)Math.Ceiling((double)engagementLetters.Count / letterBatchCount);

                    var tasks = new List<Task>();

                    for (int i = 0; i < letterBatchCount; i++)
                    {
                        var batch = engagementLetters.Skip(i * batchSize).Take(batchSize).ToList();
                        if (batch.Any())
                        {
                            try
                            {
                                //signingInfosList.AddRange(await SendToAdobe(batch, authorize));
                                tasks.Add(Task.Run(async () => signingInfosList.AddRange(await SendToAdobe(batch, authorize))));
                            }
                            catch (Exception ex)
                            {
                                var errorMessage = ex.InnerException?.ToString() + ex.StackTrace?.ToString() + ex.Message?.ToString();
                                var requestLog = JsonConvert.SerializeObject(engagementLetters);
                                await _hangfireCCRepository.SaveEngagementLetterErrorLog(0, requestLog, errorMessage);
                            }
                        }
                    }
                    await Task.WhenAll(tasks);
                    List<EngagementLetterResponse> engagementLetterList = new List<EngagementLetterResponse>();
                    List<SigningInfoModel> signingInfos = new List<SigningInfoModel>();
                    try
                    {
                        if (signingInfosList.Any())
                        {
                            await _hangfireCCRepository.SaveSigningInfos(signingInfosList);
                            var respMap = _mapper.Map<List<SigningInfoModel>>(signingInfosList);
                            foreach (var agreement in respMap)
                            {
                                try
                                {
                                    var agreementID = agreement.AgreementId;
                                    if (agreementID != null && agreement.ManualSigning == false)
                                    {
                                        var getdocumentURL = await _adobeService.GetSignedDocument(agreementID);
                                        if (getdocumentURL.StatusCode == HttpStatusCode.OK)
                                        {
                                            var docFileURL = JsonConvert.SerializeObject(getdocumentURL);
                                            var completeDocFileURL = JsonConvert.DeserializeObject<ResponseModel>(docFileURL);

                                            HtmlFileResponseModel htmlFileResponse = new HtmlFileResponseModel();
                                            string fileName = await _hangfireCCRepository.GetEngagementLetterName(agreement.EngagementLetterId);
                                            UploadPdfFile uploadPdf = new UploadPdfFile();
                                            uploadPdf.PdfUrl = completeDocFileURL?.Data?.ToString() ?? string.Empty;
                                            uploadPdf.FileName = agreement.EngagementLetterId.ToString() + "-" + fileName ?? string.Empty;
                                            var respPDF = await _adobeService.UploadPdfFileFromBlob(uploadPdf);
                                            //resobj.PdfUrl = respPDF.PdfUrl;
                                            agreement.PdfUrl = respPDF?.Data?.ToString();
                                            signingInfos.Add(agreement);
                                        }
                                    }
                                    else if (agreement.ManualSigning == true && agreement.PdfUrl != null)
                                    {
                                        signingInfos.Add(agreement);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    var errorMessage = ex.InnerException?.ToString() + ex.StackTrace?.ToString() + ex.Message?.ToString();
                                    var requestLog = JsonConvert.SerializeObject(engagementLetters);
                                    await _hangfireCCRepository.EsigningEngagementLetterErrorLog(agreement, requestLog, errorMessage);
                                }
                            }
                            engagementLetterList = _mapper.Map<List<EngagementLetterResponse>>(engagementLetters);
                            foreach (var eachLetter in engagementLetterList)
                            {
                                eachLetter.PdfUrl = signingInfos.Where(n => n.EngagementLetterId == eachLetter.EngagementLetterId).Select(n => n.PdfUrl).FirstOrDefault();
                            }
                        }
                        if (engagementLetters.Any())
                        {
                            await _hangfireCCRepository.UpdateEngagementLetter(engagementLetters);
                            if (engagementLetterList.Any())
                            {
                                //Insert into History Log
                                List<HistoryLogModel> logModels = await _hangfireCCRepository.GetHistoryLogVersion(engagementLetterList);
                                List<EngagementLetter> engagements = new List<EngagementLetter>();

                                logModels = logModels.Select(vm =>
                                {
                                    var dataItem = engagementLetterList.FirstOrDefault(di => di.EngagementLetterId == vm.EngagementLetterId);
                                    if (dataItem != null)
                                    {
                                        DateTime utcNow = DateTime.UtcNow;
                                        TimeZoneInfo easternTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                                        DateTime estTime = TimeZoneInfo.ConvertTimeFromUtc(utcNow, easternTimeZone);
                                        vm.EngagementLetterId = dataItem.EngagementLetterId;
                                        vm.EngagementLetterName = dataItem.EngagementLetterName;
                                        vm.BatchId = dataItem.BatchId;
                                        vm.Status = dataItem.IsEsigning == true ? "With Client For E-Signing" : "With Client For Manual Signing";
                                        vm.EditedBy = dataItem.CreatedBy;
                                        vm.LastModified = estTime;
                                        vm.PDFUrl = dataItem.PdfUrl;
                                    }
                                    return vm;
                                }).ToList();
                                if (logModels.Any())
                                {
                                    var historyResp = await _hangfireCCRepository.UpdateHistoryLog(logModels);
                                }
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        var errorMessage = ex.InnerException?.ToString() + ex.StackTrace?.ToString() + ex.Message?.ToString();
                        var requestLog = JsonConvert.SerializeObject(engagementLetters);
                        await _hangfireCCRepository.SaveEngagementLetterErrorLog(0, requestLog, errorMessage);
                    }
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                var errorMessage = ex.InnerException?.ToString() + ex.StackTrace?.ToString() + ex.Message?.ToString();
                var requestLog = string.Empty;
                await _hangfireCCRepository.SaveEngagementLetterErrorLog(0, requestLog, errorMessage);
            }
            return false;
        }
        public async Task<List<SigningInfo>> SendToAdobe(List<EngagementLetter> engagementLetters, string accessToken)
        {
            ResponseModel responseModel = new ResponseModel();
            List<SigningInfo> signingInfosList = new List<SigningInfo>();
            string letterPdf = string.Empty;
            string letterPdfAssetId = string.Empty;
            string formPdfAssetId = string.Empty;
            try
            {
                if (engagementLetters.Any())
                {
                    foreach (var eng in engagementLetters)
                    {
                        try
                        {
                            SigningInfo signingInfo = new SigningInfo();
                            var masterTemplate = await _hangfireCCRepository.GetMasterTemplate(eng.TemplateId ?? 0);

                            var fields = await _hangfireCCRepository.GetLetterFieldValues(eng.BulkLettersId);
                            if (!string.IsNullOrEmpty(masterTemplate) && fields.Any())
                            {
                                string updatedContent = masterTemplate;
                                foreach (var val in fields)
                                {
                                    if (val.FieldName == "Date")
                                    {

                                        string dateFl = Convert.ToDateTime(val.FieldValue).ToString("MMMM d, yyyy");
                                        updatedContent = updatedContent.Replace("##" + val.FieldName + "##", dateFl);
                                    }
                                    else
                                    {
                                        if (val.FieldName == "ChildEntities")
                                        {
                                            if (!string.IsNullOrEmpty(val.FieldValue))
                                            {
                                                //string[] values = val.FieldValue.Split(',');
                                                List<string> childEntitiesArray = JsonConvert.DeserializeObject<List<string>>(val.FieldValue);
                                                updatedContent = ReplacePlaceholderWithValues(updatedContent, childEntitiesArray);
                                            }
                                            else
                                            {
                                                updatedContent = updatedContent.Replace("##" + val.FieldName + "##", string.Empty);
                                            }
                                        }
                                        else
                                        {
                                            if (!string.IsNullOrEmpty(val.FieldValue))
                                            {
                                                updatedContent = updatedContent.Replace("##" + val.FieldName + "##", val.FieldValue);
                                            }
                                            else
                                            {
                                                updatedContent = updatedContent.Replace("##" + val.FieldName + "##", string.Empty);
                                            }
                                        }
                                    }
                                }
                                //string dateF = date.ToString("MMMM d, yyyy");
                                //updatedContent = updatedContent.Replace("##Date##", dateF);
                                updatedContent = updatedContent.Replace("##ClientID##", eng.ClientId.ToString());
                                updatedContent = updatedContent.Replace("##Lettername##", eng.EngagementLetterName);

                                UploadHtmlFile uploadHtmlFile = new UploadHtmlFile();

                                uploadHtmlFile.HtmlData = updatedContent;
                                uploadHtmlFile.FileName = eng.EngagementLetterName ?? string.Empty;
                                var uploadRes = await _adobeService.UploadHtmlFileFromBlob(uploadHtmlFile);
                                if (uploadRes != null && uploadRes.StatusCode == HttpStatusCode.OK && uploadRes.Data != null)
                                {
                                    PdfConversionRequest pdfConversionRequest = new PdfConversionRequest();
                                    var pdfConversion = new PdfConversion();

                                    pdfConversion.inputUrl = uploadRes?.Data?.ToString() ?? string.Empty;
                                    pdfConversion.includeHeaderFooter = true;
                                    pdfConversion.pageLayout = new PageLayout { pageWidth = "11", pageHeight = "8.5" };
                                    pdfConversion.json = "{}";

                                    pdfConversionRequest.pdfConversion = pdfConversion;

                                    var pdfResponse = await _adobeService.HtmltoPDFConvert(pdfConversionRequest, accessToken);
                                    if (pdfResponse.StatusCode == HttpStatusCode.OK && pdfResponse.Data != null)
                                    {
                                        letterPdf = pdfResponse?.Data?.ToString() ?? string.Empty;
                                        letterPdfAssetId = pdfResponse?.PdfUrl?.ToString() ?? string.Empty;
                                    }
                                }
                            }

                            string attachment = await _hangfireCCRepository.GetTemplateAttachments(eng.TemplateId ?? 0, eng.BatchId);
                            int signatureCount = await _hangfireCCRepository.GetClientSignatureCountfromTemplate(eng.TemplateId ?? 0);
                            string TaxReturnConsentForm = string.Empty;
                            if (eng.Is7216Available == true)
                            {
                                ResponseModel dataRes = new ResponseModel();
                                if (signatureCount == 1)
                                {
                                    dataRes = await _adobeService.GetFileFromBlob(1);
                                }
                                else if (signatureCount == 2)
                                {
                                    dataRes = await _adobeService.GetFileFromBlob(2);
                                }

                                if (dataRes != null && dataRes.StatusCode == HttpStatusCode.OK && dataRes.Data != null)
                                {
                                    string consentFormPath = dataRes?.Data?.ToString() ?? string.Empty;
                                    if (!string.IsNullOrEmpty(consentFormPath))
                                    {
                                        using (HttpClient client = new HttpClient())
                                        {
                                            HttpResponseMessage responseUrl = await client.GetAsync(consentFormPath);
                                            if (responseUrl.IsSuccessStatusCode)
                                            {
                                                string htmlContent = await responseUrl.Content.ReadAsStringAsync();

                                                int form7216Taxyear = Convert.ToInt32(_configuration["Form7216_TaxYear"]);
                                                var year = Convert.ToInt16(eng.TaxYear) + form7216Taxyear;
                                                string get7216ExpiryDate = "December 31, " + year;
                                                signingInfo.ExpiryDate7216 = get7216ExpiryDate;
                                                string modifiedContent = htmlContent.Replace("{{consentDate}}", "December 31, " + year);
                                                modifiedContent = modifiedContent.Replace("##ClientId##", eng.ClientId.ToString());
                                                modifiedContent = modifiedContent.Replace("##ClientName##", eng.ClientName);
                                                UploadHtmlFile uploadHtmlFile = new UploadHtmlFile();

                                                uploadHtmlFile.HtmlData = modifiedContent;
                                                uploadHtmlFile.FileName = eng.EngagementLetterName + "_Form7216";
                                                var uploadRes = await _adobeService.UploadHtmlFileFromBlob(uploadHtmlFile);
                                                if (uploadRes != null && uploadRes.StatusCode == HttpStatusCode.OK && uploadRes.Data != null)
                                                {
                                                    PdfConversionRequest pdfConversionRequest = new PdfConversionRequest();
                                                    var pdfConversion = new PdfConversion();

                                                    pdfConversion.inputUrl = uploadRes?.Data?.ToString() ?? string.Empty;
                                                    pdfConversion.includeHeaderFooter = true;
                                                    pdfConversion.pageLayout = new PageLayout { pageWidth = "11", pageHeight = "8.5" };
                                                    pdfConversion.json = "{}";

                                                    pdfConversionRequest.pdfConversion = pdfConversion;

                                                    var pdfResponse = await _adobeService.HtmltoPDFConvert(pdfConversionRequest, accessToken);
                                                    if (pdfResponse.StatusCode == HttpStatusCode.OK && pdfResponse.Data != null)
                                                    {
                                                        TaxReturnConsentForm = pdfResponse?.Data?.ToString() ?? string.Empty;
                                                        formPdfAssetId = pdfResponse?.PdfUrl?.ToString() ?? string.Empty;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            var estTime = await _getCurrentDateTime.GetCurrentEstDatetime();
                            signingInfo.EngagementLetterId = eng.EngagementLetterId;
                            signingInfo.SigningPartnerName = eng.PartnerName;
                            signingInfo.ContactPerson1Email = eng.SignatoryEmailId;
                            signingInfo.ContactPerson1Name = eng.SignatoryFirstName;
                            signingInfo.ContactPerson2Email = eng.SpouseEmailId;
                            signingInfo.ContactPerson2Name = eng.SpouseFirstName;
                            signingInfo.CreatedBy = eng.CreatedBy;
                            signingInfo.CreatedOn = estTime;

                            if (eng.IsEsigning == true)
                            {
                                signingInfosList = await SendForESigning(eng, signingInfo, letterPdf, attachment, TaxReturnConsentForm);
                            }
                            else
                            {
                                signingInfosList = await SendForManualSigning(eng, signingInfo, letterPdf, attachment, letterPdfAssetId, formPdfAssetId, accessToken);
                            }
                        }
                        catch (Exception ex)
                        {
                            var errorMessage = ex.InnerException?.ToString() + ex.StackTrace?.ToString() + ex.Message?.ToString();
                            responseModel.StatusCode = HttpStatusCode.InternalServerError;
                            responseModel.ErrorMessage = errorMessage;
                            var requestLog = JsonConvert.SerializeObject(engagementLetters);
                            await _hangfireCCRepository.EngagementLetterErrorLog(eng, requestLog, errorMessage);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return signingInfosList;
        }

        static string ReplacePlaceholderWithValues(string inputHtml, List<string> values)
        {
            string placeholder = "##ChildEntities##";
            string resultHtml = inputHtml;
            string replacement = "";
            int instCount = 1;
            for (int i = 0; i < values.Count(); i++)
            {
                if (!string.IsNullOrEmpty(values[i]))
                {
                    if (values.Count() == 1)
                    {
                        replacement += "&nbsp; &nbsp; &nbsp;" + values[i] + "<br/>";
                    }
                    else
                    {
                        if (values.Count() == instCount)
                        {
                            replacement += "&nbsp; &nbsp; &nbsp;" + values[i] + "<br/>";
                        }
                        else
                        {
                            replacement += "&nbsp; &nbsp; &nbsp;" + values[i] + ",<br/>";
                        }

                    }
                    instCount++;
                }
            }
            resultHtml = resultHtml.Replace(placeholder, replacement);

            return resultHtml;
        }

        public async Task<List<SigningInfo>> SendForESigning(EngagementLetter engagementLetter, SigningInfo signingInfo, string letterPdf, string attachment, string TaxReturnConsentForm)
        {
            ResponseModel responseModel = new ResponseModel();
            List<SigningInfo> signingInfosList = new List<SigningInfo>();
            try
            {
                string connectionString = _configuration.GetConnectionString("EngageDbConnection") ?? string.Empty;
                string subject_Append = "Dev";
                if (!string.IsNullOrEmpty(connectionString))
                {
                    var builder = new DbConnectionStringBuilder { ConnectionString = connectionString };
                    if (builder.ContainsKey("Database"))
                    {
                        string databaseName = builder["Database"].ToString() ?? string.Empty;
                        if (databaseName == "Engage3_MainSqlDB")
                        {
                            subject_Append = "Dev";
                        }
                        else if (databaseName == "Engage3_QA_MainSqlDB")
                        {
                            subject_Append = "Qa";
                        }
                        else if (databaseName == "Engage3_UAT_MainSqlDB")
                        {
                            subject_Append = "Uat";
                        }
                    }
                }
                var integrationkey = _configuration["AppSettingsAdobe:adobe_integrationkey"];
                var adobeURL = _configuration["AppSettingsAdobe:SendToAdobeSignURL"] ?? string.Empty;
                var Oauthreq = new RestClient(adobeURL);
                var Oauthreq1 = new RestRequest(Method.POST);
                Oauthreq1.AddHeader("Authorization", "Bearer " + integrationkey);
                Oauthreq1.AddHeader("Content-Type", "application/json");
                var getProductionEmailStatus = _configuration["AppSettingsAdobe:ProductionEmailNeeded"] ?? string.Empty;
                string signer2 = string.Empty;
                string ContactPerson1Email = string.Empty;
                if (getProductionEmailStatus == "true")
                {
                    if (subject_Append == "Dev")
                    {
                        ContactPerson1Email = _configuration["ESignToEmail:DevSigner1"] ?? string.Empty;
                        signingInfo.ContactPerson1Email = ContactPerson1Email;
                        if (engagementLetter.ClientSignatureCount == 2)
                        {
                            signer2 = _configuration["ESignToEmail:DevSigner2"] ?? string.Empty;
                            signingInfo.ContactPerson2Email = signer2;
                        }
                    }
                    else if (subject_Append == "Qa")
                    {
                        ContactPerson1Email = _configuration["ESignToEmail:QaSigner1"] ?? string.Empty;
                        signingInfo.ContactPerson1Email = ContactPerson1Email;
                        if (engagementLetter.ClientSignatureCount == 2)
                        {
                            signer2 = _configuration["ESignToEmail:QaSigner2"] ?? string.Empty;
                            signingInfo.ContactPerson2Email = signer2;
                        }
                    }
                    else if (subject_Append == "Uat")
                    {
                        ContactPerson1Email = _configuration["ESignToEmail:UatSigner1"] ?? string.Empty;
                        signingInfo.ContactPerson1Email = ContactPerson1Email;
                        if (engagementLetter.ClientSignatureCount == 2)
                        {
                            signer2 = _configuration["ESignToEmail:UatSigner2"] ?? string.Empty;
                            signingInfo.ContactPerson2Email = signer2;
                        }
                    }
                }
                else
                {
                    ContactPerson1Email = signingInfo?.ContactPerson1Email ?? string.Empty;
                    if (engagementLetter.ClientSignatureCount == 2)
                    {
                        signer2 = signingInfo?.ContactPerson2Email ?? string.Empty;
                    }
                }

                var ContactPerson2Email = signer2;
                var PDFUrl = letterPdf;
                var EngagementLetterName = engagementLetter.EngagementLetterName + ".pdf";

                var Attachment0 = attachment;
                var message = "";

                var reqbody = "";



                string emailSubject = subject_Append + "_" + engagementLetter.BatchId + "_" + engagementLetter.EngagementLetterId + "_Signature";
                reqbody = @"{
                          ""fileInfos"": [
                        {
                          ""urlFileInfo"": {
                              ""mimeType"": ""application/pdf"",
                              ""name"": ""@@EngagementLetterName"",
                              ""url"": ""@@PDFUrl""
                              }
                        },
                        {
                          ""urlFileInfo"": {
                              ""mimeType"": ""application/pdf"",
                              ""name"": ""Attachment File0.pdf"",
                              ""url"": ""@@Attachment0""
                              }
                         },
                         {
                          ""urlFileInfo"": {
                              ""mimeType"": ""application/pdf"",
                              ""name"": ""7216 Consent Form.pdf"",
                              ""url"": ""@@TaxReturnConsentForm""
                              }
                         }
                         ],
                          ""name"": ""@@emailSubject"",
                          ""message"": ""@@Message"",
                          ""participantSetsInfo"": [{
                              ""memberInfos"": [
                                {
                                    ""email"": ""@@ContactPerson1Email""
                                }
                              ],
                              ""order"": 1,
                              ""role"": ""SIGNER""
                           },
                           {
                              ""memberInfos"": [
                                {
                                 ""email"": ""@@ContactPerson2Email""
                                }
                              ],
                              ""order"": 1,
                              ""role"": ""SIGNER""
                            }
                          ],
                           
                          ""signatureType"": ""ESIGN"",
                          ""externalId"": {
                            ""id"": ""NA2Account_{{$timestamp}}""
                          },
                          ""state"": ""IN_PROCESS""
                        }";

                reqbody = reqbody.Replace("@@EngagementLetterName", EngagementLetterName)
                                     .Replace("@@ContactPerson1Email", ContactPerson1Email)
                                     .Replace("@@ContactPerson2Email", ContactPerson2Email)
                                     .Replace("@@PDFUrl", PDFUrl)
                                     .Replace("@@Attachment0", Attachment0)
                                     .Replace("@@TaxReturnConsentForm", TaxReturnConsentForm)
                                     .Replace("@@Message", message)
                                     .Replace("@@emailSubject", emailSubject);

                var resultsjson = JsonConvert.DeserializeObject<AdobeESignExpireyModel>(reqbody);
                for (int i = 0; i < resultsjson.fileInfos.Count(); i++)
                {
                    if (resultsjson.fileInfos[i].urlFileInfo.url == "" || resultsjson.fileInfos[i].urlFileInfo.url == "0")
                    {
                        int indexToRemove = i--;
                        resultsjson.fileInfos = resultsjson.fileInfos.Where((source, index) => index != indexToRemove).ToList();
                    }
                }
                for (int i = 0; i < resultsjson.participantSetsInfo.Count(); i++)
                {

                    if (resultsjson.participantSetsInfo[i].memberInfos[0].email == "")
                    {
                        int indexToRemove = i--;
                        resultsjson.participantSetsInfo = resultsjson.participantSetsInfo.Where((source, index) => index != indexToRemove).ToList();
                    }
                }

                var newresults = JsonConvert.SerializeObject(resultsjson);
                Oauthreq1.AddJsonBody(newresults);
                IRestResponse response = await Oauthreq.ExecuteAsync(Oauthreq1);
                if (response.StatusCode == HttpStatusCode.Created)
                {
                    var responseContent = JsonConvert.DeserializeObject<AdobeESignResponse>(response.Content);

                    signingInfo.AgreementId = responseContent.id;
                    signingInfo.ManualSigning = false;
                    signingInfosList.Add(signingInfo);
                }
                else
                {
                    responseModel.ErrorMessage = response.Content;
                    responseModel.Status = false;
                    responseModel.StatusCode = response.StatusCode;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return signingInfosList;
        }


        public async Task<List<SigningInfo>> SendForManualSigning(EngagementLetter engagementLetter, SigningInfo signingInfo, string letterPdf, string attachment, string letterPdfAssetId, string formPdfAssetId, string accessToken)
        {
            ResponseModel responseModel = new ResponseModel();
            List<SigningInfo> signingInfosList = new List<SigningInfo>();
            try
            {
                CombinedPDFRequest combinePDFRequest = new CombinedPDFRequest();
                combinePDFRequest.assets = new List<Assete>();
                Assete asset = new Assete();
                asset.assetID = letterPdfAssetId.Replace("\"", string.Empty).Replace('"', ' ').ToString();

                combinePDFRequest.assets.Add(asset);
                if (!string.IsNullOrEmpty(formPdfAssetId))
                {
                    asset = new Assete();
                    asset.assetID = formPdfAssetId.Replace("\"", string.Empty).Replace('"', ' ').ToString();
                    combinePDFRequest.assets.Add(asset);
                }
                if (!string.IsNullOrEmpty(attachment))
                {
                    AdobeResponse adobeResponse = new AdobeResponse();
                    adobeResponse.AttachemntPDFcontent = attachment;
                    var pdfAssetRes = await _adobeService.UploadPdfFromUrl(adobeResponse, accessToken);
                    if (pdfAssetRes.StatusCode == HttpStatusCode.OK)
                    {
                        var attachmentAssetId = JsonConvert.SerializeObject(pdfAssetRes.Data);
                        asset = new Assete();
                        attachmentAssetId = attachmentAssetId.Replace("\"", string.Empty).Replace('"', ' ').ToString();
                        asset.assetID = attachmentAssetId;
                        combinePDFRequest.assets.Add(asset);
                    }
                }

                UploadPdfFile uploadPdf = new UploadPdfFile();
                uploadPdf.PdfUrl = letterPdf;
                uploadPdf.FileName = engagementLetter?.EngagementLetterId.ToString() + "-" + engagementLetter?.EngagementLetterName ?? string.Empty;

                if (combinePDFRequest.assets.Count > 1)
                {
                    ResponseModel resCombinepdf = new ResponseModel();
                    resCombinepdf = await _adobeService.CombinePDF(combinePDFRequest, accessToken);
                    if (resCombinepdf != null && resCombinepdf.Data != null && resCombinepdf.StatusCode == HttpStatusCode.OK)
                    {
                        string requiredPath = string.Empty;
                        PdfFileResponse htmlFileResponse = new PdfFileResponse();
                        ResponseModel responseBlob = new ResponseModel();
                        uploadPdf.PdfUrl = resCombinepdf?.Data.ToString() ?? string.Empty;
                    }
                }

                if (!string.IsNullOrEmpty(uploadPdf.PdfUrl))
                {
                    var respPDF = await _adobeService.UploadPdfFileFromBlob(uploadPdf);
                    if (respPDF != null && respPDF.StatusCode == HttpStatusCode.OK)
                    {
                        signingInfo.PdfUrl = respPDF.Data?.ToString() ?? string.Empty;
                        signingInfo.ManualSigning = true;
                        signingInfosList.Add(signingInfo);
                    }
                }
                return signingInfosList;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
