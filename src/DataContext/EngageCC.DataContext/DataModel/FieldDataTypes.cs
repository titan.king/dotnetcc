﻿using System.ComponentModel.DataAnnotations;

namespace EngageCC.DataContext.DataModel
{
    public class FieldDataTypes
    {
        [Key]
        public int FieldDataTypeId { get; set; }
        public string? FieldDataType { get; set; } = string.Empty;
    }
}
