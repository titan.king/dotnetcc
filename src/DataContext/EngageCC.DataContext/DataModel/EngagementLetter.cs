﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EngageCC.DataContext.DataModel
{
    public class EngagementLetter
    {
        [Key]
        public int EngagementLetterId { get; set; }
        public int BatchId { get; set; }
        public string? EngagementLetterName { get; set; } = string.Empty;
        public int OfficeId { get; set; }
        [MaxLength(255)]
        public string? OfficeName { get; set; } = string.Empty;
        public int? YearId { get; set; }
        [MaxLength(50)]
        public string? TaxYear { get; set; }
        public int? TemplateId { get; set; }
        [MaxLength(255)]
        public string? TemplateName { get; set; } = string.Empty;
        public int? TemplateVersion { get; set; }
        public int? ClientId { get; set; }
        [MaxLength(255)]
        public string? ClientName { get; set; } = string.Empty;
        public int? PartnerId { get; set; }
        [MaxLength(255)]
        public string? PartnerName { get; set; } = string.Empty;
        public int? AdminId { get; set; }
        [MaxLength(255)]
        public string? AdminName { get; set; } = string.Empty;
        public int DocumentStatusId { get; set; }

        [ForeignKey("DocumentStatusId")]
        public virtual DocumentStatus DocumentStatus { get; set; } 
        public int EngageTypeId { get; set; }

        [ForeignKey("EngageTypeId")]
        public virtual EngageType EngageType { get; set; } 
        public int DepartmentId { get; set; }
        [ForeignKey("DepartmentId")]
        public virtual Department Departments { get; set; } 
        public DateTime CreatedOn { get; set; }
        [MaxLength(40)]
        public string? CreatedBy { get; set; } = string.Empty;
        public DateTime? ModifiedOn { get; set; }
        public bool IsDeleted { get; set; } = false;
        public DateTime? DeletedOn { get; set; }
        [MaxLength(40)]
        public string? DeletedBy { get; set; } = string.Empty;
        [MaxLength(40)]
        public string? ModifiedBy { get; set; } = string.Empty;
        public int BulkLettersId { get; set; }
        public bool IsProcess { get; set; } = false;
        public bool IsEnqueue { get; set; } = false;
        public bool Is7216Available { get; set; } = false;
        public int? ClientSignatureCount { get; set; }
        public bool IsNewClient { get; set; } = false;
        [MaxLength(255)]
        public string? SpouseFirstName { get; set; }
        [MaxLength(255)]
        public string? SpouseEmailId { get; set; }
        [MaxLength(255)]
        public string? SignatoryEmailId { get; set; }
        [MaxLength(255)]
        public string? SignatoryFirstName { get; set; }
        public bool? IsEsigning { get; set; }
        public string? OtherEntityDetails { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? PrimarySignerLastName { get; set; }
        [MaxLength(255)]
        public string? AdminEmail { get; set; }
        [MaxLength(255)]
        public string? PartnerEmail { get; set; }
        [MaxLength(255)]
        public string? SpouseLastName { get; set; }
        [MaxLength(255)]
        public string? SignatoryLastName { get; set; }
        [MaxLength(255)]
        public string? SignatoryTitle { get; set; }
        public bool? IsCanabisAvailable { get; set; }
        public string? ReturnTypeCode { get; set; }
    }
}
