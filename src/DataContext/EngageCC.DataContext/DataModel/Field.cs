﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EngageCC.DataContext.DataModel
{
    public class Field
    {
        [Key]
        public int FieldId { get; set; }
        [MaxLength(50)]
        public string FieldName { get; set; } = string.Empty;
        [MaxLength(255)]
        public string DisplayName { get; set; } = string.Empty;
        public int? FieldDataTypeId { get; set; }
        [ForeignKey("FieldDataTypeId")]
        public virtual FieldDataTypes FieldDataTypes { get; set; }
        [MaxLength(50)]
        public string? HintText { get; set; } = string.Empty;
        public string ChangeNotes { get; set; } = string.Empty;
        public bool IsDeleted { get; set; } = false;
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool DirectInput { get; set; } = false;
        public bool MDDLookup { get; set; } = false;
        public int? ColumnId { get; set; }
        [MaxLength(200)]
        public string? MDDColumnNames { get; set; } = string.Empty;
        public string? MDDColumnData { get; set; } = string.Empty;

        [MaxLength(40)]
        public string CreatedBy { get; set; } = string.Empty;
        [MaxLength(40)]
        public string? ModifiedBy { get; set; } = string.Empty;
        public DateTime? DeletedOn { get; set; }
        [MaxLength(40)]
        public string? DeletedBy { get; set; } = string.Empty;

        public int StatusId { get; set; }
    }
}
