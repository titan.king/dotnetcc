﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EngageCC.DataContext.DataModel
{
    public class RolePermissionMapping
    {
        [Key]
        public int RolePermissionId { get; set; }
        public int RoleId { get; set; }
        [ForeignKey("RoleId")]
        public virtual Roles Roles { get; set; } 
        public int ScreenId { get; set; }
        [ForeignKey("ScreenId")]
        public virtual Screens Screens { get; set; } 

    }
}
