﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.DataContext.DataModel
{
    public class ParentChildMapping
    {
        [Key]
        public int Id { get; set; }
        public string? ParentClientId { get; set; }
        public string? ClientName { get; set; }
        public string? child_client_name { get; set; }
        public string? Office { get; set; }

    }
}
