﻿using System.ComponentModel.DataAnnotations;

namespace EngageCC.DataContext.DataModel
{
    public class ClientFields
    {
        [Key]
        public int ClientFieldsId { get; set; }
        [MaxLength(255)]
        public string? ClientFirstName { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? Spouse { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? ClientLastName { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? Signing { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? Officer { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? Lastname_Spouse { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? TrustEsatate_Name { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? TrusteeFiduciary_Name { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? TrusteeFiduciary_Last_Name { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? TrusteeFiduciary_Title { get; set; } = string.Empty;
        [MaxLength(50)]
        public string? Year { get; set; }
        [MaxLength(255)]
        public string? TrustName { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? Trust { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? HusbandFullName { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? WifeFullName { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? EstateName { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? Trust_Name { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? Street_Address { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? City_Address { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? City { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? State { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? Business_Name { get; set; } = string.Empty;
        public int? ClientId { get; set; }
        [MaxLength(255)]
        public string? NameofClients { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? AddressofClients { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? NameofClient { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? ClientName { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? Address1 { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? Address2 { get; set; } = string.Empty;
        public int? OfficeLocation { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool? IsDeleted { get; set; } = false;
        public DateTime? DeletedOn { get; set; }
    }
}
