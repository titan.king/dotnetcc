﻿using System.ComponentModel.DataAnnotations;

namespace EngageCC.DataContext.DataModel
{
    public class ColumnNames
    {
        [Key]
        public int ColumnNamesId { get; set; }
        [MaxLength(255)]
        public string? ColumnName { get; set; }
    }
}
