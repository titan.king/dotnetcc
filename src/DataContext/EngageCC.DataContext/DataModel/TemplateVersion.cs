﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EngageCC.DataContext.DataModel
{
    public class TemplateVersion
    {
        [Key]
        public int TemplateVersionId { get; set; }
        public int TemplateId { get; set; }
        [ForeignKey("TemplateId")]
        public virtual Template Template { get; set; }
        public string? TemplateLogic { get; set; } = string.Empty;
        public int? VersionNumber { get; set; }
    }
}
