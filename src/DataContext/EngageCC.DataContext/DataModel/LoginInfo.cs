﻿using System.ComponentModel.DataAnnotations;

namespace EngageCC.DataContext.DataModel
{
    public class LoginInfo
    {
        [Key]
        public int LoginInfoId { get; set; }
        [MaxLength(255)]
        public string? UserName { get; set; } = string.Empty;
        [MaxLength(50)]
        public string? UserObjectId { get; set; } = string.Empty;
    }
}
