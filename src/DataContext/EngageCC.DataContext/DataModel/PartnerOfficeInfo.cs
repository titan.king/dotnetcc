﻿using System.ComponentModel.DataAnnotations;

namespace EngageCC.DataContext.DataModel
{
    public class PartnerOfficeInfo
    {
        [Key]
        public int PartnerOfficeInfoId { get; set; }
        public int? PartnerId { get; set; }
        [MaxLength(255)]
        public string? PartnerName { get; set; } = string.Empty;
        public int? OfficeId { get; set; }
        [MaxLength(255)]
        public string? OfficeName { get; set; } = string.Empty;
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool? IsDeleted { get; set; } = false;
        public DateTime? DeletedOn { get; set; }
    }
}
