﻿using System.ComponentModel.DataAnnotations;

namespace EngageCC.DataContext.DataModel
{
    public class UserDetails
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(50)]
        public string LoginId { get; set; } = string.Empty;
        [MaxLength(50)]
        public string LoginUserName { get; set; } = string.Empty;
        [MaxLength(500)]
        public string GroupId { get; set; } = string.Empty;
        [MaxLength(255)]
        public string GroupName { get; set; } = string.Empty;
        public DateTime LoggedOnTime { get; set; }
        public DateTime? LoggedOutTime { get; set; }
        public bool IsLoggedIn { get; set; }
    }
}
