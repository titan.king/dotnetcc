﻿using System.ComponentModel.DataAnnotations;

namespace EngageCC.DataContext.DataModel
{
    public class ParentChildInfo
    {
        [Key]
        public int ParentChildInfoId { get; set; }
        public int? ParentClientId { get; set; }
        [MaxLength(255)]
        public string? Child1 { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? Child2 { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? Child3 { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? Child4 { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? Child5 { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? Child6 { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? Child7 { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? Child8 { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? Child9 { get; set; } = string.Empty;
        [MaxLength(255)]
        public string? Child10 { get; set; } = string.Empty;
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public bool? IsDeleted { get; set; } = false;
        public DateTime? DeletedOn { get; set; }
    }
}
