﻿using System.ComponentModel.DataAnnotations;

namespace EngageCC.DataContext.DataModel
{
    public class Status
    {
        [Key]
        public int StatusId { get; set; }
        public string? StatusName { get; set; } = string.Empty;
    }
}
