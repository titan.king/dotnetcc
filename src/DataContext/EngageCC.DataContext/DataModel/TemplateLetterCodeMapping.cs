﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.DataContext.DataModel
{
    public class TemplateLetterCodeMapping
    {
        [Key]
        public int Id { get; set; }
        public int TemplateId { get; set; }
        public string MajorReturnType { get; set; }
    }
}
