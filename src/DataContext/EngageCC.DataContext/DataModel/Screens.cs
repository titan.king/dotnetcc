﻿using System.ComponentModel.DataAnnotations;

namespace EngageCC.DataContext.DataModel
{
    public class Screens
    {
        [Key]
        public int ScreenId { get; set; }
        [MaxLength(255)]
        public string? ScreenName { get; set; } = string.Empty;
    }
}
