﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EngageCC.DataContext.Migrations
{
    /// <inheritdoc />
    public partial class Template : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApprovalComments",
                table: "Template");

            migrationBuilder.DropColumn(
                name: "RequiresApproval",
                table: "Template");

            migrationBuilder.DropColumn(
                name: "FieldStatus",
                table: "Field");

            migrationBuilder.DropColumn(
                name: "EditFreeBlock",
                table: "Block");

            migrationBuilder.DropColumn(
                name: "QCRequired",
                table: "Block");

            migrationBuilder.AddColumn<int>(
                name: "AttachmentCount",
                table: "Template",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StatusId",
                table: "Template",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "StatusId",
                table: "Field",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AttachmentCount",
                table: "Template");

            migrationBuilder.DropColumn(
                name: "StatusId",
                table: "Template");

            migrationBuilder.DropColumn(
                name: "StatusId",
                table: "Field");

            migrationBuilder.AddColumn<string>(
                name: "ApprovalComments",
                table: "Template",
                type: "nvarchar(500)",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "RequiresApproval",
                table: "Template",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "FieldStatus",
                table: "Field",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "EditFreeBlock",
                table: "Block",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "QCRequired",
                table: "Block",
                type: "bit",
                nullable: true);
        }
    }
}
