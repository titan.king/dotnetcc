﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EngageCC.DataContext.Migrations
{
    /// <inheritdoc />
    public partial class UpdateBulkLetter : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RowId",
                table: "BulkLetters");

            migrationBuilder.RenameColumn(
                name: "VersionNumber",
                table: "BulkLetters",
                newName: "BatchId");

            migrationBuilder.RenameColumn(
                name: "Partners",
                table: "BulkLetters",
                newName: "PartnerName");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PartnerName",
                table: "BulkLetters",
                newName: "Partners");

            migrationBuilder.RenameColumn(
                name: "BatchId",
                table: "BulkLetters",
                newName: "VersionNumber");

            migrationBuilder.AddColumn<int>(
                name: "RowId",
                table: "BulkLetters",
                type: "int",
                nullable: true);
        }
    }
}
