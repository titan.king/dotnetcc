﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EngageCC.DataContext.Migrations
{
    /// <inheritdoc />
    public partial class updatedEngtables : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AdminEmail",
                table: "SigningInfo",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AdminName",
                table: "SigningInfo",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PartnerEmail",
                table: "SigningInfo",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PartnerName",
                table: "SigningInfo",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AdminEmail",
                table: "EngagementLetter",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PartnerEmail",
                table: "EngagementLetter",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SignatoryLastName",
                table: "EngagementLetter",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SignatoryTitle",
                table: "EngagementLetter",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SpouseLastName",
                table: "EngagementLetter",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SignatoryLastName",
                table: "BulkLetters",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SignatoryTitle",
                table: "BulkLetters",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SpouseLastName",
                table: "BulkLetters",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdminEmail",
                table: "SigningInfo");

            migrationBuilder.DropColumn(
                name: "AdminName",
                table: "SigningInfo");

            migrationBuilder.DropColumn(
                name: "PartnerEmail",
                table: "SigningInfo");

            migrationBuilder.DropColumn(
                name: "PartnerName",
                table: "SigningInfo");

            migrationBuilder.DropColumn(
                name: "AdminEmail",
                table: "EngagementLetter");

            migrationBuilder.DropColumn(
                name: "PartnerEmail",
                table: "EngagementLetter");

            migrationBuilder.DropColumn(
                name: "SignatoryLastName",
                table: "EngagementLetter");

            migrationBuilder.DropColumn(
                name: "SignatoryTitle",
                table: "EngagementLetter");

            migrationBuilder.DropColumn(
                name: "SpouseLastName",
                table: "EngagementLetter");

            migrationBuilder.DropColumn(
                name: "SignatoryLastName",
                table: "BulkLetters");

            migrationBuilder.DropColumn(
                name: "SignatoryTitle",
                table: "BulkLetters");

            migrationBuilder.DropColumn(
                name: "SpouseLastName",
                table: "BulkLetters");
        }
    }
}
