﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EngageCC.DataContext.Migrations
{
    /// <inheritdoc />
    public partial class UpdateFields : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "NameOfClients",
                table: "ClientFields",
                newName: "NameofClients");

            migrationBuilder.RenameColumn(
                name: "NameOfClient",
                table: "ClientFields",
                newName: "NameofClient");

            migrationBuilder.RenameColumn(
                name: "AddressOfClients",
                table: "ClientFields",
                newName: "AddressofClients");

            migrationBuilder.AlterColumn<string>(
                name: "Year",
                table: "ClientFields",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "NameofClients",
                table: "ClientFields",
                newName: "NameOfClients");

            migrationBuilder.RenameColumn(
                name: "NameofClient",
                table: "ClientFields",
                newName: "NameOfClient");

            migrationBuilder.RenameColumn(
                name: "AddressofClients",
                table: "ClientFields",
                newName: "AddressOfClients");

            migrationBuilder.AlterColumn<int>(
                name: "Year",
                table: "ClientFields",
                type: "int",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);
        }
    }
}
