﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EngageCC.DataContext.Migrations
{
    /// <inheritdoc />
    public partial class Update : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Field_Status_StatusId",
                table: "Field");

            migrationBuilder.DropIndex(
                name: "IX_Field_StatusId",
                table: "Field");

            migrationBuilder.DropColumn(
                name: "StatusId",
                table: "Field");

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "Template",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedOn",
                table: "Template",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy",
                table: "Template",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "SigningInfo",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedOn",
                table: "SigningInfo",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy",
                table: "SigningInfo",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "Field",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "Field",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedOn",
                table: "Field",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FieldStatus",
                table: "Field",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy",
                table: "Field",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "EngageType",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "EngageType",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedOn",
                table: "EngageType",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy",
                table: "EngageType",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy",
                table: "EngagementLetter",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "DocumentStatus",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "DocumentStatus",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedOn",
                table: "DocumentStatus",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy",
                table: "DocumentStatus",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "Department",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "Department",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedOn",
                table: "Department",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy",
                table: "Department",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "Block",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "DeletedBy",
                table: "Block",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DeletedOn",
                table: "Block",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ModifiedBy",
                table: "Block",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Template");

            migrationBuilder.DropColumn(
                name: "DeletedOn",
                table: "Template");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Template");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "SigningInfo");

            migrationBuilder.DropColumn(
                name: "DeletedOn",
                table: "SigningInfo");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "SigningInfo");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Field");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Field");

            migrationBuilder.DropColumn(
                name: "DeletedOn",
                table: "Field");

            migrationBuilder.DropColumn(
                name: "FieldStatus",
                table: "Field");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Field");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "EngageType");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "EngageType");

            migrationBuilder.DropColumn(
                name: "DeletedOn",
                table: "EngageType");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "EngageType");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "EngagementLetter");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "DocumentStatus");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "DocumentStatus");

            migrationBuilder.DropColumn(
                name: "DeletedOn",
                table: "DocumentStatus");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "DocumentStatus");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Department");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Department");

            migrationBuilder.DropColumn(
                name: "DeletedOn",
                table: "Department");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Department");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Block");

            migrationBuilder.DropColumn(
                name: "DeletedBy",
                table: "Block");

            migrationBuilder.DropColumn(
                name: "DeletedOn",
                table: "Block");

            migrationBuilder.DropColumn(
                name: "ModifiedBy",
                table: "Block");

            migrationBuilder.AddColumn<int>(
                name: "StatusId",
                table: "Field",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Field_StatusId",
                table: "Field",
                column: "StatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_Field_Status_StatusId",
                table: "Field",
                column: "StatusId",
                principalTable: "Status",
                principalColumn: "StatusId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
