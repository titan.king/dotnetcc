﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EngageCC.DataContext.Migrations
{
    /// <inheritdoc />
    public partial class TemplateStatus : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Template_DocumentStatus_DocumentStatusId",
                table: "Template");

            migrationBuilder.DropIndex(
                name: "IX_Template_DocumentStatusId",
                table: "Template");

            migrationBuilder.DropColumn(
                name: "DocumentStatusId",
                table: "Template");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DocumentStatusId",
                table: "Template",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Template_DocumentStatusId",
                table: "Template",
                column: "DocumentStatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_Template_DocumentStatus_DocumentStatusId",
                table: "Template",
                column: "DocumentStatusId",
                principalTable: "DocumentStatus",
                principalColumn: "DocumentStatusId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
