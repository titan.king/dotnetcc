﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EngageCC.DataContext.Migrations
{
    /// <inheritdoc />
    public partial class updateddbctx : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "HistoryLog",
                columns: table => new
                {
                    HistoryLogId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EngagementLetterId = table.Column<int>(type: "int", nullable: true),
                    EngagementLetterName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BatchId = table.Column<int>(type: "int", nullable: true),
                    Status = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EditedBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Version = table.Column<int>(type: "int", nullable: true),
                    LastModified = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Downloaded = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Delegated = table.Column<bool>(type: "bit", nullable: true),
                    ClientEmailId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ReasonforDecline = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DeclineTimestamp = table.Column<DateTime>(type: "datetime2", nullable: true),
                    PDFUrl = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistoryLog", x => x.HistoryLogId);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HistoryLog");
        }
    }
}
