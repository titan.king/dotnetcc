﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EngageCC.DataContext.Migrations
{
    /// <inheritdoc />
    public partial class Attachments : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsEsigning",
                table: "BulkLetters",
                type: "bit",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BlockName",
                table: "Block",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50);

            migrationBuilder.CreateTable(
                name: "BulkLetterAttachments",
                columns: table => new
                {
                    BulkLetterAttachmentId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BatchId = table.Column<int>(type: "int", nullable: false),
                    TemplateId = table.Column<int>(type: "int", nullable: false),
                    AttachmentsJSON = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AttachmentsURL = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: true),
                    VersionNumber = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BulkLetterAttachments", x => x.BulkLetterAttachmentId);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BulkLetterAttachments");

            migrationBuilder.DropColumn(
                name: "IsEsigning",
                table: "BulkLetters");

            migrationBuilder.AlterColumn<string>(
                name: "BlockName",
                table: "Block",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255);
        }
    }
}
