﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EngageCC.DataContext.Migrations
{
    /// <inheritdoc />
    public partial class columnupdatesforBot : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ExpiryDate7216",
                table: "SigningInfo",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OtherEntityDetails",
                table: "EngagementLetter",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PrimarySignerLastName",
                table: "EngagementLetter",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExpiryDate7216",
                table: "SigningInfo");

            migrationBuilder.DropColumn(
                name: "OtherEntityDetails",
                table: "EngagementLetter");

            migrationBuilder.DropColumn(
                name: "PrimarySignerLastName",
                table: "EngagementLetter");
        }
    }
}
