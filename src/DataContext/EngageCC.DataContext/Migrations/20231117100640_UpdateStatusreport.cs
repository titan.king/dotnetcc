﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EngageCC.DataContext.Migrations
{
    /// <inheritdoc />
    public partial class UpdateStatusreport : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsCanabisAvailable",
                table: "LettersStatusReports",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReturnTypeCode",
                table: "LettersStatusReports",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsCanabisAvailable",
                table: "EngagementLetter",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReturnTypeCode",
                table: "EngagementLetter",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsCanabisAvailable",
                table: "BulkLetters",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReturnTypeCode",
                table: "BulkLetters",
                type: "nvarchar(max)",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsCanabisAvailable",
                table: "LettersStatusReports");

            migrationBuilder.DropColumn(
                name: "ReturnTypeCode",
                table: "LettersStatusReports");

            migrationBuilder.DropColumn(
                name: "IsCanabisAvailable",
                table: "EngagementLetter");

            migrationBuilder.DropColumn(
                name: "ReturnTypeCode",
                table: "EngagementLetter");

            migrationBuilder.DropColumn(
                name: "IsCanabisAvailable",
                table: "BulkLetters");

            migrationBuilder.DropColumn(
                name: "ReturnTypeCode",
                table: "BulkLetters");
        }
    }
}
