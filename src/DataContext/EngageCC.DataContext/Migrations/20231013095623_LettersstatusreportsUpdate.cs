﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EngageCC.DataContext.Migrations
{
    /// <inheritdoc />
    public partial class LettersstatusreportsUpdate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "LettersStatusReports",
                columns: table => new
                {
                    LettersStatusReportId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EngagementLetterId = table.Column<int>(type: "int", nullable: false),
                    EngagementLetterName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClientId = table.Column<int>(type: "int", nullable: true),
                    ClientName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    PartnerId = table.Column<int>(type: "int", nullable: true),
                    PartnerName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    OfficeId = table.Column<int>(type: "int", nullable: false),
                    OfficeName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    YearId = table.Column<int>(type: "int", nullable: true),
                    TaxYear = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Date = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Department = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Type = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    SignersCount = table.Column<int>(type: "int", nullable: true),
                    OtherEntityDetails = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    IsEsigning = table.Column<bool>(type: "bit", nullable: true),
                    IsManualSigning = table.Column<bool>(type: "bit", nullable: true),
                    Is7216Available = table.Column<bool>(type: "bit", nullable: false),
                    IsNewClient = table.Column<bool>(type: "bit", nullable: false),
                    ExpiryDate7216 = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    EngagementLetterStatus = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    EngagementLetterPDF = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsLetterUploaded = table.Column<bool>(type: "bit", nullable: true),
                    IsLetterStatusUpdated = table.Column<bool>(type: "bit", nullable: true),
                    Is7216StatusUpdated = table.Column<bool>(type: "bit", nullable: true),
                    Is7216ExpiryStatusUpdated = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LettersStatusReports", x => x.LettersStatusReportId);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LettersStatusReports");
        }
    }
}
