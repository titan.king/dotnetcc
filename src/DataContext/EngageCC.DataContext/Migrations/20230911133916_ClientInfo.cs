﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EngageCC.DataContext.Migrations
{
    /// <inheritdoc />
    public partial class ClientInfo : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ClientFields",
                columns: table => new
                {
                    ClientFieldsId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ClientFirstName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Spouse = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    ClientLastName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Signing = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Officer = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Lastname_Spouse = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    TrustEsatate_Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    TrusteeFiduciary_Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    TrusteeFiduciary_Last_Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    TrusteeFiduciary_Title = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Year = table.Column<int>(type: "int", nullable: true),
                    TrustName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Trust = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    HusbandFullName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    WifeFullName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    EstateName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Trust_Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Street_Address = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    City_Address = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    City = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    State = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Business_Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    ClientId = table.Column<int>(type: "int", nullable: true),
                    NameOfClients = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    AddressOfClients = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    NameOfClient = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    ClientName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Address1 = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Address2 = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    OfficeLocation = table.Column<int>(type: "int", nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: true),
                    DeletedOn = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientFields", x => x.ClientFieldsId);
                });

            migrationBuilder.CreateTable(
                name: "OfficeInfo",
                columns: table => new
                {
                    OfficeInfoId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OfficeId = table.Column<int>(type: "int", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Office = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Address = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    City = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    State = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    ZipCode = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    StateSignatureNameAddress = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    StateSignatureCity = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    StateSignatureState = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    StateSignatureZipCode = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: true),
                    DeletedOn = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OfficeInfo", x => x.OfficeInfoId);
                });

            migrationBuilder.CreateTable(
                name: "ParentChildInfo",
                columns: table => new
                {
                    ParentChildInfoId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ParentClientId = table.Column<int>(type: "int", nullable: true),
                    Child1 = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Child2 = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Child3 = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Child4 = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Child5 = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Child6 = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Child7 = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Child8 = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Child9 = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Child10 = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", maxLength: 255, nullable: false),
                    ModifiedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: true),
                    DeletedOn = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParentChildInfo", x => x.ParentChildInfoId);
                });

            migrationBuilder.CreateTable(
                name: "Partner",
                columns: table => new
                {
                    PartnersId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PartnerId = table.Column<int>(type: "int", nullable: true),
                    PartnerName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: true),
                    DeletedOn = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Partner", x => x.PartnersId);
                });

            migrationBuilder.CreateTable(
                name: "PartnerClientMapping",
                columns: table => new
                {
                    PartnerClientMappingId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PartnerId = table.Column<int>(type: "int", nullable: true),
                    PartnerName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    ParentClientId = table.Column<int>(type: "int", nullable: true),
                    ParentClientName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    ChildClient = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    AddressDetail = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    City = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    State = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    ZipCode = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    ContactName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    ContactEmail = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: true),
                    DeletedOn = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartnerClientMapping", x => x.PartnerClientMappingId);
                });

            migrationBuilder.CreateTable(
                name: "PartnerOfficeInfo",
                columns: table => new
                {
                    PartnerOfficeInfoId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PartnerId = table.Column<int>(type: "int", nullable: true),
                    PartnerName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    OfficeId = table.Column<int>(type: "int", nullable: true),
                    OfficeName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "datetime2", nullable: false),
                    ModifiedOn = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: true),
                    DeletedOn = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PartnerOfficeInfo", x => x.PartnerOfficeInfoId);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClientFields");

            migrationBuilder.DropTable(
                name: "OfficeInfo");

            migrationBuilder.DropTable(
                name: "ParentChildInfo");

            migrationBuilder.DropTable(
                name: "Partner");

            migrationBuilder.DropTable(
                name: "PartnerClientMapping");

            migrationBuilder.DropTable(
                name: "PartnerOfficeInfo");
        }
    }
}
