﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EngageCC.DataContext.Migrations
{
    /// <inheritdoc />
    public partial class Attachment : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AttachmentURL",
                table: "Template",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ClientSignatureCount",
                table: "Template",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Is7216Available",
                table: "Template",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AttachmentURL",
                table: "Template");

            migrationBuilder.DropColumn(
                name: "ClientSignatureCount",
                table: "Template");

            migrationBuilder.DropColumn(
                name: "Is7216Available",
                table: "Template");
        }
    }
}
