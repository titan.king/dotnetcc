﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EngageCC.DataContext.Migrations
{
    /// <inheritdoc />
    public partial class BulkLetterTableUpdate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SigningPerson",
                table: "BulkLetters",
                newName: "SpouseFirstName");

            migrationBuilder.RenameColumn(
                name: "EmailAddress",
                table: "BulkLetters",
                newName: "SpouseEmailId");

            migrationBuilder.AddColumn<string>(
                name: "SignatoryEmailId",
                table: "BulkLetters",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SignatoryFirstName",
                table: "BulkLetters",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SignatoryEmailId",
                table: "BulkLetters");

            migrationBuilder.DropColumn(
                name: "SignatoryFirstName",
                table: "BulkLetters");

            migrationBuilder.RenameColumn(
                name: "SpouseFirstName",
                table: "BulkLetters",
                newName: "SigningPerson");

            migrationBuilder.RenameColumn(
                name: "SpouseEmailId",
                table: "BulkLetters",
                newName: "EmailAddress");
        }
    }
}
