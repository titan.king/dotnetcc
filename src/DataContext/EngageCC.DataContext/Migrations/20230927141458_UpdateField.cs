﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EngageCC.DataContext.Migrations
{
    /// <inheritdoc />
    public partial class UpdateField : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "BulkLettersId",
                table: "LetterFieldValues",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "AttachmentsURL",
                table: "BulkLetterAttachments",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(150)",
                oldMaxLength: 150,
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_LetterFieldValues_BulkLettersId",
                table: "LetterFieldValues",
                column: "BulkLettersId");

            migrationBuilder.AddForeignKey(
                name: "FK_LetterFieldValues_BulkLetters_BulkLettersId",
                table: "LetterFieldValues",
                column: "BulkLettersId",
                principalTable: "BulkLetters",
                principalColumn: "BulkLettersId",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_LetterFieldValues_BulkLetters_BulkLettersId",
                table: "LetterFieldValues");

            migrationBuilder.DropIndex(
                name: "IX_LetterFieldValues_BulkLettersId",
                table: "LetterFieldValues");

            migrationBuilder.AlterColumn<int>(
                name: "BulkLettersId",
                table: "LetterFieldValues",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<string>(
                name: "AttachmentsURL",
                table: "BulkLetterAttachments",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);
        }
    }
}
