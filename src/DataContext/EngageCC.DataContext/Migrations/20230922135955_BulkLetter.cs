﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EngageCC.DataContext.Migrations
{
    /// <inheritdoc />
    public partial class BulkLetter : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ClientSignatureCount",
                table: "BulkLetters",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Is7216Available",
                table: "BulkLetters",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ClientSignatureCount",
                table: "BulkLetters");

            migrationBuilder.DropColumn(
                name: "Is7216Available",
                table: "BulkLetters");
        }
    }
}
