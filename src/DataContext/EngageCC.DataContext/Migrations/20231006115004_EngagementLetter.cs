﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EngageCC.DataContext.Migrations
{
    /// <inheritdoc />
    public partial class EngagementLetter : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BulkLettersId",
                table: "EngagementLetter",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ClientSignatureCount",
                table: "EngagementLetter",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Is7216Available",
                table: "EngagementLetter",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsEnqueue",
                table: "EngagementLetter",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsNewClient",
                table: "EngagementLetter",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsProcess",
                table: "EngagementLetter",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "SignatoryEmailId",
                table: "EngagementLetter",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SignatoryFirstName",
                table: "EngagementLetter",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SpouseEmailId",
                table: "EngagementLetter",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SpouseFirstName",
                table: "EngagementLetter",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BulkLettersId",
                table: "EngagementLetter");

            migrationBuilder.DropColumn(
                name: "ClientSignatureCount",
                table: "EngagementLetter");

            migrationBuilder.DropColumn(
                name: "Is7216Available",
                table: "EngagementLetter");

            migrationBuilder.DropColumn(
                name: "IsEnqueue",
                table: "EngagementLetter");

            migrationBuilder.DropColumn(
                name: "IsNewClient",
                table: "EngagementLetter");

            migrationBuilder.DropColumn(
                name: "IsProcess",
                table: "EngagementLetter");

            migrationBuilder.DropColumn(
                name: "SignatoryEmailId",
                table: "EngagementLetter");

            migrationBuilder.DropColumn(
                name: "SignatoryFirstName",
                table: "EngagementLetter");

            migrationBuilder.DropColumn(
                name: "SpouseEmailId",
                table: "EngagementLetter");

            migrationBuilder.DropColumn(
                name: "SpouseFirstName",
                table: "EngagementLetter");
        }
    }
}
