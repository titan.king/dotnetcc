﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace EngageCC.DataContext.Migrations
{
    /// <inheritdoc />
    public partial class LetterFieldsUpdate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DisplayName",
                table: "LetterFieldValues",
                type: "nvarchar(max)",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DisplayName",
                table: "LetterFieldValues");
        }
    }
}
