﻿using EngageCC.DataContext.DataModel;
using Microsoft.EntityFrameworkCore;

namespace EngageCC.DataContext
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }
        public DbSet<AzureGroup> AzureGroup { get; set; }
        public DbSet<EngageType> EngageType { get; set; }
        public DbSet<Department> Department { get; set; }
        public DbSet<DocumentStatus> DocumentStatus { get; set; }
        public DbSet<EngagementLetter> EngagementLetter { get; set; }
        public DbSet<Template> Template { get; set; }
        public DbSet<TemplateVersion> TemplateVersion { get; set; }
        public DbSet<MasterTemplate> MasterTemplate { get; set; }
        public DbSet<Roles> Roles { get; set; }
        public DbSet<Screens> Screens { get; set; }
        public DbSet<RolePermissionMapping> RolePermissionMapping { get; set; }
        public DbSet<SigningInfo> SigningInfo { get; set; }
        public DbSet<Status> Status { get; set; }
        public DbSet<FieldDataTypes> FieldDataTypes { get; set; }
        public DbSet<Block> Block { get; set; }
        public DbSet<Field> Field { get; set; }
        public DbSet<LoginInfo> LoginInfo { get; set; }
        public DbSet<TemplateAttachments> TemplateAttachments { get; set; }
        public DbSet<Partners> Partner { get; set; }
        public DbSet<ClientFields> ClientFields { get; set; }
        public DbSet<PartnerClientMapping> PartnerClientMapping { get; set; }
        public DbSet<ParentChildInfo> ParentChildInfo { get; set; }
        public DbSet<PartnerOfficeInfo> PartnerOfficeInfo { get; set; }
        public DbSet<OfficeInfo> OfficeInfo { get; set; }
        public DbSet<BulkLetters> BulkLetters { get; set; }
        public DbSet<ColumnNames> ColumnNames { get; set; }
        public DbSet<UserDetails> UserDetails { get; set; }
        public DbSet<LetterFieldValues> LetterFieldValues { get; set; }
        public DbSet<BulkLetterAttachments> BulkLetterAttachments { get; set; }
        public DbSet<BatchRequest> BatchRequest { get; set; }
        public DbSet<Jurisdiction> Jurisdiction { get; set; }
        public DbSet<LettersStatusReports> LettersStatusReports { get; set; }
        public DbSet<EmailSettings> EmailSettings { get; set; }
        public DbSet<EmailTemplate> EmailTemplate { get; set; }
        public DbSet<HistoryLog> HistoryLog { get; set; }
        public DbSet<ClientData> ClientData { get; set; }
        public DbSet<ParentChildMapping> ParentChildMapping { get; set; }
        public DbSet<BulkLogger> BulkLoggers { get; set; }
        public DbSet<SignerStatus> signerStatuses { get; set; }
        public DbSet<TemplateLetterCodeMapping> TemplateLetterCodeMapping { get; set; }
    }
}
