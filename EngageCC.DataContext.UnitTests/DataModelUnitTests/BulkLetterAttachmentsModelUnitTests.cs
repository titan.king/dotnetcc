﻿using AutoFixture;
using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class BulkLetterAttachmentsModelUnitTests
    {
        [Fact]
        public void BulkLetterAttachments_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new BulkLetterAttachments
            {
                BulkLetterAttachmentId = 1,
                BatchId = 2,
                TemplateId = 3,
                AttachmentsJSON = "SampleAttachmentsJSON",
                AttachmentsURL = "SampleAttachmentsURL",
                VersionNumber = 4
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
