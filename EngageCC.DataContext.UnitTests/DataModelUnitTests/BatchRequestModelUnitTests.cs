﻿using AutoFixture;
using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class BatchRequestModelUnitTests
    {
        [Fact]
        public void BatchRequest_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new BatchRequest
            {
                BatchRequestId = 1,
                BatchId = 2,
                BatchJson = "SampleBatchJson",
                IsProcess = true,
                Status = "Completed",
                TemplateId = 3,
                TemplateName = "SampleTemplate",
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,  
                CreatedBy = "JohnDoe"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void BatchRequest_TemplateNameExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new BatchRequest
            {
                BatchRequestId = 1,
                BatchId = 2,
                BatchJson = "SampleBatchJson",
                IsProcess = true,
                Status = "Completed",
                TemplateId = 3,
                TemplateName = new string('a', 256), 
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("TemplateName"));
        }

    }
}
