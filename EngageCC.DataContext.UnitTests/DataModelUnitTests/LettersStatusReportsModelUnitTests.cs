﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class LettersStatusReportsModelUnitTests
    {
        [Fact]
        public void LettersStatusReports_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new LettersStatusReports
            {
                LettersStatusReportId = 1,
                EngagementLetterId = 2,
                EngagementLetterName = "Valid Name",
                ClientId = "Valid Client Id",
                ClientName = "Valid Client Name",
                PartnerId = 4,
                PartnerName = "Valid Partner Name",
                OfficeId = 5,
                OfficeName = "Valid Office Name",
                YearId = 6,
                TaxYear = "2023",
                Date = "2023-10-26",
                Department = "Valid Department",
                Type = "Valid Type",
                SignersCount = 2,
                OtherEntityDetails = "Other Entity Details",
                IsEsigning = true,
                IsManualSigning = false,
                Is7216Available = false,
                IsNewClient = true,
                ExpiryDate7216 = "2023-12-31",
                EngagementLetterStatus = "Active",
                EngagementLetterPDF = "ValidPDF.pdf",
                IsLetterUploaded = true,
                IsLetterStatusUpdated = false,
                Is7216StatusUpdated = true,
                Is7216ExpiryStatusUpdated = true,
                Bot_Status = "Bot Active",
                BotExtracted7216SignCount = 3,
                PrimarySignerFirstName = "John",
                PrimarySignerLastName = "Doe"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void LettersStatusReport_ExpiryDate7216ExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new LettersStatusReports
            {
                LettersStatusReportId = 1,
                EngagementLetterId = 2,
                EngagementLetterName = "LetterName",
                ClientId = "Client Id",
                SignersCount = 1,
                IsLetterUploaded = true,
                ExpiryDate7216 = new string('a', 256), 
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("ExpiryDate7216"));
        }

    }
}
