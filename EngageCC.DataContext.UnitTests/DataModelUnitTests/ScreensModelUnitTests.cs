﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class ScreensModelUnitTests
    {
        [Fact]
        public void Screens_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new Screens
            {
                ScreenId = 1,
                ScreenName = "ValidScreenName",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void Screens_ScreenNameExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new Screens
            {
                ScreenId = 1,
                ScreenName = new string('a', 256),
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("ScreenName"));
        }
    }
}
