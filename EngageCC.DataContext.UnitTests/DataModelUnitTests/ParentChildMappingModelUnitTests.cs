﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class ParentChildMappingModelUnitTests
    {
        [Fact]
        public void ValidModel_PassesValidation()
        {
            // Arrange
            var model = new ParentChildMapping
            {
                Id = 1,
                ParentClientId = "Parent1",
                ClientName = "Child1",
                child_client_name = "Child2",
                Office = "Office1",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

    }
}
