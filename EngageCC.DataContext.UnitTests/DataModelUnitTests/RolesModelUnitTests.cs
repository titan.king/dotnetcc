﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class RolesModelUnitTests
    {

        [Fact]
        public void Roles_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new Roles
            {
                RoleId = 1,
                RoleName = "ValidRoleName",
                AzureGroupId = 1,
                AzureGroup = new AzureGroup
                {
                    AzureGroupId = 1,
                    GroupId = "ValidGroupId",
                    GroupName = "ValidGroupName",
                }
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void Roles_RoleNameExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new Roles
            {
                RoleId = 1,
                RoleName = new string('a', 256),
                AzureGroupId = 1,
                AzureGroup = new AzureGroup
                {
                    AzureGroupId = 1,
                    GroupId = "ValidGroupId",
                    GroupName = "ValidGroupName",
                }
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("RoleName"));
        }

    }
}
