﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class ClientDataModelUnitTests
    {
        [Fact]
        public void ClientData_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new ClientData
            {
                Id = 1,
                row_num = "123",
                ClientId = "Client123",
                OfficeId = "Office456",
                PartnerId = "Partner789",
                PartnerName = "Partner Name",
                OfficeName = "Office Name",
                OfficeAddress = "123 Main St",
                OfficeCity = "City",
                OfficeState = "State",
                OfficeZipCode = "12345",
                OfficePhoneNumber = "555-555-5555",
                Date = "2023-10-26",
                ClientName = "Client Name",
                Address = "456 Elm St",
                City = "City",
                State = "State",
                Zip = "54321",
                SignatoryEmail = "signatory@example.com",
                SpouseEmail = "spouse@example.com",
                SignatoryFirstName = "Signatory First Name",
                SignatoryLastName = "Signatory Last Name",
                SignatoryTitle = "Signatory Title",
                SpouseFirstName = "Spouse First Name",
                SpouseLastName = "Spouse Last Name",
                Jurisdiction = "Jurisdiction",
                FiscalYear = "2023",
                ELTemplate = "EL Template",
                Expiration7216Form = "Expiration Form",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
