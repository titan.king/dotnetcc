﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class ParentChildInfoModelUnitTests
    {
        [Fact]
        public void ParentChildInfo_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new ParentChildInfo
            {
                ParentChildInfoId = 1,
                ParentClientId = 2,
                Child1 = "Child1",
                Child2 = "Child2",
                Child3 = "Child3",
                Child4 = "Child4",
                Child5 = "Child5",
                Child6 = "Child6",
                Child7 = "Child7",
                Child8 = "Child8",
                Child9 = "Child9",
                Child10 = "Child10",
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                DeletedOn = DateTime.Now,
                IsDeleted = false,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void ParentChildInfo_Child1ExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new ParentChildInfo
            {
                ParentChildInfoId = 1,
                ParentClientId = 2,
                Child1 = new string('a', 256),
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("Child1"));
        }

    }
}
