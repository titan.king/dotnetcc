﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class EngagementLetterModelUnitTests
    {
        [Fact]
        public void EngagementLetter_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new EngagementLetter
            {
                EngagementLetterId = 1,
                BatchId = 2,
                EngagementLetterName = "LetterName",
                OfficeId = 3,
                OfficeName = "OfficeName",
                YearId = 4,
                TaxYear = "2023",
                TemplateId = 5,
                TemplateName = "TemplateName",
                TemplateVersion = 6,
                ClientId = 7,
                ClientName = "ClientName",
                PartnerId = 8,
                PartnerName = "PartnerName",
                AdminId = 9,
                AdminName = "AdminName",
                DocumentStatusId = 10,
                DocumentStatus = new DocumentStatus
                {
                    DocumentStatusId=10
                },
                EngageTypeId = 11,
                EngageType = new EngageType
                {
                    EngageTypeId=11
                },
                DepartmentId = 12,
                Departments = new Department
                {
                    DepartmentId =12
                },
                CreatedOn = DateTime.UtcNow,
                CreatedBy = "CreatedBy",
                ModifiedOn = DateTime.UtcNow,
                DeletedOn = DateTime.UtcNow,
                DeletedBy = "DeletedBy",
                ModifiedBy = "ModifiedBy",
                BulkLettersId = 13,
                IsProcess = true,
                IsEnqueue = false,
                Is7216Available = true,
                ClientSignatureCount = 14,
                IsNewClient = false,
                SpouseFirstName = "SpouseFirst",
                SpouseEmailId = "spouse@example.com",
                SignatoryEmailId = "signatory@example.com",
                SignatoryFirstName = "SignatoryFirst",
                IsEsigning = true,
                OtherEntityDetails = "OtherEntityDetails",
                PrimarySignerLastName = "PrimarySignerLast",
                AdminEmail = "admin@example.com",
                PartnerEmail = "partner@example.com",
                SpouseLastName = "SpouseLast",
                SignatoryLastName = "SignatoryLast",
                SignatoryTitle = "SignatoryTitle",
                IsDeleted = false,  
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void EngagementLetter_TemplateNameExceedsMaxLength_FailsValidation()
        {
            // Arrange:
            var model = new EngagementLetter
            {
                EngagementLetterId = 1,
                TemplateName = new string('a', 256),
            };

            // Act:
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("TemplateName"));
        }
    }
}
