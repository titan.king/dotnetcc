﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class TemplateVersionModelUnitTests
    {
        [Fact]
        public void TemplateVersion_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new TemplateVersion
            {
                TemplateVersionId = 1,
                TemplateId = 2,
                Template = new Template
                {
                    TemplateId =2
                },
                TemplateLogic = "SampleTemplateLogic",
                VersionNumber = 1
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
