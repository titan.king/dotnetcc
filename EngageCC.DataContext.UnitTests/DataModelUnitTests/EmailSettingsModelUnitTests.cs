﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class EmailSettingsModelUnitTests
    {
        [Fact]
        public void EmailSettings_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new EmailSettings
            {
                EmailSettingsId=1,
                ServerName = "SampleServerName",
                Port = 587,
                SecurityDetails = "SampleSecurityDetails",
                UserName = "user@example.com",
                Password = "SamplePassword",
                StatusId = 1,
                IsDeleted = false,
                CreatedOn = DateTime.Now,
                ModifiedOn = null,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }   
    }
}
