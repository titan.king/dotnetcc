﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class TemplateModelUnitTests
    {
        [Fact]
        public void Template_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new Template
            {
                TemplateId = 1,
                TemplateName = "ValidTemplateName",
                TemplateDescription = "ValidDescription",
                DepartmentId = 1,
                Departments = new Department
                {
                    DepartmentId =1
                },
                EngageTypeId = 2,
                EngageType = new EngageType
                {
                    EngageTypeId =2
                },
                ChangeNotes = "ValidChangeNotes",
                IsDeleted = false,
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                CreatedBy = "ValidUser",
                ModifiedBy = "ValidUser",
                DeletedOn = DateTime.Now,
                DeletedBy = "SampleDeletedBy",
                AttachmentCount= 5,
                StatusId = 3,
                Status = new Status
                {
                    StatusId =3
                },
                AttachmentURL = "SampleAttachmentURL",
                Is7216Available = true,
                ClientSignatureCount = 10
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void Template_TemplateNameExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new Template
            {
                TemplateId = 1,
                TemplateName = new string('a', 256), 
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("TemplateName"));
        }

        [Fact]
        public void Template_ChangeNotesExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new Template
            {
                TemplateId = 1,
                ChangeNotes = new string('a', 501),
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("ChangeNotes"));
        }
    }
}
