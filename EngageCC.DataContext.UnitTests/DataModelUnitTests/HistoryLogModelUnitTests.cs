﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class HistoryLogModelUnitTests
    {
        [Fact]
        public void HistoryLog_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new HistoryLog
            {
                HistoryLogId = 1,
                EngagementLetterId = 2,
                EngagementLetterName = "Valid Engagement Letter Name",
                BatchId = 3,
                Status = "Valid Status",
                EditedBy = "Valid User",
                Version = 4,
                LastModified = DateTime.Now,
                Downloaded = DateTime.Now,
                Delegated = true,
                ClientEmailId = "valid@example.com",
                ReasonforDecline = "Valid Reason for Decline",
                DeclineTimestamp = DateTime.Now,
                PDFUrl = "Valid PDF URL",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        } 
    }
}
