﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class RolePermissionMappingModelUnitTests
    {
        [Fact]
        public void RolePermissionMapping_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new RolePermissionMapping
            {
                RolePermissionId = 1,
                RoleId = 2,
                ScreenId = 3,
                Roles = new Roles
                {
                    RoleId = 1,
                    RoleName = "RoleName",
                    AzureGroupId = 1,
                    AzureGroup = new AzureGroup
                    {
                        AzureGroupId = 1,
                        GroupId = "1",
                        GroupName = "GroupName"
                    }
                },
                Screens = new Screens
                {
                        ScreenId=1,
                        ScreenName="ScreenName"
                }
            };


            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }
    }
}
