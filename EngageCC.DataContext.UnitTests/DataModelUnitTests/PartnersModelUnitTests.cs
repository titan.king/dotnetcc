﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class PartnersModelUnitTests
    {
        [Fact]
        public void Partners_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new Partners
            {
                PartnersId = 1,
                PartnerId = 2,
                PartnerName = "Valid Partner Name",
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                IsDeleted = false,
                DeletedOn = DateTime.Now,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void Partners_PartnerNameExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new Partners
            {
                PartnersId = 1,
                PartnerId = 2,
                PartnerName = new string('a', 256),
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                IsDeleted = false,
                DeletedOn = DateTime.Now,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("PartnerName"));
        }

    }
}
