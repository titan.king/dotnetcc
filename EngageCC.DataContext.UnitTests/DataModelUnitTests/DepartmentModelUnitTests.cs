﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class DepartmentModelUnitTests
    {
        [Fact]
        public void Department_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new Department
            {
                DepartmentName = "Sample Department Name",
                IsDeleted = false,
                DepartmentStatusId = 1,
                CreatedOn = DateTime.Now,
                ModifiedOn = null,
                CreatedBy = "Sample CreatedBy",
                ModifiedBy = null,
                DeletedOn = null,
                DeletedBy = null,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void Department_InvalidDepartmentName_FailsValidation()
        {
            // Arrange
            var model = new Department
            {
                DepartmentId = 1,
                DepartmentName = new string('a', 256),
                IsDeleted = false,
                DepartmentStatusId = 1,
                CreatedOn = DateTime.Now,
                ModifiedOn = null,
                CreatedBy = "Sample CreatedBy",
                ModifiedBy = null,
                DeletedOn = null,
                DeletedBy = null,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("DepartmentName"));
        }
    }
}
