﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class DocumentStatusModelUnitTests
    {
        [Fact]
        public void DocumentStatus_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new DocumentStatus
            {
                DocumentStatusId = 1,
                Description = "Valid Description",
                Remarks = "Valid Remarks",
                IsDeleted = false,
                CreatedOn = DateTime.Now,
                ModifiedOn = null,
                CreatedBy = "Valid User",
                ModifiedBy = null,
                DeletedOn = null,
                DeletedBy = null,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

    }
}
