﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class SigningInfoModelUnitTests
    {
        [Fact]
        public void SigningInfo_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new SigningInfo
            {
                SigningInfoId = 1,
                EngagementLetterId = 2,
                EngagementLetter = new EngagementLetter
                {
                    EngagementLetterId=1
                },
                SigningPartnerName = "ValidSigningPartnerName",
                DelegatedName = "ValidDelegatedName",
                SPEmail = "valid@example.com",
                ContactPerson1Name = "ContactPerson1",
                ContactPerson1Email = "contact1@example.com",
                ContactPerson2Name = "ContactPerson2",
                ContactPerson2Email = "contact2@example.com",
                Title = "Valid Title",
                Message = "Valid Message",
                IsDeleted = false,
                CreatedBy = "CreatedBy",
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                ManualSigning = false,
                FileId = "FileId",
                DelegatedBy = "DelegatedBy",
                AgreementId = "AgreementId",
                ModifiedBy = "ModifiedBy",
                DeletedOn = DateTime.Now,
                DeletedBy = "DeletedBy",
                PdfUrl = "PdfUrl",
                ExpiryDate7216 = "ExpiryDate7216",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void SigningInfo_SigningPartnerNameExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new SigningInfo
            {
                SigningInfoId = 1,
                EngagementLetterId = 2,
                SigningPartnerName = new string('a', 256),
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("SigningPartnerName"));
        }
    }
}
