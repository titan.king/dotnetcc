﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class UserDetailsModelUnitTests
    {
        [Fact]
        public void UserDetails_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new UserDetails
            {
                Id = 1,
                LoginId = "user123",
                LoginUserName = "JohnDoe",
                GroupId = "group456",
                GroupName = "MyGroup",
                LoggedOnTime = DateTime.Now,
                IsLoggedIn = true,
                LoggedOutTime = DateTime.Now,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void UserDetails_LoginIdExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new UserDetails
            {
                Id = 1,
                LoginId = new string('a', 51), 
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("LoginId"));
        }

        [Fact]
        public void UserDetails_LoginUserNameExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new UserDetails
            {
                Id = 1,
                LoginUserName = new string('a', 51),
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("LoginUserName"));
        }

        [Fact]
        public void UserDetails_GroupIdExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new UserDetails
            {
                Id = 1,
                GroupId = new string('a', 501),
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("GroupId"));
        }

        [Fact]
        public void UserDetails_GroupNameExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new UserDetails
            {
                Id = 1,
                GroupName = new string('a', 256),
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("GroupName"));

        }
    }
}
