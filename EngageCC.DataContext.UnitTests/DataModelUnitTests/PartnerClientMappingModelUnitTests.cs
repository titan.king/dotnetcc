﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class PartnerClientMappingModelUnitTests
    {
        [Fact]
        public void PartnerClientMapping_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new PartnerClientMapping
            {
                PartnerClientMappingId = 1,
                PartnerId = 2,
                PartnerName = "Sample Partner",
                ParentClientId = 3,
                ParentClientName = "Sample Parent Client",
                ChildClient = "Sample Child Client",
                AddressDetail = "123 Main St",
                City = "Sample City",
                State = "Sample State",
                ZipCode = "12345",
                ContactName = "John Doe",
                ContactEmail = "john@example.com",
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                DeletedOn = DateTime.Now,
                IsDeleted = false
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void PartnerClientMapping_PartnerNameExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new PartnerClientMapping
            {
                PartnerClientMappingId = 1,
                PartnerId = 2,
                PartnerName = new string('a', 256), 
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("PartnerName"));
        }

        [Fact]
        public void PartnerClientMapping_ParentClientNameExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new PartnerClientMapping
            {
                PartnerClientMappingId = 1,
                PartnerId = 2,
                ParentClientName = new string('a', 256),
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("ParentClientName"));
        }

        [Fact]
        public void PartnerClientMapping_ChildClientExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new PartnerClientMapping
            {
                PartnerClientMappingId = 1,
                PartnerId = 2,
                ChildClient = new string('a', 256),
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("ChildClient"));
        }

    }
}
