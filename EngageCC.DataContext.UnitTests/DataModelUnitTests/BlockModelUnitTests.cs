﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class BlockModelUnitTests
    {
        [Fact]
        public void Block_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new Block
            {
                BlockId = 1,
                BlockName = "Valid Block Name",
                Description = "Valid Description",
                Content = "Valid Content",
                ChangeNotes = "Valid Change Notes",
                IsDeleted = false,
                StatusId = 2,
                Status = new Status
                {
                    StatusId = 2
                },
                CreatedOn = DateTime.Now,
                ModifiedOn = null,
                CreatedBy = "Valid User",
                ModifiedBy = null,
                DeletedOn = null,
                DeletedBy = null,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void Block_BlockNameExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new Block
            {
                BlockId = 1,
                BlockName = new string('a', 256), 
                Description = "Valid Description",
                Content = "Valid Content",
                ChangeNotes = "Valid Change Notes",
                IsDeleted = false,
                StatusId = 2,
                Status = new Status
                {
                    StatusId =2
                },
                CreatedOn = DateTime.Now,
                ModifiedOn = null,
                CreatedBy = "Valid User",
                ModifiedBy = null,
                DeletedOn = null,
                DeletedBy = null,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("BlockName"));
        }

        [Fact]
        public void Block_DescriptionExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new Block
            {
                BlockId = 1,
                BlockName = "Valid Block Name",
                Description = new string('a', 256),
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("Description"));
        }

        [Fact]
        public void Block_InvalidCreatedBy_FailsValidation()
        {
            // Arrange
            var model = new Block
            {
                BlockId = 1,
                BlockName = "Valid Block Name",
                Description = "Valid Description",
                Content = "Valid Content",
                ChangeNotes = "Valid Change Notes",
                IsDeleted = false,
                StatusId = 2,
                CreatedOn = DateTime.Now,
                ModifiedOn = null,
                CreatedBy = new string('a', 41), 
                ModifiedBy = null,
                DeletedOn = null,
                DeletedBy = null,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("CreatedBy"));
        }
    }
}
