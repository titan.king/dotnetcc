﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class AzureGroupModelUnitTests
    {
        [Fact]
        public void AzureGroup_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new AzureGroup
            {
                AzureGroupId = 1,
                GroupId = "ValidGroupId",
                GroupName = "ValidGroupName",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void AzureGroup_GroupIdExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new AzureGroup
            {
                AzureGroupId = 1,
                GroupId = new string('a', 256),
                GroupName = "ValidGroupName",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("GroupId"));
        }

        [Fact]
        public void AzureGroup_GroupNameExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new AzureGroup
            {
                AzureGroupId = 1,
                GroupId = "ValidGroupId",
                GroupName = new string('a', 256),
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("GroupName"));
        }
    }
}
