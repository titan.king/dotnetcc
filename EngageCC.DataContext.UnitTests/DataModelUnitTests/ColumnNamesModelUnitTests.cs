﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class ColumnNamesModelUnitTests
    {
        [Fact]
        public void ColumnNames_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new ColumnNames
            {
                ColumnNamesId = 1,
                ColumnName = "ValidColumnName",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void ColumnNames_ColumnNameExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new ColumnNames
            {
                ColumnNamesId = 1,
                ColumnName = new string('a', 256),
            };

            // Act and Assert
            Assert.Throws<ValidationException>(() => Validator.ValidateObject(model, new ValidationContext(model), true));
        }
    }
}
