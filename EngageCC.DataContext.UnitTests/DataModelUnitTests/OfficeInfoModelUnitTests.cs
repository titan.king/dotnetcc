﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class OfficeInfoModelUnitTests
    {
        [Fact]
        public void OfficeInfo_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new OfficeInfo
            {
                OfficeInfoId = 1,
                OfficeId = 2,
                Name = "Office Name",
                Office = "Office",
                Address = "Office Address",
                City = "Office City",
                State = "Office State",
                ZipCode = "12345",
                StateSignatureNameAddress = "State Signature Address",
                StateSignatureCity = "State Signature City",
                StateSignatureState = "State Signature State",
                StateSignatureZipCode = "54321",
                PhoneNumber = "123-456-7890",
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                IsDeleted = false,
                DeletedOn = DateTime.Now,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void OfficeInfo_NameExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new OfficeInfo
            {
                OfficeInfoId = 1,
                OfficeId = 2,
                Name = new string('a', 256), // Invalid, exceeds MaxLength
                Office = "Office",
                Address = "Office Address",
                City = "Office City",
                State = "Office State",
                ZipCode = "12345",
                StateSignatureNameAddress = "State Signature Address",
                StateSignatureCity = "State Signature City",
                StateSignatureState = "State Signature State",
                StateSignatureZipCode = "54321",
                PhoneNumber = "123-456-7890",
                CreatedOn = DateTime.Now,
                IsDeleted = false
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("Name"));
        }
    }
}
