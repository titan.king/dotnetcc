﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class BulkLettersModelUnitTests
    {
        [Fact]
        public void BulkLetters_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new BulkLetters
            {
                BulkLettersId = 1,
                ClientId = 2,
                ClientName = "SampleClientName",
                Office = "SampleOffice",
                SignatoryEmailId = "signatory@example.com",
                SignatoryFirstName = "John",
                PartnerName = "SamplePartner",
                BatchId = 3,
                TemplateName = "SampleTemplate",
                FieldJson = "{ \"field1\": \"value1\", \"field2\": \"value2\" }",
                DocumentStatusName = "Draft",
                IsDraft = false,
                IsDeleted = false,
                IsUpdated = false,
                CreatedOn = DateTime.Now,
                ModifiedOn = null, 
                CreatedBy = "SampleUser",
                ModifiedBy = null,
                DeletedOn = null,
                DeletedBy = null,
                IsBulkLetter = true,
                IsProcess = false,
                IsEnqueue = false,
                PartnerId = 4,
                IsEsigning = false,
                Is7216Available = true,
                ClientSignatureCount = 5,
                IsNewClient = true,
                SpouseFirstName = "Jane",
                SpouseEmailId = "spouse@example.com",
                TaxYear = "2023",
                OfficeId = 6,
                IsBatchActive = true,
                IsBatchDelete = false
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void BulkLetters_InvalidClientName_FailsValidation()
        {
            // Arrange
            var model = new BulkLetters
            {
                BulkLettersId = 1,
                ClientId = 2,
                ClientName = new string('a', 256),
                CreatedOn = DateTime.Now,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("ClientName"));
        }
    }
}
