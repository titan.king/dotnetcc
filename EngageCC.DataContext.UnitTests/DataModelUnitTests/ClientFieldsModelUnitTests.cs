﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class ClientFieldsModelUnitTests
    {
        [Fact]
        public void ClientFields_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new ClientFields
            {
                ClientFieldsId = 1,
                ClientFirstName = "John",
                Spouse = "Jane",
                ClientLastName = "Doe",
                Signing = "John Doe",
                Officer = "CEO",
                Lastname_Spouse = "Doe",
                TrustEsatate_Name = "EstateName",
                TrusteeFiduciary_Name = "FiduciaryName",
                TrusteeFiduciary_Last_Name = "FiduciaryLastName",
                TrusteeFiduciary_Title = "FiduciaryTitle",
                Year = "2023",
                TrustName = "TrustName",
                Trust = "Trust",
                HusbandFullName = "John Doe",
                WifeFullName = "Jane Doe",
                EstateName = "EstateName",
                Trust_Name = "TrustName",
                Street_Address = "123 Main St",
                City_Address = "City Address",
                City = "City",
                State = "State",
                Business_Name = "BusinessName",
                ClientId = 123,
                NameofClients = "Name of Clients",
                AddressofClients = "Address of Clients",
                NameofClient = "Name of Client",
                ClientName = "Client Name",
                Address1 = "Address 1",
                Address2 = "Address 2",
                OfficeLocation = 456,
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                IsDeleted = false,
                DeletedOn = DateTime.Now,

            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new System.Collections.Generic.List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void ClientFields_ClientFirstNameExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new ClientFields
            {
                ClientFieldsId = 1,
                ClientFirstName = new string('A', 256), 
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("ClientFirstName"));
        }

    }
}
