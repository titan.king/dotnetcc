﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class JurisdictionModelUnitTests
    {

        [Fact]
        public void Jurisdiction_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new Jurisdiction
            {
                JurisdictionId = 1,
                CitrinDatabase = "Valid Citrin Database",
                ReceivedFromCitrin = "Valid Data Received from Citrin",
                JurisdictionData = "Valid Jurisdiction Data",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void Jurisdiction_CitrinDatabaseExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new Jurisdiction
            {
                JurisdictionId = 1,
                CitrinDatabase = new string('a', 256),
                ReceivedFromCitrin = "Valid Data Received from Citrin",
                JurisdictionData = "Valid Jurisdiction Data",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("CitrinDatabase"));
        }
    }
}
