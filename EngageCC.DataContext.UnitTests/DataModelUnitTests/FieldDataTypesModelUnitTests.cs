﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class FieldDataTypesModelUnitTests
    {
        [Fact]
        public void FieldDataTypes_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new FieldDataTypes
            {
                FieldDataTypeId = 1,
                FieldDataType = "Valid Field Data Type",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

    }
}
