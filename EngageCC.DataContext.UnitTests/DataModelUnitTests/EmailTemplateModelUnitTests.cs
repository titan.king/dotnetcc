﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class EmailTemplateModelUnitTests
    {
        [Fact]
        public void EmailTemplate_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new EmailTemplate
            {
                EmailTemplateId = 1,
                DocumentStatusId = 2,
                EmailSubject = "Valid Email Subject",
                EmailContent = "Valid Email Content",
                IsDeleted = false,
                CreatedOn = DateTime.Now,
                ModifiedOn = null,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

    }
}
