﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class MasterTemplateModelUnitTests
    {
        [Fact]
        public void MasterTemplate_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new MasterTemplate
            {
                MasterTemplateId = 1,
                TemplateId = 2,
                TemplateName = "Valid Template Name",
                TemplateVersionId = 3,
                Template = "Valid Template Content",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void MasterTemplate_MissingTemplateName_FailsValidation()
        {
            // Arrange
            var model = new MasterTemplate
            {
                MasterTemplateId = 1,
                TemplateId = 2,
                TemplateVersionId = 3,
                Template = "Valid Template Content",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("TemplateName"));
        }

        [Fact]
        public void MasterTemplate_TemplateNameExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new MasterTemplate
            {
                MasterTemplateId = 1,
                TemplateId = 2,
                TemplateName = new string('a', 256), 
                TemplateVersionId = 3,
                Template = "Valid Template Content",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("TemplateName"));
        }

        [Fact]
        public void MasterTemplate_MissingTemplateContent_FailsValidation()
        {
            // Arrange
            var model = new MasterTemplate
            {
                MasterTemplateId = 1,
                TemplateId = 2,
                TemplateName = "Valid Template Name",
                TemplateVersionId = 3,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("Template"));
        }
    }
}

