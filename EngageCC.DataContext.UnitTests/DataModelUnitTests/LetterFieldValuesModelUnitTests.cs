﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class LetterFieldValuesModelUnitTests
    {
        [Fact]
        public void LetterFieldValues_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new LetterFieldValues
            {
                LetterFieldValuesId = 1,
                ClientId = 2,
                PartnerId = 3,
                BulkLettersId = 4,
                BulkLetters = new BulkLetters
                {
                    BulkLettersId =4
                },
                FieldId = 5,
                FieldValue = "Valid Field Value",
                FieldName = "Valid Field Name",
                LetterVersion = 6,
                DisplayName = "Valid Display Name",
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

    }
}
