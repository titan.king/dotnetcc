﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class LoginInfoModelUnitTests
    {
        [Fact]
        public void LoginInfo_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new LoginInfo
            {
                LoginInfoId = 1,
                UserName = "ValidUserName",
                UserObjectId = "ValidUserObjectId"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void LoginInfo_UserNameExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new LoginInfo
            {
                LoginInfoId = 1,
                UserName = new string('a', 256),
                UserObjectId = "ValidUserObjectId"
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("UserName"));
        }

        [Fact]
        public void LoginInfo_UserObjectIdExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new LoginInfo
            {
                LoginInfoId = 1,
                UserName = "ValidUserName",
                UserObjectId = new string('a', 51),
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("UserObjectId"));
        }
    }
}
