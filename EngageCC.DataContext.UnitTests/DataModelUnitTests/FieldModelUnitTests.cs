﻿using EngageCC.DataContext.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EngageCC.DataContext.UnitTests.DataModelUnitTests
{
    public class FieldModelUnitTests
    {
        [Fact]
        public void Field_ValidModel_PassesValidation()
        {
            // Arrange
            var model = new Field
            {
                FieldId = 1,
                FieldName = "FieldName",
                DisplayName = "Display Name",
                FieldDataTypeId = 2,
                FieldDataTypes = new FieldDataTypes
                {
                    FieldDataTypeId =2
                },
                HintText = "Hint Text",
                ChangeNotes = "Change Notes",
                IsDeleted = false,
                CreatedOn = DateTime.Now,
                ModifiedOn = DateTime.Now,
                DirectInput = true,
                MDDLookup = false,
                ColumnId = 3,
                MDDColumnNames = "MDD Column Names",
                MDDColumnData = "MDD Column Data",
                CreatedBy = "Valid User",
                ModifiedBy = null,
                DeletedOn = null,
                DeletedBy = null,
                StatusId = 4,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.True(isValid);
        }

        [Fact]
        public void Field_FieldNameExceedsMaxLength_FailsValidation()
        {
            // Arrange
            var model = new Field
            {
                FieldId = 1,
                FieldName = new string('a', 51), 
                DisplayName = "Display Name",
                FieldDataTypeId = 2,
                HintText = "Hint Text",
                ChangeNotes = "Change Notes",
                CreatedOn = DateTime.Now,
                DirectInput = true,
                MDDLookup = false,
                ColumnId = 3,
                MDDColumnNames = "MDD Column Names",
                MDDColumnData = "MDD Column Data",
                CreatedBy = "Valid User",
                ModifiedBy = null,
                DeletedOn = null,
                DeletedBy = null,
                StatusId = 4,
            };

            // Act
            var validationContext = new ValidationContext(model);
            var validationResults = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(model, validationContext, validationResults, true);

            // Assert
            Assert.False(isValid);
            Assert.Contains(validationResults, result => result.MemberNames.Contains("FieldName"));
        }
    }
}
